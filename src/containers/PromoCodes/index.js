import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFilter, faPlus, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  getAllPromoCodes,
  addPromoCodeReset,
  deletePromoCode,
  toggleAddNewPromoCodeModal,
  openEditPromoModal,
} from 'redux-modules/promoCodes/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import PromoCodes from 'components/PromoCodes';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class PromoCodesContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    promoCodes: PropTypes.arrayOf(PropTypes.shape({})),
    hasMorePromoCodes: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    isGetAllPromoCodesFetching: PropTypes.bool,
    isGetAllPromoCodesSuccess: PropTypes.bool,
    isGetAllPromoCodesError: PropTypes.bool,
    isDeletePromoCodeFetching: PropTypes.bool,
    isDeletePromoCodeSuccess: PropTypes.bool,
    getAllPromoCodes: PropTypes.func,
    addPromoCodeReset: PropTypes.func,
    deletePromoCode: PropTypes.func,
    toggleAddNewPromoCodeModal: PropTypes.func,
    openEditPromoModal: PropTypes.func,
    isAddNewPromoCodeModalOpen: PropTypes.bool,
    isEditPromoModalOpen: PropTypes.bool,
    getAllPromoCodesErrorMessage: PropTypes.string,
    isDeletePromoCodeError: PropTypes.bool,
    isDeletePromoCodeErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    promoCodes: undefined,
    hasMorePromoCodes: true,
    applyNewFilter: false,
    isGetAllPromoCodesFetching: false,
    isGetAllPromoCodesSuccess: false,
    isGetAllPromoCodesError: false,
    isDeletePromoCodeFetching: false,
    isDeletePromoCodeSuccess: false,
    getAllPromoCodes: () => {},
    addPromoCodeReset: () => {},
    deletePromoCode: () => {},
    toggleAddNewPromoCodeModal: () => {},
    openEditPromoModal: () => {},
    isAddNewPromoCodeModalOpen: false,
    isEditPromoModalOpen: false,
    getAllPromoCodesErrorMessage: undefined,
    isDeletePromoCodeError: false,
    isDeletePromoCodeErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('promoCodesFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      isConfirmModalOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      index: 0,
      promoDetails: undefined,
    };
  }

  componentDidMount() {
    const { getAllPromoCodes: getAllPromoCodesAction } = this.props;
    const { filterQueries } = this.state;

    getAllPromoCodesAction(filterQueries, true);
  }

  componentDidUpdate(prevProps) {
    const { isDeletePromoCodeSuccess } = this.props;

    if (
      isDeletePromoCodeSuccess &&
      isDeletePromoCodeSuccess !== prevProps.isDeletePromoCodeSuccess
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }
  }

  /**
   * Creates lazy loading promoCodes block
   */
  getLoadingPromoCodes = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of promoCodes blocks
   */
  createLoadingPromoCodesList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingPromoCodes(counter));
    }
    return list;
  };

  loadMorePromoCodes = () => {
    const { getAllPromoCodes: getAllPromoCodesAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllPromoCodesAction(filterQueriesNextState);
      },
    );
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleAddNewPromoCodeModal = () => {
    const {
      toggleAddNewPromoCodeModal: toggleAddNewPromoCodeModalAction,
      addPromoCodeReset: addPromoCodeResetAction,
    } = this.props;

    addPromoCodeResetAction();
    toggleAddNewPromoCodeModalAction();
  };

  handleToggleEditPromoModal = (index, promoDetails) => {
    const { openEditPromoModal: openEditPromoModalAction } = this.props;
    this.setState({ promoDetails, index });
    openEditPromoModalAction();
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user,
      user: { permissions },
      promoCodes,
      hasMorePromoCodes,
      applyNewFilter,
      isGetAllPromoCodesFetching,
      isGetAllPromoCodesSuccess,
      isGetAllPromoCodesError,
      getAllPromoCodes: getAllPromoCodesAction,
      deletePromoCode: deletePromoCodeAction,
      isAddNewPromoCodeModalOpen,
      isEditPromoModalOpen,
      isDeletePromoCodeFetching,
      getAllPromoCodesErrorMessage,
      isDeletePromoCodeError,
      isDeletePromoCodeErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      isConfirmModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      filterQueries,
      index,
      promoDetails,
    } = this.state;

    let promoCodesRenderer;

    if (
      (isGetAllPromoCodesFetching && (!promoCodes || applyNewFilter)) ||
      (!isGetAllPromoCodesFetching && !isGetAllPromoCodesSuccess && !isGetAllPromoCodesError)
    ) {
      promoCodesRenderer = this.createLoadingPromoCodesList();
    } else if (isGetAllPromoCodesError) {
      promoCodesRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllPromoCodesErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!promoCodes.length) {
      promoCodesRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      promoCodesRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={promoCodes.length}
            next={this.loadMorePromoCodes}
            hasMore={hasMorePromoCodes}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <PromoCodes
              user={user}
              promoCodes={promoCodes}
              isConfirmModalOpen={isConfirmModalOpen}
              isAddNewPromoCodeModalOpen={isAddNewPromoCodeModalOpen}
              confirmModalHeader={confirmModalHeader}
              confirmModalText={confirmModalText}
              confirmModalFunction={confirmModalFunction}
              handleToggleConfirmModal={this.handleToggleConfirmModal}
              handleDeletePromoCode={deletePromoCodeAction}
              isDeletePromoCodeFetching={isDeletePromoCodeFetching}
              promoIndex={index}
              promoDetails={promoDetails}
              isEditPromoModalOpen={isEditPromoModalOpen}
              handleToggleEditPromoModal={this.handleToggleEditPromoModal}
              isDeletePromoCodeError={isDeletePromoCodeError}
              isDeletePromoCodeErrorMessage={isDeletePromoCodeErrorMessage}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>البروموكود</title>
        </Helmet>
        {permissions && permissions.includes('Get All Promo Codes') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                البروموكود
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Add New Promo Code') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={this.handleToggleAddNewPromoCodeModal}
                    icon={faPlus}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={false}
                    reverse
                  >
                    <Text
                      cursor="pointer"
                      mx={2}
                      type={SUBHEADING}
                      color={COLORS_VALUES[DISABLED]}
                      fontWeight={NORMAL}
                    >
                      أضف بروموكود
                    </Text>
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="promoCodes"
              filterSections={['promoCode', 'field', 'city', 'isForNewCustomer']}
              fieldsQueryKey="field"
              citiesQueryKey="city"
              promoCodeQueryKey="code"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllPromoCodesAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {promoCodesRenderer}
          </>
        )}
      </Flex>
    );
  }
}
PromoCodesContainer.displayName = 'PromoCodesContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  promoCodes: state.promoCodes.promoCodes,
  hasMorePromoCodes: state.promoCodes.hasMorePromoCodes,
  applyNewFilter: state.promoCodes.applyNewFilter,
  isGetAllPromoCodesFetching: state.promoCodes.getAllPromoCodes.isFetching,
  isGetAllPromoCodesError: state.promoCodes.getAllPromoCodes.isFail.isError,
  getAllPromoCodesErrorMessage: state.promoCodes.getAllPromoCodes.isFail.message,
  isGetAllPromoCodesSuccess: state.promoCodes.getAllPromoCodes.isSuccess,
  isDeletePromoCodeFetching: state.promoCodes.deletePromoCode.isFetching,
  isDeletePromoCodeSuccess: state.promoCodes.deletePromoCode.isSuccess,
  isDeletePromoCodeError: state.promoCodes.deletePromoCode.isFail.isError,
  isDeletePromoCodeErrorMessage: state.promoCodes.deletePromoCode.isFail.message,
  isAddNewPromoCodeModalOpen: state.promoCodes.addNewPromoCodeModal.isOpen,
  isEditPromoModalOpen: state.promoCodes.editPromoModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllPromoCodes,
      addPromoCodeReset,
      deletePromoCode,
      toggleAddNewPromoCodeModal,
      openEditPromoModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(PromoCodesContainer);
