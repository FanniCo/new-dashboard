import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFilter, faDownload, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  getAllClients,
  suspendClient,
  editClientReset,
  convertClientToWorkerReset,
  toggleEditClientModal,
  toggleConvertClientToWorkerModal,
  toggleClientAddCreditModal,
  clientAddCreditReset,
  toggleSuspendModal,
  toggleConfirmModal,
} from 'redux-modules/clients/actions';
import { getUsersForEachTypeCount } from 'redux-modules/dashboard/actions';
import ROUTES from 'routes';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import Clients from 'components/Clients';
import { Urls } from 'utils/api';
import { exportFile, getAllAdmins } from 'redux-modules/user/actions';
import Card from 'components/Card';
import EmptyState from 'components/EmptyState';
import InputField from 'components/InputField';
import { USER_ROLES } from 'utils/constants';
import Caution from 'components/Caution';

const { SINGLE_WORKER } = ROUTES;
const {
  GREY_MEDIUM,
  DISABLED,
  BRANDING_GREEN,
  PRIMARY,
  WHITE,
  GREY_LIGHT,
  GREY_DARK,
  LIGHT_RED,
  ERROR,
} = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class ClientsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    clients: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreClients: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    history: PropTypes.shape({}).isRequired,
    isGetAllClientsFetching: PropTypes.bool,
    isGetAllClientsSuccess: PropTypes.bool,
    isGetAllClientsError: PropTypes.bool,
    isSuspendClientFetching: PropTypes.bool,
    isSuspendClientError: PropTypes.bool,
    isSuspendClientSuccess: PropTypes.bool,
    isConvertClientToWorkerSuccess: PropTypes.bool,
    getAllClients: PropTypes.func,
    suspendClient: PropTypes.func,
    editClientReset: PropTypes.func,
    convertClientToWorkerReset: PropTypes.func,
    toggleEditClientModal: PropTypes.func,
    toggleConvertClientToWorkerModal: PropTypes.func,
    isEditClientModalOpen: PropTypes.bool,
    isConvertClientToWorkerModalOpen: PropTypes.bool,
    toggleClientAddCreditModal: PropTypes.func,
    clientAddCreditReset: PropTypes.func,
    isAddCreditModalOpen: PropTypes.bool,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    toggleSuspendModal: PropTypes.func,
    isSuspendModalOpen: PropTypes.bool,
    toggleConfirmModal: PropTypes.func,
    isConfirmModalOpened: PropTypes.bool,
    suspendClientErrorMessage: PropTypes.string,
    getAllClientsErrorMessage: PropTypes.string,
    getAllAdmins: PropTypes.func,
    getUsersForEachTypeCount: PropTypes.func,
    isGetUsersCountsForEachTypeFetching: PropTypes.bool,
    isGetUsersCountsForEachTypeSuccess: PropTypes.bool,
    isGetUsersCountsForEachTypeError: PropTypes.bool,
    getUsersCountsForEachTypeErrorMessage: PropTypes.string,
    UsersCountForEachType: PropTypes.shape({}),
  };

  static defaultProps = {
    user: undefined,
    clients: undefined,
    hasMoreClients: true,
    applyNewFilter: false,
    isGetAllClientsFetching: false,
    isGetAllClientsSuccess: false,
    isGetAllClientsError: false,
    isSuspendClientFetching: false,
    isSuspendClientError: false,
    isSuspendClientSuccess: false,
    isEditClientModalOpen: false,
    isConvertClientToWorkerModalOpen: false,
    isConvertClientToWorkerSuccess: false,
    getAllClients: () => {},
    suspendClient: () => {},
    editClientReset: () => {},
    convertClientToWorkerReset: () => {},
    toggleEditClientModal: () => {},
    toggleConvertClientToWorkerModal: () => {},
    toggleClientAddCreditModal: () => {},
    clientAddCreditReset: () => {},
    isAddCreditModalOpen: false,
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    toggleSuspendModal: () => {},
    isSuspendModalOpen: false,
    toggleConfirmModal: () => {},
    isConfirmModalOpened: false,
    suspendClientErrorMessage: undefined,
    getAllClientsErrorMessage: undefined,
    getAllAdmins: () => {},
    getUsersForEachTypeCount: () => {},
    isGetUsersCountsForEachTypeFetching: false,
    isGetUsersCountsForEachTypeSuccess: false,
    isGetUsersCountsForEachTypeError: false,
    getUsersCountsForEachTypeErrorMessage: undefined,
    UsersCountForEachType: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('clientsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      isOpsCommentsModalOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      clientIndex: 0,
      clientDetails: undefined,
    };
  }

  componentDidMount() {
    const {
      getAllClients: getAllClientsAction,
      getAllAdmins: getAllAdminsAction,
      getUsersForEachTypeCount: getUsersForEachTypeCountAction,
    } = this.props;
    const { filterQueries } = this.state;

    getAllClientsAction(filterQueries, true);
    getUsersForEachTypeCountAction({ role: 'customer', ...filterQueries }, true);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  componentDidUpdate(prevProps) {
    const { isConvertClientToWorkerSuccess } = this.props;

    if (
      isConvertClientToWorkerSuccess &&
      isConvertClientToWorkerSuccess !== prevProps.isConvertClientToWorkerSuccess
    ) {
      const { history } = this.props;
      const {
        clientDetails: { username },
      } = this.state;

      history.push(`${SINGLE_WORKER}/${username}`);
    }
  }

  /**
   * Creates lazy loading request block
   */
  getLoadingClients = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests blocks
   */
  createLoadingClientsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingClients(counter));
    }
    return list;
  };

  loadMoreClients = () => {
    const { getAllClients: getAllClientsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllClientsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleEditClientModal = (clientIndex, clientDetails) => {
    const {
      toggleEditClientModal: toggleEditClientModalAction,
      editClientReset: editClientResetAction,
    } = this.props;

    this.setState(
      {
        clientIndex,
        clientDetails,
      },
      () => {
        editClientResetAction();
        toggleEditClientModalAction();
      },
    );
  };

  handleToggleConvertClientToWorkerModal = (clientIndex, clientDetails) => {
    const {
      toggleConvertClientToWorkerModal: toggleConvertClientToWorkerModalAction,
      convertClientToWorkerReset: convertClientToWorkerResetAction,
    } = this.props;

    this.setState(
      {
        clientIndex,
        clientDetails,
      },
      () => {
        convertClientToWorkerResetAction();
        toggleConvertClientToWorkerModalAction();
      },
    );
  };

  handleToggleOpsCommentsModal = index => {
    const { isOpsCommentsModalOpen } = this.state;

    this.setState({ clientIndex: index }, () => {
      this.setState({
        isOpsCommentsModalOpen: !isOpsCommentsModalOpen,
      });
    });
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { toggleConfirmModal: toggleConfirmModalAction, isConfirmModalOpened } = this.props;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpened ? header : '',
        confirmModalText: !isConfirmModalOpened ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpened ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpened);
      },
    );
  };

  handleToggleSuspendModal = (clientIndex, clientDetails) => {
    const { toggleSuspendModal: toggleSuspendModalAction } = this.props;
    this.setState(
      {
        clientIndex,
        clientDetails,
      },
      () => {
        toggleSuspendModalAction();
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } }, () => {
      this.setState(
        { filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } },
        () => {
          const { filterQueries: newFilterQueries } = this.state;
          const { getUsersForEachTypeCount: getUsersForEachTypeCountAction } = this.props;

          getUsersForEachTypeCountAction({ role: 'customer', ...newFilterQueries }, true);
        },
      );
    });
  };

  handleToggleAddCreditModal = (clientIndex, clientDetails) => {
    const {
      toggleClientAddCreditModal: toggleAddCreditModalAction,
      clientAddCreditReset: addCreditResetAction,
    } = this.props;

    this.setState(
      {
        clientIndex,
        clientDetails,
      },
      () => {
        addCreditResetAction();
        toggleAddCreditModalAction();
      },
    );
  };

  handleExportFile = () => {
    let {
      clients: { exportClients },
    } = Urls;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('clientsFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0 && !exportClients.includes('?')) {
          exportClients = `${exportClients}?${key}=${cachedFilterQueries[key]}`;
        } else {
          exportClients = `${exportClients}&${key}=${cachedFilterQueries[key]}`;
        }
        return true;
      });
    }
    exportFileAction(exportClients);
  };

  render() {
    const {
      user,
      user: { permissions },
      clients,
      hasMoreClients,
      applyNewFilter,
      isGetAllClientsFetching,
      isGetAllClientsSuccess,
      isGetAllClientsError,
      getAllClients: getAllClientsAction,
      suspendClient: suspendClientAction,
      isSuspendClientFetching,
      isSuspendClientError,
      isEditClientModalOpen,
      isConvertClientToWorkerModalOpen,
      isAddCreditModalOpen,
      isExportFileFetching,
      isExportFileSuccess,
      isSuspendModalOpen,
      isConfirmModalOpened,
      suspendClientErrorMessage,
      getAllClientsErrorMessage,
      UsersCountForEachType,
      isGetUsersCountsForEachTypeSuccess,
      isGetUsersCountsForEachTypeError,
      getUsersCountsForEachTypeErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      isOpsCommentsModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      filterQueries,
      clientIndex,
      clientDetails,
    } = this.state;

    let clientsRenderer;

    if (
      (isGetAllClientsFetching && (!clients || applyNewFilter)) ||
      (!isGetAllClientsFetching && !isGetAllClientsSuccess && !isGetAllClientsError)
    ) {
      clientsRenderer = this.createLoadingClientsList();
    } else if (isGetAllClientsError) {
      clientsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllClientsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!clients.length) {
      clientsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      clientsRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={clients.length}
            next={this.loadMoreClients}
            hasMore={hasMoreClients}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <Clients
              user={user}
              clients={clients}
              clientIndex={clientIndex}
              clientDetails={clientDetails}
              isConfirmModalOpen={isConfirmModalOpened}
              confirmModalHeader={confirmModalHeader}
              confirmModalText={confirmModalText}
              confirmModalFunction={confirmModalFunction}
              isOpsCommentsModalOpen={isOpsCommentsModalOpen}
              isEditClientModalOpen={isEditClientModalOpen}
              isConvertClientToWorkerModalOpen={isConvertClientToWorkerModalOpen}
              handleToggleOpsCommentsModal={this.handleToggleOpsCommentsModal}
              handleToggleConfirmModal={this.handleToggleConfirmModal}
              handleToggleEditClientModal={this.handleToggleEditClientModal}
              handleToggleConvertClientToWorkerModal={this.handleToggleConvertClientToWorkerModal}
              handleSuspendClient={suspendClientAction}
              isSuspendClientFetching={isSuspendClientFetching}
              isSuspendClientError={isSuspendClientError}
              handleToggleAddCreditModal={this.handleToggleAddCreditModal}
              isAddCreditModalOpen={isAddCreditModalOpen}
              handleToggleSuspendModal={this.handleToggleSuspendModal}
              isSuspendModalOpen={isSuspendModalOpen}
              suspendClientErrorMessage={suspendClientErrorMessage}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>العملاء</title>
        </Helmet>
        {permissions && permissions.includes('Get All Users') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                العملاء
              </Text>
              <Flex flexDirection="row-reverse" px={3}>
                <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} my={2}>
                    مجموع العملاء
                  </Text>
                  {!isGetUsersCountsForEachTypeError && isGetUsersCountsForEachTypeSuccess && (
                    <InputField
                      type="text"
                      width={1}
                      placeholder="المجموع"
                      value={
                        isGetUsersCountsForEachTypeSuccess
                          ? UsersCountForEachType[USER_ROLES.CUSTOMER].count
                          : ''
                      }
                      mb={2}
                      disabled
                      color={COLORS_VALUES[WHITE]}
                    />
                  )}
                  {isGetUsersCountsForEachTypeError && (
                    <Flex flexDirection="row-reverse" px={3}>
                      <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                        {getUsersCountsForEachTypeErrorMessage}
                      </Caution>
                    </Flex>
                  )}
                </Flex>
              </Flex>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                >
                  <Text
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Download Customers Report') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={() => this.handleExportFile()}
                    icon={faDownload}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={isExportFileFetching && !isExportFileSuccess}
                    disable={isExportFileFetching && !isExportFileSuccess}
                    reverse
                  >
                    تصدير الي ملف
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="clients"
              filterSections={[
                'clientUsername',
                'requestsCount',
                'requestsStatus',
                'clientEmail',
                'active',
                'suspended',
                'city',
                'dates',
                'requestsDates',
              ]}
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllClientsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
              isRequestsStatusMulti={false}
            />
            {clientsRenderer}
          </>
        )}
      </Flex>
    );
  }
}
ClientsContainer.displayName = 'ClientsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  clients: state.clients.clients,
  hasMoreClients: state.clients.hasMoreClients,
  applyNewFilter: state.clients.applyNewFilter,
  isGetAllClientsFetching: state.clients.getAllClients.isFetching,
  isGetAllClientsError: state.clients.getAllClients.isFail.isError,
  getAllClientsErrorMessage: state.clients.getAllClients.isFail.message,
  isGetAllClientsSuccess: state.clients.getAllClients.isSuccess,
  isSuspendClientFetching: state.clients.suspendClient.isFetching,
  isSuspendClientError: state.clients.suspendClient.isFail.isError,
  isSuspendClientSuccess: state.clients.suspendClient.isSuccess,
  suspendClientErrorMessage: state.clients.suspendClient.isFail.message,
  isConvertClientToWorkerSuccess: state.clients.convertClientToWorker.isSuccess,
  isEditClientModalOpen: state.clients.editClientModal.isOpen,
  isConvertClientToWorkerModalOpen: state.clients.convertClientToWorkerModal.isOpen,
  isAddCreditModalOpen: state.clients.clientAddCreditsModal.isOpen,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,

  isSuspendModalOpen: state.clients.suspendModal.isOpen,
  isConfirmModalOpened: state.clients.confirmModal.isOpen,

  isGetUsersCountsForEachTypeFetching: state.dashboard.getUsersForEachTypeCount.isFetching,
  isGetUsersCountsForEachTypeSuccess: state.dashboard.getUsersForEachTypeCount.isSuccess,
  isGetUsersCountsForEachTypeError: state.dashboard.getUsersForEachTypeCount.isFail.isError,
  getUsersCountsForEachTypeErrorMessage: state.dashboard.getUsersForEachTypeCount.isFail.message,
  UsersCountForEachType: state.dashboard.UsersForEachTypeCount,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllClients,
      suspendClient,
      editClientReset,
      convertClientToWorkerReset,
      toggleEditClientModal,
      toggleConvertClientToWorkerModal,
      toggleClientAddCreditModal,
      clientAddCreditReset,
      exportFile,
      toggleSuspendModal,
      toggleConfirmModal,
      getAllAdmins,
      getUsersForEachTypeCount,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ClientsContainer);
