import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faSpinner /* faDownload */ } from '@fortawesome/free-solid-svg-icons';
import { getAllTransactions } from 'redux-modules/transactions/actions';
import { exportFile } from 'redux-modules/user/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import TransactionsList from 'components/TransactionsLogs';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class TransactionsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    transactionsClassification: PropTypes.shape({}),
    transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
    transactionsState: PropTypes.shape({}),
    isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
    getAllTransactions: PropTypes.func,
    isGetAllTransactionsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    isGetAllTransactionsSuccess: PropTypes.bool,
    isGetAllTransactionsError: PropTypes.bool,
    getAllTransactionsErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    transactionsClassification: undefined,
    transactionsList: [],
    transactionsState: undefined,
    getAllTransactions: () => {},
    isGetAllTransactionsFetching: false,
    applyNewFilter: false,
    isGetAllTransactionsSuccess: false,
    isGetAllTransactionsError: false,
    getAllTransactionsErrorMessage: false,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('transactionsLogsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      isVendorDetailsModalOpen: false,
      activeVendorDetails: {},
    };
  }

  componentDidMount() {
    const { getAllTransactions: getAllTransactionsAction } = this.props;
    const { filterQueries } = this.state;

    getAllTransactionsAction(filterQueries, true);
  }

  loadMoreTransactions = () => {
    const { getAllTransactions: getAllTransactionsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllTransactionsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  createLoadingTransactionsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingTransactions(counter));
    }
    return list;
  };

  handleToggleOpenVendorDetailsModal = vendor => {
    const { isVendorDetailsModalOpen } = this.state;

    this.setState({
      isVendorDetailsModalOpen: !isVendorDetailsModalOpen,
      activeVendorDetails: vendor,
    });
  };

  render() {
    const {
      user: { permissions },
      transactionsClassification,
      transactionsList,
      transactionsState,
      isLoadMoreTransactionsAvailable,
      getAllTransactions: getAllTransactionsAction,
      isGetAllTransactionsFetching,
      applyNewFilter,
      isGetAllTransactionsSuccess,
      isGetAllTransactionsError,
      getAllTransactionsErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      activeVendorDetails,
      isVendorDetailsModalOpen,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>المعاملات</title>
        </Helmet>
        {permissions && permissions.includes('Get Transactions') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                المعاملات
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="transactionsLogs"
              transactionsClassification={transactionsClassification}
              filterSections={[
                'workerUsername',
                'groupedTransactionTypes',
                'transactionClassificationForGroupedTransactionTypes',
                'commissionRate',
                'transactionSign',
                'field',
                'credits',
                'dates',
              ]}
              workerUsernameQueryKey="username"
              creditsTitle="القيمة"
              minCreditsQueryKey="min_worth"
              maxCreditsQueryKey="max_worth"
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllTransactionsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            <Box width={1}>
              <InfiniteScroll
                dataLength={transactionsList.length}
                next={this.loadMoreTransactions}
                hasMore={isLoadMoreTransactionsAvailable}
                loader={
                  <Flex m={2} justifyContent="center" alignItems="center">
                    <FontAwesomeIcon
                      icon={faSpinner}
                      size="lg"
                      spin
                      color={COLORS_VALUES[BRANDING_GREEN]}
                    />
                  </Flex>
                }
              >
                <TransactionsList
                  transactionsList={transactionsList}
                  transactionsState={transactionsState}
                  isLoadMoreTransactionsAvailable={isLoadMoreTransactionsAvailable}
                  loadMoreTransactions={this.loadMoreTransactions}
                  isGetAllTransactionsFetching={isGetAllTransactionsFetching}
                  applyNewFilter={applyNewFilter}
                  handleToggleOpenVendorDetailsModal={this.handleToggleOpenVendorDetailsModal}
                  activeVendorDetails={activeVendorDetails}
                  isVendorDetailsModalOpen={isVendorDetailsModalOpen}
                  isGetAllTransactionsSuccess={isGetAllTransactionsSuccess}
                  isGetAllTransactionsError={isGetAllTransactionsError}
                  getAllTransactionsErrorMessage={getAllTransactionsErrorMessage}
                />
              </InfiniteScroll>
            </Box>
          </>
        )}
      </Flex>
    );
  }
}
TransactionsContainer.displayName = 'TransactionsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.transactions.applyNewFilter,
  transactionsClassification: state.statics.transactionsClassification,
  transactionsList: state.transactions.transactions,
  transactionsState: state.transactions.getAllTransactions,
  isGetAllTransactionsFetching: state.transactions.getAllTransactions.isFetching,
  isGetAllTransactionsSuccess: state.transactions.getAllTransactions.isSuccess,
  isGetAllTransactionsError: state.transactions.getAllTransactions.isFail.isError,
  getAllTransactionsErrorMessage: state.transactions.getAllTransactions.isFail.message,
  isLoadMoreTransactionsAvailable: state.transactions.hasMoreTransactions,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getAllTransactions, exportFile }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(TransactionsContainer);
