import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faPlus } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  getAllRechargeAwards,
  addRechargeAwardReset,
  deleteRechargeAward,
  toggleAddNewRechargeAwardModal,
  openEditRechargeAwardModal,
} from 'redux-modules/rechargeAwards/actions';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import RechargeAwards from 'components/RechargeAwards';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class RechargeAwardsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    rechargeAwards: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreRechargeAwards: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    isGetAllRechargeAwardsFetching: PropTypes.bool,
    isGetAllRechargeAwardsSuccess: PropTypes.bool,
    isGetAllRechargeAwardsError: PropTypes.bool,
    isDeleteRechargeAwardFetching: PropTypes.bool,
    isDeleteRechargeAwardSuccess: PropTypes.bool,
    getAllRechargeAwards: PropTypes.func,
    addRechargeAwardReset: PropTypes.func,
    deleteRechargeAward: PropTypes.func,
    toggleAddNewRechargeAwardModal: PropTypes.func,
    openEditRechargeAwardModal: PropTypes.func,
    isAddNewRechargeAwardModalOpen: PropTypes.bool,
    isEditRechargeAwardModalOpen: PropTypes.bool,
    getAllRechargeAwardsErrorMessage: PropTypes.string,
    isDeleteRechargeAwardError: PropTypes.bool,
    isDeleteRechargeAwardErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    rechargeAwards: [],
    hasMoreRechargeAwards: false,
    applyNewFilter: false,
    isGetAllRechargeAwardsFetching: false,
    isGetAllRechargeAwardsSuccess: false,
    isGetAllRechargeAwardsError: false,
    isDeleteRechargeAwardFetching: false,
    isDeleteRechargeAwardSuccess: false,
    getAllRechargeAwards: () => {},
    addRechargeAwardReset: () => {},
    deleteRechargeAward: () => {},
    toggleAddNewRechargeAwardModal: () => {},
    openEditRechargeAwardModal: () => {},
    isAddNewRechargeAwardModalOpen: false,
    isEditRechargeAwardModalOpen: false,
    getAllRechargeAwardsErrorMessage: undefined,
    isDeleteRechargeAwardError: false,
    isDeleteRechargeAwardErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('rechargeAwardsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      isConfirmModalOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      index: 0,
      RechargeAwardDetails: undefined,
    };
  }

  componentDidMount() {
    const { getAllRechargeAwards: getAllRechargeAwardsAction } = this.props;
    const { filterQueries } = this.state;

    getAllRechargeAwardsAction(filterQueries, true);
  }

  componentDidUpdate(prevProps) {
    const { isDeleteRechargeAwardSuccess } = this.props;

    if (
      isDeleteRechargeAwardSuccess &&
      isDeleteRechargeAwardSuccess !== prevProps.isDeleteRechargeAwardSuccess
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }
  }

  loadMoreRechargeAwards = () => {
    const { getAllRechargeAwards: getAllRechargeAwardsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllRechargeAwardsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleAddNewRechargeAwardModal = () => {
    const {
      toggleAddNewRechargeAwardModal: toggleAddNewRechargeAwardModalAction,
      addRechargeAwardReset: addRechargeAwardResetAction,
    } = this.props;

    addRechargeAwardResetAction();
    toggleAddNewRechargeAwardModalAction();
  };

  handleToggleEditRechargeAwardModal = (index, RechargeAwardDetails) => {
    const { openEditRechargeAwardModal: openEditRechargeAwardModalAction } = this.props;
    this.setState({ RechargeAwardDetails, index });
    openEditRechargeAwardModalAction();
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user,
      user: { permissions },
      rechargeAwards,
      hasMoreRechargeAwards,
      applyNewFilter,
      isGetAllRechargeAwardsFetching,
      isGetAllRechargeAwardsSuccess,
      isGetAllRechargeAwardsError,
      deleteRechargeAward: deleteRechargeAwardAction,
      isAddNewRechargeAwardModalOpen,
      isEditRechargeAwardModalOpen,
      isDeleteRechargeAwardFetching,
      getAllRechargeAwardsErrorMessage,
      isDeleteRechargeAwardError,
      isDeleteRechargeAwardErrorMessage,
    } = this.props;
    const {
      isConfirmModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      index,
      RechargeAwardDetails,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>مكافآت شحن الفنيين</title>
        </Helmet>
        {permissions && permissions.includes('Get All Recharge Awards') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                مكافآة شحن الفنيين
              </Text>
              <Flex m={2}>
                {permissions && permissions.includes('Add New Recharge Award') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={this.handleToggleAddNewRechargeAwardModal}
                    icon={faPlus}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={false}
                    reverse
                  >
                    <Text
                      cursor="pointer"
                      mx={2}
                      type={SUBHEADING}
                      color={COLORS_VALUES[DISABLED]}
                      fontWeight={NORMAL}
                    >
                      أضف مكافآة شحن
                    </Text>
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Box width={1}>
              <InfiniteScroll
                dataLength={rechargeAwards.length}
                next={this.loadMoreRechargeAwards}
                hasMore={hasMoreRechargeAwards}
                loader={
                  <Flex m={2} justifyContent="center" alignItems="center">
                    <FontAwesomeIcon
                      icon={faSpinner}
                      size="lg"
                      spin
                      color={COLORS_VALUES[BRANDING_GREEN]}
                    />
                  </Flex>
                }
              >
                <RechargeAwards
                  user={user}
                  rechargeAwards={rechargeAwards}
                  isConfirmModalOpen={isConfirmModalOpen}
                  isAddNewRechargeAwardModalOpen={isAddNewRechargeAwardModalOpen}
                  confirmModalHeader={confirmModalHeader}
                  confirmModalText={confirmModalText}
                  confirmModalFunction={confirmModalFunction}
                  handleToggleConfirmModal={this.handleToggleConfirmModal}
                  handleDeleteRechargeAward={deleteRechargeAwardAction}
                  isDeleteRechargeAwardFetching={isDeleteRechargeAwardFetching}
                  rechargeAwardIndex={index}
                  rechargeAwardDetails={RechargeAwardDetails}
                  isEditRechargeAwardModalOpen={isEditRechargeAwardModalOpen}
                  handleToggleEditRechargeAwardModal={this.handleToggleEditRechargeAwardModal}
                  isDeleteRechargeAwardError={isDeleteRechargeAwardError}
                  isDeleteRechargeAwardErrorMessage={isDeleteRechargeAwardErrorMessage}
                  isGetAllRechargeAwardsFetching={isGetAllRechargeAwardsFetching}
                  isGetAllRechargeAwardsSuccess={isGetAllRechargeAwardsSuccess}
                  isGetAllRechargeAwardsError={isGetAllRechargeAwardsError}
                  getAllRechargeAwardsErrorMessage={getAllRechargeAwardsErrorMessage}
                  applyNewFilter={applyNewFilter}
                />
              </InfiniteScroll>
            </Box>
          </>
        )}
      </Flex>
    );
  }
}
RechargeAwardsContainer.displayName = 'RechargeAwardsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  rechargeAwards: state.rechargeAwards.rechargeAwards,
  hasMoreRechargeAwards: state.rechargeAwards.hasMoreRechargeAwards,
  applyNewFilter: state.rechargeAwards.applyNewFilter,
  isGetAllRechargeAwardsFetching: state.rechargeAwards.getAllRechargeAwards.isFetching,
  isGetAllRechargeAwardsError: state.rechargeAwards.getAllRechargeAwards.isFail.isError,
  getAllRechargeAwardsErrorMessage: state.rechargeAwards.getAllRechargeAwards.isFail.message,
  isGetAllRechargeAwardsSuccess: state.rechargeAwards.getAllRechargeAwards.isSuccess,
  isDeleteRechargeAwardFetching: state.rechargeAwards.deleteRechargeAward.isFetching,
  isDeleteRechargeAwardSuccess: state.rechargeAwards.deleteRechargeAward.isSuccess,
  isDeleteRechargeAwardError: state.rechargeAwards.deleteRechargeAward.isFail.isError,
  isDeleteRechargeAwardErrorMessage: state.rechargeAwards.deleteRechargeAward.isFail.message,
  isAddNewRechargeAwardModalOpen: state.rechargeAwards.addNewRechargeAwardModal.isOpen,
  isEditRechargeAwardModalOpen: state.rechargeAwards.editRechargeAwardModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllRechargeAwards,
      addRechargeAwardReset,
      deleteRechargeAward,
      toggleAddNewRechargeAwardModal,
      openEditRechargeAwardModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(RechargeAwardsContainer);
