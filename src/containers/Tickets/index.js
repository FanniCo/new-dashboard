import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFilter, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { fetchTransactionsClassification } from 'redux-modules/statics/actions';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { getAllTickets, getTicketsCountInEachStatus } from 'redux-modules/tickets/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import Tickets from 'components/Tickets';
import styled from 'styled-components';
import { borderBottom, color } from 'styled-system';
import Card from 'components/Card';
import EmptyState from 'components/EmptyState';
import { TICKETS_STATUS } from 'utils/constants';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, GREY_DARK, WHITE, GREY_LIGHT } = COLORS;
const { SUPER_TITLE, SUBHEADING, HEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const TabsContainer = styled(Flex)`
  ${borderBottom};
`;

const Tab = styled(Flex)`
  cursor: pointer;
  width: max-content;
  ${color};
  ${borderBottom};
`;

class TicketsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    tickets: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreTickets: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    configs: PropTypes.shape({}),
    isGetAllTicketsFetching: PropTypes.bool,
    isGetAllTicketsSuccess: PropTypes.bool,
    isGetAllTicketsError: PropTypes.bool,
    getAllTickets: PropTypes.func,
    isGetTransactionsClassificationSuccess: PropTypes.bool,
    fetchTransactionsClassification: PropTypes.func,
    isFetchConfigSuccess: PropTypes.bool,
    fetchConfigs: PropTypes.func,
    getTicketsCountInEachStatus: PropTypes.func,
    isGetTicketsCountInEachStatusSuccess: PropTypes.bool,
    ticketsCountInEachStatus: PropTypes.shape({}),
    getAllTicketsErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    tickets: undefined,
    hasMoreTickets: true,
    applyNewFilter: false,
    configs: undefined,
    isGetAllTicketsFetching: false,
    isGetAllTicketsSuccess: false,
    isGetAllTicketsError: false,
    getAllTickets: () => {},
    isGetTransactionsClassificationSuccess: false,
    fetchTransactionsClassification: () => {},
    isFetchConfigSuccess: false,
    fetchConfigs: () => {},
    getTicketsCountInEachStatus: () => {},
    isGetTicketsCountInEachStatusSuccess: false,
    ticketsCountInEachStatus: undefined,
    getAllTicketsErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem(`tickets${0}FilterState`));
    let filterQueries = {
      skip: 0,
      limit: 20,
      status: TICKETS_STATUS.open.code,
    };
    const tabs = [
      { name: 'جارية', nameEn: 'open', searchCriteria: { status: TICKETS_STATUS.open.code } },
      {
        name: 'جاري الحل',
        nameEn: 'solving',
        searchCriteria: { status: TICKETS_STATUS.solving.code },
      },
      { name: 'تم الحل', nameEn: 'solved', searchCriteria: { status: TICKETS_STATUS.solved.code } },
    ];
    let activeTab = 0;

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
      tabs.forEach((tab, index) => {
        if (tab.searchCriteria.status === filterQueries.status) {
          activeTab = index;
        }
      });
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      tabs,
      activeTab,
      cachedFilterPage: `tickets${0}`,
    };
  }

  componentDidMount() {
    const {
      getAllTickets: getAllTicketsAction,
      isGetTransactionsClassificationSuccess,
      fetchTransactionsClassification: fetchTransactionsClassificationAction,
      isFetchConfigSuccess,
      fetchConfigs: fetchConfigsAction,
      getTicketsCountInEachStatus: getTicketsCountInEachStatusAction,
    } = this.props;
    const { filterQueries } = this.state;
    getAllTicketsAction(filterQueries, true);
    getTicketsCountInEachStatusAction(true);

    if (!isGetTransactionsClassificationSuccess) {
      fetchTransactionsClassificationAction();
    }

    if (!isFetchConfigSuccess) {
      fetchConfigsAction();
    }
  }

  /**
   * Creates lazy loading tickets block
   */
  getLoadingTickets = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of tickets blocks
   */
  createLoadingTicketsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingTickets(counter));
    }
    return list;
  };

  loadMoreTickets = () => {
    const { getAllTickets: getAllTicketsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllTicketsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;
    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleTabChange = value => {
    const { activeTab, tabs } = this.state;
    const { getAllTickets: getAllTicketsAction } = this.props;
    const filters = JSON.parse(sessionStorage.getItem(`tickets${value}FilterState`)) || {};
    const newFilterQueries = {
      ...filters.filterQueries,
      status: tabs[value].searchCriteria.status,
      skip: 0,
      limit: 20,
    };
    if (activeTab !== value) {
      this.setState(
        {
          activeTab: value,
          filterQueries: { ...filters.filterQueries, ...newFilterQueries },
          cachedFilterPage: `tickets${value}`,
        },
        () => {
          sessionStorage.setItem(
            `tickets${value}FilterState`,
            JSON.stringify({ ...filters, filterQueries: newFilterQueries }),
          );
          getAllTicketsAction({ ...filters.filterQueries, ...newFilterQueries }, true);
        },
      );
    }
  };

  render() {
    const {
      user: { permissions },
      tickets,
      hasMoreTickets,
      applyNewFilter,
      isGetAllTicketsFetching,
      isGetAllTicketsSuccess,
      isGetAllTicketsError,
      getAllTickets: getAllTicketsAction,
      configs,
      isGetTicketsCountInEachStatusSuccess,
      ticketsCountInEachStatus,
      getAllTicketsErrorMessage,
    } = this.props;
    const { isFilterOpen, filterQueries, tabs, activeTab, cachedFilterPage } = this.state;
    const complainsList = configs && configs.enums.complaintsList.map(complain => ({
      value: complain.code,
      label: complain.name,
    }));

    let ticketsRenderer;

    if (
      (isGetAllTicketsFetching && (!tickets || applyNewFilter)) ||
      (!isGetAllTicketsFetching && !isGetAllTicketsSuccess && !isGetAllTicketsError)
    ) {
      ticketsRenderer = this.createLoadingTicketsList();
    } else if (isGetAllTicketsError) {
      ticketsRenderer = (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text={getAllTicketsErrorMessage}
            />
          </Card>
        </Flex>
      );
    } else if (!tickets.length) {
      ticketsRenderer = (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text="لا يوجد سجلات"
            />
          </Card>
        </Flex>
      );
    } else {
      ticketsRenderer = (
        <InfiniteScroll
          dataLength={tickets.length}
          next={this.loadMoreTickets}
          hasMore={hasMoreTickets}
          loader={
            <Flex m={2} justifyContent="center" alignItems="center">
              <FontAwesomeIcon
                icon={faSpinner}
                size="lg"
                spin
                color={COLORS_VALUES[BRANDING_GREEN]}
              />
            </Flex>
          }
        >
          <Tickets tickets={tickets} />
        </InfiniteScroll>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>الشكاوي</title>
        </Helmet>
        {permissions && permissions.includes('Get All Tickets') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الشكاوي
              </Text>
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={this.handleToggleFilterModal}
                icon={faFilter}
                iconWidth="sm"
                xMargin={3}
                isLoading={false}
              >
                <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  فرز
                </Text>
              </Button>
            </StickyContainer>
            <Filter
              cachedFilterPage={cachedFilterPage}
              filterSections={[
                'ticketNumber',
                'requestNumber',
                'clientUsername',
                'workerUsername',
                'complainName',
                'field',
                'city',
              ]}
              requestNumberQueryKey="requestPrettyId"
              clientUsernameQueryKey="customerUsername"
              workerUsernameQueryKey="workerUsername"
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllTicketsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
              complainsList={complainsList}
            />
            <Box width={1}>
              <Box width={[1, 1, 1, 1, 1]}>
                <TabsContainer flexDirection="row-reverse" alignItems="center">
                  {tabs.map((tab, index) => (
                    <Tab
                      key={tab.name}
                      flexDirection="row-reverse"
                      alignItems="center"
                      px={5}
                      py={3}
                      borderBottom={
                        activeTab === index ? `2px solid ${COLORS_VALUES[BRANDING_GREEN]}` : 'none'
                      }
                      onClick={() => this.handleTabChange(index, tab.drawMap)}
                    >
                      <Text
                        cursor="pointer"
                        textAlign="center"
                        color={
                          activeTab === index
                            ? COLORS_VALUES[BRANDING_GREEN]
                            : COLORS_VALUES[DISABLED]
                        }
                        type={HEADING}
                        fontWeight={NORMAL}
                        mx={3}
                      >
                        {tab.name}
                      </Text>
                      {isGetTicketsCountInEachStatusSuccess && (
                        <Box
                          display="inline-block"
                          p="1"
                          color="white"
                          bg="primary"
                          borderRadius="9999"
                          border
                        >
                          {ticketsCountInEachStatus[tab.nameEn]
                            ? ticketsCountInEachStatus[tab.nameEn].count
                            : 0}
                        </Box>
                      )}
                    </Tab>
                  ))}
                </TabsContainer>
                <Card
                  minHeight={500}
                  ml={[0, 0, 0, 0, 0]}
                  mb={3}
                  flexDirection="column"
                  bgColor={COLORS_VALUES[GREY_DARK]}
                >
                  {activeTab === 0 && <>{ticketsRenderer}</>}
                  {activeTab === 1 && <>{ticketsRenderer}</>}
                  {activeTab === 2 && <>{ticketsRenderer}</>}
                </Card>
              </Box>
            </Box>
          </>
        )}
      </Flex>
    );
  }
}
TicketsContainer.displayName = 'TicketsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  tickets: state.tickets.tickets,
  hasMoreTickets: state.tickets.hasMoreTickets,
  applyNewFilter: state.tickets.applyNewFilter,
  configs: state.configs.configs,
  isGetTransactionsClassificationSuccess: state.statics.getTransactionsClassification.isSuccess,
  isGetAllTicketsFetching: state.tickets.getAllTickets.isFetching,
  isGetAllTicketsSuccess: state.tickets.getAllTickets.isSuccess,
  isGetAllTicketsError: state.tickets.getAllTickets.isFail.isError,
  getAllTicketsErrorMessage: state.tickets.getAllTickets.isFail.message,

  isGetTicketsCountInEachStatusFetching: state.tickets.getTicketsCountInEachStatus.isFetching,
  isGetTicketsCountInEachStatusSuccess: state.tickets.getTicketsCountInEachStatus.isSuccess,
  isGetTicketsCountInEachStatusError: state.tickets.getTicketsCountInEachStatus.isFail.isError,
  ticketsCountInEachStatus: state.tickets.ticketsCountInEachStatus,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { fetchTransactionsClassification, fetchConfigs, getAllTickets, getTicketsCountInEachStatus },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(TicketsContainer);
