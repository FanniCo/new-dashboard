import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFile, faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllMessages,
  toggleAddNewMessageModal,
  deleteMessage,
  openConfirmModal,
  openEditMessageModal,
} from 'redux-modules/messages/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import MessagesList from 'components/Messages';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class MessagesContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    messagesList: PropTypes.arrayOf(PropTypes.shape({})),
    transactionsState: PropTypes.shape({}),
    isLoadMoreMessagesAvailable: PropTypes.bool.isRequired,
    getAllMessages: PropTypes.func,
    isGetAllMessagesFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewMessageModal: PropTypes.func,
    isAddNewMessageModalOpen: PropTypes.bool,
    deleteMessage: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeleteMessageFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditMessageModal: PropTypes.func,
    isEditMessageModalOpen: PropTypes.bool,
    isGetAllMessagesSuccess: PropTypes.bool,
    isGetAllMessagesError: PropTypes.bool,
    isGetAllMessagesErrorMessage: PropTypes.string,
    isDeleteMessageError: PropTypes.bool,
    isDeleteMessageErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    messagesList: undefined,
    transactionsState: undefined,
    getAllMessages: () => {},
    applyNewFilter: false,
    isGetAllMessagesFetching: false,
    toggleAddNewMessageModal: () => {},
    isAddNewMessageModalOpen: false,
    deleteMessage: () => {},
    openConfirmModal: () => {},
    isDeleteMessageFetching: false,
    isConfirmModalOpen: false,
    openEditMessageModal: () => {},
    isEditMessageModalOpen: false,
    isGetAllMessagesSuccess: false,
    isGetAllMessagesError: false,
    isGetAllMessagesErrorMessage: false,
    isDeleteMessageError: false,
    isDeleteMessageErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('messagesFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      messageId: '',
      index: 0,
      messageDetails: {},
    };
  }

  componentDidMount() {
    const { getAllMessages: getAllMessagesAction } = this.props;
    const { filterQueries } = this.state;

    getAllMessagesAction(filterQueries, true);
  }

  loadMoreMessages = () => {
    const { getAllMessages: getAllMessagesAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllMessagesAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewMessageModal = () => {
    const { toggleAddNewMessageModal: toggleAddNewMessageModalAction } = this.props;

    toggleAddNewMessageModalAction();
  };

  handleToggleConfirmModal = (messageId, index) => {
    const { openConfirmModal: openConfirmModalAction } = this.props;
    this.setState({ messageId, index });
    openConfirmModalAction();
  };

  handleToggleEditWorkerModal = (index, messageDetails) => {
    const { openEditMessageModal: openEditMessageModalAction } = this.props;
    this.setState({ messageDetails, index });
    openEditMessageModalAction();
  };

  getLoadingMessages = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingMessagesList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingMessages(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      user: { permissions },
      messagesList,
      transactionsState,
      isLoadMoreMessagesAvailable,
      getAllMessages: getAllMessagesAction,
      isGetAllMessagesFetching,
      applyNewFilter,
      isAddNewMessageModalOpen,
      deleteMessage: deleteMessageAction,
      isDeleteMessageFetching,
      isConfirmModalOpen,
      isEditMessageModalOpen,
      isGetAllMessagesSuccess,
      isGetAllMessagesError,
      isGetAllMessagesErrorMessage,
      isDeleteMessageError,
      isDeleteMessageErrorMessage,
    } = this.props;
    const { isFilterOpen, filterQueries, messageId, index, messageDetails } = this.state;

    let messagesRenderer;

    if (
      (isGetAllMessagesFetching && (!messagesList || applyNewFilter)) ||
      (!isGetAllMessagesFetching && !isGetAllMessagesSuccess && !isGetAllMessagesError)
    ) {
      messagesRenderer = this.createLoadingMessagesList();
    } else if (isGetAllMessagesError) {
      messagesRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllMessagesErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!messagesList.length) {
      messagesRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      messagesRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={messagesList.length}
            next={this.loadMoreMessages}
            hasMore={isLoadMoreMessagesAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <MessagesList
              isAddNewMessageModalOpen={isAddNewMessageModalOpen}
              messagesList={messagesList}
              transactionsState={transactionsState}
              isLoadMoreMessagesAvailable={isLoadMoreMessagesAvailable}
              loadMoreMessages={this.loadMoreMessages}
              isGetAllMessagesFetching={isGetAllMessagesFetching}
              applyNewFilter={applyNewFilter}
              user={user}
              handleDeleteMessage={() => {
                deleteMessageAction(messageId, index);
              }}
              handleToggleConfirmModal={this.handleToggleConfirmModal}
              isConfirmModalOpen={isConfirmModalOpen}
              isDeleteMessageFetching={isDeleteMessageFetching}
              handleToggleEditWorkerModal={this.handleToggleEditWorkerModal}
              isEditMessageModalOpen={isEditMessageModalOpen}
              messageDetails={messageDetails}
              messageIndex={index}
              isDeleteMessageError={isDeleteMessageError}
              isDeleteMessageErrorMessage={isDeleteMessageErrorMessage}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>الرسائل</title>
        </Helmet>
        {permissions && permissions.includes('Get All Users') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الرسائل
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Send Workers Messages') && (
                  <>
                    <Button
                      ml={1}
                      mr={1}
                      color={PRIMARY}
                      onClick={this.handleToggleAddNewMessageModal}
                      icon={faPlus}
                      iconWidth="sm"
                      xMargin={3}
                      isLoading={false}
                      reverse
                    >
                      <Text
                        cursor="pointer"
                        mx={2}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        أضف رسالة جديده
                      </Text>
                    </Button>
                  </>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="messages"
              filterSections={['field', 'city']}
              creditsTitle="القيمة"
              fieldsQueryKey="fields"
              citiesQueryKey="cities"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllMessagesAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {messagesRenderer}
          </>
        )}
      </Flex>
    );
  }
}
MessagesContainer.displayName = 'MessagesContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.messages.applyNewFilter,
  messagesList: state.messages.messages,
  transactionsState: state.messages.getAllMessages,
  isGetAllMessagesFetching: state.messages.getAllMessages.isFetching,
  isGetAllMessagesSuccess: state.messages.getAllMessages.isSuccess,
  isGetAllMessagesError: state.messages.getAllMessages.isFail.isError,
  isGetAllMessagesErrorMessage: state.messages.getAllMessages.isFail.message,
  isLoadMoreMessagesAvailable: state.messages.hasMoreMessages,
  isAddNewMessageModalOpen: state.messages.addNewMessageModal.isOpen,
  isDeleteMessageFetching: state.messages.deleteMessage.isFetching,
  isDeleteMessageError: state.messages.deleteMessage.isFail.isError,
  isDeleteMessageErrorMessage: state.messages.deleteMessage.isFail.message,
  isConfirmModalOpen: state.messages.confirmModal.isOpen,
  isEditMessageModalOpen: state.messages.editMessageModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllMessages,
      toggleAddNewMessageModal,
      deleteMessage,
      openConfirmModal,
      openEditMessageModal,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(MessagesContainer);
