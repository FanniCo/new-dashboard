import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFilter, faPlus, faDownload, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  getAllWorkers,
  addWorkerReset,
  editWorkerReset,
  addCreditReset,
  activateWorker,
  featureWorker,
  makeWorkerElite,
  // deleteWorker,
  toggleAddNewWorkerModal,
  toggleEditWorkerModal,
  toggleAddCreditModal,
  toggleSuspendWorkerModal,
  suspendWorker,
  toggleConfirmModal,
} from 'redux-modules/workers/actions';
import { getUsersForEachTypeCount } from 'redux-modules/dashboard/actions';
import { getAllVendors } from 'redux-modules/vendors/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import Workers from 'components/Workers';
import { Urls } from 'utils/api';
import { exportFile, getAllAdmins } from 'redux-modules/user/actions';
import Card from 'components/Card';
import EmptyState from 'components/EmptyState';
import InputField from 'components/InputField';
import { USER_ROLES } from 'utils/constants';
import Caution from 'components/Caution';

const {
  GREY_MEDIUM,
  DISABLED,
  BRANDING_GREEN,
  PRIMARY,
  WHITE,
  GREY_LIGHT,
  GREY_DARK,
  LIGHT_RED,
  ERROR,
} = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class WorkersContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    isGetAllWorkersFetching: PropTypes.bool,
    isGetAllWorkersSuccess: PropTypes.bool,
    isGetAllWorkersError: PropTypes.bool,
    isActivateWorkerFetching: PropTypes.bool,
    isActivateWorkerError: PropTypes.bool,
    isFeatureWorkerFetching: PropTypes.bool,
    isFeatureWorkerError: PropTypes.bool,
    featureWorkerErrorMessage: PropTypes.string,
    isMakeWorkerEliteFetching: PropTypes.bool,
    isMakeWorkerEliteError: PropTypes.bool,
    // isDeleteWorkerFetching: PropTypes.bool,
    getAllWorkers: PropTypes.func,
    addWorkerReset: PropTypes.func,
    editWorkerReset: PropTypes.func,
    addCreditReset: PropTypes.func,
    activateWorker: PropTypes.func,
    // deleteWorker: PropTypes.func,
    featureWorker: PropTypes.func,
    makeWorkerElite: PropTypes.func,
    toggleAddNewWorkerModal: PropTypes.func,
    toggleEditWorkerModal: PropTypes.func,
    toggleAddCreditModal: PropTypes.func,
    workers: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreWorkers: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    isAddNewWorkerModalOpen: PropTypes.bool,
    isEditWorkerModalOpen: PropTypes.bool,
    isAddCreditModalOpen: PropTypes.bool,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    exportFile: PropTypes.func,
    toggleSuspendWorkerModal: PropTypes.func,
    isSuspendWorkerModalOpen: PropTypes.bool,
    suspendWorker: PropTypes.func,
    isSuspendWorkerFetching: PropTypes.bool,
    isSuspendWorkerError: PropTypes.bool,
    toggleConfirmModal: PropTypes.func,
    isConfirmModalOpened: PropTypes.bool,
    suspendWorkerErrorMessage: PropTypes.string,
    getAllWorkersErrorMessage: PropTypes.string,
    getAllVendors: PropTypes.func,
    vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
    getAllAdmins: PropTypes.func,
    getUsersForEachTypeCount: PropTypes.func,

    isGetUsersCountsForEachTypeSuccess: PropTypes.bool,
    isGetUsersCountsForEachTypeError: PropTypes.bool,
    getUsersCountsForEachTypeErrorMessage: PropTypes.string,
    UsersCountForEachType: PropTypes.shape({}),
  };

  static defaultProps = {
    user: undefined,
    workers: undefined,
    hasMoreWorkers: true,
    applyNewFilter: false,
    isGetAllWorkersFetching: false,
    isGetAllWorkersSuccess: false,
    isGetAllWorkersError: false,
    isActivateWorkerFetching: false,
    isActivateWorkerError: false,
    isFeatureWorkerFetching: false,
    isFeatureWorkerError: false,
    featureWorkerErrorMessage: undefined,
    isMakeWorkerEliteFetching: false,
    isMakeWorkerEliteError: false,
    // isDeleteWorkerFetching: false,
    getAllWorkers: () => {},
    addWorkerReset: () => {},
    editWorkerReset: () => {},
    addCreditReset: () => {},
    activateWorker: () => {},
    // deleteWorker: () => {},
    featureWorker: () => {},
    makeWorkerElite: () => {},
    toggleAddNewWorkerModal: () => {},
    toggleEditWorkerModal: () => {},
    toggleAddCreditModal: () => {},
    isAddNewWorkerModalOpen: false,
    isEditWorkerModalOpen: false,
    isAddCreditModalOpen: false,
    isExportFileFetching: false,
    isExportFileSuccess: false,
    exportFile: () => {},
    toggleSuspendWorkerModal: () => {},
    isSuspendWorkerModalOpen: false,
    suspendWorker: PropTypes.func,
    isSuspendWorkerFetching: false,
    isSuspendWorkerError: false,
    toggleConfirmModal: () => {},
    isConfirmModalOpened: false,
    suspendWorkerErrorMessage: undefined,
    getAllWorkersErrorMessage: undefined,
    getAllVendors: () => {},
    vendorsList: undefined,
    getAllAdmins: () => {},
    getUsersForEachTypeCount: () => {},
    isGetUsersCountsForEachTypeSuccess: false,
    isGetUsersCountsForEachTypeError: false,
    getUsersCountsForEachTypeErrorMessage: undefined,
    UsersCountForEachType: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      isOpsCommentsModalOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      workerIndex: 0,
      workerDetails: undefined,
    };
  }

  componentDidMount() {
    const {
      getAllWorkers: getAllWorkersAction,
      getAllVendors: getAllVendorsAction,
      getAllAdmins: getAllAdminsAction,
      getUsersForEachTypeCount: getUsersForEachTypeCountAction,
    } = this.props;
    const { filterQueries } = this.state;

    getAllWorkersAction(filterQueries, true);
    getUsersForEachTypeCountAction({ role: 'worker', ...filterQueries }, true);
    getAllVendorsAction({ skip: 0, limit: 5000 }, true);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  /**
   * Creates lazy loading request block
   */
  getLoadingWorkers = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of workers blocks
   */
  createLoadingWorkersList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingWorkers(counter));
    }
    return list;
  };

  loadMoreWorkers = () => {
    const { getAllWorkers: getAllWorkersAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllWorkersAction(filterQueriesNextState);
      },
    );
  };

  handleToggleOpsCommentsModal = index => {
    const { isOpsCommentsModalOpen } = this.state;

    this.setState({ workerIndex: index }, () => {
      this.setState({
        isOpsCommentsModalOpen: !isOpsCommentsModalOpen,
      });
    });
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { toggleConfirmModal: toggleConfirmModalAction, isConfirmModalOpened } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpened ? header : '',
        confirmModalText: !isConfirmModalOpened ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpened ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpened);
      },
    );
  };

  handleToggleAddNewWorkerModal = () => {
    const {
      toggleAddNewWorkerModal: toggleAddNewWorkerModalAction,
      addWorkerReset: addWorkerResetAction,
    } = this.props;

    addWorkerResetAction();
    toggleAddNewWorkerModalAction();
  };

  handleToggleEditWorkerModal = (workerIndex, workerDetails) => {
    const {
      toggleEditWorkerModal: toggleEditWorkerModalAction,
      editWorkerReset: editWorkerResetAction,
    } = this.props;

    this.setState(
      {
        workerIndex,
        workerDetails,
      },
      () => {
        editWorkerResetAction();
        toggleEditWorkerModalAction();
      },
    );
  };

  handleToggleSuspendWorkerModal = (workerIndex, workerDetails) => {
    const { toggleSuspendWorkerModal: toggleSuspendWorkerModalAction } = this.props;

    this.setState(
      {
        workerIndex,
        workerDetails,
      },
      () => {
        toggleSuspendWorkerModalAction();
      },
    );
  };

  handleToggleAddCreditModal = (workerIndex, workerDetails) => {
    const {
      toggleAddCreditModal: toggleAddCreditModalAction,
      addCreditReset: addCreditResetAction,
    } = this.props;

    this.setState(
      {
        workerIndex,
        workerDetails,
      },
      () => {
        addCreditResetAction();
        toggleAddCreditModalAction();
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } }, () => {
      const { filterQueries: newFilterQueries } = this.state;
      const { getUsersForEachTypeCount: getUsersForEachTypeCountAction } = this.props;

      getUsersForEachTypeCountAction({ role: 'worker', ...newFilterQueries }, true);
    });
  };

  handleExportFile = () => {
    let {
      workers: { exportWorkers },
    } = Urls;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0 && !exportWorkers.includes('?')) {
          exportWorkers = `${exportWorkers}?${key}=${cachedFilterQueries[key]}`;
        } else {
          exportWorkers = `${exportWorkers}&${key}=${cachedFilterQueries[key]}`;
        }
        return true;
      });
    }
    exportFileAction(exportWorkers);
  };

  render() {
    const {
      user,
      user: { permissions },
      workers,
      hasMoreWorkers,
      applyNewFilter,
      isGetAllWorkersFetching,
      isGetAllWorkersSuccess,
      isGetAllWorkersError,
      getAllWorkers: getAllWorkersAction,
      activateWorker: activateWorkerAction,
      featureWorker: featureWorkerAction,
      makeWorkerElite: makeWorkerEliteAction,
      // deleteWorker: deleteWorkerAction,
      isAddNewWorkerModalOpen,
      isEditWorkerModalOpen,
      isAddCreditModalOpen,
      isActivateWorkerFetching,
      isActivateWorkerError,
      isFeatureWorkerFetching,
      isFeatureWorkerError,
      featureWorkerErrorMessage,
      isMakeWorkerEliteFetching,
      isMakeWorkerEliteError,
      // isDeleteWorkerFetching,
      isExportFileFetching,
      isExportFileSuccess,
      isSuspendWorkerModalOpen,
      suspendWorker: suspendWorkerAction,
      isSuspendWorkerFetching,
      isSuspendWorkerError,
      isConfirmModalOpened,
      suspendWorkerErrorMessage,
      getAllWorkersErrorMessage,
      vendorsList,
      UsersCountForEachType,
      isGetUsersCountsForEachTypeSuccess,
      isGetUsersCountsForEachTypeError,
      getUsersCountsForEachTypeErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      isOpsCommentsModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      filterQueries,
      workerIndex,
      workerDetails,
    } = this.state;

    let workersRenderer;

    if (
      (isGetAllWorkersFetching && (!workers || applyNewFilter)) ||
      (!isGetAllWorkersFetching && !isGetAllWorkersSuccess && !isGetAllWorkersError)
    ) {
      workersRenderer = this.createLoadingWorkersList();
    } else if (isGetAllWorkersError) {
      workersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllWorkersErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!workers.length) {
      workersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      workersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <InfiniteScroll
                dataLength={workers.length}
                next={this.loadMoreWorkers}
                hasMore={hasMoreWorkers}
                loader={
                  <Flex m={2} justifyContent="center" alignItems="center">
                    <FontAwesomeIcon
                      icon={faSpinner}
                      size="lg"
                      spin
                      color={COLORS_VALUES[BRANDING_GREEN]}
                    />
                  </Flex>
                }
              >
                <Workers
                  user={user}
                  workers={workers}
                  workerIndex={workerIndex}
                  workerDetails={workerDetails}
                  isOpsCommentsModalOpen={isOpsCommentsModalOpen}
                  isAddNewWorkerModalOpen={isAddNewWorkerModalOpen}
                  isEditWorkerModalOpen={isEditWorkerModalOpen}
                  isAddCreditModalOpen={isAddCreditModalOpen}
                  isConfirmModalOpen={isConfirmModalOpened}
                  confirmModalHeader={confirmModalHeader}
                  confirmModalText={confirmModalText}
                  confirmModalFunction={confirmModalFunction}
                  handleToggleOpsCommentsModal={this.handleToggleOpsCommentsModal}
                  handleToggleConfirmModal={this.handleToggleConfirmModal}
                  handleToggleEditWorkerModal={this.handleToggleEditWorkerModal}
                  handleToggleAddCreditModal={this.handleToggleAddCreditModal}
                  handleActivateWorker={activateWorkerAction}
                  handleFeatureWorker={featureWorkerAction}
                  handleMakeWorkerElite={makeWorkerEliteAction}
                  // handleDeleteWorker={deleteWorkerAction}
                  isActivateWorkerFetching={isActivateWorkerFetching}
                  isActivateWorkerError={isActivateWorkerError}
                  isFeatureWorkerFetching={isFeatureWorkerFetching}
                  isFeatureWorkerError={isFeatureWorkerError}
                  featureWorkerErrorMessage={featureWorkerErrorMessage}
                  isMakeWorkerEliteFetching={isMakeWorkerEliteFetching}
                  isMakeWorkerEliteError={isMakeWorkerEliteError}
                  // isDeleteWorkerFetching={isDeleteWorkerFetching}
                  handleToggleSuspendWorkerModal={this.handleToggleSuspendWorkerModal}
                  isSuspendWorkerModalOpen={isSuspendWorkerModalOpen}
                  handleSuspendWorker={suspendWorkerAction}
                  isSuspendWorkerFetching={isSuspendWorkerFetching}
                  isSuspendWorkerError={isSuspendWorkerError}
                  suspendWorkerErrorMessage={suspendWorkerErrorMessage}
                  vendorsList={vendorsList}
                />
              </InfiniteScroll>
            </Card>
          </Box>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>الفنيين</title>
        </Helmet>
        {permissions && permissions.includes('Get All Workers') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الفنيين
              </Text>
              <Flex flexDirection="row-reverse" px={3}>
                <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} my={2}>
                    مجموع الفنيين
                  </Text>
                  {!isGetUsersCountsForEachTypeError && isGetUsersCountsForEachTypeSuccess && (
                    <InputField
                      type="text"
                      width={1}
                      placeholder="المجموع"
                      value={UsersCountForEachType[USER_ROLES.WORKER].count || 0 }
                      mb={2}
                      disabled
                      color={COLORS_VALUES[WHITE]}
                    />
                  )}
                  {isGetUsersCountsForEachTypeError && (
                    <Flex flexDirection="row-reverse" px={3}>
                      <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                        {getUsersCountsForEachTypeErrorMessage}
                      </Caution>
                    </Flex>
                  )}
                </Flex>
              </Flex>
              <Flex flexDirection="row-reverse" px={1}>
                <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} my={2}>
                    مجموع الموقفين
                  </Text>
                  {!isGetUsersCountsForEachTypeError && isGetUsersCountsForEachTypeSuccess && (
                    <InputField
                      type="text"
                      width={1}
                      placeholder="المجموع"
                      value={UsersCountForEachType.suspendedWorkersCount || 0}
                      mb={2}
                      disabled
                      color={COLORS_VALUES[WHITE]}
                    />
                  )}
                  {isGetUsersCountsForEachTypeError && (
                    <Flex flexDirection="row-reverse" px={3}>
                      <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                        {getUsersCountsForEachTypeErrorMessage}
                      </Caution>
                    </Flex>
                  )}
                </Flex>
              </Flex>
              <Flex flexDirection="row-reverse" px={1}>
                <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} my={2}>
                    مجموع النشطين
                  </Text>
                  {!isGetUsersCountsForEachTypeError && isGetUsersCountsForEachTypeSuccess && (
                    <InputField
                      type="text"
                      width={1}
                      placeholder="المجموع"
                      value={UsersCountForEachType.activeWorkersCount || 0}
                      mb={2}
                      disabled
                      color={COLORS_VALUES[WHITE]}
                    />
                  )}
                  {isGetUsersCountsForEachTypeError && (
                    <Flex flexDirection="row-reverse" px={3}>
                      <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                        {getUsersCountsForEachTypeErrorMessage}
                      </Caution>
                    </Flex>
                  )}
                </Flex>
              </Flex>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Download Workers Report') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={() => this.handleExportFile()}
                    icon={faDownload}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={isExportFileFetching && !isExportFileSuccess}
                    disable={isExportFileFetching && !isExportFileSuccess}
                    reverse
                  >
                    تصدير الي ملف
                  </Button>
                )}
                {permissions && permissions.includes('Add New Worker') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={this.handleToggleAddNewWorkerModal}
                    icon={faPlus}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={false}
                    reverse
                  >
                    <Text
                      cursor="pointer"
                      mx={2}
                      type={SUBHEADING}
                      color={COLORS_VALUES[DISABLED]}
                      fontWeight={NORMAL}
                    >
                      أضف فني جديد
                    </Text>
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="workers"
              filterSections={[
                'workerUsername',
                'governmentId',
                'field',
                'city',
                'active',
                'suspended',
                'featured',
                'elite',
                'credits',
                'dates',
                'vendors',
                'requestsStatus',
                'requestsCount',
                'isForWorkerHasResidenceInfo',
                'isApprovedWorkerHasResidenceInfo',
              ]}
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllWorkersAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
              vendorsList={vendorsList}
            />
            {workersRenderer}
          </>
        )}
      </Flex>
    );
  }
}
WorkersContainer.displayName = 'WorkersContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  workers: state.workers.workers,
  hasMoreWorkers: state.workers.hasMoreWorkers,
  applyNewFilter: state.workers.applyNewFilter,
  isGetAllWorkersFetching: state.workers.getAllWorkers.isFetching,
  isGetAllWorkersError: state.workers.getAllWorkers.isFail.isError,
  getAllWorkersErrorMessage: state.workers.getAllWorkers.isFail.message,
  isGetAllWorkersSuccess: state.workers.getAllWorkers.isSuccess,
  isActivateWorkerFetching: state.workers.activateWorker.isFetching,
  isActivateWorkerError: state.workers.activateWorker.isFail.isError,
  isActivateWorkerSuccess: state.workers.activateWorker.isSuccess,
  isFeatureWorkerFetching: state.workers.featureWorker.isFetching,
  isFeatureWorkerError: state.workers.featureWorker.isFail.isError,
  featureWorkerErrorMessage: state.workers.featureWorker.isFail.message,
  isFeatureWorkerSuccess: state.workers.featureWorker.isSuccess,
  isMakeWorkerEliteFetching: state.workers.makeWorkerElite.isFetching,
  isMakeWorkerEliteError: state.workers.makeWorkerElite.isFail.isError,
  isMakeWorkerEliteSuccess: state.workers.makeWorkerElite.isSuccess,
  // isDeleteWorkerFetching: state.workers.deleteWorker.isFetching,
  // isDeleteWorkerSuccess: state.workers.deleteWorker.isSuccess,
  isAddNewWorkerModalOpen: state.workers.addNewWorkerModal.isOpen,
  isEditWorkerModalOpen: state.workers.editWorkerModal.isOpen,
  isAddCreditModalOpen: state.workers.addCreditModal.isOpen,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
  isSuspendWorkerModalOpen: state.workers.suspendWorkerModal.isOpen,
  isSuspendWorkerFetching: state.workers.suspendWorker.isFetching,
  isSuspendWorkerSuccess: state.workers.suspendWorker.isSuccess,
  isSuspendWorkerError: state.workers.suspendWorker.isFail.isError,
  suspendWorkerErrorMessage: state.workers.suspendWorker.isFail.message,
  isConfirmModalOpened: state.workers.confirmModal.isOpen,
  vendorsList: state.vendors.vendors,

  isGetUsersCountsForEachTypeFetching: state.dashboard.getUsersForEachTypeCount.isFetching,
  isGetUsersCountsForEachTypeSuccess: state.dashboard.getUsersForEachTypeCount.isSuccess,
  isGetUsersCountsForEachTypeError: state.dashboard.getUsersForEachTypeCount.isFail.message,
  getUsersCountsForEachTypeErrorMessage: state.dashboard.getUsersForEachTypeCount.isFail.message,
  UsersCountForEachType: state.dashboard.UsersForEachTypeCount,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllWorkers,
      addWorkerReset,
      editWorkerReset,
      addCreditReset,
      activateWorker,
      featureWorker,
      makeWorkerElite,
      // deleteWorker,
      toggleAddNewWorkerModal,
      toggleEditWorkerModal,
      toggleAddCreditModal,
      exportFile,
      toggleSuspendWorkerModal,
      suspendWorker,
      toggleConfirmModal,
      getAllVendors,
      getAllAdmins,
      getUsersForEachTypeCount,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(WorkersContainer);
