import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import {
  addVoucherReset,
  deleteVoucher,
  getAllVouchers,
  toggleAddVoucherModal,
  toggleEditVoucherModal,
  updateVoucher,
} from 'redux-modules/vouchers/actions';
import Vouchers from 'components/Vouchers';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class VouchersContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    vouchersList: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreVouchers: PropTypes.bool,
    applyNewFilter: PropTypes.bool,

    isGetAllVouchersFetching: PropTypes.bool,
    isGetAllVouchersSuccess: PropTypes.bool,
    isGetAllVouchersError: PropTypes.bool,
    isGetAllVouchersErrorMessage: PropTypes.string,

    isDeleteVoucherFetching: PropTypes.bool,
    isDeleteVoucherSuccess: PropTypes.bool,
    isDeleteVoucherError: PropTypes.bool,
    isDeleteVoucherErrorMessage: PropTypes.string,

    isAddVoucherModalOpen: PropTypes.bool,
    isEditVoucherModalOpen: PropTypes.bool,

    getAllVouchers: PropTypes.func,
    deleteVoucher: PropTypes.func,
    toggleAddVoucherModal: PropTypes.func,
    toggleEditVoucherModal: PropTypes.func,
  };

  static defaultProps = {
    user: undefined,
    vouchersList: undefined,
    hasMoreVouchers: true,
    applyNewFilter: false,

    isGetAllVouchersFetching: false,
    isGetAllVouchersSuccess: false,
    isGetAllVouchersError: false,
    isGetAllVouchersErrorMessage: undefined,

    isDeleteVoucherFetching: false,
    isDeleteVoucherSuccess: false,
    isDeleteVoucherError: false,
    isDeleteVoucherErrorMessage: undefined,

    isAddVoucherModalOpen: false,
    isEditVoucherModalOpen: false,

    getAllVouchers: () => {},
    deleteVoucher: () => {},
    toggleAddVoucherModal: () => {},
    toggleEditVoucherModal: () => {},
  };

  constructor(props) {
    super(props);

    const filterQueries = {
      skip: 0,
      limit: 20,
    };
    /*
        const cachedFilterState = JSON.parse(sessionStorage.getItem('vouchersFilterState'));

        if (cachedFilterState) {
          const { filterQueries: cachedFilterQueries } = cachedFilterState;

          filterQueries = cachedFilterQueries;
        }
    */

    this.state = {
      index: 0,
      filterQueries,
      confirmModalText: '',
      confirmModalHeader: '',
      voucherDetails: undefined,
      isConfirmModalOpen: false,
      isFilterSidebarOpen: false,
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllVouchers: getAllVouchersAction } = this.props;
    const { filterQueries } = this.state;

    getAllVouchersAction(filterQueries, true);
  }

  componentDidUpdate(prevProps) {
    const { isDeleteVoucherSuccess } = this.props;

    if (isDeleteVoucherSuccess && isDeleteVoucherSuccess !== prevProps.isDeleteVoucherSuccess) {
      this.handleToggleConfirmModal('', '', () => {});
    }
  }

  loadingVouchers = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingVouchersList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.loadingVouchers(counter));
    }
    return list;
  };

  loadMoreVouchers = () => {
    const { getAllVouchers: getAllVouchersAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: {
          ...filterQueries,
          skip: skip + 20,
          limit,
        },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllVouchersAction(filterQueriesNextState);
      },
    );
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({ isConfirmModalOpen: !isConfirmModalOpen });
      },
    );
  };

  handleToggleAddVoucherModal = () => {
    const { toggleAddVoucherModal: toggleAddVoucherModalAction } = this.props;

    toggleAddVoucherModalAction();
  };

  handleToggleEditVoucherModal = (index, voucherDetails) => {
    const { toggleEditVoucherModal: toggleEditVoucherModalAction } = this.props;
    this.setState({ voucherDetails, index });

    toggleEditVoucherModalAction();
  };

  handleToggleFilterModal = () => {
    const { isFilterSidebarOpen } = this.state;

    this.setState({ isFilterSidebarOpen: !isFilterSidebarOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({
      filterQueries: {
        ...filterQueries,
        [type]: val,
        skip: 0,
        limit: 20,
      },
    });
  };

  render() {
    const {
      user,
      user: { permissions },
      vouchersList,
      hasMoreVouchers,
      applyNewFilter,

      isGetAllVouchersFetching,
      isGetAllVouchersSuccess,
      isGetAllVouchersError,
      isGetAllVouchersErrorMessage,

      isDeleteVoucherFetching,
      isDeleteVoucherError,
      isDeleteVoucherErrorMessage,

      isAddVoucherModalOpen,
      isEditVoucherModalOpen,

      getAllVouchers: getAllVouchersAction,
      deleteVoucher: deleteVoucherAction,
    } = this.props;

    const {
      index,
      filterQueries,
      confirmModalText,
      confirmModalHeader,
      voucherDetails,
      isConfirmModalOpen,
      isFilterSidebarOpen,
      confirmModalFunction,
    } = this.state;

    let vouchersRenderer = this.createLoadingVouchersList();

    if (
      (isGetAllVouchersFetching && (!vouchersList || applyNewFilter)) ||
      (!isGetAllVouchersFetching && !isGetAllVouchersSuccess && !isGetAllVouchersError)
    ) {
      // Loading vouchers from backend
      vouchersRenderer = this.createLoadingVouchersList();
    } else if (isGetAllVouchersError) {
      // Backend responded with error while loading vouchers
      vouchersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllVouchersErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!vouchersList.length) {
      // Backend responded with empty [] after loading vouchers
      vouchersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      // Backend responded with [vouchers]
      vouchersRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={vouchersList.length}
            next={this.loadMoreVouchers}
            hasMore={hasMoreVouchers}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <Vouchers
              user={user}
              vouchers={vouchersList}
              isConfirmModalOpen={isConfirmModalOpen}
              isAddVoucherModalOpen={isAddVoucherModalOpen}
              confirmModalHeader={confirmModalHeader}
              confirmModalText={confirmModalText}
              confirmModalFunction={confirmModalFunction}
              handleToggleConfirmModal={this.handleToggleConfirmModal}
              handleDeleteVoucher={deleteVoucherAction}
              isDeleteVoucherFetching={isDeleteVoucherFetching}
              isDeleteVoucherError={isDeleteVoucherError}
              isDeleteVoucherErrorMessage={isDeleteVoucherErrorMessage}
              voucherIndex={index}
              voucherDetails={voucherDetails}
              isEditVoucherModalOpen={isEditVoucherModalOpen}
              handleToggleEditVoucherModal={this.handleToggleEditVoucherModal}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>الفوتشرز</title>
        </Helmet>
        {permissions && permissions.includes('Get All Vouchers') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الفوتشرز
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Add New Voucher') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={this.handleToggleAddVoucherModal}
                    icon={faPlus}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={false}
                    reverse
                  >
                    <Text
                      cursor="pointer"
                      mx={2}
                      type={SUBHEADING}
                      color={COLORS_VALUES[DISABLED]}
                      fontWeight={NORMAL}
                    >
                      أضف فوتشر جديد
                    </Text>
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="vouchers"
              filterSections={['city', 'isForNewCustomer']}
              citiesQueryKey="city"
              voucherCodeQueryKey="code"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterSidebarOpen}
              filterFunction={getAllVouchersAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {vouchersRenderer}
          </>
        )}
      </Flex>
    );
  }
}

VouchersContainer.displayName = 'VouchersContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  vouchersList: state.vouchers.vouchersList,
  hasMoreVouchers: state.vouchers.hasMoreVouchers,
  applyNewFilter: state.vouchers.applyNewFilter,

  isGetAllVouchersFetching: state.vouchers.getAllVouchers.isFetching,
  isGetAllVouchersSuccess: state.vouchers.getAllVouchers.isSuccess,
  isGetAllVouchersError: state.vouchers.getAllVouchers.isFail.isError,
  isGetAllVouchersErrorMessage: state.vouchers.getAllVouchers.isFail.message,

  isAddVoucherFetching: state.vouchers.addVoucher.isFetching,
  isAddVoucherSuccess: state.vouchers.addVoucher.isSuccess,
  isAddVoucherError: state.vouchers.addVoucher.isFail.isError,
  isAddVoucherErrorMessage: state.vouchers.addVoucher.isFail.message,

  isEditVoucherFetching: state.vouchers.editVoucher.isFetching,
  isEditVoucherSuccess: state.vouchers.editVoucher.isSuccess,
  isEditVoucherError: state.vouchers.editVoucher.isFail.isError,
  isEditVoucherErrorMessage: state.vouchers.editVoucher.isFail.message,

  isDeleteVoucherFetching: state.vouchers.deleteVoucher.isFetching,
  isDeleteVoucherSuccess: state.vouchers.deleteVoucher.isSuccess,
  isDeleteVoucherError: state.vouchers.deleteVoucher.isFail.isError,
  isDeleteVoucherErrorMessage: state.vouchers.deleteVoucher.isFail.message,

  isAddVoucherModalOpen: state.vouchers.addVoucherModal.isOpen,
  isEditVoucherModalOpen: state.vouchers.editVoucherModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllVouchers,
      addVoucherReset,
      updateVoucher,
      deleteVoucher,
      toggleAddVoucherModal,
      toggleEditVoucherModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(VouchersContainer);
