import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getRequestDetailsByPrettyId,
  cancelRequest,
  activateCancelledRequest,
  addComment,
  addTicket,
  updateTicketStatus,
  activateRequest,
  activatePostponedRequest,
  toggleAdminCancelRequestModal,
  toggleAdminPostponeRequestModal,
  postponeRequest,
  chooseWorker,
  acceptWorker,
} from 'redux-modules/requests/actions';
import { getAllAdmins } from 'redux-modules/user/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import SingleRequest from 'components/SingleRequest';
import { Box, Flex } from '@rebass/grid';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import {REQUESTS_STATUSES} from "utils/constants";
import {initializeApp, getApp, getApps} from "firebase/app";
import {getDatabase, onValue, ref} from "firebase/database";
import {isEmpty, isEqual} from "lodash";

const { BRANDING_GREEN, WHITE, GREY_LIGHT } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class SingleRequestContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    requestDetails: PropTypes.shape({}),
    isGetRequestDetailsByPrettyIdFetching: PropTypes.bool,
    isGetRequestDetailsByPrettyIdError: PropTypes.bool,
    getRequestDetailsByPrettyIdErrorMessage: PropTypes.string,
    isCancelRequestFetching: PropTypes.bool,
    isCancelRequestSuccess: PropTypes.bool,
    isAddRequestTicketFetching: PropTypes.bool,
    isAddRequestTicketSuccess: PropTypes.bool,
    isUpdateRequestTicketStatusFetching: PropTypes.bool,
    getRequestDetailsByPrettyId: PropTypes.func,
    cancelRequest: PropTypes.func,
    activateCancelledRequest: PropTypes.func,
    addComment: PropTypes.func,
    addTicket: PropTypes.func,
    updateTicketStatus: PropTypes.func,
    activateRequest: PropTypes.func,
    activatePostponedRequest: PropTypes.func,
    match: PropTypes.shape({}),
    addCommentState: PropTypes.string,
    addCommentErrorMessage: PropTypes.string,
    isActivateRequestFetching: PropTypes.bool,
    isActivatePostponedRequestFetching: PropTypes.bool,
    isActivateRequestSuccess: PropTypes.bool,
    isActivatePostponedRequestSuccess: PropTypes.bool,
    isUpdateRequestTicketStatusError: PropTypes.bool,
    updateRequestTicketStatusErrorMessage: PropTypes.string,
    isAddRequestTicketError: PropTypes.bool,
    addRequestTicketErrorMessage: PropTypes.string,
    getAllAdmins: PropTypes.func,
    admins: PropTypes.arrayOf(PropTypes.shape({})),
    isConfirmModalHasError: PropTypes.bool,
    confirmModalErrorMessage: PropTypes.string,
    isEditInvoiceFetching: PropTypes.bool,
    isEditInvoiceSuccess: PropTypes.bool,
    isEditInvoiceError: PropTypes.bool,
    isEditInvoiceErrorMessage: PropTypes.string,
    toggleAdminCancelRequestModal: PropTypes.func,
    isAdminCancelRequestModalOpen: PropTypes.bool,
    toggleAdminPostponeRequestModal: PropTypes.func,
    isPostponeRequestFetching: PropTypes.bool,
    isPostponeRequestSuccess: PropTypes.bool,
    postponeRequest: PropTypes.func,
    chooseWorker: PropTypes.func,
    acceptWorker: PropTypes.func,

    isAcceptWorkerSuccess: PropTypes.bool,
    isAcceptWorkerFetching: PropTypes.bool,
    isAcceptWorkerError: PropTypes.bool,
    acceptWorkerErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    requestDetails: undefined,
    isGetRequestDetailsByPrettyIdFetching: false,
    isGetRequestDetailsByPrettyIdError: false,
    getRequestDetailsByPrettyIdErrorMessage: undefined,
    isCancelRequestFetching: false,
    isCancelRequestSuccess: false,
    isAddRequestTicketFetching: false,
    isAddRequestTicketSuccess: false,
    isUpdateRequestTicketStatusFetching: false,
    getRequestDetailsByPrettyId: () => {},
    cancelRequest: () => {},
    activateCancelledRequest: () => {},
    addComment: () => {},
    addTicket: () => {},
    updateTicketStatus: () => {},
    activateRequest: () => {},
    activatePostponedRequest: () => {},
    match: undefined,
    addCommentState: undefined,
    addCommentErrorMessage: undefined,
    isActivateRequestFetching: false,
    isActivatePostponedRequestFetching: false,
    isActivateRequestSuccess: false,
    isActivatePostponedRequestSuccess: false,
    isUpdateRequestTicketStatusError: false,
    updateRequestTicketStatusErrorMessage: undefined,
    isAddRequestTicketError: false,
    addRequestTicketErrorMessage: undefined,
    getAllAdmins: () => {},
    admins: [],
    isConfirmModalHasError: false,
    confirmModalErrorMessage: undefined,
    isEditInvoiceFetching: false,
    isEditInvoiceSuccess: false,
    isEditInvoiceError: false,
    isEditInvoiceErrorMessage: undefined,
    toggleAdminCancelRequestModal: () => {},
    isAdminCancelRequestModalOpen: false,
    toggleAdminPostponeRequestModal: () => {},
    isPostponeRequestFetching: false,
    isPostponeRequestSuccess: false,
    postponeRequest: () => {},
    chooseWorker: () => {},
    acceptWorker: () => {},
    isAcceptWorkerSuccess: false,
    isAcceptWorkerFetching: false,
    isAcceptWorkerError: false,
    acceptWorkerErrorMessage: undefined,
  };

  state = {
    isConfirmModalOpen: false,
    confirmModalHeader: '',
    confirmModalText: '',
    confirmModalFunction: () => {},
    isAppliedWorkersModalOpen: false,
    isOpenTicketModalOpen: false,
    isEditInvoiceModalOpen: false,
    addedCommentValue: '',
    mentionData: [],
    isOpenFinishRequestModalOpen: false,
    isOpenChooseWorkerModalOpen: false,
    appliedWorkersDetails: [],
  };

  componentDidMount() {
    const {
      getRequestDetailsByPrettyId: getRequestDetailsByPrettyIdAction,
      getAllAdmins: getAllAdminsAction,
      match: {
        params: { id },
      },
    } = this.props;

    getRequestDetailsByPrettyIdAction(id);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  componentDidUpdate(prevProps) {
    const {
      isActivateRequestSuccess,
      isActivatePostponedRequestSuccess,
      isAddRequestTicketSuccess,
      isEditInvoiceSuccess,
      requestDetails,
      isPostponeRequestSuccess,
      isChooseWorkerSuccess,
      isAcceptWorkerSuccess,
    } = this.props;

    if (
      isActivateRequestSuccess &&
      isActivateRequestSuccess !== prevProps.isActivateRequestSuccess
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }

    if (
      isActivatePostponedRequestSuccess &&
      isActivatePostponedRequestSuccess !== prevProps.isActivatePostponedRequestSuccess
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }

    if (
      isAddRequestTicketSuccess &&
      isAddRequestTicketSuccess !== prevProps.isAddRequestTicketSuccess
    ) {
      this.handleToggleOpenTicketModal();
    }

    if (isEditInvoiceSuccess && isEditInvoiceSuccess !== prevProps.isEditInvoiceSuccess) {
      this.handleToggleEditInvoiceModal();
    }

    if (
      requestDetails !== prevProps.requestDetails &&
      requestDetails.paymentStatuses[requestDetails.paymentStatuses.length - 1] &&
      requestDetails.paymentStatuses[requestDetails.paymentStatuses.length - 1].status ===
        'requested'
    ) {
      this.handleToggleOpenFinishRequestModal();
    }

    if (
      isPostponeRequestSuccess &&
      isPostponeRequestSuccess !== prevProps.isPostponeRequestSuccess
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }

    if (
      isChooseWorkerSuccess &&
      isChooseWorkerSuccess !== prevProps.isChooseWorkerSuccess
    ) {
      this.handleToggleOpenChooseWorkerModal();
    }

    if (
      isAcceptWorkerSuccess &&
      isAcceptWorkerSuccess !== prevProps.isAcceptWorkerSuccess
    ) {
      this.handleToggleConfirmModal();
    }
  }

  openRequestLocation = (lat, lng) => {
    window.open(`https://maps.google.com/?q=${lat},${lng}`, '_blank');
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleAppliedWorkersModal = () => {
    const { isAppliedWorkersModalOpen } = this.state;

    this.setState({
      isAppliedWorkersModalOpen: !isAppliedWorkersModalOpen,
    });
  };

  handleToggleOpenTicketModal = () => {
    const { isOpenTicketModalOpen } = this.state;

    this.setState({
      isOpenTicketModalOpen: !isOpenTicketModalOpen,
    });
  };

  handleToggleEditInvoiceModal = () => {
    const { isEditInvoiceModalOpen } = this.state;

    this.setState({
      isEditInvoiceModalOpen: !isEditInvoiceModalOpen,
    });
  };

  handleToggleOpenFinishRequestModal = () => {
    const { isOpenFinishRequestModalOpen } = this.state;

    this.setState({
      isOpenFinishRequestModalOpen: !isOpenFinishRequestModalOpen,
    });
  };

  handleToggleOpenChooseWorkerModal = () => {
    const { isOpenChooseWorkerModalOpen } = this.state;
    this.setState({
      isOpenChooseWorkerModalOpen: !isOpenChooseWorkerModalOpen,
    });
  };

  handleAddCommentInputChange = (event, newValue, newPlainTextValue, mentions) => {
    const { mentionData } = this.state;
    this.setState({
      addedCommentValue: newPlainTextValue,
      mentionData: mentionData.concat(mentions),
    });
  };

  resetAddCommentInput = () => {
    this.setState({ addedCommentValue: '' });
  };

  handleToggleAdminCancelRequestModal = () => {
    const { toggleAdminCancelRequestModal: toggleAdminCancelRequestModalAction } = this.props;

    toggleAdminCancelRequestModalAction();
  };

  handleToggleAdminPostponeRequestModal = () => {
    const { toggleAdminPostponeRequestModal: toggleAdminPostponeRequestModalAction } = this.props;

    toggleAdminPostponeRequestModalAction();
  };

  render() {
    const {
      user,
      user: { permissions },
      requestDetails,
      isGetRequestDetailsByPrettyIdFetching,
      cancelRequest: cancelRequestAction,
      activateCancelledRequest: activateCancelledRequestAction,
      addComment: addCommentAction,
      addTicket: addTicketAction,
      updateTicketStatus: updateTicketStatusAction,
      isCancelRequestFetching,
      isAddRequestTicketFetching,
      isUpdateRequestTicketStatusFetching,
      addCommentState,
      addCommentErrorMessage,
      isActivateRequestFetching,
      activateRequest: activateRequestAction,
      activatePostponedRequest: activatePostponedRequestAction,
      isGetRequestDetailsByPrettyIdError,
      getRequestDetailsByPrettyIdErrorMessage,
      isUpdateRequestTicketStatusError,
      updateRequestTicketStatusErrorMessage,
      isAddRequestTicketError,
      addRequestTicketErrorMessage,
      admins,
      isConfirmModalHasError,
      confirmModalErrorMessage,
      isEditInvoiceFetching,
      isEditInvoiceError,
      isEditInvoiceErrorMessage,
      isAdminCancelRequestModalOpen,
      isPostponeRequestFetching,
      postponeRequest: postponeRequestAction,
      chooseWorker: chooseWorkerAction,
      acceptWorker: acceptWorkerAction,
      isChooseWorkerFetching,
      isChooseWorkerError,
      chooseWorkerErrorMessage,

      isAcceptWorkerSuccess,
      isAcceptWorkerFetching,
      isAcceptWorkerError,
      acceptWorkerErrorMessage,
    } = this.props;
    const {
      isConfirmModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isAppliedWorkersModalOpen,
      isOpenTicketModalOpen,
      isEditInvoiceModalOpen,
      addedCommentValue,
      isOpenFinishRequestModalOpen,
      isOpenChooseWorkerModalOpen,
      mentionData,
      appliedWorkersDetails
    } = this.state;


    if((requestDetails &&requestDetails.status) === REQUESTS_STATUSES.pending.en.toLowerCase()) {
      const firebaseConfig = {
        apiKey: "AIzaSyCr6qJuyOgqLtNjcbVNV09x7eMdgYPCCZI",
        authDomain: "fanniapp-bdcf4.firebaseapp.com",
        databaseURL: "https://fanniapp-bdcf4.firebaseio.com",
        projectId: "fanniapp-bdcf4",
        storageBucket: "fanniapp-bdcf4.appspot.com",
        messagingSenderId: "591978559350",
        appId: "1:591978559350:web:df9a05b421f33c831c6c57",
        measurementId: "G-EXML4S483L"
      };

      const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();

      const db = getDatabase(app);
      const starCountRef = ref(db, `dev/requests/${requestDetails.requestPrettyId}`);

      onValue(starCountRef, (snapshot) => {
        const request = snapshot.val();
        const dbAppliedWorkersDetails = request && !isEmpty( request.appliedWorkersDetails)? request.appliedWorkersDetails: [];
        console.log('dbAppliedWorkersDetails chnage', dbAppliedWorkersDetails.map((worker) => worker._id.toString()).sort())
        console.log('appliedWorkersDetails chnage', appliedWorkersDetails.map((worker) => worker._id.toString()).sort())

        if(!isEqual(this.state.appliedWorkersDetails.map((worker) => worker._id.toString()).sort(), dbAppliedWorkersDetails.map((worker) => worker._id.toString()).sort())) {
          this.setState({ appliedWorkersDetails: dbAppliedWorkersDetails });
        }
      });
    }


    if (isGetRequestDetailsByPrettyIdFetching && !requestDetails) {
      return (
        <ShimmerEffect width={1}>
          <Rect width={1 / 3} height={550} m={2} />
          <Rect width={2 / 3} height={550} m={2} />
        </ShimmerEffect>
      );
    }
    if (isGetRequestDetailsByPrettyIdError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getRequestDetailsByPrettyIdErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      permissions &&
      permissions.includes('Get Requests By Its Pretty Id') && (
        <SingleRequest
          user={user}
          admins={admins}
          requestDetails={requestDetails}
          handleAppliedWorkerDetailsChanged={this.handleAppliedWorkerDetailsChanged}
          appliedWorkersDetails={appliedWorkersDetails}
          isGetRequestDetailsFetching={isGetRequestDetailsByPrettyIdFetching}
          openRequestLocation={this.openRequestLocation}
          isConfirmModalOpen={isConfirmModalOpen}
          isAppliedWorkersModalOpen={isAppliedWorkersModalOpen}
          isOpenTicketModalOpen={isOpenTicketModalOpen}
          isEditInvoiceModalOpen={isEditInvoiceModalOpen}
          confirmModalHeader={confirmModalHeader}
          confirmModalText={confirmModalText}
          confirmModalFunction={confirmModalFunction}
          handleToggleConfirmModal={this.handleToggleConfirmModal}
          handleToggleAppliedWorkersModal={this.handleToggleAppliedWorkersModal}
          handleToggleOpenTicketModal={this.handleToggleOpenTicketModal}
          handleToggleEditInvoiceModal={this.handleToggleEditInvoiceModal}
          handleCancelRequest={cancelRequestAction}
          handleActivateCancelledRequest={activateCancelledRequestAction}
          handleAddRequestTicket={addTicketAction}
          handleUpdateRequestTicketStatus={updateTicketStatusAction}
          isCancelRequestFetching={isCancelRequestFetching}
          isAddRequestTicketFetching={isAddRequestTicketFetching}
          isUpdateRequestTicketStatusFetching={isUpdateRequestTicketStatusFetching}
          addedCommentValue={addedCommentValue}
          handleAddCommentInputChange={this.handleAddCommentInputChange}
          addComment={addCommentAction}
          addCommentState={addCommentState}
          addCommentErrorMessage={addCommentErrorMessage}
          resetAddCommentInput={this.resetAddCommentInput}
          handleToggleOpenFinishRequestModal={this.handleToggleOpenFinishRequestModal}
          handleToggleOpenChooseWorkerModal={this.handleToggleOpenChooseWorkerModal}
          chooseWorkerAction={chooseWorkerAction}
          acceptWorkerAction={acceptWorkerAction}
          isChooseWorkerFetching={isChooseWorkerFetching}
          isChooseWorkerError={isChooseWorkerError}
          chooseWorkerErrorMessage={chooseWorkerErrorMessage}

          isAcceptWorkerSuccess={isAcceptWorkerSuccess}
          isAcceptWorkerFetching={isAcceptWorkerFetching}
          isAcceptWorkerError={isAcceptWorkerError}
          acceptWorkerErrorMessage={acceptWorkerErrorMessage}

          isOpenFinishRequestModalOpen={isOpenFinishRequestModalOpen}
          isOpenChooseWorkerModalOpen={isOpenChooseWorkerModalOpen}
          handleActivateRequest={activateRequestAction}
          handleActivatePostponedRequest={activatePostponedRequestAction}
          isActivateRequestFetching={isActivateRequestFetching}
          isUpdateRequestTicketStatusError={isUpdateRequestTicketStatusError}
          updateRequestTicketStatusErrorMessage={updateRequestTicketStatusErrorMessage}
          isAddRequestTicketError={isAddRequestTicketError}
          addRequestTicketErrorMessage={addRequestTicketErrorMessage}
          mentionData={mentionData}
          isConfirmModalHasError={isConfirmModalHasError}
          confirmModalErrorMessage={confirmModalErrorMessage}
          isEditInvoiceFetching={isEditInvoiceFetching}
          isEditInvoiceError={isEditInvoiceError}
          isEditInvoiceErrorMessage={isEditInvoiceErrorMessage}
          isAdminCancelRequestModalOpen={isAdminCancelRequestModalOpen}
          handleToggleAdminCancelRequestModal={this.handleToggleAdminCancelRequestModal}
          isPostponeRequestFetching={isPostponeRequestFetching}
          handlePostponeRequest={postponeRequestAction}
        />
      )
    );
  }
}
SingleRequestContainer.displayName = 'SingleRequestContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  admins: state.user.admins,
  requestDetails: state.requests.requestDetails,
  isGetRequestDetailsByPrettyIdFetching: state.requests.getRequestDetails.isFetching,
  isGetRequestDetailsByPrettyIdError: state.requests.getRequestDetails.isFail.isError,
  getRequestDetailsByPrettyIdErrorMessage: state.requests.getRequestDetails.isFail.message,
  isGetRequestDetailsByPrettyIdSuccess: state.requests.getRequestDetails.isSuccess,
  isCancelRequestFetching: state.requests.cancelRequest.isFetching,
  isCancelRequestSuccess: state.requests.cancelRequest.isSuccess,
  addCommentState: state.requests.addComment.addCommentState,
  addCommentErrorMessage: state.requests.addComment.error,
  isAddRequestTicketFetching: state.requests.addTicket.isFetching,
  isAddRequestTicketSuccess: state.requests.addTicket.isSuccess,
  isAddRequestTicketError: state.requests.addTicket.isFail.isError,
  addRequestTicketErrorMessage: state.requests.addTicket.isFail.message,
  isUpdateRequestTicketStatusFetching: state.requests.updateTicketStatus.isFetching,
  isUpdateRequestTicketStatusSuccess: state.requests.updateTicketStatus.isSuccess,
  isUpdateRequestTicketStatusError: state.requests.updateTicketStatus.isFail.isError,
  updateRequestTicketStatusErrorMessage: state.requests.updateTicketStatus.isFail.message,
  isActivateRequestFetching: state.requests.activateRequest.isFetching,
  isActivateRequestSuccess: state.requests.activateRequest.isSuccess,

  isActivatePostponedRequestFetching: state.requests.activatePostponedRequest.isFetching,
  isActivatePostponedRequestSuccess: state.requests.activatePostponedRequest.isSuccess,
  isConfirmModalHasError: state.requests.activateRequest.isFail.isError || state.requests.activatePostponedRequest.isFail.isError,
  confirmModalErrorMessage: state.requests.activateRequest.isFail.message || state.requests.activatePostponedRequest.isFail.message,

  isEditInvoiceFetching: state.requests.editInvoice.isFetching,
  isEditInvoiceSuccess: state.requests.editInvoice.isSuccess,
  isEditInvoiceError: state.requests.editInvoice.isFail.isError,
  isEditInvoiceErrorMessage: state.requests.editInvoice.isFail.message,
  isAdminCancelRequestModalOpen: state.requests.cancelRequest.isModalOpen,
  isPostponeRequestFetching: state.requests.postponeRequest.isFetching,
  isPostponeRequestSuccess: state.requests.postponeRequest.isSuccess,

  isChooseWorkerSuccess: state.requests.chooseWorker.isSuccess,
  isChooseWorkerFetching: state.requests.chooseWorker.isFetching,
  isChooseWorkerError: state.requests.chooseWorker.isFail.isError,
  chooseWorkerErrorMessage: state.requests.chooseWorker.isFail.message,

  isAcceptWorkerSuccess: state.requests.acceptWorker.isSuccess,
  isAcceptWorkerFetching: state.requests.acceptWorker.isFetching,
  isAcceptWorkerError: state.requests.acceptWorker.isFail.isError,
  acceptWorkerErrorMessage: state.requests.acceptWorker.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRequestDetailsByPrettyId,
      cancelRequest,
      activateCancelledRequest,
      addComment,
      addTicket,
      updateTicketStatus,
      activateRequest,
      activatePostponedRequest,
      getAllAdmins,
      toggleAdminCancelRequestModal,
      toggleAdminPostponeRequestModal,
      postponeRequest,
      chooseWorker,
      acceptWorker,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(SingleRequestContainer);
