import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class DeductionReasonsDropDown extends Component {
  componentDidMount() {
    const { isFetchConfigsSuccuss, fetchConfigs: fetchConfigsAction } = this.props;

    if (!isFetchConfigsSuccuss) {
      fetchConfigsAction();
    }
  }

  render() {
    const {
      configs,
      isFetchConfigsSuccuss,
      handleInputChange,
      deductionReasonsSelectedValue,
      isLoading,
      isDisabled,
      isDanger,
      isMulti,
    } = this.props;
    const deductionReasonsSelectOptions =
      configs &&
      Object.keys(configs.enums.deductionReasons).map( key => ({
        value: key,
        label: configs.enums.deductionReasons[key].ar,
      }));

    return (
      <StyledSelect
        {...this.props}
        width="calc(100% - 14px);"
        placeholder="اختر اسباب الخصم"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={isMulti}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={deductionReasonsSelectedValue}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isFetchConfigsSuccuss || isDisabled}
        isLoading={!isFetchConfigsSuccuss || isLoading}
        options={deductionReasonsSelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}
DeductionReasonsDropDown.displayName = 'DeductionReasonsDropDown';

DeductionReasonsDropDown.propTypes = {
  configs: PropTypes.shape({}),
  isFetchConfigsSuccuss: PropTypes.bool,
  deductionReasonsSelectedValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.shape({}),
    PropTypes.string,
  ]),
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isDanger: PropTypes.bool,
  isMulti: PropTypes.bool,
  fetchConfigs: PropTypes.func,
  handleInputChange: PropTypes.func,
};

DeductionReasonsDropDown.defaultProps = {
  configs: undefined,
  isFetchConfigsSuccuss: false,
  deductionReasonsSelectedValue: undefined,
  isLoading: false,
  isDisabled: false,
  isDanger: false,
  isMulti: true,
  fetchConfigs: () => {},
  handleInputChange: () => {},
};

const mapStateToProps = state => ({
  configs: state.configs.configs,
  isFetchConfigsSuccuss: state.configs.fetchConfigs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchConfigs,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(DeductionReasonsDropDown);
