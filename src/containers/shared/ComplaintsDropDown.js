import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class ComplaintsDropDown extends Component {
  componentDidMount() {
    const { isFetchConfigsSuccuss, fetchConfigs: fetchConfigsAction } = this.props;

    if (!isFetchConfigsSuccuss) {
      fetchConfigsAction();
    }
  }

  render() {
    const {
      configs,
      isFetchConfigsSuccuss,
      handleInputChange,
      complainSelectedValue,
      isLoading,
      isDisabled,
      isDanger,
    } = this.props;
    const complainsSelectOptions =
      configs &&
      configs.enums.complaintsList.map(complain => ({
        value: complain,
        label: complain.name,
      }));

    return (
      <StyledSelect
        {...this.props}
        placeholder="نوع الشكوي"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={false}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={complainSelectedValue}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isFetchConfigsSuccuss || isDisabled}
        isLoading={!isFetchConfigsSuccuss || isLoading}
        options={complainsSelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}
ComplaintsDropDown.displayName = 'ComplaintsDropDown';

ComplaintsDropDown.propTypes = {
  configs: PropTypes.shape({}),
  isFetchConfigsSuccuss: PropTypes.bool,
  complainSelectedValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.shape({}),
    PropTypes.string,
  ]),
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isDanger: PropTypes.bool,
  isMulti: PropTypes.bool,
  fetchConfigs: PropTypes.func,
  handleInputChange: PropTypes.func,
};

ComplaintsDropDown.defaultProps = {
  configs: undefined,
  isFetchConfigsSuccuss: false,
  complainSelectedValue: undefined,
  isLoading: false,
  isDisabled: false,
  isDanger: false,
  isMulti: false,
  fetchConfigs: () => {},
  handleInputChange: () => {},
};

const mapStateToProps = state => ({
  configs: state.configs.configs,
  isFetchConfigsSuccuss: state.configs.fetchConfigs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchConfigs,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintsDropDown);
