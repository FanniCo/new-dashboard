import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class UserPermissionsDropDown extends Component {
  componentDidMount() {
    const { isFetchConfigsSuccuss, fetchConfigs: fetchConfigsAction } = this.props;

    if (!isFetchConfigsSuccuss) {
      fetchConfigsAction();
    }
  }

  render() {
    const {
      configs,
      isFetchConfigsSuccuss,
      handleInputChange,
      userPermissionsSelectedValue,
      isLoading,
      isDanger,
      isMulti,
    } = this.props;
    const userPermissionsSelectOptions =
      configs &&
      configs.enums.adminPermissions.map(permission => ({
        value: permission.en,
        label: permission.ar === '' ? permission.en : permission.ar,
      }));

    return (
      <StyledSelect
        {...this.props}
        placeholder="صلاحيات مدير النظام"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={isMulti}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={userPermissionsSelectedValue}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isFetchConfigsSuccuss}
        isLoading={!isFetchConfigsSuccuss || isLoading}
        options={userPermissionsSelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}
UserPermissionsDropDown.displayName = 'UserPermissionsDropDown';

UserPermissionsDropDown.propTypes = {
  configs: PropTypes.shape({}),
  isFetchConfigsSuccuss: PropTypes.bool,
  userPermissionsSelectedValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.shape({}),
    PropTypes.string,
  ]),
  isLoading: PropTypes.bool,
  isDanger: PropTypes.bool,
  isMulti: PropTypes.bool,
  fetchConfigs: PropTypes.func,
  handleInputChange: PropTypes.func,
};

UserPermissionsDropDown.defaultProps = {
  configs: undefined,
  isFetchConfigsSuccuss: false,
  userPermissionsSelectedValue: undefined,
  isLoading: false,
  isDanger: false,
  isMulti: true,
  fetchConfigs: () => {},
  handleInputChange: () => {},
};

const mapStateToProps = state => ({
  configs: state.configs.configs,
  isFetchConfigsSuccuss: state.configs.fetchConfigs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchConfigs,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserPermissionsDropDown);
