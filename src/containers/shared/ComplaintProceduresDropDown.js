import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class ComplaintProceduresDropDown extends Component {
  componentDidMount() {
    const { isFetchConfigsSuccuss, fetchConfigs: fetchConfigsAction } = this.props;

    if (!isFetchConfigsSuccuss) {
      fetchConfigsAction();
    }
  }

  render() {
    const {
      configs,
      isFetchConfigsSuccuss,
      handleInputChange,
      complaintProceduresSelectedValue,
      isLoading,
      isDisabled,
      isDanger,
      isMulti,
    } = this.props;
    const complaintProceduresSelectOptions =
      configs &&
      configs.enums.proceduresListForComplains.filter((procedure) => procedure.displayInDashboardDropDown).map(procedure => ({
        value: procedure.code,
        label: procedure.name,
      }));

    return (
      <StyledSelect
        {...this.props}
        placeholder="طرق التصرف تجاه الشكوي"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={isMulti}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={complaintProceduresSelectedValue}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isFetchConfigsSuccuss || isDisabled}
        isLoading={!isFetchConfigsSuccuss || isLoading}
        options={complaintProceduresSelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}
ComplaintProceduresDropDown.displayName = 'ComplaintProceduresDropDown';

ComplaintProceduresDropDown.propTypes = {
  configs: PropTypes.shape({}),
  isFetchConfigsSuccuss: PropTypes.bool,
  complaintProceduresSelectedValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.shape({}),
    PropTypes.string,
  ]),
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isDanger: PropTypes.bool,
  isMulti: PropTypes.bool,
  fetchConfigs: PropTypes.func,
  handleInputChange: PropTypes.func,
};

ComplaintProceduresDropDown.defaultProps = {
  configs: undefined,
  isFetchConfigsSuccuss: false,
  complaintProceduresSelectedValue: undefined,
  isLoading: false,
  isDisabled: false,
  isDanger: false,
  isMulti: true,
  fetchConfigs: () => {},
  handleInputChange: () => {},
};

const mapStateToProps = state => ({
  configs: state.configs.configs,
  isFetchConfigsSuccuss: state.configs.fetchConfigs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchConfigs,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintProceduresDropDown);
