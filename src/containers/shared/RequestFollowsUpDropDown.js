import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchFollowUpsList } from 'redux-modules/statics/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class RequestFollowsUpDropDown extends Component {
  componentDidMount() {
    const { isGetFollowUpsListSuccess, fetchFollowUpsList: fetchFollowUpsListAction } = this.props;

    if (!isGetFollowUpsListSuccess) {
      fetchFollowUpsListAction();
    }
  }

  render() {
    const {
      isMulti,
      isDanger,
      value,
      handleInputChange,
      isGetFollowUpsListSuccess,
      followUpsList,
      type,
    } = this.props;
    const SelectOptions =
      followUpsList &&
      followUpsList[type].map(record => ({
        value: record.ar,
        label: record.ar,
      }));

    return (
      <StyledSelect
        {...this.props}
        placeholder=""
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={isMulti}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={value}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isGetFollowUpsListSuccess}
        isLoading={!isGetFollowUpsListSuccess}
        options={SelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}

RequestFollowsUpDropDown.displayName = 'RequestFollowsUpDropDown';

RequestFollowsUpDropDown.propTypes = {
  isMulti: PropTypes.bool,
  isDanger: PropTypes.bool,
  value: PropTypes.shape({}),
  handleInputChange: PropTypes.func,
  isGetFollowUpsListSuccess: PropTypes.bool,
  fetchFollowUpsList: PropTypes.func,
  followUpsList: PropTypes.shape({}),
  isFetchingFollowUpsList: PropTypes.bool,
  type: PropTypes.string,
};

RequestFollowsUpDropDown.defaultProps = {
  isMulti: true,
  isDanger: false,
  value: undefined,
  handleInputChange: () => {},
  isGetFollowUpsListSuccess: false,
  fetchFollowUpsList: () => {},
  followUpsList: undefined,
  isFetchingFollowUpsList: false,
  type: undefined,
};

const mapStateToProps = state => ({
  followUpsList: state.statics.followUpsList,
  isGetFollowUpsListSuccess: state.statics.getFollowUpsList.isSuccess,
  isFetchingFollowUpsList: state.statics.getFollowUpsList.isFetching,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchFollowUpsList,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RequestFollowsUpDropDown);
