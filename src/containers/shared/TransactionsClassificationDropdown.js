import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchTransactionsClassification } from 'redux-modules/statics/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class TransactionsClassificationDropdown extends Component {
  componentDidMount() {
    const {
      isGetTransactionsClassificationSuccess,
      fetchTransactionsClassification: fetchTransactionsClassificationAction,
    } = this.props;

    if (!isGetTransactionsClassificationSuccess) {
      fetchTransactionsClassificationAction();
    }
  }

  render() {
    const {
      transactionsClassification,
      isGetTransactionsClassificationSuccess,
      handleInputChange,
      TransactionsClassificationSelectedValue,
      isLoading,
      isDanger,
      isMulti,
      isGetTransactionsForCustomer,
    } = this.props;
    let transactionsClassificationSelectOptions = [];

    if (isGetTransactionsForCustomer) {
      transactionsClassificationSelectOptions =
        transactionsClassification &&
        transactionsClassification.customerClassifications.map(classification => ({
          value: classification.code,
          label: `${classification.name.ar} -- ${classification.code}`,
        }));
    } else {
      transactionsClassificationSelectOptions =
        transactionsClassification &&
        transactionsClassification.workerClassifications.map(classification => ({
          value: classification.code,
          label: `${classification.name.ar} -- ${classification.code}`,
        }));
    }

    return (
      <StyledSelect
        {...this.props}
        width="calc(100% - 14px);"
        placeholder="تصنيف العملية"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={isMulti}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={TransactionsClassificationSelectedValue}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isGetTransactionsClassificationSuccess}
        isLoading={!isGetTransactionsClassificationSuccess || isLoading}
        options={transactionsClassificationSelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}
TransactionsClassificationDropdown.displayName = 'TransactionsClassificationDropdown';

TransactionsClassificationDropdown.propTypes = {
  transactionsClassification: PropTypes.shape({}),
  isGetTransactionsClassificationSuccess: PropTypes.bool,
  TransactionsClassificationSelectedValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.shape({}),
    PropTypes.string,
  ]),
  isLoading: PropTypes.bool,
  isDanger: PropTypes.bool,
  isMulti: PropTypes.bool,
  fetchTransactionsClassification: PropTypes.func,
  handleInputChange: PropTypes.func,
  isGetTransactionsForCustomer: PropTypes.bool,
};

TransactionsClassificationDropdown.defaultProps = {
  transactionsClassification: {},
  isGetTransactionsClassificationSuccess: false,
  TransactionsClassificationSelectedValue: undefined,
  isLoading: false,
  isDanger: false,
  isMulti: true,
  fetchTransactionsClassification: () => {},
  handleInputChange: () => {},
  isGetTransactionsForCustomer: false,
};

const mapStateToProps = state => ({
  transactionsClassification: state.statics.transactionsClassification,
  isGetTransactionsClassificationSuccess: state.statics.getTransactionsClassification.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchTransactionsClassification,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsClassificationDropdown);
