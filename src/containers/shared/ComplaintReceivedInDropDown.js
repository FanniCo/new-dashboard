import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { StyledSelect, selectColors, customSelectStylesIsDanger } from 'components/shared';

class ComplaintReceivedInDropDown extends Component {
  componentDidMount() {
    const { isFetchConfigsSuccuss, fetchConfigs: fetchConfigsAction } = this.props;

    if (!isFetchConfigsSuccuss) {
      fetchConfigsAction();
    }
  }

  render() {
    const {
      configs,
      isFetchConfigsSuccuss,
      handleInputChange,
      complaintReceivedInSelectedValue,
      isLoading,
      isDisabled,
      isDanger,
      isMulti,
    } = this.props;
    const complaintReceivedInSelectOptions =
      configs &&
      configs.enums.complainReceivedInList.map(receivedIn => ({
        value: receivedIn,
        label: receivedIn.name,
      }));

    return (
      <StyledSelect
        {...this.props}
        placeholder="طريقة استلام الشكوي"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        isMulti={isMulti}
        backspaceRemovesValue={false}
        menuShouldScrollIntoView
        value={complaintReceivedInSelectedValue}
        onChange={val => {
          handleInputChange(val);
        }}
        maxMenuHeight={165}
        isDisabled={!isFetchConfigsSuccuss || isDisabled}
        isLoading={!isFetchConfigsSuccuss || isLoading}
        options={complaintReceivedInSelectOptions}
        theme={selectColors}
        styles={customSelectStylesIsDanger(isDanger)}
        isClearable
      />
    );
  }
}
ComplaintReceivedInDropDown.displayName = 'ComplaintReceivedInDropDown';

ComplaintReceivedInDropDown.propTypes = {
  configs: PropTypes.shape({}),
  isFetchConfigsSuccuss: PropTypes.bool,
  complaintReceivedInSelectedValue: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.shape({}),
    PropTypes.string,
  ]),
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isDanger: PropTypes.bool,
  isMulti: PropTypes.bool,
  fetchConfigs: PropTypes.func,
  handleInputChange: PropTypes.func,
};

ComplaintReceivedInDropDown.defaultProps = {
  configs: undefined,
  isFetchConfigsSuccuss: false,
  complaintReceivedInSelectedValue: undefined,
  isLoading: false,
  isDisabled: false,
  isDanger: false,
  isMulti: true,
  fetchConfigs: () => {},
  handleInputChange: () => {},
};

const mapStateToProps = state => ({
  configs: state.configs.configs,
  isFetchConfigsSuccuss: state.configs.fetchConfigs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchConfigs,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintReceivedInDropDown);
