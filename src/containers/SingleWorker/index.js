import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getWorkerDetails,
  editWorkerReset,
  changeWorkerPassword,
  changeWorkerPasswordReset,
  addCreditReset,
  activateWorker,
  suspendWorker,
  featureWorker,
  makeWorkerElite,
  deleteWorker,
  addComment,
  toggleChangeWorkerPasswordModal,
  toggleEditWorkerModal,
  toggleAddCreditModal,
  toggleSubscribeWorkerToAutoApplyModal,
  toggleSubscribeWorkerToMultiFieldsModal,
  getWorkerStatistics,
  getWorkerMotivationAndPunishments,
  finishWorkerAutoApplySubscription,
  finishWorkerMultiFieldsSubscription,
  toggleSuspendWorkerModal,
  toggleConfirmModal,
  allowWorkerToRecharge,
  approveTheWorkerResidenceInfo,
} from 'redux-modules/workers/actions';
import { getAllAdmins } from 'redux-modules/user/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import SingleWorker from 'components/SingleWorker';
import { Box, Flex } from '@rebass/grid';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { getAllVendors } from 'redux-modules/vendors/actions';

const { BRANDING_GREEN, WHITE, GREY_LIGHT } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class SingleWorkerContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    workerDetails: PropTypes.shape({}),
    isGetWorkerDetailsFetching: PropTypes.bool,
    isGetWorkerDetailsSuccess: PropTypes.bool,
    isActivateWorkerFetching: PropTypes.bool,
    isActivateWorkerError: PropTypes.bool,
    isSuspendWorkerFetching: PropTypes.bool,
    isSuspendWorkerError: PropTypes.bool,
    suspendWorker: PropTypes.func,
    isChangeWorkerPasswordFetching: PropTypes.bool,
    isChangeWorkerPasswordError: PropTypes.bool,
    changeWorkerPassword: PropTypes.func,
    isFeatureWorkerFetching: PropTypes.bool,
    isFeatureWorkerError: PropTypes.bool,
    featureWorkerErrorMessage: PropTypes.string,
    isMakeWorkerEliteFetching: PropTypes.bool,
    isMakeWorkerEliteError: PropTypes.bool,
    isMakeWorkerEliteErrorMessage: PropTypes.string,
    isDeleteWorkerFetching: PropTypes.bool,
    getWorkerDetails: PropTypes.func,
    editWorkerReset: PropTypes.func,
    changeWorkerPasswordReset: PropTypes.func,
    addCreditReset: PropTypes.func,
    activateWorker: PropTypes.func,
    // deleteWorker: PropTypes.func,
    featureWorker: PropTypes.func,
    makeWorkerElite: PropTypes.func,
    toggleChangeWorkerPasswordModal: PropTypes.func,
    toggleSubscribeWorkerToAutoApplyModal: PropTypes.func,
    toggleSubscribeWorkerToMultiFieldsModal: PropTypes.func,
    toggleEditWorkerModal: PropTypes.func,
    toggleAddCreditModal: PropTypes.func,
    isChangeWorkerPasswordModalOpen: PropTypes.bool,
    isSubscribeWorkerToAutoApplyModalOpen: PropTypes.bool,
    isSubscribeWorkerToMultiFieldsModalOpen: PropTypes.bool,
    isEditWorkerModalOpen: PropTypes.bool,
    isAddCreditModalOpen: PropTypes.bool,
    match: PropTypes.shape({}),
    addComment: PropTypes.func,
    addCommentState: PropTypes.string,
    addCommentErrorMessage: PropTypes.string,
    getWorkerStatistics: PropTypes.func,
    workerStatistics: PropTypes.shape({}),
    isGetWorkerStatisticsFetching: PropTypes.bool,
    isGetWorkerStatisticsSuccess: PropTypes.bool,
    getWorkerMotivationAndPunishments: PropTypes.func,
    workerMotivationAndPunishments: PropTypes.shape({}),
    isGetWorkerMotivationAndPunishmentsFetching: PropTypes.bool,
    isGetWorkerMotivationAndPunishmentsSuccess: PropTypes.bool,
    finishWorkerAutoApplySubscription: PropTypes.func,
    finishWorkerMultiFieldsSubscription: PropTypes.func,
    isFinishWorkerAutoApplySubscriptionFetching: PropTypes.bool,
    isFinishWorkerAutoApplySubscriptionError: PropTypes.bool,
    isFinishWorkerMultiFieldsSubscriptionFetching: PropTypes.bool,
    isFinishWorkerMultiFieldsSubscriptionError: PropTypes.bool,
    toggleSuspendWorkerModal: PropTypes.func,
    toggleConfirmModal: PropTypes.func,
    isSuspendWorkerModalOpen: PropTypes.bool,
    isConfirmModalOpened: PropTypes.bool,
    suspendWorkerErrorMessage: PropTypes.string,
    isGetWorkerDetailsError: PropTypes.bool,
    getWorkerDetailsErrorMessage: PropTypes.string,
    getAllVendors: PropTypes.func,
    vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
    getAllAdmins: PropTypes.func,
    admins: PropTypes.arrayOf(PropTypes.shape({})),
    allowWorkerToRecharge: PropTypes.func,
    isAllowWorkerToRechargeFetching: PropTypes.bool,
    isAllowWorkerToRechargeError: PropTypes.bool,
    isAllowWorkerToRechargeErrorMessage: PropTypes.string,
    isAllowWorkerToRechargeSuccess: PropTypes.bool,
    approveTheWorkerResidenceInfo: PropTypes.func,
  };

  static defaultProps = {
    user: undefined,
    workerDetails: undefined,
    isGetWorkerDetailsFetching: false,
    isGetWorkerDetailsSuccess: false,
    isActivateWorkerFetching: false,
    isActivateWorkerError: false,
    isSuspendWorkerFetching: false,
    isSuspendWorkerError: false,
    isChangeWorkerPasswordFetching: false,
    isChangeWorkerPasswordError: false,
    isFeatureWorkerFetching: false,
    isFeatureWorkerError: false,
    featureWorkerErrorMessage: undefined,
    isMakeWorkerEliteFetching: false,
    isMakeWorkerEliteError: false,
    isMakeWorkerEliteErrorMessage: undefined,
    isDeleteWorkerFetching: false,
    getWorkerDetails: () => {},
    editWorkerReset: () => {},
    changeWorkerPasswordReset: () => {},
    addCreditReset: () => {},
    activateWorker: () => {},
    suspendWorker: () => {},
    changeWorkerPassword: () => {},
    // deleteWorker: () => {},
    featureWorker: () => {},
    makeWorkerElite: () => {},
    toggleChangeWorkerPasswordModal: () => {},
    toggleSubscribeWorkerToAutoApplyModal: () => {},
    toggleSubscribeWorkerToMultiFieldsModal: () => {},
    toggleEditWorkerModal: () => {},
    toggleAddCreditModal: () => {},
    isChangeWorkerPasswordModalOpen: false,
    isSubscribeWorkerToAutoApplyModalOpen: false,
    isSubscribeWorkerToMultiFieldsModalOpen: false,
    isEditWorkerModalOpen: false,
    isAddCreditModalOpen: false,
    match: undefined,
    addComment: undefined,
    addCommentState: undefined,
    addCommentErrorMessage: undefined,
    getWorkerStatistics: () => {},
    workerStatistics: undefined,
    isGetWorkerStatisticsFetching: false,
    isGetWorkerStatisticsSuccess: false,
    getWorkerMotivationAndPunishments: () => {},
    workerMotivationAndPunishments: undefined,
    isGetWorkerMotivationAndPunishmentsFetching: false,
    isGetWorkerMotivationAndPunishmentsSuccess: false,
    finishWorkerAutoApplySubscription: () => {},
    finishWorkerMultiFieldsSubscription: () => {},
    isFinishWorkerAutoApplySubscriptionFetching: false,
    isFinishWorkerAutoApplySubscriptionError: false,
    isFinishWorkerMultiFieldsSubscriptionFetching: false,
    isFinishWorkerMultiFieldsSubscriptionError: false,
    toggleSuspendWorkerModal: () => {},
    toggleConfirmModal: () => {},
    isSuspendWorkerModalOpen: false,
    isConfirmModalOpened: false,
    suspendWorkerErrorMessage: undefined,
    isGetWorkerDetailsError: false,
    getWorkerDetailsErrorMessage: undefined,
    getAllVendors: () => {},
    vendorsList: [],
    getAllAdmins: () => {},
    admins: [],
    allowWorkerToRecharge: () => {},
    isAllowWorkerToRechargeFetching: false,
    isAllowWorkerToRechargeError: false,
    isAllowWorkerToRechargeErrorMessage: undefined,
    isAllowWorkerToRechargeSuccess: false,
    approveTheWorkerResidenceInfo: () => {},
  };

  state = {
    tabs: [
      { name: 'التفاصيل' },
      { name: 'بيانات الاقامة' },
      { name: 'سجل العمليات' },
      { name: 'الطلبات' },
      { name: 'التقيمات' },
      { name: 'الشكاوى' },
    ],
    activeTab: 0,
    confirmModalHeader: '',
    confirmModalText: '',
    confirmModalFunction: () => {},
    addedCommentValue: '',
    mentionData: [],
  };

  componentDidMount() {
    const {
      getWorkerDetails: getWorkerDetailsAction,
      match: {
        params: { id },
      },
      getAllVendors: getAllVendorsAction,
      getAllAdmins: getAllAdminsAction,
    } = this.props;

    getWorkerDetailsAction(id);
    getAllVendorsAction({ skip: 0, limit: 5000 }, true);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  componentDidUpdate(prevProps) {
    const {
      getWorkerStatistics: getWorkerStatisticsAction,
      getWorkerMotivationAndPunishments: getWorkerMotivationAndPunishmentsAction,
      isGetWorkerDetailsSuccess,
    } = this.props;

    if (isGetWorkerDetailsSuccess !== prevProps.isGetWorkerDetailsSuccess) {
      const {
        workerDetails: { _id },
      } = this.props;
      getWorkerStatisticsAction(_id);
      getWorkerMotivationAndPunishmentsAction(_id);
    }
  }

  handleTabChange = value => {
    const { activeTab } = this.state;

    if (activeTab !== value) {
      this.setState({ activeTab: value });
    }
  };

  openWorkerLocation = (lat, lng) => {
    window.open(`https://maps.google.com/?q=${lat},${lng}`, '_blank');
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { toggleConfirmModal: toggleConfirmModalAction, isConfirmModalOpened } = this.props;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpened ? header : '',
        confirmModalText: !isConfirmModalOpened ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpened ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpened);
      },
    );
  };

  handleToggleChangeWorkerPasswordModal = () => {
    const {
      toggleChangeWorkerPasswordModal: toggleChangeWorkerPasswordModalAction,
      changeWorkerPasswordReset: changeWorkerPasswordResetAction,
    } = this.props;

    changeWorkerPasswordResetAction();
    toggleChangeWorkerPasswordModalAction();
  };

  handleToggleSubscribeWorkerToAutoApplyToRequestsModal = () => {
    const {
      toggleSubscribeWorkerToAutoApplyModal: toggleSubscribeWorkerToAutoApplyModalAction,
    } = this.props;

    toggleSubscribeWorkerToAutoApplyModalAction();
  };

  handleToggleSubscribeWorkerToMultiFieldsModal = () => {
    const {
      toggleSubscribeWorkerToMultiFieldsModal: toggleSubscribeWorkerToMultiFieldsModalAction,
    } = this.props;

    toggleSubscribeWorkerToMultiFieldsModalAction();
  };

  handleToggleEditWorkerModal = () => {
    const {
      toggleEditWorkerModal: toggleEditWorkerModalAction,
      editWorkerReset: editWorkerResetAction,
    } = this.props;

    editWorkerResetAction();
    toggleEditWorkerModalAction();
  };

  handleToggleAddCreditModal = () => {
    const {
      toggleAddCreditModal: toggleAddCreditModalAction,
      addCreditReset: addCreditResetAction,
    } = this.props;

    addCreditResetAction();
    toggleAddCreditModalAction();
  };

  handleAddCommentInputChange = (event, newValue, newPlainTextValue, mentions) => {
    const { mentionData } = this.state;
    this.setState({
      addedCommentValue: newPlainTextValue,
      mentionData: mentionData.concat(mentions),
    });
  };

  resetAddCommentInput = () => {
    this.setState({ addedCommentValue: '' });
  };

  handleToggleSuspendWorkerModal = () => {
    const { toggleSuspendWorkerModal: toggleSuspendWorkerModalAction } = this.props;

    toggleSuspendWorkerModalAction();
  };

  render() {
    const {
      user,
      workerDetails,
      isGetWorkerDetailsFetching,
      activateWorker: activateWorkerAction,
      suspendWorker: suspendWorkerAction,
      changeWorkerPassword: changeWorkerPasswordAction,
      featureWorker: featureWorkerAction,
      makeWorkerElite: makeWorkerEliteAction,
      // deleteWorker: deleteWorkerAction,
      isChangeWorkerPasswordModalOpen,
      isSubscribeWorkerToAutoApplyModalOpen,
      isSubscribeWorkerToMultiFieldsModalOpen,
      isEditWorkerModalOpen,
      isAddCreditModalOpen,
      isActivateWorkerFetching,
      isActivateWorkerError,
      isSuspendWorkerFetching,
      isSuspendWorkerError,
      isChangeWorkerPasswordFetching,
      isChangeWorkerPasswordError,
      isFeatureWorkerFetching,
      isFeatureWorkerError,
      featureWorkerErrorMessage,
      isMakeWorkerEliteFetching,
      isMakeWorkerEliteError,
      isMakeWorkerEliteErrorMessage,
      isDeleteWorkerFetching,
      addComment: addCommentAction,
      addCommentState,
      addCommentErrorMessage,
      workerStatistics,
      isGetWorkerStatisticsSuccess,
      isGetWorkerStatisticsFetching,
      workerMotivationAndPunishments,
      isGetWorkerMotivationAndPunishmentsFetching,
      isGetWorkerMotivationAndPunishmentsSuccess,
      finishWorkerAutoApplySubscription: finishWorkerAutoApplySubscriptionAction,
      finishWorkerMultiFieldsSubscription: finishWorkerMultiFieldsSubscriptionAction,
      isFinishWorkerAutoApplySubscriptionFetching,
      isFinishWorkerAutoApplySubscriptionError,
      isFinishWorkerMultiFieldsSubscriptionFetching,
      isFinishWorkerMultiFieldsSubscriptionError,
      isSuspendWorkerModalOpen,
      isConfirmModalOpened,
      suspendWorkerErrorMessage,
      isGetWorkerDetailsError,
      getWorkerDetailsErrorMessage,
      vendorsList,
      admins,
      allowWorkerToRecharge: handleWorkerAllowedToRecharge,
      isAllowWorkerToRechargeFetching,
      isAllowWorkerToRechargeError,
      isAllowWorkerToRechargeErrorMessage,
      isAllowWorkerToRechargeSuccess,
      approveTheWorkerResidenceInfo: approveTheWorkerResidenceInfoAction,
    } = this.props;
    const {
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      addedCommentValue,
      tabs,
      activeTab,
      mentionData,
    } = this.state;

    if (isGetWorkerDetailsFetching && !workerDetails) {
      return (
        <ShimmerEffect width={1}>
          <Rect width={1 / 3} height={550} m={2} />
          <Rect width={2 / 3} height={550} m={2} />
        </ShimmerEffect>
      );
    }

    if (isGetWorkerDetailsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkerDetailsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      <SingleWorker
        tabs={tabs}
        activeTab={activeTab}
        handleTabChange={this.handleTabChange}
        user={user}
        workerDetails={workerDetails}
        isGetWorkerDetailsFetching={isGetWorkerDetailsFetching}
        openWorkerLocation={this.openWorkerLocation}
        isConfirmModalOpen={isConfirmModalOpened}
        isChangeWorkerPasswordModalOpen={isChangeWorkerPasswordModalOpen}
        isSubscribeWorkerToAutoApplyModalOpen={isSubscribeWorkerToAutoApplyModalOpen}
        isSubscribeWorkerToMultiFieldsModalOpen={isSubscribeWorkerToMultiFieldsModalOpen}
        isEditWorkerModalOpen={isEditWorkerModalOpen}
        isAddCreditModalOpen={isAddCreditModalOpen}
        confirmModalHeader={confirmModalHeader}
        confirmModalText={confirmModalText}
        confirmModalFunction={confirmModalFunction}
        handleToggleConfirmModal={this.handleToggleConfirmModal}
        handleToggleChangeWorkerPasswordModal={this.handleToggleChangeWorkerPasswordModal}
        handleToggleSubscribeWorkerToAutoApplyToRequestsModal={
          this.handleToggleSubscribeWorkerToAutoApplyToRequestsModal
        }
        handleToggleSubscribeWorkerToMultiFieldsModal={
          this.handleToggleSubscribeWorkerToMultiFieldsModal
        }
        handleToggleEditWorkerModal={this.handleToggleEditWorkerModal}
        handleToggleAddCreditModal={this.handleToggleAddCreditModal}
        handleActivateWorker={activateWorkerAction}
        handleSuspendWorker={suspendWorkerAction}
        handleChangeWorkerPassword={changeWorkerPasswordAction}
        handleFeatureWorker={featureWorkerAction}
        handleMakeWorkerElite={makeWorkerEliteAction}
        // handleDeleteWorker={deleteWorkerAction}
        isActivateWorkerFetching={isActivateWorkerFetching}
        isActivateWorkerError={isActivateWorkerError}
        isSuspendWorkerFetching={isSuspendWorkerFetching}
        isSuspendWorkerError={isSuspendWorkerError}
        isChangeWorkerPasswordFetching={isChangeWorkerPasswordFetching}
        isChangeWorkerPasswordError={isChangeWorkerPasswordError}
        isFeatureWorkerFetching={isFeatureWorkerFetching}
        isFeatureWorkerError={isFeatureWorkerError}
        featureWorkerErrorMessage={featureWorkerErrorMessage}
        isMakeWorkerEliteFetching={isMakeWorkerEliteFetching}
        isMakeWorkerEliteError={isMakeWorkerEliteError}
        isMakeWorkerEliteErrorMessage={isMakeWorkerEliteErrorMessage}
        isDeleteWorkerFetching={isDeleteWorkerFetching}
        addedCommentValue={addedCommentValue}
        handleAddCommentInputChange={this.handleAddCommentInputChange}
        addComment={addCommentAction}
        addCommentState={addCommentState}
        addCommentErrorMessage={addCommentErrorMessage}
        resetAddCommentInput={this.resetAddCommentInput}
        workerStatistics={workerStatistics}
        isGetWorkerStatisticsFetching={isGetWorkerStatisticsFetching}
        isGetWorkerStatisticsSuccess={isGetWorkerStatisticsSuccess}
        workerMotivationAndPunishments={workerMotivationAndPunishments}
        isGetWorkerMotivationAndPunishmentsFetching={isGetWorkerMotivationAndPunishmentsFetching}
        isGetWorkerMotivationAndPunishmentsSuccess={isGetWorkerMotivationAndPunishmentsSuccess}
        finishWorkerAutoApplySubscription={finishWorkerAutoApplySubscriptionAction}
        finishWorkerMultiFieldsSubscription={finishWorkerMultiFieldsSubscriptionAction}
        isFinishWorkerAutoApplySubscriptionFetching={isFinishWorkerAutoApplySubscriptionFetching}
        isFinishWorkerAutoApplySubscriptionError={isFinishWorkerAutoApplySubscriptionError}
        isFinishWorkerMultiFieldsSubscriptionFetching={
          isFinishWorkerMultiFieldsSubscriptionFetching
        }
        isFinishWorkerMultiFieldsSubscriptionError={isFinishWorkerMultiFieldsSubscriptionError}
        handleToggleSuspendWorkerModal={this.handleToggleSuspendWorkerModal}
        isSuspendWorkerModalOpen={isSuspendWorkerModalOpen}
        suspendWorkerErrorMessage={suspendWorkerErrorMessage}
        vendorsList={vendorsList}
        admins={admins}
        mentionData={mentionData}
        handleWorkerAllowedToRecharge={handleWorkerAllowedToRecharge}
        isAllowWorkerToRechargeFetching={isAllowWorkerToRechargeFetching}
        isAllowWorkerToRechargeError={isAllowWorkerToRechargeError}
        isAllowWorkerToRechargeErrorMessage={isAllowWorkerToRechargeErrorMessage}
        isAllowWorkerToRechargeSuccess={isAllowWorkerToRechargeSuccess}
        approveTheWorkerResidenceInfo={approveTheWorkerResidenceInfoAction}
      />
    );
  }
}
SingleWorkerContainer.displayName = 'SingleWorkerContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  admins: state.user.admins,
  workerDetails: state.workers.workerDetails,
  isGetWorkerDetailsFetching: state.workers.getWorkerDetails.isFetching,
  isGetWorkerDetailsError: state.workers.getWorkerDetails.isFail.isError,
  getWorkerDetailsErrorMessage: state.workers.getWorkerDetails.isFail.message,
  isGetWorkerDetailsSuccess: state.workers.getWorkerDetails.isSuccess,
  isActivateWorkerFetching: state.workers.activateWorker.isFetching,
  isActivateWorkerError: state.workers.activateWorker.isFail.isError,
  isActivateWorkerSuccess: state.workers.activateWorker.isSuccess,
  isSuspendWorkerFetching: state.workers.suspendWorker.isFetching,
  isSuspendWorkerError: state.workers.suspendWorker.isFail.isError,
  suspendWorkerErrorMessage: state.workers.suspendWorker.isFail.message,
  isSuspendWorkerSuccess: state.workers.suspendWorker.isSuccess,
  isChangeWorkerPasswordFetching: state.workers.changeWorkerPassword.isFetching,
  isChangeWorkerPasswordError: state.workers.changeWorkerPassword.isFail.isError,
  isChangeWorkerPasswordSuccess: state.workers.changeWorkerPassword.isSuccess,
  isFeatureWorkerFetching: state.workers.featureWorker.isFetching,
  isFeatureWorkerError: state.workers.featureWorker.isFail.isError,
  featureWorkerErrorMessage: state.workers.featureWorker.isFail.message,
  isFeatureWorkerSuccess: state.workers.featureWorker.isSuccess,
  isMakeWorkerEliteFetching: state.workers.makeWorkerElite.isFetching,
  isMakeWorkerEliteError: state.workers.makeWorkerElite.isFail.isError,
  isMakeWorkerEliteErrorMessage: state.workers.makeWorkerElite.isFail.message,
  isMakeWorkerEliteSuccess: state.workers.makeWorkerElite.isSuccess,
  isDeleteWorkerFetching: state.workers.deleteWorker.isFetching,
  isChangeWorkerPasswordModalOpen: state.workers.changeWorkerPasswordModal.isOpen,
  isSubscribeWorkerToAutoApplyModalOpen: state.workers.subscribeWorkerToAutoApplyModal.isOpen,
  isSubscribeWorkerToMultiFieldsModalOpen: state.workers.subscribeWorkerToMultiFieldsModal.isOpen,
  isEditWorkerModalOpen: state.workers.editWorkerModal.isOpen,
  isAddCreditModalOpen: state.workers.addCreditModal.isOpen,
  addCommentState: state.workers.addComment.addCommentState,
  addCommentErrorMessage: state.workers.addComment.error,
  workerStatistics: state.workers.workerStatistics,
  isGetWorkerStatisticsFetching: state.workers.getWorkerStatistics.isFetching,
  isGetWorkerStatisticsSuccess: state.workers.getWorkerStatistics.isSuccess,
  workerMotivationAndPunishments: state.workers.workerMotivationAndPunishments,
  isGetWorkerMotivationAndPunishmentsFetching:
    state.workers.getWorkerMotivationAndPunishments.isFetching,
  isGetWorkerMotivationAndPunishmentsSuccess:
    state.workers.getWorkerMotivationAndPunishments.isSuccess,
  isFinishWorkerAutoApplySubscriptionFetching:
    state.workers.finishWorkerAutoApplySubscription.isFetching,
  isFinishWorkerAutoApplySubscriptionError:
    state.workers.finishWorkerAutoApplySubscription.isFail.isError,
  isFinishWorkerAutoApplySubscriptionSuccess:
    state.workers.finishWorkerAutoApplySubscription.isSuccess,
  isFinishWorkerMultiFieldsSubscriptionFetching:
    state.workers.finishWorkerMultiFieldsSubscription.isFetching,
  isFinishWorkerMultiFieldsSubscriptionError:
    state.workers.finishWorkerMultiFieldsSubscription.isFail.isError,
  isFinishWorkerMultiFieldsSubscriptionSuccess:
    state.workers.finishWorkerMultiFieldsSubscription.isSuccess,
  isSuspendWorkerModalOpen: state.workers.suspendWorkerModal.isOpen,
  isConfirmModalOpened: state.workers.confirmModal.isOpen,
  vendorsList: state.vendors.vendors,
  isAllowWorkerToRechargeFetching: state.workers.allowWorkerToRecharge.isFetching,
  isAllowWorkerToRechargeSuccess: state.workers.allowWorkerToRecharge.isSuccess,
  isAllowWorkerToRechargeError: state.workers.allowWorkerToRecharge.isFail.isError,
  isAllowWorkerToRechargeErrorMessage: state.workers.allowWorkerToRecharge.isFail.message,

  isApprovingTheWorkerResidenceInfoFetching: state.workers.approveTheWorkerResidenceInfo.isFetching,
  isApprovingTheWorkerResidenceInfoError:
    state.workers.approveTheWorkerResidenceInfo.isFail.isError,
  approvingTheWorkerResidenceInfoErrorMessage:
    state.workers.approveTheWorkerResidenceInfo.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWorkerDetails,
      editWorkerReset,
      changeWorkerPasswordReset,
      addCreditReset,
      activateWorker,
      suspendWorker,
      changeWorkerPassword,
      featureWorker,
      makeWorkerElite,
      deleteWorker,
      addComment,
      toggleChangeWorkerPasswordModal,
      toggleEditWorkerModal,
      toggleAddCreditModal,
      toggleSubscribeWorkerToAutoApplyModal,
      toggleSubscribeWorkerToMultiFieldsModal,
      getWorkerStatistics,
      getWorkerMotivationAndPunishments,
      finishWorkerAutoApplySubscription,
      finishWorkerMultiFieldsSubscription,
      toggleSuspendWorkerModal,
      toggleConfirmModal,
      getAllVendors,
      getAllAdmins,
      allowWorkerToRecharge,
      approveTheWorkerResidenceInfo,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(SingleWorkerContainer);
