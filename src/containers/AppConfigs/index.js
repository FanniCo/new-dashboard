import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Box, Flex } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { faEdit, faFile } from '@fortawesome/free-solid-svg-icons';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import Text from 'components/Text';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { CardTitle, FlexContainer, StickyContainer } from 'components/shared';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { getAllAppConfigs } from 'redux-modules/appConfigs/actions';
import Button from 'components/Buttons';
import EditAppConfigsModal from 'components/EditAppConfigsModal';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class AppConfigsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    appConfigs: PropTypes.arrayOf(PropTypes.shape({})),

    getAllAppConfigs: PropTypes.func,
    isGetAllAppConfigsFetching: PropTypes.bool,
    isGetAllAppConfigsSuccess: PropTypes.bool,
    isGetAllAppConfigsError: PropTypes.bool,
    getAllAppConfigsErrorMessage: PropTypes.string,

  };

  static defaultProps = {
    user: undefined,
    appConfigs: undefined,

    getAllAppConfigs: () => {
    },
    isGetAllAppConfigsFetching: false,
    isGetAllAppConfigsSuccess: false,
    isGetAllAppConfigsError: false,
    getAllAppConfigsErrorMessage: undefined,

  };

  constructor(props) {
    super(props);

    this.state = {
      isUpdateAppConfigsModalOpen: false,
    };
  }

  componentDidMount() {
    const { getAllAppConfigs: getAllAppConfigsAction } = this.props;

    getAllAppConfigsAction();
  }

  getLoadingAppConfigs = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingAppConfigsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingAppConfigs(counter));
    }
    return list;
  };

  handleToggleEditAppConfigModal = () => {
    const { isUpdateAppConfigsModalOpen } = this.state;

    this.setState({ isUpdateAppConfigsModalOpen: !isUpdateAppConfigsModalOpen });
  };

  render() {
    const {
      user: { permissions },
      appConfigs,

      isGetAllAppConfigsFetching,
      isGetAllAppConfigsSuccess,
      isGetAllAppConfigsError,
      getAllAppConfigsErrorMessage,
    } = this.props;

    const {
      isUpdateAppConfigsModalOpen,
    } = this.state;

    let appConfigsRenderer = this.createLoadingAppConfigsList();

    if (
      (isGetAllAppConfigsFetching && (!appConfigs)) ||
      (!isGetAllAppConfigsFetching && !isGetAllAppConfigsSuccess && !isGetAllAppConfigsError)
    ) {
      appConfigsRenderer = this.createLoadingAppConfigsList();
    } else if (isGetAllAppConfigsError) {
      appConfigsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection='column'
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent='center' alignItems='center'>
                <Card width={1} minHeight={500} alignItems='center' backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize='3x'
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllAppConfigsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!appConfigs) {
      appConfigsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection='column'
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent='center' alignItems='center'>
                <Card width={1} minHeight={500} alignItems='center' backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize='3x'
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text='لا يوجد سجلات'
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      appConfigsRenderer = (
        <Box width={1}>
          <Flex flexDirection='row-reverse' flexWrap='wrap' width={1}>

            <Box key={`key`} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
              <Card
                minHeight='100px'
                flexDirection='column'
                alignItems='center'
                mx={2} mb={2}
                bgColor={COLORS_VALUES[GREY_LIGHT]}
              >

                <CardTitle px={4} py={2} width={1}>
                  <Text
                    textAlign='center'
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {'Android تطبيق'}
                  </Text>
                </CardTitle>

                <FlexContainer
                  flexDirection='row-reverse'
                  width={1} p={3}
                  alignItems='center'
                  justifyContent='center'
                >
                  <Flex alignItems='baseline' justifyContent='center' flexDirection='row-reverse'>
                    <Text
                      mx={1} color={COLORS_VALUES[BRANDING_GREEN]}
                      type={SUPER_TITLE} fontWeight={NORMAL}
                    >
                      {appConfigs.androidFanni.forceUpdate} : اصدار التحديث الإجباري
                    </Text>
                  </Flex>
                </FlexContainer>

                <FlexContainer
                  flexDirection='row-reverse'
                  width={1} p={3}
                  alignItems='center'
                  justifyContent='center'
                >
                  <Flex alignItems='baseline' justifyContent='center' flexDirection='row-reverse'>
                    <Text
                      mx={1} color={COLORS_VALUES[BRANDING_GREEN]}
                      type={SUPER_TITLE} fontWeight={NORMAL}
                    >
                      {appConfigs.androidFanni.recommendedUpdate} : اصدار التحديث الإختياري
                    </Text>
                  </Flex>
                </FlexContainer>

              </Card>
            </Box>

            <Box key={`key`} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
              <Card
                minHeight='100px'
                flexDirection='column'
                alignItems='center'
                mx={2} mb={2}
                bgColor={COLORS_VALUES[GREY_LIGHT]}
              >
                <CardTitle px={4} py={2} width={1}>
                  <Text
                    textAlign='center'
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {'IOS تطبيق'}
                  </Text>
                </CardTitle>
                <FlexContainer
                  flexDirection='row-reverse'
                  width={1} p={3}
                  alignItems='center'
                  justifyContent='center'
                >
                  <Flex alignItems='baseline' justifyContent='center' flexDirection='row-reverse'>
                    <Text
                      mx={1} color={COLORS_VALUES[BRANDING_GREEN]}
                      type={SUPER_TITLE} fontWeight={NORMAL}
                    >
                      {appConfigs.iosFanni.forceUpdate} : اصدار التحديث الإجباري
                    </Text>
                  </Flex>
                </FlexContainer>

                <FlexContainer
                  flexDirection='row-reverse'
                  width={1} p={3}
                  alignItems='center'
                  justifyContent='center'
                >
                  <Flex alignItems='baseline' justifyContent='center' flexDirection='row-reverse'>
                    <Text
                      mx={1} color={COLORS_VALUES[BRANDING_GREEN]}
                      type={SUPER_TITLE} fontWeight={NORMAL}
                    >
                      {appConfigs.iosFanni.recommendedUpdate} : اصدار التحديث الإختياري
                    </Text>
                  </Flex>
                </FlexContainer>
              </Card>
            </Box>

          </Flex>
        </Box>
      );
    }

    return (
      <Flex flexDirection='column' alignItems='flex-end' width={1}>
        <Helmet>
          <title>اعدادات النظام</title>
        </Helmet>
        {permissions && permissions.includes('Get All App Configs') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection='row-reverse'
              flexWrap='wrap'
              justifyContent='space-between'
              alignItems='center'
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                اعدادات النظام
              </Text>

              <Flex m={2}>
                {permissions && permissions.includes('Update App Configs') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={this.handleToggleEditAppConfigModal}
                    icon={faEdit}
                    iconWidth='sm'
                    xMargin={3}
                    isLoading={false}
                    reverse
                  >
                    <Text
                      cursor='pointer'
                      mx={2}
                      type={SUBHEADING}
                      color={COLORS_VALUES[DISABLED]}
                      fontWeight={NORMAL}
                    >
                      تعديل الاصدارات
                    </Text>
                  </Button>
                )}
              </Flex>

            </StickyContainer>

            {appConfigsRenderer}

            {isUpdateAppConfigsModalOpen && (
              <EditAppConfigsModal
                appConfigDetails={appConfigs}
                isUpdateAppConfigsModalOpen={isUpdateAppConfigsModalOpen}
                handleToggleEditAppConfigModal={this.handleToggleEditAppConfigModal}
              />
            )}
          </>
        )}
      </Flex>
    );
  }
}

AppConfigsContainer.displayName = 'AppConfigsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  appConfigs: state.appConfigs.appConfigs,

  isGetAllAppConfigsFetching: state.appConfigs.getAllAppConfigs.isFetching,
  isGetAllAppConfigsError: state.appConfigs.getAllAppConfigs.isFail.isError,
  getAllAppConfigsErrorMessage: state.appConfigs.getAllAppConfigs.isFail.message,
  isGetAllAppConfigsSuccess: state.appConfigs.getAllAppConfigs.isSuccess,

});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllAppConfigs,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AppConfigsContainer);
