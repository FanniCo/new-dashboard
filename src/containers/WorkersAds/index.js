import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllWorkersAds,
  toggleAddNewWorkersAdModal,
  deleteWorkersAd,
  openConfirmModal,
  openEditWorkersAdModal,
} from 'redux-modules/workersAds/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import WorkersAdsList from 'components/WorkersAds';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class WorkersAdsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    workersAdsList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreWorkersAdsAvailable: PropTypes.bool.isRequired,
    getAllWorkersAds: PropTypes.func,
    isGetAllWorkersAdsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewWorkersAdModal: PropTypes.func,
    isAddNewWorkersAdModalOpen: PropTypes.bool,
    deleteWorkersAd: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeleteWorkersAdFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditWorkersAdModal: PropTypes.func,
    isEditWorkersAdModalOpen: PropTypes.bool,
    isGetAllWorkersAdsSuccess: PropTypes.bool,
    isGetAllWorkersAdsError: PropTypes.bool,
    isGetAllWorkersAdsErrorMessage: PropTypes.string,
    isDeleteWorkersAdError: PropTypes.bool,
    isDeleteWorkersAdErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    workersAdsList: [],
    getAllWorkersAds: () => {},
    applyNewFilter: false,
    isGetAllWorkersAdsFetching: false,
    toggleAddNewWorkersAdModal: () => {},
    isAddNewWorkersAdModalOpen: false,
    deleteWorkersAd: () => {},
    openConfirmModal: () => {},
    isDeleteWorkersAdFetching: false,
    isConfirmModalOpen: false,
    openEditWorkersAdModal: () => {},
    isEditWorkersAdModalOpen: false,
    isGetAllWorkersAdsSuccess: false,
    isGetAllWorkersAdsError: false,
    isGetAllWorkersAdsErrorMessage: undefined,
    isDeleteWorkersAdError: false,
    isDeleteWorkersAdErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersAdsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      index: 0,
      workersAdDetails: {},

      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllWorkersAds: getAllWorkersAdsAction } = this.props;
    const { filterQueries } = this.state;

    getAllWorkersAdsAction(filterQueries, true);
  }

  loadMoreWorkersAds = () => {
    const { getAllWorkersAds: getAllWorkersAdsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllWorkersAdsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewWorkersAdModal = () => {
    const { toggleAddNewWorkersAdModal: toggleAddNewWorkersAdModalAction } = this.props;

    toggleAddNewWorkersAdModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { openConfirmModal: toggleConfirmModalAction, isConfirmModalOpen } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpen);
      },
    );
  };

  handleToggleEditWorkersAdModal = (index, workersAdDetails) => {
    const { openEditWorkersAdModal: openEditWorkersAdModalAction } = this.props;
    this.setState({ workersAdDetails, index });
    openEditWorkersAdModalAction();
  };

  render() {
    const {
      user,
      user: { permissions },
      workersAdsList,
      isLoadMoreWorkersAdsAvailable,
      getAllWorkersAds: getAllWorkersAdsAction,
      isGetAllWorkersAdsFetching,
      applyNewFilter,
      isAddNewWorkersAdModalOpen,
      deleteWorkersAd: deleteWorkersAdAction,
      isDeleteWorkersAdFetching,
      isConfirmModalOpen,
      isEditWorkersAdModalOpen,
      isGetAllWorkersAdsSuccess,
      isGetAllWorkersAdsError,
      isGetAllWorkersAdsErrorMessage,
      isDeleteWorkersAdError,
      isDeleteWorkersAdErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      index,
      workersAdDetails,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>اعلانات الفنين</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اعلانات الفنين
          </Text>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Add New Workers Ad') && (
              <>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleAddNewWorkersAdModal}
                  icon={faPlus}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    أضف اعلان جديد
                  </Text>
                </Button>
              </>
            )}
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="workersAds"
          filterSections={['dates']}
          creditsTitle="القيمة"
          fieldsQueryKey="fields"
          citiesQueryKey="cities"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllWorkersAdsAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        />
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <InfiniteScroll
              dataLength={workersAdsList.length}
              next={this.loadMoreWorkersAds}
              hasMore={isLoadMoreWorkersAdsAvailable}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <WorkersAdsList
                isAddNewWorkersAdModalOpen={isAddNewWorkersAdModalOpen}
                workersAdsList={workersAdsList}
                isLoadMoreWorkersAdsAvailable={isLoadMoreWorkersAdsAvailable}
                loadMoreWorkersAds={this.loadMoreWorkersAds}
                isGetAllWorkersAdsFetching={isGetAllWorkersAdsFetching}
                isGetAllWorkersAdsSuccess={isGetAllWorkersAdsSuccess}
                isGetAllWorkersAdsError={isGetAllWorkersAdsError}
                isGetAllWorkersAdsErrorMessage={isGetAllWorkersAdsErrorMessage}
                applyNewFilter={applyNewFilter}
                user={user}
                deleteWorkersAdAction={deleteWorkersAdAction}
                handleToggleConfirmModal={this.handleToggleConfirmModal}
                isConfirmModalOpen={isConfirmModalOpen}
                isDeleteWorkersAdFetching={isDeleteWorkersAdFetching}
                handleToggleEditWorkersAdModal={this.handleToggleEditWorkersAdModal}
                isEditWorkersAdModalOpen={isEditWorkersAdModalOpen}
                workersAdDetails={workersAdDetails}
                workersAdIndex={index}
                confirmModalHeader={confirmModalHeader}
                confirmModalText={confirmModalText}
                confirmModalFunction={confirmModalFunction}
                isDeleteWorkersAdError={isDeleteWorkersAdError}
                isDeleteWorkersAdErrorMessage={isDeleteWorkersAdErrorMessage}
              />
            </InfiniteScroll>
          </Card>
        </Box>
      </Flex>
    );
  }
}
WorkersAdsContainer.displayName = 'WorkersAdsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.workersAds.applyNewFilter,
  workersAdsList: state.workersAds.workersAds,
  isGetAllWorkersAdsFetching: state.workersAds.getAllWorkersAds.isFetching,
  isGetAllWorkersAdsSuccess: state.workersAds.getAllWorkersAds.isSuccess,
  isGetAllWorkersAdsError: state.workersAds.getAllWorkersAds.isFail.isError,
  isGetAllWorkersAdsErrorMessage: state.workersAds.getAllWorkersAds.isFail.message,
  isLoadMoreWorkersAdsAvailable: state.workersAds.hasMoreWorkersAds,
  isAddNewWorkersAdModalOpen: state.workersAds.addNewWorkersAdModal.isOpen,
  isDeleteWorkersAdFetching: state.workersAds.deleteWorkersAd.isFetching,
  isDeleteWorkersAdError: state.workersAds.deleteWorkersAd.isFail.isError,
  isDeleteWorkersAdErrorMessage: state.workersAds.deleteWorkersAd.isFail.message,
  isConfirmModalOpen: state.workersAds.confirmModal.isOpen,
  isEditWorkersAdModalOpen: state.workersAds.editWorkersAdModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllWorkersAds,
      toggleAddNewWorkersAdModal,
      deleteWorkersAd,
      openConfirmModal,
      openEditWorkersAdModal,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(WorkersAdsContainer);
