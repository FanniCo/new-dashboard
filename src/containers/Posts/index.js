import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllPosts,
  toggleAddNewPostModal,
  deletePost,
  openConfirmModal,
  openEditPostModal,
} from 'redux-modules/posts/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import PostsList from 'components/Posts';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class PostsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    postsList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMorePostsAvailable: PropTypes.bool.isRequired,
    getAllPosts: PropTypes.func,
    isGetAllPostsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewPostModal: PropTypes.func,
    isAddNewPostModalOpen: PropTypes.bool,
    deletePost: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeletePostFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditPostModal: PropTypes.func,
    isEditPostModalOpen: PropTypes.bool,
    isGetAllPostsSuccess: PropTypes.bool,
    isGetAllPostsError: PropTypes.bool,
    isGetAllPostsErrorMessage: PropTypes.string,
    isDeletePostError: PropTypes.bool,
    isDeletePostErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    postsList: [],
    getAllPosts: () => {},
    applyNewFilter: false,
    isGetAllPostsFetching: false,
    toggleAddNewPostModal: () => {},
    isAddNewPostModalOpen: false,
    deletePost: () => {},
    openConfirmModal: () => {},
    isDeletePostFetching: false,
    isConfirmModalOpen: false,
    openEditPostModal: () => {},
    isEditPostModalOpen: false,
    isGetAllPostsSuccess: false,
    isGetAllPostsError: false,
    isGetAllPostsErrorMessage: undefined,
    isDeletePostError: false,
    isDeletePostErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('postsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      index: 0,
      postDetails: {},

      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllPosts: getAllPostsAction } = this.props;
    const { filterQueries } = this.state;

    getAllPostsAction(filterQueries, true);
  }

  loadMorePosts = () => {
    const { getAllPosts: getAllPostsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllPostsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewPostModal = () => {
    const { toggleAddNewPostModal: toggleAddNewPostModalAction } = this.props;

    toggleAddNewPostModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { openConfirmModal: toggleConfirmModalAction, isConfirmModalOpen } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpen);
      },
    );
  };

  handleToggleEditPostModal = (index, postDetails) => {
    const { openEditPostModal: openEditPostModalAction } = this.props;
    this.setState({ postDetails, index });
    openEditPostModalAction();
  };

  render() {
    const {
      user,
      user: { permissions },
      postsList,
      isLoadMorePostsAvailable,
      getAllPosts: getAllPostsAction,
      isGetAllPostsFetching,
      applyNewFilter,
      isAddNewPostModalOpen,
      deletePost: deletePostAction,
      isDeletePostFetching,
      isConfirmModalOpen,
      isEditPostModalOpen,
      isGetAllPostsSuccess,
      isGetAllPostsError,
      isGetAllPostsErrorMessage,
      isDeletePostError,
      isDeletePostErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      index,
      postDetails,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>المقالات</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            المقالات
          </Text>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Add New Post') && (
              <>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleAddNewPostModal}
                  icon={faPlus}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    أضف مقال جديدة
                  </Text>
                </Button>
              </>
            )}
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="posts"
          filterSections={['dates']}
          creditsTitle="القيمة"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllPostsAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        />
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <InfiniteScroll
              dataLength={postsList.length}
              next={this.loadMorePosts}
              hasMore={isLoadMorePostsAvailable}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <PostsList
                isAddNewPostModalOpen={isAddNewPostModalOpen}
                postsList={postsList}
                isLoadMorePostsAvailable={isLoadMorePostsAvailable}
                loadMorePosts={this.loadMorePosts}
                isGetAllPostsFetching={isGetAllPostsFetching}
                isGetAllPostsSuccess={isGetAllPostsSuccess}
                isGetAllPostsError={isGetAllPostsError}
                isGetAllPostsErrorMessage={isGetAllPostsErrorMessage}
                applyNewFilter={applyNewFilter}
                user={user}
                deletePostAction={deletePostAction}
                handleToggleConfirmModal={this.handleToggleConfirmModal}
                isConfirmModalOpen={isConfirmModalOpen}
                isDeletePostFetching={isDeletePostFetching}
                handleToggleEditPostModal={this.handleToggleEditPostModal}
                isEditPostModalOpen={isEditPostModalOpen}
                postDetails={postDetails}
                postIndex={index}
                confirmModalHeader={confirmModalHeader}
                confirmModalText={confirmModalText}
                confirmModalFunction={confirmModalFunction}
                isDeletePostError={isDeletePostError}
                isDeletePostErrorMessage={isDeletePostErrorMessage}
              />
            </InfiniteScroll>
          </Card>
        </Box>
      </Flex>
    );
  }
}
PostsContainer.displayName = 'PostsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.posts.applyNewFilter,
  postsList: state.posts.posts,
  isGetAllPostsFetching: state.posts.getAllPosts.isFetching,
  isGetAllPostsSuccess: state.posts.getAllPosts.isSuccess,
  isGetAllPostsError: state.posts.getAllPosts.isFail.isError,
  isGetAllPostsErrorMessage: state.posts.getAllPosts.isFail.message,
  isLoadMorePostsAvailable: state.posts.hasMorePosts,
  isAddNewPostModalOpen: state.posts.addNewPostModal.isOpen,
  isDeletePostFetching: state.posts.deletePost.isFetching,
  isDeletePostError: state.posts.deletePost.isFail.isError,
  isDeletePostErrorMessage: state.posts.deletePost.isFail.message,
  isConfirmModalOpen: state.posts.confirmModal.isOpen,
  isEditPostModalOpen: state.posts.editPostModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllPosts,
      toggleAddNewPostModal,
      deletePost,
      openConfirmModal,
      openEditPostModal,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(PostsContainer);
