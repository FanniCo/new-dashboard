import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faPlus, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  getAllAdmins,
  createAdmin,
  deleteAdmin,
  changeAdminPassword,
  changeAdminPasswordReset,
  addPermissionsToUser,
  toggleAddEditAdminModal,
  toggleChangeAdminPasswordModal,
} from 'redux-modules/user/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import Admins from 'components/Admins';
import Card from 'components/Card';
import EmptyState from 'components/EmptyState';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class AdminsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    admins: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreAdmins: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    isGetAllAdminsFetching: PropTypes.bool,
    isGetAllAdminsSuccess: PropTypes.bool,
    isGetAllAdminsError: PropTypes.bool,
    deleteAdmin: PropTypes.func,
    isDeleteAdminFetching: PropTypes.bool,
    isDeleteAdminSuccess: PropTypes.bool,
    changeAdminPassword: PropTypes.func,
    changeAdminPasswordReset: PropTypes.func,
    isChangeAdminPasswordFetching: PropTypes.bool,
    isChangeAdminPasswordError: PropTypes.bool,
    isChangeAdminPasswordModalOpen: PropTypes.bool,
    toggleChangeAdminPasswordModal: PropTypes.func,
    getAllAdmins: PropTypes.func,
    addAdminReset: PropTypes.func,
    toggleAddEditAdminModal: PropTypes.func,
    isAddEditAdminModalOpen: PropTypes.bool,
    getAllAdminsErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    admins: undefined,
    hasMoreAdmins: true,
    applyNewFilter: false,
    isGetAllAdminsFetching: false,
    isGetAllAdminsSuccess: false,
    isGetAllAdminsError: false,
    deleteAdmin: () => {},
    isDeleteAdminFetching: false,
    isDeleteAdminSuccess: false,
    changeAdminPassword: () => {},
    changeAdminPasswordReset: () => {},
    isChangeAdminPasswordFetching: false,
    isChangeAdminPasswordError: false,
    isChangeAdminPasswordModalOpen: false,
    toggleChangeAdminPasswordModal: () => {},
    getAllAdmins: () => {},
    addAdminReset: () => {},
    toggleAddEditAdminModal: () => {},
    isAddEditAdminModalOpen: false,
    getAllAdminsErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('adminsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      isConfirmModalOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      activeAdminDetails: undefined,
    };
  }

  componentDidMount() {
    const { getAllAdmins: getAllAdminsAction } = this.props;
    const { filterQueries } = this.state;

    getAllAdminsAction(filterQueries, true);
  }

  componentDidUpdate(prevProps) {
    const { isDeleteAdminSuccess } = this.props;

    if (isDeleteAdminSuccess && isDeleteAdminSuccess !== prevProps.isDeleteAdminSuccess) {
      this.handleToggleConfirmModal('', '', () => {});
    }
  }

  /**
   * Creates lazy loading admins block
   */
  getLoadingAdmins = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of admins blocks
   */
  createLoadingAdminsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingAdmins(counter));
    }
    return list;
  };

  loadMoreAdmins = () => {
    const { getAllAdmins: getAllAdminsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllAdminsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleChangeAdminPasswordModal = admin => {
    const {
      toggleChangeAdminPasswordModal: toggleChangeAdminPasswordModalAction,
      changeAdminPasswordReset: changeAdminPasswordResetAction,
    } = this.props;

    this.setState({ activeAdminDetails: admin }, () => {
      changeAdminPasswordResetAction();
      toggleChangeAdminPasswordModalAction();
    });
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleAddEditAdminModal = admin => {
    const {
      toggleAddEditAdminModal: toggleAddEditAdminModalAction,
      addAdminReset: addAdminResetAction,
    } = this.props;

    this.setState({ activeAdminDetails: admin });
    addAdminResetAction();
    toggleAddEditAdminModalAction();
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user: { permissions },
      admins,
      hasMoreAdmins,
      applyNewFilter,
      isGetAllAdminsFetching,
      isGetAllAdminsSuccess,
      isGetAllAdminsError,
      deleteAdmin: deleteAdminAction,
      isDeleteAdminFetching,
      changeAdminPassword: changeAdminPasswordAction,
      isChangeAdminPasswordFetching,
      isChangeAdminPasswordError,
      isChangeAdminPasswordModalOpen,
      isAddEditAdminModalOpen,
      getAllAdminsErrorMessage,
    } = this.props;
    const {
      activeAdminDetails,
      isConfirmModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction /* , isFilterOpen, filterQueries */,
    } = this.state;
    let adminsRenderer;

    if (
      (isGetAllAdminsFetching && (!admins || applyNewFilter)) ||
      (!isGetAllAdminsFetching && !isGetAllAdminsSuccess && !isGetAllAdminsError)
    ) {
      adminsRenderer = this.createLoadingAdminsList();
    } else if (isGetAllAdminsError) {
      adminsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllAdminsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!admins.length) {
      adminsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      adminsRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={admins.length}
            next={this.loadMoreAdmins}
            hasMore={hasMoreAdmins}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <Admins
              admins={admins}
              adminDetails={activeAdminDetails}
              handleToggleConfirmModal={this.handleToggleConfirmModal}
              isConfirmModalOpen={isConfirmModalOpen}
              confirmModalHeader={confirmModalHeader}
              confirmModalText={confirmModalText}
              confirmModalFunction={confirmModalFunction}
              handleToggleAddEditAdminModal={this.handleToggleAddEditAdminModal}
              isAddEditAdminModalOpen={isAddEditAdminModalOpen}
              handleDeleteAdmin={deleteAdminAction}
              isDeleteAdminFetching={isDeleteAdminFetching}
              handleChangeAdminPassword={changeAdminPasswordAction}
              handleToggleChangeAdminPasswordModal={this.handleToggleChangeAdminPasswordModal}
              isChangeAdminPasswordModalOpen={isChangeAdminPasswordModalOpen}
              isChangeAdminPasswordFetching={isChangeAdminPasswordFetching}
              isChangeAdminPasswordError={isChangeAdminPasswordError}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>مديرين النظام</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            جميع المديرين
          </Text>
          <Flex m={2}>
            {/* <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
           </Button> */}
            {permissions && permissions.includes('Add New Admin') && (
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={() => this.handleToggleAddEditAdminModal(undefined)}
                icon={faPlus}
                iconWidth="sm"
                xMargin={3}
                isLoading={false}
                reverse
              >
                <Text
                  cursor="pointer"
                  mx={2}
                  type={SUBHEADING}
                  color={COLORS_VALUES[DISABLED]}
                  fontWeight={NORMAL}
                >
                  أضف مدير النظام
                </Text>
              </Button>
            )}
          </Flex>
        </StickyContainer>
        {/* <Filter
          cachedFilterPage="promoCodes"
          filterSections={['promoCode', 'field', 'city']}
          fieldsQueryKey="field"
          citiesQueryKey="city"
          promoCodeQueryKey="code"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllAdminsAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        /> */}
        {adminsRenderer}
      </Flex>
    );
  }
}
AdminsContainer.displayName = 'AdminsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  admins: state.user.admins,
  hasMoreAdmins: state.user.hasMoreAdmins,
  applyNewFilter: state.user.applyNewFilter,
  isGetAllAdminsFetching: state.user.getAllAdmins.isFetching,
  isGetAllAdminsError: state.user.getAllAdmins.isFail.isError,
  getAllAdminsErrorMessage: state.user.getAllAdmins.isFail.message,
  isGetAllAdminsSuccess: state.user.getAllAdmins.isSuccess,
  isAddEditAdminModalOpen: state.user.addEditAdminModal.isOpen,
  isDeleteAdminFetching: state.user.deleteAdmin.isFetching,
  isDeleteAdminSuccess: state.user.deleteAdmin.isSuccess,
  isChangeAdminPasswordFetching: state.user.changeAdminPassword.isFetching,
  isChangeAdminPasswordError: state.user.changeAdminPassword.isFail.isError,
  isChangeAdminPasswordModalOpen: state.user.changeAdminPasswordModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllAdmins,
      createAdmin,
      deleteAdmin,
      changeAdminPassword,
      changeAdminPasswordReset,
      addPermissionsToUser,
      toggleAddEditAdminModal,
      toggleChangeAdminPasswordModal,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AdminsContainer);
