import React, { Component, lazy } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Loadable from 'react-loadable';
import Cookies from 'js-cookie';
import ls from 'local-storage';
import { createGlobalStyle } from 'styled-components';
import ROUTES from 'routes';
import { fetchTransactionsClassification } from 'redux-modules/statics/actions';
import { fetchConfigs } from 'redux-modules/configs/actions';
import { loginSuccess } from 'redux-modules/user/actions';
import { USER_ROLES, COOKIES_KEYS } from 'utils/constants';
import { decodeJwtToken } from 'utils/shared/encryption';
import Layout from 'components/Layout';
import Splash from 'components/Splash';
import Loading from 'components/Loading';
import Logo from 'static/images/logo.png';
import { englishFont, arabicFont } from '../static/fonts';

import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: english-font;
    src: url(${englishFont});
  }
  // @font-face {
  //   font-family: arabic-font;
  //   src: url(${arabicFont});
  // }

  @import url('https://fonts.googleapis.com/css2?family=Tajawal:wght@300;400;700&display=swap');

  input, textarea, select, button {
    font-family: 'english-font';

    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }
  }

  body {
    font-family: 'english-font';
    background-color: ${props => props.theme.colors.primary};
    margin: 0;

    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }
  }

  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus
  textarea:-webkit-autofill,
  textarea:-webkit-autofill:hover
  textarea:-webkit-autofill:focus,
  select:-webkit-autofill,
  select:-webkit-autofill:hover,
  select:-webkit-autofill:focus {
    border: 1px solid ${props => props.theme.colors.greyDark};
    -webkit-text-fill-color: ${props => props.theme.colors.brandingOrange};
    -webkit-box-shadow: 0 0 0px 1000px ${props => props.theme.colors.greyLight} inset;
    transition: background-color 5000s ease-in-out 0s;
  }

  textarea {
    resize: none;
  }
`;


const DashboardContainer = lazy(() => import('./Dashboard'));
const LoginContainer = lazy(() => import('./Login'));
const RequestsContainer = lazy(() => import('./Requests'));
const TicketsContainer = lazy(() => import('./Tickets'));
const ClientsContainer = lazy(() => import('./Clients'));
const WorkersContainer = lazy(() => import('./Workers'));
const SingleWorkerContainer = lazy(() => import('./SingleWorker'));
const SingleRequestContainer = lazy(() => import('./SingleRequest'));
const SingleTicketContainer = lazy(() => import('./SingleTicket'));
const SingleClientContainer = lazy(() => import('./SingleClient'));
const ReviewsContainer = lazy(() => import('./Reviews'));
const NotificationsContainer = lazy(() => import('./Notifications'));
const TransactionsLogs = lazy(() => import('./TransactionsLogs'));
const RequestsPaymentsContainer = lazy(() => import('./Reports/RequestsPayments'));
const ClientsPaymentsContainer = lazy(() => import('./Reports/ClientsPayments'));
const WorkersRequestsCountContainer = lazy(() => import('./Reports/WorkersRrquestsCount'));
const WorkersTransactionsContainer = lazy(() => import('./Reports/WorkersTransactions'));
const theMostWorkersHasTicketsContainer = lazy(() => import('./Dashboard/theMostWorkersHasTickets'));
const PromoCodesContainer = lazy(() => import('./PromoCodes'));
const AdminsContainer = lazy(() => import('./Admins'));
const MessagesContainer = lazy(() => import('./Messages'));
const ActivitiesLogsContainer = lazy(() => import('./ActivitiesLogs'));
const ClientsRequestsContainer = lazy(() => import('./Reports/Clients'));
const PromoCodesReportContainer = lazy(() => import('./Reports/PromoCodes'));
const VendorsContainer = lazy(() => import('./Vendors'));
const WorkersAdsContainer = lazy(() => import('./WorkersAds'));
const CustomersAdsContainer = lazy(() => import('./CustomersAds'));
const OffersContainer = lazy(() => import('./Offers'));
const PostsContainer = lazy(() => import('./Posts'));
const CommissionsContainer = lazy(() => import('./Commissions'));
const AdminsActivitiesContainer = lazy(() => import('./Reports/AdminsActivities'));
const VouchersContainer = lazy(() => import('./Vouchers'));
const AppConfigsContainer = lazy(() => import('./AppConfigs'));
const TicketsReportContainer = lazy(() => import('./Reports/Tickets'));
const TheMonthSummaryReportContainer = lazy(() => import('./Reports/the-month-summary'));
const loyaltyPointsPartnersReportContainer = lazy(() => import('./Reports/LoyaltyPointsPartners'));
const rechargesAwardsContainer = lazy(() => import('./RechargesAwards'));


const firebaseConfig = {
  apiKey: "AIzaSyCr6qJuyOgqLtNjcbVNV09x7eMdgYPCCZI",
  authDomain: "fanniapp-bdcf4.firebaseapp.com",
  databaseURL: "https://fanniapp-bdcf4.firebaseio.com",
  projectId: "fanniapp-bdcf4",
  storageBucket: "fanniapp-bdcf4.appspot.com",
  messagingSenderId: "591978559350",
  appId: "1:591978559350:web:1ce628a34f84beb41c6c57",
  measurementId: "G-8NZWXSXHQS"
};

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

class App extends Component {
  static propTypes = {
    history: PropTypes.shape({}).isRequired,
    user: PropTypes.shape({}),
    isGetTransactionsClassificationSuccess: PropTypes.bool,
    fetchTransactionsClassification: PropTypes.func,
    isFetchConfigSuccess: PropTypes.bool,
    fetchConfigs: PropTypes.func,
    loginSuccess: PropTypes.func,
  };

  static defaultProps = {
    user: undefined,
    isGetTransactionsClassificationSuccess: false,
    fetchTransactionsClassification: () => {},
    isFetchConfigSuccess: false,
    fetchConfigs: () => {},
    loginSuccess: () => {},
  };

  state = {
    isLoading: true,
  };

  componentDidMount() {
    const {
      history,
      isGetTransactionsClassificationSuccess,
      fetchTransactionsClassification: fetchTransactionsClassificationAction,
      isFetchConfigSuccess,
      fetchConfigs: fetchConfigsAction,
      loginSuccess: loginSuccessAction,
    } = this.props;
    const { TOKEN, PLAIN_USER } = COOKIES_KEYS;
    const { ADMIN } = USER_ROLES;
    const { LOGIN, DASHBOARD } = ROUTES;
    const isUserLoggedIn = Cookies.get(TOKEN) && ls.get(PLAIN_USER);
    const token = Cookies.get(TOKEN);
    const plainUser = ls.get(PLAIN_USER);
    const {
      location: { pathname },
    } = window;

    if (isUserLoggedIn) {
      const {
        payLoad: { role },
      } = decodeJwtToken(token);
      const user = plainUser;

      loginSuccessAction(user);

      if (!isGetTransactionsClassificationSuccess) {
        fetchTransactionsClassificationAction();
      }

      if (!isFetchConfigSuccess) {
        fetchConfigsAction();
      }

      if (role !== ADMIN) {
        Cookies.remove(TOKEN);

        if (pathname !== LOGIN) {
          history.push(LOGIN);
        }

        setTimeout(() => {
          this.setState({ isLoading: false });
        }, 1500);
      } else {
        if (pathname === LOGIN) {
          history.push(DASHBOARD);
        }

        setTimeout(() => {
          this.setState({ isLoading: false });
        }, 1500);
      }
    } else if (pathname !== LOGIN) {
      history.push(LOGIN);

      setTimeout(() => {
        this.setState({ isLoading: false });
      }, 1500);
    } else {
      setTimeout(() => {
        this.setState({ isLoading: false });
      }, 1500);
    }
  }

  logout = () => {
    const { history } = this.props;
    const { TOKEN, PLAIN_USER } = COOKIES_KEYS;
    const { LOGIN } = ROUTES;

    Cookies.remove(TOKEN);
    ls.remove(PLAIN_USER);
    history.push(LOGIN);
  };

  render() {
    const { history, user } = this.props;
    const { isLoading } = this.state;
    const {
      DASHBOARD,
      REQUESTS,
      TICKETS,
      LOGIN,
      CLIENTS,
      WORKERS,
      SINGLE_WORKER,
      SINGLE_REQUEST,
      SINGLE_TICKET,
      SINGLE_CLIENT,
      REVIEWS,
      NOTIFICATIONS,
      TRANSACTIONS_LOGS,
      REPORT_REQUESTS_PAYMENTS,
      REPORT_CLIENTS_PAYMENTS,
      REPORT_WORKERS_REQUESTS_COUNT,
      REPORT_WORKERS_TRANSACTIONS,
      ADMINS,
      PROMO_CODES,
      MESSAGES,
      ACTIVITIES_LOGS,
      REPORT_CLIENTS_REQUESTS,
      REPORT_PROMO_CODES,
      VENDORS,
      WORKERS_ADS,
      CUSTOMERS_ADS,
      OFFERS,
      POSTS,
      COMMISSIONS,
      ADMINS_ACTIVITIES,
      VOUCHERS,
      APP_CONFIGS,
      TICKETS_REPORT,
      THE_MONTH_SUMMARY_REPORT,
      LOYALTY_POINTS_PARTNERS_REPORT,
      RECHARGE_AWARDS,
      THE_MOST_WORKERS_HAS_TICKETS_REPORT,
    } = ROUTES;

    let content;

    if (isLoading) {
      content = <Splash logoUrl={Logo} />;
    } else {
      content = (
        <Switch>
          <Route exact path={LOGIN} component={LoginContainer} />
          <Layout logout={this.logout} history={history} user={user}>
            <Switch>
              <Route exact path={DASHBOARD} component={DashboardContainer} />
              <Route exact path={REQUESTS} component={RequestsContainer} />
              <Route exact path={TICKETS} component={TicketsContainer} />
              <Route exact path={CLIENTS} component={ClientsContainer} />
              <Route exact path={VENDORS} component={VendorsContainer} />
              <Route exact path={WORKERS} component={WorkersContainer} />
              <Route exact path={`${SINGLE_WORKER}/:id`} component={SingleWorkerContainer} />
              <Route exact path={`${SINGLE_REQUEST}/:id`} component={SingleRequestContainer} />
              <Route exact path={`${SINGLE_TICKET}/:id`} component={SingleTicketContainer} />
              <Route exact path={`${SINGLE_CLIENT}/:id`} component={SingleClientContainer} />
              <Route exact path={REVIEWS} component={ReviewsContainer} />
              <Route exact path={NOTIFICATIONS} component={NotificationsContainer} />
              <Route exact path={TRANSACTIONS_LOGS} component={TransactionsLogs} />
              <Route exact path={REPORT_REQUESTS_PAYMENTS} component={RequestsPaymentsContainer} />
              <Route exact path={REPORT_CLIENTS_PAYMENTS} component={ClientsPaymentsContainer} />
              <Route
                exact
                path={REPORT_WORKERS_REQUESTS_COUNT}
                component={WorkersRequestsCountContainer}
              />
              <Route exact path={REPORT_CLIENTS_REQUESTS} component={ClientsRequestsContainer} />
              <Route exact path={REPORT_PROMO_CODES} component={PromoCodesReportContainer} />
              <Route
                exact
                path={REPORT_WORKERS_TRANSACTIONS}
                component={WorkersTransactionsContainer}
              />
              <Route exact path={PROMO_CODES} component={PromoCodesContainer} />
              <Route exact path={MESSAGES} component={MessagesContainer} />
              <Route exact path={ACTIVITIES_LOGS} component={ActivitiesLogsContainer} />
              <Route exact path={ADMINS} component={AdminsContainer} />
              <Route exact path={WORKERS_ADS} component={WorkersAdsContainer} />
              <Route exact path={CUSTOMERS_ADS} component={CustomersAdsContainer} />
              <Route exact path={OFFERS} component={OffersContainer} />
              <Route exact path={POSTS} component={PostsContainer} />
              <Route exact path={COMMISSIONS} component={CommissionsContainer} />
              <Route exact path={ADMINS_ACTIVITIES} component={AdminsActivitiesContainer} />
              <Route exact path={VOUCHERS} component={VouchersContainer} />
              <Route exact path={RECHARGE_AWARDS} component={rechargesAwardsContainer} />
              <Route exact path={APP_CONFIGS} component={AppConfigsContainer} />
              <Route exact path={TICKETS_REPORT} component={TicketsReportContainer} />
              <Route exact path={THE_MOST_WORKERS_HAS_TICKETS_REPORT} component={theMostWorkersHasTicketsContainer} />
              <Route
                exact
                path={THE_MONTH_SUMMARY_REPORT}
                component={TheMonthSummaryReportContainer}
              />
              <Route
                exact
                path={LOYALTY_POINTS_PARTNERS_REPORT}
                component={loyaltyPointsPartnersReportContainer}
              />
              <Redirect to={DASHBOARD} />
            </Switch>
          </Layout>
        </Switch>
      );
    }
    return (
      <>
        <GlobalStyle />
        {content}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  isFetchConfigSuccess: state.configs.fetchConfigs.isSuccess,
  isGetTransactionsClassificationSuccess: state.statics.getTransactionsClassification.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ fetchTransactionsClassification, fetchConfigs, loginSuccess }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
