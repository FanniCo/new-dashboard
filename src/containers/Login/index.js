import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Flex } from '@rebass/grid';
import { faUserTie, faKey, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { login } from 'redux-modules/user/actions';
import Card from 'components/Card';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Button from 'components/Buttons';
import { COLORS } from 'components/theme/colors';
import Logo from 'static/images/logo.png';

const Container = styled(Flex)`
  height: 100vh;
`;

const LoginFormContainer = styled.form`
  width: 100%;
  display: block;
  margin:0 auto;
  background: ${props => props.theme.colors.greyDark};
  border-radius:10px;
  padding:75px 35px;
  text-align:center;
  width:80%;
  max-width:400px;
`;

const LogoImage = styled.img`
  width: 100px;
  margin-bottom: ${props => props.theme.space[3]}px;
`;






class Login extends Component {
  static propTypes = {
    history: PropTypes.shape({
      action: PropTypes.string,
      block: PropTypes.func,
      createHref: PropTypes.func,
      go: PropTypes.func,
      goBack: PropTypes.func,
      goForward: PropTypes.func,
      length: PropTypes.number,
      listen: PropTypes.func,
      location: PropTypes.shape({
        hash: PropTypes.string,
        pathname: PropTypes.string,
        search: PropTypes.string,
      }),
      push: PropTypes.func,
      replace: PropTypes.func,
    }).isRequired,
    isLoginFetching: PropTypes.bool,
    login: PropTypes.func,
    isLoginError: PropTypes.bool,
    loginErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    isLoginFetching: false,
    login: () => {},
    isLoginError: false,
    loginErrorMessage: undefined,
  };

  state = {
    userName: '',
    password: '',
  };

  onUsernameFieldChange = value => {
    this.setState({ userName: value });
  };

  onPasswordFieldChange = value => {
    this.setState({ password: value });
  };

  onSubmitLoginForm = e => {
    const { login: loginAction, history } = this.props;
    const { userName, password } = this.state;

    e.preventDefault();

    const loginPayLoad = {
      userName,
      password,
    };
    const { push } = history;

    loginAction(loginPayLoad, push);
  };

  render() {
    const { isLoginFetching, isLoginError, loginErrorMessage } = this.props;
    const { userName, password } = this.state;

    return (
      <Container flexDirection="column" alignItems="center" justifyContent="center" className='Login_container'>
        <Helmet>
          <title>Login | Fanni Dashboard</title>
        </Helmet>
        <LogoImage src={Logo} alt="logo" />
        <Card
          className='Login_Form_container'
          style={{
            padding:0,
            borderRadius:'10px',
            background: 'transparent',
            width: '100%'
          }}
        >
          <LoginFormContainer onSubmit={this.submitLoginForm} className='Login_Form'>
            <InputField
              type="text"
              placeholder="أسم المستخدم (رقم الهاتف)"
              ref={inputField => {
                this.userNameField = inputField;
              }}
              width={1}
              icon={faUserTie}
              value={userName}
              onChange={this.onUsernameFieldChange}
              mb={2}
              autoComplete="on"
            />
            <InputField
              login="login_input"
              type="password"
              placeholder="كلمة المرور"
              ref={inputField => {
                this.passwordField = inputField;
              }}
              icon={faKey}
              value={password}
              onChange={this.onPasswordFieldChange}
              mb={isLoginError ? 2 : 5}
              autoComplete="on"
            />
            {isLoginError && (
              <Caution
                bgColor={COLORS.LIGHT_RED}
                textColor={COLORS.WHITE}
                borderColorProp={COLORS.ERROR}
                mb={isLoginError ? 3 : 2}
                className='Login_Error'
              >
                {loginErrorMessage}
              </Caution>
            )}
            <Button
              type="submit"
              primary
              color={COLORS.BRANDING_BLUE}
              style={{
                height:'45px',
                width: '90%',
                display: 'block',
                margin: 'auto',
                maxWidth: 'none',
                fontWeight:'900'
              }}
              onClick={this.onSubmitLoginForm}
              isLoading={isLoginFetching}
            >
              تسجيل الدخول
            </Button>
          </LoginFormContainer>
        </Card>
      </Container>
    );
  }
}
Login.displayName = 'Login';

const mapStateToProps = state => ({
  isLoginFetching: state.user.login.isFetching,
  isLoginError: state.user.login.isFail.isError,
  loginErrorMessage: state.user.login.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      login,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Login);
