import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllOffers,
  toggleAddNewOfferModal,
  deleteOffer,
  openConfirmModal,
  openEditOfferModal,
} from 'redux-modules/offers/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import OffersList from 'components/Offers';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class OffersContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    offersList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreOffersAvailable: PropTypes.bool.isRequired,
    getAllOffers: PropTypes.func,
    isGetAllOffersFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewOfferModal: PropTypes.func,
    isAddNewOfferModalOpen: PropTypes.bool,
    deleteOffer: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeleteOfferFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditOfferModal: PropTypes.func,
    isEditOfferModalOpen: PropTypes.bool,
    isGetAllOffersSuccess: PropTypes.bool,
    isGetAllOffersError: PropTypes.bool,
    isGetAllOffersErrorMessage: PropTypes.string,
    isDeleteOfferError: PropTypes.bool,
    isDeleteOfferErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    offersList: [],
    getAllOffers: () => {},
    applyNewFilter: false,
    isGetAllOffersFetching: false,
    toggleAddNewOfferModal: () => {},
    isAddNewOfferModalOpen: false,
    deleteOffer: () => {},
    openConfirmModal: () => {},
    isDeleteOfferFetching: false,
    isConfirmModalOpen: false,
    openEditOfferModal: () => {},
    isEditOfferModalOpen: false,
    isGetAllOffersSuccess: false,
    isGetAllOffersError: false,
    isGetAllOffersErrorMessage: undefined,
    isDeleteOfferError: false,
    isDeleteOfferErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('offersFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      index: 0,
      offerDetails: {},

      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllOffers: getAllOffersAction } = this.props;
    const { filterQueries } = this.state;

    getAllOffersAction(filterQueries, true);
  }

  loadMoreOffers = () => {
    const { getAllOffers: getAllOffersAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllOffersAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewOfferModal = () => {
    const { toggleAddNewOfferModal: toggleAddNewOfferModalAction } = this.props;

    toggleAddNewOfferModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { openConfirmModal: toggleConfirmModalAction, isConfirmModalOpen } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpen);
      },
    );
  };

  handleToggleEditOfferModal = (index, offerDetails) => {
    const { openEditOfferModal: openEditOfferModalAction } = this.props;
    this.setState({ offerDetails, index });
    openEditOfferModalAction();
  };

  render() {
    const {
      user,
      user: { permissions },
      offersList,
      isLoadMoreOffersAvailable,
      getAllOffers: getAllOffersAction,
      isGetAllOffersFetching,
      applyNewFilter,
      isAddNewOfferModalOpen,
      deleteOffer: deleteOfferAction,
      isDeleteOfferFetching,
      isConfirmModalOpen,
      isEditOfferModalOpen,
      isGetAllOffersSuccess,
      isGetAllOffersError,
      isGetAllOffersErrorMessage,
      isDeleteOfferError,
      isDeleteOfferErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      index,
      offerDetails,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>العروض</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العروض
          </Text>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Add New Offer') && (
              <>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleAddNewOfferModal}
                  icon={faPlus}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    أضف عرض جديد
                  </Text>
                </Button>
              </>
            )}
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="offers"
          filterSections={['dates', 'activeStatus']}
          creditsTitle="القيمة"
          fieldsQueryKey="fields"
          citiesQueryKey="cities"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllOffersAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        />
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <InfiniteScroll
              dataLength={offersList.length}
              next={this.loadMoreOffers}
              hasMore={isLoadMoreOffersAvailable}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <OffersList
                isAddNewOfferModalOpen={isAddNewOfferModalOpen}
                offersList={offersList}
                isLoadMoreOffersAvailable={isLoadMoreOffersAvailable}
                loadMoreOffers={this.loadMoreOffers}
                isGetAllOffersFetching={isGetAllOffersFetching}
                isGetAllOffersSuccess={isGetAllOffersSuccess}
                isGetAllOffersError={isGetAllOffersError}
                isGetAllOffersErrorMessage={isGetAllOffersErrorMessage}
                applyNewFilter={applyNewFilter}
                user={user}
                deleteOfferAction={deleteOfferAction}
                handleToggleConfirmModal={this.handleToggleConfirmModal}
                isConfirmModalOpen={isConfirmModalOpen}
                isDeleteOfferFetching={isDeleteOfferFetching}
                handleToggleEditOfferModal={this.handleToggleEditOfferModal}
                isEditOfferModalOpen={isEditOfferModalOpen}
                offerDetails={offerDetails}
                offerIndex={index}
                confirmModalHeader={confirmModalHeader}
                confirmModalText={confirmModalText}
                confirmModalFunction={confirmModalFunction}
                isDeleteOfferError={isDeleteOfferError}
                isDeleteOfferErrorMessage={isDeleteOfferErrorMessage}
              />
            </InfiniteScroll>
          </Card>
        </Box>
      </Flex>
    );
  }
}
OffersContainer.displayName = 'OffersContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.offers.applyNewFilter,
  offersList: state.offers.offers,
  isGetAllOffersFetching: state.offers.getAllOffers.isFetching,
  isGetAllOffersSuccess: state.offers.getAllOffers.isSuccess,
  isGetAllOffersError: state.offers.getAllOffers.isFail.isError,
  isGetAllOffersErrorMessage: state.offers.getAllOffers.isFail.message,
  isLoadMoreOffersAvailable: state.offers.hasMoreOffers,
  isAddNewOfferModalOpen: state.offers.addNewOfferModal.isOpen,
  isDeleteOfferFetching: state.offers.deleteOffer.isFetching,
  isDeleteOfferError: state.offers.deleteOffer.isFail.isError,
  isDeleteOfferErrorMessage: state.offers.deleteOffer.isFail.message,
  isConfirmModalOpen: state.offers.confirmModal.isOpen,
  isEditOfferModalOpen: state.offers.editOfferModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllOffers,
      toggleAddNewOfferModal,
      deleteOffer,
      openConfirmModal,
      openEditOfferModal,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(OffersContainer);
