import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faFilter, faSpinner } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { fetchTransactionsClassification } from 'redux-modules/statics/actions';
import {
  cancelAllPendingRequests,
  cancelRequest,
  connectToSocketIo,
  getAllRequests,
  getReconditioningRequestsCount,
  toggleAdminCancelRequestModal,
  toggleRequestFollowUpsModal,
} from 'redux-modules/requests/actions';
import { getAllVendors } from 'redux-modules/vendors/actions';
import { getAllAdmins } from 'redux-modules/user/actions';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer, SubTitle } from 'components/shared';
import Requests from 'components/Requests';
import Card from 'components/Card';
import styled from 'styled-components';
import { borderBottom, color } from 'styled-system';
import EmptyState from 'components/EmptyState';
import InputField from 'components/InputField';
import { getRequestsCounts } from 'redux-modules/dashboard/actions';
import Caution from 'components/Caution';
import RequestsCounts from 'components/Dashboard/RequestsCounts';
import {REQUESTS_STATUSES} from "utils/constants";

const {
  GREY_MEDIUM,
  DISABLED,
  BRANDING_GREEN,
  PRIMARY,
  GREY_DARK,
  WHITE,
  GREY_LIGHT,
  LIGHT_RED,
  ERROR,
} = COLORS;
const { SUPER_TITLE, SUBHEADING, HEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const TabsContainer = styled(Flex)`
  ${borderBottom};
`;

const Tab = styled(Flex)`
  cursor: pointer;
  width: max-content;
  ${color};
  ${borderBottom};
`;

class RequestsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),

    getAllRequests: PropTypes.func,
    hasMoreRequests: PropTypes.bool,
    requests: PropTypes.arrayOf(PropTypes.shape({})),
    isGetAllRequestsFetching: PropTypes.bool,
    isGetAllRequestsSuccess: PropTypes.bool,
    isGetAllRequestsError: PropTypes.bool,
    getAllRequestsErrorMessage: PropTypes.string,

    applyNewFilter: PropTypes.bool,

    toggleAdminCancelRequestModal: PropTypes.func,
    isCancelRequestFetching: PropTypes.bool,
    isAdminCancelRequestModalOpen: PropTypes.bool,

    isGetTransactionsClassificationSuccess: PropTypes.bool,
    fetchTransactionsClassification: PropTypes.func,

    isRequestFollowsUpModalOpen: PropTypes.bool,
    toggleRequestFollowUpsModal: PropTypes.func,
    connectToSocketIo: PropTypes.func,

    getReconditioningRequestsCount: PropTypes.func,
    reconditioningRequestsCount: PropTypes.shape({}),
    isReconditioningRequestsCountIsSuccess: PropTypes.bool,

    getRequestsCounts: PropTypes.func,
    requestsCounts: PropTypes.shape({}),
    isGetRequestsCountsFetching: PropTypes.bool,
    isGetRequestsCountsSuccess: PropTypes.bool,
    isGetRequestsCountsError: PropTypes.bool,
    getRequestsCountsErrorMessage: PropTypes.string,

    socketIoStatus: PropTypes.string,

    getAllVendors: PropTypes.func,
    vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
    getAllAdmins: PropTypes.func,

    cancelAllPendingRequests: PropTypes.func,
    isCancelAllPendingRequestsFetching: PropTypes.bool,
    isCancelAllPendingRequestsSuccess: PropTypes.bool,

    isConfirmModalHasError: PropTypes.bool,
    confirmModalErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    requests: undefined,
    hasMoreRequests: true,
    applyNewFilter: false,
    isGetAllRequestsFetching: false,
    isGetAllRequestsSuccess: false,
    isGetAllRequestsError: false,
    getAllRequests: () => {},
    isCancelRequestFetching: false,
    isGetTransactionsClassificationSuccess: false,
    fetchTransactionsClassification: () => {},
    isRequestFollowsUpModalOpen: false,
    toggleRequestFollowUpsModal: () => {},
    toggleAdminCancelRequestModal: () => {},
    connectToSocketIo: () => {},
    getReconditioningRequestsCount: () => {},
    socketIoStatus: '',
    reconditioningRequestsCount: undefined,
    isReconditioningRequestsCountIsSuccess: false,
    getRequestsCounts: () => {},
    isGetRequestsCountsFetching: false,
    isGetRequestsCountsSuccess: false,
    isGetRequestsCountsError: false,
    getRequestsCountsErrorMessage: undefined,
    requestsCounts: undefined,
    getAllRequestsErrorMessage: undefined,
    getAllVendors: () => {},
    getAllAdmins: () => {},
    vendorsList: undefined,
    cancelAllPendingRequests: () => {},
    isCancelAllPendingRequestsFetching: false,
    isCancelAllPendingRequestsSuccess: false,
    isConfirmModalHasError: false,
    confirmModalErrorMessage: undefined,
    isAdminCancelRequestModalOpen: false,
  };

  static filterSections = [
    'requestNumber',
    'clientUsername',
    'workerUsername',
    'rejectInvoiceThenApproved',
    'promoCode',
    'requestCancellationReasons',
    'field',
    'city',
    'requestsStatus',
    'groupedRequestsStatus',
    'ticketStatus',
    'delay',
    'dates',
    'requestFollowUps',
    'vendors',
    'paymentMethodChanged',
  ];

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem(`requests${0}FilterState`));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    const tabs = [
      {
        name: 'كل الطلبات',
      },
      { name: 'طلبات تحتاج المتابعة', searchCriteria: { types: '', statuses: REQUESTS_STATUSES.canceled.en.toLowerCase() } },
      { name: 'طلبات خدمة ترميم', searchCriteria: { types: 'reconditioning', statuses: '' } },
      { name: 'احصائيات' },
    ];
    let activeTab = 0;

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      filterQueries = Object.assign({}, filterQueries, cachedFilterQueries);

      tabs.forEach((tab, index) => {
        if (tab.searchCriteria && tab.searchCriteria.types === filterQueries.types && tab.searchCriteria.statuses === filterQueries.statuses) {
          activeTab = index;
        }
      });
    }

    let filterSections = RequestsContainer.filterSections;

    if (activeTab === 2) {
      filterSections = RequestsContainer.filterSections.filter(e => e !== 'field');
    }

    if (activeTab === 3) {
      filterSections = ['dates'];
    }

    this.state = {
      isConfirmModalOpen: false,
      isOpsCommentsModalOpen: false,
      isFilterOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      activeRequestModalIndex: undefined,
      activeRequest: undefined,
      tabs,
      activeTab,
      filterSections,
      fieldsQueryValues: [],
      cachedFilterPage: `requests${0}`,
    };
  }

  componentDidMount() {
    const {
      user,
      getAllRequests: getAllRequestsAction,
      isGetTransactionsClassificationSuccess,
      fetchTransactionsClassification: fetchTransactionsClassificationAction,
      connectToSocketIo: connectToSocketIoAction,
      getReconditioningRequestsCount: getReconditioningRequestsCountAction,
      getRequestsCounts: getRequestsCountsAction,
      getAllVendors: getAllVendorsAction,
      getAllAdmins: getAllAdminsAction,
    } = this.props;
    const { filterQueries } = this.state;
    getAllRequestsAction(filterQueries, true);

    if (!isGetTransactionsClassificationSuccess) {
      fetchTransactionsClassificationAction();
    }

    connectToSocketIoAction(user.username);

    const newFilterQueries = JSON.parse(JSON.stringify(filterQueries));

    if (!filterQueries.fromDate) {
      const today = new Date();
      const dd = String(today.getDate()).padStart(2, '0');
      const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
      const yyyy = today.getFullYear();
      newFilterQueries.fromDate = `${mm}/${dd}/${yyyy}`;
    }

    getRequestsCountsAction(newFilterQueries, true);
    getReconditioningRequestsCountAction();
    getAllVendorsAction({ skip: 0, limit: 5000 }, true);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  componentDidUpdate(prevProps, prevState) {
    const { isCancelAllPendingRequestsSuccess, requests } = this.props;
    const { activeRequest } = this.state;

    if (
      isCancelAllPendingRequestsSuccess &&
      isCancelAllPendingRequestsSuccess !== prevProps.isCancelAllPendingRequestsSuccess
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }

    if (
      requests &&
      requests.length &&
      !_.isEmpty(activeRequest) &&
      !_.isEqual(
        prevState.activeRequest,
        requests.find(request => request._id === activeRequest._id),
      )
    ) {
      const request = requests.find(request => request._id === activeRequest._id);

      this.setState({ activeRequest: request });
    }
  }

  /**
   * Creates lazy loading request block
   */
  getLoadingRequest = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests blocks
   */
  createLoadingRequestsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingRequest(counter));
    }
    return list;
  };

  loadMoreRequests = () => {
    const { getAllRequests: getAllRequestsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllRequestsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleOpsCommentsModal = index => {
    const { isOpsCommentsModalOpen } = this.state;
    const { requests } = this.props;

    this.setState(
      {
        activeRequestModalIndex: index,
        activeRequest: requests[index],
      },
      () => {
        this.setState({
          isOpsCommentsModalOpen: !isOpsCommentsModalOpen,
        });
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleRequestFollowsUpModal = index => {
    const { toggleRequestFollowUpsModal: toggleRequestFollowUpsModalAction, requests } = this.props;
    this.setState(
      {
        activeRequestModalIndex: index,
        activeRequest: requests[index],
      },
      () => {
        toggleRequestFollowUpsModalAction();
      },
    );
  };

  handleTabChange = value => {
    const { activeTab, tabs } = this.state;
    const {
      getAllRequests: getAllRequestsAction,
      getRequestsCounts: getRequestsCountsAction,
    } = this.props;
    const filters = JSON.parse(sessionStorage.getItem(`requests${value}FilterState`)) || {};

    if (activeTab !== value) {
      let newFilterSections = RequestsContainer.filterSections;

      if (value === 1) {
        newFilterSections = newFilterSections.filter(e => ![
          'rejectInvoiceThenApproved',
          'requestsStatus',
          'paymentMethodChanged',
          'promoCode',
          'groupedRequestsStatus',
        ].includes(e));
      }

      if (value === 2) {
        newFilterSections = newFilterSections.filter(e => e !== 'field');
      }

      if (value === 3) {
        newFilterSections = ['dates'];
      }

      let newFilterQueries = {
        types: tabs[value].searchCriteria ? tabs[value].searchCriteria.types : '',
        skip: 0,
        limit: 20,
      };

      if(value === 1) {
        newFilterQueries = {
          ...newFilterQueries,
          needsFollow: 30,
          statuses: tabs[value].searchCriteria ? tabs[value].searchCriteria.statuses : ''
        }
      }
      sessionStorage.setItem(
        `requests${value}FilterState`,
        JSON.stringify({ ...filters, filterQueries: { ...newFilterQueries, ...filters.filterQueries } }),
      );
      this.setState(
        {
          activeTab: value,
          filterQueries: { ...newFilterQueries, ...filters.filterQueries },
          filterSections: newFilterSections,
          fieldsQueryValues: [],
          cachedFilterPage: `requests${value}`,
        },
        () => {
          if (value !== 2) {
            getAllRequestsAction({ ...newFilterQueries, ...filters.filterQueries }, true);
          }

          const filtersQuery = JSON.parse(
            JSON.stringify({  ...newFilterQueries, ...filters.filterQueries }),
          );

          if (!newFilterQueries.fromDate) {
            const today = new Date();
            const dd = String(today.getDate()).padStart(2, '0');
            const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
            const yyyy = today.getFullYear();
            filtersQuery.fromDate = `${mm}/${dd}/${yyyy}`;
          }
          getRequestsCountsAction(filtersQuery, true);
        },
      );
    }
  };

  countCancelRequest = cancelRequestsTypes => {
    let counter = 0;
    Object.values(cancelRequestsTypes).forEach(value => {
      counter += value;
    });
    return counter;
  };

  

  handleToggleAdminCancelRequestModal = index => {
    const {
      toggleAdminCancelRequestModal: toggleAdminCancelRequestModalAction,
      requests,
    } = this.props;

    this.setState(
      {
        activeRequestModalIndex: index,
        activeRequest: requests[index],
      },
      () => {
        toggleAdminCancelRequestModalAction();
      },
    );
  };

  render() {
    const {
      user,
      user: { permissions },
      requests,
      hasMoreRequests,
      applyNewFilter,
      isGetAllRequestsFetching,
      isGetAllRequestsSuccess,
      isGetAllRequestsError,
      getAllRequests: getAllRequestsAction,
      getRequestsCounts: getRequestsCountsAction,
      isCancelRequestFetching,
      isRequestFollowsUpModalOpen,
      socketIoStatus,
      reconditioningRequestsCount,
      isReconditioningRequestsCountIsSuccess,
      isGetRequestsCountsFetching,
      isGetRequestsCountsSuccess,
      isGetRequestsCountsError,
      getRequestsCountsErrorMessage,
      requestsCounts,
      getAllRequestsErrorMessage,
      vendorsList,
      cancelAllPendingRequests: cancelAllPendingRequestsAction,
      isCancelAllPendingRequestsFetching,
      isConfirmModalHasError,
      confirmModalErrorMessage,
      isAdminCancelRequestModalOpen,
    } = this.props;
    const {
      isFilterOpen,
      isConfirmModalOpen,
      isOpsCommentsModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      filterQueries,
      activeRequestModalIndex,
      activeRequest,
      tabs,
      activeTab,
      filterSections,
      fieldsQueryValues,
      cachedFilterPage,
    } = this.state;

    let requestsRenderer;
    let requestsStatisticsRenderer;

    if (
      (isGetAllRequestsFetching && (!requests || applyNewFilter)) ||
      (!isGetAllRequestsFetching && !isGetAllRequestsSuccess && !isGetAllRequestsError)
    ) {
      requestsRenderer = this.createLoadingRequestsList();
    } else if (isGetAllRequestsError) {
      requestsRenderer = (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text={getAllRequestsErrorMessage}
            />
          </Card>
        </Flex>
      );
    } else if (requests.length) {
      requestsRenderer = (
        <InfiniteScroll
          dataLength={requests.length}
          next={this.loadMoreRequests}
          hasMore={hasMoreRequests}
          loader={
            <Flex m={2} justifyContent="center" alignItems="center">
              <FontAwesomeIcon
                icon={faSpinner}
                size="lg"
                spin
                color={COLORS_VALUES[BRANDING_GREEN]}
              />
            </Flex>
          }
        >
          <Requests
            user={user}
            requests={requests}
            isConfirmModalOpen={isConfirmModalOpen}
            confirmModalHeader={confirmModalHeader}
            confirmModalText={confirmModalText}
            confirmModalFunction={confirmModalFunction}
            handleToggleConfirmModal={this.handleToggleConfirmModal}
            isConfirmModalHasError={isConfirmModalHasError}
            confirmModalErrorMessage={confirmModalErrorMessage}
            isCancelRequestFetching={isCancelRequestFetching}
            isOpsCommentsModalOpen={isOpsCommentsModalOpen}
            handleToggleOpsCommentsModal={this.handleToggleOpsCommentsModal}
            activeRequestModalIndex={activeRequestModalIndex}
            activeRequest={activeRequest}
            isRequestFollowsUpModalOpen={isRequestFollowsUpModalOpen}
            handleToggleRequestFollowsUpModal={this.handleToggleRequestFollowsUpModal}
            isCancelAllPendingRequestsFetching={isCancelAllPendingRequestsFetching}
            isAdminCancelRequestModalOpen={isAdminCancelRequestModalOpen}
            handleToggleAdminCancelRequestModal={this.handleToggleAdminCancelRequestModal}
          />
        </InfiniteScroll>
      );
    } else {
      requestsRenderer = (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text="لا يوجد سجلات"
            />
          </Card>
        </Flex>
      );
    }

    if (
      (isGetRequestsCountsFetching && (!requests || applyNewFilter)) ||
      (!isGetRequestsCountsFetching && !isGetRequestsCountsSuccess && !isGetRequestsCountsError)
    ) {
      requestsStatisticsRenderer = this.createLoadingRequestsList();
    } else if (isGetRequestsCountsError) {
      requestsStatisticsRenderer = (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text={getRequestsCountsErrorMessage}
            />
          </Card>
        </Flex>
      );
    } else {
      requestsStatisticsRenderer = (
        <Flex flexDirection="column" alignItems="flex-end" width={1}>
          <Flex
            width={1}
            py={2}
            flexDirection="row-reverse"
            flexWrap="wrap"
            justifyContent="space-between"
            alignItems="center"
          >
            <Box width={1}>
              <RequestsCounts requestsCounts={requestsCounts} />
            </Box>
          </Flex>
        </Flex>
      );
    }
    const ServerConected =  socketIoStatus == 'متصل بالسرفر' ? 'ServerConected' : '';
    const ServerNotConected =  socketIoStatus == 'غير قادر على الاتصال بالسرفر' ? 'ServerNotConected' : '';
    const ServerConectting =  socketIoStatus == 'جاري الاتصال بالسرفر' ? 'ServerConectting' : '';
    
    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>الطلبات</title>
        </Helmet>
        {permissions && permissions.includes('Get All Requests') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
              className='RequestsSticky'
            >
              
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} className='TopTitle'>
                الطلبات
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                <SubTitle className={
                  `${ServerConected} ${ServerNotConected} ${ServerConectting}`
                }>
                  {' '}
                  {socketIoStatus}
                </SubTitle>
              </Text>
              {isGetRequestsCountsSuccess && (
                <Flex flexDirection="row-reverse" px={3} className='RequestsCountsContainer'>
                  <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text className="TopLabel">
                  المكتمل    
                    </Text>
                    <span className="CountValue">
                      {this.countCancelRequest(requestsCounts.done + requestsCounts.reviewed)} 
                      <span className="CountValueText">
                      طلب
                      </span>
                    </span>
                    
                  </Flex>

                  <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text className="TopLabel">
                  الملغى    
                    </Text>
                    <span className="CountValue">
                      {this.countCancelRequest(requestsCounts.canceled)} 
                      <span className="CountValueText">
                      طلب
                      </span>
                    </span>
                    
                  </Flex>

                  <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text className="TopLabel">
                  النشط    
                    </Text>
                    <span className="CountValue">
                      {requestsCounts.active} 
                      <span className="CountValueText">
                      طلب
                      </span>
                    </span>
                   
                  </Flex>

                  <Flex flexDirection="column" alignItems="center" justifyContent="center">
                  <Text className="TopLabel">
                      المؤجل    
                    </Text>
                    <span className="CountValue">
                      {requestsCounts.postponed} 
                      <span className="CountValueText">
                      طلب
                      </span>
                    </span>
                  </Flex>

                  <Flex flexDirection="column" alignItems="center" justifyContent="center">
                    <Text className="TopLabel">
                      في الانتظار
                    </Text>
                    <span className="CountValue">
                      {requestsCounts.pending} 
                      <span className="CountValueText">
                      طلب
                      </span>
                    </span>
                  </Flex>
                </Flex>
              )}
              {isGetRequestsCountsError && (
                <Flex flexDirection="row-reverse" px={3} className='ErrorMessageContainer'>
                  <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                    {getRequestsCountsErrorMessage}
                  </Caution>
                </Flex>
              )}
              <Flex flexDirection="row-reverse" px={3} className='RequestsControllers'>
                {permissions && permissions.includes('Cancel All Pending Requests') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={LIGHT_RED}
                    onClick={() =>
                      this.handleToggleConfirmModal(
                        'تأكيد الغاء كل الطلبات لم يتم اعتمادها',
                        'هل أنت متأكد أنك تريد إلغاء كل الطلبات لم يتم اعتمادها؟',
                        () => {
                          cancelAllPendingRequestsAction();
                        },
                      )
                    }
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={false}
                  >
                    <Text
                      mx={2}
                      type={SUBHEADING}
                      color={COLORS_VALUES[DISABLED]}
                      fontWeight={NORMAL}
                    >
                      الغاء كل الطلبات لم يتم اعتمادها
                    </Text>
                  </Button>
                )}
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                >
                  <Text
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage={cachedFilterPage}
              filterSections={filterSections}
              clientUsernameQueryKey="clientUsername"
              workerUsernameQueryKey="workerUsername"
              requestCancellationReasonsQueryKey="requestCancellationReasons"
              fieldsQueryKey="types"
              citiesQueryKey="cities"
              selectedFields={fieldsQueryValues}
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={(filtersState) => {
                getAllRequestsAction(filtersState, true);
                getRequestsCountsAction(filtersState, true);
              }}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
              vendorsList={vendorsList}
            />
            <Box width={1}>
              <Box width={[1, 1, 1, 1, 1]}>
                <TabsContainer flexDirection="row-reverse" alignItems="center" className='RequestsTabsContainer'>
                  {tabs.map((tab, index) => (
                    <Tab
                      key={tab.name}
                      flexDirection="row-reverse"
                      alignItems="center"
                      px={5}
                      py={3}
                      className={
                        activeTab === index ? 'active' : ''
                      }
                      onClick={() => this.handleTabChange(index, tab.drawMap)}
                    >
                      <Text
                        cursor="pointer"
                        textAlign="center"
                        color={
                          activeTab === index
                            ? COLORS_VALUES[BRANDING_GREEN]
                            : COLORS_VALUES[DISABLED]
                        }
                        type={HEADING}
                        fontWeight={NORMAL}
                        mx={3}
                      >
                        {tab.name}
                      </Text>
                      {tab.name === 'طلبات خدمة ترميم' && isReconditioningRequestsCountIsSuccess && (
                        <Box
                          display="inline-block"
                          p="1"
                          color="white"
                          bg="primary"
                          borderRadius="9999"
                          border
                        >
                          {reconditioningRequestsCount.reconditioning}
                        </Box>
                      )}
                    </Tab>
                  ))}
                </TabsContainer>
                <Card
                  minHeight={500}
                  ml={[0, 0, 0, 0, 0]}
                  mb={3}
                  flexDirection="column"
                  bgColor={COLORS_VALUES[GREY_DARK]}
                 className="CardsContainer">
                  {activeTab === 0 && <>{requestsRenderer}</>}
                  {activeTab === 1 && <>{requestsRenderer}</>}
                  {activeTab === 2 && <>{requestsRenderer}</>}
                  {activeTab === 3 && <>{requestsStatisticsRenderer}</>}
                </Card>
              </Box>
            </Box>
          </>
        )}
      </Flex>
    );
  }
}

RequestsContainer.displayName = 'RequestsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  requests: state.requests.requests,
  hasMoreRequests: state.requests.hasMoreRequests,
  applyNewFilter: state.requests.applyNewFilter,
  isGetTransactionsClassificationSuccess: state.statics.getTransactionsClassification.isSuccess,
  isGetAllRequestsFetching: state.requests.getAllRequests.isFetching,
  isGetAllRequestsError: state.requests.getAllRequests.isFail.isError,
  getAllRequestsErrorMessage: state.requests.getAllRequests.isFail.message,
  isGetAllRequestsSuccess: state.requests.getAllRequests.isSuccess,
  isCancelRequestFetching: state.requests.cancelRequest.isFetching,
  isCancelRequestSuccess: state.requests.cancelRequest.isSuccess,
  isConfirmModalOpen: state.requests.confirmModal.isOpen,
  isOpsCommentsModalOpen: state.requests.opsCommentsModal.isOpen,
  isRequestFollowsUpModalOpen: state.requests.requestFollowUpsModal.isOpen,
  socketIoStatus: state.requests.socketIoStatus,
  reconditioningRequestsCount: state.requests.reconditioningRequestsCount,
  isReconditioningRequestsCountIsSuccess: state.requests.getReconditioningRequestsCount.isSuccess,
  isGetRequestsCountsFetching: state.dashboard.getRequestsCounts.isFetching,
  isGetRequestsCountsSuccess: state.dashboard.getRequestsCounts.isSuccess,
  isGetRequestsCountsError: state.dashboard.getRequestsCounts.isFail.isError,
  getRequestsCountsErrormessage: state.dashboard.getRequestsCounts.isFail.message,
  getRequestsCountsErrorMessage: state.dashboard.getRequestsCounts.isFail.message,
  requestsCounts: state.dashboard.requestsCounts,
  vendorsList: state.vendors.vendors,
  adminsList: state.user.admins,
  isCancelAllPendingRequestsFetching: state.requests.cancelAllPendingRequests.isFetching,
  isCancelAllPendingRequestsSuccess: state.requests.cancelAllPendingRequests.isSuccess,
  isConfirmModalHasError: state.requests.cancelAllPendingRequests.isFail.isError,
  confirmModalErrorMessage: state.requests.cancelAllPendingRequests.isFail.message,
  isAdminCancelRequestModalOpen: state.requests.cancelRequest.isModalOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchTransactionsClassification,
      getAllRequests,
      cancelRequest,
      toggleRequestFollowUpsModal,
      connectToSocketIo,
      getReconditioningRequestsCount,
      getRequestsCounts,
      getAllVendors,
      getAllAdmins,
      cancelAllPendingRequests,
      toggleAdminCancelRequestModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(RequestsContainer);
