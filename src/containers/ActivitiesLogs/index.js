import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFilter, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { getAllActivitiesLogs } from 'redux-modules/activitiesLogs/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import ActivitiesLogs from 'components/ActivitiesLogs';
import Card from 'components/Card';
import EmptyState from 'components/EmptyState';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class WorkersContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    isGetAllActivitiesLogsFetching: PropTypes.bool,
    isGetAllActivitiesLogsSuccess: PropTypes.bool,
    isGetAllActivitiesLogsError: PropTypes.bool,
    activitiesLogs: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreActivitiesLogs: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    getAllActivitiesLogs: PropTypes.func,
    getAllActivitiesLogsErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    activitiesLogs: undefined,
    hasMoreActivitiesLogs: true,
    applyNewFilter: false,
    isGetAllActivitiesLogsFetching: false,
    isGetAllActivitiesLogsSuccess: false,
    isGetAllActivitiesLogsError: false,
    getAllActivitiesLogs: () => {},
    getAllActivitiesLogsErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('activitiesLogsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getAllActivitiesLogs: getAllActivitiesLogsAction } = this.props;
    const { filterQueries } = this.state;

    getAllActivitiesLogsAction(filterQueries, true);
  }

  /**
   * Creates lazy loading request block
   */
  getLoadingActivitiesLogs = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingActivitiesLogsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingActivitiesLogs(counter));
    }
    return list;
  };

  loadMoreActivitiesLogs = () => {
    const { getAllActivitiesLogs: getAllActivitiesLogsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllActivitiesLogsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user,
      user: { permissions },
      activitiesLogs,
      hasMoreActivitiesLogs,
      isGetAllActivitiesLogsFetching,
      isGetAllActivitiesLogsSuccess,
      isGetAllActivitiesLogsError,
      applyNewFilter,
      getAllActivitiesLogs: getAllActivityLogsAction,
      getAllActivitiesLogsErrorMessage,
    } = this.props;
    const { isFilterOpen, filterQueries } = this.state;
    let activityLogsRenderer;

    if (
      (isGetAllActivitiesLogsFetching && (!activitiesLogs || applyNewFilter)) ||
      (!isGetAllActivitiesLogsFetching &&
        !isGetAllActivitiesLogsSuccess &&
        !isGetAllActivitiesLogsError)
    ) {
      activityLogsRenderer = this.createLoadingActivitiesLogsList();
    } else if (isGetAllActivitiesLogsError) {
      activityLogsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllActivitiesLogsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!activitiesLogs.length) {
      activityLogsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      activityLogsRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={activitiesLogs.length}
            next={this.loadMoreActivitiesLogs}
            hasMore={hasMoreActivitiesLogs}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <ActivitiesLogs user={user} activitiesLogs={activitiesLogs} />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>النشاطات</title>
        </Helmet>
        {permissions && permissions.includes('Get Activity Logs') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                النشاطات
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="activitiesLogs"
              filterSections={['dates', 'activitiesLogs', 'adminUsername']}
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllActivityLogsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {activityLogsRenderer}
          </>
        )}
      </Flex>
    );
  }
}
WorkersContainer.displayName = 'WorkersContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  activitiesLogs: state.activitiesLogs.activitiesLogs,
  hasMoreActivitiesLogs: state.activitiesLogs.hasMoreActivitiesLogs,
  applyNewFilter: state.activitiesLogs.applyNewFilter,
  isGetAllActivitiesLogsFetching: state.activitiesLogs.getAllActivitiesLogs.isFetching,
  isGetAllActivitiesLogsError: state.activitiesLogs.getAllActivitiesLogs.isFail.isError,
  getAllActivitiesLogsErrorMessage: state.activitiesLogs.getAllActivitiesLogs.isFail.message,
  isGetAllActivitiesLogsSuccess: state.activitiesLogs.getAllActivitiesLogs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActivitiesLogs,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WorkersContainer);
