import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllCommissions,
  toggleAddNewCommissionModal,
  deleteCommission,
  openConfirmModal,
  openEditCommissionModal,
} from 'redux-modules/commissions/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import CommissionsList from 'components/Commissions';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class CommissionsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    commissionsList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreCommissionsAvailable: PropTypes.bool.isRequired,
    getAllCommissions: PropTypes.func,
    isGetAllCommissionsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewCommissionModal: PropTypes.func,
    isAddNewCommissionModalOpen: PropTypes.bool,
    deleteCommission: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeleteCommissionFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditCommissionModal: PropTypes.func,
    isEditCommissionModalOpen: PropTypes.bool,
    isGetAllCommissionsSuccess: PropTypes.bool,
    isGetAllCommissionsError: PropTypes.bool,
    isGetAllCommissionsErrorMessage: PropTypes.string,
    isDeleteCommissionError: PropTypes.bool,
    isDeleteCommissionErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    commissionsList: [],
    getAllCommissions: () => {},
    applyNewFilter: false,
    isGetAllCommissionsFetching: false,
    toggleAddNewCommissionModal: () => {},
    isAddNewCommissionModalOpen: false,
    deleteCommission: () => {},
    openConfirmModal: () => {},
    isDeleteCommissionFetching: false,
    isConfirmModalOpen: false,
    openEditCommissionModal: () => {},
    isEditCommissionModalOpen: false,
    isGetAllCommissionsSuccess: false,
    isGetAllCommissionsError: false,
    isGetAllCommissionsErrorMessage: undefined,
    isDeleteCommissionError: false,
    isDeleteCommissionErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('commissionsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      index: 0,
      commissionDetails: {},

      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllCommissions: getAllCommissionsAction } = this.props;
    const { filterQueries } = this.state;

    getAllCommissionsAction(filterQueries, true);
  }

  loadMoreCommissions = () => {
    const { getAllCommissions: getAllCommissionsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllCommissionsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewCommissionModal = () => {
    const { toggleAddNewCommissionModal: toggleAddNewCommissionModalAction } = this.props;

    toggleAddNewCommissionModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { openConfirmModal: toggleConfirmModalAction, isConfirmModalOpen } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpen);
      },
    );
  };

  handleToggleEditCommissionModal = (index, commissionDetails) => {
    const { openEditCommissionModal: openEditCommissionModalAction } = this.props;
    this.setState({ commissionDetails, index });
    openEditCommissionModalAction();
  };

  render() {
    const {
      user,
      user: { permissions },
      commissionsList,
      isLoadMoreCommissionsAvailable,
      getAllCommissions: getAllCommissionsAction,
      isGetAllCommissionsFetching,
      applyNewFilter,
      isAddNewCommissionModalOpen,
      deleteCommission: deleteCommissionAction,
      isDeleteCommissionFetching,
      isConfirmModalOpen,
      isEditCommissionModalOpen,
      isGetAllCommissionsSuccess,
      isGetAllCommissionsError,
      isGetAllCommissionsErrorMessage,
      isDeleteCommissionError,
      isDeleteCommissionErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      index,
      commissionDetails,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>معدلات العموله</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            معدلات العموله
          </Text>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Add New Commission') && (
              <>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleAddNewCommissionModal}
                  icon={faPlus}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    أضف معدل عمولة جديدة
                  </Text>
                </Button>
              </>
            )}
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="commissions"
          filterSections={['dates']}
          creditsTitle="القيمة"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllCommissionsAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        />
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <InfiniteScroll
              dataLength={commissionsList.length}
              next={this.loadMoreCommissions}
              hasMore={isLoadMoreCommissionsAvailable}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <CommissionsList
                isAddNewCommissionModalOpen={isAddNewCommissionModalOpen}
                commissionsList={commissionsList}
                isLoadMoreCommissionsAvailable={isLoadMoreCommissionsAvailable}
                loadMoreCommissions={this.loadMoreCommissions}
                isGetAllCommissionsFetching={isGetAllCommissionsFetching}
                isGetAllCommissionsSuccess={isGetAllCommissionsSuccess}
                isGetAllCommissionsError={isGetAllCommissionsError}
                isGetAllCommissionsErrorMessage={isGetAllCommissionsErrorMessage}
                applyNewFilter={applyNewFilter}
                user={user}
                deleteCommissionAction={deleteCommissionAction}
                handleToggleConfirmModal={this.handleToggleConfirmModal}
                isConfirmModalOpen={isConfirmModalOpen}
                isDeleteCommissionFetching={isDeleteCommissionFetching}
                handleToggleEditCommissionModal={this.handleToggleEditCommissionModal}
                isEditCommissionModalOpen={isEditCommissionModalOpen}
                commissionDetails={commissionDetails}
                commissionIndex={index}
                confirmModalHeader={confirmModalHeader}
                confirmModalText={confirmModalText}
                confirmModalFunction={confirmModalFunction}
                isDeleteCommissionError={isDeleteCommissionError}
                isDeleteCommissionErrorMessage={isDeleteCommissionErrorMessage}
              />
            </InfiniteScroll>
          </Card>
        </Box>
      </Flex>
    );
  }
}
CommissionsContainer.displayName = 'CommissionsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.commissions.applyNewFilter,
  commissionsList: state.commissions.commissions,
  isGetAllCommissionsFetching: state.commissions.getAllCommissions.isFetching,
  isGetAllCommissionsSuccess: state.commissions.getAllCommissions.isSuccess,
  isGetAllCommissionsError: state.commissions.getAllCommissions.isFail.isError,
  isGetAllCommissionsErrorMessage: state.commissions.getAllCommissions.isFail.message,
  isLoadMoreCommissionsAvailable: state.commissions.hasMoreCommissions,
  isAddNewCommissionModalOpen: state.commissions.addNewCommissionModal.isOpen,
  isDeleteCommissionFetching: state.commissions.deleteCommission.isFetching,
  isDeleteCommissionError: state.commissions.deleteCommission.isFail.isError,
  isDeleteCommissionErrorMessage: state.commissions.deleteCommission.isFail.message,
  isConfirmModalOpen: state.commissions.confirmModal.isOpen,
  isEditCommissionModalOpen: state.commissions.editCommissionModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllCommissions,
      toggleAddNewCommissionModal,
      deleteCommission,
      openConfirmModal,
      openEditCommissionModal,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CommissionsContainer);
