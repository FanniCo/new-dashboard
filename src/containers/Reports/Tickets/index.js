import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { exportFile } from 'redux-modules/user/actions';
import { getTicketsReport } from 'redux-modules/tickets/actions';
import { Urls } from 'utils/api';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import Tickets from 'components/Reports/Tickets';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class TicketsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    ticketsList: PropTypes.arrayOf(PropTypes.shape({})),
    ticketsListState: PropTypes.shape({}),
    isLoadMoreTicketsAvailable: PropTypes.bool,
    getTicketsReport: PropTypes.func,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    isGetTicketsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    ticketsList: undefined,
    ticketsListState: undefined,
    isLoadMoreTicketsAvailable: true,
    getTicketsReport: () => {},
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    isGetTicketsFetching: false,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFilterOpen: false,
      filterQueries: {
        skip: 0,
        limit: 20,
      },
    };
  }

  componentDidMount() {
    const { getTicketsReport: getTicketsReportAction } = this.props;
    const { filterQueries } = this.state;

    getTicketsReportAction(filterQueries, true);
  }

  loadMoreTickets = () => {
    const { getTicketsReport: getTicketsReportAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getTicketsReportAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      applyNewFilter,
      ticketsList,
      ticketsListState,
      isLoadMoreTicketsAvailable,
      exportFile: exportFileAction,
      isExportFileFetching,
      isExportFileSuccess,
      isGetTicketsFetching,
    } = this.props;
    const {
      reports: { exportTicketsReport },
    } = Urls;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير الشكاوىء</title>
        </Helmet>
        {/* {permissions && permissions.includes('Get Tickets Report') && ( */}
        <>
          <StickyContainer
            width={1}
            py={2}
            flexDirection="row-reverse"
            flexWrap="wrap"
            justifyContent="space-between"
            alignItems="center"
            bgColor={COLORS_VALUES[GREY_MEDIUM]}
          >
            <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              تقرير الشكاوى
            </Text>
            {/* {permissions && permissions.includes('Download Tickets Report') && ( */}
            <Flex m={2}>
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={() => exportFileAction(exportTicketsReport)}
                icon={faDownload}
                iconWidth="sm"
                xMargin={3}
                isLoading={isExportFileFetching && !isExportFileSuccess}
                disable={isExportFileFetching && !isExportFileSuccess}
                reverse
              >
                تصدير الي ملف
              </Button>
            </Flex>
            {/* )} */}
          </StickyContainer>
          <Tickets
            ticketsList={ticketsList}
            ticketsListState={ticketsListState}
            isLoadMoreTicketsAvailable={isLoadMoreTicketsAvailable}
            loadMoreTickets={this.loadMoreTickets}
            isGetTicketsFetching={isGetTicketsFetching}
            applyNewFilter={applyNewFilter}
          />
        </>
        {/* )} */}
      </Flex>
    );
  }
}
TicketsContainer.displayName = 'TicketsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  ticketsList: state.tickets.tickets,
  applyNewFilter: state.clients.applyNewFilter,
  ticketsListState: state.tickets.getTicketsReport,
  isLoadMoreTicketsAvailable: state.tickets.hasMoreTickets,
  isGetTicketsFetching: state.tickets.getTicketsReport.isFetching,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTicketsReport,
      exportFile,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(TicketsContainer);
