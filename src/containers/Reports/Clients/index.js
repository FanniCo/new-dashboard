import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faDownload, faFilter } from '@fortawesome/free-solid-svg-icons';
import { exportFile } from 'redux-modules/user/actions';
import { getClientsRequestsCount } from 'redux-modules/clients/actions';
import { Urls } from 'utils/api';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import ClientsRequestsCount from 'components/Reports/ClientsRequestsCount';
import Filter from 'components/Filter';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class ClientsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    clientsRequestsCountList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreClientsRequestsCountAvailable: PropTypes.bool,
    getClientsRequestsCount: PropTypes.func,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    isGetClientsRequestsCountFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    clientsRequestsCountList: undefined,
    isLoadMoreClientsRequestsCountAvailable: true,
    getClientsRequestsCount: () => {},
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    isGetClientsRequestsCountFetching: false,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('clientsRequestsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
      role: 'customer',
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getClientsRequestsCount: getClientsRequestsCountAction } = this.props;
    const { filterQueries } = this.state;

    getClientsRequestsCountAction(filterQueries, true);
  }

  loadMoreClientsRequests = () => {
    const { getClientsRequestsCount: getClientsRequestsCountAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getClientsRequestsCountAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user: { permissions },
      applyNewFilter,
      clientsRequestsCountList,
      isLoadMoreClientsRequestsCountAvailable,
      exportFile: exportFileAction,
      isExportFileFetching,
      isExportFileSuccess,
      isGetClientsRequestsCountFetching,
      getClientsRequestsCount: getClientsRequestsCountAction,
    } = this.props;
    const {
      clients: { exportClients },
    } = Urls;

    const { isFilterOpen, filterQueries } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير احصائى العملاء</title>
        </Helmet>
        {permissions && permissions.includes('Get Customers Requests Count Report') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير احصائى العملاء
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Download Customers Requests Count Report') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={() => exportFileAction(exportClients)}
                    icon={faDownload}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={isExportFileFetching && !isExportFileSuccess}
                    disable={isExportFileFetching && !isExportFileSuccess}
                    reverse
                  >
                    تصدير الي ملف
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="clientsRequests"
              filterSections={['dates']}
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getClientsRequestsCountAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            <ClientsRequestsCount
              clientsRequestsCountList={clientsRequestsCountList}
              isLoadMoreClientsRequestsCountAvailable={isLoadMoreClientsRequestsCountAvailable}
              loadMoreClientsRequestsCount={this.loadMoreClientsRequests}
              isGetClientsRequestsCountFetching={isGetClientsRequestsCountFetching}
              applyNewFilter={applyNewFilter}
            />
          </>
        )}
      </Flex>
    );
  }
}
ClientsContainer.displayName = 'ClientsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  clientsRequestsCountList: state.clients.clientsRequestsCount,
  applyNewFilter: state.clients.applyNewFilter,
  clientsPaymentsListState: state.clients.getClientsPayments,
  isLoadMoreClientsRequestsCountAvailable: state.clients.hasMoreClientsRequestsCount,
  isGetClientsRequestsCountFetching: state.clients.getClientsRequestsCount.isFetching,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getClientsRequestsCount, exportFile }, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ClientsContainer);
