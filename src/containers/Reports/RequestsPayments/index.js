import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faFilter, faDownload } from '@fortawesome/free-solid-svg-icons';
import { exportFile } from 'redux-modules/user/actions';
import { getRequestsPayments } from 'redux-modules/requests/actions';
import { Urls } from 'utils/api';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import RequestsPayment from 'components/Reports/RequestsPayments';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class RequestsPaymentsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    requestsPaymentsList: PropTypes.arrayOf(PropTypes.shape({})),
    requestsPaymentsListState: PropTypes.shape({}),
    isLoadMoreRequestsPaymentsAvailable: PropTypes.bool,
    getRequestsPayments: PropTypes.func,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    isGetRequestsPaymentsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    requestsPaymentsList: undefined,
    requestsPaymentsListState: undefined,
    isLoadMoreRequestsPaymentsAvailable: true,
    getRequestsPayments: () => {},
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    isGetRequestsPaymentsFetching: false,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('requestsPaymentsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getRequestsPayments: getRequestsPaymentsAction } = this.props;
    const { filterQueries } = this.state;

    getRequestsPaymentsAction(filterQueries, true);
  }

  loadMoreRequestsPayments = () => {
    const { getRequestsPayments: getRequestsPaymentsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getRequestsPaymentsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleExportFile = () => {
    let {
      requests: { exportRequestsPayments },
    } = Urls;

    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('requestsPaymentsFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          exportRequestsPayments = `${exportRequestsPayments}?${key}=${cachedFilterQueries[key]}`;
        } else {
          exportRequestsPayments = `${exportRequestsPayments}&${key}=${cachedFilterQueries[key]}`;
        }
      });
    }
    exportFileAction(exportRequestsPayments);
  };

  render() {
    const {
      user: { permissions },
      applyNewFilter,
      getRequestsPayments: getRequestsPaymentsAction,
      requestsPaymentsList,
      requestsPaymentsListState,
      isLoadMoreRequestsPaymentsAvailable,
      isExportFileFetching,
      isExportFileSuccess,
      isGetRequestsPaymentsFetching,
    } = this.props;
    const { isFilterOpen, filterQueries } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير التكلفة لكل طلب</title>
        </Helmet>
        {permissions && permissions.includes('Get Request Payment Report') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير التكلفة لكل طلب
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Download Report Request Payment') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={() => this.handleExportFile()}
                    icon={faDownload}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={isExportFileFetching && !isExportFileSuccess}
                    disable={isExportFileFetching && !isExportFileSuccess}
                    reverse
                  >
                    تصدير الي ملف
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="requestsPayments"
              filterSections={['field', 'city', 'paymentMethod', 'dates']}
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getRequestsPaymentsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            <RequestsPayment
              requestsPaymentsList={requestsPaymentsList}
              requestsPaymentsListState={requestsPaymentsListState}
              isLoadMoreRequestsPaymentsAvailable={isLoadMoreRequestsPaymentsAvailable}
              loadMoreRequestsPayments={this.loadMoreRequestsPayments}
              isGetRequestsPaymentsFetching={isGetRequestsPaymentsFetching}
              applyNewFilter={applyNewFilter}
            />
          </>
        )}
      </Flex>
    );
  }
}
RequestsPaymentsContainer.displayName = 'RequestsPaymentsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  requestsPaymentsList: state.requests.requestsPayments,
  applyNewFilter: state.requests.applyNewFilter,
  requestsPaymentsListState: state.requests.getRequestsPayments,
  isLoadMoreRequestsPaymentsAvailable: state.requests.hasMoreRequestsPayments,
  isGetRequestsPaymentsFetching: state.requests.getRequestsPayments.isFetching,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getRequestsPayments, exportFile }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(RequestsPaymentsContainer);
