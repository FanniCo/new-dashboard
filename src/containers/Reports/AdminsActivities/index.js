import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faDownload, faFilter, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { getAllAdminsActivities } from 'redux-modules/adminsActivities/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import AdminsActivitiesList from 'components/Reports/AdminsActivities';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';
import { Urls } from 'utils/api';
import { exportFile } from 'redux-modules/user/actions';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class AdminsActivitiesContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    adminsActivitiesList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreAdminsActivitiesAvailable: PropTypes.bool.isRequired,
    getAllAdminsActivities: PropTypes.func,
    isGetAllAdminsActivitiesFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    isGetAllAdminsActivitiesSuccess: PropTypes.bool,
    isGetAllAdminsActivitiesError: PropTypes.bool,
    isGetAllAdminsActivitiesErrorMessage: PropTypes.string,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    adminsActivitiesList: [],
    getAllAdminsActivities: () => {},
    applyNewFilter: false,
    isGetAllAdminsActivitiesFetching: false,
    isGetAllAdminsActivitiesSuccess: false,
    isGetAllAdminsActivitiesError: false,
    isGetAllAdminsActivitiesErrorMessage: undefined,
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('adminsActivitiesFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getAllAdminsActivities: getAllAdminsActivitiesAction } = this.props;
    const { filterQueries } = this.state;

    getAllAdminsActivitiesAction(filterQueries, true);
  }

  loadMoreAdminsActivities = () => {
    const { getAllAdminsActivities: getAllAdminsActivitiesAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllAdminsActivitiesAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleExportFile = () => {
    let {
      reports: { downloadAdminsActivities },
    } = Urls;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('adminsActivitiesFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          downloadAdminsActivities = `${downloadAdminsActivities}?${key}=${cachedFilterQueries[key]}`;
        } else {
          downloadAdminsActivities = `${downloadAdminsActivities}&${key}=${cachedFilterQueries[key]}`;
        }
        return true;
      });
    }

    exportFileAction(downloadAdminsActivities);
  };

  render() {
    const {
      user: { permissions },
      adminsActivitiesList,
      isLoadMoreAdminsActivitiesAvailable,
      getAllAdminsActivities: getAllAdminsActivitiesAction,
      isGetAllAdminsActivitiesFetching,
      applyNewFilter,
      isGetAllAdminsActivitiesSuccess,
      isGetAllAdminsActivitiesError,
      isGetAllAdminsActivitiesErrorMessage,
      isExportFileFetching,
      isExportFileSuccess,
    } = this.props;
    const { isFilterOpen, filterQueries } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير سجل النظام</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اتقرير انشطة الادمنز
          </Text>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Download Admins Activities Report') && (
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={this.handleExportFile}
                icon={faDownload}
                iconWidth="sm"
                xMargin={3}
                isLoading={isExportFileFetching && !isExportFileSuccess}
                disable={isExportFileFetching && !isExportFileSuccess}
                reverse
              >
                تصدير الي ملف
              </Button>
            )}
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="adminsActivities"
          filterSections={['dates']}
          creditsTitle="القيمة"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllAdminsActivitiesAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        />
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <InfiniteScroll
              dataLength={adminsActivitiesList.length}
              next={this.loadMoreAdminsActivities}
              hasMore={isLoadMoreAdminsActivitiesAvailable}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <AdminsActivitiesList
                adminsActivitiesList={adminsActivitiesList}
                isLoadMoreAdminsActivitiesAvailable={isLoadMoreAdminsActivitiesAvailable}
                loadMoreAdminsActivities={this.loadMoreAdminsActivities}
                isGetAllAdminsActivitiesFetching={isGetAllAdminsActivitiesFetching}
                isGetAllAdminsActivitiesSuccess={isGetAllAdminsActivitiesSuccess}
                isGetAllAdminsActivitiesError={isGetAllAdminsActivitiesError}
                isGetAllAdminsActivitiesErrorMessage={isGetAllAdminsActivitiesErrorMessage}
                applyNewFilter={applyNewFilter}
              />
            </InfiniteScroll>
          </Card>
        </Box>
      </Flex>
    );
  }
}
AdminsActivitiesContainer.displayName = 'AdminsActivitiesContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  adminsActivitiesList: state.adminsActivities.adminsActivities,
  isGetAllAdminsActivitiesFetching: state.adminsActivities.getAllAdminsActivities.isFetching,
  isGetAllAdminsActivitiesSuccess: state.adminsActivities.getAllAdminsActivities.isSuccess,
  isGetAllAdminsActivitiesError: state.adminsActivities.getAllAdminsActivities.isFail.isError,
  isGetAllAdminsActivitiesErrorMessage:
    state.adminsActivities.getAllAdminsActivities.isFail.message,
  isLoadMoreAdminsActivitiesAvailable: state.adminsActivities.hasMoreAdminsActivities,
  applyNewFilter: state.adminsActivities.applyNewFilter,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      exportFile,
      getAllAdminsActivities,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(AdminsActivitiesContainer);
