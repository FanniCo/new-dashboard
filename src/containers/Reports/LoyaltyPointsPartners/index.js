import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFile, faFilter, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllLoyaltyPointsPartners,
} from 'redux-modules/loyaltyPointsPartners/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import LoyaltyPointsPartnersList from 'components/LoyaltyPointsPartners';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class LoyaltyPointsPartnersContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    loyaltyPointsPartnersList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreLoyaltyPointsPartnersAvailable: PropTypes.bool.isRequired,

    getAllLoyaltyPointsPartners: PropTypes.func,
    isGetAllLoyaltyPointsPartnersFetching: PropTypes.bool,
    isGetAllLoyaltyPointsPartnersSuccess: PropTypes.bool,
    isGetAllLoyaltyPointsPartnersError: PropTypes.bool,
    getAllLoyaltyPointsPartnersErrorMessage: PropTypes.string,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    loyaltyPointsPartnersList: undefined,
    isLoadMoreLoyaltyPointsPartnersAvailable: true,
    getAllLoyaltyPointsPartners: () => {},
    applyNewFilter: false,
    isGetAllLoyaltyPointsPartnersFetching: false,
    isGetAllLoyaltyPointsPartnersSuccess: false,
    isGetAllLoyaltyPointsPartnersError: false,
    getAllLoyaltyPointsPartnersErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('loyaltyPointsPartnersFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      messageId: '',
      index: 0,
      messageDetails: {},
    };
  }

  componentDidMount() {
    const { getAllLoyaltyPointsPartners: getAllLoyaltyPointsPartnersAction } = this.props;
    const { filterQueries } = this.state;

    getAllLoyaltyPointsPartnersAction(filterQueries, true);
  }

  loadMoreLoyaltyPointsPartners = () => {
    const { getAllLoyaltyPointsPartners: getAllLoyaltyPointsPartnersAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllLoyaltyPointsPartnersAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  getLoadingLoyaltyPointsPartners = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingLoyaltyPointsPartnersList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingLoyaltyPointsPartners(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      user: { permissions },
      loyaltyPointsPartnersList,
      isLoadMoreLoyaltyPointsPartnersAvailable,
      getAllLoyaltyPointsPartners: getAllLoyaltyPointsPartnersAction,
      isGetAllLoyaltyPointsPartnersFetching,
      isGetAllLoyaltyPointsPartnersSuccess,
      isGetAllLoyaltyPointsPartnersError,
      getAllLoyaltyPointsPartnersErrorMessage,
      applyNewFilter,
    } = this.props;
    const { isFilterOpen, filterQueries, index, messageDetails } = this.state;

    let loyaltyPointsPartnersRenderer;

    if (
      (isGetAllLoyaltyPointsPartnersFetching && (!loyaltyPointsPartnersList || applyNewFilter)) ||
      (!isGetAllLoyaltyPointsPartnersFetching && !isGetAllLoyaltyPointsPartnersSuccess && !isGetAllLoyaltyPointsPartnersError)
    ) {
      loyaltyPointsPartnersRenderer = this.createLoadingLoyaltyPointsPartnersList();
    } else if (isGetAllLoyaltyPointsPartnersError) {
      loyaltyPointsPartnersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllLoyaltyPointsPartnersErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!loyaltyPointsPartnersList.length) {
      loyaltyPointsPartnersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      loyaltyPointsPartnersRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={loyaltyPointsPartnersList.length}
            next={this.loadMoreLoyaltyPointsPartners}
            hasMore={isLoadMoreLoyaltyPointsPartnersAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <LoyaltyPointsPartnersList
              loyaltyPointsPartnersList={loyaltyPointsPartnersList}
              isLoadMoreLoyaltyPointsPartnersAvailable={isLoadMoreLoyaltyPointsPartnersAvailable}
              loadMoreLoyaltyPointsPartners={this.loadMoreLoyaltyPointsPartners}
              isGetAllLoyaltyPointsPartnersFetching={isGetAllLoyaltyPointsPartnersFetching}
              applyNewFilter={applyNewFilter}
              user={user}
              messageDetails={messageDetails}
              messageIndex={index}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير استخدام اكواد شركاء نقط الولاء</title>
        </Helmet>
        {permissions && permissions.includes('loyalty points partners report') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير استخدام اكواد شركاء نقط الولاء
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="loyaltyPointsPartners"
              filterSections={['dates']}
              creditsTitle="القيمة"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllLoyaltyPointsPartnersAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {loyaltyPointsPartnersRenderer}
          </>
        )}
      </Flex>
    );
  }
}
LoyaltyPointsPartnersContainer.displayName = 'LoyaltyPointsPartnersContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.loyaltyPointsPartners.applyNewFilter,
  loyaltyPointsPartnersList: state.loyaltyPointsPartners.loyaltyPointsPartners,
  isGetAllLoyaltyPointsPartnersFetching: state.loyaltyPointsPartners.getAllLoyaltyPointsPartners.isFetching,
  isGetAllLoyaltyPointsPartnersSuccess: state.loyaltyPointsPartners.getAllLoyaltyPointsPartners.isSuccess,
  isGetAllLoyaltyPointsPartnersError: state.loyaltyPointsPartners.getAllLoyaltyPointsPartners.isFail.isError,
  getAllLoyaltyPointsPartnersErrorMessage: state.loyaltyPointsPartners.getAllLoyaltyPointsPartners.isFail.message,
  isLoadMoreLoyaltyPointsPartnersAvailable: state.loyaltyPointsPartners.hasMoreLoyaltyPointsPartners,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllLoyaltyPointsPartners,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(LoyaltyPointsPartnersContainer);
