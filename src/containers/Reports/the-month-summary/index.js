import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { saveTheMonthSummaryReport, getTheMonthSummaryReport } from 'redux-modules/statics/actions';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { customSelectStyles, StickyContainer } from 'components/shared';
import Card from 'components/Card';
import InputField from 'components/InputField';
import { TagInput } from 'reactjs-tag-input';
import Button from 'components/Buttons';
import { faChevronLeft, faFile } from '@fortawesome/free-solid-svg-icons';
import Caution from 'components/Caution';
import _ from 'lodash';
import EmptyState from 'components/EmptyState';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';

const { GREY_MEDIUM } = COLORS;
const { DISABLED, GREY_LIGHT, BRANDING_GREEN, LIGHT_RED, WHITE, ERROR, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const RtlContainer = styled(Flex)`
  direction: rtl;
`;

class TheMonthSummaryContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    saveTheMonthSummaryReport: PropTypes.func,
    getTheMonthSummaryReport: PropTypes.func,
    isSaveTheMonthSummaryReportFetching: PropTypes.bool,
    isGetTheMonthSummaryReportSuccess: PropTypes.bool,
    isSaveTheMonthSummaryReportError: PropTypes.bool,
    saveTheMonthSummaryReportErrorMessage: PropTypes.string,

    isGetTheMonthSummaryReportFetching: PropTypes.bool,
    isGetTheMonthSummaryReportError: PropTypes.bool,
    getTheMonthSummaryReportErrorMessage: PropTypes.string,
    theMonthSummaryReport: PropTypes.shape({}),
  };

  static defaultProps = {
    user: undefined,
    saveTheMonthSummaryReport: () => {},
    getTheMonthSummaryReport: () => {},
    isSaveTheMonthSummaryReportFetching: false,
    isSaveTheMonthSummaryReportError: false,
    saveTheMonthSummaryReportErrorMessage: undefined,

    isGetTheMonthSummaryReportFetching: false,
    isGetTheMonthSummaryReportSuccess: false,
    isGetTheMonthSummaryReportError: false,
    getTheMonthSummaryReportErrorMessage: undefined,
    theMonthSummaryReport: undefined,
  };

  constructor(props) {
    super(props);

    this.state = {
      emails: [],
      incomeGoal: 0,
      doneRequestsGoal: 0,
      doneRequestsPercentageGoal: 0,
      totalRequestsGoal: 0,
      customersGoal: 0,
      isLoaded: false,
    };
  }

  componentDidMount() {
    const { getTheMonthSummaryReport: getTheMonthSummaryReportAction } = this.props;

    getTheMonthSummaryReportAction();
  }

  componentDidUpdate(prevProps) {
    const { theMonthSummaryReport = {}, isGetTheMonthSummaryReportSuccess } = this.props;

    if (prevProps.isGetTheMonthSummaryReportSuccess !== isGetTheMonthSummaryReportSuccess) {
      this.setState({
        emails: (theMonthSummaryReport.emails || []).map((email, index) => ({
          id: index,
          displayValue: email,
        })),
        incomeGoal: theMonthSummaryReport.incomeGoal || 0,
        doneRequestsGoal: theMonthSummaryReport.doneRequestsGoal || 0,
        doneRequestsPercentageGoal: theMonthSummaryReport.doneRequestsPercentageGoal || 0,
        totalRequestsGoal: theMonthSummaryReport.totalRequestsGoal || 0,
        customersGoal: theMonthSummaryReport.customersGoal || 0,
        isLoaded: true,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitForm = e => {
    e.preventDefault();

    const { saveTheMonthSummaryReport: saveTheMonthSummaryReportAction } = this.props;
    const {
      emails,
      incomeGoal,
      doneRequestsGoal,
      doneRequestsPercentageGoal,
      totalRequestsGoal,
      customersGoal,
    } = this.state;

    const data = {
      emails: emails.map(email => email.displayValue),
      incomeGoal,
      doneRequestsGoal,
      doneRequestsPercentageGoal,
      totalRequestsGoal,
      customersGoal,
    };

    saveTheMonthSummaryReportAction(data);
  };

  getLoadingMessages = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingMessagesList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingMessages(counter));
    }
    return list;
  };

  render() {
    const {
      isSaveTheMonthSummaryReportFetching,
      isSaveTheMonthSummaryReportError,
      saveTheMonthSummaryReportErrorMessage,

      isGetTheMonthSummaryReportFetching,
      isGetTheMonthSummaryReportSuccess,
      isGetTheMonthSummaryReportError,
      getTheMonthSummaryReportErrorMessage,
      theMonthSummaryReport = {},
    } = this.props;

    const {
      emails,
      incomeGoal,
      doneRequestsGoal,
      doneRequestsPercentageGoal,
      totalRequestsGoal,
      customersGoal,
      isLoaded,
    } = this.state;

    if (isGetTheMonthSummaryReportFetching) {
      return this.createLoadingMessagesList();
    }

    if (isGetTheMonthSummaryReportError) {
      return (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getTheMonthSummaryReportErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    }

    if (isGetTheMonthSummaryReportSuccess && isLoaded && theMonthSummaryReport) {
      return (
        <Flex flexDirection="column" alignItems="flex-end" width={1}>
          <Helmet>
            <title>تقرير ملخص الشهر</title>
          </Helmet>
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير ملخص الشهر
              </Text>
            </StickyContainer>

            <RtlContainer flexDirection="column" alignItems="flex" width={1}>
              <Card flexWrap="wrap" width={1} mb={3} p={4} bgColor={COLORS_VALUES[GREY_LIGHT]}>
                <Box width={[1, 1, 1, 1, 'calc(50% - 8px)']} ml={2}>
                  <Flex pb={3} px={4}>
                    <Flex flexDirection="column" width={1}>
                      <Text
                        mx={2}
                        mb={4}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        ايميلات مستلمين التقرير
                      </Text>
                      <TagInput
                        placeholder="ايميلات مستلمين التقرير"
                        tags={emails}
                        onTagsChanged={value => this.handleInputFieldChange('emails', value)}
                        wrapperStyle={`
    box-shadow: none;
    width: 100%;
    transform: none;
    position: unset;
    border-radius: 4px;
    background: rgb(0 0 0 / 0%);
    margin: 0px;
    padding: 0px 0px 0px 15px;
  `}
                        inputStyle={`
    background: #424242;
    border: 1px solid #212121;
    color: #25C281;
    &::-webkit-input-placeholder {
    -webkit-text-security: none;
    color: rgb(117, 117, 117);
    direction: inherit !important;
    pointer-events: none !important;
    text-orientation: inherit !important;
    writing-mode: inherit !important;

    text-overflow: inherit;
    line-height: initial;
    white-space: pre;
    overflow-wrap: normal;
    -webkit-user-modify: read-only !important;
    overflow: hidden;
    padding: 8px;
    }
  `}
                        tagStyle={
                          'background-color: #25C281;padding-top: 0px;padding-bottom: 0px;font-size: 12px;}'
                        }
                      />
                    </Flex>
                  </Flex>

                  <Flex pb={3} px={4}>
                    <Flex flexDirection="column" width={1}>
                      <Text
                        mx={2}
                        mb={4}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        الايراد المستهدف
                      </Text>
                      <InputField
                        type="Number"
                        placeholder="هدف الايراد"
                        width={1}
                        styles={customSelectStyles}
                        value={incomeGoal}
                        onChange={value => this.handleInputFieldChange('incomeGoal', value)}
                        mb={2}
                        autoComplete="on"
                      />
                    </Flex>
                  </Flex>

                  <Flex pb={3} px={4}>
                    <Flex flexDirection="column" width={1}>
                      <Text
                        mx={2}
                        mb={4}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        عدد الطلبات المكتملة المستهدفة
                      </Text>
                      <InputField
                        type="Number"
                        placeholder="هدف الطلبات المكتملة"
                        width={1}
                        styles={customSelectStyles}
                        value={doneRequestsGoal}
                        onChange={value => this.handleInputFieldChange('doneRequestsGoal', value)}
                        mb={2}
                        autoComplete="on"
                      />
                    </Flex>
                    <Flex flexDirection="column" width={1}>
                      <Text
                        mx={2}
                        mb={4}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        نسبة الطلبات المكتملة المستهدفة
                      </Text>
                      <InputField
                        type="Number"
                        placeholder="هدف نسبة الطلبات المكتملة"
                        width={1}
                        styles={customSelectStyles}
                        value={doneRequestsPercentageGoal}
                        onChange={value =>
                          this.handleInputFieldChange('doneRequestsPercentageGoal', value)
                        }
                        mb={2}
                        autoComplete="on"
                      />
                    </Flex>
                    <Flex flexDirection="column" width={1}>
                      <Text
                        mx={2}
                        mb={4}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        عدد الطلبات المستهدفة
                      </Text>
                      <InputField
                        type="Number"
                        placeholder="عدد الطلبات المستهدفة"
                        width={1}
                        styles={customSelectStyles}
                        value={totalRequestsGoal}
                        onChange={value =>
                          this.handleInputFieldChange('totalRequestsGoal', value)
                        }
                        mb={2}
                        autoComplete="on"
                      />
                    </Flex>
                  </Flex>

                  <Flex pb={3} px={4}>
                    <Flex flexWrap="wrap" flexDirection="column" width={1}>
                      <Text
                        mx={2}
                        mb={4}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        عدد عودة العملاء المستهدفة
                      </Text>
                      <InputField
                        type="Number"
                        placeholder="هدف عودة العملاء"
                        width={1}
                        styles={customSelectStyles}
                        value={customersGoal}
                        onChange={value => this.handleInputFieldChange('customersGoal', value)}
                        mb={2}
                        autoComplete="on"
                      />
                    </Flex>
                  </Flex>

                  <Flex
                    flexDirection="row-reverse"
                    alignItems="center"
                    justifyContent="space-between"
                    px={4}
                  >
                    <Button
                      type="submit"
                      primary
                      color={BRANDING_GREEN}
                      icon={faChevronLeft}
                      reverse
                      onClick={this.onSubmitForm}
                      disabled={
                        // isSaveTheMonthSummaryReportFetching ||
                        !emails.length ||
                        _.isNil(incomeGoal) ||
                        _.isNil(customersGoal) ||
                        _.isNil(doneRequestsGoal) ||
                        _.isNil(doneRequestsPercentageGoal) ||
                        _.isNil(totalRequestsGoal)
                      }
                      isLoading={isSaveTheMonthSummaryReportFetching}
                    >
                      حفظ
                    </Button>
                  </Flex>
                  {isSaveTheMonthSummaryReportError && (
                    <Caution
                      mx={2}
                      bgColor={LIGHT_RED}
                      textColor={WHITE}
                      borderColorProp={ERROR}
                      style={{ height: 'auto' }}
                    >
                      {saveTheMonthSummaryReportErrorMessage}
                    </Caution>
                  )}
                </Box>
                <Box width={[1, 1, 1, 1, 'calc(50% - 8px)']} ml={2}>
                  <h3 style={{ color: 'white' }}>الايراد</h3>

                  <table style={{ borderCollapse: 'collapse', color: 'white' }}>
                    <thead>
                      <tr style={{ backgroundColor: 'rgba(150, 212, 212, 0.4)' }}>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          الايراد الفعلي
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.income || 0}
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <br />
                  <h3 style={{ color: 'white' }}>الطلبات</h3>
                  <table style={{ borderCollapse: 'collapse', color: 'white' }}>
                    <thead>
                      <tr style={{ backgroundColor: 'rgba(150, 212, 212, 0.4)' }}>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          اجمالي الطلبات
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          اجمالي الطلبات المكتملة
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          اجمالي الطلبات الملغية
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          <span style={{ display: 'inline-block' }}>اجمالي الطلبات آخرى</span>
                          <span style={{ display: 'inline-block' }}>(نشطة-مؤجل-منتظر)</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.totalRequests || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.totalCompletedAndReviewed || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.totalCanceled || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.totalOther || 0}
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <br />

                  <h3 style={{ color: 'white' }}>العملاء</h3>
                  <table style={{ borderCollapse: 'collapse', color: 'white' }}>
                    <thead>
                      <tr style={{ backgroundColor: 'rgba(150, 212, 212, 0.4)' }}>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          معدل عودة العملاء
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          معدل الشكاوي التي تم حلها خلال ٢٤ ساعة
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.totalNumberOfCustomersHasMoreThanOneRequest || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.ticketsCount ||  0}
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <br />

                  <table style={{ borderCollapse: 'collapse', color: 'white' }}>
                    <thead>
                      <tr style={{ backgroundColor: 'rgba(150, 212, 212, 0.4)' }}>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          عدد المستخدمين الجدد
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          عدد التحول لعميل
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          النسبة
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.newCustomersCount || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.newCustomersCountWithOneRequest || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.customersPercentage || 0}
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  <br />

                  <h3 style={{ color: 'white' }}>الفنيين</h3>
                  <table style={{ borderCollapse: 'collapse', color: 'white' }}>
                    <thead>
                      <tr style={{ backgroundColor: 'rgba(150, 212, 212, 0.4)' }}>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          عدد المؤسسات الجديدة
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          عدد الفنيين النشطين
                        </th>
                        <th
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          معدل طلبات الفنيين
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.newVendorsCount || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.totalNumberOfActiveWorkingWorkers || 0}
                        </td>
                        <td
                          style={{ border: '1px solid black', textAlign: 'center', padding: '8px' }}
                        >
                          {theMonthSummaryReport.workersPercentage || 0}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </Box>
              </Card>
            </RtlContainer>
          </>
        </Flex>
      );
    }

    return null;
  }
}
TheMonthSummaryContainer.displayName = 'TheMonthSummaryContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  isSaveTheMonthSummaryReportFetching: state.statics.saveTheMonthSummaryReport.isFetching,
  isSaveTheMonthSummaryReportError: state.statics.saveTheMonthSummaryReport.isFail.isError,
  saveTheMonthSummaryReportErrorMessage: state.statics.saveTheMonthSummaryReport.isFail.message,

  isGetTheMonthSummaryReportFetching: state.statics.getTheMonthSummaryReport.isFetching,
  isGetTheMonthSummaryReportSuccess: state.statics.getTheMonthSummaryReport.isSuccess,
  isGetTheMonthSummaryReportError: state.statics.getTheMonthSummaryReport.isFail.isError,
  getTheMonthSummaryReportErrorMessage: state.statics.getTheMonthSummaryReport.isFail.message,
  theMonthSummaryReport: state.statics.theMonthSummaryReport,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      saveTheMonthSummaryReport,
      getTheMonthSummaryReport,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(TheMonthSummaryContainer);
