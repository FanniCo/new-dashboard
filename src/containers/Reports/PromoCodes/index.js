import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faDownload, faFilter } from '@fortawesome/free-solid-svg-icons';
import { exportFile } from 'redux-modules/user/actions';
import { getPromoCodes } from 'redux-modules/promoCodes/actions';
import { Urls } from 'utils/api';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import PromoCodes from 'components/Reports/PromoCodes';
import Filter from 'components/Filter';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class PromoCodesContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    promoCodesList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMorePromoCodes: PropTypes.bool,
    getPromoCodes: PropTypes.func,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    isGetPromoCodesFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    promoCodesList: undefined,
    isLoadMorePromoCodes: true,
    getPromoCodes: () => {},
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    isGetPromoCodesFetching: false,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('promoCodesReportFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getPromoCodes: getPromoCodesAction } = this.props;
    const { filterQueries } = this.state;

    getPromoCodesAction(filterQueries, true);
  }

  loadMorePromoCodes = () => {
    const { getPromoCodes: getPromoCodesAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getPromoCodesAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleExportFile = () => {
    let {
      promoCodes: { exportPromoCodeReport },
    } = Urls;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('promoCodesReportFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0 && !exportPromoCodeReport.includes('?')) {
          exportPromoCodeReport = `${exportPromoCodeReport}?${key}=${cachedFilterQueries[key]}`;
        } else {
          exportPromoCodeReport = `${exportPromoCodeReport}&${key}=${cachedFilterQueries[key]}`;
        }
        return true;
      });
    }

    exportFileAction(exportPromoCodeReport);
  };

  render() {
    const {
      user: { permissions },
      applyNewFilter,
      promoCodesList,
      isLoadMorePromoCodes,
      isExportFileFetching,
      isExportFileSuccess,
      isGetPromoCodesFetching,
      getPromoCodes: getPromoCodesAction,
    } = this.props;
    const { isFilterOpen, filterQueries } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير اكواد الخصم</title>
        </Helmet>
        {permissions && permissions.includes('Get Promo Code Report') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير اكواد الخصم
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Download Promo Codes Report') && (
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={this.handleExportFile}
                    icon={faDownload}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={isExportFileFetching && !isExportFileSuccess}
                    disable={isExportFileFetching && !isExportFileSuccess}
                    reverse
                  >
                    تصدير الي ملف
                  </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="promoCodesReport"
              filterSections={['dates', 'promoCode', 'mostUsed', 'mostCompletedRequests']}
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getPromoCodesAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            <PromoCodes
              promoCodesList={promoCodesList}
              isLoadMorePromoCodes={isLoadMorePromoCodes}
              loadMorePromoCodes={this.loadMorePromoCodes}
              isGetPromoCodesFetching={isGetPromoCodesFetching}
              applyNewFilter={applyNewFilter}
            />
          </>
        )}
      </Flex>
    );
  }
}
PromoCodesContainer.displayName = 'PromoCodesContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  promoCodesList: state.promoCodes.promoCodesReportList,
  applyNewFilter: state.promoCodes.applyNewFilter,
  isLoadMorePromoCodes: state.promoCodes.isLoadMorePromoCodes,
  isGetPromoCodesFetching: state.promoCodes.getPromoCodesReport.isFetching,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getPromoCodes, exportFile }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(PromoCodesContainer);
