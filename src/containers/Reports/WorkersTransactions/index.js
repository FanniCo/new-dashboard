import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faDownload, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getAllTransactions } from 'redux-modules/transactions/actions';
import { exportFile } from 'redux-modules/user/actions';
import { Urls } from 'utils/api';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import TransactionsList from 'components/Reports/WorkersTransactions';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class TransactionsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    transactionsClassification: PropTypes.shape({}),
    transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
    transactionsState: PropTypes.shape({}),
    isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
    getAllTransactions: PropTypes.func,
    isGetAllTransactionsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    transactionsClassification: undefined,
    transactionsList: undefined,
    transactionsState: undefined,
    getAllTransactions: () => {},
    isGetAllTransactionsFetching: false,
    applyNewFilter: false,
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersTransactionsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
      role: 'worker',
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getAllTransactions: getAllTransactionsAction } = this.props;
    const { filterQueries } = this.state;

    getAllTransactionsAction(filterQueries, true);
  }

  loadMoreTransactions = () => {
    const { getAllTransactions: getAllTransactionsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;
    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllTransactionsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleExportFile = () => {
    let {
      transactions: { exportWorkersTransactions },
    } = Urls;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersTransactionsFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (
          typeof cachedFilterQueries[key] === 'object' &&
          cachedFilterQueries[key].constructor === Object
        ) {
          Object.keys(cachedFilterQueries[key]).forEach((nestedKey, nestedIndex) => {
            if (nestedIndex === 0) {
              exportWorkersTransactions = `${exportWorkersTransactions}?${nestedKey}=${
                cachedFilterQueries[key][nestedKey]
              }`;
            } else {
              exportWorkersTransactions = `${exportWorkersTransactions}&${nestedKey}=${
                cachedFilterQueries[key][nestedKey]
              }`;
            }
          });
          return true;
        }
        if (index === 0) {
          exportWorkersTransactions = `${exportWorkersTransactions}?${key}=${
            cachedFilterQueries[key]
          }`;
        } else {
          exportWorkersTransactions = `${exportWorkersTransactions}&${key}=${
            cachedFilterQueries[key]
          }`;
        }
        return true;
      });
    }
    exportFileAction(exportWorkersTransactions);
  };

  render() {
    const {
      user: { permissions },
      transactionsClassification,
      transactionsList,
      transactionsState,
      isLoadMoreTransactionsAvailable,
      getAllTransactions: getAllTransactionsAction,
      isGetAllTransactionsFetching,
      applyNewFilter,
      isExportFileFetching,
      isExportFileSuccess,
    } = this.props;
    const { isFilterOpen, filterQueries } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير العمليات المالية لجميع الفنيين</title>
        </Helmet>
        {permissions && permissions.includes('Get Transactions') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير العمليات المالية لجميع الفنيين
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions &&
                  permissions.includes('Download Report Transactions For All Workers') && (
                    <Button
                      ml={1}
                      mr={1}
                      color={PRIMARY}
                      onClick={this.handleExportFile}
                      icon={faDownload}
                      iconWidth="sm"
                      xMargin={3}
                      isLoading={isExportFileFetching && !isExportFileSuccess}
                      disable={isExportFileFetching && !isExportFileSuccess}
                      reverse
                    >
                      تصدير الي ملف
                    </Button>
                  )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="workersTransactions"
              transactionsClassification={transactionsClassification}
              filterSections={[
                'workerUsername',
                'groupedTransactionTypes',
                'transactionClassificationForGroupedTransactionTypes',
                'commissionRate',
                'transactionSign',
                'field',
                'credits',
                'dates',
              ]}
              workerUsernameQueryKey="username"
              creditsTitle="القيمة"
              minCreditsQueryKey="min_worth"
              maxCreditsQueryKey="max_worth"
              fieldsQueryKey="field"
              citiesQueryKey="city"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllTransactionsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            <TransactionsList
              transactionsList={transactionsList}
              transactionsState={transactionsState}
              isLoadMoreTransactionsAvailable={isLoadMoreTransactionsAvailable}
              loadMoreTransactions={this.loadMoreTransactions}
              isGetAllTransactionsFetching={isGetAllTransactionsFetching}
              applyNewFilter={applyNewFilter}
            />
          </>
        )}
      </Flex>
    );
  }
}
TransactionsContainer.displayName = 'TransactionsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.transactions.applyNewFilter,
  transactionsClassification: state.statics.transactionsClassification,
  transactionsList: state.transactions.transactions,
  transactionsState: state.transactions.getAllTransactions,
  isGetAllTransactionsFetching: state.transactions.getAllTransactions.isFetching,
  isLoadMoreTransactionsAvailable: state.transactions.hasMoreTransactions,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getAllTransactions, exportFile }, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(TransactionsContainer);
