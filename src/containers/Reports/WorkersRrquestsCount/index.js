import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faDownload, faFilter } from '@fortawesome/free-solid-svg-icons';
import { exportFile } from 'redux-modules/user/actions';
import { getWorkersRequestsCount } from 'redux-modules/workers/actions';
import { Urls } from 'utils/api';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import WorkersRequestsCount from 'components/Reports/WorkersRequestsCount';
import Filter from 'components/Filter';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class WorkersRequestsCountContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    workersRequestsCountList: PropTypes.arrayOf(PropTypes.shape({})),
    workersRequestsCountListState: PropTypes.shape({}),
    isLoadMoreWorkersRequestsCountAvailable: PropTypes.bool,
    getWorkersRequestsCount: PropTypes.func,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    workersRequestsCountList: undefined,
    workersRequestsCountListState: undefined,
    isLoadMoreWorkersRequestsCountAvailable: true,
    getWorkersRequestsCount: () => {},
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersRequestsCountFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
    };
  }

  componentDidMount() {
    const { getWorkersRequestsCount: getWorkersRequestsCountAction } = this.props;
    const { filterQueries } = this.state;

    getWorkersRequestsCountAction(filterQueries, true);
  }

  loadMoreWorkersRequestsCount = () => {
    const { getWorkersRequestsCount: getWorkersRequestsCountAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getWorkersRequestsCountAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleExportFile = () => {
    let {
      workers: { exportWorkersRequestsCount },
    } = Urls;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem('workersRequestsCountFilterState'));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          exportWorkersRequestsCount = `${exportWorkersRequestsCount}?${key}=${
            cachedFilterQueries[key]
          }`;
        } else {
          exportWorkersRequestsCount = `${exportWorkersRequestsCount}&${key}=${
            cachedFilterQueries[key]
          }`;
        }
        return true;
      });
    }
    exportFileAction(exportWorkersRequestsCount);
  };

  render() {
    const {
      user: { permissions },
      applyNewFilter,
      workersRequestsCountList,
      workersRequestsCountListState,
      isLoadMoreWorkersRequestsCountAvailable,
      isExportFileFetching,
      isExportFileSuccess,
      getWorkersRequestsCount: getWorkersRequestsCountAction,
    } = this.props;
    const { isFilterOpen, filterQueries } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير احصائى للفنيين</title>
        </Helmet>
        {permissions && permissions.includes('Get Worker Requests Count Report') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير احصائى للفنيين
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions &&
                  permissions.includes('Download Worker Requests Count And Total Paid Report') && (
                    <Button
                      ml={1}
                      mr={1}
                      color={PRIMARY}
                      onClick={() => this.handleExportFile()}
                      icon={faDownload}
                      iconWidth="sm"
                      xMargin={3}
                      isLoading={isExportFileFetching && !isExportFileSuccess}
                      disable={isExportFileFetching && !isExportFileSuccess}
                      reverse
                    >
                      تصدير الي ملف
                    </Button>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="workersRequestsCount"
              filterSections={['dates']}
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getWorkersRequestsCountAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            <WorkersRequestsCount
              workersRequestsCountList={workersRequestsCountList}
              workersRequestsCountListState={workersRequestsCountListState}
              isLoadMoreWorkersRequestsCountAvailable={isLoadMoreWorkersRequestsCountAvailable}
              loadMoreWorkersRequestsCount={this.loadMoreWorkersRequestsCount}
              applyNewFilter={applyNewFilter}
            />
          </>
        )}
      </Flex>
    );
  }
}
WorkersRequestsCountContainer.displayName = 'WorkersRequestsCountContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  workersRequestsCountList: state.workers.workersRequestsCount,
  applyNewFilter: state.workers.applyNewFilter,
  workersRequestsCountListState: state.workers.getWorkersRequestsCount,
  isLoadMoreWorkersRequestsCountAvailable: state.workers.hasMoreWorkersRequestsCount,
  isGetWorkersRequestsCountFetching: state.workers.getWorkersRequestsCount.isFetching,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getWorkersRequestsCount, exportFile }, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(WorkersRequestsCountContainer);
