import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Flex } from '@rebass/grid';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { exportFile } from 'redux-modules/user/actions';
import { getClientsPayments } from 'redux-modules/clients/actions';
import { Urls } from 'utils/api';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import ClientsPayments from 'components/Reports/ClientsPayments';

const { GREY_MEDIUM, DISABLED, PRIMARY } = COLORS;
const { SUPER_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class ClientsPaymentsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    clientsPaymentsList: PropTypes.arrayOf(PropTypes.shape({})),
    clientsPaymentsListState: PropTypes.shape({}),
    isLoadMoreClientsPaymentsAvailable: PropTypes.bool,
    getClientsPayments: PropTypes.func,
    exportFile: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    isGetClientsPaymentsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    clientsPaymentsList: undefined,
    clientsPaymentsListState: undefined,
    isLoadMoreClientsPaymentsAvailable: true,
    getClientsPayments: () => {},
    exportFile: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    isGetClientsPaymentsFetching: false,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      isFilterOpen: false,
      filterQueries: {
        skip: 0,
        limit: 20,
      },
    };
  }

  componentDidMount() {
    const { getClientsPayments: getClientsPaymentsAction } = this.props;
    const { filterQueries } = this.state;

    getClientsPaymentsAction(filterQueries, true);
  }

  loadMoreClientsPayments = () => {
    const { getClientsPayments: getClientsPaymentsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getClientsPaymentsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user: { permissions },
      applyNewFilter,
      clientsPaymentsList,
      clientsPaymentsListState,
      isLoadMoreClientsPaymentsAvailable,
      exportFile: exportFileAction,
      isExportFileFetching,
      isExportFileSuccess,
      isGetClientsPaymentsFetching,
    } = this.props;
    const {
      clients: { exportClientsPayments },
    } = Urls;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير اجمالى المدفوعات للعملاء</title>
        </Helmet>
        {permissions && permissions.includes('Get Client Payment Report') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تقرير اجمالى المدفوعات للعملاء
              </Text>
              {permissions && permissions.includes('Download Client Payment Report') && (
                <Flex m={2}>
                  <Button
                    ml={1}
                    mr={1}
                    color={PRIMARY}
                    onClick={() => exportFileAction(exportClientsPayments)}
                    icon={faDownload}
                    iconWidth="sm"
                    xMargin={3}
                    isLoading={isExportFileFetching && !isExportFileSuccess}
                    disable={isExportFileFetching && !isExportFileSuccess}
                    reverse
                  >
                    تصدير الي ملف
                  </Button>
                </Flex>
              )}
            </StickyContainer>
            <ClientsPayments
              clientsPaymentsList={clientsPaymentsList}
              clientsPaymentsListState={clientsPaymentsListState}
              isLoadMoreClientsPaymentsAvailable={isLoadMoreClientsPaymentsAvailable}
              loadMoreClientsPayments={this.loadMoreClientsPayments}
              isGetClientsPaymentsFetching={isGetClientsPaymentsFetching}
              applyNewFilter={applyNewFilter}
            />
          </>
        )}
      </Flex>
    );
  }
}
ClientsPaymentsContainer.displayName = 'ClientsPaymentsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  clientsPaymentsList: state.clients.clientsPayments,
  applyNewFilter: state.clients.applyNewFilter,
  clientsPaymentsListState: state.clients.getClientsPayments,
  isLoadMoreClientsPaymentsAvailable: state.clients.hasMoreClientsPayments,
  isGetClientsPaymentsFetching: state.clients.getClientsPayments.isFetching,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getClientsPayments, exportFile }, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ClientsPaymentsContainer);
