import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getTicketDetails,
  updateTicket,
  deleteTicket,
  addComment,
} from 'redux-modules/tickets/actions';
import { getAllAdmins } from 'redux-modules/user/actions';
import { toggleAddNewRequestModal, toggleAdminAddedRequest } from 'redux-modules/clients/actions';
import SingleTicket from 'components/SingleTicket';

class SingleTicketContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    ticketDetails: PropTypes.shape({}),
    isGetTicketDetailsFetching: PropTypes.bool,
    isUpdateTicketFetching: PropTypes.bool,
    isDeleteTicketFetching: PropTypes.bool,
    getTicketDetails: PropTypes.func,
    deleteTicket: PropTypes.func,
    match: PropTypes.shape({}),
    addComment: PropTypes.func,
    addCommentState: PropTypes.string,
    addCommentErrorMessage: PropTypes.string,
    isGetTicketDetailsError: PropTypes.bool,
    getTicketDetailsErrorMessage: PropTypes.string,
    isUpdateTicketError: PropTypes.bool,
    updateTicketErrorMessage: PropTypes.string,
    getAllAdmins: PropTypes.func,
    toggleAddNewRequestModal: PropTypes.func,
    admins: PropTypes.arrayOf(PropTypes.shape({})),
    isDeleteTicketError: PropTypes.bool,
    isDeleteTicketErrorMessage: PropTypes.string,
    adminAddedRequest: PropTypes.bool,
    toggleAdminAddedRequest: PropTypes.func,
  };

  static defaultProps = {
    user: undefined,
    ticketDetails: undefined,
    isGetTicketDetailsFetching: false,
    isUpdateTicketFetching: false,
    isDeleteTicketFetching: false,
    getTicketDetails: () => {},
    deleteTicket: () => {},
    match: undefined,
    addComment: undefined,
    addCommentState: undefined,
    addCommentErrorMessage: undefined,
    isGetTicketDetailsError: false,
    getTicketDetailsErrorMessage: undefined,
    isUpdateTicketError: false,
    updateTicketErrorMessage: undefined,
    getAllAdmins: () => {},
    toggleAddNewRequestModal: () => {},
    admins: [],
    isDeleteTicketError: false,
    isDeleteTicketErrorMessage: undefined,
    adminAddedRequest: false,
    toggleAdminAddedRequest: () => {},
  };

  state = {
    isConfirmModalOpen: false,
    confirmModalHeader: '',
    confirmModalText: '',
    confirmModalFunction: () => {},
    addedCommentValue: '',
    mentionData: [],
    isOpenMarkTicketAsSolvedModal: false,
    activeComplain: undefined,
    tabs: [{ name: 'تفاصيل الشكوي' }, { name: 'سجل العمليات المالية' }, { name: 'الطلبات' }],
    activeTab: 0,
  };

  componentDidMount() {
    const {
      getTicketDetails: getTicketDetailsAction,
      match: {
        params: { id },
      },
      getAllAdmins: getAllAdminsAction,
    } = this.props;

    getTicketDetailsAction(id);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleAddCommentInputChange = (event, newValue, newPlainTextValue, mentions) => {
    const { mentionData } = this.state;
    this.setState({
      addedCommentValue: newPlainTextValue,
      mentionData: mentionData.concat(mentions),
    });
  };

  resetAddCommentInput = () => {
    this.setState({ addedCommentValue: '' });
  };

  toggleMarkTicketAsModal = (type, complaint) => {
    this.setState(prevState => ({
      [type]: !prevState[type],
      activeComplain: complaint,
    }));
  };

  handleToggleAddNewRequestModal = complaint => {
    const { toggleAddNewRequestModal: toggleAddNewRequestModalAction } = this.props;
    this.setState({ activeComplain: complaint }, () => {
      toggleAddNewRequestModalAction();
    });
  };

  handleTabChange = value => {
    const { activeTab } = this.state;

    if (activeTab !== value) {
      this.setState({ activeTab: value });
    }
  };

  render() {
    return (
      <SingleTicket
        {...this.state}
        {...this.props}
        handleToggleConfirmModal={this.handleToggleConfirmModal}
        handleAddCommentInputChange={this.handleAddCommentInputChange}
        resetAddCommentInput={this.resetAddCommentInput}
        toggleMarkTicketAsModal={this.toggleMarkTicketAsModal}
        handleToggleAddNewRequestModal={this.handleToggleAddNewRequestModal}
        handleTabChange={this.handleTabChange}
      />
    );
  }
}
SingleTicketContainer.displayName = 'SingleTicketContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  admins: state.user.admins,
  ticketDetails: state.tickets.ticketDetails,
  isGetTicketDetailsFetching: state.tickets.getTicketDetails.isFetching,
  isGetTicketDetailsError: state.tickets.getTicketDetails.isFail.isError,
  getTicketDetailsErrorMessage: state.tickets.getTicketDetails.isFail.message,
  isGetTicketDetailsSuccess: state.tickets.getTicketDetails.isSuccess,
  isUpdateTicketFetching: state.tickets.updateTicket.isFetching,
  isUpdateTicketSuccess: state.tickets.updateTicket.isSuccess,
  isUpdateTicketError: state.tickets.updateTicket.isFail.isError,
  updateTicketErrorMessage: state.tickets.updateTicket.isFail.message,
  isDeleteTicketFetching: state.tickets.deleteTicket.isFetching,
  isDeleteTicketError: state.tickets.deleteTicket.isFail.isError,
  isDeleteTicketErrorMessage: state.tickets.deleteTicket.isFail.message,
  addCommentState: state.tickets.addComment.addCommentState,
  addCommentErrorMessage: state.tickets.addComment.error,

  isAddNewRequestModalOpen: state.clients.addNewRequestModal.isOpen,
  adminAddedRequest: state.clients.adminAddedRequest,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTicketDetails,
      updateTicket,
      deleteTicket,
      addComment,
      getAllAdmins,
      toggleAddNewRequestModal,
      toggleAdminAddedRequest,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(SingleTicketContainer);
