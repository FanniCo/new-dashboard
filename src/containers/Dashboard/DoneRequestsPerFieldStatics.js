import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getDoneRequestsPerFieldStatics } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import DoneRequestsPerFieldStatics from 'components/Dashboard/DoneRequestsPerFieldStatics';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class DoneRequestsPerFieldStaticsContainer extends Component {
  static propTypes = {
    isGetDoneRequestsPerFieldStaticsFetching: PropTypes.bool,
    isGetDoneRequestsPerFieldStaticsSuccess: PropTypes.bool,
    isGetDoneRequestsPerFieldStaticsError: PropTypes.bool,
    doneRequestsPerFieldStatics: PropTypes.shape({}),
    getDoneRequestsPerFieldStaticsErrorMessage: PropTypes.string,
    getDoneRequestsPerFieldStatics: PropTypes.func,
  };

  static defaultProps = {
    isGetDoneRequestsPerFieldStaticsFetching: false,
    isGetDoneRequestsPerFieldStaticsSuccess: false,
    isGetDoneRequestsPerFieldStaticsError: false,
    doneRequestsPerFieldStatics: undefined,
    getDoneRequestsPerFieldStaticsErrorMessage: undefined,
    getDoneRequestsPerFieldStatics: () => {},
  };

  constructor() {
    super();

    this.state = {
      filterDoneRequestsPerFieldStaticsQueries: {},
      isFilterDoneRequestsPerFieldStaticsOpen: false,
    };
  }

  componentDidMount() {
    const { getDoneRequestsPerFieldStatics: getDoneRequestsPerFieldStaticsAction } = this.props;
    const { filterDoneRequestsPerFieldStaticsQueries } = this.state;

    getDoneRequestsPerFieldStaticsAction(filterDoneRequestsPerFieldStaticsQueries, true);
  }

  handleToggleDoneRequestsPerFieldStaticsFilterModal = () => {
    const { isFilterDoneRequestsPerFieldStaticsOpen } = this.state;

    this.setState({
      isFilterDoneRequestsPerFieldStaticsOpen: !isFilterDoneRequestsPerFieldStaticsOpen,
    });
  };

  handleChangeDoneRequestsPerFieldStaticsFilterQueries = (type, val) => {
    const { filterDoneRequestsPerFieldStaticsQueries } = this.state;

    this.setState({
      filterDoneRequestsPerFieldStaticsQueries: {
        ...filterDoneRequestsPerFieldStaticsQueries,
        [type]: val,
      },
    });
  };

  render() {
    const {
      isGetDoneRequestsPerFieldStaticsFetching,
      isGetDoneRequestsPerFieldStaticsSuccess,
      isGetDoneRequestsPerFieldStaticsError,
      doneRequestsPerFieldStatics,
      getDoneRequestsPerFieldStaticsErrorMessage,
      getDoneRequestsPerFieldStatics: getDoneRequestsPerFieldStaticsAction,
    } = this.props;
    const {
      isFilterDoneRequestsPerFieldStaticsOpen,
      filterDoneRequestsPerFieldStaticsQueries,
    } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetDoneRequestsPerFieldStaticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetDoneRequestsPerFieldStaticsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getDoneRequestsPerFieldStaticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetDoneRequestsPerFieldStaticsSuccess && (
        <>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleDoneRequestsPerFieldStaticsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز المستخدمين
              </Text>
            </Button>
            <Filter
              cachedFilterPage="doneRequestsPerFieldStatics"
              filterSections={['dates']}
              header="فرز المستخدمين"
              filterQueries={filterDoneRequestsPerFieldStaticsQueries}
              isOpened={isFilterDoneRequestsPerFieldStaticsOpen}
              filterFunction={getDoneRequestsPerFieldStaticsAction}
              toggleFilter={this.handleToggleDoneRequestsPerFieldStaticsFilterModal}
              handleChangeFilterQueries={this.handleChangeDoneRequestsPerFieldStaticsFilterQueries}
            />
          </Flex>
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <DoneRequestsPerFieldStatics
              doneRequestsPerFieldStatics={doneRequestsPerFieldStatics}
            />
          </Card>
        </>
      )
    );
  }
}
DoneRequestsPerFieldStaticsContainer.displayName = 'DoneRequestsPerFieldStaticsContainer';

const mapStateToProps = state => ({
  doneRequestsPerFieldStatics: state.dashboard.doneRequestsPerFieldStatics,
  isGetDoneRequestsPerFieldStaticsFetching:
    state.dashboard.getDoneRequestsPerFieldStatics.isFetching,
  isGetDoneRequestsPerFieldStaticsSuccess: state.dashboard.getDoneRequestsPerFieldStatics.isSuccess,
  isGetDoneRequestsPerFieldStaticsError:
    state.dashboard.getDoneRequestsPerFieldStatics.isFail.isError,
  getDoneRequestsPerFieldStaticsErrorMessage:
    state.dashboard.getDoneRequestsPerFieldStatics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getDoneRequestsPerFieldStatics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  DoneRequestsPerFieldStaticsContainer,
);
