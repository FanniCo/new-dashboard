import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getCustomerRequestsStatistics } from 'redux-modules/dashboard/actions';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import CustomersRequestsStatistics from 'components/Dashboard/CustomersRequestsStatistics';
import Button from 'components/Buttons';
import Text from 'components/Text';
import Filter from 'components/Filter'; 

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class CustomersRequestsStatisticsContainer extends Component {
  static propTypes = {
    customerRequestsStatistics: PropTypes.shape({}),
    isGetCustomerRequestsStatisticsFetching: PropTypes.bool,
    isGetCustomerRequestsStatisticsSuccess: PropTypes.bool,
    isGetCustomerRequestsStatisticsError: PropTypes.bool,
    getCustomerRequestsStatisticsErrorMessage: PropTypes.string,
    getCustomerRequestsStatistics: PropTypes.func,
  };

  static defaultProps = {
    customerRequestsStatistics: {},
    isGetCustomerRequestsStatisticsFetching: false,
    isGetCustomerRequestsStatisticsSuccess: false,
    isGetCustomerRequestsStatisticsError: false,
    getCustomerRequestsStatisticsErrorMessage: undefined,
    getCustomerRequestsStatistics: () => {},
  };

  constructor(props) {
    super(props);
    const cachedCustomerRequestsStatsFilterState = JSON.parse(
      sessionStorage.getItem('customerRequestsStatsFilterState'),
    );

    let filterCustomersRequestsStatsQueries = {};

    if (cachedCustomerRequestsStatsFilterState) {
      const {
        filterQueries: cachedCustomerRequestsStatsFilterQueries,
      } = cachedCustomerRequestsStatsFilterState;

      filterCustomersRequestsStatsQueries = cachedCustomerRequestsStatsFilterQueries;
    }

    this.state = {
      isFilterCustomersRequestsStatsOpen: false,
      filterCustomersRequestsStatsQueries,
    };
  }

  componentDidMount() {
    const { getCustomerRequestsStatistics: getCustomerRequestsStatisticsAction } = this.props;
    const { filterCustomersRequestsStatsQueries } = this.state;

    getCustomerRequestsStatisticsAction(filterCustomersRequestsStatsQueries, true);
  }

  handleChangeCustomersRequestsStatsFilterQueries = (type, val) => {
    const { filterCustomersRequestsStatsQueries } = this.state;

    this.setState({
      filterCustomersRequestsStatsQueries: { ...filterCustomersRequestsStatsQueries, [type]: val },
    });
  };

  handleToggleCustomersRequestsStatsFilterModal = () => {
    const { isFilterCustomersRequestsStatsOpen } = this.state;

    this.setState({ isFilterCustomersRequestsStatsOpen: !isFilterCustomersRequestsStatsOpen });
  };

  render() {
    const {
      isGetCustomerRequestsStatisticsFetching,
      isGetCustomerRequestsStatisticsSuccess,
      isGetCustomerRequestsStatisticsError,
      getCustomerRequestsStatisticsErrorMessage,
      customerRequestsStatistics,
      getCustomerRequestsStatistics: getCustomerRequestsStatisticsAction,
    } = this.props;
    const { filterCustomersRequestsStatsQueries, isFilterCustomersRequestsStatsOpen } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetCustomerRequestsStatisticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetCustomerRequestsStatisticsError) {
      return (
        <Box width={1} className='RequestCounts'>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getCustomerRequestsStatisticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetCustomerRequestsStatisticsSuccess && (
        <>
        <Box className='StatictesBoxTitle'>
        <Text className='StatictesTopTitle'>
          إحصائيات طلبات العملاء
        </Text>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleCustomersRequestsStatsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton'
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز
              </Text>
            </Button>
            </Box>
            <Filter
              cachedFilterPage="customerRequestsStats"
              filterSections={['dates']}
              header="فرز"
              filterQueries={filterCustomersRequestsStatsQueries}
              isOpened={isFilterCustomersRequestsStatsOpen}
              filterFunction={getCustomerRequestsStatisticsAction}
              toggleFilter={this.handleToggleCustomersRequestsStatsFilterModal}
              handleChangeFilterQueries={this.handleChangeCustomersRequestsStatsFilterQueries}
            />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
            className="StatisticsContainer RequestsStatistics"
          >
            <CustomersRequestsStatistics
              customerRequestsStatistics={customerRequestsStatistics}
              isGetCustomerRequestsStatisticsFetching={isGetCustomerRequestsStatisticsFetching}
              isGetCustomerRequestsStatisticsError={isGetCustomerRequestsStatisticsError}
              getCustomerRequestsStatisticsErrorMessage={getCustomerRequestsStatisticsErrorMessage}
            />
          </Card>
        </>
      )
    );
  }
}
CustomersRequestsStatisticsContainer.displayName = 'CustomersRequestsStatisticsContainer';

const mapStateToProps = state => ({
  customerRequestsStatistics: state.dashboard.customerRequestsStatistics,
  isGetCustomerRequestsStatisticsFetching: state.dashboard.getCustomerRequestsStatistics.isFetching,
  isGetCustomerRequestsStatisticsSuccess: state.dashboard.getCustomerRequestsStatistics.isSuccess,
  isGetCustomerRequestsStatisticsError:
    state.dashboard.getCustomerRequestsStatistics.isFail.isError,
  getCustomerRequestsStatisticsErrorMessage:
    state.dashboard.getCustomerRequestsStatistics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCustomerRequestsStatistics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CustomersRequestsStatisticsContainer,
);
