import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getRequestsCostStatistics } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import RequestsCostStatistics from 'components/Dashboard/RequestsCostStatistics';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class RequestsCostStatisticsContainer extends Component {
  static propTypes = {
    requestsCostStatistics: PropTypes.shape({}),
    isGetRequestsCostStatisticsFetching: PropTypes.bool,
    isGetRequestsCostStatisticsSuccess: PropTypes.bool,
    isGetRequestsCostStatisticsError: PropTypes.bool,
    getRequestsCostStatisticsErrorMessage: PropTypes.string,
    getRequestsCostStatistics: PropTypes.func,
  };

  static defaultProps = {
    requestsCostStatistics: {},
    isGetRequestsCostStatisticsFetching: false,
    isGetRequestsCostStatisticsSuccess: false,
    isGetRequestsCostStatisticsError: false,
    getRequestsCostStatisticsErrorMessage: undefined,
    getRequestsCostStatistics: () => {},
  };

  constructor(props) {
    super(props);
    const cachedRequestsCostStatisticsFilterState = JSON.parse(
      sessionStorage.getItem('requestsCostStatisticsFilterState'),
    );

    let filterRequestsCostStatisticsQueries = {};

    if (cachedRequestsCostStatisticsFilterState) {
      const {
        filterQueries: cachedRequestsCostStatisticsFilterQueries,
      } = cachedRequestsCostStatisticsFilterState;

      filterRequestsCostStatisticsQueries = cachedRequestsCostStatisticsFilterQueries;
    }

    this.state = {
      isFilterRequestsCostStatisticsOpen: false,
      filterRequestsCostStatisticsQueries,
    };
  }

  componentDidMount() {
    const { getRequestsCostStatistics: getRequestsCostStatisticsAction } = this.props;
    const { filterRequestsCostStatisticsQueries } = this.state;

    getRequestsCostStatisticsAction(filterRequestsCostStatisticsQueries, true);
  }

  handleChangeRequestsCostStatisticsFilterQueries = (type, val) => {
    const { filterRequestsCostStatisticsQueries } = this.state;

    this.setState({
      filterRequestsCostStatisticsQueries: { ...filterRequestsCostStatisticsQueries, [type]: val },
    });
  };

  handleToggleRequestsCostStatisticsFilterModal = () => {
    const { isFilterRequestsCostStatisticsOpen } = this.state;

    this.setState({ isFilterRequestsCostStatisticsOpen: !isFilterRequestsCostStatisticsOpen });
  };

  render() {
    const {
      requestsCostStatistics,
      isGetRequestsCostStatisticsFetching,
      isGetRequestsCostStatisticsSuccess,
      isGetRequestsCostStatisticsError,
      getRequestsCostStatisticsErrorMessage,
      getRequestsCostStatistics: getRequestsCostStatisticsAction,
    } = this.props;
    const { isFilterRequestsCostStatisticsOpen, filterRequestsCostStatisticsQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetRequestsCostStatisticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetRequestsCostStatisticsError) {
      return (
        <Box width={1} className='RequestCounts'>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getRequestsCostStatisticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetRequestsCostStatisticsSuccess && (
        <>
        <Box className='StatictesBoxTitle'>
        <Text className='StatictesTopTitle'>
          إحصائيات التكاليف
        </Text>
          <Button
            ml={1}
            mr={1}
            color={PRIMARY}
            onClick={this.handleToggleRequestsCostStatisticsFilterModal}
            icon={faFilter}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
            className='FilterButton'
          >
            <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              فرز
            </Text>
          </Button>
          </Box>
          <Filter
            cachedFilterPage="requestsCostStatistics"
            filterSections={['field', 'city', 'dates']}
            fieldsQueryKey="types"
            citiesQueryKey="cities"
            header="فرز"
            filterQueries={filterRequestsCostStatisticsQueries}
            isOpened={isFilterRequestsCostStatisticsOpen}
            filterFunction={getRequestsCostStatisticsAction}
            toggleFilter={this.handleToggleRequestsCostStatisticsFilterModal}
            handleChangeFilterQueries={this.handleChangeRequestsCostStatisticsFilterQueries}
          />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
            className="StatisticsContainer RequestsStatistics"
          >
            <RequestsCostStatistics requestsCostStatistics={requestsCostStatistics} />
          </Card>
        </>
      )
    );
  }
}
RequestsCostStatisticsContainer.displayName = 'RequestsCostStatisticsContainer';

const mapStateToProps = state => ({
  requestsCostStatistics: state.dashboard.requestsCostStatistics,
  isGetRequestsCostStatisticsFetching: state.dashboard.getRequestsCostStatistics.isFetching,
  isGetRequestsCostStatisticsSuccess: state.dashboard.getRequestsCostStatistics.isSuccess,
  isGetRequestsCostStatisticsError: state.dashboard.getRequestsCostStatistics.isFail.isError,
  getRequestsCostStatisticsErrorMessage: state.dashboard.getRequestsCostStatistics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRequestsCostStatistics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  RequestsCostStatisticsContainer,
);
