import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { getRequestsLocation } from 'redux-modules/dashboard/actions';
import { exportFile } from 'redux-modules/user/actions';
import { fetchConfigs } from 'redux-modules/configs/actions';
import Text from 'components/Text';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import RequestsStatistics from 'containers/Dashboard/RequestsStatistics';
import CustomersRequestsStatistics from 'containers/Dashboard/CustomersRequestsStatistics';
import GeneralStatistics from 'containers/Dashboard/GeneralStatistics';
import RequestsCostStatistics from 'containers/Dashboard/RequestsCostStatistics';
import WorkersCredits from 'containers/Dashboard/WorkersCredits';
import WorkersHeatMap from 'containers/Dashboard/WorkersHeatMap';
import RequestsHeatMap from 'containers/Dashboard/RequestsHeatMap';
import FinancialByCities from 'containers/Dashboard/FinancialByCities';
import FinancialByFields from 'containers/Dashboard/FinancialByFields';
import WorkersInEachField from 'containers/Dashboard/WorkersInEachField';
import WorkersInEachCity from 'containers/Dashboard/WorkersInEachCity';
import UsersCount from 'containers/Dashboard/UsersCount';
import styled from 'styled-components';
import { border, borderBottom, color, background } from 'styled-system';
import TicketsStatistics from 'containers/Dashboard/TicketsStatistics';
import CustomersCredits from './CustomersCredits';
import PaymentMethodsStatics from './PaymentMethodsStatics';
import ReChargeMethodsStatics from './ReChargetMethodsStatics';
import DoneRequestsPerFieldStatics from './DoneRequestsPerFieldStatics';
import CancellationReasonsStatics from './CancellationReasonsStatics';
import TheMostWorkersHasTickets from './theMostWorkersHasTickets';
import WorkerDoneRequestToTicketsHasCloseFees from './workerDoneRequestToTicketsHasCloseFees';

const { DISABLED, BRANDING_GREEN, WARNING } = COLORS;
const { BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const TabsContainer = styled(Flex)`
  ${borderBottom};
`;

const Tab = styled(Flex)`
  cursor: pointer;
  width: max-content;
  border-radius: 4px;
  white-space:nowrap;
  ${color};
  ${background};
  padding: 8px 15px;
  transition :all 0.2s ease-in-out 0s;
  &:hover {
    background: ${COLORS_VALUES[COLORS.GREY_DARK]};
    transform:scale3d(1.05,1.05,1.05);
  }
  /*${border};
  ${borderBottom};*/
`;

class DashboardContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    fetchConfigs: PropTypes.func,
    isFetchConfigsSuccuss: PropTypes.bool,
  };

  static defaultProps = {
    user: undefined,
    fetchConfigs: () => {},
    isFetchConfigsSuccuss: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      statisticsActiveTab: 0,
      statisticsTabs: [
        { name: 'إحصائيات الطلبات' },
        { name: 'إحصائيات عامة' },
        { name: 'إحصائيات طلبات العملاء' },
        { name: 'إحصائيات التكاليف' },
        { name: 'إحصائيات المستخدمين' },
        // { name: 'إحصائيات شحن الرصيد للفنين' },
        { name: 'إحصائيات  شحن الرصيد وأرصدة الفنين' },
        { name: 'إحصائيات الايرادات حسب كل مدينه' },
        { name: 'إحصائيات الايرادات حسب كل خدمة' },
        { name: 'إحصائيات عدد الفنينن لكل خدمة' },
        { name: 'إحصائيات عدد الفنينن لكل مدينة' },
        { name: 'مواقع الفنين' },
        { name: 'مواقع الطلبات' },
        { name: 'إحصائيات الشكاوي' },
        { name: 'إحصائيات إضافة رصيد إلي العملاء' },
        { name: 'إحصائيات وسائل الدفع' },
        { name: 'إحصائيات وسائل الشحن' },
        { name: 'إحصائيات الطلبات المكتمله لكل خدمه' },
        { name: 'إحصائيات اسباب الالغاء' },
        { name: 'إحصائيات اكثر الفنيين شكاوي' },
        { name: 'إحصائيات الطلبات المكتملة للفني مقارنة بالشكاوي اللي عليها رسوم اغلاق شكوى' },
      ],
    };
  }

  componentDidMount() {
    const { isFetchConfigsSuccuss, fetchConfigs: fetchConfigsAction } = this.props;

    if (!isFetchConfigsSuccuss) {
      fetchConfigsAction();
    }
  }

  handleTabChange = (key, value) => {
    const { statisticsActiveTab } = this.state;

    if (key === 'statisticsActiveTab' && statisticsActiveTab !== value) {
      this.setState({ statisticsActiveTab: value });
    }
  };

  render() {
    const {
      user: { permissions },
    } = this.props;

    const { statisticsTabs, statisticsActiveTab } = this.state;

    return (
      <Flex flexDirection="column" width={1} className='DashborardContainer'>
        <Helmet>
          <title>إحصائيات</title>
        </Helmet>
        {permissions && permissions.includes('Dashboard') && (
          <>
            <Flex flexDirection="column" alignItems="flex-end" width={1}>
              <Flex
                width={1}
                py={2}
                flexDirection="row-reverse"
                flexWrap="wrap"
                justifyContent="space-between"
                alignItems="center"
              >
                <Box width={[1, 1, 1, 1, 1]}>
                  <TabsContainer flexDirection="row-reverse" alignItems="center" mb={2} className='TabContainer'>
                    {statisticsTabs.map(
                      (tab, index) =>
                        index < 7 && (
                          <Tab
                            key={tab.name}
                            flexDirection="row-reverse"
                            alignItems="center"
                            m={1}
                            background={
                              statisticsActiveTab === index
                                ? `${COLORS_VALUES[COLORS.GREY_DARK]}`
                                : `${COLORS_VALUES[COLORS.PRIMARY]}`
                            }
                            onClick={() => this.handleTabChange('statisticsActiveTab', index)}
                            className={
                              (statisticsActiveTab === index ? 'active' : '')
                            }
                          >
                            <Text
                              cursor="pointer"
                              textAlign="center"
                              color={
                                statisticsActiveTab === index
                                  ? COLORS_VALUES[COLORS.WHITE]
                                  : COLORS_VALUES[COLORS.WHITE]
                              }
                              type={BODY}
                              fontWeight={
                                statisticsActiveTab === index
                                  ? 'bold'
                                  : 400
                              }
                            >
                              {tab.name}
                            </Text>
                          </Tab>
                        ),
                    )}
                  </TabsContainer>
                  <TabsContainer flexDirection="row-reverse" alignItems="center" mb={2} className='TabContainer'>
                    {statisticsTabs.map(
                      (tab, index) =>
                        index > 6 &&
                        index < 13 && (
                          <Tab
                            key={tab.name}
                            flexDirection="row-reverse"
                            alignItems="center"
                            p={2}
                            m={1}
                            background={
                              statisticsActiveTab === index
                                ? `${COLORS_VALUES[COLORS.GREY_DARK]}`
                                : `${COLORS_VALUES[COLORS.PRIMARY]}`
                            }
                            onClick={() => this.handleTabChange('statisticsActiveTab', index)}
                            className={
                              (statisticsActiveTab === index ? 'active' : '')
                            }
                          >
                            <Text
                              cursor="pointer"
                              textAlign="center"
                              color={
                                statisticsActiveTab === index
                                ? COLORS_VALUES[COLORS.WHITE]
                                : COLORS_VALUES[COLORS.WHITE]
                              }
                              type={BODY}
                              fontWeight={
                                statisticsActiveTab === index
                                  ? 'bold'
                                  : 400
                              }
                            >
                              {tab.name}
                            </Text>
                          </Tab>
                        ),
                    )}
                  </TabsContainer>
                  <TabsContainer flexDirection="row-reverse" alignItems="center" mb={2} className='TabContainer'>
                    {statisticsTabs.map(
                      (tab, index) =>
                        index >= 13 && (
                          <Tab
                            key={tab.name}
                            flexDirection="row-reverse"
                            alignItems="center"
                            p={2}
                            m={1}
                            background={
                              statisticsActiveTab === index
                                ? `${COLORS_VALUES[COLORS.GREY_DARK]}`
                                : `${COLORS_VALUES[COLORS.PRIMARY]}`
                            }
                            onClick={() => this.handleTabChange('statisticsActiveTab', index)}
                            className={
                              (statisticsActiveTab === index ? 'active' : '')
                            }
                          >
                            <Text
                              cursor="pointer"
                              textAlign="center"
                              color={
                                statisticsActiveTab === index
                                ? COLORS_VALUES[COLORS.WHITE]
                                : COLORS_VALUES[COLORS.WHITE]
                              }
                              type={BODY}
                              fontWeight={
                                statisticsActiveTab === index
                                  ? 'bold'
                                  : 400
                              }
                            >
                              {tab.name}
                            </Text>
                          </Tab>
                        ),
                    )}
                  </TabsContainer>
                  {statisticsActiveTab === 0 && <RequestsStatistics />}
                  {statisticsActiveTab === 1 && <GeneralStatistics />}
                  {statisticsActiveTab === 2 && <CustomersRequestsStatistics />}
                  {statisticsActiveTab === 3 && <RequestsCostStatistics />}
                  {statisticsActiveTab === 4 && <UsersCount />}
                  {statisticsActiveTab === 5 && <WorkersCredits />}
                  {statisticsActiveTab === 6 && <FinancialByCities permissions={permissions} />}
                  {statisticsActiveTab === 7 && <FinancialByFields />}
                  {statisticsActiveTab === 8 && <WorkersInEachField />}
                  {statisticsActiveTab === 9 && <WorkersInEachCity />}
                  {statisticsActiveTab === 10 && <WorkersHeatMap />}
                  {statisticsActiveTab === 11 && <RequestsHeatMap />}
                  {statisticsActiveTab === 12 && <TicketsStatistics />}
                  {statisticsActiveTab === 13 && <CustomersCredits />}
                  {statisticsActiveTab === 14 && <PaymentMethodsStatics />}
                  {statisticsActiveTab === 15 && <ReChargeMethodsStatics />}
                  {statisticsActiveTab === 16 && <DoneRequestsPerFieldStatics />}
                  {statisticsActiveTab === 17 && <CancellationReasonsStatics />}
                  {statisticsActiveTab === 18 && <TheMostWorkersHasTickets />}
                  {statisticsActiveTab === 19 && <WorkerDoneRequestToTicketsHasCloseFees />}
                </Box>
              </Flex>
            </Flex>
          </>
        )}
      </Flex>
    );
  }
}

DashboardContainer.displayName = 'DashboardContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  isFetchConfigsSuccuss: state.configs.fetchConfigs.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRequestsLocation,
      fetchConfigs,
      exportFile,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);
