import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getRequestsLocation } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import RequestsHeatMap from 'components/Dashboard/RequestsHeatMap';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class RequestsHeatMapContainer extends Component {
  static propTypes = {
    isGetRequestLocationFetching: PropTypes.bool,
    isGetRequestLocationSuccess: PropTypes.bool,
    isGetRequestLocationError: PropTypes.bool,
    getRequestLocationErrorMessage: PropTypes.string,
    requestsLocation: PropTypes.arrayOf(PropTypes.shape({})),
    getRequestsLocation: PropTypes.func,
  };

  static defaultProps = {
    isGetRequestLocationFetching: false,
    isGetRequestLocationSuccess: false,
    isGetRequestLocationError: false,
    getRequestLocationErrorMessage: false,
    requestsLocation: [],
    getRequestsLocation: () => {},
  };

  constructor(props) {
    super(props);
    const cachedRequestsLocationsFilterState = JSON.parse(
      sessionStorage.getItem('requestsLocationsFilterState'),
    );

    let filterRequestsLocationsQueries = {};

    if (cachedRequestsLocationsFilterState) {
      const {
        filterQueries: cachedRequestsLocationsFilterQueries,
      } = cachedRequestsLocationsFilterState;

      filterRequestsLocationsQueries = cachedRequestsLocationsFilterQueries;
    }

    this.state = {
      isFilterRequestsLocationsOpen: false,
      filterRequestsLocationsQueries,
    };
  }

  componentDidMount() {
    const { getRequestsLocation: getRequestsLocationAction } = this.props;
    const { filterRequestsLocationsQueries } = this.state;

    getRequestsLocationAction(filterRequestsLocationsQueries, true);
  }

  handleChangeRequestsLocationsFilterQueries = (type, val) => {
    const { filterRequestsLocationsQueries } = this.state;

    this.setState({
      filterRequestsLocationsQueries: { ...filterRequestsLocationsQueries, [type]: val },
    });
  };

  handleToggleRequestsLocationsFilterModal = () => {
    const { isFilterRequestsLocationsOpen } = this.state;

    this.setState({ isFilterRequestsLocationsOpen: !isFilterRequestsLocationsOpen });
  };

  render() {
    const {
      isGetRequestLocationFetching,
      isGetRequestLocationSuccess,
      isGetRequestLocationError,
      getRequestLocationErrorMessage,
      requestsLocation,
      getRequestsLocation: getRequestsLocationAction,
    } = this.props;
    const { isFilterRequestsLocationsOpen, filterRequestsLocationsQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetRequestLocationFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetRequestLocationError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getRequestLocationErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetRequestLocationSuccess && (
        <>
          <Button
            ml={1}
            mr={1}
            color={PRIMARY}
            onClick={this.handleToggleRequestsLocationsFilterModal}
            icon={faFilter}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
          >
            <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              فرز
            </Text>
          </Button>
          <Filter
            cachedFilterPage="requestsLocations"
            filterSections={['field', 'city', 'dates']}
            fieldsQueryKey="fields"
            citiesQueryKey="cities"
            header="فرز"
            filterQueries={filterRequestsLocationsQueries}
            isOpened={isFilterRequestsLocationsOpen}
            filterFunction={getRequestsLocationAction}
            toggleFilter={this.handleToggleRequestsLocationsFilterModal}
            handleChangeFilterQueries={this.handleChangeRequestsLocationsFilterQueries}
          />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <RequestsHeatMap locations={requestsLocation} />
          </Card>
        </>
      )
    );
  }
}
RequestsHeatMapContainer.displayName = 'RequestsHeatMapContainer';

const mapStateToProps = state => ({
  isGetRequestLocationFetching: state.dashboard.getRequestsLocation.isFetching,
  isGetRequestLocationSuccess: state.dashboard.getRequestsLocation.isSuccess,
  isGetRequestLocationError: state.dashboard.getRequestsLocation.isFail.isError,
  getRequestLocationErrorMessage: state.dashboard.getRequestsLocation.isFail.message,
  requestsLocation: state.dashboard.requestsLocation,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRequestsLocation,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(RequestsHeatMapContainer);
