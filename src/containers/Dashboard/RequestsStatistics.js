import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faChartPie, faChartBar, faFile, faFilter, faTable } from '@fortawesome/free-solid-svg-icons';
import { getRequestsCounts } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import RequestsStatistics from 'components/Dashboard/RequestsStatistics';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

  

const showPie = (event) => {
  document.body.classList.add('show-pie');
  document.body.classList.remove('show-line');
}
const showData = (event) => {
  document.body.classList.remove('show-pie');
  document.body.classList.remove('show-line');
}

const showLine = (event) => {
  document.body.classList.remove('show-pie');
  document.body.classList.add('show-line');
}

class RequestsStatisticsContainer extends Component {
  static propTypes = {
    isGetRequestsCountsFetching: PropTypes.bool,
    isGetRequestsCountsSuccess: PropTypes.bool,
    isGetRequestsCountsError: PropTypes.bool,
    getRequestsCountsErrorMessage: PropTypes.string,
    requestsCounts: PropTypes.shape({}),
    statsDaily: PropTypes.shape({}),
    getRequestsCounts: PropTypes.func,
  };

  static defaultProps = {
    isGetRequestsCountsFetching: false,
    isGetRequestsCountsSuccess: false,
    isGetRequestsCountsError: false,
    getRequestsCountsErrorMessage: undefined,
    requestsCounts: {},
    statsDaily: {},
    getRequestsCounts: () => {},
  };

  constructor(props) {
    document.body.classList.remove('show-chart');
    document.body.classList.remove('show-line');
    document.body.classList.remove('show-pie');
    super(props);
    const cachedRequestsCountsFilterState = JSON.parse(
      sessionStorage.getItem('requestsCountsFilterState'),
    );
    let filterRequestsCountsQueries = {};

    if (cachedRequestsCountsFilterState) {
      const { filterQueries: cachedRequestsCountsFilterQueries } = cachedRequestsCountsFilterState;

      filterRequestsCountsQueries = cachedRequestsCountsFilterQueries;
    }

    this.state = {
      isFilterRequestsCountsOpen: false,
      filterRequestsCountsQueries,
    };
  }

  
  componentDidMount() {
    const { getRequestsCounts: getRequestsCountsAction } = this.props;
    const { filterRequestsCountsQueries } = this.state;

    getRequestsCountsAction(filterRequestsCountsQueries, true);
  }

  handleChangeRequestsCountsFilterQueries = (type, val) => {
    const { filterRequestsCountsQueries } = this.state;

    this.setState({ filterRequestsCountsQueries: { ...filterRequestsCountsQueries, [type]: val } });
  };

  handleToggleRequestsCountsFilterModal = () => {
    const { isFilterRequestsCountsOpen } = this.state;

    this.setState({ isFilterRequestsCountsOpen: !isFilterRequestsCountsOpen });
  };

  

  render() {
    const {
      isGetRequestsCountsFetching,
      isGetRequestsCountsSuccess,
      isGetRequestsCountsError,
      getRequestsCountsErrorMessage,
      requestsCounts,
      statsDaily,
      getRequestsCounts: getRequestsCountsAction,
    } = this.props;
    const { isFilterRequestsCountsOpen, filterRequestsCountsQueries } = this.state;
    

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetRequestsCountsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetRequestsCountsError) {
      return (
        <Box width={1} className='RequestCounts'>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getRequestsCountsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetRequestsCountsSuccess && (
        <>
        <Box className='StatictesBoxTitle'>
        <Text className='StatictesTopTitle'>
          إحصائيات الطلبات
        </Text>
          <Button
            ml={1}
            mr={1}
            color={PRIMARY}
            onClick={this.handleToggleRequestsCountsFilterModal}
            icon={faFilter}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
            className='FilterButton'
          >
            <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              فرز
            </Text>
          </Button>
          <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={showPie}
              icon={faChartPie}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton pie-view'
            >
             
            </Button>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={showLine}
              icon={faChartBar}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton line-view'
            >
             
            </Button>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={showData}
              icon={faTable}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton box-view'
            >
              
            </Button>
          </Box>
          <Filter
            cachedFilterPage="requestsCounts"
            filterSections={['field', 'city', 'dates']}
            fieldsQueryKey="types"
            citiesQueryKey="cities"
            header="فرز"
            filterQueries={filterRequestsCountsQueries}
            isOpened={isFilterRequestsCountsOpen}
            filterFunction={getRequestsCountsAction}
            toggleFilter={this.handleToggleRequestsCountsFilterModal}
            handleChangeFilterQueries={this.handleChangeRequestsCountsFilterQueries}
          />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
            className="StatisticsContainer RequestsStatistics"
          >
            <RequestsStatistics requestsCounts={requestsCounts} statsDaily={statsDaily} />
          </Card>
          
        </>
      )
    );
  }
}
RequestsStatisticsContainer.displayName = 'RequestsStatisticsContainer';

const mapStateToProps = state => ({
  requestsCounts: state.dashboard.requestsCounts,
  statsDaily: state.dashboard.statsDaily,
  isGetRequestsCountsFetching: state.dashboard.getRequestsCounts.isFetching,
  isGetRequestsCountsSuccess: state.dashboard.getRequestsCounts.isSuccess,
  isGetRequestsCountsError: state.dashboard.getRequestsCounts.isFail.isError,
  getRequestsCountsErrorMessage: state.dashboard.getRequestsCounts.isFail.message,
});



const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRequestsCounts,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(RequestsStatisticsContainer);
