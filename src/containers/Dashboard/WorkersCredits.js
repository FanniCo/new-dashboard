import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getWorkerRechargeCost, getWorkersCredits } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import WorkersCredits from 'components/Dashboard/WorkersCredits';
import WorkersRechargeCost from 'components/Dashboard/WorkersRechargeCost';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class WorkersCreditsContainer extends Component {
  static propTypes = {
    isGetWorkersCreditsFetching: PropTypes.bool,
    isGetWorkersCreditsSuccess: PropTypes.bool,
    isGetWorkersCreditsError: PropTypes.bool,
    getWorkersCreditsErrorMessage: PropTypes.string,
    workersCredits: PropTypes.shape({}),
    getWorkersCredits: PropTypes.func,

    isGetWorkersRechargeCostFetching: PropTypes.bool,
    isGetWorkersRechargeCostSuccess: PropTypes.bool,
    isGetWorkersRechargeCostError: PropTypes.bool,
    getWorkersRechargeCostErrorMessage: PropTypes.string,
    workersRechargeCost: PropTypes.shape({}),
    getWorkerRechargeCost: PropTypes.func,
  };

  static defaultProps = {
    isGetWorkersCreditsFetching: true,
    isGetWorkersCreditsSuccess: false,
    isGetWorkersCreditsError: false,
    getWorkersCreditsErrorMessage: undefined,
    workersCredits: {},
    getWorkersCredits: () => {},

    isGetWorkersRechargeCostFetching: true,
    isGetWorkersRechargeCostSuccess: false,
    isGetWorkersRechargeCostError: false,
    getWorkersRechargeCostErrorMessage: undefined,
    workersRechargeCost: {},
    getWorkerRechargeCost: () => {},
  };

  constructor(props) {
    super(props);
    const cachedWorkersCreditsFilterState = JSON.parse(
      sessionStorage.getItem('workersCreditsFilterState'),
    );

    const cachedWorkersRechargeCostFilterState = JSON.parse(
      sessionStorage.getItem('workersRechargeCostFilterState'),
    );

    let filterWorkersCreditsQueries = {};
    const todayDay = new Date();
    const firstDayOfMonth = new Date(todayDay.getFullYear(), todayDay.getMonth(), 1);
    const lastDayOfMonth = new Date(todayDay.getFullYear(), todayDay.getMonth() + 1, 0);

    let filterWorkersRechargeCostQueries = {
      fromDate: `${firstDayOfMonth.getFullYear()}/${firstDayOfMonth.getMonth() +
        1}/${firstDayOfMonth.getDate()}`,
      toDate: `${lastDayOfMonth.getFullYear()}/${lastDayOfMonth.getMonth() +
        1}/${lastDayOfMonth.getDate()}`,
    };

    if (cachedWorkersCreditsFilterState) {
      const { filterQueries: cachedWorkersCreditsFilterQueries } = cachedWorkersCreditsFilterState;

      filterWorkersCreditsQueries = cachedWorkersCreditsFilterQueries;
    }

    if (cachedWorkersRechargeCostFilterState) {
      const {
        filterQueries: cachedWorkersRechargeCostFilterQueries,
      } = cachedWorkersRechargeCostFilterState;

      filterWorkersRechargeCostQueries = cachedWorkersRechargeCostFilterQueries;
    }

    this.state = {
      isFilterWorkersCreditsOpen: false,
      filterWorkersCreditsQueries,
      isFilterWorkersRechargeCostOpen: false,
      filterWorkersRechargeCostQueries,
    };
  }

  componentDidMount() {
    const {
      getWorkersCredits: getWorkersCreditsAction,
      getWorkerRechargeCost: getWorkerRechargeCostAction,
    } = this.props;
    const { filterWorkersCreditsQueries, filterWorkersRechargeCostQueries } = this.state;

    getWorkersCreditsAction(filterWorkersCreditsQueries, true);
    getWorkerRechargeCostAction(filterWorkersRechargeCostQueries, true);
  }

  handleChangeWorkersCreditsFilterQueries = (type, val) => {
    const { filterWorkersCreditsQueries } = this.state;

    this.setState({ filterWorkersCreditsQueries: { ...filterWorkersCreditsQueries, [type]: val } });
  };

  handleToggleWorkersCreditsFilterModal = () => {
    const { isFilterWorkersCreditsOpen } = this.state;

    this.setState({ isFilterWorkersCreditsOpen: !isFilterWorkersCreditsOpen });
  };

  handleChangeWorkersRechargeCostFilterQueries = (type, val) => {
    const { filterWorkersRechargeCostQueries } = this.state;

    this.setState({
      filterWorkersRechargeCostQueries: { ...filterWorkersRechargeCostQueries, [type]: val },
    });
  };

  handleToggleWorkersRechargeCostFilterModal = () => {
    const { isFilterWorkersRechargeCostOpen } = this.state;

    this.setState({ isFilterWorkersRechargeCostOpen: !isFilterWorkersRechargeCostOpen });
  };

  render() {
    const {
      isGetWorkersCreditsFetching,
      isGetWorkersCreditsSuccess,
      isGetWorkersCreditsError,
      getWorkersCreditsErrorMessage,
      workersCredits,
      getWorkersCredits: getWorkersCreditsAction,

      isGetWorkersRechargeCostFetching,
      isGetWorkersRechargeCostSuccess,
      isGetWorkersRechargeCostError,
      getWorkersRechargeCostErrorMessage,
      workersRechargeCost,
      getWorkerRechargeCost: getWorkerRechargeCostAction,
    } = this.props;
    const {
      isFilterWorkersCreditsOpen,
      filterWorkersCreditsQueries,
      isFilterWorkersRechargeCostOpen,
      filterWorkersRechargeCostQueries,
    } = this.state;
    let workersCreditsRender;
    let workersRechargeRender;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetWorkersCreditsFetching) {
      workersCreditsRender = createLoadingStatisticsList();
    } else if (isGetWorkersCreditsError) {
      workersCreditsRender = (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkersCreditsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    } else if (isGetWorkersCreditsSuccess) {
      workersCreditsRender = <WorkersCredits workersCredits={workersCredits} />;
    }

    if (isGetWorkersRechargeCostFetching) {
      workersRechargeRender = createLoadingStatisticsList();
    } else if (isGetWorkersRechargeCostError) {
      workersRechargeRender = (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkersRechargeCostErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    } else if (isGetWorkersRechargeCostSuccess) {
      workersRechargeRender = <WorkersRechargeCost costStatistics={workersRechargeCost} />;
    }

    return (
      <>
        <Button
          ml={1}
          mr={1}
          color={PRIMARY}
          onClick={this.handleToggleWorkersCreditsFilterModal}
          icon={faFilter}
          iconWidth="sm"
          xMargin={3}
          isLoading={false}
        >
          <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            فرز
          </Text>
        </Button>
        <Filter
          cachedFilterPage="workersCredits"
          filterSections={['dates']}
          header="فرز"
          filterQueries={filterWorkersCreditsQueries}
          isOpened={isFilterWorkersCreditsOpen}
          filterFunction={getWorkersCreditsAction}
          toggleFilter={this.handleToggleWorkersCreditsFilterModal}
          handleChangeFilterQueries={this.handleChangeWorkersCreditsFilterQueries}
        />
        <Card minHeight={100} m={2} p={2} flexDirection="column" bgColor={COLORS_VALUES[GREY_DARK]}>
          {workersCreditsRender}
        </Card>
        <Button
          ml={1}
          mr={1}
          color={PRIMARY}
          onClick={this.handleToggleWorkersRechargeCostFilterModal}
          icon={faFilter}
          iconWidth="sm"
          xMargin={3}
          isLoading={false}
        >
          <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            فرز
          </Text>
        </Button>
        <Filter
          cachedFilterPage="workersRechargeCost"
          filterSections={['dates']}
          header="فرز"
          filterQueries={filterWorkersRechargeCostQueries}
          isOpened={isFilterWorkersRechargeCostOpen}
          filterFunction={getWorkerRechargeCostAction}
          toggleFilter={this.handleToggleWorkersRechargeCostFilterModal}
          handleChangeFilterQueries={this.handleChangeWorkersRechargeCostFilterQueries}
        />
        <Card minHeight={100} m={2} p={2} flexDirection="column" bgColor={COLORS_VALUES[GREY_DARK]}>
          {workersRechargeRender}
        </Card>
      </>
    );
  }
}
WorkersCreditsContainer.displayName = 'WorkersCreditsContainer';

const mapStateToProps = state => ({
  isGetWorkersCreditsFetching: state.dashboard.getWorkersCredits.isFetching,
  isGetWorkersCreditsSuccess: state.dashboard.getWorkersCredits.isSuccess,
  isGetWorkersCreditsError: state.dashboard.getWorkersCredits.isFail.isError,
  getWorkersCreditsErrorMessage: state.dashboard.getWorkersCredits.isFail.message,
  workersCredits: state.dashboard.workersCredits,

  isGetWorkersRechargeCostFetching: state.dashboard.getWorkersRechargeCost.isFetching,
  isGetWorkersRechargeCostSuccess: state.dashboard.getWorkersRechargeCost.isSuccess,
  isGetWorkersRechargeCostError: state.dashboard.getWorkersRechargeCost.isFail.isError,
  getWorkersRechargeCostErrorMessage: state.dashboard.getWorkersRechargeCost.isFail.message,
  workersRechargeCost: state.dashboard.workersRechargeCost,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWorkersCredits,
      getWorkerRechargeCost,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(WorkersCreditsContainer);
