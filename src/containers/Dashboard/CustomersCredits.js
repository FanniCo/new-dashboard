import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faDownload, faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getCustomersCreditsCount } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import CustomersCredits from 'components/Dashboard/CustomersCredits';
import { Urls } from 'utils/api';
import { exportFile } from 'redux-modules/user/actions';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class CustomersCreditsContainer extends Component {
  static propTypes = {
    isGetCustomersCreditsCountFetching: PropTypes.bool,
    isGetCustomersCreditsCountSuccess: PropTypes.bool,
    isGetCustomersCreditsCountError: PropTypes.bool,
    CustomersCreditsCount: PropTypes.shape({}),
    getCustomersCreditsCountErrorMessage: PropTypes.string,
    getCustomersCreditsCount: PropTypes.func,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    exportFile: PropTypes.func,
  };

  static defaultProps = {
    isGetCustomersCreditsCountFetching: false,
    isGetCustomersCreditsCountSuccess: false,
    isGetCustomersCreditsCountError: false,
    CustomersCreditsCount: undefined,
    getCustomersCreditsCountErrorMessage: undefined,
    getCustomersCreditsCount: () => {},
    isExportFileFetching: false,
    isExportFileSuccess: false,
    exportFile: () => {},
  };

  constructor() {
    super();
    const cachedCustomersCreditsFilterState = JSON.parse(
      sessionStorage.getItem('customersCreditsFilterState'),
    );

    let filterCustomersCreditsQueries = {};

    if (cachedCustomersCreditsFilterState) {
      const {
        filterQueries: cachedCustomersCreditsFilterQueries,
      } = cachedCustomersCreditsFilterState;

      filterCustomersCreditsQueries = cachedCustomersCreditsFilterQueries;
    }

    this.state = {
      isFilterCustomersCreditsOpen: false,
      filterCustomersCreditsQueries,
    };
  }

  componentDidMount() {
    const { getCustomersCreditsCount: getCustomersCreditsCountAction } = this.props;
    const { filterCustomersCreditsQueries } = this.state;

    getCustomersCreditsCountAction(filterCustomersCreditsQueries, false);
  }

  handleToggleCustomersCreditsFilterModal = () => {
    const { isFilterCustomersCreditsOpen } = this.state;

    this.setState({ isFilterCustomersCreditsOpen: !isFilterCustomersCreditsOpen });
  };

  handleChangeCustomersCreditsFilterQueries = (type, val) => {
    const { filterCustomersCreditsQueries } = this.state;

    this.setState({
      filterCustomersCreditsQueries: { ...filterCustomersCreditsQueries, [type]: val },
    });
  };

  handleExportFile = (url, filterState) => {
    let targetedUrl = url;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem(filterState));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          targetedUrl = `${targetedUrl}?${key}=${cachedFilterQueries[key]}`;
        } else {
          targetedUrl = `${targetedUrl}&${key}=${cachedFilterQueries[key]}`;
        }
      });
    }

    exportFileAction(targetedUrl);
  };

  render() {
    const {
      isGetCustomersCreditsCountFetching,
      isGetCustomersCreditsCountSuccess,
      isGetCustomersCreditsCountError,
      CustomersCreditsCount,
      getCustomersCreditsCountErrorMessage,
      getCustomersCreditsCount: getCustomersCreditsCountAction,
      isExportFileFetching,
      isExportFileSuccess,
    } = this.props;
    const { isFilterCustomersCreditsOpen, filterCustomersCreditsQueries } = this.state;

    const { clients } = Urls;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetCustomersCreditsCountFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetCustomersCreditsCountError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getCustomersCreditsCountErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetCustomersCreditsCountSuccess && (
        <>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleCustomersCreditsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز
              </Text>
            </Button>

            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={() =>
                this.handleExportFile(clients.exportAddedCredits, 'customersCreditsFilterState')
              }
              icon={faDownload}
              iconWidth="sm"
              xMargin={3}
              isLoading={isExportFileFetching && !isExportFileSuccess}
              disable={isExportFileFetching && !isExportFileSuccess}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تصدير الي ملف
              </Text>
            </Button>

            <Filter
              cachedFilterPage="customersCredits"
              filterSections={['dates']}
              header="فرز"
              filterQueries={filterCustomersCreditsQueries}
              isOpened={isFilterCustomersCreditsOpen}
              filterFunction={getCustomersCreditsCountAction}
              toggleFilter={this.handleToggleCustomersCreditsFilterModal}
              handleChangeFilterQueries={this.handleChangeCustomersCreditsFilterQueries}
            />
          </Flex>

          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <CustomersCredits CustomersCreditsCount={CustomersCreditsCount} />
          </Card>
        </>
      )
    );
  }
}

CustomersCreditsContainer.displayName = 'CustomersCreditsContainer';

const mapStateToProps = state => ({
  CustomersCreditsCount: state.dashboard.CustomersCreditsCount,
  isGetCustomersCreditsCountFetching: state.dashboard.getCustomersCreditsCount.isFetching,
  isGetCustomersCreditsCountSuccess: state.dashboard.getCustomersCreditsCount.isSuccess,
  isGetCustomersCreditsCountError: state.dashboard.getCustomersCreditsCount.isFail.isError,
  getCustomersCreditsCountErrorMessage: state.dashboard.getCustomersCreditsCount.isFail.message,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCustomersCreditsCount,
      exportFile,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(CustomersCreditsContainer);
