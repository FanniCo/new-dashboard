import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { getWorkersInEachCity } from 'redux-modules/dashboard/actions';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import WorkersInEachCity from 'components/Dashboard/WorkersInEachCity';

const { GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class WorkersInEachCityContainer extends Component {
  static propTypes = {
    isGetWorkersForEachCityFetching: PropTypes.bool,
    isGetWorkersForEachCitySuccess: PropTypes.bool,
    isGetWorkersForEachCityError: PropTypes.bool,
    getWorkersForEachCityErrorMessage: PropTypes.string,
    WorkersForEachCity: PropTypes.shape({}),

    getWorkersInEachCity: PropTypes.func,
  };

  static defaultProps = {
    isGetWorkersForEachCityFetching: false,
    isGetWorkersForEachCitySuccess: false,
    isGetWorkersForEachCityError: false,
    getWorkersForEachCityErrorMessage: undefined,
    WorkersForEachCity: {},

    getWorkersInEachCity: () => {},
  };

  componentDidMount() {
    const { getWorkersInEachCity: getWorkersInEachCityAction } = this.props;

    getWorkersInEachCityAction(true);
  }

  render() {
    const {
      isGetWorkersForEachCityFetching,
      isGetWorkersForEachCitySuccess,
      isGetWorkersForEachCityError,
      getWorkersForEachCityErrorMessage,
      WorkersForEachCity,
    } = this.props;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetWorkersForEachCityFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetWorkersForEachCityError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkersForEachCityErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetWorkersForEachCitySuccess && (
        <Card minHeight={500} m={2} p={2} flexDirection="column" bgColor={COLORS_VALUES[GREY_DARK]}>
          <WorkersInEachCity WorkersForEachCity={WorkersForEachCity} />
        </Card>
      )
    );
  }
}
WorkersInEachCityContainer.displayName = 'WorkersInEachCityContainer';

const mapStateToProps = state => ({
  isGetWorkersForEachCityFetching: state.dashboard.getWorkersForEachCity.isFetching,
  isGetWorkersForEachCitySuccess: state.dashboard.getWorkersForEachCity.isSuccess,
  isGetWorkersForEachCityError: state.dashboard.getWorkersForEachCity.isFail.isError,
  getWorkersForEachCityErrorMessage: state.dashboard.getWorkersForEachCity.isFail.message,
  WorkersForEachCity: state.dashboard.WorkersForEachCity,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWorkersInEachCity,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(WorkersInEachCityContainer);
