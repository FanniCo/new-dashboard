import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getCancellationReasonsStatics } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import CancellationReasonsStatics from 'components/Dashboard/CancellationReasonStatics';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class CancellationReasonsStaticsContainer extends Component {
  static propTypes = {
    isGetCancellationReasonsStaticsFetching: PropTypes.bool,
    isGetCancellationReasonsStaticsSuccess: PropTypes.bool,
    isGetCancellationReasonsStaticsError: PropTypes.bool,
    cancellationReasonsStatics: PropTypes.shape({}),
    getCancellationReasonsStaticsErrorMessage: PropTypes.string,
    getCancellationReasonsStatics: PropTypes.func,
  };

  static defaultProps = {
    isGetCancellationReasonsStaticsFetching: false,
    isGetCancellationReasonsStaticsSuccess: false,
    isGetCancellationReasonsStaticsError: false,
    cancellationReasonsStatics: undefined,
    getCancellationReasonsStaticsErrorMessage: undefined,
    getCancellationReasonsStatics: () => {},
  };

  constructor() {
    super();

    this.state = {
      filterCancellationReasonsStaticsQueries: {},
      isFilterCancellationReasonsStaticsOpen: false,
    };
  }

  componentDidMount() {
    const { getCancellationReasonsStatics: getCancellationReasonsStaticsAction } = this.props;
    const { filterCancellationReasonsStaticsQueries } = this.state;

    getCancellationReasonsStaticsAction(filterCancellationReasonsStaticsQueries, true);
  }

  handleToggleCancellationReasonsStaticsFilterModal = () => {
    const { isFilterCancellationReasonsStaticsOpen } = this.state;

    this.setState({
      isFilterCancellationReasonsStaticsOpen: !isFilterCancellationReasonsStaticsOpen,
    });
  };

  handleChangeCancellationReasonsStaticsFilterQueries = (type, val) => {
    const { filterCancellationReasonsStaticsQueries } = this.state;

    this.setState({
      filterCancellationReasonsStaticsQueries: {
        ...filterCancellationReasonsStaticsQueries,
        [type]: val,
      },
    });
  };

  render() {
    const {
      isGetCancellationReasonsStaticsFetching,
      isGetCancellationReasonsStaticsSuccess,
      isGetCancellationReasonsStaticsError,
      cancellationReasonsStatics,
      getCancellationReasonsStaticsErrorMessage,
      getCancellationReasonsStatics: getCancellationReasonsStaticsAction,
    } = this.props;
    const {
      isFilterCancellationReasonsStaticsOpen,
      filterCancellationReasonsStaticsQueries,
    } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetCancellationReasonsStaticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetCancellationReasonsStaticsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getCancellationReasonsStaticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetCancellationReasonsStaticsSuccess && (
        <>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleCancellationReasonsStaticsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز المستخدمين
              </Text>
            </Button>
            <Filter
              cachedFilterPage="cancellationReasonsStatics"
              filterSections={['dates']}
              header="فرز المستخدمين"
              filterQueries={filterCancellationReasonsStaticsQueries}
              isOpened={isFilterCancellationReasonsStaticsOpen}
              filterFunction={getCancellationReasonsStaticsAction}
              toggleFilter={this.handleToggleCancellationReasonsStaticsFilterModal}
              handleChangeFilterQueries={this.handleChangeCancellationReasonsStaticsFilterQueries}
            />
          </Flex>
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <CancellationReasonsStatics cancellationReasonsStatics={cancellationReasonsStatics} />
          </Card>
        </>
      )
    );
  }
}
CancellationReasonsStaticsContainer.displayName = 'CancellationReasonsStaticsContainer';

const mapStateToProps = state => ({
  cancellationReasonsStatics: state.dashboard.cancellationReasonsStatics,
  isGetCancellationReasonsStaticsFetching: state.dashboard.getCancellationReasonsStatics.isFetching,
  isGetCancellationReasonsStaticsSuccess: state.dashboard.getCancellationReasonsStatics.isSuccess,
  isGetCancellationReasonsStaticsError:
    state.dashboard.getCancellationReasonsStatics.isFail.isError,
  getCancellationReasonsStaticsErrorMessage:
    state.dashboard.getCancellationReasonsStatics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCancellationReasonsStatics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CancellationReasonsStaticsContainer,
);
