import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faDownload, faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getTicketsStatistics } from 'redux-modules/tickets/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import TicketsStatistics from 'components/Dashboard/TicketsStatistics';
import { Urls } from 'utils/api';
import { exportFile } from 'redux-modules/user/actions';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class TicketsStatisticsContainer extends Component {
  static propTypes = {
    getTicketsStatistics: PropTypes.func,
    ticketsStatistics: PropTypes.shape({}),
    isGetTicketsStatisticsFetching: PropTypes.bool,
    isGetTicketsStatisticsSuccess: PropTypes.bool,
    isGetTicketsStatisticsError: PropTypes.bool,
    getTicketsStatisticsErrorMessage: PropTypes.string,
    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
  };

  static defaultProps = {
    getTicketsStatistics: () => {},
    ticketsStatistics: {},
    isGetTicketsStatisticsFetching: false,
    isGetTicketsStatisticsSuccess: false,
    isGetTicketsStatisticsError: false,
    getTicketsStatisticsErrorMessage: undefined,
    isExportFileFetching: false,
    isExportFileSuccess: false,
  };

  constructor(props) {
    super(props);
    const cachedTicketsStatisticsFilterState = JSON.parse(
      sessionStorage.getItem('ticketsStatisticsFilterState'),
    );

    let filterTicketsStatisticsQueries = {};

    if (cachedTicketsStatisticsFilterState) {
      const {
        filterQueries: cachedFilterGeneralStatisticsQueries,
      } = cachedTicketsStatisticsFilterState;

      filterTicketsStatisticsQueries = cachedFilterGeneralStatisticsQueries;
    }

    this.state = {
      isFilterTicketsStatisticsOpen: false,
      filterTicketsStatisticsQueries,
    };
  }

  componentDidMount() {
    const { getTicketsStatistics: getTicketsStatisticsAction } = this.props;
    const { filterTicketsStatisticsQueries } = this.state;

    getTicketsStatisticsAction(filterTicketsStatisticsQueries, true);
  }

  handleChangeTicketsStatisticsFilterQueries = (type, val) => {
    const { filterTicketsStatisticsQueries } = this.state;

    this.setState({
      filterTicketsStatisticsQueries: { ...filterTicketsStatisticsQueries, [type]: val },
    });
  };

  handleToggleTicketsStatisticsFilterModal = () => {
    const { isFilterTicketsStatisticsOpen } = this.state;

    this.setState({ isFilterTicketsStatisticsOpen: !isFilterTicketsStatisticsOpen });
  };

  handleExportFile = (url, filterState) => {
    let targetedUrl = url;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem(filterState));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          targetedUrl = `${targetedUrl}?${key}=${cachedFilterQueries[key]}`;
        } else {
          targetedUrl = `${targetedUrl}&${key}=${cachedFilterQueries[key]}`;
        }
      });
    }

    exportFileAction(targetedUrl);
  };

  render() {
    const {
      ticketsStatistics,
      isGetTicketsStatisticsFetching,
      isGetTicketsStatisticsSuccess,
      isGetTicketsStatisticsError,
      getTicketsStatisticsErrorMessage,
      getTicketsStatistics: getTicketsStatisticsAction,
      isExportFileFetching,
      isExportFileSuccess,
    } = this.props;
    const { tickets } = Urls;

    const { isFilterTicketsStatisticsOpen, filterTicketsStatisticsQueries } = this.state;

    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetTicketsStatisticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetTicketsStatisticsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getTicketsStatisticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetTicketsStatisticsSuccess && (
        <>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleTicketsStatisticsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز
              </Text>
            </Button>

            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={() =>
                this.handleExportFile(
                  tickets.ticketsStatsForEachMonth,
                  'ticketsStatisticsFilterState',
                )
              }
              icon={faDownload}
              iconWidth="sm"
              xMargin={3}
              isLoading={isExportFileFetching && !isExportFileSuccess}
              disable={isExportFileFetching && !isExportFileSuccess}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                تصدير الي ملف احصائيات الشكاوى
              </Text>
            </Button>
          </Flex>

          <Filter
            cachedFilterPage="ticketsStatistics"
            filterSections={['dates']}
            header="فرز"
            filterQueries={filterTicketsStatisticsQueries}
            isOpened={isFilterTicketsStatisticsOpen}
            filterFunction={getTicketsStatisticsAction}
            toggleFilter={this.handleToggleTicketsStatisticsFilterModal}
            handleChangeFilterQueries={this.handleChangeTicketsStatisticsFilterQueries}
          />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <TicketsStatistics ticketsStatistics={ticketsStatistics} />
          </Card>
        </>
      )
    );
  }
}

TicketsStatisticsContainer.displayName = 'TicketsStatisticsContainer';

const mapStateToProps = state => ({
  ticketsStatistics: state.tickets.ticketsStatistics,
  isGetTicketsStatisticsFetching: state.tickets.getTicketsStatistics.isFetching,
  isGetTicketsStatisticsSuccess: state.tickets.getTicketsStatistics.isSuccess,
  isGetTicketsStatisticsError: state.tickets.getTicketsStatistics.isFail.isError,
  getTicketsStatisticsErrorMessage: state.tickets.getTicketsStatistics.isFail.message,
  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTicketsStatistics,
      exportFile,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(TicketsStatisticsContainer);
