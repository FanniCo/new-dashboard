import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getGeneralStatistics } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import GeneralStatistics from 'components/Dashboard/GeneralStatistics';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class GeneralStatisticsContainer extends Component {
  static propTypes = {
    getGeneralStatistics: PropTypes.func,
    generalStatistics: PropTypes.shape({}),
    isGetGeneralStatisticsFetching: PropTypes.bool,
    isGetGeneralStatisticsSuccess: PropTypes.bool,
    isGetGeneralStatisticsError: PropTypes.bool,
    getGeneralStatisticsErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    getGeneralStatistics: () => {},
    generalStatistics: {},
    isGetGeneralStatisticsFetching: false,
    isGetGeneralStatisticsSuccess: false,
    isGetGeneralStatisticsError: false,
    getGeneralStatisticsErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedGeneralStatisticsFilterState = JSON.parse(
      sessionStorage.getItem('generalStatisticsFilterState'),
    );

    let filterGeneralStatisticsQueries = {};

    if (cachedGeneralStatisticsFilterState) {
      const {
        filterQueries: cachedFilterGeneralStatisticsQueries,
      } = cachedGeneralStatisticsFilterState;

      filterGeneralStatisticsQueries = cachedFilterGeneralStatisticsQueries;
    }

    this.state = {
      isFilterGeneralStatisticsOpen: false,
      filterGeneralStatisticsQueries,
    };
  }

  componentDidMount() {
    const { getGeneralStatistics: getGeneralStatisticsAction } = this.props;
    const { filterGeneralStatisticsQueries } = this.state;

    getGeneralStatisticsAction(filterGeneralStatisticsQueries, true);
  }

  handleChangeGeneralStatisticsFilterQueries = (type, val) => {
    const { filterGeneralStatisticsQueries } = this.state;

    this.setState({
      filterGeneralStatisticsQueries: { ...filterGeneralStatisticsQueries, [type]: val },
    });
  };

  handleToggleGeneralStatisticsFilterModal = () => {
    const { isFilterGeneralStatisticsOpen } = this.state;

    this.setState({ isFilterGeneralStatisticsOpen: !isFilterGeneralStatisticsOpen });
  };

  render() {
    const {
      generalStatistics,
      isGetGeneralStatisticsFetching,
      isGetGeneralStatisticsSuccess,
      isGetGeneralStatisticsError,
      getGeneralStatisticsErrorMessage,
      getGeneralStatistics: getGeneralStatisticsAction,
    } = this.props;
    const { isFilterGeneralStatisticsOpen, filterGeneralStatisticsQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetGeneralStatisticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetGeneralStatisticsError) {
      return (
        <Box width={1} className='RequestCounts'>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getGeneralStatisticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetGeneralStatisticsSuccess && (
        <>
        <Box className='StatictesBoxTitle'>
        <Text className='StatictesTopTitle'>
          إحصائيات عامة
        </Text>
          <Button
            ml={1}
            mr={1}
            color={PRIMARY}
            onClick={this.handleToggleGeneralStatisticsFilterModal}
            icon={faFilter}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
            className='FilterButton'
          >
            <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              فرز
            </Text>
          </Button>
        </Box>
          <Filter
            cachedFilterPage="generalStatistics"
            filterSections={['field', 'city', 'dates']}
            fieldsQueryKey="fields"
            citiesQueryKey="cities"
            header="فرز"
            filterQueries={filterGeneralStatisticsQueries}
            isOpened={isFilterGeneralStatisticsOpen}
            filterFunction={getGeneralStatisticsAction}
            toggleFilter={this.handleToggleGeneralStatisticsFilterModal}
            handleChangeFilterQueries={this.handleChangeGeneralStatisticsFilterQueries}
          />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
            className="StatisticsContainer RequestsStatistics"
          >
            <GeneralStatistics generalStatistics={generalStatistics} />
          </Card>
        </>
      )
    );
  }
}
GeneralStatisticsContainer.displayName = 'GeneralStatisticsContainer';

const mapStateToProps = state => ({
  generalStatistics: state.dashboard.generalStatistics,
  isGetGeneralStatisticsFetching: state.dashboard.getGeneralStatistics.isFetching,
  isGetGeneralStatisticsSuccess: state.dashboard.getGeneralStatistics.isSuccess,
  isGetGeneralStatisticsError: state.dashboard.getGeneralStatistics.isFail.isError,
  getGeneralStatisticsErrorMessage: state.dashboard.getGeneralStatistics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getGeneralStatistics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(GeneralStatisticsContainer);
