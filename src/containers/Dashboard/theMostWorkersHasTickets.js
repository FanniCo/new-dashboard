import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import {
  faFile,
  faFilter
} from '@fortawesome/free-solid-svg-icons';
import { getTheMostWorkersHasTickets } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import {
  CardBody,
  CardLabel,
  StickyContainer,
  SubTitle
} from "components/shared";
import {CITIES, FIELDS, } from "utils/constants";
import {getFieldIcon} from "utils/shared/style";
import {
  FontAwesomeIcon
} from "@fortawesome/react-fontawesome";
import {Helmet} from "react-helmet";

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE, GREY_MEDIUM } = COLORS;
const { SUBHEADING, BIG_TITLE, BODY, SUPER_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;


class TheMostWorkersHasTickets extends Component {
  static propTypes = {
    theMostWorkersHasTickets: PropTypes.arrayOf(PropTypes.shape({})),
    isGetTheMostWorkersHasTicketsFetching: PropTypes.bool,
    isGetTheMostWorkersHasTicketsSuccess: PropTypes.bool,
    isGetTheMostWorkersHasTicketsError: PropTypes.bool,
    getTheMostWorkersHasTicketsErrorMessage: PropTypes.string,

    getTheMostWorkersHasTickets: PropTypes.func,
  };

  static defaultProps = {
    theMostWorkersHasTickets: undefined,
    isGetTheMostWorkersHasTicketsFetching: false,
    isGetTheMostWorkersHasTicketsSuccess: false,
    isGetTheMostWorkersHasTicketsError: false,
    getTheMostWorkersHasTicketsErrorMessage: undefined,
    getTheMostWorkersHasTickets: () => {},
  };

  constructor() {
    super();

    this.state = {
      isFilterTheMostWorkersHasTicketsOpen: false,
      filterTheMostWorkersHasTicketsQueries: {},
    };
  }

  componentDidMount() {
    const { getTheMostWorkersHasTickets: getTheMostWorkersHasTicketsAction } = this.props;
    const { filterTheMostWorkersHasTicketsQueries } = this.state;

    getTheMostWorkersHasTicketsAction(filterTheMostWorkersHasTicketsQueries, true);
  }

  handleToggleTheMostWorkersHasTicketsFilterModal = () => {
    const { isFilterTheMostWorkersHasTicketsOpen } = this.state;

    this.setState({ isFilterTheMostWorkersHasTicketsOpen: !isFilterTheMostWorkersHasTicketsOpen });
  };

  handleChangeTheMostWorkersHasTicketsFilterQueries = (type, val) => {
    const { filterTheMostWorkersHasTicketsQueries } = this.state;

    this.setState({
      filterTheMostWorkersHasTicketsQueries: { ...filterTheMostWorkersHasTicketsQueries, [type]: val },
    });
  };



  render() {
    const {
      user: { permissions },
      isGetTheMostWorkersHasTicketsFetching,
      isGetTheMostWorkersHasTicketsError,
      getTheMostWorkersHasTicketsErrorMessage,
      theMostWorkersHasTickets,
      getTheMostWorkersHasTickets: getTheMostWorkersHasTicketsAction,
    } = this.props;
    const {
      isFilterTheMostWorkersHasTicketsOpen,
      filterTheMostWorkersHasTicketsQueries
    } = this.state;

    const renderFieldLabel = (fields) => fields.map(fieldKey => {
      const fieldIcon = getFieldIcon(fieldKey);

      return (
        <Flex key={fieldKey} mb={2} alignItems="center" justifyContent="flex-end">
          <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
          <CardLabel
            py={1}
            px={2}
            mx={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            {FIELDS[fieldKey].ar}
          </CardLabel>
        </Flex>
      );
    });

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
        <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
        <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    let contentRenderer;
    if(permissions.length === 0 || !permissions.includes('Get the most worker has tickets report')) {
      contentRenderer = (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text='لا تملك صلاحية مشاهده هذا التقرير'
              />
            </Card>
          </Flex>
        </Box>
      );
    }
    else if (isGetTheMostWorkersHasTicketsFetching) {
      contentRenderer = createLoadingStatisticsList()
    } else if (isGetTheMostWorkersHasTicketsError) {
      contentRenderer = (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getTheMostWorkersHasTicketsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    } else if(theMostWorkersHasTickets.length) {
      contentRenderer = (
        theMostWorkersHasTickets.map((worker) => (
          <Box key={worker._id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <Card
              height="calc(100% - 16px)"
              minHeight={100}
              flexDirection="column"
              justifyContent="space-between"
              bgColor={COLORS_VALUES[GREY_LIGHT]}
              m={2}
            >
              <CardBody
                justifyContent="space-between"
                flexDirection={['column', 'row', 'row', 'row', 'row']}
                py={3}
                px={1}
              >
                <Flex ml={1}>
                  <Flex
                    width={1}
                    flexWrap={['wrap', 'initial', 'initial', 'initial', 'initial']}
                    flexDirection={['row-reverse', 'column', 'column', 'column', 'column']}
                    mb={[3, 0, 0, 0, 0]}
                  >
                    {renderFieldLabel(worker.fields)}
                  </Flex>
                </Flex>
                <Flex flexDirection="column" justifyContent="space-between">
                  <Flex alignItems="center" flexDirection="row-reverse" pb={2} px={2} width={1}>
                    <Flex flexDirection="row" alignItems="center">
                      <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                        {`[ ${worker.username} ] - ${worker.name}`}
                      </Text>
                    </Flex>
                  </Flex>
                  <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                    <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      المدينة :
                    </SubTitle>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      {CITIES[worker.city] ? CITIES[worker.city].ar : 'غير معرف'}
                    </Text>
                  </Flex>
                  <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                    <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      عدد الشكاوي :
                    </SubTitle>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      {worker.ticketsCount}
                    </Text>
                  </Flex>

                </Flex>
              </CardBody>
            </Card>
          </Box>
          ))
      )
    } else {
      contentRenderer =  (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text="لا يوجد سجلات"
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>تقرير اكثر الفنيين شكاوي</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تقرير اكثر الفنيين شكاوي
          </Text>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleTheMostWorkersHasTicketsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز
              </Text>
            </Button>
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="theMostWorkersHasTickets"
          filterSections={['city', 'field', 'dates']}
          header="فرز"
          filterQueries={filterTheMostWorkersHasTicketsQueries}
          isOpened={isFilterTheMostWorkersHasTicketsOpen}
          filterFunction={getTheMostWorkersHasTicketsAction}
          toggleFilter={this.handleToggleTheMostWorkersHasTicketsFilterModal}
          handleChangeFilterQueries={this.handleChangeTheMostWorkersHasTicketsFilterQueries}
        />
        <Card
          minHeight={500}
          m={2}
          p={2}
          flexDirection="column"
          bgColor={COLORS_VALUES[GREY_DARK]}
        >
          <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
            {contentRenderer}
          </Flex>
        </Card>
      </Flex>
    );
  }
}
TheMostWorkersHasTickets.displayName = 'TheMostWorkersHasTickets';

const mapStateToProps = state => ({
  user: state.user.user,
  theMostWorkersHasTickets: state.dashboard.theMostWorkersHasTickets,
  isGetTheMostWorkersHasTicketsFetching: state.dashboard.getTheMostWorkersHasTickets.isFetching,
  isGetTheMostWorkersHasTicketsSuccess: state.dashboard.getTheMostWorkersHasTickets.isSuccess,
  isGetTheMostWorkersHasTicketsError: state.dashboard.getTheMostWorkersHasTickets.isFail.isError,
  getTheMostWorkersHasTicketsErrorMessage: state.dashboard.getTheMostWorkersHasTickets.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTheMostWorkersHasTickets,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(TheMostWorkersHasTickets);
