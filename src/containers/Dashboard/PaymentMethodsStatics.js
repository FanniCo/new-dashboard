import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faBoxes, faChartPie, faFile, faFilter, faTable } from '@fortawesome/free-solid-svg-icons';
import { getPaymentMethodsStatics } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import PaymentMethodsStatics from 'components/Dashboard/PaymentMethodsStatics';







const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;


const showChart = (event) => {
  document.body.classList.add('show-chart');
}
const hideChart = (event) => {
  document.body.classList.remove('show-chart');
}

class PaymentMethodsStaticsContainer extends Component {
  static propTypes = {
    isGetPaymentMethodsStaticsFetching: PropTypes.bool,
    isGetPaymentMethodsStaticsSuccess: PropTypes.bool,
    isGetPaymentMethodsStaticsError: PropTypes.bool,
    paymentMethodsStatics: PropTypes.shape({}),
    getPaymentMethodsStaticsErrorMessage: PropTypes.string,
    getPaymentMethodsStatics: PropTypes.func,
  };

  static defaultProps = {
    isGetPaymentMethodsStaticsFetching: false,
    isGetPaymentMethodsStaticsSuccess: false,
    isGetPaymentMethodsStaticsError: false,
    paymentMethodsStatics: undefined,
    getPaymentMethodsStaticsErrorMessage: undefined,
    getPaymentMethodsStatics: () => {},
  };

  constructor() {
    document.body.classList.remove('show-chart');
    document.body.classList.remove('show-line');
    document.body.classList.remove('show-pie');
    super();

    this.state = {
      filterPaymentMethodsStaticsQueries: {},
      isFilterPaymentMethodsStaticsOpen: false,
    };
  }

  componentDidMount() {
    const { getPaymentMethodsStatics: getPaymentMethodsStaticsAction } = this.props;
    const { filterPaymentMethodsStaticsQueries } = this.state;

    getPaymentMethodsStaticsAction(filterPaymentMethodsStaticsQueries, true);
  }

  handleTogglePaymentMethodsStaticsFilterModal = () => {
    const { isFilterPaymentMethodsStaticsOpen } = this.state;

    this.setState({ isFilterPaymentMethodsStaticsOpen: !isFilterPaymentMethodsStaticsOpen });
  };

  handleChangePaymentMethodsStaticsFilterQueries = (type, val) => {
    const { filterPaymentMethodsStaticsQueries } = this.state;

    this.setState({
      filterPaymentMethodsStaticsQueries: { ...filterPaymentMethodsStaticsQueries, [type]: val },
    });
  };


  

  render() {
    const {
      isGetPaymentMethodsStaticsFetching,
      isGetPaymentMethodsStaticsSuccess,
      isGetPaymentMethodsStaticsError,
      paymentMethodsStatics,
      getPaymentMethodsStaticsErrorMessage,
      getPaymentMethodsStatics: getPaymentMethodsStaticsAction,
    } = this.props;
    const { isFilterPaymentMethodsStaticsOpen, filterPaymentMethodsStaticsQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetPaymentMethodsStaticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetPaymentMethodsStaticsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getPaymentMethodsStaticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetPaymentMethodsStaticsSuccess && (
        <>
          <Box width={1} m={1} className='StatictesBoxTitle'>
          <Text className='StatictesTopTitle'>
          إحصائيات وسائل الدفع
        </Text>

        <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleTogglePaymentMethodsStaticsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton'
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز المستخدمين
              </Text>
            </Button>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={showChart}
              icon={faChartPie}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton pie-view'
            >
             
            </Button>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={hideChart}
              icon={faTable}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton box-view'
            >
              
            </Button>
            <Filter
              cachedFilterPage="paymentMethodStatics"
              filterSections={['dates']}
              header="فرز المستخدمين"
              filterQueries={filterPaymentMethodsStaticsQueries}
              isOpened={isFilterPaymentMethodsStaticsOpen}
              filterFunction={getPaymentMethodsStaticsAction}
              toggleFilter={this.handleTogglePaymentMethodsStaticsFilterModal}
              handleChangeFilterQueries={this.handleChangePaymentMethodsStaticsFilterQueries}
            />
          </Box>
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
            className='StatisticsContainer RequestsStatistics'
          >
            <PaymentMethodsStatics paymentMethodsStatics={paymentMethodsStatics} />
          </Card>
        </>
      )
    );
  }
}
PaymentMethodsStaticsContainer.displayName = 'PaymentMethodsStaticsContainer';

const mapStateToProps = state => ({
  paymentMethodsStatics: state.dashboard.paymentMethodsStatics,
  isGetPaymentMethodsStaticsFetching: state.dashboard.getPaymentMethodsStatics.isFetching,
  isGetPaymentMethodsStaticsSuccess: state.dashboard.getPaymentMethodsStatics.isSuccess,
  isGetPaymentMethodsStaticsError: state.dashboard.getPaymentMethodsStatics.isFail.isError,
  getPaymentMethodsStaticsErrorMessage: state.dashboard.getPaymentMethodsStatics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPaymentMethodsStatics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  PaymentMethodsStaticsContainer,
);
