import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faDownload, faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getFinancialByCities } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import FinancialByCities from 'components/Dashboard/FinancialByCities';
import { Urls } from 'utils/api';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class FinancialByCitiesContainer extends Component {
  static propTypes = {
    isGetFinancialByCitiesFetching: PropTypes.bool,
    isGetFinancialByCitiesSuccess: PropTypes.bool,
    isGetFinancialByCitiesError: PropTypes.bool,
    getFinancialByCitiesErrorMessage: PropTypes.string,
    financialByCities: PropTypes.arrayOf(PropTypes.shape({})),
    getFinancialByCities: PropTypes.func,

    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    permissions: PropTypes.arrayOf(PropTypes.string),
  };

  static defaultProps = {
    isGetFinancialByCitiesFetching: false,
    isGetFinancialByCitiesSuccess: false,
    isGetFinancialByCitiesError: false,
    getFinancialByCitiesErrorMessage: false,
    financialByCities: [],
    getFinancialByCities: () => {},

    isExportFileFetching: false,
    isExportFileSuccess: false,
    permissions: [],
  };

  constructor(props) {
    super(props);
    const cachedFinancialByCitiesFilterState = JSON.parse(
      sessionStorage.getItem('financialByCitiesFilterState'),
    );

    let filterFinancialByCitiesQueries = {};

    if (cachedFinancialByCitiesFilterState) {
      const {
        filterQueries: cachedFinancialByCitiesFilterQueries,
      } = cachedFinancialByCitiesFilterState;

      filterFinancialByCitiesQueries = cachedFinancialByCitiesFilterQueries;
    }

    this.state = {
      isFilterFinancialByCitiesOpen: false,
      filterFinancialByCitiesQueries,
    };
  }

  componentDidMount() {
    const { getFinancialByCities: getFinancialByCitiesAction } = this.props;
    const { filterFinancialByCitiesQueries } = this.state;

    getFinancialByCitiesAction(filterFinancialByCitiesQueries, true);
  }

  handleChangeFinancialByCitiesFilterQueries = (type, val) => {
    const { filterFinancialByCitiesQueries } = this.state;

    this.setState({
      filterFinancialByCitiesQueries: { ...filterFinancialByCitiesQueries, [type]: val },
    });
  };

  handleToggleFinancialByCitiesFilterModal = () => {
    const { isFilterFinancialByCitiesOpen } = this.state;

    this.setState({ isFilterFinancialByCitiesOpen: !isFilterFinancialByCitiesOpen });
  };

  handleExportFile = (url, filterState) => {
    let targetedUrl = url;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem(filterState));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          targetedUrl = `${targetedUrl}?${key}=${cachedFilterQueries[key]}`;
        } else {
          targetedUrl = `${targetedUrl}&${key}=${cachedFilterQueries[key]}`;
        }
      });
    }
    exportFileAction(targetedUrl);
  };

  render() {
    const {
      permissions,
      isGetFinancialByCitiesFetching,
      isGetFinancialByCitiesSuccess,
      isGetFinancialByCitiesError,
      getFinancialByCitiesErrorMessage,
      financialByCities,
      getFinancialByCities: getFinancialByCitiesAction,

      isExportFileFetching,
      isExportFileSuccess,
    } = this.props;
    const { isFilterFinancialByCitiesOpen, filterFinancialByCitiesQueries } = this.state;
    const { reports } = Urls;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetFinancialByCitiesFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetFinancialByCitiesError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getFinancialByCitiesErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetFinancialByCitiesSuccess && (
        <>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFinancialByCitiesFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Download Financial By Cities') && (
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={() =>
                  this.handleExportFile(
                    reports.downloadFinancialByCities,
                    'financialByCitiesFilterState',
                  )
                }
                icon={faDownload}
                iconWidth="sm"
                xMargin={3}
                isLoading={isExportFileFetching && !isExportFileSuccess}
                disable={isExportFileFetching && !isExportFileSuccess}
              >
                <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  تصدير الي ملف
                </Text>
              </Button>
            )}
            <Filter
              cachedFilterPage="financialByCities"
              filterSections={['dates']}
              header="فرز"
              filterQueries={filterFinancialByCitiesQueries}
              isOpened={isFilterFinancialByCitiesOpen}
              filterFunction={getFinancialByCitiesAction}
              toggleFilter={this.handleToggleFinancialByCitiesFilterModal}
              handleChangeFilterQueries={this.handleChangeFinancialByCitiesFilterQueries}
            />
          </Flex>
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <FinancialByCities financialByCities={financialByCities} />
          </Card>
        </>
      )
    );
  }
}
FinancialByCitiesContainer.displayName = 'FinancialByCitiesContainer';

const mapStateToProps = state => ({
  isGetFinancialByCitiesFetching: state.dashboard.getFinancialByCities.isFetching,
  isGetFinancialByCitiesSuccess: state.dashboard.getFinancialByCities.isSuccess,
  isGetFinancialByCitiesError: state.dashboard.getFinancialByCities.isFail.isError,
  getFinancialByCitiesErrorMessage: state.dashboard.getFinancialByCities.isFail.message,
  financialByCities: state.dashboard.financialByCities,

  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFinancialByCities,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(FinancialByCitiesContainer);
