import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getWorkersInEachField } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import WorkersInEachField from 'components/Dashboard/WorkersInEachField';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class WorkersInEachFieldContainer extends Component {
  static propTypes = {
    isGetWorkersForEachFieldFetching: PropTypes.bool,
    isGetWorkersForEachFieldSuccess: PropTypes.bool,
    isGetWorkersForEachFieldError: PropTypes.bool,
    getWorkersForEachFieldErrorMessage: PropTypes.string,
    WorkersForEachField: PropTypes.shape({}),

    getWorkersInEachField: PropTypes.func,
  };

  static defaultProps = {
    isGetWorkersForEachFieldFetching: false,
    isGetWorkersForEachFieldSuccess: false,
    isGetWorkersForEachFieldError: false,
    getWorkersForEachFieldErrorMessage: undefined,
    WorkersForEachField: {},

    getWorkersInEachField: () => {},
  };

  constructor(props) {
    super(props);
    const cachedWorkersInEachFieldFilterState = JSON.parse(
      sessionStorage.getItem('workersForEachFieldFilterState'),
    );

    let filterWorkersInEachFieldQueries = {};

    if (cachedWorkersInEachFieldFilterState) {
      const {
        filterQueries: cachedWorkersInEachFieldQueries,
      } = cachedWorkersInEachFieldFilterState;

      filterWorkersInEachFieldQueries = cachedWorkersInEachFieldQueries;
    }

    this.state = {
      isFilterWorkersInEachFieldOpen: false,
      filterWorkersInEachFieldQueries,
    };
  }

  componentDidMount() {
    const { getWorkersInEachField: getWorkersInEachFieldAction } = this.props;
    const { filterWorkersInEachFieldQueries } = this.state;

    getWorkersInEachFieldAction(filterWorkersInEachFieldQueries, true);
  }

  handleChangeWorkersInEachFieldFilterQueries = (type, val) => {
    const { filterWorkersInEachFieldQueries } = this.state;

    this.setState({
      filterWorkersInEachFieldQueries: { ...filterWorkersInEachFieldQueries, [type]: val },
    });
  };

  handleToggleWorkersInEachFieldOpenFilterModal = () => {
    const { isFilterWorkersInEachFieldOpen } = this.state;

    this.setState({ isFilterWorkersInEachFieldOpen: !isFilterWorkersInEachFieldOpen });
  };

  render() {
    const {
      isGetWorkersForEachFieldFetching,
      isGetWorkersForEachFieldSuccess,
      isGetWorkersForEachFieldError,
      getWorkersForEachFieldErrorMessage,
      WorkersForEachField,

      getWorkersInEachField: getWorkersInEachFieldAction,
    } = this.props;
    const { isFilterWorkersInEachFieldOpen, filterWorkersInEachFieldQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetWorkersForEachFieldFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetWorkersForEachFieldError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkersForEachFieldErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetWorkersForEachFieldSuccess && (
        <>
          <Button
            ml={1}
            mr={1}
            color={PRIMARY}
            onClick={this.handleToggleWorkersInEachFieldOpenFilterModal}
            icon={faFilter}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
          >
            <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              فرز
            </Text>
          </Button>
          <Filter
            cachedFilterPage="workersForEachField"
            filterSections={['dates', 'city']}
            header="فرز"
            filterQueries={filterWorkersInEachFieldQueries}
            isOpened={isFilterWorkersInEachFieldOpen}
            filterFunction={getWorkersInEachFieldAction}
            toggleFilter={this.handleToggleWorkersInEachFieldOpenFilterModal}
            handleChangeFilterQueries={this.handleChangeWorkersInEachFieldFilterQueries}
          />
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <WorkersInEachField WorkersForEachField={WorkersForEachField} />
          </Card>
        </>
      )
    );
  }
}
WorkersInEachFieldContainer.displayName = 'WorkersInEachFieldContainer';

const mapStateToProps = state => ({
  isGetWorkersForEachFieldFetching: state.dashboard.getWorkersForEachField.isFetching,
  isGetWorkersForEachFieldSuccess: state.dashboard.getWorkersForEachField.isSuccess,
  isGetWorkersForEachFieldError: state.dashboard.getWorkersForEachField.isFail.isError,
  getWorkersForEachFieldErrorMessage: state.dashboard.getWorkersForEachField.isFail.message,
  WorkersForEachField: state.dashboard.WorkersForEachField,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWorkersInEachField,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(WorkersInEachFieldContainer);
