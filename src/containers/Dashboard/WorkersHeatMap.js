import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { getWorkersHeatMap } from 'redux-modules/dashboard/actions';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import WorkersHeatMap from 'components/Dashboard/WorkersHeatMap';

const { GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class WorkersHeatMapContainer extends Component {
  static propTypes = {
    isGetWorkersHeatMapFetching: PropTypes.bool,
    isGetWorkersHeatMapSuccess: PropTypes.bool,
    isGetWorkersHeatMapError: PropTypes.bool,
    getWorkersHeatMapErrorMessage: PropTypes.string,
    workersLocation: PropTypes.arrayOf(PropTypes.shape({})),
    getWorkersHeatMap: PropTypes.func,
  };

  static defaultProps = {
    isGetWorkersHeatMapFetching: false,
    isGetWorkersHeatMapSuccess: false,
    isGetWorkersHeatMapError: false,
    getWorkersHeatMapErrorMessage: undefined,
    workersLocation: [],
    getWorkersHeatMap: () => {},
  };

  componentDidMount() {
    const { getWorkersHeatMap: getWorkersHeatMapAction } = this.props;

    getWorkersHeatMapAction();
  }

  render() {
    const {
      isGetWorkersHeatMapFetching,
      isGetWorkersHeatMapSuccess,
      isGetWorkersHeatMapError,
      getWorkersHeatMapErrorMessage,
      workersLocation,
    } = this.props;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetWorkersHeatMapFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetWorkersHeatMapError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkersHeatMapErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetWorkersHeatMapSuccess && (
        <Card minHeight={500} m={2} p={2} flexDirection="column" bgColor={COLORS_VALUES[GREY_DARK]}>
          <WorkersHeatMap
            isGetWorkersHeatMapFetching={isGetWorkersHeatMapFetching}
            locations={workersLocation}
          />
        </Card>
      )
    );
  }
}
WorkersHeatMapContainer.displayName = 'WorkersHeatMapContainer';

const mapStateToProps = state => ({
  isGetWorkersHeatMapFetching: state.dashboard.getWorkersHeatMap.isFetching,
  isGetWorkersHeatMapSuccess: state.dashboard.getWorkersHeatMap.isSuccess,
  isGetWorkersHeatMapError: state.dashboard.getWorkersHeatMap.isFail.isError,
  getWorkersHeatMapErrorMessage: state.dashboard.getWorkersHeatMap.isFail.message,
  workersLocation: state.dashboard.workersLocation,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWorkersHeatMap,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(WorkersHeatMapContainer);
