import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getUsersForEachTypeCount } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import UsersCount from 'components/Dashboard/UsersCount';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class UsersCountContainer extends Component {
  static propTypes = {
    isGetUsersForEachTypeCountFetching: PropTypes.bool,
    isGetUsersForEachTypeCountSuccess: PropTypes.bool,
    isGetUsersForEachTypeCountError: PropTypes.bool,
    UsersForEachTypeCount: PropTypes.shape({}),
    getUsersForEachTypeCountErrorMessage: PropTypes.string,

    getUsersForEachTypeCount: PropTypes.func,
  };

  static defaultProps = {
    isGetUsersForEachTypeCountFetching: false,
    isGetUsersForEachTypeCountSuccess: false,
    isGetUsersForEachTypeCountError: false,
    UsersForEachTypeCount: undefined,
    getUsersForEachTypeCountErrorMessage: undefined,

    getUsersForEachTypeCount: () => {},
  };

  constructor() {
    super();

    this.state = {
      isFilterUsersCountForEachTypeOpen: false,
    };
  }

  componentDidMount() {
    const { getUsersForEachTypeCount: getUsersForEachTypeCountAction } = this.props;
    const { filterUsersCountForEachTypeQueries } = this.state;

    getUsersForEachTypeCountAction(
      { role: 'customer,worker', ...filterUsersCountForEachTypeQueries },
      true,
    );
  }

  handleToggleUserCountForEachTypeFilterModal = () => {
    const { isFilterUsersCountForEachTypeOpen } = this.state;

    this.setState({ isFilterUsersCountForEachTypeOpen: !isFilterUsersCountForEachTypeOpen });
  };

  handleChangeUsersCountForEachTypeFilterQueries = (type, val) => {
    const { filterUsersCountForEachTypeQueries } = this.state;

    this.setState({
      filterUsersCountForEachTypeQueries: { ...filterUsersCountForEachTypeQueries, [type]: val },
    });
  };

  render() {
    const {
      isGetUsersForEachTypeCountFetching,
      isGetUsersForEachTypeCountSuccess,
      isGetUsersForEachTypeCountError,
      UsersForEachTypeCount,
      getUsersForEachTypeCountErrorMessage,
      getUsersForEachTypeCount: getUsersForEachTypeCountAction,
    } = this.props;
    const { isFilterUsersCountForEachTypeOpen, filterUsersCountForEachTypeQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetUsersForEachTypeCountFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetUsersForEachTypeCountError) {
      return (
        <Box width={1} className='RequestCounts'>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getUsersForEachTypeCountErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetUsersForEachTypeCountSuccess && (
        <>
        <Box className='StatictesBoxTitle'>
        <Text className='StatictesTopTitle'>
          إحصائيات المستخدمين
        </Text>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleUserCountForEachTypeFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              className='FilterButton'
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز المستخدمين
              </Text>
            </Button>
            </Box>
            <Filter
              cachedFilterPage="usersCountForEachType"
              filterSections={['dates']}
              header="فرز المستخدمين"
              filterQueries={filterUsersCountForEachTypeQueries}
              isOpened={isFilterUsersCountForEachTypeOpen}
              filterFunction={getUsersForEachTypeCountAction}
              toggleFilter={this.handleToggleUserCountForEachTypeFilterModal}
              handleChangeFilterQueries={this.handleChangeUsersCountForEachTypeFilterQueries}
            />
          
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
            className="StatisticsContainer RequestsStatistics"
          >
            <UsersCount UsersForEachTypeCount={UsersForEachTypeCount} />
          </Card>
        </>
      )
    );
  }
}
UsersCountContainer.displayName = 'UsersCountContainer';

const mapStateToProps = state => ({
  UsersForEachTypeCount: state.dashboard.UsersForEachTypeCount,
  isGetUsersForEachTypeCountFetching: state.dashboard.getUsersForEachTypeCount.isFetching,
  isGetUsersForEachTypeCountSuccess: state.dashboard.getUsersForEachTypeCount.isSuccess,
  isGetUsersForEachTypeCountError: state.dashboard.getUsersForEachTypeCount.isFail.isError,
  getUsersForEachTypeCountErrorMessage: state.dashboard.getUsersForEachTypeCount.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersForEachTypeCount,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(UsersCountContainer);
