import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faDownload, faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getFinancialByFields } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import { Urls } from 'utils/api';
import FinancialByFields from 'components/Dashboard/FinancialByFields';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class FinancialByCitiesContainer extends Component {
  static propTypes = {
    isGetFinancialByFieldsFetching: PropTypes.bool,
    isGetFinancialByFieldsSuccess: PropTypes.bool,
    isGetFinancialByFieldsError: PropTypes.bool,
    getFinancialByFieldsErrorMessage: PropTypes.string,
    financialByFields: PropTypes.arrayOf(PropTypes.shape({})),

    getFinancialByFields: PropTypes.func,

    isExportFileFetching: PropTypes.bool,
    isExportFileSuccess: PropTypes.bool,
    permissions: PropTypes.arrayOf(PropTypes.string),
  };

  static defaultProps = {
    isGetFinancialByFieldsFetching: false,
    isGetFinancialByFieldsSuccess: false,
    isGetFinancialByFieldsError: false,
    getFinancialByFieldsErrorMessage: undefined,
    financialByFields: [],

    getFinancialByFields: () => {},

    isExportFileFetching: false,
    isExportFileSuccess: false,
    permissions: [],
  };

  constructor(props) {
    super(props);
    const cachedFinancialByFieldsFilterState = JSON.parse(
      sessionStorage.getItem('financialByFieldsFilterState'),
    );

    let filterFinancialByFieldsQueries = {};

    if (cachedFinancialByFieldsFilterState) {
      const {
        filterQueries: cachedFinancialByFieldsFilterQueries,
      } = cachedFinancialByFieldsFilterState;

      filterFinancialByFieldsQueries = cachedFinancialByFieldsFilterQueries;
    }

    this.state = {
      isFilterFinancialByFieldsOpen: false,
      filterFinancialByFieldsQueries,
    };
  }

  componentDidMount() {
    const { getFinancialByFields: getFinancialByFieldsAction } = this.props;
    const { filterFinancialByFieldsQueries } = this.state;

    getFinancialByFieldsAction(filterFinancialByFieldsQueries, true);
  }

  handleChangeFinancialByFieldsFilterQueries = (type, val) => {
    const { filterFinancialByFieldsQueries } = this.state;

    this.setState({
      filterFinancialByFieldsQueries: { ...filterFinancialByFieldsQueries, [type]: val },
    });
  };

  handleToggleFinancialByFieldsFilterModal = () => {
    const { isFilterFinancialByFieldsOpen } = this.state;

    this.setState({ isFilterFinancialByFieldsOpen: !isFilterFinancialByFieldsOpen });
  };

  handleExportFile = (url, filterState) => {
    let targetedUrl = url;
    const { exportFile: exportFileAction } = this.props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem(filterState));

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      delete cachedFilterQueries.skip;
      delete cachedFilterQueries.limit;
      Object.keys(cachedFilterQueries).forEach((key, index) => {
        if (index === 0) {
          targetedUrl = `${targetedUrl}?${key}=${cachedFilterQueries[key]}`;
        } else {
          targetedUrl = `${targetedUrl}&${key}=${cachedFilterQueries[key]}`;
        }
      });
    }
    exportFileAction(targetedUrl);
  };

  render() {
    const {
      permissions,
      isGetFinancialByFieldsFetching,
      isGetFinancialByFieldsSuccess,
      isGetFinancialByFieldsError,
      getFinancialByFieldsErrorMessage,
      financialByFields,

      getFinancialByFields: getFinancialByFieldsAction,

      isExportFileFetching,
      isExportFileSuccess,
    } = this.props;
    const { isFilterFinancialByFieldsOpen, filterFinancialByFieldsQueries } = this.state;
    const { reports } = Urls;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetFinancialByFieldsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetFinancialByFieldsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getFinancialByFieldsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetFinancialByFieldsSuccess && (
        <>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFinancialByFieldsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز الخدمات
              </Text>
            </Button>
            {permissions && permissions.includes('Download Financial By Fields') && (
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={() =>
                  this.handleExportFile(
                    reports.downloadFinancialByFields,
                    'financialByFieldsFilterState',
                  )
                }
                icon={faDownload}
                iconWidth="sm"
                xMargin={3}
                isLoading={isExportFileFetching && !isExportFileSuccess}
                disable={isExportFileFetching && !isExportFileSuccess}
              >
                تصدير الي ملف
              </Button>
            )}
            <Filter
              cachedFilterPage="financialByFields"
              filterSections={['dates']}
              header="فرز"
              filterQueries={filterFinancialByFieldsQueries}
              isOpened={isFilterFinancialByFieldsOpen}
              filterFunction={getFinancialByFieldsAction}
              toggleFilter={this.handleToggleFinancialByFieldsFilterModal}
              handleChangeFilterQueries={this.handleChangeFinancialByFieldsFilterQueries}
            />
          </Flex>
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <FinancialByFields financialByFields={financialByFields} />
          </Card>
        </>
      )
    );
  }
}
FinancialByCitiesContainer.displayName = 'FinancialByCitiesContainer';

const mapStateToProps = state => ({
  isGetFinancialByFieldsFetching: state.dashboard.getFinancialByFields.isFetching,
  isGetFinancialByFieldsSuccess: state.dashboard.getFinancialByFields.isSuccess,
  isGetFinancialByFieldsError: state.dashboard.getFinancialByFields.isFail.isError,
  getFinancialByFieldsErrorMessage: state.dashboard.getFinancialByFields.isFail.message,
  financialByFields: state.dashboard.financialByFields,

  isExportFileFetching: state.user.exportFile.isFetching,
  isExportFileSuccess: state.user.exportFile.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFinancialByFields,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(FinancialByCitiesContainer);
