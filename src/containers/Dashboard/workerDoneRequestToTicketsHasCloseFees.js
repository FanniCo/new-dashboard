import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import {
  faFile,
  faFilter, faSpinner
} from '@fortawesome/free-solid-svg-icons';
import { getWorkerDoneRequestToTicketsHasCloseFees } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import {
  CardBody,
  CardLabel,
  SubTitle
} from "components/shared";
import {CITIES, FIELDS, } from "utils/constants";
import {getFieldIcon} from "utils/shared/style";
import {
  FontAwesomeIcon
} from "@fortawesome/react-fontawesome";
import InfiniteScroll from 'react-infinite-scroll-component';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE, BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;


class WorkerDoneRequestToTicketsHasCloseFees extends Component {
  static propTypes = {
    workerDoneRequestToTicketsHasCloseFees: PropTypes.arrayOf(PropTypes.shape({})),
    isGetWorkerDoneRequestToTicketsHasCloseFeesFetching: PropTypes.bool,
    isGetWorkerDoneRequestToTicketsHasCloseFeesSuccess: PropTypes.bool,
    isGetWorkerDoneRequestToTicketsHasCloseFeesError: PropTypes.bool,
    getWorkerDoneRequestToTicketsHasCloseFeesErrorMessage: PropTypes.string,

    getWorkerDoneRequestToTicketsHasCloseFees: PropTypes.func,
    hasMoreWorkerDoneRequestToTicketsHasCloseFees: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
  };

  static defaultProps = {
    workerDoneRequestToTicketsHasCloseFees: undefined,
    isGetWorkerDoneRequestToTicketsHasCloseFeesFetching: false,
    isGetWorkerDoneRequestToTicketsHasCloseFeesSuccess: false,
    isGetWorkerDoneRequestToTicketsHasCloseFeesError: false,
    getWorkerDoneRequestToTicketsHasCloseFeesErrorMessage: undefined,
    getWorkerDoneRequestToTicketsHasCloseFees: () => {},
    hasMoreWorkerDoneRequestToTicketsHasCloseFees: true,
    applyNewFilter: false,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem(`workerDoneRequestToTicketsHasCloseFeesFilterState`));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    let filterWorkerDoneRequestToTicketsHasCloseFeesQueries;
    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;
      filterWorkerDoneRequestToTicketsHasCloseFeesQueries = Object.assign({}, filterQueries, cachedFilterQueries);
    }

    this.state = {
      isFilterWorkerDoneRequestToTicketsHasCloseFeesOpen: false,
      filterWorkerDoneRequestToTicketsHasCloseFeesQueries: { ...filterWorkerDoneRequestToTicketsHasCloseFeesQueries, skip: 0, limit: 20 },
    };
  }

  componentDidMount() {
    const { getWorkerDoneRequestToTicketsHasCloseFees: getWorkerDoneRequestToTicketsHasCloseFeesAction } = this.props;
    const { filterWorkerDoneRequestToTicketsHasCloseFeesQueries } = this.state;

    getWorkerDoneRequestToTicketsHasCloseFeesAction(filterWorkerDoneRequestToTicketsHasCloseFeesQueries, true);
  }

  handleToggleWorkerDoneRequestToTicketsHasCloseFeesFilterModal = () => {
    const { isFilterWorkerDoneRequestToTicketsHasCloseFeesOpen } = this.state;

    this.setState({ isFilterWorkerDoneRequestToTicketsHasCloseFeesOpen: !isFilterWorkerDoneRequestToTicketsHasCloseFeesOpen });
  };

  handleChangeWorkerDoneRequestToTicketsHasCloseFeesFilterQueries = (type, val) => {
    const { filterWorkerDoneRequestToTicketsHasCloseFeesQueries } = this.state;

    this.setState({
      filterWorkerDoneRequestToTicketsHasCloseFeesQueries: { ...filterWorkerDoneRequestToTicketsHasCloseFeesQueries, [type]: val },
    });
  };



  render() {
    const {
      isGetWorkerDoneRequestToTicketsHasCloseFeesFetching,
      isGetWorkerDoneRequestToTicketsHasCloseFeesSuccess,
      isGetWorkerDoneRequestToTicketsHasCloseFeesError,
      getWorkerDoneRequestToTicketsHasCloseFeesErrorMessage,
      workerDoneRequestToTicketsHasCloseFees,
      hasMoreWorkerDoneRequestToTicketsHasCloseFees,
      getWorkerDoneRequestToTicketsHasCloseFees: getWorkerDoneRequestToTicketsHasCloseFeesAction,
      applyNewFilter,
    } = this.props;
    const {
      isFilterWorkerDoneRequestToTicketsHasCloseFeesOpen,
      filterWorkerDoneRequestToTicketsHasCloseFeesQueries
    } = this.state;

    const renderFieldLabel = (fields) => fields.map(fieldKey => {
      const fieldIcon = getFieldIcon(fieldKey);

      return (
        <Flex key={fieldKey} mb={2} alignItems="center" justifyContent="flex-end">
          <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
          <CardLabel
            py={1}
            px={2}
            mx={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            {FIELDS[fieldKey].ar}
          </CardLabel>
        </Flex>
      );
    });

    const loadMoreWorkerDoneRequestToTicketsHasCloseFees = () => {
      const { getWorkerDoneRequestToTicketsHasCloseFees: getWorkerDoneRequestToTicketsHasCloseFeesAction } = this.props;

      const {
        filterWorkerDoneRequestToTicketsHasCloseFeesQueries,
        filterWorkerDoneRequestToTicketsHasCloseFeesQueries: { skip, limit },
      } = this.state;

      this.setState(
        {
          filterWorkerDoneRequestToTicketsHasCloseFeesQueries: { ...filterWorkerDoneRequestToTicketsHasCloseFeesQueries, skip: skip + 20, limit },
        },
        () => {
          const { filterWorkerDoneRequestToTicketsHasCloseFeesQueries: filterQueriesNextState } = this.state;

          getWorkerDoneRequestToTicketsHasCloseFeesAction(filterQueriesNextState);
        },
      );

      getWorkerDoneRequestToTicketsHasCloseFeesAction(filterWorkerDoneRequestToTicketsHasCloseFeesQueries, false);
    };

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    let contentRenderer;

    if ((isGetWorkerDoneRequestToTicketsHasCloseFeesFetching && (!workerDoneRequestToTicketsHasCloseFees || applyNewFilter)) ||
      (!isGetWorkerDoneRequestToTicketsHasCloseFeesFetching && !isGetWorkerDoneRequestToTicketsHasCloseFeesSuccess &&
        !isGetWorkerDoneRequestToTicketsHasCloseFeesError)
    ) {
      contentRenderer = createLoadingStatisticsList();
    } else if (isGetWorkerDoneRequestToTicketsHasCloseFeesError) {
      contentRenderer= (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getWorkerDoneRequestToTicketsHasCloseFeesErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }
    else if(workerDoneRequestToTicketsHasCloseFees.length) {
      contentRenderer = (
        <InfiniteScroll
          dataLength={workerDoneRequestToTicketsHasCloseFees.length}
          next={loadMoreWorkerDoneRequestToTicketsHasCloseFees}
          hasMore={hasMoreWorkerDoneRequestToTicketsHasCloseFees}
          loader={
            <Flex m={2} justifyContent="center" alignItems="center">
              <FontAwesomeIcon
                icon={faSpinner}
                size="lg"
                spin
                color={COLORS_VALUES[BRANDING_GREEN]}
              />
            </Flex>
          }
        >
          <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
            {workerDoneRequestToTicketsHasCloseFees.map((worker) => (
              <Box key={worker._id} width={[1, 1, 1, 1 / 2, 1 / 2, 0.3]} ml={2}>
                <Card
                  height="calc(100% - 16px)"
                  minHeight={100}
                  flexDirection="column"
                  justifyContent="space-between"
                  bgColor={COLORS_VALUES[GREY_LIGHT]}
                >
                  <CardBody
                    justifyContent="space-between"
                    flexDirection={['column', 'row', 'row', 'row', 'row']}
                    py={3}
                    px={1}
                  >
                    <Flex ml={1}>
                      <Flex
                        width={1}
                        flexWrap={['wrap', 'initial', 'initial', 'initial', 'initial']}
                        flexDirection={['row-reverse', 'column', 'column', 'column', 'column']}
                        mb={[3, 0, 0, 0, 0]}
                      >
                        {renderFieldLabel(worker.fields)}
                      </Flex>
                    </Flex>
                    <Flex flexDirection="column" justifyContent="space-between">
                      <Flex alignItems="center" flexDirection="row-reverse" pb={2} px={2} width={1}>
                        <Flex flexDirection="row" alignItems="center">
                          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                            {`[ ${worker.username} ] - ${worker.name}`}
                          </Text>
                        </Flex>
                      </Flex>
                      <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                        <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                          المدينة :
                        </SubTitle>
                        <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                          {CITIES[worker.city] ? CITIES[worker.city].ar : 'غير معرف'}
                        </Text>
                      </Flex>
                      <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                        <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                          عدد الطلبات المكتملة :
                        </SubTitle>
                        <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                          {worker.doneRequests}
                        </Text>
                      </Flex>
                      <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                        <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                          عدد الشكاوي برسوم غلق/عدد الطلبات المكتملة :
                        </SubTitle>
                        <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                          {(worker.ticketsHasFees / (worker.doneRequests || 1)).toFixed(0)}
                        </Text>
                      </Flex>

                    </Flex>
                  </CardBody>
                </Card>
              </Box>
            ))}
          </Flex>
        </InfiniteScroll>
      );
    }
    else {
      contentRenderer = (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text="لا يوجد سجلات"
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      <>
        <Flex width={1} m={1}>
          <Button
            ml={1}
            mr={1}
            color={PRIMARY}
            onClick={this.handleToggleWorkerDoneRequestToTicketsHasCloseFeesFilterModal}
            icon={faFilter}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
          >
            <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              فرز
            </Text>
          </Button>
          <Filter
            cachedFilterPage="workerDoneRequestToTicketsHasCloseFees"
            filterSections={['city', 'field', 'dates']}
            header="فرز"
            filterQueries={filterWorkerDoneRequestToTicketsHasCloseFeesQueries}
            isOpened={isFilterWorkerDoneRequestToTicketsHasCloseFeesOpen}
            filterFunction={getWorkerDoneRequestToTicketsHasCloseFeesAction}
            toggleFilter={this.handleToggleWorkerDoneRequestToTicketsHasCloseFeesFilterModal}
            handleChangeFilterQueries={this.handleChangeWorkerDoneRequestToTicketsHasCloseFeesFilterQueries}
          />
        </Flex>
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              m={2}
              p={2}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              {contentRenderer}
            </Card>
          </Box>
        </Box>

      </>
    );
  }
}
WorkerDoneRequestToTicketsHasCloseFees.displayName = 'WorkerDoneRequestToTicketsHasCloseFees';

const mapStateToProps = state => ({
  workerDoneRequestToTicketsHasCloseFees: state.dashboard.workerDoneRequestToTicketsHasCloseFees,
  hasMoreWorkerDoneRequestToTicketsHasCloseFees: state.dashboard.workerDoneRequestToTicketsHasCloseFees.hasMoreWorkerDoneRequestToTicketsHasCloseFees,
  applyNewFilter: state.dashboard.applyNewFilter,
  isGetWorkerDoneRequestToTicketsHasCloseFeesFetching: state.dashboard.getWorkerDoneRequestToTicketsHasCloseFees.isFetching,
  isGetWorkerDoneRequestToTicketsHasCloseFeesSuccess: state.dashboard.getWorkerDoneRequestToTicketsHasCloseFees.isSuccess,
  isGetWorkerDoneRequestToTicketsHasCloseFeesError: state.dashboard.getWorkerDoneRequestToTicketsHasCloseFees.isFail.isError,
  getWorkerDoneRequestToTicketsHasCloseFeesErrorMessage: state.dashboard.getWorkerDoneRequestToTicketsHasCloseFees.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getWorkerDoneRequestToTicketsHasCloseFees,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(WorkerDoneRequestToTicketsHasCloseFees);
