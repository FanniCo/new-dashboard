import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { faFile, faFilter } from '@fortawesome/free-solid-svg-icons';
import { getRechargeMethodsStatics } from 'redux-modules/dashboard/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { Box, Flex } from '@rebass/grid';
import EmptyState from 'components/EmptyState';
import RechargeMethodsStatics from 'components/Dashboard/RechargeMethodsStatics';

const { DISABLED, PRIMARY, GREY_DARK, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class ReChargeMethodsStaticsContainer extends Component {
  static propTypes = {
    isGetRechargeMethodsStaticsFetching: PropTypes.bool,
    isGetRechargeMethodsStaticsSuccess: PropTypes.bool,
    isGetRechargeMethodsStaticsError: PropTypes.bool,
    rechargeMethodsStatics: PropTypes.shape({}),
    getRechargeMethodsStaticsErrorMessage: PropTypes.string,
    getRechargeMethodsStatics: PropTypes.func,
  };

  static defaultProps = {
    isGetRechargeMethodsStaticsFetching: false,
    isGetRechargeMethodsStaticsSuccess: false,
    isGetRechargeMethodsStaticsError: false,
    rechargeMethodsStatics: undefined,
    getRechargeMethodsStaticsErrorMessage: undefined,
    getRechargeMethodsStatics: () => {},
  };

  constructor() {
    super();

    this.state = {
      filterRechargeMethodsStaticsQueries: {},
      isFilterRechargeMethodsStaticsOpen: false,
    };
  }

  componentDidMount() {
    const { getRechargeMethodsStatics: getRechargeMethodsStaticsAction } = this.props;
    const { filterRechargeMethodsStaticsQueries } = this.state;

    getRechargeMethodsStaticsAction(filterRechargeMethodsStaticsQueries, true);
  }

  handleToggleRechargeMethodsStaticsFilterModal = () => {
    const { isFilterRechargeMethodsStaticsOpen } = this.state;

    this.setState({ isFilterRechargeMethodsStaticsOpen: !isFilterRechargeMethodsStaticsOpen });
  };

  handleChangeRechargeMethodsStaticsFilterQueries = (type, val) => {
    const { filterRechargeMethodsStaticsQueries } = this.state;

    this.setState({
      filterRechargeMethodsStaticsQueries: { ...filterRechargeMethodsStaticsQueries, [type]: val },
    });
  };

  render() {
    const {
      isGetRechargeMethodsStaticsFetching,
      isGetRechargeMethodsStaticsSuccess,
      isGetRechargeMethodsStaticsError,
      rechargeMethodsStatics,
      getRechargeMethodsStaticsErrorMessage,
      getRechargeMethodsStatics: getRechargeMethodsStaticsAction,
    } = this.props;
    const { isFilterRechargeMethodsStaticsOpen, filterRechargeMethodsStaticsQueries } = this.state;

    /**
     * Creates lazy loading statistic block
     */
    const getLoadingStatistics = key => (
      <ShimmerEffect width={1} key={key}>
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
        <Rect width={1 / 3} height={250} m={2} />
      </ShimmerEffect>
    );

    /**
     * Render multiple loading list of requests blocks
     */
    const createLoadingStatisticsList = () => {
      const list = [];
      for (let counter = 0; counter < 3; counter += 1) {
        list.push(getLoadingStatistics(counter));
      }
      return list;
    };

    if (isGetRechargeMethodsStaticsFetching) {
      return createLoadingStatisticsList();
    }

    if (isGetRechargeMethodsStaticsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getRechargeMethodsStaticsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      isGetRechargeMethodsStaticsSuccess && (
        <>
          <Flex width={1} m={1}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleRechargeMethodsStaticsFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
            >
              <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                فرز المستخدمين
              </Text>
            </Button>
            <Filter
              cachedFilterPage="rechargeMethodsStatics"
              filterSections={['dates']}
              header="فرز المستخدمين"
              filterQueries={filterRechargeMethodsStaticsQueries}
              isOpened={isFilterRechargeMethodsStaticsOpen}
              filterFunction={getRechargeMethodsStaticsAction}
              toggleFilter={this.handleToggleRechargeMethodsStaticsFilterModal}
              handleChangeFilterQueries={this.handleChangeRechargeMethodsStaticsFilterQueries}
            />
          </Flex>
          <Card
            minHeight={500}
            m={2}
            p={2}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <RechargeMethodsStatics rechargeMethodsStatics={rechargeMethodsStatics} />
          </Card>
        </>
      )
    );
  }
}
ReChargeMethodsStaticsContainer.displayName = 'ReChargeMethodsStaticsContainer';

const mapStateToProps = state => ({
  rechargeMethodsStatics: state.dashboard.rechargeMethodsStatics,
  isGetRechargeMethodsStaticsFetching: state.dashboard.getRechargeMethodsStatics.isFetching,
  isGetRechargeMethodsStaticsSuccess: state.dashboard.getRechargeMethodsStatics.isSuccess,
  isGetRechargeMethodsStaticsError: state.dashboard.getRechargeMethodsStatics.isFail.isError,
  getRechargeMethodsStaticsErrorMessage: state.dashboard.getRechargeMethodsStatics.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getRechargeMethodsStatics,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ReChargeMethodsStaticsContainer,
);
