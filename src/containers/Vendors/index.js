import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllVendors,
  toggleAddNewVendorModal,
  deleteVendor,
  openConfirmModal,
  openEditVendorModal,
  toggleChangePasswordModal,
  changePasswordReset,
  changePassword,
  resetVendorCredits,
} from 'redux-modules/vendors/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import VendorsList from 'components/Vendors';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class VendorsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreVendorsAvailable: PropTypes.bool.isRequired,
    getAllVendors: PropTypes.func,
    isGetAllVendorsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewVendorModal: PropTypes.func,
    isAddNewVendorModalOpen: PropTypes.bool,
    deleteVendor: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeleteVendorFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditVendorModal: PropTypes.func,
    isEditVendorModalOpen: PropTypes.bool,
    isGetAllVendorsSuccess: PropTypes.bool,
    isGetAllVendorsError: PropTypes.bool,
    getAllVendorsErrorMessage: PropTypes.string,
    toggleChangePasswordModal: PropTypes.func,
    changePasswordReset: PropTypes.func,
    changePassword: PropTypes.func,
    isChangePasswordModalOpen: PropTypes.bool,
    isChangePasswordFetching: PropTypes.bool,
    isChangePasswordError: PropTypes.bool,
    changePasswordErrorMessage: PropTypes.string,
    resetVendorCredits: PropTypes.func,
    isResettingVendorCredits: PropTypes.bool,
    isDeleteVendorError: PropTypes.bool,
    isDeleteVendorErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    vendorsList: [],
    getAllVendors: () => {},
    applyNewFilter: false,
    isGetAllVendorsFetching: false,
    toggleAddNewVendorModal: () => {},
    isAddNewVendorModalOpen: false,
    deleteVendor: () => {},
    openConfirmModal: () => {},
    isDeleteVendorFetching: false,
    isConfirmModalOpen: false,
    openEditVendorModal: () => {},
    isEditVendorModalOpen: false,
    isGetAllVendorsSuccess: false,
    isGetAllVendorsError: false,
    getAllVendorsErrorMessage: false,
    changePassword: () => {},
    toggleChangePasswordModal: () => {},
    changePasswordReset: () => {},
    isChangePasswordModalOpen: false,
    isChangePasswordFetching: false,
    isChangePasswordError: false,
    changePasswordErrorMessage: false,
    resetVendorCredits: () => {},
    isResettingVendorCredits: false,
    isDeleteVendorError: false,
    isDeleteVendorErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('vendorsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      index: 0,
      vendorDetails: {},
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllVendors: getAllVendorsAction } = this.props;
    const { filterQueries } = this.state;

    getAllVendorsAction(filterQueries, true);
  }

  loadMoreVendors = () => {
    const { getAllVendors: getAllVendorsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllVendorsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewVendorModal = () => {
    const { toggleAddNewVendorModal: toggleAddNewVendorModalAction } = this.props;

    toggleAddNewVendorModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { openConfirmModal: toggleConfirmModalAction, isConfirmModalOpen } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpen);
      },
    );
  };

  handleToggleEditVendorModal = (index, vendorDetails) => {
    const { openEditVendorModal: openEditVendorModalAction } = this.props;
    this.setState({ vendorDetails, index });
    openEditVendorModalAction();
  };

  handleToggleChangePasswordModal = vendor => {
    const {
      toggleChangePasswordModal: toggleChangePasswordModalAction,
      changePasswordReset: changePasswordResetAction,
    } = this.props;

    this.setState({ vendorDetails: vendor }, () => {
      toggleChangePasswordModalAction();
      changePasswordResetAction();
    });
  };

  onSubmitChangePasswordForm = (password, confirmPassword) => {
    const { changePassword: changePasswordAction } = this.props;
    const {
      vendorDetails: { _id },
    } = this.state;

    const newPasswordInfo = {
      vendorId: _id,
      password,
      confirmPassword,
    };

    changePasswordAction(newPasswordInfo);
  };

  render() {
    const {
      user,
      user: { permissions },
      vendorsList,
      isLoadMoreVendorsAvailable,
      getAllVendors: getAllVendorsAction,
      isGetAllVendorsFetching,
      applyNewFilter,
      isAddNewVendorModalOpen,
      deleteVendor: deleteVendorAction,
      isDeleteVendorFetching,
      isConfirmModalOpen,
      isEditVendorModalOpen,
      isGetAllVendorsSuccess,
      isGetAllVendorsError,
      getAllVendorsErrorMessage,
      isChangePasswordModalOpen,
      isChangePasswordFetching,
      isChangePasswordError,
      changePasswordErrorMessage,
      resetVendorCredits: resetVendorCreditsAction,
      isResettingVendorCredits,
      isDeleteVendorError,
      isDeleteVendorErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      index,
      vendorDetails,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
    } = this.state;

    const vendorsRenderer = (
      <Box width={1}>
        <InfiniteScroll
          dataLength={vendorsList.length}
          next={this.loadMoreVendors}
          hasMore={isLoadMoreVendorsAvailable}
          loader={
            <Flex m={2} justifyContent="center" alignItems="center">
              <FontAwesomeIcon
                icon={faSpinner}
                size="lg"
                spin
                color={COLORS_VALUES[BRANDING_GREEN]}
              />
            </Flex>
          }
        >
          <VendorsList
            isAddNewVendorModalOpen={isAddNewVendorModalOpen}
            vendorsList={vendorsList}
            isLoadMoreVendorsAvailable={isLoadMoreVendorsAvailable}
            loadMoreVendors={this.loadMoreVendors}
            isGetAllVendorsFetching={isGetAllVendorsFetching}
            applyNewFilter={applyNewFilter}
            user={user}
            handleDeleteVendor={deleteVendorAction}
            handleToggleConfirmModal={this.handleToggleConfirmModal}
            isConfirmModalOpen={isConfirmModalOpen}
            isDeleteVendorFetching={isDeleteVendorFetching}
            handleToggleEditVendorModal={this.handleToggleEditVendorModal}
            isEditVendorModalOpen={isEditVendorModalOpen}
            vendorDetails={vendorDetails}
            vendorIndex={index}
            handleToggleChangePasswordModal={this.handleToggleChangePasswordModal}
            onSubmitChangePasswordForm={this.onSubmitChangePasswordForm}
            isChangePasswordModalOpen={isChangePasswordModalOpen}
            isChangePasswordFetching={isChangePasswordFetching}
            isChangePasswordError={isChangePasswordError}
            changePasswordErrorMessage={changePasswordErrorMessage}
            isGetAllVendorsSuccess={isGetAllVendorsSuccess}
            isGetAllVendorsError={isGetAllVendorsError}
            getAllVendorsErrorMessage={getAllVendorsErrorMessage}
            confirmModalHeader={confirmModalHeader}
            confirmModalText={confirmModalText}
            confirmModalFunction={confirmModalFunction}
            handleResetVendorCredits={resetVendorCreditsAction}
            isResettingVendorCredits={isResettingVendorCredits}
            isDeleteVendorError={isDeleteVendorError}
            isDeleteVendorErrorMessage={isDeleteVendorErrorMessage}
          />
        </InfiniteScroll>
      </Box>
    );

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>الشركاء</title>
        </Helmet>
        {permissions && permissions.includes('Get All Users') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الشركاء
              </Text>
              <Flex m={2}>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleFilterModal}
                  icon={faFilter}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    فرز
                  </Text>
                </Button>
                {permissions && permissions.includes('Add New Vendor') && (
                  <>
                    <Button
                      ml={1}
                      mr={1}
                      color={PRIMARY}
                      onClick={this.handleToggleAddNewVendorModal}
                      icon={faPlus}
                      iconWidth="sm"
                      xMargin={3}
                      isLoading={false}
                      reverse
                    >
                      <Text
                        cursor="pointer"
                        mx={2}
                        type={SUBHEADING}
                        color={COLORS_VALUES[DISABLED]}
                        fontWeight={NORMAL}
                      >
                        أضف شريك جديده
                      </Text>
                    </Button>
                  </>
                )}
              </Flex>
            </StickyContainer>
            <Filter
              cachedFilterPage="vendors"
              filterSections={['vendorUsername', 'dates', 'contractExpired']}
              creditsTitle="القيمة"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllVendorsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {vendorsRenderer}
          </>
        )}
      </Flex>
    );
  }
}
VendorsContainer.displayName = 'VendorsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.vendors.applyNewFilter,
  vendorsList: state.vendors.vendors,
  isGetAllVendorsFetching: state.vendors.getAllVendors.isFetching,
  isGetAllVendorsSuccess: state.vendors.getAllVendors.isSuccess,
  isGetAllVendorsError: state.vendors.getAllVendors.isFail.isError,
  getAllVendorsErrorMessage: state.vendors.getAllVendors.isFail.message,
  isLoadMoreVendorsAvailable: state.vendors.hasMoreVendors,
  isAddNewVendorModalOpen: state.vendors.addNewVendorModal.isOpen,
  isDeleteVendorFetching: state.vendors.deleteVendor.isFetching,
  isDeleteVendorError: state.vendors.deleteVendor.isFail.isError,
  isDeleteVendorErrorMessage: state.vendors.deleteVendor.isFail.message,
  isConfirmModalOpen: state.vendors.confirmModal.isOpen,
  isEditVendorModalOpen: state.vendors.editVendorModal.isOpen,

  isChangePasswordModalOpen: state.vendors.changePasswordModal.isOpen,
  isChangePasswordFetching: state.vendors.changePassword.isFetching,
  isChangePasswordError: state.vendors.changePassword.isFail.isError,
  changePasswordErrorMessage: state.vendors.changePassword.isFail.message,
  isResettingVendorCredits: state.vendors.resetVendorCredits.isFetching,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllVendors,
      toggleAddNewVendorModal,
      deleteVendor,
      openConfirmModal,
      openEditVendorModal,
      toggleChangePasswordModal,
      changePasswordReset,
      changePassword,
      resetVendorCredits,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(VendorsContainer);
