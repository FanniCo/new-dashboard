import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { sendNotifications } from 'redux-modules/notifications/actions';
import Notifications from 'components/Notifications';

class NotificationsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    sendNotifications: PropTypes.func,
    isSendNotificationsFetching: PropTypes.bool,
    isSendNotificationsSuccess: PropTypes.bool,
    isSendNotificationsError: PropTypes.bool,
    notificationsSnackBar: PropTypes.shape({}),
  };

  static defaultProps = {
    user: undefined,
    sendNotifications: () => {},
    isSendNotificationsFetching: false,
    isSendNotificationsSuccess: false,
    isSendNotificationsError: false,
    notificationsSnackBar: undefined,
  };

  state = {
    targets: null,
    os: null,
    fields: [],
    cities: [],
    arabicMessage: '',
    englishMessage: '',
  };

  componentDidUpdate(prevProps) {
    const { isSendNotificationsSuccess } = this.props;

    if (
      isSendNotificationsSuccess &&
      isSendNotificationsSuccess !== prevProps.isSendNotificationsSuccess
    ) {
      this.resetSendNotificationsForm();
    }
  }

  resetSendNotificationsForm = () => {
    this.setState({
      targets: null,
      os: null,
      fields: null,
      cities: null,
      arabicMessage: '',
      englishMessage: '',
    });
  };

  handleChangeSelect = (type, val) => {
    this.setState({ [type]: val });
  };

  handleChangeMessage = (type, message) => {
    this.setState({ [type]: message });
  };

  handleSendNotificationsAction = () => {
    const { sendNotifications: sendNotificationsAction } = this.props;
    const { targets, os, cities, fields, arabicMessage, englishMessage } = this.state;
    const target = targets.value;
    const osArray = os.map(osOption => osOption.value);
    const citiesArray = cities && cities.map(city => city.value);
    const fieldsArray = fields && fields.map(field => field.value);
    const sendNotificationsOptions = {
      target,
      os: osArray,
      fields: fieldsArray || [],
      cities: citiesArray,
      message: {
        ar: arabicMessage,
        en: englishMessage,
      },
    };

    sendNotificationsAction(sendNotificationsOptions);
  };

  render() {
    const {
      user: { permissions },
      isSendNotificationsFetching,
      isSendNotificationsError,
      notificationsSnackBar,
    } = this.props;
    const { targets, os, cities, fields, arabicMessage, englishMessage } = this.state;

    return (
      permissions &&
      permissions.includes('Send Target Notification') && (
        <Notifications
          targets={targets}
          os={os}
          cities={cities}
          fields={fields}
          arabicMessage={arabicMessage}
          englishMessage={englishMessage}
          isSendNotificationsFetching={isSendNotificationsFetching}
          isSendNotificationsError={isSendNotificationsError}
          handleChangeSelect={this.handleChangeSelect}
          handleChangeMessage={this.handleChangeMessage}
          sendNotifications={this.handleSendNotificationsAction}
          notificationsSnackBar={notificationsSnackBar}
        />
      )
    );
  }
}
NotificationsContainer.displayName = 'NotificationsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  isSendNotificationsFetching: state.notifications.sendNotifications.isFetching,
  isSendNotificationsError: state.notifications.sendNotifications.isFail.isError,
  isSendNotificationsSuccess: state.notifications.sendNotifications.isSuccess,
  notificationsSnackBar: state.notifications.snackBar,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      sendNotifications,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationsContainer);
