import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFilter, faFile } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { getAllReviews, toggleShowingReview, deleteReview } from 'redux-modules/reviews/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { StickyContainer } from 'components/shared';
import Reviews from 'components/Reviews';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, BRANDING_GREEN, PRIMARY, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class ReviewsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    isGetAllReviewsFetching: PropTypes.bool,
    isGetAllReviewsSuccess: PropTypes.bool,
    isGetAllReviewsError: PropTypes.bool,
    getAllReviews: PropTypes.func,
    toggleShowingReview: PropTypes.func,
    isToggleShowingReviewFetching: PropTypes.bool,
    isToggleShowingReviewSuccess: PropTypes.bool,
    deleteReview: PropTypes.func,
    isDeleteReviewFetching: PropTypes.bool,
    isDeleteReviewSuccess: PropTypes.bool,
    reviews: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreReviews: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    getAllReviewsErrorMessage: PropTypes.string,
    isDeleteReviewError: PropTypes.bool,
    isDeleteReviewErrorMessage: PropTypes.string,
    isToggleShowingReviewError: PropTypes.bool,
    isToggleShowingReviewErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    reviews: undefined,
    hasMoreReviews: true,
    applyNewFilter: false,
    getAllReviews: () => {},
    isGetAllReviewsFetching: false,
    isGetAllReviewsSuccess: false,
    isGetAllReviewsError: false,
    toggleShowingReview: () => {},
    isToggleShowingReviewFetching: false,
    isToggleShowingReviewSuccess: false,
    deleteReview: () => {},
    isDeleteReviewFetching: false,
    isDeleteReviewSuccess: false,
    getAllReviewsErrorMessage: undefined,
    isDeleteReviewError: false,
    isDeleteReviewErrorMessage: undefined,
    isToggleShowingReviewError: false,
    isToggleShowingReviewErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const cachedFilterState = JSON.parse(sessionStorage.getItem('reviewsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isConfirmModalOpen: false,
      isFilterOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries,
      reviewIndex: undefined,
    };
  }

  componentDidMount() {
    const { getAllReviews: getAllReviewsAction } = this.props;
    const { filterQueries } = this.state;

    getAllReviewsAction(filterQueries, true);
  }

  componentDidUpdate(prevProps) {
    const { isToggleShowingReviewSuccess, isDeleteReviewSuccess } = this.props;

    if (
      (isToggleShowingReviewSuccess &&
        isToggleShowingReviewSuccess !== prevProps.isToggleShowingReviewSuccess) ||
      (isDeleteReviewSuccess && isDeleteReviewSuccess !== prevProps.isDeleteReviewSuccess)
    ) {
      this.handleToggleConfirmModal('', '', () => {});
    }
  }

  /**
   * Creates lazy loading request block
   */
  getLoadingReview = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests blocks
   */
  createLoadingReviewsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingReview(counter));
    }
    return list;
  };

  loadMoreReviews = () => {
    const { getAllReviews: getAllReviewsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllReviewsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  render() {
    const {
      user,
      user: { permissions },
      reviews,
      hasMoreReviews,
      applyNewFilter,
      isGetAllReviewsFetching,
      isGetAllReviewsSuccess,
      isGetAllReviewsError,
      getAllReviews: getAllReviewsAction,
      toggleShowingReview: toggleShowingReviewAction,
      deleteReview: deleteReviewAction,
      isToggleShowingReviewFetching,
      isDeleteReviewFetching,
      getAllReviewsErrorMessage,
      isDeleteReviewError,
      isDeleteReviewErrorMessage,
      isToggleShowingReviewError,
      isToggleShowingReviewErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      isConfirmModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      filterQueries,
      reviewIndex,
    } = this.state;

    let reviewsRenderer;

    if (
      (isGetAllReviewsFetching && (!reviews || applyNewFilter)) ||
      (!isGetAllReviewsFetching && !isGetAllReviewsSuccess && !isGetAllReviewsError)
    ) {
      reviewsRenderer = this.createLoadingReviewsList();
    } else if (isGetAllReviewsError) {
      reviewsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllReviewsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!reviews.length) {
      reviewsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      reviewsRenderer = (
        <Box width={1}>
          <InfiniteScroll
            dataLength={reviews.length}
            next={this.loadMoreReviews}
            hasMore={hasMoreReviews}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <Reviews
              user={user}
              reviews={reviews}
              reviewIndex={reviewIndex}
              isConfirmModalOpen={isConfirmModalOpen}
              confirmModalHeader={confirmModalHeader}
              confirmModalText={confirmModalText}
              confirmModalFunction={confirmModalFunction}
              handleToggleConfirmModal={this.handleToggleConfirmModal}
              handleToggleShowingReview={toggleShowingReviewAction}
              isToggleShowingReviewFetching={isToggleShowingReviewFetching}
              handleDeleteReview={deleteReviewAction}
              isDeleteReviewFetching={isDeleteReviewFetching}
              isDeleteReviewError={isDeleteReviewError}
              isDeleteReviewErrorMessage={isDeleteReviewErrorMessage}
              isToggleShowingReviewError={isToggleShowingReviewError}
              isToggleShowingReviewErrorMessage={isToggleShowingReviewErrorMessage}
            />
          </InfiniteScroll>
        </Box>
      );
    }

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>التقييمات</title>
        </Helmet>
        {permissions && permissions.includes('Get Reviews') && (
          <>
            <StickyContainer
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
              bgColor={COLORS_VALUES[GREY_MEDIUM]}
            >
              <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                التقييمات
              </Text>
              <Button
                ml={1}
                mr={1}
                color={PRIMARY}
                onClick={this.handleToggleFilterModal}
                icon={faFilter}
                iconWidth="sm"
                xMargin={3}
                isLoading={false}
              >
                <Text mx={2} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  فرز
                </Text>
              </Button>
            </StickyContainer>
            <Filter
              cachedFilterPage="reviews"
              filterSections={['clientUsername', 'workerUsername', 'selectedWorkerRating']}
              clientUsernameQueryKey="reviewerUsername"
              workerUsernameQueryKey="username"
              selectedWorkerRating="selectedWorkerRating"
              header="فرز"
              filterQueries={filterQueries}
              isOpened={isFilterOpen}
              filterFunction={getAllReviewsAction}
              toggleFilter={this.handleToggleFilterModal}
              handleChangeFilterQueries={this.handleChangeFilterQueries}
            />
            {reviewsRenderer}
          </>
        )}
      </Flex>
    );
  }
}
ReviewsContainer.displayName = 'ReviewsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  reviews: state.reviews.reviews,
  hasMoreReviews: state.reviews.hasMoreReviews,
  applyNewFilter: state.reviews.applyNewFilter,
  isGetAllReviewsFetching: state.reviews.getAllReviews.isFetching,
  isGetAllReviewsError: state.reviews.getAllReviews.isFail.isError,
  getAllReviewsErrorMessage: state.reviews.getAllReviews.isFail.message,
  isGetAllReviewsSuccess: state.reviews.getAllReviews.isSuccess,
  isToggleShowingReviewFetching: state.reviews.toggleShowingReview.isFetching,
  isToggleShowingReviewError: state.reviews.toggleShowingReview.isFail.isError,
  isToggleShowingReviewErrorMessage: state.reviews.toggleShowingReview.isFail.message,
  isToggleShowingReviewSuccess: state.reviews.toggleShowingReview.isSuccess,
  isDeleteReviewFetching: state.reviews.deleteReview.isFetching,
  isDeleteReviewError: state.reviews.deleteReview.isFail.isError,
  isDeleteReviewErrorMessage: state.reviews.deleteReview.isFail.message,
  isDeleteReviewSuccess: state.reviews.deleteReview.isSuccess,
  isConfirmModalOpen: state.reviews.confirmModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllReviews,
      toggleShowingReview,
      deleteReview,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReviewsContainer);
