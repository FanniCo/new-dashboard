import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  getClientDetails,
  suspendClient,
  addComment,
  editClientReset,
  convertClientToWorkerReset,
  toggleEditClientModal,
  toggleConvertClientToWorkerModal,
  toggleClientAddCreditModal,
  clientAddCreditReset,
  toggleAddNewRequestModal,
  toggleSuspendModal,
  toggleConfirmModal,
} from 'redux-modules/clients/actions';
import { getAllAdmins } from 'redux-modules/user/actions';
import ROUTES from 'routes';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import SingleClient from 'components/SingleClient';
import { Box, Flex } from '@rebass/grid';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';

const { BRANDING_GREEN, WHITE, GREY_LIGHT } = COLORS;
const { BIG_TITLE } = FONT_TYPES;
const { SINGLE_WORKER } = ROUTES;

class SingleClientContainer extends Component {
  static propTypes = {
    history: PropTypes.shape({}).isRequired,
    user: PropTypes.shape({}),
    clientDetails: PropTypes.shape({}),
    isGetClientDetailsFetching: PropTypes.bool,
    isSuspendClientFetching: PropTypes.bool,
    isSuspendClientError: PropTypes.bool,
    isConvertClientToWorkerSuccess: PropTypes.bool,
    suspendClient: PropTypes.func,
    editClientReset: PropTypes.func,
    convertClientToWorkerReset: PropTypes.func,
    toggleEditClientModal: PropTypes.func,
    toggleConvertClientToWorkerModal: PropTypes.func,
    getClientDetails: PropTypes.func,
    match: PropTypes.shape({}),
    addComment: PropTypes.func,
    addCommentState: PropTypes.string,
    addCommentErrorMessage: PropTypes.string,
    isEditClientModalOpen: PropTypes.bool,
    isConvertClientToWorkerModalOpen: PropTypes.bool,
    toggleClientAddCreditModal: PropTypes.func,
    clientAddCreditReset: PropTypes.func,
    isAddCreditModalOpen: PropTypes.bool,
    toggleAddNewRequestModal: PropTypes.func,
    isAddNewRequestModalOpen: PropTypes.bool,
    toggleSuspendModal: PropTypes.func,
    toggleConfirmModal: PropTypes.func,
    isSuspendModalOpen: PropTypes.bool,
    isConfirmModalOpened: PropTypes.bool,
    suspendClientErrorMessage: PropTypes.string,
    getClientDetailsErrorMessage: PropTypes.string,
    isGetClientDetailsError: PropTypes.bool,
    getAllAdmins: PropTypes.func,
    admins: PropTypes.arrayOf(PropTypes.shape({})),
  };

  static defaultProps = {
    user: undefined,
    clientDetails: undefined,
    isGetClientDetailsFetching: false,
    isSuspendClientFetching: false,
    isSuspendClientError: false,
    isEditClientModalOpen: false,
    isConvertClientToWorkerModalOpen: false,
    isConvertClientToWorkerSuccess: false,
    getClientDetails: () => {},
    suspendClient: () => {},
    editClientReset: () => {},
    convertClientToWorkerReset: () => {},
    toggleEditClientModal: () => {},
    toggleConvertClientToWorkerModal: () => {},
    match: undefined,
    addComment: undefined,
    addCommentState: undefined,
    addCommentErrorMessage: undefined,
    toggleClientAddCreditModal: () => {},
    clientAddCreditReset: () => {},
    isAddCreditModalOpen: false,
    toggleAddNewRequestModal: () => {},
    isAddNewRequestModalOpen: false,
    toggleSuspendModal: () => {},
    toggleConfirmModal: () => {},
    isSuspendModalOpen: false,
    isConfirmModalOpened: false,
    suspendClientErrorMessage: undefined,
    getClientDetailsErrorMessage: undefined,
    isGetClientDetailsError: false,
    getAllAdmins: () => {},
    admins: [],
  };

  state = {
    tabs: [{ name: 'تفاصيل العميل' }, { name: 'طلبات العميل' }, { name: 'سجل العمليات' }],
    activeTab: 0,
    confirmModalHeader: '',
    confirmModalText: '',
    confirmModalFunction: () => {},
    addedCommentValue: '',
    mentionData: [],
  };

  componentDidMount() {
    const {
      getClientDetails: getClientDetailssAction,
      match: {
        params: { id },
      },
      getAllAdmins: getAllAdminsAction,
    } = this.props;

    getClientDetailssAction(id);
    getAllAdminsAction({ skip: 0, limit: 5000 }, true);
  }

  componentDidUpdate(prevProps) {
    const { isConvertClientToWorkerSuccess } = this.props;

    if (
      isConvertClientToWorkerSuccess &&
      isConvertClientToWorkerSuccess !== prevProps.isConvertClientToWorkerSuccess
    ) {
      const {
        history,
        clientDetails: { username },
      } = this.props;

      history.push(`${SINGLE_WORKER}/${username}`);
    }
  }

  handleTabChange = value => {
    const { activeTab } = this.state;

    if (activeTab !== value) {
      this.setState({ activeTab: value });
    }
  };

  openClientLocation = (lat, lng) => {
    window.open(`https://maps.google.com/?q=${lat},${lng}`, '_blank');
  };

  handleToggleEditClientModal = () => {
    const {
      toggleEditClientModal: toggleEditClientModalAction,
      editClientReset: editClientResetAction,
    } = this.props;

    editClientResetAction();
    toggleEditClientModalAction();
  };

  handleToggleConvertClientToWorkerModal = () => {
    const {
      toggleConvertClientToWorkerModal: toggleConvertClientToWorkerModalAction,
      convertClientToWorkerReset: convertClientToWorkerResetAction,
    } = this.props;

    convertClientToWorkerResetAction();
    toggleConvertClientToWorkerModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { toggleConfirmModal: toggleConfirmModalAction, isConfirmModalOpened } = this.props;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpened ? header : '',
        confirmModalText: !isConfirmModalOpened ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpened ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpened);
      },
    );
  };

  handleToggleSuspendModal = () => {
    const { toggleSuspendModal: toggleSuspendModalAction } = this.props;

    toggleSuspendModalAction();
  };

  handleAddCommentInputChange = (event, newValue, newPlainTextValue, mentions) => {
    const { mentionData } = this.state;
    this.setState({
      addedCommentValue: newPlainTextValue,
      mentionData: mentionData.concat(mentions),
    });
  };

  resetAddCommentInput = () => {
    this.setState({ addedCommentValue: '' });
  };

  handleToggleAddCreditModal = () => {
    const {
      toggleClientAddCreditModal: toggleCustomerAddCreditModalAction,
      clientAddCreditReset: CustomerAddCreditResetAction,
    } = this.props;

    CustomerAddCreditResetAction();
    toggleCustomerAddCreditModalAction();
  };

  handleToggleAddNewRequestModal = () => {
    const { toggleAddNewRequestModal: toggleAddNewRequestModalAction } = this.props;

    toggleAddNewRequestModalAction();
  };

  render() {
    const {
      user,
      clientDetails,
      isGetClientDetailsFetching,
      suspendClient: suspendClientAction,
      isSuspendClientFetching,
      isSuspendClientError,
      addComment: addCommentAction,
      addCommentState,
      addCommentErrorMessage,
      isEditClientModalOpen,
      isConvertClientToWorkerModalOpen,
      isAddCreditModalOpen,
      isAddNewRequestModalOpen,
      isSuspendModalOpen,
      isConfirmModalOpened,
      suspendClientErrorMessage,
      isGetClientDetailsError,
      getClientDetailsErrorMessage,
      admins,
    } = this.props;
    const {
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      addedCommentValue,
      tabs,
      activeTab,
      mentionData,
    } = this.state;

    if (isGetClientDetailsFetching && !clientDetails) {
      return (
        <ShimmerEffect width={1}>
          <Rect width={1 / 3} height={550} m={2} />
          <Rect width={2 / 3} height={550} m={2} />
        </ShimmerEffect>
      );
    }

    if (isGetClientDetailsError) {
      return (
        <Box width={1}>
          <Flex m={2} justifyContent="center" alignItems="center">
            <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getClientDetailsErrorMessage}
              />
            </Card>
          </Flex>
        </Box>
      );
    }

    return (
      <SingleClient
        tabs={tabs}
        activeTab={activeTab}
        handleTabChange={this.handleTabChange}
        user={user}
        clientDetails={clientDetails}
        isGetClientDetailsFetching={isGetClientDetailsFetching}
        openClientLocation={this.openClientLocation}
        isConfirmModalOpen={isConfirmModalOpened}
        isEditClientModalOpen={isEditClientModalOpen}
        isConvertClientToWorkerModalOpen={isConvertClientToWorkerModalOpen}
        confirmModalHeader={confirmModalHeader}
        confirmModalText={confirmModalText}
        confirmModalFunction={confirmModalFunction}
        handleToggleConfirmModal={this.handleToggleConfirmModal}
        handleToggleEditClientModal={this.handleToggleEditClientModal}
        handleToggleConvertClientToWorkerModal={this.handleToggleConvertClientToWorkerModal}
        handleSuspendClient={suspendClientAction}
        isSuspendClientFetching={isSuspendClientFetching}
        isSuspendClientError={isSuspendClientError}
        addedCommentValue={addedCommentValue}
        handleAddCommentInputChange={this.handleAddCommentInputChange}
        addComment={addCommentAction}
        addCommentState={addCommentState}
        addCommentErrorMessage={addCommentErrorMessage}
        resetAddCommentInput={this.resetAddCommentInput}
        handleToggleAddCreditModal={this.handleToggleAddCreditModal}
        isAddCreditModalOpen={isAddCreditModalOpen}
        handleToggleAddNewRequestModal={this.handleToggleAddNewRequestModal}
        isAddNewRequestModalOpen={isAddNewRequestModalOpen}
        handleToggleSuspendModal={this.handleToggleSuspendModal}
        isSuspendModalOpen={isSuspendModalOpen}
        suspendClientErrorMessage={suspendClientErrorMessage}
        admins={admins}
        mentionData={mentionData}
      />
    );
  }
}
SingleClientContainer.displayName = 'SingleClientContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  admins: state.user.admins,
  clientDetails: state.clients.clientDetails,
  isGetClientDetailsFetching: state.clients.getClientDetails.isFetching,
  isGetClientDetailsError: state.clients.getClientDetails.isFail.isError,
  getClientDetailsErrorMessage: state.clients.getClientDetails.isFail.message,
  isGetClientDetailsSuccess: state.clients.getClientDetails.isSuccess,
  isSuspendClientFetching: state.clients.suspendClient.isFetching,
  isSuspendClientError: state.clients.suspendClient.isFail.isError,
  isSuspendClientSuccess: state.clients.suspendClient.isSuccess,
  suspendClientErrorMessage: state.clients.suspendClient.isFail.message,
  isConvertClientToWorkerSuccess: state.clients.convertClientToWorker.isSuccess,
  addCommentState: state.clients.addComment.addCommentState,
  addCommentErrorMessage: state.clients.addComment.error,
  isEditClientModalOpen: state.clients.editClientModal.isOpen,
  isConvertClientToWorkerModalOpen: state.clients.convertClientToWorkerModal.isOpen,
  isAddCreditModalOpen: state.clients.clientAddCreditsModal.isOpen,
  isAddNewRequestModalOpen: state.clients.addNewRequestModal.isOpen,
  isSuspendModalOpen: state.clients.suspendModal.isOpen,
  isConfirmModalOpened: state.clients.confirmModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getClientDetails,
      suspendClient,
      addComment,
      editClientReset,
      convertClientToWorkerReset,
      toggleEditClientModal,
      toggleConvertClientToWorkerModal,
      toggleClientAddCreditModal,
      clientAddCreditReset,
      toggleAddNewRequestModal,
      toggleSuspendModal,
      toggleConfirmModal,
      getAllAdmins,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SingleClientContainer);
