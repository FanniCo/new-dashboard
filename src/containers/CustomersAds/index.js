import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Box, Flex } from '@rebass/grid';
import { faFilter, faPlus, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  getAllCustomersAds,
  toggleAddNewCustomersAdModal,
  deleteCustomersAd,
  openConfirmModal,
  openEditCustomersAdModal,
} from 'redux-modules/customersAds/actions';
import Filter from 'components/Filter';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StickyContainer } from 'components/shared';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import CustomersAdsList from 'components/CustomersAds';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Card from 'components/Card';

const { GREY_MEDIUM, DISABLED, PRIMARY, BRANDING_GREEN, GREY_DARK } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

class CustomersAdsContainer extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    customersAdsList: PropTypes.arrayOf(PropTypes.shape({})),
    isLoadMoreCustomersAdsAvailable: PropTypes.bool.isRequired,
    getAllCustomersAds: PropTypes.func,
    isGetAllCustomersAdsFetching: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    toggleAddNewCustomersAdModal: PropTypes.func,
    isAddNewCustomersAdModalOpen: PropTypes.bool,
    deleteCustomersAd: PropTypes.func,
    openConfirmModal: PropTypes.func,
    isDeleteCustomersAdFetching: PropTypes.bool,
    isConfirmModalOpen: PropTypes.bool,
    openEditCustomersAdModal: PropTypes.func,
    isEditCustomersAdModalOpen: PropTypes.bool,
    isGetAllCustomersAdsSuccess: PropTypes.bool,
    isGetAllCustomersAdsError: PropTypes.bool,
    isGetAllCustomersAdsErrorMessage: PropTypes.string,
    isDeleteCustomersAdError: PropTypes.bool,
    isDeleteCustomersAdErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    user: undefined,
    customersAdsList: [],
    getAllCustomersAds: () => {},
    applyNewFilter: false,
    isGetAllCustomersAdsFetching: false,
    toggleAddNewCustomersAdModal: () => {},
    isAddNewCustomersAdModalOpen: false,
    deleteCustomersAd: () => {},
    openConfirmModal: () => {},
    isDeleteCustomersAdFetching: false,
    isConfirmModalOpen: false,
    openEditCustomersAdModal: () => {},
    isEditCustomersAdModalOpen: false,
    isGetAllCustomersAdsSuccess: false,
    isGetAllCustomersAdsError: false,
    isGetAllCustomersAdsErrorMessage: undefined,
    isDeleteCustomersAdError: false,
    isDeleteCustomersAdErrorMessage: undefined,
  };

  constructor(props) {
    super(props);
    const cachedFilterState = JSON.parse(sessionStorage.getItem('customersAdsFilterState'));
    let filterQueries = {
      skip: 0,
      limit: 20,
    };

    if (cachedFilterState) {
      const { filterQueries: cachedFilterQueries } = cachedFilterState;

      filterQueries = cachedFilterQueries;
    }

    this.state = {
      isFilterOpen: false,
      filterQueries,
      index: 0,
      customersAdDetails: {},

      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
    };
  }

  componentDidMount() {
    const { getAllCustomersAds: getAllCustomersAdsAction } = this.props;
    const { filterQueries } = this.state;

    getAllCustomersAdsAction(filterQueries, true);
  }

  loadMoreCustomersAds = () => {
    const { getAllCustomersAds: getAllCustomersAdsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllCustomersAdsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleAddNewCustomersAdModal = () => {
    const { toggleAddNewCustomersAdModal: toggleAddNewCustomersAdModalAction } = this.props;

    toggleAddNewCustomersAdModalAction();
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { openConfirmModal: toggleConfirmModalAction, isConfirmModalOpen } = this.props;
    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        toggleConfirmModalAction(isConfirmModalOpen);
      },
    );
  };

  handleToggleEditCustomersAdModal = (index, customersAdDetails) => {
    const { openEditCustomersAdModal: openEditCustomersAdModalAction } = this.props;
    this.setState({ customersAdDetails, index });
    openEditCustomersAdModalAction();
  };

  render() {
    const {
      user,
      user: { permissions },
      customersAdsList,
      isLoadMoreCustomersAdsAvailable,
      getAllCustomersAds: getAllCustomersAdsAction,
      isGetAllCustomersAdsFetching,
      applyNewFilter,
      isAddNewCustomersAdModalOpen,
      deleteCustomersAd: deleteCustomersAdAction,
      isDeleteCustomersAdFetching,
      isConfirmModalOpen,
      isEditCustomersAdModalOpen,
      isGetAllCustomersAdsSuccess,
      isGetAllCustomersAdsError,
      isGetAllCustomersAdsErrorMessage,
      isDeleteCustomersAdError,
      isDeleteCustomersAdErrorMessage,
    } = this.props;
    const {
      isFilterOpen,
      filterQueries,
      index,
      customersAdDetails,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
    } = this.state;

    return (
      <Flex flexDirection="column" alignItems="flex-end" width={1}>
        <Helmet>
          <title>اعلانات العملاء</title>
        </Helmet>
        <StickyContainer
          width={1}
          py={2}
          flexDirection="row-reverse"
          flexWrap="wrap"
          justifyContent="space-between"
          alignItems="center"
          bgColor={COLORS_VALUES[GREY_MEDIUM]}
        >
          <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اعلانات العملاء
          </Text>
          <Flex m={2}>
            <Button
              ml={1}
              mr={1}
              color={PRIMARY}
              onClick={this.handleToggleFilterModal}
              icon={faFilter}
              iconWidth="sm"
              xMargin={3}
              isLoading={false}
              reverse
            >
              <Text
                cursor="pointer"
                mx={2}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                فرز
              </Text>
            </Button>
            {permissions && permissions.includes('Add New Customers Ad') && (
              <>
                <Button
                  ml={1}
                  mr={1}
                  color={PRIMARY}
                  onClick={this.handleToggleAddNewCustomersAdModal}
                  icon={faPlus}
                  iconWidth="sm"
                  xMargin={3}
                  isLoading={false}
                  reverse
                >
                  <Text
                    cursor="pointer"
                    mx={2}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    أضف اعلان جديد
                  </Text>
                </Button>
              </>
            )}
          </Flex>
        </StickyContainer>
        <Filter
          cachedFilterPage="customersAds"
          filterSections={['dates','activeStatus']}
          creditsTitle="القيمة"
          fieldsQueryKey="fields"
          citiesQueryKey="cities"
          header="فرز"
          filterQueries={filterQueries}
          isOpened={isFilterOpen}
          filterFunction={getAllCustomersAdsAction}
          toggleFilter={this.handleToggleFilterModal}
          handleChangeFilterQueries={this.handleChangeFilterQueries}
        />
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <InfiniteScroll
              dataLength={customersAdsList.length}
              next={this.loadMoreCustomersAds}
              hasMore={isLoadMoreCustomersAdsAvailable}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <CustomersAdsList
                isAddNewCustomersAdModalOpen={isAddNewCustomersAdModalOpen}
                customersAdsList={customersAdsList}
                isLoadMoreCustomersAdsAvailable={isLoadMoreCustomersAdsAvailable}
                loadMoreCustomersAds={this.loadMoreCustomersAds}
                isGetAllCustomersAdsFetching={isGetAllCustomersAdsFetching}
                isGetAllCustomersAdsSuccess={isGetAllCustomersAdsSuccess}
                isGetAllCustomersAdsError={isGetAllCustomersAdsError}
                isGetAllCustomersAdsErrorMessage={isGetAllCustomersAdsErrorMessage}
                applyNewFilter={applyNewFilter}
                user={user}
                deleteCustomersAdAction={deleteCustomersAdAction}
                handleToggleConfirmModal={this.handleToggleConfirmModal}
                isConfirmModalOpen={isConfirmModalOpen}
                isDeleteCustomersAdFetching={isDeleteCustomersAdFetching}
                handleToggleEditCustomersAdModal={this.handleToggleEditCustomersAdModal}
                isEditCustomersAdModalOpen={isEditCustomersAdModalOpen}
                customersAdDetails={customersAdDetails}
                customersAdIndex={index}
                confirmModalHeader={confirmModalHeader}
                confirmModalText={confirmModalText}
                confirmModalFunction={confirmModalFunction}
                isDeleteCustomersAdError={isDeleteCustomersAdError}
                isDeleteCustomersAdErrorMessage={isDeleteCustomersAdErrorMessage}
              />
            </InfiniteScroll>
          </Card>
        </Box>
      </Flex>
    );
  }
}
CustomersAdsContainer.displayName = 'CustomersAdsContainer';

const mapStateToProps = state => ({
  user: state.user.user,
  applyNewFilter: state.customersAds.applyNewFilter,
  customersAdsList: state.customersAds.customersAds,
  isGetAllCustomersAdsFetching: state.customersAds.getAllCustomersAds.isFetching,
  isGetAllCustomersAdsSuccess: state.customersAds.getAllCustomersAds.isSuccess,
  isGetAllCustomersAdsError: state.customersAds.getAllCustomersAds.isFail.isError,
  isGetAllCustomersAdsErrorMessage: state.customersAds.getAllCustomersAds.isFail.message,
  isLoadMoreCustomersAdsAvailable: state.customersAds.hasMoreCustomersAds,
  isAddNewCustomersAdModalOpen: state.customersAds.addNewCustomersAdModal.isOpen,
  isDeleteCustomersAdFetching: state.customersAds.deleteCustomersAd.isFetching,
  isDeleteCustomersAdError: state.customersAds.deleteCustomersAd.isFail.isError,
  isDeleteCustomersAdErrorMessage: state.customersAds.deleteCustomersAd.isFail.message,
  isConfirmModalOpen: state.customersAds.confirmModal.isOpen,
  isEditCustomersAdModalOpen: state.customersAds.editCustomersAdModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllCustomersAds,
      toggleAddNewCustomersAdModal,
      deleteCustomersAd,
      openConfirmModal,
      openEditCustomersAdModal,
    },
    dispatch,
  );

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(CustomersAdsContainer);
