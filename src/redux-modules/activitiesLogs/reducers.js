import { unionBy } from 'lodash';
import { ACTIVITIES_LOGS } from './actions';

const initialState = {
  activitiesLogs: undefined,
  hasMoreActivitiesLogs: true,
  applyNewFilter: false,
  getAllActivitiesLogs: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_ACTIVITIES_LOGS,
    GET_ALL_ACTIVITIES_LOGS_SUCCESS,
    GET_ALL_ACTIVITIES_LOGS_FAIL,
  } = ACTIVITIES_LOGS;

  const { activitiesLogs, applyNewFilter, err } = payload;
  const { activitiesLogs: activitiesLogsState } = state;

  switch (type) {
    case GET_ALL_ACTIVITIES_LOGS: {
      return {
        ...state,
        applyNewFilter,
        getAllActivitiesLogs: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_ACTIVITIES_LOGS_SUCCESS: {
      return {
        ...state,
        activitiesLogs:
          activitiesLogsState && !applyNewFilter
            ? unionBy(activitiesLogsState, activitiesLogs, '_id')
            : activitiesLogs,
        hasMoreActivitiesLogs: activitiesLogs.length === 20,
        getAllActivitiesLogs: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_ACTIVITIES_LOGS_FAIL: {
      return {
        ...state,
        getAllActivitiesLogs: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
