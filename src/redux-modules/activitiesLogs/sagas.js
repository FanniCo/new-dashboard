import { takeLatest, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { ACTIVITIES_LOGS } from './actions';

const {
  GET_ALL_ACTIVITIES_LOGS,
  GET_ALL_ACTIVITIES_LOGS_SUCCESS,
  GET_ALL_ACTIVITIES_LOGS_FAIL,
} = ACTIVITIES_LOGS;

function* getAllActivitiesLogs(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { activitiesLogs } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = activitiesLogs.getAllActivitiesLogs;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allActivitiesLogs } = response;
    return yield put({
      type: GET_ALL_ACTIVITIES_LOGS_SUCCESS,
      activitiesLogs: allActivitiesLogs,
      applyNewFilter,
    });
  }

  const {
    response: { message },
  } = response;

  return yield put({
    type: GET_ALL_ACTIVITIES_LOGS_FAIL,
    err: message,
  });
}

function* activityLogsSaga() {
  yield takeLatest(GET_ALL_ACTIVITIES_LOGS, getAllActivitiesLogs);
}

export default activityLogsSaga;
