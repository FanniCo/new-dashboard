export const ACTIVITIES_LOGS = {
  GET_ALL_ACTIVITIES_LOGS: 'GET_ALL_ACTIVITIES_LOGS',
  GET_ALL_ACTIVITIES_LOGS_SUCCESS: 'GET_ALL_ACTIVITIES_LOGS_SUCCESS',
  GET_ALL_ACTIVITIES_LOGS_FAIL: 'GET_ALL_ACTIVITIES_LOGS_FAIL',
};

export const getAllActivitiesLogs = (options, applyNewFilter = false) => ({
  type: ACTIVITIES_LOGS.GET_ALL_ACTIVITIES_LOGS,
  options,
  applyNewFilter,
});

export const getAllActivitiesLogsSuccess = (activityLogs, applyNewFilter = false) => ({
  type: ACTIVITIES_LOGS.GET_ALL_ACTIVITIES_LOGS_SUCCESS,
  activityLogs,
  applyNewFilter,
});

export const getAllActivitiesLogsFail = err => ({
  type: ACTIVITIES_LOGS.GET_ALL_ACTIVITIES_LOGS_FAIL,
  err,
});
