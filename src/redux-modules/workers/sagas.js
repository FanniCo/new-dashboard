import { takeEvery, takeLatest, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import moment from 'moment';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { WORKERS } from './actions';

const {
  GET_ALL_WORKERS,
  GET_ALL_WORKERS_SUCCESS,
  GET_ALL_WORKERS_FAIL,
  ADD_WORKER,
  ADD_WORKER_SUCCESS,
  ADD_WORKER_FAIL,
  EDIT_WORKER,
  EDIT_WORKER_SUCCESS,
  EDIT_WORKER_FAIL,
  ADD_CREDIT,
  ADD_CREDIT_SUCCESS,
  ADD_CREDIT_FAIL,
  ACTIVATE_WORKER,
  ACTIVATE_WORKER_SUCCESS,
  ACTIVATE_WORKER_FAIL,
  SUSPEND_WORKER,
  SUSPEND_WORKER_SUCCESS,
  SUSPEND_WORKER_FAIL,
  CHANGE_WORKER_PASSWORD,
  CHANGE_WORKER_PASSWORD_SUCCESS,
  CHANGE_WORKER_PASSWORD_FAIL,
  FEATURE_WORKER,
  FEATURE_WORKER_SUCCESS,
  FEATURE_WORKER_FAIL,
  MAKE_WORKER_ELITE,
  MAKE_WORKER_ELITE_SUCCESS,
  MAKE_WORKER_ELITE_FAIL,
  DELETE_WORKER,
  DELETE_WORKER_SUCCESS,
  DELETE_WORKER_FAIL,
  GET_WORKER_DETAILS,
  GET_WORKER_DETAILS_SUCCESS,
  GET_WORKER_DETAILS_FAIL,
  ADD_WORKER_COMMENT,
  GET_WORKERS_REQUESTS_COUNT,
  GET_WORKERS_REQUESTS_COUNT_SUCCESS,
  GET_WORKERS_REQUESTS_COUNT_FAIL,
  SUBSCRIBE_WORKER,
  SUBSCRIBE_WORKER_SUCCESS,
  SUBSCRIBE_WORKER_FAIL,
  GET_WORKER_STATISTICS,
  GET_WORKER_STATISTICS_SUCCESS,
  GET_WORKER_STATISTICS_FAIL,
  GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS,
  GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS,
  GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL,
  FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION,
  FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS,
  FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL,
  FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION,
  FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS,
  FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL,
  ALLOW_WORKER_TO_RECHARGE,
  ALLOW_WORKER_TO_RECHARGE_SUCCESS,
  ALLOW_WORKER_TO_RECHARGE_FAIL,

  APPROVE_THE_WORKER_RESIDENCE,
  APPROVE_THE_WORKER_RESIDENCE_SUCCESS,
  APPROVE_THE_WORKER_RESIDENCE_FAIL,
} = WORKERS;

function* getAllWorkers(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = workers.getAllWorkers;

  Object.keys(options).forEach(key => {
    url = `${url}&${key}=${options[key]}`;
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allWorkers } = response;

    yield put({
      type: GET_ALL_WORKERS_SUCCESS,
      workers: allWorkers,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_WORKERS_FAIL,
      error: message,
    });
  }
}

function* addWorker(payload) {
  const { newWorkerInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const url = `${workers.signupWorker}`;
  const api = new Api();
  const body = {
    ...newWorkerInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newWorkerInfoResponse } = response;

    yield put({
      type: ADD_WORKER_SUCCESS,
      newWorkerInfo: newWorkerInfoResponse,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_WORKER_FAIL,
      error: message,
    });
  }
}

function* editWorker(payload) {
  const { workerId, index, editParams } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = `${user.general}/${workerId}`;
  const api = new Api();
  const body = {
    ...editParams,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newWorkerInfoResponse } = response;
    yield put({
      type: EDIT_WORKER_SUCCESS,
      index,
      workerDetails: newWorkerInfoResponse,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: EDIT_WORKER_FAIL,
      error: message,
    });
  }
}

function* addCredit(payload) {
  const { index, addCreditInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { transactions } = Urls;
  const url = transactions.addWorkerCredit;
  const api = new Api();
  const body = {
    ...addCreditInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);
  
  if (Status.isSuccess(response.status)) {
    const { worth: credits } = addCreditInfo;
    const {
      response: { balance: newBalance },
    } = response;
    
    yield put({
      type: ADD_CREDIT_SUCCESS,
      index,
      credits,
      newBalance,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_CREDIT_FAIL,
      error: message.includes('NaN') ? 'الرصيد ليس رقم صحيح' : message,
    });
  }
}

function* activateWorker(payload) {
  const { workerId, index, active } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const url = workers.activateWorker;
  const api = new Api();
  const body = {
    userId: workerId,
    active,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: ACTIVATE_WORKER_SUCCESS,
      index,
      active,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ACTIVATE_WORKER_FAIL,
      error: message,
    });
  }
}

function* suspendWorker(payload) {
  const { workerId, index, suspended } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = user.suspendUser;
  const api = new Api();
  const body = {
    userId: workerId,
    suspend: suspended,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: SUSPEND_WORKER_SUCCESS,
      index,
      suspended,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: SUSPEND_WORKER_FAIL,
      error: message,
    });
  }
}

function* changeWorkerPassword(payload) {
  const { newPasswordInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const url = workers.changeWorkerPassword;
  const api = new Api();
  const body = {
    ...newPasswordInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: CHANGE_WORKER_PASSWORD_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CHANGE_WORKER_PASSWORD_FAIL,
      error: message,
    });
  }
}

function* featureWorker(payload) {
  const { workerId, index, featured } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = `${user.changeFeatured}`;
  const api = new Api();
  const body = {
    featured,
    workerId,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: FEATURE_WORKER_SUCCESS,
      index,
      featuredSubscriptionValidTo: response.response.featuredSubscriptionValidTo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: FEATURE_WORKER_FAIL,
      error: message,
    });
  }
}

function* makeWorkerElite(payload) {
  const { workerId, index, elite } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = `${user.general}/${workerId}`;
  const api = new Api();
  const body = {
    elite,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: MAKE_WORKER_ELITE_SUCCESS,
      index,
      elite,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: MAKE_WORKER_ELITE_FAIL,
      error: message,
    });
  }
}

function* deleteWorker(payload) {
  const { workerId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = user.deleteUser;
  const api = new Api();
  const body = {
    userId: workerId,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.delete(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_WORKER_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_WORKER_FAIL,
      error: message,
    });
  }
}

function* getWorkerDetails(payload) {
  const { workerUserName } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const url = `${workers.getAllWorkers}&limit=1&skip=0&partialUsername=${workerUserName}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workersArray } = response;
    const workerDetails = workersArray[0];

    yield put({
      type: GET_WORKER_DETAILS_SUCCESS,
      workerDetails,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKER_DETAILS_FAIL,
      error: message,
    });
  }
}

function* addComment(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const url = user.comments;
  const { commentInfo } = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, commentInfo, header);
  const responseStatus = response.status;
  const { comments, _id: workerId } = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ADD_WORKER_COMMENT.SUCCESS,
      comments,
      workerId,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_WORKER_COMMENT.FAIL,
      error: message,
    });
  }
}

function* getWorkersPayments(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = workers.getWorkersRequestsCount;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workersRequestsCount } = response;

    yield put({
      type: GET_WORKERS_REQUESTS_COUNT_SUCCESS,
      workersRequestsCount,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKERS_REQUESTS_COUNT_FAIL,
      error: message,
    });
  }
}

function* subscribeWorker(payload) {
  const { Info, subscribeEndpoint } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const api = new Api();
  const body = {
    ...Info,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(subscribeEndpoint, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: SUBSCRIBE_WORKER_SUCCESS,
      subscribeDueDate: moment().add(1, 'months'),
      subscribeEndpoint,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: SUBSCRIBE_WORKER_FAIL,
      error: message,
    });
  }
}

function* getWorkerStatistics(payload) {
  const { workerId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const { workers } = Urls;
  const options = { workerId };
  let url = workers.statistics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });
  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workerStatistics } = response;
    yield put({
      type: GET_WORKER_STATISTICS_SUCCESS,
      workerStatistics,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKER_STATISTICS_FAIL,
      error: message,
    });
  }
}

function* getWorkerMotivationsAndPunishments(payload) {
  const { workerId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const { workers } = Urls;
  const options = { workerId };
  let url = workers.motivationsAndPunishments;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });
  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workerMotivationAndPunishments } = response;
    yield put({
      type: GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS,
      workerMotivationAndPunishments,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL,
      error: message,
    });
  }
}

function* finishWorkerAutoApplySubscription(payload) {
  const { workerId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const { workers } = Urls;
  const body = { workerId };
  const url = workers.finishAutoApplySubscription;
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { autoApplyToRequestsSubscriptionValidTo } = response.response;
    yield put({
      type: FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS,
      autoApplyToRequestsSubscriptionValidTo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL,
      error: message,
    });
  }
}

function* finishWorkerMultiFieldsSubscription(payload) {
  const { workerId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const { workers } = Urls;
  const body = { workerId };
  const url = workers.finishMultiFieldsSubscription;

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { multiFieldsSubscriptionValidTo } = response.response;
    yield put({
      type: FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS,
      multiFieldsSubscriptionValidTo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL,
      error: message,
    });
  }
}

function* allowWorkerToRecharge(payload) {
  const { workerId, status: isAllowedToRecharge } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = `${user.general}/${workerId}`;
  const api = new Api();
  const body = {
    isAllowedToRecharge,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: ALLOW_WORKER_TO_RECHARGE_SUCCESS,
      status: isAllowedToRecharge,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ALLOW_WORKER_TO_RECHARGE_FAIL,
      error: message,
    });
  }
}

function* approveTheWorkerResidenceInfo(payload) {
  const { workerId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = `${user.general}/${workerId}/residence-info/approve`;
  const api = new Api();

  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, {}, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: APPROVE_THE_WORKER_RESIDENCE_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: APPROVE_THE_WORKER_RESIDENCE_FAIL,
      error: message,
    });
  }
}

function* workersSaga() {
  yield takeLatest(GET_ALL_WORKERS, getAllWorkers);
  yield takeEvery(ADD_WORKER, addWorker);
  yield takeEvery(EDIT_WORKER, editWorker);
  yield takeEvery(ADD_CREDIT, addCredit);
  yield takeEvery(ACTIVATE_WORKER, activateWorker);
  yield takeEvery(SUSPEND_WORKER, suspendWorker);
  yield takeEvery(CHANGE_WORKER_PASSWORD, changeWorkerPassword);
  yield takeEvery(FEATURE_WORKER, featureWorker);
  yield takeEvery(MAKE_WORKER_ELITE, makeWorkerElite);
  yield takeEvery(DELETE_WORKER, deleteWorker);
  yield takeEvery(GET_WORKER_DETAILS, getWorkerDetails);
  yield takeEvery(ADD_WORKER_COMMENT.LOAD, addComment);
  yield takeLatest(GET_WORKERS_REQUESTS_COUNT, getWorkersPayments);
  yield takeLatest(SUBSCRIBE_WORKER, subscribeWorker);
  yield takeLatest(GET_WORKER_STATISTICS, getWorkerStatistics);
  yield takeLatest(GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS, getWorkerMotivationsAndPunishments);
  yield takeLatest(FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION, finishWorkerAutoApplySubscription);
  yield takeLatest(FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION, finishWorkerMultiFieldsSubscription);
  yield takeEvery(ALLOW_WORKER_TO_RECHARGE, allowWorkerToRecharge);
  yield takeEvery(APPROVE_THE_WORKER_RESIDENCE, approveTheWorkerResidenceInfo);
}

export default workersSaga;
