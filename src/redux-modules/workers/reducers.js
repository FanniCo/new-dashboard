import { findIndex, unionBy } from 'lodash';
import update from 'immutability-helper';
import ROUTES from 'routes';
import { Urls } from 'utils/api';
import { WORKERS } from './actions';

const initialState = {
  workers: undefined,
  hasMoreWorkers: true,
  applyNewFilter: false,
  workerDetails: undefined,
  workersRequestsCount: undefined,
  hasMoreWorkersRequestsCount: true,
  getAllWorkers: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addCredit: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  activateWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  suspendWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  changeWorkerPassword: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  subscribeWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  featureWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  makeWorkerElite: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getWorkerDetails: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addComment: {
    commentInfo: undefined,
    addCommentState: undefined,
    error: undefined,
  },
  getWorkersRequestsCount: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  opsCommentsModal: { isOpen: false },
  confirmModal: { isOpen: false },
  addNewWorkerModal: { isOpen: false },
  changeWorkerPasswordModal: { isOpen: false },
  subscribeWorkerToAutoApplyModal: { isOpen: false },
  subscribeWorkerToMultiFieldsModal: { isOpen: false },
  editWorkerModal: { isOpen: false },
  addCreditModal: { isOpen: false },
  getWorkerStatistics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  workerStatistics: undefined,
  getWorkerMotivationAndPunishments: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  workerMotivationAndPunishments: undefined,
  finishWorkerAutoApplySubscription: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  finishWorkerMultiFieldsSubscription: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  suspendWorkerModal: { isOpen: false },
  allowWorkerToRecharge: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  workerId: undefined,
  approveTheWorkerResidenceInfo: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};
export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_WORKERS,
    GET_ALL_WORKERS_SUCCESS,
    GET_ALL_WORKERS_FAIL,
    ADD_WORKER,
    ADD_WORKER_SUCCESS,
    ADD_WORKER_FAIL,
    ADD_WORKER_RESET,
    EDIT_WORKER,
    EDIT_WORKER_SUCCESS,
    EDIT_WORKER_FAIL,
    EDIT_WORKER_RESET,
    ADD_CREDIT,
    ADD_CREDIT_SUCCESS,
    ADD_CREDIT_FAIL,
    ACTIVATE_WORKER,
    ACTIVATE_WORKER_SUCCESS,
    ACTIVATE_WORKER_FAIL,
    SUSPEND_WORKER,
    SUSPEND_WORKER_SUCCESS,
    SUSPEND_WORKER_FAIL,
    CHANGE_WORKER_PASSWORD,
    CHANGE_WORKER_PASSWORD_SUCCESS,
    CHANGE_WORKER_PASSWORD_FAIL,
    CHANGE_WORKER_PASSWORD_RESET,
    FEATURE_WORKER,
    FEATURE_WORKER_SUCCESS,
    FEATURE_WORKER_FAIL,
    MAKE_WORKER_ELITE,
    MAKE_WORKER_ELITE_SUCCESS,
    MAKE_WORKER_ELITE_FAIL,
    DELETE_WORKER,
    DELETE_WORKER_SUCCESS,
    DELETE_WORKER_FAIL,
    GET_WORKER_DETAILS,
    GET_WORKER_DETAILS_SUCCESS,
    GET_WORKER_DETAILS_FAIL,
    ADD_WORKER_COMMENT,
    TOGGLE_CHANGE_WORKER_PASSWORD_MODAL,
    TOGGLE_ADD_NEW_WORKER_MODAL,
    TOGGLE_EDIT_WORKER_MODAL,
    TOGGLE_ADD_CREDIT_MODAL,
    GET_WORKERS_REQUESTS_COUNT,
    GET_WORKERS_REQUESTS_COUNT_SUCCESS,
    GET_WORKERS_REQUESTS_COUNT_FAIL,
    TOGGLE_SUBSCRIBE_WORKER_TO_AUTO_APPLY_MODAL,
    SUBSCRIBE_WORKER,
    SUBSCRIBE_WORKER_SUCCESS,
    SUBSCRIBE_WORKER_FAIL,
    SUBSCRIBE_WORKER_RESET,
    TOGGLE_SUBSCRIBE_WORKER_TO_MULTI_FIELDS_MODAL,
    GET_WORKER_STATISTICS,
    GET_WORKER_STATISTICS_SUCCESS,
    GET_WORKER_STATISTICS_FAIL,
    GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS,
    GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS,
    GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL,
    FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION,
    FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS,
    FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL,
    FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION,
    FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS,
    FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL,
    TOGGLE_SUSPEND_WORKER_MODAL,
    TOGGLE_WORKERS_CONFIRM_MODAL,
    ALLOW_WORKER_TO_RECHARGE,
    ALLOW_WORKER_TO_RECHARGE_SUCCESS,
    ALLOW_WORKER_TO_RECHARGE_FAIL,

    APPROVE_THE_WORKER_RESIDENCE,
    APPROVE_THE_WORKER_RESIDENCE_SUCCESS,
    APPROVE_THE_WORKER_RESIDENCE_FAIL,
  } = WORKERS;
  const { WORKERS: WORKERS_ROUTE } = ROUTES;
  const {
    workers,
    workersRequestsCount,
    applyNewFilter,
    index,
    active,
    suspended,
    featuredSubscriptionValidTo,
    elite,
    credits,
    newBalance,
    newIncome,
    workerDetails,
    isOpen,
    newWorkerInfo,
    commentInfo,
    comments,
    subscribeDueDate,
    subscribeEndpoint,
    workerStatistics,
    workerMotivationAndPunishments,
    autoApplyToRequestsSubscriptionValidTo,
    multiFieldsSubscriptionValidTo,
    error,
    status,
    workerId,
  } = payload;
  const {
    workers: workersState,
    workerDetails: workerDetailsState,
    workersRequestsCount: workersRequestsCountState,
    changeWorkerPasswordModal: { isOpen: changeWorkerPasswordModalIsOpenState },
    subscribeWorkerToAutoApplyModal: { isOpen: subscribeWorkerToAutoApplyModalIsOpenState },
    subscribeWorkerToMultiFieldsModal: { isOpen: subscribeWorkerToMultiFieldsModalIsOpenState },
    addNewWorkerModal: { isOpen: addNewWorkerModalIsOpenState },
    editWorkerModal: { isOpen: editWorkerModalIsOpenState },
    addCreditModal: { isOpen: addCreditModalIsOpenState },
    suspendWorkerModal: { isOpen: suspendWorkerModalIsOpenState },
    confirmModal: { isOpen: confirmModalIsOpenState },
  } = state;

  switch (type) {
    case GET_ALL_WORKERS: {
      return {
        ...state,
        applyNewFilter,
        getAllWorkers: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_WORKERS_SUCCESS: {
      return {
        ...state,
        workers: workersState && !applyNewFilter ? unionBy(workersState, workers, '_id') : workers,
        hasMoreWorkers: workers.length === 20,
        getAllWorkers: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_WORKERS_FAIL: {
      return {
        ...state,
        getAllWorkers: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case ADD_WORKER: {
      return {
        ...state,
        addWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_WORKER_SUCCESS: {
      workersState.unshift(newWorkerInfo);

      return {
        ...state,
        workers: workersState,
        addWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewWorkerModal: { isOpen },
      };
    }
    case ADD_WORKER_FAIL: {
      return {
        ...state,
        addWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case ADD_WORKER_RESET: {
      return {
        ...state,
        addWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EDIT_WORKER: {
      return {
        ...state,
        editWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EDIT_WORKER_SUCCESS: {
      const updatedWorkers =
        index !== undefined
          ? update(workersState, {
            [index]: { $set: { ...workersState[index], ...workerDetails } },
          })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined ? workerDetailsState : { ...workerDetailsState, ...workerDetails };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        editWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editWorkerModal: { isOpen },
      };
    }
    case EDIT_WORKER_FAIL: {
      return {
        ...state,
        editWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case EDIT_WORKER_RESET: {
      return {
        ...state,
        editWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_CREDIT: {
      return {
        ...state,
        addCredit: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_CREDIT_SUCCESS: {
      const computedCredits =
        index !== undefined
          ? newBalance === 0
            ? 0
            : workersState[index].credits + credits
          : newBalance === 0
            ? 0
            : workerDetailsState.credits + credits;
      const newIncome =
        index !== undefined
          ? newBalance === 0
            ? workersState[index].income + (workersState[index].credits + credits)
            : workersState[index].income
          : newBalance === 0
            ? workerDetailsState.income + (workerDetailsState.credits + credits)
            : workerDetailsState.income;
      const updatedWorkers =
        index !== undefined
          ? update(workersState, { [index]: { credits: { $set: computedCredits }, income: { $set: newIncome } } })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined
          ? workerDetailsState
          : { ...workerDetailsState, credits: computedCredits, income: newIncome };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        addCredit: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addCreditModal: { isOpen },
      };
    }
    case ADD_CREDIT_FAIL: {
      return {
        ...state,
        addCredit: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case ACTIVATE_WORKER: {
      return {
        ...state,
        activateWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACTIVATE_WORKER_SUCCESS: {
      const updatedWorkers =
        index !== undefined
          ? update(workersState, { [index]: { active: { $set: active } } })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined ? workerDetailsState : { ...workerDetailsState, active };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        activateWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case ACTIVATE_WORKER_FAIL: {
      return {
        ...state,
        activateWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case SUSPEND_WORKER: {
      return {
        ...state,
        suspendWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case SUSPEND_WORKER_SUCCESS: {
      const updatedWorkers =
        index !== undefined
          ? update(workersState, { [index]: { suspendedTo: { $set: suspended.toString() } } })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined
          ? workerDetailsState
          : { ...workerDetailsState, suspendedTo: suspended.toString() };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        suspendWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
        suspendWorkerModal: { isOpen: false },
      };
    }
    case SUSPEND_WORKER_FAIL: {
      return {
        ...state,
        suspendWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case CHANGE_WORKER_PASSWORD: {
      return {
        ...state,
        changeWorkerPassword: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CHANGE_WORKER_PASSWORD_SUCCESS: {
      return {
        ...state,
        changeWorkerPassword: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        changeWorkerPasswordModal: { isOpen },
      };
    }
    case CHANGE_WORKER_PASSWORD_FAIL: {
      return {
        ...state,
        changeWorkerPassword: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case CHANGE_WORKER_PASSWORD_RESET: {
      return {
        ...state,
        changeWorkerPassword: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FEATURE_WORKER: {
      return {
        ...state,
        featureWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FEATURE_WORKER_SUCCESS: {
      const updatedWorkers =
        index !== undefined
          ? update(workersState, { [index]: { featured: { $set: featuredSubscriptionValidTo } } })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined
          ? workerDetailsState
          : { ...workerDetailsState, featuredSubscriptionValidTo };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        featureWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case FEATURE_WORKER_FAIL: {
      return {
        ...state,
        featureWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case MAKE_WORKER_ELITE: {
      return {
        ...state,
        makeWorkerElite: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case MAKE_WORKER_ELITE_SUCCESS: {
      const updatedWorkers =
        index !== undefined
          ? update(workersState, { [index]: { elite: { $set: elite } } })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined ? workerDetailsState : { ...workerDetailsState, elite };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        makeWorkerElite: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case MAKE_WORKER_ELITE_FAIL: {
      return {
        ...state,
        makeWorkerElite: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case DELETE_WORKER: {
      return {
        ...state,
        deleteWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_WORKER_SUCCESS: {
      if (index !== undefined) workersState.splice(index, 1);
      else window.location.pathname = WORKERS_ROUTE;

      return {
        ...state,
        workers: workersState,
        deleteWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case DELETE_WORKER_FAIL: {
      return {
        ...state,
        deleteWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case GET_WORKER_DETAILS: {
      return {
        ...state,
        getWorkerDetails: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_DETAILS_SUCCESS: {
      return {
        ...state,
        workerDetails,
        getWorkerDetails: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_DETAILS_FAIL: {
      return {
        ...state,
        getWorkerDetails: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case ADD_WORKER_COMMENT.LOAD: {
      return {
        ...state,
        addComment: {
          commentInfo,
          addCommentState: ADD_WORKER_COMMENT.LOAD,
          error: undefined,
        },
      };
    }
    case ADD_WORKER_COMMENT.SUCCESS: {
      const foundIndex = findIndex(workersState, workerState => workerState._id === workerId);

      const updatedWorkers =
        foundIndex >= 0
          ? update(workersState, { [foundIndex]: { comments: { $set: comments } } })
          : workersState;
      const updatedWorkerDetails =
        index !== undefined ? workerDetailsState : { ...workerDetailsState, comments };

      return {
        ...state,
        workers: updatedWorkers,
        workerDetails: updatedWorkerDetails,
        addComment: {
          commentInfo,
          addCommentState: ADD_WORKER_COMMENT.SUCCESS,
          error: undefined,
        },
      };
    }
    case ADD_WORKER_COMMENT.FAIL: {
      return {
        ...state,
        addComment: {
          commentInfo: undefined,
          addCommentState: ADD_WORKER_COMMENT.FAIL,
          error,
        },
      };
    }
    case TOGGLE_ADD_NEW_WORKER_MODAL: {
      return {
        ...state,
        addNewWorkerModal: {
          isOpen: !addNewWorkerModalIsOpenState,
        },
      };
    }
    case TOGGLE_CHANGE_WORKER_PASSWORD_MODAL: {
      return {
        ...state,
        changeWorkerPasswordModal: {
          isOpen: !changeWorkerPasswordModalIsOpenState,
        },
      };
    }
    case TOGGLE_SUBSCRIBE_WORKER_TO_AUTO_APPLY_MODAL: {
      return {
        ...state,
        subscribeWorkerToAutoApplyModal: {
          isOpen: !subscribeWorkerToAutoApplyModalIsOpenState,
        },
      };
    }
    case TOGGLE_SUBSCRIBE_WORKER_TO_MULTI_FIELDS_MODAL: {
      return {
        ...state,
        subscribeWorkerToMultiFieldsModal: {
          isOpen: !subscribeWorkerToMultiFieldsModalIsOpenState,
        },
      };
    }
    case TOGGLE_EDIT_WORKER_MODAL: {
      return {
        ...state,
        editWorkerModal: {
          isOpen: !editWorkerModalIsOpenState,
        },
      };
    }
    case TOGGLE_ADD_CREDIT_MODAL: {
      return {
        ...state,
        addCreditModal: {
          isOpen: !addCreditModalIsOpenState,
        },
      };
    }
    case GET_WORKERS_REQUESTS_COUNT: {
      return {
        ...state,
        applyNewFilter,
        getWorkersRequestsCount: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_REQUESTS_COUNT_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        workersRequestsCount:
          workersRequestsCountState && !applyNewFilter
            ? unionBy(workersRequestsCountState, workersRequestsCount, '_id')
            : workersRequestsCount,
        hasMoreWorkersRequestsCount: workersRequestsCount.length === 20,
        getWorkersRequestsCount: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_REQUESTS_COUNT_FAIL: {
      return {
        ...state,
        getWorkersRequestsCount: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case SUBSCRIBE_WORKER: {
      return {
        ...state,
        subscribeWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case SUBSCRIBE_WORKER_SUCCESS: {
      let mergeObject = {};
      const { workers: workersEndPoints } = Urls;
      if (subscribeEndpoint === workersEndPoints.subscribeToAutoApply) {
        mergeObject = { autoApplyToRequestsSubscriptionValidTo: subscribeDueDate };
      }
      if (subscribeEndpoint === workersEndPoints.subscribeToMultiFields) {
        mergeObject = { multiFieldsSubscriptionValidTo: subscribeDueDate };
      }
      return {
        ...state,
        subscribeWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        workerDetails: update(workerDetailsState, {
          $merge: mergeObject,
        }),
        subscribeWorkerToAutoApplyModal: { isOpen },
        subscribeWorkerToMultiFieldsModal: { isOpen },
      };
    }
    case SUBSCRIBE_WORKER_FAIL: {
      return {
        ...state,
        subscribeWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case SUBSCRIBE_WORKER_RESET: {
      return {
        ...state,
        subscribeWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_STATISTICS: {
      return {
        ...state,
        getWorkerStatistics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_STATISTICS_SUCCESS: {
      return {
        ...state,
        getWorkerStatistics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        workerStatistics,
      };
    }
    case GET_WORKER_STATISTICS_FAIL: {
      return {
        ...state,
        getWorkerStatistics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS: {
      return {
        ...state,
        getWorkerMotivationAndPunishments: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS: {
      return {
        ...state,
        getWorkerMotivationAndPunishments: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        workerMotivationAndPunishments,
      };
    }
    case GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL: {
      return {
        ...state,
        getWorkerMotivationAndPunishments: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION: {
      return {
        ...state,
        finishWorkerAutoApplySubscription: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS: {
      const updatedWorkerDetails = {
        ...workerDetailsState,
        autoApplyToRequestsSubscriptionValidTo,
      };

      return {
        ...state,
        workerDetails: updatedWorkerDetails,
        finishWorkerAutoApplySubscription: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL: {
      return {
        ...state,
        finishWorkerAutoApplySubscription: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION: {
      return {
        ...state,
        finishWorkerMultiFieldsSubscription: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS: {
      const updatedWorkerDetails = { ...workerDetailsState, multiFieldsSubscriptionValidTo };

      return {
        ...state,
        workerDetails: updatedWorkerDetails,
        finishWorkerMultiFieldsSubscription: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL: {
      return {
        ...state,
        finishWorkerMultiFieldsSubscription: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case TOGGLE_SUSPEND_WORKER_MODAL: {
      return {
        ...state,
        suspendWorkerModal: { isOpen: !suspendWorkerModalIsOpenState },
      };
    }
    case TOGGLE_WORKERS_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: {
          isOpen: !confirmModalIsOpenState,
        },
      };
    }
    case ALLOW_WORKER_TO_RECHARGE: {
      return {
        ...state,
        allowWorkerToRecharge: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ALLOW_WORKER_TO_RECHARGE_SUCCESS: {
      const updatedWorkerDetails = { ...workerDetailsState, isAllowedToRecharge: status };

      return {
        ...state,
        allowWorkerToRecharge: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        workerDetails: updatedWorkerDetails,
        confirmModal: { isOpen: false },
      };
    }
    case ALLOW_WORKER_TO_RECHARGE_FAIL: {
      return {
        ...state,
        allowWorkerToRecharge: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }

    case APPROVE_THE_WORKER_RESIDENCE: {
      return {
        ...state,
        approveTheWorkerResidenceInfo: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }

    case APPROVE_THE_WORKER_RESIDENCE_SUCCESS: {
      return {
        ...state,
        approveTheWorkerResidenceInfo: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }

    case APPROVE_THE_WORKER_RESIDENCE_FAIL: {
      return {
        ...state,
        approveTheWorkerResidenceInfo: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    default:
      return state;
  }
};
