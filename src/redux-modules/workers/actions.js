export const WORKERS = {
  GET_ALL_WORKERS: 'GET_ALL_WORKERS',
  GET_ALL_WORKERS_SUCCESS: 'GET_ALL_WORKERS_SUCCESS',
  GET_ALL_WORKERS_FAIL: 'GET_ALL_WORKERS_FAIL',
  ADD_WORKER: 'ADD_WORKER',
  ADD_WORKER_SUCCESS: 'ADD_WORKER_SUCCESS',
  ADD_WORKER_FAIL: 'ADD_WORKER_FAIL',
  ADD_WORKER_RESET: 'ADD_WORKER_RESET',
  EDIT_WORKER: 'EDIT_WORKER',
  EDIT_WORKER_SUCCESS: 'EDIT_WORKER_SUCCESS',
  EDIT_WORKER_FAIL: 'EDIT_WORKER_FAIL',
  EDIT_WORKER_RESET: 'EDIT_WORKER_RESET',
  ADD_CREDIT: 'ADD_CREDIT',
  ADD_CREDIT_SUCCESS: 'ADD_CREDIT_SUCCESS',
  ADD_CREDIT_FAIL: 'ADD_CREDIT_FAIL',
  ADD_CREDIT_RESET: 'ADD_CREDIT_RESET',
  ACTIVATE_WORKER: 'ACTIVATE_WORKER',
  ACTIVATE_WORKER_SUCCESS: 'ACTIVATE_WORKER_SUCCESS',
  ACTIVATE_WORKER_FAIL: 'ACTIVATE_WORKER_FAIL',
  SUSPEND_WORKER: 'SUSPEND_WORKER',
  SUSPEND_WORKER_SUCCESS: 'SUSPEND_WORKER_SUCCESS',
  SUSPEND_WORKER_FAIL: 'SUSPEND_WORKER_FAIL',
  CHANGE_WORKER_PASSWORD: 'CHANGE_WORKER_PASSWORD',
  CHANGE_WORKER_PASSWORD_SUCCESS: 'CHANGE_WORKER_PASSWORD_SUCCESS',
  CHANGE_WORKER_PASSWORD_FAIL: 'CHANGE_WORKER_PASSWORD_FAIL',
  CHANGE_WORKER_PASSWORD_RESET: 'CHANGE_WORKER_PASSWORD_RESET',
  FEATURE_WORKER: 'FEATURE_WORKER',
  FEATURE_WORKER_SUCCESS: 'FEATURE_WORKER_SUCCESS',
  FEATURE_WORKER_FAIL: 'FEATURE_WORKER_FAIL',
  MAKE_WORKER_ELITE: 'MAKE_WORKER_ELITE',
  MAKE_WORKER_ELITE_SUCCESS: 'MAKE_WORKER_ELITE_SUCCESS',
  MAKE_WORKER_ELITE_FAIL: 'MAKE_WORKER_ELITE_FAIL',
  DELETE_WORKER: 'DELETE_WORKER',
  DELETE_WORKER_SUCCESS: 'DELETE_WORKER_SUCCESS',
  DELETE_WORKER_FAIL: 'DELETE_WORKER_FAIL',
  GET_WORKER_DETAILS: 'GET_WORKER_DETAILS',
  GET_WORKER_DETAILS_SUCCESS: 'GET_WORKER_DETAILS_SUCCESS',
  GET_WORKER_DETAILS_FAIL: 'GET_WORKER_DETAILS_FAIL',
  ADD_WORKER_COMMENT: {
    LOAD: 'WORKERS_ADD_WORKER_COMMENT_LOAD',
    SUCCESS: 'WORKERS_ADD_WORKER_COMMENT_SUCCESS',
    FAIL: 'WORKERS_ADD_WORKER_COMMENT_FAIL',
  },
  TOGGLE_CHANGE_WORKER_PASSWORD_MODAL: 'TOGGLE_CHANGE_WORKER_PASSWORD_MODAL',
  TOGGLE_ADD_NEW_WORKER_MODAL: 'TOGGLE_ADD_NEW_WORKER_MODAL',
  TOGGLE_EDIT_WORKER_MODAL: 'TOGGLE_EDIT_WORKER_MODAL',
  TOGGLE_ADD_CREDIT_MODAL: 'TOGGLE_ADD_CREDIT_MODAL',
  GET_WORKERS_REQUESTS_COUNT: 'GET_WORKERS_REQUESTS_COUNT',
  GET_WORKERS_REQUESTS_COUNT_SUCCESS: 'GET_WORKERS_REQUESTS_COUNT_SUCCESS',
  GET_WORKERS_REQUESTS_COUNT_FAIL: 'GET_WORKERS_REQUESTS_COUNT_FAIL',
  TOGGLE_SUBSCRIBE_WORKER_TO_AUTO_APPLY_MODAL: 'TOGGLE_SUBSCRIBE_WORKER_TO_AUTO_APPLY_MODAL',
  SUBSCRIBE_WORKER: 'SUBSCRIBE_WORKER',
  SUBSCRIBE_WORKER_SUCCESS: 'SUBSCRIBE_WORKER_SUCCESS',
  SUBSCRIBE_WORKER_FAIL: 'SUBSCRIBE_WORKER_FAIL',
  SUBSCRIBE_WORKER_RESET: 'SUBSCRIBE_WORKER_RESET',
  TOGGLE_SUBSCRIBE_WORKER_TO_MULTI_FIELDS_MODAL: 'TOGGLE_SUBSCRIBE_WORKER_TO_MULTI_FIELDS_MODAL',
  GET_WORKER_STATISTICS: 'GET_WORKER_STATISTICS',
  GET_WORKER_STATISTICS_SUCCESS: 'GET_WORKER_STATISTICS_SUCCESS',
  GET_WORKER_STATISTICS_FAIL: 'GET_WORKER_STATISTICS_FAIL',
  GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS: 'GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS',
  GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS: 'GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS',
  GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL: 'GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL',
  TOGGLE_SUSPEND_WORKER_MODAL: 'TOGGLE_SUSPEND_WORKER_MODAL',
  FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION: 'FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION',
  FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS: 'FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS',
  FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL: 'FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL',
  FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION: 'FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION',
  FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS:
    'FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS',
  FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL: 'FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL',
  TOGGLE_WORKERS_CONFIRM_MODAL: 'TOGGLE_WORKERS_CONFIRM_MODAL',
  ALLOW_WORKER_TO_RECHARGE: 'ALLOW_WORKER_TO_RECHARGE',
  ALLOW_WORKER_TO_RECHARGE_SUCCESS: 'ALLOW_WORKER_TO_RECHARGE_SUCCESS',
  ALLOW_WORKER_TO_RECHARGE_FAIL: 'ALLOW_WORKER_TO_RECHARGE_FAIL',

  APPROVE_THE_WORKER_RESIDENCE: 'APPROVE_THE_WORKER_RESIDENCE',
  APPROVE_THE_WORKER_RESIDENCE_SUCCESS: 'APPROVE_THE_WORKER_RESIDENCE_SUCCESS',
  APPROVE_THE_WORKER_RESIDENCE_FAIL: 'APPROVE_THE_WORKER_RESIDENCE_FAIL',
};

export const getAllWorkers = (options, applyNewFilter = false) => ({
  type: WORKERS.GET_ALL_WORKERS,
  options,
  applyNewFilter,
});

export const getAllWorkersSuccess = (workers, applyNewFilter = false) => ({
  type: WORKERS.GET_ALL_WORKERS_SUCCESS,
  workers,
  applyNewFilter,
});

export const getAllWorkersFail = error => ({
  type: WORKERS.GET_ALL_WORKERS_FAIL,
  error,
});

export const addWorker = newWorkerInfo => ({
  type: WORKERS.ADD_WORKER,
  newWorkerInfo,
});

export const addWorkerSuccess = newWorkerInfo => ({
  type: WORKERS.ADD_WORKER_SUCCESS,
  newWorkerInfo,
});

export const addWorkerFail = error => ({
  type: WORKERS.ADD_WORKER_FAIL,
  error,
});

export const addWorkerReset = () => ({
  type: WORKERS.ADD_WORKER_RESET,
});

export const editWorker = (workerId, index, editParams) => ({
  type: WORKERS.EDIT_WORKER,
  workerId,
  index,
  editParams,
});

export const editWorkerSuccess = (index, workerDetails) => ({
  type: WORKERS.EDIT_WORKER_SUCCESS,
  index,
  workerDetails,
});

export const editWorkerFail = error => ({
  type: WORKERS.EDIT_WORKER_FAIL,
  error,
});

export const editWorkerReset = () => ({
  type: WORKERS.EDIT_WORKER_RESET,
});

export const addCredit = (index, addCreditInfo) => ({
  type: WORKERS.ADD_CREDIT,
  index,
  addCreditInfo,
});

export const addCreditSuccess = (index, credits) => ({
  type: WORKERS.ADD_CREDIT_SUCCESS,
  index,
  credits,
});

export const addCreditFail = error => ({
  type: WORKERS.ADD_CREDIT_FAIL,
  error,
});

export const addCreditReset = () => ({
  type: WORKERS.ADD_CREDIT_RESET,
});

export const activateWorker = (workerId, index, active = true) => ({
  type: WORKERS.ACTIVATE_WORKER,
  workerId,
  index,
  active,
});

export const activateWorkerSuccess = (index, active) => ({
  type: WORKERS.ACTIVATE_WORKER_SUCCESS,
  index,
  active,
});

export const activateWorkerFail = error => ({
  type: WORKERS.ACTIVATE_WORKER_FAIL,
  error,
});

export const suspendWorker = (workerId, index, suspended = true) => ({
  type: WORKERS.SUSPEND_WORKER,
  workerId,
  index,
  suspended,
});

export const suspendWorkerSuccess = (index, suspended) => ({
  type: WORKERS.SUSPEND_WORKER_SUCCESS,
  index,
  suspended,
});

export const suspendWorkerFail = error => ({
  type: WORKERS.SUSPEND_WORKER_FAIL,
  error,
});

export const changeWorkerPassword = (index, newPasswordInfo) => ({
  type: WORKERS.CHANGE_WORKER_PASSWORD,
  index,
  newPasswordInfo,
});

export const changeWorkerPasswordSuccess = () => ({
  type: WORKERS.CHANGE_WORKER_PASSWORD_SUCCESS,
});

export const changeWorkerPasswordFail = error => ({
  type: WORKERS.CHANGE_WORKER_PASSWORD_FAIL,
  error,
});

export const changeWorkerPasswordReset = () => ({
  type: WORKERS.CHANGE_WORKER_PASSWORD_RESET,
});

export const featureWorker = (workerId, index, featured = true) => ({
  type: WORKERS.FEATURE_WORKER,
  workerId,
  index,
  featured,
});

export const featureWorkerSuccess = (index, featured) => ({
  type: WORKERS.FEATURE_WORKER_SUCCESS,
  index,
  featured,
});

export const featureWorkerFail = error => ({
  type: WORKERS.FEATURE_WORKER_FAIL,
  error,
});

export const makeWorkerElite = (workerId, index, elite = true) => ({
  type: WORKERS.MAKE_WORKER_ELITE,
  workerId,
  index,
  elite,
});

export const makeWorkerEliteSuccess = (index, elite) => ({
  type: WORKERS.MAKE_WORKER_ELITE_SUCCESS,
  index,
  elite,
});

export const makeWorkerEliteFail = error => ({
  type: WORKERS.MAKE_WORKER_ELITE_FAIL,
  error,
});

export const deleteWorker = (workerId, index) => ({
  type: WORKERS.DELETE_WORKER,
  workerId,
  index,
});

export const deleteWorkerSuccess = index => ({
  type: WORKERS.DELETE_WORKER_SUCCESS,
  index,
});

export const deleteWorkerFail = error => ({
  type: WORKERS.DELETE_WORKER_FAIL,
  error,
});

export const getWorkerDetails = workerUserName => ({
  type: WORKERS.GET_WORKER_DETAILS,
  workerUserName,
});

export const getWorkerDetailsSuccess = workerDetails => ({
  type: WORKERS.GET_WORKER_DETAILS_SUCCESS,
  workerDetails,
});

export const getWorkerDetailsFail = error => ({
  type: WORKERS.GET_WORKER_DETAILS_FAIL,
  error,
});

export const addComment = (commentInfo, workerId) => ({
  type: WORKERS.ADD_WORKER_COMMENT.LOAD,
  commentInfo,
  workerId,
});

export const addCommentSuccess = (comments, workerId) => ({
  type: WORKERS.ADD_WORKER_COMMENT.SUCCESS,
  comments,
  workerId,
});

export const addCommentFail = error => ({
  type: WORKERS.ADD_WORKER_COMMENT.FAIL,
  error,
});

export const toggleChangeWorkerPasswordModal = () => ({
  type: WORKERS.TOGGLE_CHANGE_WORKER_PASSWORD_MODAL,
});

export const toggleEditWorkerModal = () => ({
  type: WORKERS.TOGGLE_EDIT_WORKER_MODAL,
});

export const toggleAddNewWorkerModal = () => ({
  type: WORKERS.TOGGLE_ADD_NEW_WORKER_MODAL,
});

export const toggleAddCreditModal = () => ({
  type: WORKERS.TOGGLE_ADD_CREDIT_MODAL,
});

export const getWorkersRequestsCount = (options, applyNewFilter: false) => ({
  type: WORKERS.GET_WORKERS_REQUESTS_COUNT,
  options,
  applyNewFilter,
});

export const getWorkersRequestsCountSuccess = (workersRequestsCount, applyNewFilter: false) => ({
  type: WORKERS.GET_WORKERS_REQUESTS_COUNT_SUCCESS,
  workersRequestsCount,
  applyNewFilter,
});

export const getWorkersRequestsCountFail = error => ({
  type: WORKERS.GET_WORKERS_REQUESTS_COUNT_FAIL,
  error,
});

export const toggleSubscribeWorkerToAutoApplyModal = (options, applyNewFilter: false) => ({
  type: WORKERS.TOGGLE_SUBSCRIBE_WORKER_TO_AUTO_APPLY_MODAL,
  options,
  applyNewFilter,
});

export const subscribeWorker = (index, subscribeEndpoint, Info) => ({
  type: WORKERS.SUBSCRIBE_WORKER,
  index,
  subscribeEndpoint,
  Info,
});

export const subscribeWorkerToAutoApplySuccess = (subscribeDueDate, subscribeEndpoint) => ({
  type: WORKERS.SUBSCRIBE_WORKER_SUCCESS,
  subscribeDueDate,
  subscribeEndpoint,
});

export const subscribeWorkerToAutoApplyFail = error => ({
  type: WORKERS.SUBSCRIBE_WORKER_FAIL,
  error,
});

export const subscribeWorkerToAutoApplyReset = () => ({
  type: WORKERS.SUBSCRIBE_WORKER_RESET,
});

export const toggleSubscribeWorkerToMultiFieldsModal = () => ({
  type: WORKERS.TOGGLE_SUBSCRIBE_WORKER_TO_MULTI_FIELDS_MODAL,
});

export const getWorkerStatistics = workerId => ({
  type: WORKERS.GET_WORKER_STATISTICS,
  workerId,
});

export const getWorkerStatisticsSuccess = (subscribeDueDate, subscribeEndpoint) => ({
  type: WORKERS.GET_WORKER_STATISTICS_SUCCESS,
  subscribeDueDate,
  subscribeEndpoint,
});

export const getWorkerStatisticsFail = error => ({
  type: WORKERS.GET_WORKER_STATISTICS_FAIL,
  error,
});

export const getWorkerMotivationAndPunishments = workerId => ({
  type: WORKERS.GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS,
  workerId,
});

export const getWorkerMotivationAndPunishmentsSuccess = (subscribeDueDate, subscribeEndpoint) => ({
  type: WORKERS.GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_SUCCESS,
  subscribeDueDate,
  subscribeEndpoint,
});

export const getWorkerMotivationAndPunishmentsFail = error => ({
  type: WORKERS.GET_WORKER_MOTIVATIONS_AND_PUNISHMENTS_FAIL,
  error,
});

export const finishWorkerAutoApplySubscription = workerId => ({
  type: WORKERS.FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION,
  workerId,
});

export const finishWorkerAutoApplySubscriptionSuccess = autoApplyToRequestsSubscriptionValidTo => ({
  type: WORKERS.FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_SUCCESS,
  autoApplyToRequestsSubscriptionValidTo,
});

export const finishWorkerAutoApplySubscriptionFail = error => ({
  type: WORKERS.FINISH_WORKER_AUTO_APPLY_SUBSCRIPTION_FAIL,
  error,
});

export const finishWorkerMultiFieldsSubscription = workerId => ({
  type: WORKERS.FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION,
  workerId,
});

export const finishWorkerMultiFieldsSubscriptionSuccess = multiFieldsSubscriptionValidTo => ({
  type: WORKERS.FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_SUCCESS,
  multiFieldsSubscriptionValidTo,
});

export const finishWorkerMultiFieldsSubscriptionFail = error => ({
  type: WORKERS.FINISH_WORKER_MULTI_FIELDS_SUBSCRIPTION_FAIL,
  error,
});

export const toggleSuspendWorkerModal = workerId => ({
  type: WORKERS.TOGGLE_SUSPEND_WORKER_MODAL,
  workerId,
});

export const toggleConfirmModal = isOpen => ({
  type: WORKERS.TOGGLE_WORKERS_CONFIRM_MODAL,
  isOpen,
});

export const allowWorkerToRecharge = (workerId, status = true) => ({
  type: WORKERS.ALLOW_WORKER_TO_RECHARGE,
  workerId,
  status,
});

export const allowWorkerToRechargeSuccess = status => ({
  type: WORKERS.ALLOW_WORKER_TO_RECHARGE_SUCCESS,
  status,
});

export const allowWorkerToRechargeFail = error => ({
  type: WORKERS.ALLOW_WORKER_TO_RECHARGE_FAIL,
  error,
});

export const approveTheWorkerResidenceInfo = workerId => ({
  type: WORKERS.APPROVE_THE_WORKER_RESIDENCE,
  workerId,
});

export const approveTheWorkerResidenceInfoSuccess = workerId => ({
  type: WORKERS.APPROVE_THE_WORKER_RESIDENCE_SUCCESS,
  workerId,
});

export const approveTheWorkerResidenceInfoFail = error => ({
  type: WORKERS.APPROVE_THE_WORKER_RESIDENCE_FAIL,
  error,
});
