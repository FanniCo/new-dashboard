import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { NOTIFICATIONS } from './actions';

const { SEND_NOTIFICATIONS, SEND_NOTIFICATIONS_SUCCESS, SEND_NOTIFICATIONS_FAIL } = NOTIFICATIONS;

function* sendNotifications(payload) {
  const { options: body } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const url = user.targetNotification;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: SEND_NOTIFICATIONS_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: SEND_NOTIFICATIONS_FAIL,
      err: message,
    });
  }
}

function* notificationsSaga() {
  yield takeEvery(SEND_NOTIFICATIONS, sendNotifications);
}

export default notificationsSaga;
