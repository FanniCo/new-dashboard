import { NOTIFICATIONS } from './actions';

const initialState = {
  sendNotifications: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  snackBar: { show: false, message: '' },
};

export default (state = initialState, { type, ...payload }) => {
  const { SEND_NOTIFICATIONS, SEND_NOTIFICATIONS_SUCCESS, SEND_NOTIFICATIONS_FAIL } = NOTIFICATIONS;
  const { err } = payload;

  switch (type) {
    case SEND_NOTIFICATIONS: {
      return {
        ...state,
        sendNotifications: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        snackBar: { show: false, message: '' },
      };
    }
    case SEND_NOTIFICATIONS_SUCCESS: {
      return {
        ...state,
        sendNotifications: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        snackBar: { show: true, message: 'تم إرسال التنبيه بنجاح' },
      };
    }
    case SEND_NOTIFICATIONS_FAIL: {
      return {
        ...state,
        sendNotifications: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
        snackBar: { show: true, message: err },
      };
    }
    default:
      return state;
  }
};
