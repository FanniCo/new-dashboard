export const NOTIFICATIONS = {
  SEND_NOTIFICATIONS: 'SEND_NOTIFICATIONS',
  SEND_NOTIFICATIONS_SUCCESS: 'SEND_NOTIFICATIONS_SUCCESS',
  SEND_NOTIFICATIONS_FAIL: 'SEND_NOTIFICATIONS_FAIL',
};

export const sendNotifications = options => ({
  type: NOTIFICATIONS.SEND_NOTIFICATIONS,
  options,
});

export const sendNotificationsSuccess = () => ({
  type: NOTIFICATIONS.SEND_NOTIFICATIONS_SUCCESS,
});

export const sendNotificationsFail = err => ({
  type: NOTIFICATIONS.SEND_NOTIFICATIONS_FAIL,
  err,
});
