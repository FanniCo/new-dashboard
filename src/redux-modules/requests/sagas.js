import {
  call,
  fork,
  put,
  race,
  take,
  takeEvery,
  takeLatest
} from 'redux-saga/effects';
import {delay, eventChannel} from 'redux-saga';
import Cookies from 'js-cookie';
import {COOKIES_KEYS} from 'utils/constants';
import socketIOClient from 'socket.io-client';
import querystring from 'querystring';
import {REQUESTS} from './actions';
import {Api, Status, Urls} from '../../utils/api';

const {
  GET_ALL_REQUESTS,
  GET_ALL_REQUESTS_ALL,
  GET_ALL_REQUESTS_SUCCESS,
  GET_ALL_REQUESTS_FAIL,
  CANCEL_REQUEST,
  CANCEL_REQUEST_SUCCESS,
  CANCEL_REQUEST_FAIL,
  GET_REQUEST_DETAILS_BY_PRETTY_ID,
  GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS,
  GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL,
  ADD_REQUEST_COMMENT,
  ADD_REQUEST_TICKET,
  ADD_REQUEST_TICKET_SUCCESS,
  ADD_REQUEST_TICKET_FAIL,
  EDIT_REQUEST_INVOICE,
  EDIT_REQUEST_INVOICE_SUCCESS,
  EDIT_REQUEST_INVOICE_FAIL,
  UPDATE_REQUEST_TICKET_STATUS,
  UPDATE_REQUEST_TICKET_STATUS_SUCCESS,
  UPDATE_REQUEST_TICKET_STATUS_FAIL,
  GET_REQUESTS_PAYMENTS,
  GET_REQUESTS_PAYMENTS_SUCCESS,
  GET_REQUESTS_PAYMENTS_FAIL,
  ADD_REQUEST_FOLLOW_UPS_LOAD,
  ADD_REQUEST_FOLLOW_UPS_SUCCESS,
  ADD_REQUEST_FOLLOW_UPS_FAIL,
  CONNECT_TO_SOCKET_IO,
  NEW_REQUEST_CREATED,
  SOCKET_IO_SERVER_ON,
  SOCKET_IO_SERVER_OFF,
  REQUEST_UPDATED,
  FINISH_REQUEST,
  FINISH_REQUEST_SUCCESS,
  FINISH_REQUEST_FAIL,
  FINISH_REQUEST_FOR_RECONDITIONING_FIELD,
  FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS,
  FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL,
  ACTIVATE_REQUEST,
  ACTIVATE_REQUEST_SUCCESS,
  ACTIVATE_REQUEST_FAIL,
  GET_RECONDITIONING_REQUESTS_COUNT,
  GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS,
  GET_RECONDITIONING_REQUESTS_COUNT_FAIL,
  ACTIVATE_CANCELLED_REQUEST,
  ACTIVATE_CANCELLED_REQUEST_SUCCESS,
  ACTIVATE_CANCELLED_REQUEST_FAIL,
  CANCEL_ALL_PENDING_REQUESTS,
  CANCEL_ALL_PENDING_REQUESTS_SUCCESS,
  CANCEL_ALL_PENDING_REQUESTS_FAIL,
  POSTPONE_REQUEST,
  POSTPONE_REQUEST_SUCCESS,
  POSTPONE_REQUEST_FAIL,

  ACTIVATE_POSTPONED_REQUEST,
  ACTIVATE_POSTPONED_REQUEST_SUCCESS,
  ACTIVATE_POSTPONED_REQUEST_FAIL,

  CHOOSE_WORKER,
  CHOOSE_WORKER_SUCCESS,
  CHOOSE_WORKER_FAIL,

  ACCEPT_WORKER,
  ACCEPT_WORKER_SUCCESS,
  ACCEPT_WORKER_FAIL
} = REQUESTS;
let socket;

function* getAllRequests(payload) {
  const {options, applyNewFilter} = payload;
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const url = `${requests.getAllRequests}?${querystring.stringify(options)}`;

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {response: allRequests} = response;

    yield put({
      type: GET_ALL_REQUESTS_SUCCESS,
      requests: allRequests,
      applyNewFilter,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: GET_ALL_REQUESTS_FAIL,
      err: message,
    });
  }
}

function* getAllRequestsAll(payload) {
  const {options, applyNewFilter} = payload;
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const url = `${requests.getAllRequestsAll}?${querystring.stringify(options)}`;

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {response: allRequests} = response;

    yield put({
      type: GET_ALL_REQUESTS_SUCCESS,
      requests: allRequests,
      applyNewFilter,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: GET_ALL_REQUESTS_FAIL,
      err: message,
    });
  }
}


function* cancelRequest(payload) {
  const {requestId, requestInfo} = payload;
  const {
    requestIndex,
    cancellationReason,
    cancellationReasonStatus,
    cancellationReasonComment,
  } = requestInfo;
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const url = requests.cancelRequest;
  const api = new Api();
  const body = {
    requestId,
    cancellationReason,
    cancellationReasonStatus,
    cancellationReasonComment,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: CANCEL_REQUEST_SUCCESS,
      index: requestIndex,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: CANCEL_REQUEST_FAIL,
      err: message,
    });
  }
}

function* getRequestDetails(payload) {
  const {requestPrettyId} = payload;
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const url = `${requests.getAllRequests}/prettyId?requestPrettyId=${requestPrettyId}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {response: requestDetails} = response;

    yield put({
      type: GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS,
      requestDetails,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL,
      err: message,
    });
  }
}

function* addComment(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.comments;
  const {commentInfo} = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.post(url, commentInfo, header);
  const responseStatus = response.status;
  const {comments, _id} = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ADD_REQUEST_COMMENT.SUCCESS,
      comments,
      requestId: _id,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ADD_REQUEST_COMMENT.FAIL,
      err: message,
    });
  }
}

function* addTicket(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {tickets} = Urls;
  const api = new Api();
  const url = tickets.general;
  const {ticketInfo} = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = {...ticketInfo};

  const response = yield api.post(url, body, header);
  const responseStatus = response.status;
  const ticket = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ADD_REQUEST_TICKET_SUCCESS,
      ticket,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ADD_REQUEST_TICKET_FAIL,
      err: message,
    });
  }
}

function* editInvoice(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.editCostDetails;
  const {invoice} = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = {...invoice};

  const response = yield api.patch(url, body, header);
  const responseStatus = response.status;
  const updatedRequest = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: EDIT_REQUEST_INVOICE_SUCCESS,
      updatedRequest,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: EDIT_REQUEST_INVOICE_FAIL,
      err: message,
    });
  }
}

function* updateTicketStatus(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {tickets} = Urls;
  const api = new Api();
  const {
    ticketInfo: {_id, statuses, ...restTicketInfo},
  } = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const url = `${tickets.general}/${_id}`;
  const body = {...restTicketInfo};

  const response = yield api.patch(url, body, header);
  const responseStatus = response.status;
  const ticket = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: UPDATE_REQUEST_TICKET_STATUS_SUCCESS,
      ticket,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: UPDATE_REQUEST_TICKET_STATUS_FAIL,
      err: message,
    });
  }
}

function* getRequestsPayments(payload) {
  const {options, applyNewFilter} = payload;
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = requests.getRequestsPayments;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {response: allRequestsPayments} = response;

    yield put({
      type: GET_REQUESTS_PAYMENTS_SUCCESS,
      requestsPayments: allRequestsPayments,
      applyNewFilter,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: GET_REQUESTS_PAYMENTS_FAIL,
      err: message,
    });
  }
}

function* addRequestFollowUps(payload) {
  const api = new Api();
  const {requests} = Urls;
  const url = requests.addRequestFollowUps;
  const {requestFollowUpsInfo} = payload;
  const header = [
    {
      key: 'token',
      value: Cookies.get(COOKIES_KEYS.TOKEN),
    },
  ];
  const body = {...requestFollowUpsInfo};
  const response = yield api.post(url, body, header);
  const responseStatus = response.status;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ADD_REQUEST_FOLLOW_UPS_SUCCESS,
      requestFollowUpsInfo,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ADD_REQUEST_FOLLOW_UPS_FAIL,
      err: message,
    });
  }
}

function connectToSocketIo() {
  const {requests} = Urls;
  const url = requests.getAllRequests;

  socket = socketIOClient(url);
  return new Promise(resolve => {
    socket.on('connect', () => {
      resolve(socket);
    });
  });
}

const reconnect = () => {
  const {requests} = Urls;
  const url = requests.getAllRequests;

  socket = socketIOClient(url);
  return new Promise(resolve => {
    socket.on('reconnect', () => {
      resolve(socket);
    });
  });
};

const disconnect = () => {
  const {requests} = Urls;
  const url = requests.getAllRequests;

  socket = socketIOClient(url);
  return new Promise(resolve => {
    socket.on('disconnect', () => {
      resolve(socket);
    });
  });
};

const createSocketChannel = sendSocket =>
  eventChannel(emit => {
    sendSocket.on('newRequest', data => {
      emit({
        type: NEW_REQUEST_CREATED,
        data,
        key: 'newRequest'
      });
    });

    sendSocket.on('requestUpdated', data => {
      emit({
        type: REQUEST_UPDATED,
        data,
        key: 'updatedRequest'
      });
    });

    return () => {
      sendSocket.off('newRequest', data => {
        emit({type: NEW_REQUEST_CREATED, data});
      });
    };
  });

const listenDisconnectSaga = function* () {
  while (true) {
    yield call(disconnect);
    yield put({type: SOCKET_IO_SERVER_OFF});
  }
};

const listenConnectSaga = function* () {
  while (true) {
    yield call(reconnect);
    yield put({type: SOCKET_IO_SERVER_ON});
  }
};

function* connect() {
  try {
    const {timeout} = yield race({
      connected: call(connectToSocketIo),
      timeout: delay(20000),
    });

    if (timeout) {
      yield put({type: SOCKET_IO_SERVER_OFF});
    }

    const createdSocket = yield call(connectToSocketIo);
    const socketChannel = yield call(createSocketChannel, createdSocket);
    yield fork(listenDisconnectSaga);
    yield fork(listenConnectSaga);
    yield put({type: SOCKET_IO_SERVER_ON});
    while (true) {
      const payload = yield take(socketChannel);
      yield put({
        type: payload.type,
        [payload.key]: payload.data
      });
    }
  } catch (error) {
    console.log(error);
  }
}

function* finishRequest(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.finishRequest;
  const {info} = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = {...info};

  const response = yield api.patch(url, body, header);

  const {response: requestDetails, status} = response;

  // requestDetails is empty here.
  if (Status.isSuccess(status)) {
    yield put({
      type: FINISH_REQUEST_SUCCESS,
      requestDetails,
    });

    window.location.reload();
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: FINISH_REQUEST_FAIL,
      err: message,
    });
  }
}

function* finishRequestForReconditiongFiled(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.finishForReconditioningField;
  const {info} = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = {...info};

  const response = yield api.patch(url, body, header);
  const responseStatus = response.status;
  const requestDetails = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS,
      requestDetails,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL,
      err: message,
    });
  }
}

function* activateRequest(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.activate;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const {info} = payload;
  const body = {...info};
  const response = yield api.patch(url, body, header);
  const responseStatus = response.status;
  const requestDetails = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ACTIVATE_REQUEST_SUCCESS,
      requestDetails,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ACTIVATE_REQUEST_FAIL,
      err: message,
    });
  }
}

function* getReconditioningRequestsCount() {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.reconditioningCount;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(url, header);
  const responseStatus = response.status;
  const reconditioningRequestsCount = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS,
      reconditioningRequestsCount,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: GET_RECONDITIONING_REQUESTS_COUNT_FAIL,
      err: message,
    });
  }
}

function* activateCancelledRequest(payload) {
  const {requestId, index} = payload;
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const url = requests.activateCancelledRequest;
  const api = new Api();
  const body = {
    requestId,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: ACTIVATE_CANCELLED_REQUEST_SUCCESS,
      index,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ACTIVATE_CANCELLED_REQUEST_FAIL,
      err: message,
    });
  }
}

function* cancelAllPendingRequests() {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const url = requests.cancelAllPendingRequests;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, {}, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: CANCEL_ALL_PENDING_REQUESTS_SUCCESS,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: CANCEL_ALL_PENDING_REQUESTS_FAIL,
      err: message,
    });
  }
}

function* postponeRequest(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.postpone;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const {requestId} = payload;
  const body = {requestId, confirmation: true};

  const response = yield api.post(url, body, header);
  const responseStatus = response.status;
  const requestDetails = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: POSTPONE_REQUEST_SUCCESS,
      requestDetails,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: POSTPONE_REQUEST_FAIL,
      err: message,
    });
  }
}

function* activatePostponedRequest(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.activatePostponed;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const {requestId} = payload;
  const response = yield api.post(url, {requestId}, header);
  const responseStatus = response.status;
  const requestDetails = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ACTIVATE_POSTPONED_REQUEST_SUCCESS,
      requestDetails,
    });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ACTIVATE_POSTPONED_REQUEST_FAIL,
      err: message,
    });
  }
}

function* chooseWorker(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.chooseWorker;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const {requestId, workerId} = payload;
  const response = yield api.post(url, {
    requestId,
    workerId
  }, header);
  const responseStatus = response.status;

  if (Status.isSuccess(responseStatus)) {
    yield put({type: CHOOSE_WORKER_SUCCESS});
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: CHOOSE_WORKER_FAIL,
      err: message,
    });
  }
}

function* acceptWorker(payload) {
  const {TOKEN} = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {requests} = Urls;
  const api = new Api();
  const url = requests.chooseWorker;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const {requestId, workerId} = payload;
  const response = yield api.post(url, {
    requestId,
    workerId
  }, header);
  const responseStatus = response.status;
  const { response: requestDetails } = response;

  if (Status.isSuccess(responseStatus)) {
    yield put({ type: ACCEPT_WORKER_SUCCESS, requestDetails });
  } else {
    const {
      response: {message},
    } = response;

    yield put({
      type: ACCEPT_WORKER_FAIL,
      err: message,
    });
  }
}


function* requestsSaga() {
  yield takeLatest(GET_ALL_REQUESTS, getAllRequests);
  yield takeLatest(GET_ALL_REQUESTS_ALL, getAllRequestsAll);
  yield takeEvery(CANCEL_REQUEST, cancelRequest);
  yield takeEvery(GET_REQUEST_DETAILS_BY_PRETTY_ID, getRequestDetails);
  yield takeEvery(ADD_REQUEST_COMMENT.LOAD, addComment);
  yield takeEvery(ADD_REQUEST_TICKET, addTicket);
  yield takeEvery(EDIT_REQUEST_INVOICE, editInvoice);
  yield takeEvery(UPDATE_REQUEST_TICKET_STATUS, updateTicketStatus);
  yield takeLatest(GET_REQUESTS_PAYMENTS, getRequestsPayments);
  yield takeEvery(ADD_REQUEST_FOLLOW_UPS_LOAD, addRequestFollowUps);
  yield takeEvery(CONNECT_TO_SOCKET_IO, connect);
  yield takeEvery(FINISH_REQUEST, finishRequest);
  yield takeEvery(FINISH_REQUEST_FOR_RECONDITIONING_FIELD, finishRequestForReconditiongFiled);
  yield takeEvery(ACTIVATE_REQUEST, activateRequest);
  yield takeEvery(GET_RECONDITIONING_REQUESTS_COUNT, getReconditioningRequestsCount);
  yield takeEvery(ACTIVATE_CANCELLED_REQUEST, activateCancelledRequest);
  yield takeEvery(CANCEL_ALL_PENDING_REQUESTS, cancelAllPendingRequests);
  yield takeEvery(POSTPONE_REQUEST, postponeRequest);
  yield takeEvery(ACTIVATE_POSTPONED_REQUEST, activatePostponedRequest);
  yield takeEvery(CHOOSE_WORKER, chooseWorker);
  yield takeEvery(ACCEPT_WORKER, acceptWorker);

}

export default requestsSaga;
