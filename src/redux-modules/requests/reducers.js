import { findIndex, unionBy, find } from 'lodash';
import update from 'immutability-helper';
import { REQUESTS } from './actions';
import {REQUESTS_STATUSES} from "utils/constants";

const initialState = {
  requests: undefined,
  hasMoreRequests: true,
  applyNewFilter: false,
  requestDetails: undefined,
  requestsPayments: undefined,
  hasMoreRequestsPayments: true,
  getAllRequests: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getAllRequestsAll: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  cancelRequest: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
    isModalOpen: false,
  },
  getRequestDetails: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addComment: {
    commentInfo: undefined,
    addCommentState: undefined,
    error: undefined,
  },
  addTicket: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editInvoice: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  updateTicketStatus: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getRequestsPayments: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  opsCommentsModal: { isOpen: false },
  confirmModal: {
    isOpen: false,
    isFail: { isError: false, message: '' },
  },
  addRequestFollowUps: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  requestFollowUpsModal: { isOpen: false },
  socketIoStatus: '',
  requestId: undefined,
  finishRequest: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  activateRequest: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getReconditioningRequestsCount: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  cancelAllPendingRequests: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  reconditioningRequestsCount: undefined,
  postponeRequest: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
    isModalOpen: false,
  },
  activatePostponedRequest: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  chooseWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },

  acceptWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_REQUESTS,
    GET_ALL_REQUESTS_ALL,
    GET_ALL_REQUESTS_SUCCESS,
    GET_ALL_REQUESTS_FAIL,
    CANCEL_REQUEST,
    CANCEL_REQUEST_SUCCESS,
    CANCEL_REQUEST_FAIL,
    GET_REQUEST_DETAILS_BY_PRETTY_ID,
    GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS,
    GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL,
    ADD_REQUEST_COMMENT,
    ADD_REQUEST_TICKET,
    ADD_REQUEST_TICKET_SUCCESS,
    ADD_REQUEST_TICKET_FAIL,
    EDIT_REQUEST_INVOICE,
    EDIT_REQUEST_INVOICE_SUCCESS,
    EDIT_REQUEST_INVOICE_FAIL,
    UPDATE_REQUEST_TICKET_STATUS,
    UPDATE_REQUEST_TICKET_STATUS_SUCCESS,
    UPDATE_REQUEST_TICKET_STATUS_FAIL,
    GET_REQUESTS_PAYMENTS,
    GET_REQUESTS_PAYMENTS_SUCCESS,
    GET_REQUESTS_PAYMENTS_FAIL,
    ADD_REQUEST_FOLLOW_UPS_LOAD,
    ADD_REQUEST_FOLLOW_UPS_SUCCESS,
    ADD_REQUEST_FOLLOW_UPS_FAIL,
    TOGGLE_REQUEST_FOLLOW_UPS_MODAL,
    NEW_REQUEST_CREATED,
    CONNECT_TO_SOCKET_IO,
    SOCKET_IO_SERVER_OFF,
    SOCKET_IO_SERVER_ON,
    REQUEST_UPDATED,
    FINISH_REQUEST,
    FINISH_REQUEST_SUCCESS,
    FINISH_REQUEST_FAIL,
    ACTIVATE_REQUEST,
    ACTIVATE_REQUEST_SUCCESS,
    ACTIVATE_REQUEST_FAIL,
    FINISH_REQUEST_FOR_RECONDITIONING_FIELD,
    FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS,
    FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL,
    GET_RECONDITIONING_REQUESTS_COUNT,
    GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS,
    GET_RECONDITIONING_REQUESTS_COUNT_FAIL,
    ACTIVATE_CANCELLED_REQUEST,
    ACTIVATE_CANCELLED_REQUEST_SUCCESS,
    ACTIVATE_CANCELLED_REQUEST_FAIL,
    CANCEL_ALL_PENDING_REQUESTS,
    CANCEL_ALL_PENDING_REQUESTS_SUCCESS,
    CANCEL_ALL_PENDING_REQUESTS_FAIL,
    TOGGLE_ADMIN_CANCEL_REQUEST_MODAL,

    POSTPONE_REQUEST,
    POSTPONE_REQUEST_SUCCESS,
    POSTPONE_REQUEST_FAIL,

    ACTIVATE_POSTPONED_REQUEST,
    ACTIVATE_POSTPONED_REQUEST_SUCCESS,
    ACTIVATE_POSTPONED_REQUEST_FAIL,

    CHOOSE_WORKER,
    CHOOSE_WORKER_SUCCESS,
    CHOOSE_WORKER_FAIL,

    ACCEPT_WORKER,
    ACCEPT_WORKER_SUCCESS,
    ACCEPT_WORKER_FAIL
  } = REQUESTS;
  const {
    requests,
    requestsPayments,
    applyNewFilter,
    index,
    requestDetails,
    isOpen,
    commentInfo,
    comments,
    ticket,
    invoice,
    requestFollowUpsInfo,
    newRequest,
    updatedRequest,
    requestId,
    reconditioningRequestsCount,
    err,
  } = payload;
  const {
    requests: requestsState,
    requestDetails: requestDetailsState,
    requestsPayments: requestsPaymentsState,
    requestFollowUpsModal: { isOpen: followUpsModalIsOpenState },
  } = state;

  switch (type) {
    case GET_ALL_REQUESTS: {
      return {
        ...state,
        applyNewFilter,
        getAllRequests: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_REQUESTS_ALL: {
      return {
        ...state,
        applyNewFilter,
        getAllRequestsAll: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_REQUESTS_SUCCESS: {
      return {
        ...state,
        requests:
          requestsState && !applyNewFilter ? unionBy(requestsState, requests, '_id') : requests,
        hasMoreRequests: requests.length === 20,
        getAllRequests: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_REQUESTS_FAIL: {
      return {
        ...state,
        getAllRequests: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case CANCEL_REQUEST: {
      return {
        ...state,
        cancelRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
          isModalOpen: state.cancelRequest.isModalOpen,
        },
      };
    }
    case CANCEL_REQUEST_SUCCESS: {
      const updatedRequests =
        index !== undefined
          ? update(requestsState, { [index]: { status: { $set: 'canceled' } } })
          : requestsState;
      const updatedRequestDetails =
        index !== undefined ? requestDetailsState : { ...requestDetailsState, status: 'canceled' };

      return {
        ...state,
        requests: updatedRequests,
        requestDetails: updatedRequestDetails,
        cancelRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
          isModalOpen: !state.cancelRequest.isModalOpen,
        },
      };
    }
    case CANCEL_REQUEST_FAIL: {
      return {
        ...state,
        cancelRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
          isModalOpen: state.cancelRequest.isModalOpen,
        },
      };
    }
    case TOGGLE_ADMIN_CANCEL_REQUEST_MODAL: {
      return {
        ...state,
        cancelRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
          isModalOpen: !state.cancelRequest.isModalOpen,
        },
      };
    }
    case GET_REQUEST_DETAILS_BY_PRETTY_ID: {
      return {
        ...state,
        getRequestDetails: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS: {
      return {
        ...state,
        requestDetails,
        getRequestDetails: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL: {
      return {
        ...state,
        getRequestDetails: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_REQUEST_COMMENT.LOAD: {
      return {
        ...state,
        addComment: {
          commentInfo,
          addCommentState: ADD_REQUEST_COMMENT.LOAD,
          error: undefined,
        },
      };
    }
    case ADD_REQUEST_COMMENT.SUCCESS: {
      const foundIndex = findIndex(requestsState, requestState => requestState._id === requestId);
      const updatedRequests =
        foundIndex >= 0
          ? update(requestsState, { [foundIndex]: { comments: { $set: comments } } })
          : requestsState;
      const updatedRequestDetails =
        requestDetailsState && requestId === requestDetailsState._id
          ? { ...requestDetailsState, comments }
          : requestDetailsState;

      return {
        ...state,
        requests: updatedRequests,
        requestDetails: updatedRequestDetails,
        addComment: {
          commentInfo,
          addCommentState: ADD_REQUEST_COMMENT.SUCCESS,
          error: undefined,
        },
      };
    }
    case ADD_REQUEST_COMMENT.FAIL: {
      return {
        ...state,
        addComment: {
          commentInfo: undefined,
          addCommentState: ADD_REQUEST_COMMENT.FAIL,
          error: err,
        },
      };
    }
    case ADD_REQUEST_TICKET: {
      return {
        ...state,
        addTicket: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_REQUEST_TICKET_SUCCESS: {
      const updatedRequestDetails = { ...requestDetailsState, ticket };

      return {
        ...state,
        requestDetails: updatedRequestDetails,
        addTicket: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_REQUEST_TICKET_FAIL: {
      return {
        ...state,
        addTicket: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case EDIT_REQUEST_INVOICE: {
      return {
        ...state,
        editInvoice: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EDIT_REQUEST_INVOICE_SUCCESS: {
      const updatedRequestDetails = { ...requestDetailsState, invoice };

      return {
        ...state,
        requestDetails: updatedRequestDetails,
        editInvoice: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EDIT_REQUEST_INVOICE_FAIL: {
      return {
        ...state,
        editInvoice: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_REQUEST_TICKET_STATUS: {
      return {
        ...state,
        updateTicketStatus: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_REQUEST_TICKET_STATUS_SUCCESS: {
      const updatedRequestDetails = { ...requestDetailsState, ticket };

      return {
        ...state,
        requestDetails: updatedRequestDetails,
        updateTicketStatus: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_REQUEST_TICKET_STATUS_FAIL: {
      return {
        ...state,
        updateTicketStatus: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_REQUESTS_PAYMENTS: {
      return {
        ...state,
        applyNewFilter,
        getRequestsPayments: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_PAYMENTS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        requestsPayments:
          requestsPaymentsState && !applyNewFilter
            ? unionBy(requestsPaymentsState, requestsPayments, '_id')
            : requestsPayments,
        hasMoreRequestsPayments: requestsPayments.length === 20,
        getRequestsPayments: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_PAYMENTS_FAIL: {
      return {
        ...state,
        getRequestsPayments: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_REQUEST_FOLLOW_UPS_LOAD: {
      return {
        ...state,
        addRequestFollowUps: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_REQUEST_FOLLOW_UPS_SUCCESS: {
      const {
        requestId: _id,
        customerServiceFollowUps,
        operationsFollowUps,
        commentFollowUps,
      } = requestFollowUpsInfo;
      const foundIndex = findIndex(requestsState, requestState => requestState._id === _id);
      const updatedRequestByIndex =
        foundIndex !== undefined &&
        update(requestsState, {
          [foundIndex]: {
            customerServiceFollowUps: { $set: customerServiceFollowUps },
            operationsFollowUps: { $set: operationsFollowUps },
            commentFollowUps: { $set: commentFollowUps },
          },
        });
      const updatedRequests = foundIndex !== undefined ? updatedRequestByIndex : requestsState;

      return {
        ...state,
        addRequestFollowUps: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        requests: updatedRequests,
        requestFollowUpsModal: {
          isOpen: !followUpsModalIsOpenState,
        },
      };
    }
    case ADD_REQUEST_FOLLOW_UPS_FAIL: {
      return {
        ...state,
        addRequestFollowUps: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_REQUEST_FOLLOW_UPS_MODAL: {
      return {
        ...state,
        requestFollowUpsModal: {
          isOpen: !followUpsModalIsOpenState,
        },
      };
    }
    case CONNECT_TO_SOCKET_IO: {
      return {
        ...state,
        socketIoStatus: 'جاري الاتصال بالسرفر',
      };
    }
    case SOCKET_IO_SERVER_OFF: {
      return {
        ...state,
        socketIoStatus: 'غير قادر على الاتصال بالسرفر',
      };
    }
    case SOCKET_IO_SERVER_ON: {
      return {
        ...state,
        socketIoStatus: 'متصل بالسرفر',
      };
    }
    case NEW_REQUEST_CREATED: {
      let newRequestsState = requestsState;
      const cachedFilterState = JSON.parse(sessionStorage.getItem('requests0FilterState'));

      if (cachedFilterState) {
        const { filterQueries: cachedFilterQueries } = cachedFilterState;

        if (
          (cachedFilterQueries.statuses === undefined ||
            cachedFilterQueries.statuses === '' ||
            cachedFilterQueries.statuses.split(',').includes('pending')) &&
          (cachedFilterQueries.types === undefined ||
            cachedFilterQueries.types === '' ||
            cachedFilterQueries.types.split(',').includes(newRequest.field))
        ) {
          newRequestsState = unionBy([newRequest], requestsState);
        }
      }

      return {
        ...state,
        requests: newRequestsState,
      };
    }
    case REQUEST_UPDATED: {
      // TODO MAKE SURE NO DUPLICATION ON LOADING
      let updatedRequests =  requestsState;
      const foundIndex = findIndex(
        updatedRequests,
        requestState => requestState._id === updatedRequest._id,
      );

      const foundRequest = find(updatedRequests,
        requestState => requestState._id === updatedRequest._id);

      if (foundIndex !== -1 && (updatedRequest.status && foundRequest.status !== updatedRequest.status)) {
        updatedRequests = update(updatedRequests, { $splice: [[foundIndex, 1]] });
        updatedRequests = unionBy([update(foundRequest, { $merge: updatedRequest })], updatedRequests);
      } else if (foundIndex !== -1 && (!updatedRequest.status || foundRequest.status === updatedRequest.status)) {
        updatedRequests = update(updatedRequests, { [foundIndex]: { $merge: updatedRequest } });
      }

      if (foundIndex !== -1 && updatedRequest.customer && updatedRequest.customer.name &&  updatedRequest.customer.username) {
        updatedRequests = update(updatedRequests, {
          $apply: items =>
            items.map(item => {
              if (item.customer._id === updatedRequest.customer._id) {
                return update(item, {
                  $merge: { customer: updatedRequest.customer },
                });
              }
              return item;
            }),
        });
      }

      const cachedFilterState = JSON.parse(sessionStorage.getItem('requests0FilterState'));

      if (foundIndex === -1 && !cachedFilterState && updatedRequest.status) {
        updatedRequests = unionBy([updatedRequest], updatedRequests);
      }

      if (foundIndex === -1 && cachedFilterState && updatedRequest.status) {
        const {filterQueries: cachedFilterQueries} = cachedFilterState;

        if (
          (cachedFilterQueries.statuses === undefined ||
            cachedFilterQueries.statuses === '' ||
            cachedFilterQueries.statuses.split(',').includes(updatedRequest.status)) &&
          (cachedFilterQueries.types === undefined ||
            cachedFilterQueries.types === '' ||
            cachedFilterQueries.types.split(',').includes(updatedRequest.field))
        ) {
          updatedRequests = unionBy([updatedRequest], updatedRequests);
        }
      }

      return {
        ...state,
        requests: updatedRequests,
      };
    }
    case FINISH_REQUEST: {
      return {
        ...state,
        finishRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FINISH_REQUEST_SUCCESS: {
      return {
        ...state,
        finishRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FINISH_REQUEST_FAIL: {
      return {
        ...state,
        finishRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case FINISH_REQUEST_FOR_RECONDITIONING_FIELD: {
      return {
        ...state,
        finishRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS: {
      delete requestDetails.customer;
      delete requestDetails.worker;
      return {
        ...state,
        requestDetails: update(requestDetailsState, { $merge: requestDetails }),
        finishRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL: {
      return {
        ...state,
        requestDetails,
        finishRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ACTIVATE_REQUEST: {
      return {
        ...state,
        activateRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACTIVATE_REQUEST_SUCCESS: {
      return {
        ...state,
        requestDetails: update(requestDetailsState, {
          $merge: { status: 'active', worker: { username: '966566699544', name: 'هانى' } },
        }),
        // confirmModal: { isOpen: false },
        activateRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACTIVATE_REQUEST_FAIL: {
      return {
        ...state,
        activateRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_RECONDITIONING_REQUESTS_COUNT: {
      return {
        ...state,
        getReconditioningRequestsCount: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS: {
      return {
        ...state,
        reconditioningRequestsCount,
        getReconditioningRequestsCount: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_RECONDITIONING_REQUESTS_COUNT_FAIL: {
      return {
        ...state,
        getReconditioningRequestsCount: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ACTIVATE_CANCELLED_REQUEST: {
      return {
        ...state,
        activateRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACTIVATE_CANCELLED_REQUEST_SUCCESS: {
      const updatedRequests =
        index !== undefined
          ? update(requestsState, { [index]: { status: { $set: 'active' } } })
          : requestsState;
      const updatedRequestDetails =
        index !== undefined ? requestDetailsState : { ...requestDetailsState, status: 'active' };

      return {
        ...state,
        requests: updatedRequests,
        requestDetails: updatedRequestDetails,
        activateRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case ACTIVATE_CANCELLED_REQUEST_FAIL: {
      return {
        ...state,
        activateRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
        confirmModal: { isFail: { isError: true, message: err } },
      };
    }
    case CANCEL_ALL_PENDING_REQUESTS: {
      return {
        ...state,
        cancelAllPendingRequests: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CANCEL_ALL_PENDING_REQUESTS_SUCCESS: {
      // requestsState.forEach(function (request, index) {
      //
      // });
      // const updatedRequests = update(requestsState, {
      //   [index]: { status: { $set: 'cancelledByAdmin' } },
      // });
      return {
        ...state,
        // requests: updatedRequests,
        cancelAllPendingRequests: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
      };
    }
    case CANCEL_ALL_PENDING_REQUESTS_FAIL: {
      return {
        ...state,
        cancelAllPendingRequests: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
        confirmModal: { isFail: { isError: true, message: err } },
      };
    }

    case POSTPONE_REQUEST: {
      return {
        ...state,
        postponeRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case POSTPONE_REQUEST_SUCCESS: {
      return {
        ...state,
        requestDetails: update(requestDetailsState, {
          $merge: { status: 'postponed' },
        }),
        postponeRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case POSTPONE_REQUEST_FAIL: {
      return {
        ...state,
        postponeRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }


    case ACTIVATE_POSTPONED_REQUEST: {
      return {
        ...state,
        activatePostponedRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACTIVATE_POSTPONED_REQUEST_SUCCESS: {
      return {
        ...state,
        requestDetails: update(requestDetailsState, {
          $merge: { status: 'active' },
        }),
        activatePostponedRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACTIVATE_POSTPONED_REQUEST_FAIL: {
      return {
        ...state,
        activatePostponedRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case CHOOSE_WORKER: {
      return {
        ...state,
        chooseWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CHOOSE_WORKER_SUCCESS: {
      return {
        ...state,
        chooseWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CHOOSE_WORKER_FAIL: {
      return {
        ...state,
        chooseWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case ACCEPT_WORKER: {
      return {
        ...state,
        acceptWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACCEPT_WORKER_SUCCESS: {
      console.log('requestDetails', requestDetails)
      return {
        ...state,
        requestDetails: requestDetails,
        acceptWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ACCEPT_WORKER_FAIL: {
      return {
        ...state,
        acceptWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    default:
      return state;
  }
};
