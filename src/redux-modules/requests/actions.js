export const REQUESTS = {
  GET_ALL_REQUESTS: 'GET_ALL_REQUESTS',
  GET_ALL_REQUESTS_ALL: 'GET_ALL_REQUESTS_ALL',
  GET_ALL_REQUESTS_SUCCESS: 'GET_ALL_REQUESTS_SUCCESS',
  GET_ALL_REQUESTS_FAIL: 'GET_ALL_REQUESTS_FAIL',
  CANCEL_REQUEST: 'CANCEL_REQUEST',
  CANCEL_REQUEST_SUCCESS: 'CANCEL_REQUEST_SUCCESS',
  CANCEL_REQUEST_FAIL: 'CANCEL_REQUEST_FAIL',
  GET_REQUEST_DETAILS_BY_PRETTY_ID: 'GET_REQUEST_DETAILS_BY_PRETTY_ID',
  GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS: 'GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS',
  GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL: 'GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL',
  ADD_REQUEST_COMMENT: {
    LOAD: 'REQUESTS_ADD_REQUEST_COMMENT_LOAD',
    SUCCESS: 'REQUESTS_ADD_REQUEST_COMMENT_SUCCESS',
    FAIL: 'REQUESTS_ADD_REQUEST_COMMENT_FAIL',
  },
  ADD_REQUEST_TICKET: 'ADD_REQUEST_TICKET',
  ADD_REQUEST_TICKET_SUCCESS: 'ADD_REQUEST_TICKET_SUCCESS',
  ADD_REQUEST_TICKET_FAIL: 'ADD_REQUEST_TICKET_FAIL',
  EDIT_REQUEST_INVOICE: 'EDIT_REQUEST_INVOICE',
  EDIT_REQUEST_INVOICE_SUCCESS: 'EDIT_REQUEST_INVOICE_SUCCESS',
  EDIT_REQUEST_INVOICE_FAIL: 'EDIT_REQUEST_INVOICE_FAIL',
  UPDATE_REQUEST_TICKET_STATUS: 'UPDATE_REQUEST_TICKET_STATUS',
  UPDATE_REQUEST_TICKET_STATUS_SUCCESS: 'UPDATE_REQUEST_TICKET_STATUS_SUCCESS',
  UPDATE_REQUEST_TICKET_STATUS_FAIL: 'UPDATE_REQUEST_TICKET_STATUS_FAIL',
  GET_REQUESTS_PAYMENTS: 'GET_REQUESTS_PAYMENTS',
  GET_REQUESTS_PAYMENTS_SUCCESS: 'GET_REQUESTS_PAYMENTS_SUCCESS',
  GET_REQUESTS_PAYMENTS_FAIL: 'GET_REQUESTS_PAYMENTS_FAIL',
  ADD_REQUEST_FOLLOW_UPS_LOAD: 'ADD_REQUEST_FOLLOW_UPS_LOAD',
  ADD_REQUEST_FOLLOW_UPS_SUCCESS: 'ADD_REQUEST_FOLLOW_UPS_SUCCESS',
  ADD_REQUEST_FOLLOW_UPS_FAIL: 'ADD_REQUEST_FOLLOW_UPS_FAIL',
  TOGGLE_REQUEST_FOLLOW_UPS_MODAL: 'TOGGLE_REQUEST_FOLLOW_UPS_MODAL',
  CONNECT_TO_SOCKET_IO: 'CONNECT_TO_SOCKET_IO',
  NEW_REQUEST_CREATED: 'NEW_REQUEST_CREATED',
  SOCKET_IO_SERVER_OFF: 'SOCKET_IO_SERVER_OFF',
  SOCKET_IO_SERVER_ON: 'SOCKET_IO_SERVER_ON',
  REQUEST_UPDATED: 'REQUEST_UPDATED',
  FINISH_REQUEST: 'FINISH_REQUEST',
  FINISH_REQUEST_SUCCESS: 'FINISH_REQUEST_SUCCESS',
  FINISH_REQUEST_FAIL: 'FINISH_REQUEST_FAIL',
  ACTIVATE_REQUEST: 'ACTIVATE_REQUEST',
  ACTIVATE_REQUEST_SUCCESS: 'ACTIVATE_REQUEST_SUCCESS',
  ACTIVATE_REQUEST_FAIL: 'ACTIVATE_REQUEST_FAIL',
  FINISH_REQUEST_FOR_RECONDITIONING_FIELD: 'FINISH_REQUEST_FOR_RECONDITIONING_FIELD',
  FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS:
    'FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS',
  FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL: 'FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL',
  GET_RECONDITIONING_REQUESTS_COUNT: 'GET_RECONDITIONING_REQUESTS_COUNT',
  GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS: 'GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS',
  GET_RECONDITIONING_REQUESTS_COUNT_FAIL: 'GET_RECONDITIONING_REQUESTS_COUNT_FAIL',
  ACTIVATE_CANCELLED_REQUEST: 'ACTIVATE_CANCELLED_REQUEST',
  ACTIVATE_CANCELLED_REQUEST_SUCCESS: 'ACTIVATE_CANCELLED_REQUEST_SUCCESS',
  ACTIVATE_CANCELLED_REQUEST_FAIL: 'ACTIVATE_CANCELLED_REQUEST_FAIL',
  CANCEL_ALL_PENDING_REQUESTS: 'CANCEL_ALL_PENDING_REQUESTS',
  CANCEL_ALL_PENDING_REQUESTS_SUCCESS: 'CANCEL_ALL_PENDING_REQUESTS_SUCCESS',
  CANCEL_ALL_PENDING_REQUESTS_FAIL: 'CANCEL_ALL_PENDING_REQUESTS_FAIL',
  TOGGLE_ADMIN_CANCEL_REQUEST_MODAL: 'TOGGLE_ADMIN_CANCEL_REQUEST_MODAL',
  TOGGLE_ADMIN_POSTPONE_REQUEST_MODAL: 'TOGGLE_ADMIN_POSTPONE_REQUEST_MODAL',
  POSTPONE_REQUEST: 'POSTPONE_REQUEST',
  POSTPONE_REQUEST_SUCCESS: 'POSTPONE_REQUEST_SUCCESS',
  POSTPONE_REQUEST_FAIL: 'POSTPONE_REQUEST_FAIL',

  ACTIVATE_POSTPONED_REQUEST: 'ACTIVATE_POSTPONED_REQUEST',
  ACTIVATE_POSTPONED_REQUEST_SUCCESS: 'ACTIVATE_POSTPONED_REQUEST_SUCCESS',
  ACTIVATE_POSTPONED_REQUEST_FAIL: 'ACTIVATE_POSTPONED_REQUEST_FAIL',

  CHOOSE_WORKER: 'CHOOSE_WORKER',
  CHOOSE_WORKER_SUCCESS: 'CHOOSE_WORKER_SUCCESS',
  CHOOSE_WORKER_FAIL: 'CHOOSE_WORKER_FAIL',

  ACCEPT_WORKER: 'ACCEPT_WORKER',
  ACCEPT_WORKER_SUCCESS: 'ACCEPT_WORKER_SUCCESS',
  ACCEPT_WORKER_FAIL: 'ACCEPT_WORKER_FAIL',
};

export const getAllRequests = (options, applyNewFilter: false) => ({
  type: REQUESTS.GET_ALL_REQUESTS,
  options,
  applyNewFilter,
});

export const getAllRequestsAll = (options, applyNewFilter: false) => ({
  type: REQUESTS.GET_ALL_REQUESTS_ALL,
  options,
  applyNewFilter,
});

export const getAllRequestsSuccess = (requests, applyNewFilter: false) => ({
  type: REQUESTS.GET_ALL_REQUESTS_SUCCESS,
  requests,
  applyNewFilter,
});

export const getAllRequestsFail = err => ({
  type: REQUESTS.GET_ALL_REQUESTS_FAIL,
  err,
});

export const cancelRequest = (requestId, requestInfo) => ({
  type: REQUESTS.CANCEL_REQUEST,
  requestId,
  requestInfo,
});

export const cancelRequestSuccess = index => ({
  type: REQUESTS.CANCEL_REQUEST_SUCCESS,
  index,
});

export const cancelRequestFail = err => ({
  type: REQUESTS.CANCEL_REQUEST_FAIL,
  err,
});

export const getRequestDetailsByPrettyId = requestPrettyId => ({
  type: REQUESTS.GET_REQUEST_DETAILS_BY_PRETTY_ID,
  requestPrettyId,
});

export const getRequestDetailsByPrettyIdSuccess = requestDetails => ({
  type: REQUESTS.GET_REQUEST_DETAILS_BY_PRETTY_ID_SUCCESS,
  requestDetails,
});

export const getRequestDetailsByPrettyIdFail = err => ({
  type: REQUESTS.GET_REQUEST_DETAILS_BY_PRETTY_ID_FAIL,
  err,
});

export const addComment = (commentInfo, requestId) => ({
  type: REQUESTS.ADD_REQUEST_COMMENT.LOAD,
  commentInfo,
  requestId,
});

export const addCommentSuccess = (comments, requestId) => ({
  type: REQUESTS.ADD_REQUEST_COMMENT.SUCCESS,
  comments,
  requestId,
});

export const addCommentFail = err => ({
  type: REQUESTS.ADD_REQUEST_COMMENT.FAIL,
  err,
});

export const addTicket = ticketInfo => ({
  type: REQUESTS.ADD_REQUEST_TICKET,
  ticketInfo,
});

export const addTicketSuccess = ticket => ({
  type: REQUESTS.ADD_REQUEST_TICKET_SUCCESS,
  ticket,
});

export const addTicketFail = err => ({
  type: REQUESTS.ADD_REQUEST_TICKET_FAIL,
  err,
});

export const editInvoice = invoice => ({
  type: REQUESTS.EDIT_REQUEST_INVOICE,
  invoice,
});

export const editInvoiceSuccess = invoice => ({
  type: REQUESTS.EDIT_REQUEST_INVOICE_SUCCESS,
  invoice,
});

export const editInvoiceFail = err => ({
  type: REQUESTS.EDIT_REQUEST_INVOICE_FAIL,
  err,
});

export const updateTicketStatus = ticketInfo => ({
  type: REQUESTS.UPDATE_REQUEST_TICKET_STATUS,
  ticketInfo,
});

export const updateTicketStatusSuccess = ticket => ({
  type: REQUESTS.UPDATE_REQUEST_TICKET_STATUS_SUCCESS,
  ticket,
});

export const updateTicketStatusFail = err => ({
  type: REQUESTS.UPDATE_REQUEST_TICKET_STATUS_FAIL,
  err,
});

export const getRequestsPayments = (options, applyNewFilter: false) => ({
  type: REQUESTS.GET_REQUESTS_PAYMENTS,
  options,
  applyNewFilter,
});

export const getRequestsPaymentsSuccess = (requestsPayments, applyNewFilter: false) => ({
  type: REQUESTS.GET_REQUESTS_PAYMENTS_SUCCESS,
  requestsPayments,
  applyNewFilter,
});

export const getRequestsPaymentsFail = err => ({
  type: REQUESTS.GET_REQUESTS_PAYMENTS_FAIL,
  err,
});

export const addRequestFollowUps = requestFollowUpsInfo => ({
  type: REQUESTS.ADD_REQUEST_FOLLOW_UPS_LOAD,
  requestFollowUpsInfo,
});

export const addRequestFollowUpsSuccess = requestFollowUpsInfo => ({
  type: REQUESTS.ADD_REQUEST_FOLLOW_UPS_SUCCESS,
  requestFollowUpsInfo,
});

export const addRequestFollowUpsFail = err => ({
  type: REQUESTS.ADD_REQUEST_FOLLOW_UPS_FAIL,
  err,
});

export const toggleRequestFollowUpsModal = () => ({
  type: REQUESTS.TOGGLE_REQUEST_FOLLOW_UPS_MODAL,
});

export const connectToSocketIo = username => ({
  type: REQUESTS.CONNECT_TO_SOCKET_IO,
  username,
});

export const socketIoDidnotConnectToServer = () => ({
  type: REQUESTS.SOCKET_IO_SERVER_OFF,
});

export const socketIoConnectedToServer = () => ({
  type: REQUESTS.SOCKET_IO_SERVER_ON,
});

export const newRequestCreated = newRequest => ({
  type: REQUESTS.NEW_REQUEST_CREATED,
  newRequest,
});

export const updateRequest = updatedRequest => ({
  type: REQUESTS.REQUEST_UPDATED,
  updatedRequest,
});

export const finishRequest = info => ({
  type: REQUESTS.FINISH_REQUEST,
  info,
});

export const finishRequestSuccess = requestDetails => ({
  type: REQUESTS.FINISH_REQUEST_SUCCESS,
  requestDetails,
});

export const finishRequestFail = err => ({
  type: REQUESTS.FINISH_REQUEST_FAIL,
  err,
});

export const activateRequest = info => ({
  type: REQUESTS.ACTIVATE_REQUEST,
  info,
});

export const activateRequestSuccess = requestDetails => ({
  type: REQUESTS.ACTIVATE_REQUEST_SUCCESS,
  requestDetails,
});

export const activateRequestFail = err => ({
  type: REQUESTS.ACTIVATE_REQUEST_FAIL,
  err,
});

export const finishRequestForReconditioningField = info => ({
  type: REQUESTS.FINISH_REQUEST_FOR_RECONDITIONING_FIELD,
  info,
});

export const finishRequestForReconditioningFieldSuccess = requestDetails => ({
  type: REQUESTS.FINISH_REQUEST_FOR_RECONDITIONING_FIELD_SUCCESS,
  requestDetails,
});

export const finishRequestForReconditioningFieldFail = err => ({
  type: REQUESTS.FINISH_REQUEST_FOR_RECONDITIONING_FIELD_FAIL,
  err,
});

export const getReconditioningRequestsCount = () => ({
  type: REQUESTS.GET_RECONDITIONING_REQUESTS_COUNT,
});

export const getReconditioningRequestsCountSuccess = reconditioningRequestsCount => ({
  type: REQUESTS.GET_RECONDITIONING_REQUESTS_COUNT_SUCCESS,
  reconditioningRequestsCount,
});

export const getReconditioningRequestsCountFail = err => ({
  type: REQUESTS.GET_RECONDITIONING_REQUESTS_COUNT_FAIL,
  err,
});

export const activateCancelledRequest = (requestId, index) => ({
  type: REQUESTS.ACTIVATE_CANCELLED_REQUEST,
  requestId,
  index,
});

export const activateCancelledRequestSuccess = index => ({
  type: REQUESTS.ACTIVATE_CANCELLED_REQUEST_SUCCESS,
  index,
});

export const activateCancelledRequestFail = err => ({
  type: REQUESTS.ACTIVATE_CANCELLED_REQUEST_FAIL,
  err,
});

export const cancelAllPendingRequests = () => ({
  type: REQUESTS.CANCEL_ALL_PENDING_REQUESTS,
});

export const cancelAllPendingRequestsSuccess = () => ({
  type: REQUESTS.CANCEL_ALL_PENDING_REQUESTS_SUCCESS,
});

export const cancelAllPendingRequestsFail = err => ({
  type: REQUESTS.CANCEL_ALL_PENDING_REQUESTS_FAIL,
  err,
});

export const toggleAdminCancelRequestModal = index => ({
  type: REQUESTS.TOGGLE_ADMIN_CANCEL_REQUEST_MODAL,
  index,
});

export const toggleAdminPostponeRequestModal = index => ({
  type: REQUESTS.TOGGLE_ADMIN_POSTPONE_REQUEST_MODAL,
  index,
});

export const postponeRequest = requestId => ({
  type: REQUESTS.POSTPONE_REQUEST,
  requestId,
});

export const postponeRequestSuccess = index => ({
  type: REQUESTS.POSTPONE_REQUEST_SUCCESS,
  index,
});

export const postponeRequestFail = err => ({
  type: REQUESTS.POSTPONE_REQUEST_FAIL,
  err,
});

export const chooseWorker = (requestId, workerId) => ({
  type: REQUESTS.CHOOSE_WORKER,
  requestId,
  workerId,
});

export const chooseWorkerSuccess = index => ({
  type: REQUESTS.CHOOSE_WORKER_SUCCESS,
  index,
});

export const chooseWorkerFail = err => ({
  type: REQUESTS.CHOOSE_WORKER_FAIL,
  err,
});

export const acceptWorker = (requestId, workerId) => ({
  type: REQUESTS.ACCEPT_WORKER,
  requestId,
  workerId,
});

export const acceptWorkerSuccess = index => ({
  type: REQUESTS.ACCEPT_WORKER_SUCCESS,
  index,
});

export const acceptWorkerFail = err => ({
  type: REQUESTS.ACCEPT_WORKER_FAIL,
  err,
});

export const activatePostponedRequest = requestId => ({
  type: REQUESTS.ACTIVATE_POSTPONED_REQUEST,
  requestId,
});

export const activatePostponedRequestSuccess = requestDetails => ({
  type: REQUESTS.ACTIVATE_POSTPONED_REQUEST_SUCCESS,
  requestDetails,
});

export const activatePostponedRequestFail = err => ({
  type: REQUESTS.ACTIVATE_POSTPONED_REQUEST_FAIL,
  err,
});
