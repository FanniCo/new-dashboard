export const COMMISSIONS = {
  TOGGLE_ADD_NEW_COMMISSION_MODAL: 'TOGGLE_ADD_NEW_COMMISSION_MODAL',
  GET_ALL_COMMISSIONS: 'GET_ALL_COMMISSIONS',
  GET_ALL_COMMISSIONS_SUCCESS: 'GET_ALL_COMMISSIONS_SUCCESS',
  GET_ALL_COMMISSIONS_FAIL: 'GET_ALL_COMMISSIONS_FAIL',
  ADD_NEW_COMMISSION: 'ADD_NEW_COMMISSION',
  ADD_NEW_COMMISSION_SUCCESS: 'ADD_NEW_COMMISSION_SUCCESS',
  ADD_NEW_COMMISSION_FAIL: 'ADD_NEW_COMMISSION_FAIL',
  DELETE_COMMISSION: 'DELETE_COMMISSION',
  DELETE_COMMISSION_SUCCESS: 'DELETE_COMMISSION_SUCCESS',
  DELETE_COMMISSION_FAIL: 'DELETE_COMMISSION_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_COMMISSION_MODAL: 'OPEN_EDIT_COMMISSION_MODAL',
  UPDATE_COMMISSION: 'UPDATE_COMMISSION',
  UPDATE_COMMISSION_SUCCESS: 'UPDATE_COMMISSION_SUCCESS',
  UPDATE_COMMISSION_FAIL: 'UPDATE_COMMISSION_FAIL',
};

export const toggleAddNewCommissionModal = () => ({
  type: COMMISSIONS.TOGGLE_ADD_NEW_COMMISSION_MODAL,
});

export const getAllCommissions = (options, applyNewFilter = false) => ({
  type: COMMISSIONS.GET_ALL_COMMISSIONS,
  options,
  applyNewFilter,
});

export const getAllCommissionsSuccess = (commissionsList, applyNewFilter: false) => ({
  type: COMMISSIONS.GET_ALL_COMMISSIONS_SUCCESS,
  commissionsList,
  applyNewFilter,
});

export const getAllCommissionsFail = err => ({
  type: COMMISSIONS.GET_ALL_COMMISSIONS_FAIL,
  err,
});

export const addNewCommission = newCommission => ({
  type: COMMISSIONS.ADD_NEW_COMMISSION,
  newCommission,
});

export const addNewCommissionSuccess = newCommissionInfo => ({
  type: COMMISSIONS.ADD_NEW_COMMISSION_SUCCESS,
  newCommissionInfo,
});

export const addNewCommissionFail = err => ({
  type: COMMISSIONS.ADD_NEW_COMMISSION_FAIL,
  err,
});

export const deleteCommission = (commissionId, index) => ({
  type: COMMISSIONS.DELETE_COMMISSION,
  commissionId,
  index,
});

export const deleteCommissionSuccess = index => ({
  type: COMMISSIONS.DELETE_COMMISSION_SUCCESS,
  index,
});

export const deleteCommissionFail = err => ({
  type: COMMISSIONS.DELETE_COMMISSION_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: COMMISSIONS.OPEN_CONFIRM_MODAL,
});

export const openEditCommissionModal = () => ({
  type: COMMISSIONS.OPEN_EDIT_COMMISSION_MODAL,
});

export const updateCommission = (newCommission, commissionId, index) => ({
  type: COMMISSIONS.UPDATE_COMMISSION,
  newCommission,
  commissionId,
  index,
});

export const updateCommissionSuccess = (newCommissionInfo, index) => ({
  type: COMMISSIONS.UPDATE_COMMISSION_SUCCESS,
  newCommissionInfo,
  index,
});

export const updateCommissionFail = err => ({
  type: COMMISSIONS.UPDATE_COMMISSION_FAIL,
  err,
});
