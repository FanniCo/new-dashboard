import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { COMMISSIONS } from './actions';

const {
  GET_ALL_COMMISSIONS,
  GET_ALL_COMMISSIONS_SUCCESS,
  GET_ALL_COMMISSIONS_FAIL,
  ADD_NEW_COMMISSION,
  ADD_NEW_COMMISSION_SUCCESS,
  ADD_NEW_COMMISSION_FAIL,
  DELETE_COMMISSION,
  DELETE_COMMISSION_SUCCESS,
  DELETE_COMMISSION_FAIL,
  UPDATE_COMMISSION,
  UPDATE_COMMISSION_SUCCESS,
  UPDATE_COMMISSION_FAIL,
} = COMMISSIONS;

function* getAllCommissions(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { commissions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = commissions.getAllCommissions;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: commissionsList } = response;

    yield put({
      type: GET_ALL_COMMISSIONS_SUCCESS,
      commissions: commissionsList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_COMMISSIONS_FAIL,
      err: message,
    });
  }
}

function* addNewCommission(payload) {
  const { newCommission } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { commissions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newCommission;

  const url = commissions.addNewCommission;
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newCommissionInfo } = response;

    yield put({
      type: ADD_NEW_COMMISSION_SUCCESS,
      newCommissionInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_COMMISSION_FAIL,
      err: message,
    });
  }
}

function* deleteCommission(payload) {
  const { commissionId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { commissions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = commissions.delete;
  const response = yield api.delete(`${url}/${commissionId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_COMMISSION_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_COMMISSION_FAIL,
      err: message,
    });
  }
}

function* updateCommission(payload) {
  const { newCommission, commissionId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { commissions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newCommission;

  const url = `${commissions.update}/${commissionId}`;
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newCommissionInfo } = response;
    yield put({
      type: UPDATE_COMMISSION_SUCCESS,
      newCommissionInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_COMMISSION_FAIL,
      err: message,
    });
  }
}

function* commissionsSaga() {
  yield takeEvery(GET_ALL_COMMISSIONS, getAllCommissions);
  yield takeEvery(ADD_NEW_COMMISSION, addNewCommission);
  yield takeEvery(DELETE_COMMISSION, deleteCommission);
  yield takeEvery(UPDATE_COMMISSION, updateCommission);
}

export default commissionsSaga;
