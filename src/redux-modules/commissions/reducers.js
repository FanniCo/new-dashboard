import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { COMMISSIONS } from './actions';

const initialState = {
  applyNewFilter: false,
  commissions: undefined,
  hasMoreCommissions: true,
  addNewCommissionModal: {
    isOpen: false,
  },
  getAllCommissions: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addCommission: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteCommission: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editCommissionModal: { isOpen: false },
  updateCommission: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_COMMISSION_MODAL,
    GET_ALL_COMMISSIONS,
    GET_ALL_COMMISSIONS_SUCCESS,
    GET_ALL_COMMISSIONS_FAIL,
    ADD_NEW_COMMISSION,
    ADD_NEW_COMMISSION_SUCCESS,
    ADD_NEW_COMMISSION_FAIL,
    DELETE_COMMISSION,
    DELETE_COMMISSION_SUCCESS,
    DELETE_COMMISSION_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_COMMISSION_MODAL,
    UPDATE_COMMISSION,
    UPDATE_COMMISSION_SUCCESS,
    UPDATE_COMMISSION_FAIL,
  } = COMMISSIONS;
  const { commissions, applyNewFilter, newCommissionInfo, index, err } = payload;
  const {
    commissions: commissionsState,
    addNewCommissionModal: { isOpen: addNewCommissionModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editCommissionModal: { isOpen: editCommissionModalOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_COMMISSION_MODAL: {
      return {
        ...state,
        addNewCommissionModal: {
          isOpen: !addNewCommissionModalIsOpenState,
        },
        addCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updateCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_COMMISSIONS: {
      return {
        ...state,
        applyNewFilter,
        getAllCommissions: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_COMMISSIONS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        commissions:
          commissionsState && !applyNewFilter
            ? unionBy(commissionsState, commissions, '_id')
            : commissions,
        hasMoreCommissions: commissions.length === 20,
        getAllCommissions: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, Commission: '' },
        },
      };
    }
    case GET_ALL_COMMISSIONS_FAIL: {
      return {
        ...state,
        getAllCommissions: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_COMMISSION: {
      return {
        ...state,
        addCommission: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_COMMISSION_SUCCESS: {
      commissionsState.unshift(newCommissionInfo);
      return {
        ...state,
        commissions: commissionsState,
        addCommission: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewCommissionModal: {
          isOpen: !addNewCommissionModalIsOpenState,
        },
      };
    }
    case ADD_NEW_COMMISSION_FAIL: {
      return {
        ...state,
        addCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_COMMISSION_MODAL: {
      return {
        ...state,
        editCommissionModal: { isOpen: !editCommissionModalOpenState },
        addCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updateCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_COMMISSION: {
      return {
        ...state,
        deleteCommission: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_COMMISSION_SUCCESS: {
      if (index !== undefined) commissionsState.splice(index, 1);
      return {
        ...state,
        commissions: commissionsState,
        deleteCommission: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_COMMISSION_FAIL: {
      return {
        ...state,
        deleteCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_COMMISSION: {
      return {
        ...state,
        updateCommission: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_COMMISSION_SUCCESS: {
      const updatedCommissions = update(commissionsState, {
        [index]: { $set: { ...newCommissionInfo } },
      });
      return {
        ...state,
        commissions: updatedCommissions,
        updateCommission: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editCommissionModal: {
          isOpen: !editCommissionModalOpenState,
        },
      };
    }
    case UPDATE_COMMISSION_FAIL: {
      return {
        ...state,
        updateCommission: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
