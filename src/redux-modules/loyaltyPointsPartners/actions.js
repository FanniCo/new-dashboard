export const LOYALTY_POINTS_PARTNERS = {
  GET_ALL_LOYALTY_POINTS_PARTNERS: 'GET_ALL_LOYALTY_POINTS_PARTNERS',
  GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS: 'GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS',
  GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL: 'GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL',
};

export const getAllLoyaltyPointsPartners = (options, applyNewFilter = false) => ({
  type: LOYALTY_POINTS_PARTNERS.GET_ALL_LOYALTY_POINTS_PARTNERS,
  options,
  applyNewFilter,
});

export const getAllLoyaltyPointsPartnersSuccess = (messagesList, applyNewFilter: false) => ({
  type: LOYALTY_POINTS_PARTNERS.GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS,
  messagesList,
  applyNewFilter,
});

export const getAllLoyaltyPointsPartnersFail = err => ({
  type: LOYALTY_POINTS_PARTNERS.GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL,
  err,
});
