import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { LOYALTY_POINTS_PARTNERS } from './actions';

const {
  GET_ALL_LOYALTY_POINTS_PARTNERS,
  GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS,
  GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL,
} = LOYALTY_POINTS_PARTNERS;

function* getAllLoyaltyPointsPartners(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getAllLoyaltyPointsPartners;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: loyaltyPointsPartnersList } = response;

    yield put({
      type: GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS,
      loyaltyPointsPartners: loyaltyPointsPartnersList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL,
      err: message,
    });
  }
}

function* loyaltyPointsPartnersSaga() {
  yield takeEvery(GET_ALL_LOYALTY_POINTS_PARTNERS, getAllLoyaltyPointsPartners);
}

export default loyaltyPointsPartnersSaga;
