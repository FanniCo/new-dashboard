import { unionBy } from 'lodash';
import { LOYALTY_POINTS_PARTNERS } from './actions';

const initialState = {
  applyNewFilter: false,
  loyaltyPointsPartners: undefined,
  hasMoreLoyaltyPointsPartners: true,
  addNewMessageModal: {
    isOpen: false,
  },
  getAllLoyaltyPointsPartners: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_LOYALTY_POINTS_PARTNERS,
    GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS,
    GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL,
  } = LOYALTY_POINTS_PARTNERS;
  const { loyaltyPointsPartners, applyNewFilter, err } = payload;
  const {
    loyaltyPointsPartners: loyaltyPointsPartnersState,
  } = state;

  switch (type) {
    case GET_ALL_LOYALTY_POINTS_PARTNERS: {
      return {
        ...state,
        applyNewFilter,
        getAllLoyaltyPointsPartners: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_LOYALTY_POINTS_PARTNERS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        loyaltyPointsPartners:
          loyaltyPointsPartnersState && !applyNewFilter ? unionBy(loyaltyPointsPartnersState, loyaltyPointsPartners, '_id') : loyaltyPointsPartners,
        hasMoreLoyaltyPointsPartners: loyaltyPointsPartners.length === 20,
        getAllLoyaltyPointsPartners: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_LOYALTY_POINTS_PARTNERS_FAIL: {
      return {
        ...state,
        getAllLoyaltyPointsPartners: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
