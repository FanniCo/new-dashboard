import { all, fork } from 'redux-saga/effects';
import staticsSaga from './statics/sagas';
import configsSaga from './configs/sagas';
import dashboardSaga from './dashboard/sagas';
import userSaga from './user/sagas';
import requestsSaga from './requests/sagas';
import ticketsSaga from './tickets/sagas';
import clientsSaga from './clients/sagas';
import workersSaga from './workers/sagas';
import transactionsSaga from './transactions/sagas';
import reviewsSaga from './reviews/sagas';
import promoCodesSaga from './promoCodes/sagas';
import notificationsSaga from './notifications/sagas';
import messagesSaga from './messages/sagas';
import activitiesLogsSaga from './activitiesLogs/sagas';
import vendorsSaga from './vendors/sagas';
import workersAdsSaga from './workersAds/sagas';
import customersAdsSaga from './customersAds/sagas';
import offersSaga from './offers/sagas';
import postsSaga from './posts/sagas';
import commissionsSaga from './commissions/sagas';
import adminActivitiesSaga from './adminsActivities/sagas';
import vouchersSaga from './vouchers/sagas';
import appConfigs from './appConfigs/sagas';
import loyaltyPointsPartnersSage from './loyaltyPointsPartners/sagas';
import rechargeAwardsSage from './rechargeAwards/sagas';

const sagas = [
  staticsSaga,
  configsSaga,
  dashboardSaga,
  userSaga,
  requestsSaga,
  ticketsSaga,
  clientsSaga,
  workersSaga,
  transactionsSaga,
  reviewsSaga,
  promoCodesSaga,
  notificationsSaga,
  messagesSaga,
  activitiesLogsSaga,
  vendorsSaga,
  workersAdsSaga,
  customersAdsSaga,
  offersSaga,
  postsSaga,
  commissionsSaga,
  adminActivitiesSaga,
  vouchersSaga,
  appConfigs,
  loyaltyPointsPartnersSage,
  rechargeAwardsSage,
];

export default function* rootSaga() {
  yield all(sagas.map(saga => fork(saga)));
}
