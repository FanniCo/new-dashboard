import { concat } from 'lodash';
import update from 'immutability-helper';
import { REVIEWS } from './actions';

const initialState = {
  reviews: undefined,
  hasMoreReviews: true,
  applyNewFilter: false,
  getAllReviews: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  toggleShowingReview: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteReview: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_REVIEWS,
    GET_ALL_REVIEWS_SUCCESS,
    GET_ALL_REVIEWS_FAIL,
    TOGGLE_SHOWING_REVIEW,
    TOGGLE_SHOWING_REVIEW_SUCCESS,
    TOGGLE_SHOWING_REVIEW_FAIL,
    DELETE_REVIEW,
    DELETE_REVIEW_SUCCESS,
    DELETE_REVIEW_FAIL,
  } = REVIEWS;
  const { reviews, applyNewFilter, index, isOpen, isHidden, err } = payload;
  const { reviews: reviewsState } = state;

  switch (type) {
    case GET_ALL_REVIEWS: {
      return {
        ...state,
        getAllReviews: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_REVIEWS_SUCCESS: {
      return {
        ...state,
        reviews: reviewsState && !applyNewFilter ? concat(reviewsState, reviews) : reviews,
        hasMoreReviews: reviews.length === 20,
        getAllReviews: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_REVIEWS_FAIL: {
      return {
        ...state,
        getAllReviews: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_SHOWING_REVIEW: {
      return {
        ...state,
        toggleShowingReview: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case TOGGLE_SHOWING_REVIEW_SUCCESS: {
      const updatedReviews = update(reviewsState, {
        [index]: { review: { isHidden: { $set: isHidden } } },
      });

      return {
        ...state,
        reviews: updatedReviews,
        toggleShowingReview: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case TOGGLE_SHOWING_REVIEW_FAIL: {
      return {
        ...state,
        toggleShowingReview: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case DELETE_REVIEW: {
      return {
        ...state,
        deleteReview: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_REVIEW_SUCCESS: {
      reviewsState.splice(index, 1);

      return {
        ...state,
        reviews: reviewsState,
        deleteReview: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case DELETE_REVIEW_FAIL: {
      return {
        ...state,
        deleteReview: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
