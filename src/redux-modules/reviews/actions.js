export const REVIEWS = {
  GET_ALL_REVIEWS: 'GET_ALL_REVIEWS',
  GET_ALL_REVIEWS_SUCCESS: 'GET_ALL_REVIEWS_SUCCESS',
  GET_ALL_REVIEWS_FAIL: 'GET_ALL_REVIEWS_FAIL',
  TOGGLE_SHOWING_REVIEW: 'TOGGLE_SHOWING_REVIEW',
  TOGGLE_SHOWING_REVIEW_SUCCESS: 'TOGGLE_SHOWING_REVIEW_SUCCESS',
  TOGGLE_SHOWING_REVIEW_FAIL: 'TOGGLE_SHOWING_REVIEW_FAIL',
  DELETE_REVIEW: 'DELETE_REVIEW',
  DELETE_REVIEW_SUCCESS: 'DELETE_REVIEW_SUCCESS',
  DELETE_REVIEW_FAIL: 'DELETE_REVIEW_FAIL',
};

export const getAllReviews = (options, applyNewFilter: false) => ({
  type: REVIEWS.GET_ALL_REVIEWS,
  options,
  applyNewFilter,
});

export const getAllReviewsSuccess = (reviews, applyNewFilter: false) => ({
  type: REVIEWS.GET_ALL_REVIEWS_SUCCESS,
  reviews,
  applyNewFilter,
});

export const getAllReviewsFail = err => ({
  type: REVIEWS.GET_ALL_REVIEWS_FAIL,
  err,
});

export const toggleShowingReview = (reviewId, workerId, index, isHidden = false) => ({
  type: REVIEWS.TOGGLE_SHOWING_REVIEW,
  reviewId,
  workerId,
  index,
  isHidden,
});

export const toggleShowingReviewSuccess = (index, isHidden) => ({
  type: REVIEWS.TOGGLE_SHOWING_REVIEW_SUCCESS,
  index,
  isHidden,
});

export const toggleShowingReviewFail = err => ({
  type: REVIEWS.TOGGLE_SHOWING_REVIEW_FAIL,
  err,
});

export const deleteReview = (reviewId, workerId, index) => ({
  type: REVIEWS.DELETE_REVIEW,
  reviewId,
  workerId,
  index,
});

export const deleteReviewSuccess = index => ({
  type: REVIEWS.DELETE_REVIEW_SUCCESS,
  index,
});

export const deleteReviewFail = err => ({
  type: REVIEWS.DELETE_REVIEW_FAIL,
  err,
});
