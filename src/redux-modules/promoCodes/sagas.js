import { takeEvery, takeLatest, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { PROMO_CODES } from './actions';

const {
  GET_ALL_PROMO_CODES,
  GET_ALL_PROMO_CODES_SUCCESS,
  GET_ALL_PROMO_CODES_FAIL,
  ADD_PROMO_CODE,
  ADD_PROMO_CODE_SUCCESS,
  ADD_PROMO_CODE_FAIL,
  DELETE_PROMO_CODE,
  DELETE_PROMO_CODE_SUCCESS,
  DELETE_PROMO_CODE_FAIL,
  GET_PROMO_CODES,
  GET_PROMO_CODES_SUCCESS,
  GET_PROMO_CODES_FAIL,

  UPDATE_PROMO_CODE,
  UPDATE_PROMO_CODE_SUCCESS,
  UPDATE_PROMO_CODE_FAIL,
} = PROMO_CODES;

function* getAllPromoCodes(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { promoCodes } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = promoCodes.general;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else if (options[key] && options[key] !== '') {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allPromoCodes } = response;

    yield put({
      type: GET_ALL_PROMO_CODES_SUCCESS,
      promoCodes: allPromoCodes,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;
    yield put({
      type: GET_ALL_PROMO_CODES_FAIL,
      err: message,
    });
  }
}

function* addPromoCode(payload) {
  const { newPromoCodeInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { promoCodes } = Urls;
  const url = `${promoCodes.general}`;
  const api = new Api();
  const body = {
    ...newPromoCodeInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newPromoCodeInfoResponse } = response;

    yield put({
      type: ADD_PROMO_CODE_SUCCESS,
      newPromoCodeInfo: newPromoCodeInfoResponse,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_PROMO_CODE_FAIL,
      err: message,
    });
  }
}
function* updatePromoCode(payload) {
  const { newPromoCodeInfo, promoId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { promoCodes } = Urls;
  const url = `${promoCodes.general}/${promoId}`;
  const api = new Api();
  const body = {
    ...newPromoCodeInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {

    yield put({
      type: UPDATE_PROMO_CODE_SUCCESS,
      newPromoCodeInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_PROMO_CODE_FAIL,
      err: message,
    });
  }
}

function* deletePromoCode(payload) {
  const { promoCodeId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { promoCodes } = Urls;
  const url = `${promoCodes.general}/${promoCodeId}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.delete(url, undefined, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_PROMO_CODE_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_PROMO_CODE_FAIL,
      err: message,
    });
  }
}

function* getPromoCodesReport(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { promoCodes } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = promoCodes.promoCodeReport;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else if (options[key] && options[key] !== '') {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: promoCodesReportList } = response;

    yield put({
      type: GET_PROMO_CODES_SUCCESS,
      promoCodesReportList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_PROMO_CODES_FAIL,
      err: message,
    });
  }
}

function* workersSaga() {
  yield takeLatest(GET_ALL_PROMO_CODES, getAllPromoCodes);
  yield takeEvery(ADD_PROMO_CODE, addPromoCode);
  yield takeEvery(UPDATE_PROMO_CODE, updatePromoCode);
  yield takeEvery(DELETE_PROMO_CODE, deletePromoCode);
  yield takeLatest(GET_PROMO_CODES, getPromoCodesReport);
}

export default workersSaga;
