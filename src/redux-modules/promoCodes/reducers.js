import { unionBy } from 'lodash';
import ROUTES from 'routes';
import update from 'immutability-helper';
import { PROMO_CODES } from './actions';

const initialState = {
  promoCodes: undefined,
  hasMorePromoCodes: true,
  applyNewFilter: false,
  getAllPromoCodes: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addPromoCode: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editPromoCode: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deletePromoCode: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  addNewPromoCodeModal: { isOpen: false },
  editPromoModal: { isOpen: false },
  promoCodesReportList: undefined,
  getPromoCodesReport: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_PROMO_CODES,
    GET_ALL_PROMO_CODES_SUCCESS,
    GET_ALL_PROMO_CODES_FAIL,
    ADD_PROMO_CODE,
    ADD_PROMO_CODE_SUCCESS,
    ADD_PROMO_CODE_FAIL,
    ADD_PROMO_CODE_RESET,
    DELETE_PROMO_CODE,
    DELETE_PROMO_CODE_SUCCESS,
    DELETE_PROMO_CODE_FAIL,
    TOGGLE_ADD_NEW_PROMO_CODE_MODAL,
    GET_PROMO_CODES,
    GET_PROMO_CODES_SUCCESS,
    GET_PROMO_CODES_FAIL,

    UPDATE_PROMO_CODE,
    UPDATE_PROMO_CODE_SUCCESS,
    UPDATE_PROMO_CODE_FAIL,
    TOGGLE_EDIT_PROMO_CODE_MODAL,
  } = PROMO_CODES;
  const { PROMO_CODES: PROMO_CODES_ROUTE } = ROUTES;
  const {
    promoCodes,
    applyNewFilter,
    index,
    isOpen,
    newPromoCodeInfo,
    promoCodesReportList,
    err,
  } = payload;
  const {
    promoCodes: promoCodesState,
    promoCodesReportList: promoCodesReportListState,
    addNewPromoCodeModal: { isOpen: addNewPromoCodeModalIsOpenState },
    editPromoModal: { isOpen: editPromoCodeModalOpenState },
  } = state;

  switch (type) {
    case GET_ALL_PROMO_CODES: {
      return {
        ...state,
        applyNewFilter,
        getAllPromoCodes: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_PROMO_CODES_SUCCESS: {
      return {
        ...state,
        promoCodes:
          promoCodesState && !applyNewFilter
            ? unionBy(promoCodesState, promoCodes, '_id')
            : promoCodes,
        hasMorePromoCodes: promoCodes.length === 20,
        getAllPromoCodes: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_PROMO_CODES_FAIL: {
      return {
        ...state,
        getAllPromoCodes: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_PROMO_CODE: {
      return {
        ...state,
        addPromoCode: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_PROMO_CODE_SUCCESS: {
      promoCodesState.unshift(newPromoCodeInfo);

      return {
        ...state,
        promoCodes: promoCodesState,
        addPromoCode: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewPromoCodeModal: { isOpen },
      };
    }
    case ADD_PROMO_CODE_FAIL: {
      return {
        ...state,
        addPromoCode: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_PROMO_CODE_RESET: {
      return {
        ...state,
        addPromoCode: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_PROMO_CODE: {
      return {
        ...state,
        deletePromoCode: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_PROMO_CODE_SUCCESS: {
      if (index !== undefined) promoCodesState.splice(index, 1);
      else window.location.pathname = PROMO_CODES_ROUTE;

      return {
        ...state,
        promoCodes: promoCodesState,
        deletePromoCode: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case DELETE_PROMO_CODE_FAIL: {
      return {
        ...state,
        deletePromoCode: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_ADD_NEW_PROMO_CODE_MODAL: {
      return {
        ...state,
        addNewPromoCodeModal: {
          isOpen: !addNewPromoCodeModalIsOpenState,
        },
      };
    }
    case GET_PROMO_CODES: {
      return {
        ...state,
        applyNewFilter,
        getPromoCodesReport: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_PROMO_CODES_SUCCESS: {
      return {
        ...state,
        promoCodesReportList:
          promoCodesReportListState && !applyNewFilter
            ? unionBy(promoCodesReportListState, promoCodesReportList, '_id')
            : promoCodesReportList,
        hasMorePromoCodes: promoCodesReportList.length === 20,
        getPromoCodesReport: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_PROMO_CODES_FAIL: {
      return {
        ...state,
        getPromoCodesReport: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case UPDATE_PROMO_CODE: {
      return {
        ...state,
        editPromoCode: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_PROMO_CODE_SUCCESS: {
      const updatedPromoCodes = update(promoCodesState, {
        [index]: { $set: { ...newPromoCodeInfo } },
      });
      return {
        ...state,
        posts: updatedPromoCodes,
        editPromoCode: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editPromoModal: {
          isOpen: !editPromoCodeModalOpenState,
        },
      };
    }
    case UPDATE_PROMO_CODE_FAIL: {
      return {
        ...state,
        editPromoCode: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_EDIT_PROMO_CODE_MODAL: {
      return {
        ...state,
        editPromoModal: { isOpen: !editPromoCodeModalOpenState },
      };
    }
    default:
      return state;
  }
};
