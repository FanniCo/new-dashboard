export const PROMO_CODES = {
  GET_ALL_PROMO_CODES: 'GET_ALL_PROMO_CODES',
  GET_ALL_PROMO_CODES_SUCCESS: 'GET_ALL_PROMO_CODES_SUCCESS',
  GET_ALL_PROMO_CODES_FAIL: 'GET_ALL_PROMO_CODES_FAIL',
  ADD_PROMO_CODE: 'ADD_PROMO_CODE',
  ADD_PROMO_CODE_SUCCESS: 'ADD_PROMO_CODE_SUCCESS',
  ADD_PROMO_CODE_FAIL: 'ADD_PROMO_CODE_FAIL',
  ADD_PROMO_CODE_RESET: 'ADD_PROMO_CODE_RESET',
  DELETE_PROMO_CODE: 'DELETE_PROMO_CODE',
  DELETE_PROMO_CODE_SUCCESS: 'DELETE_PROMO_CODE_SUCCESS',
  DELETE_PROMO_CODE_FAIL: 'DELETE_PROMO_CODE_FAIL',
  TOGGLE_ADD_NEW_PROMO_CODE_MODAL: 'TOGGLE_ADD_NEW_PROMO_CODE_MODAL',
  TOGGLE_EDIT_PROMO_CODE_MODAL: 'TOGGLE_EDIT_PROMO_CODE_MODAL',
  GET_PROMO_CODES: 'GET_PROMO_CODES',
  GET_PROMO_CODES_SUCCESS: 'GET_PROMO_CODES_SUCCESS',
  GET_PROMO_CODES_FAIL: 'GET_PROMO_CODES_FAIL',
  UPDATE_PROMO_CODE: 'UPDATE_PROMO_CODE',
  UPDATE_PROMO_CODE_SUCCESS: 'UPDATE_PROMO_CODE_SUCCESS',
  UPDATE_PROMO_CODE_FAIL: 'UPDATE_PROMO_CODE_FAIL',
};

export const getAllPromoCodes = (options, applyNewFilter = false) => ({
  type: PROMO_CODES.GET_ALL_PROMO_CODES,
  options,
  applyNewFilter,
});

export const getAllPromoCodesSuccess = (promoCodes, applyNewFilter = false) => ({
  type: PROMO_CODES.GET_ALL_PROMO_CODES_SUCCESS,
  promoCodes,
  applyNewFilter,
});

export const getAllPromoCodesFail = err => ({
  type: PROMO_CODES.GET_ALL_PROMO_CODES_FAIL,
  err,
});

export const addPromoCode = newPromoCodeInfo => ({
  type: PROMO_CODES.ADD_PROMO_CODE,
  newPromoCodeInfo,
});

export const addPromoCodeSuccess = newPromoCodeInfo => ({
  type: PROMO_CODES.ADD_PROMO_CODE_SUCCESS,
  newPromoCodeInfo,
});

export const addPromoCodeFail = err => ({
  type: PROMO_CODES.ADD_WORKER_FAIL,
  err,
});

export const updatePromoCode = (newPromoCodeInfo, promoId, index) => ({
  type: PROMO_CODES.UPDATE_PROMO_CODE,
  newPromoCodeInfo,
  promoId,
  index,
});

export const updatePromoCodeSuccess = (newPromoCodeInfo, index) => ({
  type: PROMO_CODES.UPDATE_PROMO_CODE_SUCCESS,
  newPromoCodeInfo,
  index,
});

export const updatePromoCodeFail = err => ({
  type: PROMO_CODES.UPDATE_PROMO_CODE_FAIL,
  err,
});

export const addPromoCodeReset = () => ({
  type: PROMO_CODES.ADD_PROMO_CODE_RESET,
});

export const deletePromoCode = (promoCodeId, index) => ({
  type: PROMO_CODES.DELETE_PROMO_CODE,
  promoCodeId,
  index,
});

export const deletePromoCodeSuccess = index => ({
  type: PROMO_CODES.DELETE_PROMO_CODE_SUCCESS,
  index,
});

export const deletePromoCodeFail = err => ({
  type: PROMO_CODES.DELETE_PROMO_CODE_FAIL,
  err,
});

export const toggleAddNewPromoCodeModal = () => ({
  type: PROMO_CODES.TOGGLE_ADD_NEW_PROMO_CODE_MODAL,
});

export const openEditPromoModal = () => ({
  type: PROMO_CODES.TOGGLE_EDIT_PROMO_CODE_MODAL,
});

export const getPromoCodes = (options, applyNewFilter = false) => ({
  type: PROMO_CODES.GET_PROMO_CODES,
  options,
  applyNewFilter,
});

export const getPromoCodesSuccess = (promoCodesReportList, applyNewFilter = false) => ({
  type: PROMO_CODES.GET_PROMO_CODES_SUCCESS,
  promoCodesReportList,
  applyNewFilter,
});

export const getPromoCodesFail = err => ({
  type: PROMO_CODES.GET_PROMO_CODES_FAIL,
  err,
});
