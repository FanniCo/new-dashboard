import { put, takeEvery, takeLatest } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Status, Urls } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { APP_CONFIGS } from './actions';

const {
  GET_ALL_APP_CONFIGS,
  GET_ALL_APP_CONFIGS_SUCCESS,
  GET_ALL_APP_CONFIGS_FAIL,

  UPDATE_APP_CONFIG,
  UPDATE_APP_CONFIG_SUCCESS,
  UPDATE_APP_CONFIG_FAIL,
} = APP_CONFIGS;

function* getAllAppConfigs() {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const url = Urls.configs.getAppUpdates;

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allAppConfigs } = response;

    yield put({
      type: GET_ALL_APP_CONFIGS_SUCCESS,
      appConfigs: allAppConfigs,
    });
  } else {
    const {
      response: { message },
    } = response;
    yield put({
      type: GET_ALL_APP_CONFIGS_FAIL,
      err: message,
    });
  }
}

function* updateAppConfig(payload) {
  const { appConfigInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const url = `${Urls.configs.setAppUpdates}`;
  const api = new Api();
  const body = { ...appConfigInfo };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {

    yield put({
      type: UPDATE_APP_CONFIG_SUCCESS,
      appConfigInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_APP_CONFIG_FAIL,
      err: message,
    });
  }
}

function* workersSaga() {
  yield takeLatest(GET_ALL_APP_CONFIGS, getAllAppConfigs);
  yield takeEvery(UPDATE_APP_CONFIG, updateAppConfig);
}

export default workersSaga;
