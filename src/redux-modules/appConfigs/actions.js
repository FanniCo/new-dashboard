export const APP_CONFIGS = {
  GET_ALL_APP_CONFIGS: 'GET_ALL_APP_CONFIGS',
  GET_ALL_APP_CONFIGS_SUCCESS: 'GET_ALL_APP_CONFIGS_SUCCESS',
  GET_ALL_APP_CONFIGS_FAIL: 'GET_ALL_APP_CONFIGS_FAIL',

  UPDATE_APP_CONFIG: 'UPDATE_APP_CONFIG',
  UPDATE_APP_CONFIG_SUCCESS: 'UPDATE_APP_CONFIG_SUCCESS',
  UPDATE_APP_CONFIG_FAIL: 'UPDATE_APP_CONFIG_FAIL',

  TOGGLE_EDIT_APP_CONFIG_MODAL: 'TOGGLE_EDIT_APP_CONFIG_MODAL',
};

export const getAllAppConfigs = () => ({
  type: APP_CONFIGS.GET_ALL_APP_CONFIGS,
});

export const getAllAppConfigsSuccess = appConfigs => ({
  type: APP_CONFIGS.GET_ALL_APP_CONFIGS_SUCCESS,
  appConfigs,
});

export const getAllAppConfigsFail = err => ({
  type: APP_CONFIGS.GET_ALL_APP_CONFIGS_FAIL,
  err,
});

// --------------------------------------------------------

export const updateAppConfig = appConfigInfo => ({
  type: APP_CONFIGS.UPDATE_APP_CONFIG,
  appConfigInfo,
});

export const updateAppConfigSuccess = appConfigInfo => ({
  type: APP_CONFIGS.UPDATE_APP_CONFIG_SUCCESS,
  appConfigInfo,
});

export const updateAppConfigFail = err => ({
  type: APP_CONFIGS.UPDATE_APP_CONFIG_FAIL,
  err,
});

// --------------------------------------------------------

export const toggleAddNewAppConfigModal = () => ({
  type: APP_CONFIGS.TOGGLE_ADD_NEW_APP_CONFIG_MODAL,
});

export const openEditPromoModal = () => ({
  type: APP_CONFIGS.TOGGLE_EDIT_APP_CONFIG_MODAL,
});
