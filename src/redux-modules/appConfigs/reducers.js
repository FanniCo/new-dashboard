import { unionBy } from 'lodash';
import { APP_CONFIGS } from './actions';

const initialState = {
  appConfigs: undefined,
  getAllAppConfigs: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editAppConfig: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_APP_CONFIGS,
    GET_ALL_APP_CONFIGS_FAIL,
    GET_ALL_APP_CONFIGS_SUCCESS,

    UPDATE_APP_CONFIG,
    UPDATE_APP_CONFIG_FAIL,
    UPDATE_APP_CONFIG_SUCCESS,

  } = APP_CONFIGS;

  const {
    appConfigs,
    appConfigInfo,
    err,
  } = payload;

  const {
    appConfigs: appConfigsState,
  } = state;

  switch (type) {
    case GET_ALL_APP_CONFIGS: {
      return {
        ...state,
        getAllAppConfigs: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_APP_CONFIGS_SUCCESS: {
      return {
        ...state,
        appConfigs:
          appConfigsState
            ? unionBy(appConfigsState, appConfigs, '_id')
            : appConfigs,
        getAllAppConfigs: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_APP_CONFIGS_FAIL: {
      return {
        ...state,
        getAllAppConfigs: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case UPDATE_APP_CONFIG: {
      return {
        ...state,
        editAppConfig: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_APP_CONFIG_SUCCESS: {
      return {
        ...state,
        appConfigs: appConfigInfo,
        editAppConfig: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_APP_CONFIG_FAIL: {
      return {
        ...state,
        editAppConfig: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    default:
      return state;
  }
};
