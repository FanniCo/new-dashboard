import { takeEvery, takeLatest, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import querystring from 'query-string';
import { TRANSACTIONS } from './actions';

const {
  GET_ALL_TRANSACTIONS,
  GET_ALL_TRANSACTIONS_SUCCESS,
  GET_ALL_TRANSACTIONS_FAIL,
  GET_WORKER_TRANSACTIONS,
  GET_WORKER_TRANSACTIONS_SUCCESS,
  GET_WORKER_TRANSACTIONS_FAIL,
  GET_CLIENT_TRANSACTIONS,
  GET_CLIENT_TRANSACTIONS_SUCCESS,
  GET_CLIENT_TRANSACTIONS_FAIL,
} = TRANSACTIONS;

function* getAllTransactions(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { transactions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = transactions.getAllTransactions;
  const queryString = querystring.stringify(options);
  url += `?${queryString}`;
  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allTransactions } = response;

    yield put({
      type: GET_ALL_TRANSACTIONS_SUCCESS,
      transactions: allTransactions,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_TRANSACTIONS_FAIL,
      err: message,
    });
  }
}

function* getWorkerTransactions(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { transactions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = transactions.getAllTransactions;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allTransactions } = response;

    yield put({
      type: GET_WORKER_TRANSACTIONS_SUCCESS,
      workerTransactions: allTransactions,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKER_TRANSACTIONS_FAIL,
      err: message,
    });
  }
}

function* getClientTransactions(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { transactions } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = transactions.getAllTransactions;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allTransactions } = response;

    yield put({
      type: GET_CLIENT_TRANSACTIONS_SUCCESS,
      clientTransactions: allTransactions,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CLIENT_TRANSACTIONS_FAIL,
      err: message,
    });
  }
}

function* userSaga() {
  yield takeLatest(GET_ALL_TRANSACTIONS, getAllTransactions);
  yield takeEvery(GET_WORKER_TRANSACTIONS, getWorkerTransactions);
  yield takeEvery(GET_CLIENT_TRANSACTIONS, getClientTransactions);
}

export default userSaga;
