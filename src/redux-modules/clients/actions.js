export const CLIENTS = {
  GET_ALL_CLIENTS: 'GET_ALL_CLIENTS',
  GET_ALL_CLIENTS_SUCCESS: 'GET_ALL_CLIENTS_SUCCESS',
  GET_ALL_CLIENTS_FAIL: 'GET_ALL_CLIENTS_FAIL',
  EDIT_CLIENT: 'EDIT_CLIENT',
  EDIT_CLIENT_SUCCESS: 'EDIT_CLIENT_SUCCESS',
  EDIT_CLIENT_FAIL: 'EDIT_CLIENT_FAIL',
  EDIT_CLIENT_RESET: 'EDIT_CLIENT_RESET',
  CONVERT_CLIENT_TO_WORKER: 'CONVERT_CLIENT_TO_WORKER',
  CONVERT_CLIENT_TO_WORKER_SUCCESS: 'CONVERT_CLIENT_TO_WORKER_SUCCESS',
  CONVERT_CLIENT_TO_WORKER_FAIL: 'CONVERT_CLIENT_TO_WORKER_FAIL',
  CONVERT_CLIENT_TO_WORKER_RESET: 'CONVERT_CLIENT_TO_WORKER_RESET',
  SUSPEND_CLIENT: 'SUSPEND_CLIENT',
  SUSPEND_CLIENT_SUCCESS: 'SUSPEND_CLIENT_SUCCESS',
  SUSPEND_CLIENT_FAIL: 'SUSPEND_CLIENT_FAIL',
  GET_CLIENT_DETAILS: 'GET_CLIENT_DETAILS',
  GET_CLIENT_DETAILS_SUCCESS: 'GET_CLIENT_DETAILS_SUCCESS',
  GET_CLIENT_DETAILS_FAIL: 'GET_CLIENT_DETAILS_FAIL',
  ADD_CLIENT_COMMENT: {
    LOAD: 'CLIENTS_ADD_CLIENT_COMMENT_LOAD',
    SUCCESS: 'CLIENTS_ADD_CLIENT_COMMENT_SUCCESS',
    FAIL: 'CLIENTS_ADD_CLIENT_COMMENT_FAIL',
  },
  TOGGLE_EDIT_CLIENT_MODAL: 'TOGGLE_EDIT_CLIENT_MODAL',
  TOGGLE_CONVERT_CLIENT_TO_WORKER_MODAL: 'TOGGLE_CONVERT_CLIENT_TO_WORKER_MODAL',
  GET_CLIENTS_PAYMENTS: 'GET_CLIENTS_PAYMENTS',
  GET_CLIENTS_PAYMENTS_SUCCESS: 'GET_CLIENTS_PAYMENTS_SUCCESS',
  GET_CLIENTS_PAYMENTS_FAIL: 'GET_CLIENTS_PAYMENTS_FAIL',
  TOGGLE_CLIENT_ADD_CREDIT_MODAL: 'TOGGLE_CLIENT_ADD_CREDIT_MODAL',
  ADD_CREDIT_RESET: 'ADD_CREDIT_RESET',
  CLIENT_ADD_CREDIT: 'CLIENT_ADD_CREDIT',
  CLIENT_ADD_CREDIT_SUCCESS: 'CLIENT_ADD_CREDIT_SUCCESS',
  CLIENT_ADD_CREDIT_FAIL: 'CLIENT_ADD_CREDIT_FAIL',
  TOGGLE_ADD_NEW_REQUEST_MODAL: 'TOGGLE_ADD_NEW_REQUEST_MODAL',
  ADD_NEW_REQUEST: 'ADD_NEW_REQUEST',
  ADD_NEW_REQUEST_SUCCESS: 'ADD_NEW_REQUEST_SUCCESS',
  ADD_NEW_REQUEST_FAIL: 'ADD_NEW_REQUEST_FAIL',
  GET_CLIENTS_REQUESTS_COUNT: 'GET_CLIENTS_REQUESTS_COUNT',
  GET_CLIENTS_REQUESTS_COUNT_SUCCESS: 'GET_CLIENTS_REQUESTS_COUNT_SUCCESS',
  GET_CLIENTS_REQUESTS_COUNT_FAIL: 'GET_CLIENTS_REQUESTS_COUNT_FAIL',
  TOGGLE_SUSPEND_MODAL: 'TOGGLE_SUSPEND_MODAL',
  TOGGLE_CONFIRM_MODAL: 'TOGGLE_CONFIRM_MODAL',
  TOGGLE_ADMIN_ADDED_REQUEST: 'TOGGLE_ADMIN_ADDED_REQUEST',
};

export const getAllClients = (options, applyNewFilter: false) => ({
  type: CLIENTS.GET_ALL_CLIENTS,
  options,
  applyNewFilter,
});

export const getAllClientsSuccess = (clients, applyNewFilter: false) => ({
  type: CLIENTS.GET_ALL_CLIENTS_SUCCESS,
  clients,
  applyNewFilter,
});

export const getAllClientsFail = error => ({
  type: CLIENTS.GET_ALL_CLIENTS_FAIL,
  error,
});

export const editClient = (clientId, index, editParams) => ({
  type: CLIENTS.EDIT_CLIENT,
  clientId,
  index,
  editParams,
});

export const editClientSuccess = (index, editParams) => ({
  type: CLIENTS.EDIT_CLIENT_SUCCESS,
  index,
  editParams,
});

export const editClientFail = error => ({
  type: CLIENTS.EDIT_CLIENT_FAIL,
  error,
});

export const editClientReset = () => ({
  type: CLIENTS.EDIT_CLIENT_RESET,
});

export const convertClientToWorker = convertInfo => ({
  type: CLIENTS.CONVERT_CLIENT_TO_WORKER,
  convertInfo,
});

export const convertClientToWorkerSuccess = convertInfo => ({
  type: CLIENTS.CONVERT_CLIENT_TO_WORKER_SUCCESS,
  convertInfo,
});

export const convertClientToWorkerFail = error => ({
  type: CLIENTS.CONVERT_CLIENT_TO_WORKER_FAIL,
  error,
});

export const convertClientToWorkerReset = () => ({
  type: CLIENTS.CONVERT_CLIENT_TO_WORKER_RESET,
});

export const suspendClient = (clientId, index, suspended) => ({
  type: CLIENTS.SUSPEND_CLIENT,
  clientId,
  index,
  suspended,
});

export const suspendClientSuccess = (index, suspended) => ({
  type: CLIENTS.SUSPEND_CLIENT_SUCCESS,
  index,
  suspended,
});

export const suspendClientFail = error => ({
  type: CLIENTS.SUSPEND_CLIENT_FAIL,
  error,
});

export const getClientDetails = clientUserName => ({
  type: CLIENTS.GET_CLIENT_DETAILS,
  clientUserName,
});

export const getClientDetailsSuccess = clientDetails => ({
  type: CLIENTS.GET_CLIENT_DETAILS_SUCCESS,
  clientDetails,
});

export const getClientDetailsFail = error => ({
  type: CLIENTS.GET_CLIENT_DETAILS_FAIL,
  error,
});

export const addComment = (commentInfo, customerId) => ({
  type: CLIENTS.ADD_CLIENT_COMMENT.LOAD,
  commentInfo,
  customerId,
});

export const addCommentSuccess = (comments, index) => ({
  type: CLIENTS.ADD_CLIENT_COMMENT.SUCCESS,
  comments,
  index,
});

export const addCommentFail = error => ({
  type: CLIENTS.ADD_CLIENT_COMMENT.FAIL,
  error,
});

export const toggleEditClientModal = () => ({
  type: CLIENTS.TOGGLE_EDIT_CLIENT_MODAL,
});

export const toggleConvertClientToWorkerModal = () => ({
  type: CLIENTS.TOGGLE_CONVERT_CLIENT_TO_WORKER_MODAL,
});

export const getClientsPayments = (options, applyNewFilter: false) => ({
  type: CLIENTS.GET_CLIENTS_PAYMENTS,
  options,
  applyNewFilter,
});

export const getClientsPaymentsSuccess = (clientsPayments, applyNewFilter: false) => ({
  type: CLIENTS.GET_CLIENTS_PAYMENTS_SUCCESS,
  clientsPayments,
  applyNewFilter,
});

export const getClientsPaymentsFail = error => ({
  type: CLIENTS.GET_CLIENTS_PAYMENTS_FAIL,
  error,
});

export const toggleClientAddCreditModal = error => ({
  type: CLIENTS.TOGGLE_CLIENT_ADD_CREDIT_MODAL,
  error,
});

export const clientAddCreditReset = () => ({
  type: CLIENTS.ADD_CREDIT_RESET,
});

export const clientAddCredit = (index, addCreditInfo) => ({
  type: CLIENTS.CLIENT_ADD_CREDIT,
  index,
  addCreditInfo,
});

export const clientAddCreditSuccess = () => ({
  type: CLIENTS.CLIENT_ADD_CREDIT_SUCCESS,
});

export const clientAddCreditFail = () => ({
  type: CLIENTS.CLIENT_ADD_CREDIT_FAIL,
});

export const toggleAddNewRequestModal = () => ({
  type: CLIENTS.TOGGLE_ADD_NEW_REQUEST_MODAL,
});

export const addNewRequest = newRequestData => ({
  type: CLIENTS.ADD_NEW_REQUEST,
  newRequestData,
});

export const addNewRequestSuccess = () => ({
  type: CLIENTS.ADD_NEW_REQUEST_SUCCESS,
});

export const addNewRequestFail = error => ({
  type: CLIENTS.ADD_NEW_REQUEST_FAIL,
  error,
});

export const getClientsRequestsCount = (options, applyNewFilter) => ({
  type: CLIENTS.GET_CLIENTS_REQUESTS_COUNT,
  options,
  applyNewFilter,
});

export const getClientsRequestsCountSuccess = (clientsRequestsCount, applyNewFilter: false) => ({
  type: CLIENTS.GET_CLIENTS_REQUESTS_COUNT_SUCCESS,
  clientsRequestsCount,
  applyNewFilter,
});

export const getClientsRequestsCountFail = error => ({
  type: CLIENTS.GET_CLIENTS_REQUESTS_COUNT_FAIL,
  error,
});

export const toggleSuspendModal = workerId => ({
  type: CLIENTS.TOGGLE_SUSPEND_MODAL,
  workerId,
});

export const toggleConfirmModal = isOpen => ({
  type: CLIENTS.TOGGLE_CONFIRM_MODAL,
  isOpen,
});

export const toggleAdminAddedRequest = isOpen => ({
  type: CLIENTS.TOGGLE_ADMIN_ADDED_REQUEST,
  isOpen,
});
