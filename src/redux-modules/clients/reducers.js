import { findIndex, unionBy } from 'lodash';
import update from 'immutability-helper';
import { CLIENTS } from './actions';

const initialState = {
  clients: undefined,
  hasMoreClients: true,
  applyNewFilter: false,
  clientDetails: undefined,
  clientsPayments: undefined,
  hasMoreClientsPayments: true,
  hasMoreClientsRequestsCount: true,
  clientsRequestsCount: undefined,
  getAllClients: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editClient: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  convertClientToWorker: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  suspendClient: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getClientDetails: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addComment: {
    commentInfo: undefined,
    addCommentState: undefined,
    error: undefined,
  },
  getClientsPayments: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editClientModal: { isOpen: false },
  convertClientToWorkerModal: { isOpen: false },
  clientAddCreditsModal: {
    isOpen: false,
  },
  addNewRequestModal: {
    isOpen: false,
  },
  addNewRequest: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getClientsRequestsCount: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  suspendModal: { isOpen: false },
  confirmModal: { isOpen: false },
  addCredit: {
    isFetching: false,
    isSuccess: true,
    isFail: { isError: false, message: '' },
  },
  customerId: undefined,
  adminAddedRequest: false,
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_CLIENTS,
    GET_ALL_CLIENTS_SUCCESS,
    GET_ALL_CLIENTS_FAIL,
    EDIT_CLIENT,
    EDIT_CLIENT_SUCCESS,
    EDIT_CLIENT_FAIL,
    EDIT_CLIENT_RESET,
    CONVERT_CLIENT_TO_WORKER,
    CONVERT_CLIENT_TO_WORKER_SUCCESS,
    CONVERT_CLIENT_TO_WORKER_FAIL,
    CONVERT_CLIENT_TO_WORKER_RESET,
    SUSPEND_CLIENT,
    SUSPEND_CLIENT_SUCCESS,
    SUSPEND_CLIENT_FAIL,
    GET_CLIENT_DETAILS,
    GET_CLIENT_DETAILS_SUCCESS,
    GET_CLIENT_DETAILS_FAIL,
    ADD_CLIENT_COMMENT,
    TOGGLE_EDIT_CLIENT_MODAL,
    TOGGLE_CONVERT_CLIENT_TO_WORKER_MODAL,
    GET_CLIENTS_PAYMENTS,
    GET_CLIENTS_PAYMENTS_SUCCESS,
    GET_CLIENTS_PAYMENTS_FAIL,
    TOGGLE_CLIENT_ADD_CREDIT_MODAL,
    CLIENT_ADD_CREDIT,
    CLIENT_ADD_CREDIT_SUCCESS,
    CLIENT_ADD_CREDIT_FAIL,
    TOGGLE_ADD_NEW_REQUEST_MODAL,
    ADD_NEW_REQUEST,
    ADD_NEW_REQUEST_SUCCESS,
    ADD_NEW_REQUEST_FAIL,
    GET_CLIENTS_REQUESTS_COUNT,
    GET_CLIENTS_REQUESTS_COUNT_SUCCESS,
    GET_CLIENTS_REQUESTS_COUNT_FAIL,
    TOGGLE_SUSPEND_MODAL,
    TOGGLE_CONFIRM_MODAL,
    TOGGLE_ADMIN_ADDED_REQUEST,
  } = CLIENTS;
  const {
    clients,
    clientsPayments,
    applyNewFilter,
    index,
    suspended,
    clientDetails,
    editParams,
    isOpen,
    commentInfo,
    comments,
    error,
    credits,
    clientsRequestsCount,
    customerId,
  } = payload;
  const {
    clients: clientsState,
    clientDetails: clientDetailsState,
    clientsPayments: clientsPaymentsState,
    editClientModal: { isOpen: editClientModalIsOpenState },
    convertClientToWorkerModal: { isOpen: convertClientToWorkerModalIsOpenState },
    clientAddCreditsModal: { isOpen: addCreditModalIsOpenState },
    addNewRequestModal: { isOpen: addNewRequestModalIsOpenState },
    clientsRequestsCount: clientsRequestsCountState,
    suspendModal: { isOpen: suspendModalIsOpenState },
    confirmModal: { isOpen: confirmModalIsOpenState },
  } = state;

  switch (type) {
    case GET_ALL_CLIENTS: {
      return {
        ...state,
        applyNewFilter,
        getAllClients: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_CLIENTS_SUCCESS: {
      return {
        ...state,
        clients: clientsState && !applyNewFilter ? unionBy(clientsState, clients, '_id') : clients,
        hasMoreClients: clients.length === 20,
        getAllClients: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_CLIENTS_FAIL: {
      return {
        ...state,
        getAllClients: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case EDIT_CLIENT: {
      return {
        ...state,
        editClient: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EDIT_CLIENT_SUCCESS: {
      const updatedClients =
        index !== undefined
          ? update(clientsState, { [index]: { $set: { ...clientsState[index], ...editParams } } })
          : clientsState;
      const updatedClientDetails =
        index !== undefined ? clientDetailsState : { ...clientDetailsState, ...editParams };

      return {
        ...state,
        clients: updatedClients,
        clientDetails: updatedClientDetails,
        editClient: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editClientModal: { isOpen },
      };
    }
    case EDIT_CLIENT_FAIL: {
      return {
        ...state,
        editClient: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case EDIT_CLIENT_RESET: {
      return {
        ...state,
        editClient: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CONVERT_CLIENT_TO_WORKER: {
      return {
        ...state,
        convertClientToWorker: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CONVERT_CLIENT_TO_WORKER_SUCCESS: {
      return {
        ...state,
        convertClientToWorker: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        convertClientToWorkerModal: { isOpen },
      };
    }
    case CONVERT_CLIENT_TO_WORKER_FAIL: {
      return {
        ...state,
        convertClientToWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case CONVERT_CLIENT_TO_WORKER_RESET: {
      return {
        ...state,
        convertClientToWorker: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case SUSPEND_CLIENT: {
      return {
        ...state,
        suspendClient: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case SUSPEND_CLIENT_SUCCESS: {
      const updatedClients =
        index !== undefined
          ? update(clientsState, { [index]: { suspendedTo: { $set: suspended.toString() } } })
          : clientsState;
      const updatedClientDetails =
        index !== undefined
          ? clientDetailsState
          : { ...clientDetailsState, suspendedTo: suspended.toString() };

      return {
        ...state,
        clients: updatedClients,
        clientDetails: updatedClientDetails,
        suspendClient: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: false },
        suspendModal: { isOpen: false },
      };
    }
    case SUSPEND_CLIENT_FAIL: {
      return {
        ...state,
        suspendClient: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case GET_CLIENT_DETAILS: {
      return {
        ...state,
        getClientDetails: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CLIENT_DETAILS_SUCCESS: {
      return {
        ...state,
        clientDetails,
        getClientDetails: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CLIENT_DETAILS_FAIL: {
      return {
        ...state,
        getClientDetails: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case ADD_CLIENT_COMMENT.LOAD: {
      return {
        ...state,
        addComment: {
          commentInfo,
          addCommentState: ADD_CLIENT_COMMENT.LOAD,
          error: undefined,
        },
      };
    }
    case ADD_CLIENT_COMMENT.SUCCESS: {
      const foundIndex = findIndex(clientsState, clientState => clientState._id === customerId);

      const updatedClients =
        foundIndex >= 0
          ? update(clientsState, { [foundIndex]: { comments: { $set: comments } } })
          : clientsState;
      const updatedClientDetails = index ? clientDetailsState : { ...clientDetailsState, comments };

      return {
        ...state,
        clients: updatedClients,
        clientDetails: updatedClientDetails,
        addComment: {
          commentInfo,
          addCommentState: ADD_CLIENT_COMMENT.SUCCESS,
          error: undefined,
        },
      };
    }
    case ADD_CLIENT_COMMENT.FAIL: {
      return {
        ...state,
        addComment: {
          commentInfo: undefined,
          addCommentState: ADD_CLIENT_COMMENT.FAIL,
          error,
        },
      };
    }
    case TOGGLE_EDIT_CLIENT_MODAL: {
      return {
        ...state,
        editClientModal: {
          isOpen: !editClientModalIsOpenState,
        },
      };
    }
    case TOGGLE_CONVERT_CLIENT_TO_WORKER_MODAL: {
      return {
        ...state,
        convertClientToWorkerModal: {
          isOpen: !convertClientToWorkerModalIsOpenState,
        },
      };
    }
    case GET_CLIENTS_PAYMENTS: {
      return {
        ...state,
        applyNewFilter,
        getClientsPayments: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CLIENTS_PAYMENTS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        clientsPayments:
          clientsPaymentsState && !applyNewFilter
            ? unionBy(clientsPaymentsState, clientsPayments, '_id')
            : clientsPayments,
        hasMoreClientsPayments: clientsPayments.length === 20,
        getClientsPayments: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CLIENTS_PAYMENTS_FAIL: {
      return {
        ...state,
        getClientsPayments: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case TOGGLE_CLIENT_ADD_CREDIT_MODAL: {
      return {
        ...state,
        clientAddCreditsModal: {
          isOpen: !addCreditModalIsOpenState,
        },
      };
    }
    case CLIENT_ADD_CREDIT: {
      return {
        ...state,
        addCredit: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CLIENT_ADD_CREDIT_SUCCESS: {
      const computedCredits =
        index !== undefined
          ? clientsState[index].credits + credits
          : clientDetailsState.credits + credits;
      const updatedClients =
        index !== undefined
          ? update(clientsState, { [index]: { credits: { $set: computedCredits } } })
          : clientsState;
      const updatedClientDetails =
        index !== undefined
          ? clientDetailsState
          : { ...clientDetailsState, credits: computedCredits };

      return {
        ...state,
        clients: updatedClients,
        clientDetails: updatedClientDetails,
        addCredit: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        clientAddCreditsModal: { isOpen: !addCreditModalIsOpenState },
      };
    }
    case CLIENT_ADD_CREDIT_FAIL: {
      return {
        ...state,
        addCredit: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case TOGGLE_ADD_NEW_REQUEST_MODAL: {
      return {
        ...state,
        addNewRequestModal: {
          isOpen: !addNewRequestModalIsOpenState,
        },
        addNewRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_REQUEST: {
      return {
        ...state,
        addNewRequest: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_REQUEST_SUCCESS: {
      return {
        ...state,
        addNewRequest: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewRequestModal: { isOpen },
        adminAddedRequest: true,
      };
    }
    case ADD_NEW_REQUEST_FAIL: {
      return {
        ...state,
        addNewRequest: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case GET_CLIENTS_REQUESTS_COUNT: {
      return {
        ...state,
        getClientsRequestsCount: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CLIENTS_REQUESTS_COUNT_SUCCESS: {
      return {
        ...state,
        clientsRequestsCount:
          clientsRequestsCountState && !applyNewFilter
            ? unionBy(clientsRequestsCountState, clientsRequestsCount, '_id')
            : clientsRequestsCount,
        hasMoreClientsRequestsCount: clientsRequestsCount.length === 20,
        getClientsRequestsCount: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CLIENTS_REQUESTS_COUNT_FAIL: {
      return {
        ...state,
        getClientsRequestsCount: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: error },
        },
      };
    }
    case TOGGLE_SUSPEND_MODAL: {
      return {
        ...state,
        suspendModal: { isOpen: !suspendModalIsOpenState },
      };
    }
    case TOGGLE_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: {
          isOpen: !confirmModalIsOpenState,
        },
      };
    }
    case TOGGLE_ADMIN_ADDED_REQUEST: {
      return {
        ...state,
        adminAddedRequest: false,
      };
    }
    default:
      return state;
  }
};
