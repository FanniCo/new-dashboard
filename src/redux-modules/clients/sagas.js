import { put, takeEvery, takeLatest } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Status, Urls } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { CLIENTS } from './actions';

const {
  GET_ALL_CLIENTS,
  GET_ALL_CLIENTS_SUCCESS,
  GET_ALL_CLIENTS_FAIL,
  EDIT_CLIENT,
  EDIT_CLIENT_SUCCESS,
  EDIT_CLIENT_FAIL,
  CONVERT_CLIENT_TO_WORKER,
  CONVERT_CLIENT_TO_WORKER_SUCCESS,
  CONVERT_CLIENT_TO_WORKER_FAIL,
  SUSPEND_CLIENT,
  SUSPEND_CLIENT_SUCCESS,
  SUSPEND_CLIENT_FAIL,
  GET_CLIENT_DETAILS,
  GET_CLIENT_DETAILS_SUCCESS,
  GET_CLIENT_DETAILS_FAIL,
  ADD_CLIENT_COMMENT,
  GET_CLIENTS_PAYMENTS,
  GET_CLIENTS_PAYMENTS_SUCCESS,
  GET_CLIENTS_PAYMENTS_FAIL,
  CLIENT_ADD_CREDIT,
  CLIENT_ADD_CREDIT_SUCCESS,
  CLIENT_ADD_CREDIT_FAIL,
  ADD_NEW_REQUEST,
  ADD_NEW_REQUEST_SUCCESS,
  ADD_NEW_REQUEST_FAIL,
  GET_CLIENTS_REQUESTS_COUNT,
  GET_CLIENTS_REQUESTS_COUNT_SUCCESS,
  GET_CLIENTS_REQUESTS_COUNT_FAIL,
} = CLIENTS;

function* getAllClients(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { clients } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = clients.getAllClients;

  Object.keys(options).forEach(key => {
    url = `${url}&${key}=${options[key]}`;
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allClients } = response;

    yield put({
      type: GET_ALL_CLIENTS_SUCCESS,
      clients: allClients,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_CLIENTS_FAIL,
      error: message,
    });
  }
}

function* editClient(payload) {
  const { clientId, index, editParams } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = `${user.general}/${clientId}`;
  const api = new Api();
  const body = {
    ...editParams,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: EDIT_CLIENT_SUCCESS,
      index,
      editParams,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: EDIT_CLIENT_FAIL,
      error: message,
    });
  }
}

function* convertClientToWorker(payload) {
  const { convertInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { clients } = Urls;
  const url = clients.convertClientToWorker;
  const api = new Api();
  const body = {
    ...convertInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: CONVERT_CLIENT_TO_WORKER_SUCCESS,
      convertInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CONVERT_CLIENT_TO_WORKER_FAIL,
      error: message.includes('governmentId') ? 'رقم هوية غير صحيح' : message,
    });
  }
}

function* suspendClient(payload) {
  const { clientId, index, suspended } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = user.suspendUser;
  const api = new Api();
  const body = {
    userId: clientId,
    suspend: suspended,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: SUSPEND_CLIENT_SUCCESS,
      index,
      suspended,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: SUSPEND_CLIENT_FAIL,
      error: message,
    });
  }
}

function* getClientDetails(payload) {
  const { clientUserName } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { clients } = Urls;
  const url = `${clients.getClientsByUsername}/${clientUserName}?role=customer`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: clientsArray } = response;
    const clientDetails = clientsArray[0];

    yield put({
      type: GET_CLIENT_DETAILS_SUCCESS,
      clientDetails,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CLIENT_DETAILS_FAIL,
      error: message,
    });
  }
}

function* addComment(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const url = user.comments;
  const { commentInfo } = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.post(url, commentInfo, header);
  const responseStatus = response.status;
  const { comments, _id: customerId } = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ADD_CLIENT_COMMENT.SUCCESS,
      comments,
      customerId,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_CLIENT_COMMENT.FAIL,
      error: message,
    });
  }
}

function* getClientsPayments(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { clients } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = clients.getClientsPayments;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allClientsPayments } = response;

    yield put({
      type: GET_CLIENTS_PAYMENTS_SUCCESS,
      clientsPayments: allClientsPayments,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CLIENTS_PAYMENTS_FAIL,
      error: message,
    });
  }
}

function* addCredit(payload) {
  const { index, addCreditInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { transactions } = Urls;
  const url = transactions.addClientCredit;
  const api = new Api();
  const body = {
    ...addCreditInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { worth: credits } = addCreditInfo;
    yield put({
      type: CLIENT_ADD_CREDIT_SUCCESS,
      index,
      credits,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CLIENT_ADD_CREDIT_FAIL,
      error: message.includes('NaN') ? 'الرصيد ليس رقم صحيح' : message,
    });
  }
}

function* addNewRequest(payload) {
  const { newRequestData } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { requests } = Urls;
  const url = requests.addNewRequest;
  const api = new Api();
  const body = {
    ...newRequestData,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: ADD_NEW_REQUEST_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_REQUEST_FAIL,
      error: message,
    });
  }
}

function* getAllClientsRequestsCount(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { clients } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = clients.clientsReport;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: clientsRequestsCount } = response;

    yield put({
      type: GET_CLIENTS_REQUESTS_COUNT_SUCCESS,
      clientsRequestsCount,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CLIENTS_REQUESTS_COUNT_FAIL,
      error: message,
    });
  }
}

function* clientsSaga() {
  yield takeLatest(GET_ALL_CLIENTS, getAllClients);
  yield takeEvery(EDIT_CLIENT, editClient);
  yield takeEvery(CONVERT_CLIENT_TO_WORKER, convertClientToWorker);
  yield takeEvery(SUSPEND_CLIENT, suspendClient);
  yield takeEvery(GET_CLIENT_DETAILS, getClientDetails);
  yield takeEvery(ADD_CLIENT_COMMENT.LOAD, addComment);
  yield takeEvery(CLIENT_ADD_CREDIT, addCredit);
  yield takeLatest(GET_CLIENTS_PAYMENTS, getClientsPayments);
  yield takeEvery(ADD_NEW_REQUEST, addNewRequest);
  yield takeLatest(GET_CLIENTS_REQUESTS_COUNT, getAllClientsRequestsCount);
}

export default clientsSaga;
