import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { DASHBOARD } from './actions';

const {
  GET_REQUESTS_COUNTS,
  GET_REQUESTS_COUNTS_SUCCESS,
  GET_REQUESTS_COUNTS_FAIL,
  GET_REQUESTS_COST_STATISTICS,
  GET_REQUESTS_COST_STATISTICS_SUCCESS,
  GET_REQUESTS_COST_STATISTICS_FAIL,
  GET_WORKERS_HEAT_MAP,
  GET_WORKERS_HEAT_MAP_SUCCESS,
  GET_WORKERS_HEAT_MAP_FAIL,
  GET_REQUESTS_LOCATION,
  GET_REQUESTS_LOCATION_SUCCESS,
  GET_REQUESTS_LOCATION_FAIL,
  GET_WORKERS_RECHARGE_COST,
  GET_WORKERS_RECHARGE_COST_SUCCESS,
  GET_WORKERS_RECHARGE_COST_FAIL,
  GET_FINANCIAL_BY_CITIES,
  GET_FINANCIAL_BY_CITIES_SUCCESS,
  GET_FINANCIAL_BY_CITIES_FAIL,
  GET_FINANCIAL_BY_FIELDS,
  GET_FINANCIAL_BY_FIELDS_SUCCESS,
  GET_FINANCIAL_BY_FIELDS_FAIL,
  GET_WORKERS_CREDITS,
  GET_WORKERS_CREDITS_SUCCESS,
  GET_WORKERS_CREDITS_FAIL,
  GET_WORKERS_IN_EACH_FIELD,
  GET_WORKERS_IN_EACH_FIELD_SUCCESS,
  GET_WORKERS_IN_EACH_FIELD_FAIL,
  GET_WORKERS_IN_EACH_CITY,
  GET_WORKERS_IN_EACH_CITY_SUCCESS,
  GET_WORKERS_IN_EACH_CITY_FAIL,

  GET_USERS_FOR_EACH_TYPE,
  GET_USERS_FOR_EACH_TYPE_SUCCESS,
  GET_USERS_FOR_EACH_TYPE_FAIL,

  GET_GENERAL_STATISTICS,
  GET_GENERAL_STATISTICS_FAIL,
  GET_GENERAL_STATISTICS_SUCCESS,

  GET_CUSTOMER_REQUESTS_STATISTICS,
  GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS,
  GET_CUSTOMER_REQUESTS_STATISTICS_FAIL,

  GET_CUSTOMERS_CREDITS,
  GET_CUSTOMERS_CREDITS_SUCCESS,
  GET_CUSTOMERS_CREDITS_FAIL,

  GET_PAYMENT_METHODS_STATICS,
  GET_PAYMENT_METHODS_STATICS_SUCCESS,
  GET_PAYMENT_METHODS_STATICS_FAIL,

  GET_CANCELLATION_REASONS_STATICS,
  GET_CANCELLATION_REASONS_STATICS_SUCCESS,
  GET_CANCELLATION_REASONS_STATICS_FAIL,

  GET_DONE_REQUESTS_PER_FIELD_STATICS,
  GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS,
  GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL,

  GET_RECHARGE_METHODS_STATICS,
  GET_RECHARGE_METHODS_STATICS_SUCCESS,
  GET_RECHARGE_METHODS_STATICS_FAIL,

  GET_THE_MOST_WORKERS_HAS_TICKETS,
  GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS,
  GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL,

  GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES,
  GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS,
  GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL,
} = DASHBOARD;

function* getRequestsCounts(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { requests } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = requests.getRequestsCounts;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { requestsCounts, statsDaily },
    } = response;

    yield put({
      type: GET_REQUESTS_COUNTS_SUCCESS,
      requestsCounts,
      statsDaily,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_REQUESTS_COUNTS_FAIL,
      err: message,
    });
  }
}

function* getRequestsCostStatistics(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { requests } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = requests.getRequestsCostStatistics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { statistics: requestsCostStatistics },
    } = response;

    yield put({
      type: GET_REQUESTS_COST_STATISTICS_SUCCESS,
      requestsCostStatistics,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_REQUESTS_COST_STATISTICS_FAIL,
      err: message,
    });
  }
}

function* getWorkersHeatMap() {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = workers.locations;
  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: locations } = response;

    yield put({
      type: GET_WORKERS_HEAT_MAP_SUCCESS,
      workersLocation: locations,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKERS_HEAT_MAP_FAIL,
      err: message,
    });
  }
}

function* getRequestsLocation(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { requests } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = requests.locations;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: requestsLocation } = response;

    yield put({
      type: GET_REQUESTS_LOCATION_SUCCESS,
      requestsLocation,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_REQUESTS_LOCATION_FAIL,
      err: message,
    });
  }
}

function* getWorkersRechargeCost(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = workers.rechargeCost;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workersRechargeCost } = response;

    yield put({
      type: GET_WORKERS_RECHARGE_COST_SUCCESS,
      workersRechargeCost,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKERS_RECHARGE_COST_FAIL,
      err: message,
    });
  }
}

function* getFinancialByCities(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.financialByCities;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: financialByCities } = response;

    yield put({
      type: GET_FINANCIAL_BY_CITIES_SUCCESS,
      financialByCities,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_FINANCIAL_BY_CITIES_FAIL,
      err: message,
    });
  }
}

function* getFinancialByFields(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.financialByFields;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: financialByFields } = response;

    yield put({
      type: GET_FINANCIAL_BY_FIELDS_SUCCESS,
      financialByFields,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_FINANCIAL_BY_FIELDS_FAIL,
      err: message,
    });
  }
}

function* getWorkersCredits(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.workersCredits;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workersCredits } = response;

    yield put({
      type: GET_WORKERS_CREDITS_SUCCESS,
      workersCredits,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKERS_CREDITS_FAIL,
      err: message,
    });
  }
}

function* getWorkersInEachField(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = workers.workersInEachField;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: WorkersForEachField } = response;

    yield put({
      type: GET_WORKERS_IN_EACH_FIELD_SUCCESS,
      WorkersForEachField,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKERS_IN_EACH_FIELD_FAIL,
      err: message,
    });
  }
}

function* getWorkersInEachCity(payload) {
  const { applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = reports.cities;

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: WorkersForEachCity } = response;

    yield put({
      type: GET_WORKERS_IN_EACH_CITY_SUCCESS,
      WorkersForEachCity,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKERS_IN_EACH_CITY_FAIL,
      err: message,
    });
  }
}

function* getUsersForEachTypeCount(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = user.usersCounts;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: UsersForEachTypeCount } = response;

    yield put({
      type: GET_USERS_FOR_EACH_TYPE_SUCCESS,
      UsersForEachTypeCount,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_USERS_FOR_EACH_TYPE_FAIL,
      err: message,
    });
  }
}

function* getGeneralStatistics(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getGeneralStatistics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);
  if (Status.isSuccess(response.status)) {
    const { response: generalStatistics } = response;

    yield put({
      type: GET_GENERAL_STATISTICS_SUCCESS,
      generalStatistics,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_GENERAL_STATISTICS_FAIL,
      err: message,
    });
  }
}

function* getCustomerRequestsStatistics(payload) {
  const { options } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getCustomerRequests;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);
  if (Status.isSuccess(response.status)) {
    const { response: customerRequestsStatistics } = response;

    yield put({
      type: GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS,
      customerRequestsStatistics,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CUSTOMER_REQUESTS_STATISTICS_FAIL,
      err: message,
    });
  }
}

function* getCustomersCreditsCount(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { clients } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = clients.addedCredits;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: CustomersCreditsCount } = response;
    yield put({
      type: GET_CUSTOMERS_CREDITS_SUCCESS,
      CustomersCreditsCount,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CUSTOMERS_CREDITS_FAIL,
      err: message,
    });
  }
}

function* getPaymentMethodsStatics(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getPaymentMethodsStatics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { data },
    } = response;
    yield put({
      type: GET_PAYMENT_METHODS_STATICS_SUCCESS,
      paymentMethodsStatics: data,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_PAYMENT_METHODS_STATICS_FAIL,
      err: message,
    });
  }
}

function* getCancellationReasonStatics(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getCancellationReasonStatics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { data },
    } = response;
    yield put({
      type: GET_CANCELLATION_REASONS_STATICS_SUCCESS,
      cancellationReasonsStatics: data,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_CANCELLATION_REASONS_STATICS_FAIL,
      err: message,
    });
  }
}

function* getDoneRequestsPerFieldStatics(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getDoneRequestsPerFieldStatics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { data },
    } = response;
    yield put({
      type: GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS,
      doneRequestsPerFieldStatics: data,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL,
      err: message,
    });
  }
}

function* getRechargeMethodsStatics(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getRechargeMethodsStatics;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { data },
    } = response;
    yield put({
      type: GET_RECHARGE_METHODS_STATICS_SUCCESS,
      rechargeMethodsStatics: data,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_RECHARGE_METHODS_STATICS_FAIL,
      err: message,
    });
  }
}

function* getTheMostWorkersHasTickets(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getTheMostWorkersHasTickets;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: data ,
    } = response;
    yield put({
      type: GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS,
      theMostWorkersHasTickets: data,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL,
      err: message,
    });
  }
}

function* getWorkerDoneRequestToTicketsHasCloseFees(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.getWorkerDoneRequestToTicketsHasCloseFees;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: data ,
    } = response;
    yield put({
      type: GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS,
      workerDoneRequestToTicketsHasCloseFees: data,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL,
      err: message,
    });
  }
}

function* notificationsSaga() {
  yield takeEvery(GET_REQUESTS_COUNTS, getRequestsCounts);
  yield takeEvery(GET_REQUESTS_COST_STATISTICS, getRequestsCostStatistics);
  yield takeEvery(GET_WORKERS_HEAT_MAP, getWorkersHeatMap);
  yield takeEvery(GET_REQUESTS_LOCATION, getRequestsLocation);
  yield takeEvery(GET_WORKERS_RECHARGE_COST, getWorkersRechargeCost);
  yield takeEvery(GET_FINANCIAL_BY_CITIES, getFinancialByCities);
  yield takeEvery(GET_FINANCIAL_BY_FIELDS, getFinancialByFields);
  yield takeEvery(GET_WORKERS_CREDITS, getWorkersCredits);
  yield takeEvery(GET_WORKERS_IN_EACH_FIELD, getWorkersInEachField);
  yield takeEvery(GET_WORKERS_IN_EACH_CITY, getWorkersInEachCity);
  yield takeEvery(GET_USERS_FOR_EACH_TYPE, getUsersForEachTypeCount);
  yield takeEvery(GET_GENERAL_STATISTICS, getGeneralStatistics);
  yield takeEvery(GET_CUSTOMER_REQUESTS_STATISTICS, getCustomerRequestsStatistics);
  yield takeEvery(GET_CUSTOMERS_CREDITS, getCustomersCreditsCount);
  yield takeEvery(GET_PAYMENT_METHODS_STATICS, getPaymentMethodsStatics);
  yield takeEvery(GET_CANCELLATION_REASONS_STATICS, getCancellationReasonStatics);
  yield takeEvery(GET_DONE_REQUESTS_PER_FIELD_STATICS, getDoneRequestsPerFieldStatics);
  yield takeEvery(GET_RECHARGE_METHODS_STATICS, getRechargeMethodsStatics);
  yield takeEvery(GET_THE_MOST_WORKERS_HAS_TICKETS, getTheMostWorkersHasTickets);
  yield takeEvery(GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES, getWorkerDoneRequestToTicketsHasCloseFees);
}

export default notificationsSaga;
