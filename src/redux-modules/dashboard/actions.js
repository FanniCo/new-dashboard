export const DASHBOARD = {
  GET_REQUESTS_COUNTS: 'GET_REQUESTS_COUNTS',
  GET_REQUESTS_COUNTS_SUCCESS: 'GET_REQUESTS_COUNTS_SUCCESS',
  GET_REQUESTS_COUNTS_FAIL: 'GET_REQUESTS_COUNTS_FAIL',
  GET_REQUESTS_COST_STATISTICS: 'GET_REQUESTS_COST_STATISTICS',
  GET_REQUESTS_COST_STATISTICS_SUCCESS: 'GET_REQUESTS_COST_STATISTICS_SUCCESS',
  GET_REQUESTS_COST_STATISTICS_FAIL: 'GET_REQUESTS_COST_STATISTICS_FAIL',
  GET_WORKERS_HEAT_MAP: 'GET_WORKERS_HEAT_MAP',
  GET_WORKERS_HEAT_MAP_SUCCESS: 'GET_WORKERS_HEAT_MAP_SUCCESS',
  GET_WORKERS_HEAT_MAP_FAIL: 'GET_WORKERS_HEAT_MAP_FAIL',
  GET_REQUESTS_LOCATION: 'GET_REQUESTS_LOCATION',
  GET_REQUESTS_LOCATION_SUCCESS: 'GET_REQUESTS_LOCATION_SUCCESS',
  GET_REQUESTS_LOCATION_FAIL: 'GET_REQUESTS_LOCATION_FAIL',
  GET_WORKERS_RECHARGE_COST: 'GET_WORKERS_RECHARGE_COST',
  GET_WORKERS_RECHARGE_COST_SUCCESS: 'GET_WORKERS_RECHARGE_COST_SUCCESS',
  GET_WORKERS_RECHARGE_COST_FAIL: 'GET_WORKERS_RECHARGE_COST_FAIL',
  GET_FINANCIAL_BY_CITIES: 'GET_FINANCIAL_BY_CITIES',
  GET_FINANCIAL_BY_CITIES_SUCCESS: 'GET_FINANCIAL_BY_CITIES_SUCCESS',
  GET_FINANCIAL_BY_CITIES_FAIL: 'GET_FINANCIAL_BY_CITIES_FAIL',
  GET_FINANCIAL_BY_FIELDS: 'GET_FINANCIAL_BY_FIELDS',
  GET_FINANCIAL_BY_FIELDS_SUCCESS: 'GET_FINANCIAL_BY_FIELDS_SUCCESS',
  GET_FINANCIAL_BY_FIELDS_FAIL: 'GET_FINANCIAL_BY_FIELDS_FAIL',
  GET_WORKERS_CREDITS: 'GET_WORKERS_CREDITS',
  GET_WORKERS_CREDITS_SUCCESS: 'GET_WORKERS_CREDITS_SUCCESS',
  GET_WORKERS_CREDITS_FAIL: 'GET_WORKERS_CREDITS_FAIL',
  GET_WORKERS_IN_EACH_FIELD: 'GET_WORKERS_IN_EACH_FIELD',
  GET_WORKERS_IN_EACH_FIELD_SUCCESS: 'GET_WORKERS_IN_EACH_FIELD_SUCCESS',
  GET_WORKERS_IN_EACH_FIELD_FAIL: 'GET_WORKERS_IN_EACH_FIELD_FAIL',
  GET_WORKERS_IN_EACH_CITY: 'GET_WORKERS_IN_EACH_CITY',
  GET_WORKERS_IN_EACH_CITY_SUCCESS: 'GET_WORKERS_IN_EACH_CITY_SUCCESS',
  GET_WORKERS_IN_EACH_CITY_FAIL: 'GET_WORKERS_IN_EACH_CITY_FAIL',
  GET_USERS_FOR_EACH_TYPE: 'GET_USERS_FOR_EACH_TYPE',
  GET_USERS_FOR_EACH_TYPE_SUCCESS: 'GET_USERS_FOR_EACH_TYPE_SUCCESS',
  GET_USERS_FOR_EACH_TYPE_FAIL: 'GET_USERS_FOR_EACH_TYPE_FAIL',
  GET_GENERAL_STATISTICS: 'GET_GENERAL_STATISTICS',
  GET_GENERAL_STATISTICS_SUCCESS: 'GET_GENERAL_STATISTICS_SUCCESS',
  GET_GENERAL_STATISTICS_FAIL: 'GET_GENERAL_STATISTICS_FAIL',

  GET_CUSTOMER_REQUESTS_STATISTICS: 'GET_CUSTOMER_REQUESTS_STATISTICS',
  GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS: 'GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS',
  GET_CUSTOMER_REQUESTS_STATISTICS_FAIL: 'GET_CUSTOMER_REQUESTS_STATISTICS_FAIL',

  GET_CUSTOMERS_CREDITS: 'GET_CUSTOMERS_CREDITS',
  GET_CUSTOMERS_CREDITS_SUCCESS: 'GET_CUSTOMERS_CREDITS_SUCCESS',
  GET_CUSTOMERS_CREDITS_FAIL: 'GET_CUSTOMERS_CREDITS_FAIL',

  GET_PAYMENT_METHODS_STATICS: 'GET_PAYMENT_METHODS_STATICS',
  GET_PAYMENT_METHODS_STATICS_SUCCESS: 'GET_PAYMENT_METHODS_STATICS_SUCCESS',
  GET_PAYMENT_METHODS_STATICS_FAIL: 'GET_PAYMENT_METHODS_STATICS_FAIL',

  GET_CANCELLATION_REASONS_STATICS: 'GET_CANCELLATION_REASONS_STATICS',
  GET_CANCELLATION_REASONS_STATICS_SUCCESS: 'GET_CANCELLATION_REASONS_STATICS_SUCCESS',
  GET_CANCELLATION_REASONS_STATICS_FAIL: 'GET_CANCELLATION_REASONS_STATICS_FAIL',

  GET_DONE_REQUESTS_PER_FIELD_STATICS: 'GET_DONE_REQUESTS_PER_FIELD_STATICS',
  GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS: 'GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS',
  GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL: 'GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL',

  GET_RECHARGE_METHODS_STATICS: 'GET_RECHARGE_METHODS_STATICS',
  GET_RECHARGE_METHODS_STATICS_SUCCESS: 'GET_RECHARGE_METHODS_STATICS_SUCCESS',
  GET_RECHARGE_METHODS_STATICS_FAIL: 'GET_RECHARGE_METHODS_STATICS_FAIL',

  GET_THE_MOST_WORKERS_HAS_TICKETS: 'GET_THE_MOST_WORKERS_HAS_TICKETS',
  GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS: 'GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS',
  GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL: 'GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL',


  GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES: 'GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES',
  GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS: 'GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS',
  GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL: 'GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL',
};

export const getRequestsCounts = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_REQUESTS_COUNTS,
  options,
  applyNewFilter,
});

export const getRequestsCountsSuccess = (requestsCounts, statsDaily, applyNewFilter: false) => ({
  type: DASHBOARD.GET_REQUESTS_COUNTS_SUCCESS,
  requestsCounts,
  statsDaily,
  applyNewFilter,
});

export const getRequestsCountsFail = err => ({
  type: DASHBOARD.GET_REQUESTS_COUNTS_FAIL,
  err,
});

export const getRequestsCostStatistics = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_REQUESTS_COST_STATISTICS,
  options,
  applyNewFilter,
});

export const getRequestsCostStatisticsSuccess = (requestCostStatistics, applyNewFilter: false) => ({
  type: DASHBOARD.GET_REQUESTS_COST_STATISTICS_SUCCESS,
  requestCostStatistics,
  applyNewFilter,
});

export const getRequestsCostStatisticsFail = err => ({
  type: DASHBOARD.GET_REQUESTS_COST_STATISTICS_FAIL,
  err,
});

export const getWorkersHeatMap = () => ({
  type: DASHBOARD.GET_WORKERS_HEAT_MAP,
});

export const getWorkersHeatMapSuccess = workersLocation => ({
  type: DASHBOARD.GET_WORKERS_HEAT_MAP_SUCCESS,
  workersLocation,
});

export const getWorkersHeatMapFail = err => ({
  type: DASHBOARD.GET_WORKERS_HEAT_MAP_FAIL,
  err,
});

export const getRequestsLocation = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_REQUESTS_LOCATION,
  options,
  applyNewFilter,
});

export const getRequestsLocationSuccess = (requestsLocation, applyNewFilter: false) => ({
  type: DASHBOARD.GET_REQUESTS_LOCATION_SUCCESS,
  requestsLocation,
  applyNewFilter,
});

export const getRequestsLocationFail = err => ({
  type: DASHBOARD.GET_REQUESTS_LOCATION_FAIL,
  err,
});

export const getWorkerRechargeCost = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_RECHARGE_COST,
  options,
  applyNewFilter,
});

export const getWorkerRechargeCostSuccess = (workersRechargeCost, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_RECHARGE_COST_SUCCESS,
  workersRechargeCost,
  applyNewFilter,
});

export const getWorkerRechargeCostFail = err => ({
  type: DASHBOARD.GET_WORKERS_RECHARGE_COST_FAIL,
  err,
});

export const getFinancialByCities = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_FINANCIAL_BY_CITIES,
  options,
  applyNewFilter,
});

export const getFinancialByCitiesSuccess = (financialByCities, applyNewFilter: false) => ({
  type: DASHBOARD.GET_FINANCIAL_BY_CITIES_SUCCESS,
  financialByCities,
  applyNewFilter,
});

export const getFinancialByCitiesFail = err => ({
  type: DASHBOARD.GET_FINANCIAL_BY_CITIES_FAIL,
  err,
});

export const getFinancialByFields = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_FINANCIAL_BY_FIELDS,
  options,
  applyNewFilter,
});

export const getFinancialByFieldsSuccess = (financialByFields, applyNewFilter: false) => ({
  type: DASHBOARD.GET_FINANCIAL_BY_FIELDS_SUCCESS,
  financialByFields,
  applyNewFilter,
});

export const getFinancialByFieldsFail = err => ({
  type: DASHBOARD.GET_FINANCIAL_BY_FIELDS_FAIL,
  err,
});

export const getWorkersCredits = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_CREDITS,
  options,
  applyNewFilter,
});

export const getWorkersCreditsSuccess = (workersCredits, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_CREDITS_SUCCESS,
  workersCredits,
  applyNewFilter,
});

export const getWorkersCreditsFail = err => ({
  type: DASHBOARD.GET_WORKERS_CREDITS_FAIL,
  err,
});

export const getWorkersInEachField = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_IN_EACH_FIELD,
  options,
  applyNewFilter,
});

export const getWorkersInEachFieldSuccess = (WorkersForEachField, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_IN_EACH_FIELD_SUCCESS,
  WorkersForEachField,
  applyNewFilter,
});

export const getWorkersInEachFieldFail = err => ({
  type: DASHBOARD.GET_WORKERS_IN_EACH_FIELD_FAIL,
  err,
});

export const getWorkersInEachCity = (applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_IN_EACH_CITY,
  applyNewFilter,
});

export const getWorkersInEachCitySuccess = (WorkersForEachCity, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKERS_IN_EACH_CITY_SUCCESS,
  WorkersForEachCity,
  applyNewFilter,
});

export const getWorkersInEachCityFail = err => ({
  type: DASHBOARD.GET_WORKERS_IN_EACH_CITY_FAIL,
  err,
});

export const getUsersForEachTypeCount = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_USERS_FOR_EACH_TYPE,
  options,
  applyNewFilter,
});

export const getUsersForEachTypeCountSuccess = (UsersForEachTypeCount, applyNewFilter: false) => ({
  type: DASHBOARD.GET_USERS_FOR_EACH_TYPE_SUCCESS,
  UsersForEachTypeCount,
  applyNewFilter,
});

export const getUsersForEachTypeCountFail = err => ({
  type: DASHBOARD.GET_USERS_FOR_EACH_TYPE_FAIL,
  err,
});

export const getGeneralStatistics = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_GENERAL_STATISTICS,
  options,
  applyNewFilter,
});

export const getGeneralStatisticsSuccess = (generalStatistics, applyNewFilter: false) => ({
  type: DASHBOARD.GET_GENERAL_STATISTICS_SUCCESS,
  generalStatistics,
  applyNewFilter,
});

export const getGeneralStatisticsFail = err => ({
  type: DASHBOARD.GET_GENERAL_STATISTICS_FAIL,
  err,
});

export const getCustomerRequestsStatistics = options => ({
  type: DASHBOARD.GET_CUSTOMER_REQUESTS_STATISTICS,
  options,
});

export const getCustomerRequestsStatisticsSuccess = customerRequestsStatistics => ({
  type: DASHBOARD.GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS,
  customerRequestsStatistics,
});

export const getCustomerRequestsStatisticsFail = err => ({
  type: DASHBOARD.GET_CUSTOMER_REQUESTS_STATISTICS_FAIL,
  err,
});

export const getCustomersCreditsCount = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_CUSTOMERS_CREDITS,
  options,
  applyNewFilter,
});

export const getCustomersCreditsCountSuccess = (CustomersCreditsCount, applyNewFilter: false) => ({
  type: DASHBOARD.GET_CUSTOMERS_CREDITS_SUCCESS,
  CustomersCreditsCount,
  applyNewFilter,
});

export const getCustomersCreditsCountFail = err => ({
  type: DASHBOARD.GET_CUSTOMERS_CREDITS_FAIL,
  err,
});

export const getPaymentMethodsStatics = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_PAYMENT_METHODS_STATICS,
  options,
  applyNewFilter,
});

export const getPaymentMethodsStaticsSuccess = (paymentMethodsStatics, applyNewFilter: false) => ({
  type: DASHBOARD.GET_PAYMENT_METHODS_STATICS_SUCCESS,
  paymentMethodsStatics,
  applyNewFilter,
});

export const getPaymentMethodsStaticsFail = err => ({
  type: DASHBOARD.GET_PAYMENT_METHODS_STATICS_FAIL,
  err,
});

export const getCancellationReasonsStatics = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_CANCELLATION_REASONS_STATICS,
  options,
  applyNewFilter,
});

export const getCancellationReasonsStaticsSuccess = (
  cancellationReasonsStatics,
  applyNewFilter: false,
) => ({
  type: DASHBOARD.GET_CANCELLATION_REASONS_STATICS_SUCCESS,
  cancellationReasonsStatics,
  applyNewFilter,
});

export const getCancellationReasonsStaticsFail = err => ({
  type: DASHBOARD.GET_CANCELLATION_REASONS_STATICS_FAIL,
  err,
});

export const getDoneRequestsPerFieldStatics = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_DONE_REQUESTS_PER_FIELD_STATICS,
  options,
  applyNewFilter,
});

export const getDoneRequestsPerFieldStaticsSuccess = (
  doneRequestsPerFieldStatics,
  applyNewFilter: false,
) => ({
  type: DASHBOARD.GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS,
  doneRequestsPerFieldStatics,
  applyNewFilter,
});

export const getDoneRequestsPerFieldStaticsFail = err => ({
  type: DASHBOARD.GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL,
  err,
});

export const getRechargeMethodsStatics = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_RECHARGE_METHODS_STATICS,
  options,
  applyNewFilter,
});

export const getRechargeMethodsStaticsSuccess = (
  rechargeMethodsStatics,
  applyNewFilter: false,
) => ({
  type: DASHBOARD.GET_RECHARGE_METHODS_STATICS_SUCCESS,
  rechargeMethodsStatics,
  applyNewFilter,
});

export const getRechargeMethodsStaticsFail = err => ({
  type: DASHBOARD.GET_RECHARGE_METHODS_STATICS_FAIL,
  err,
});

export const getTheMostWorkersHasTickets = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_THE_MOST_WORKERS_HAS_TICKETS,
  options,
  applyNewFilter,
});

export const getTheMostWorkersHasTicketsSuccess = (theMostWorkersHasTickets, applyNewFilter: false) => ({
  type: DASHBOARD.GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS,
  theMostWorkersHasTickets,
  applyNewFilter,
});

export const getTheMostWorkersHasTicketsFail = err => ({
  type: DASHBOARD.GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL,
  err,
});

export const getWorkerDoneRequestToTicketsHasCloseFees = (options, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES,
  options,
  applyNewFilter,
});

export const getWorkerDoneRequestToTicketsHasCloseFeesSuccess = (theMostWorkersHasTickets, applyNewFilter: false) => ({
  type: DASHBOARD.GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS,
  theMostWorkersHasTickets,
  applyNewFilter,
});

export const getWorkerDoneRequestToTicketsHasCloseFeesFail = err => ({
  type: DASHBOARD.GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL,
  err,
});
