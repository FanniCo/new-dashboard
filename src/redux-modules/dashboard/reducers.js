import { DASHBOARD } from './actions';
import {unionBy} from "lodash";

const initialState = {
  requestsCounts: undefined,
  requestsCostStatistics: undefined,
  statsDaily: undefined,
  applyNewFilter: false,
  getRequestsCounts: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getRequestsCostStatistics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getWorkersHeatMap: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  workersLocation: undefined,
  getRequestsLocation: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  requestsLocation: undefined,
  getWorkersRechargeCost: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  workersRechargeCost: undefined,
  getFinancialByCities: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  financialByCities: undefined,
  getFinancialByFields: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  financialByFields: undefined,
  getWorkersCredits: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  workersCredits: undefined,
  getWorkersForEachField: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  WorkersForEachField: undefined,
  getWorkersForEachCity: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  WorkersForEachCity: undefined,
  getUsersForEachTypeCount: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  UsersForEachTypeCount: undefined,
  generalStatistics: undefined,
  getGeneralStatistics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  customerRequestsStatistics: undefined,
  getCustomerRequestsStatistics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getCustomersCreditsCount: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  CustomersCreditsCount: undefined,

  getPaymentMethodsStatics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  paymentMethodsStatics: undefined,

  getCancellationReasonsStatics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  cancellationReasonsStatics: undefined,

  getDoneRequestsPerFieldStatics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  doneRequestsPerFieldStatics: undefined,

  getRechargeMethodsStatics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  rechargeMethodsStatics: undefined,

  getTheMostWorkersHasTickets: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  theMostWorkersHasTickets: [],

  getWorkerDoneRequestToTicketsHasCloseFees: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  hasMoreWorkerDoneRequestToTicketsHasCloseFees: true,
  workerDoneRequestToTicketsHasCloseFees: [],
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_REQUESTS_COUNTS,
    GET_REQUESTS_COUNTS_SUCCESS,
    GET_REQUESTS_COUNTS_FAIL,
    GET_REQUESTS_COST_STATISTICS,
    GET_REQUESTS_COST_STATISTICS_SUCCESS,
    GET_REQUESTS_COST_STATISTICS_FAIL,
    GET_WORKERS_HEAT_MAP,
    GET_WORKERS_HEAT_MAP_SUCCESS,
    GET_WORKERS_HEAT_MAP_FAIL,
    GET_REQUESTS_LOCATION,
    GET_REQUESTS_LOCATION_SUCCESS,
    GET_REQUESTS_LOCATION_FAIL,
    GET_WORKERS_RECHARGE_COST,
    GET_WORKERS_RECHARGE_COST_SUCCESS,
    GET_WORKERS_RECHARGE_COST_FAIL,
    GET_FINANCIAL_BY_CITIES,
    GET_FINANCIAL_BY_CITIES_SUCCESS,
    GET_FINANCIAL_BY_CITIES_FAIL,
    GET_FINANCIAL_BY_FIELDS,
    GET_FINANCIAL_BY_FIELDS_SUCCESS,
    GET_FINANCIAL_BY_FIELDS_FAIL,
    GET_WORKERS_CREDITS,
    GET_WORKERS_CREDITS_SUCCESS,
    GET_WORKERS_CREDITS_FAIL,
    GET_WORKERS_IN_EACH_FIELD,
    GET_WORKERS_IN_EACH_FIELD_SUCCESS,
    GET_WORKERS_IN_EACH_FIELD_FAIL,
    GET_WORKERS_IN_EACH_CITY,
    GET_WORKERS_IN_EACH_CITY_SUCCESS,
    GET_WORKERS_IN_EACH_CITY_FAIL,
    GET_USERS_FOR_EACH_TYPE,
    GET_USERS_FOR_EACH_TYPE_SUCCESS,
    GET_USERS_FOR_EACH_TYPE_FAIL,
    GET_GENERAL_STATISTICS,
    GET_GENERAL_STATISTICS_FAIL,
    GET_GENERAL_STATISTICS_SUCCESS,

    GET_CUSTOMER_REQUESTS_STATISTICS,
    GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS,
    GET_CUSTOMER_REQUESTS_STATISTICS_FAIL,

    GET_CUSTOMERS_CREDITS,
    GET_CUSTOMERS_CREDITS_SUCCESS,
    GET_CUSTOMERS_CREDITS_FAIL,

    GET_PAYMENT_METHODS_STATICS,
    GET_PAYMENT_METHODS_STATICS_SUCCESS,
    GET_PAYMENT_METHODS_STATICS_FAIL,

    GET_CANCELLATION_REASONS_STATICS,
    GET_CANCELLATION_REASONS_STATICS_SUCCESS,
    GET_CANCELLATION_REASONS_STATICS_FAIL,

    GET_DONE_REQUESTS_PER_FIELD_STATICS,
    GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS,
    GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL,

    GET_RECHARGE_METHODS_STATICS,
    GET_RECHARGE_METHODS_STATICS_SUCCESS,
    GET_RECHARGE_METHODS_STATICS_FAIL,

    GET_THE_MOST_WORKERS_HAS_TICKETS,
    GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS,
    GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL,

    GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES,
    GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS,
    GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL,
  } = DASHBOARD;
  const {
    requestsCounts,
    statsDaily,
    generalStatistics,
    customerRequestsStatistics,
    requestsCostStatistics,
    workersLocation,
    requestsLocation,
    workersRechargeCost,
    financialByCities,
    financialByFields,
    workersCredits,
    WorkersForEachField,
    WorkersForEachCity,
    UsersForEachTypeCount,
    CustomersCreditsCount,
    paymentMethodsStatics,
    cancellationReasonsStatics,
    doneRequestsPerFieldStatics,
    rechargeMethodsStatics,
    theMostWorkersHasTickets,
    workerDoneRequestToTicketsHasCloseFees,
    applyNewFilter,
    err,
  } = payload;

  const {
    workerDoneRequestToTicketsHasCloseFees: workerDoneRequestToTicketsHasCloseFeesState,
  } = state;

  switch (type) {
    case GET_REQUESTS_COUNTS: {
      return {
        ...state,
        getRequestsCounts: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_COUNTS_SUCCESS: {
      return {
        ...state,
        requestsCounts,
        statsDaily,
        getRequestsCounts: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_COUNTS_FAIL: {
      return {
        ...state,
        getRequestsCounts: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_REQUESTS_COST_STATISTICS: {
      return {
        ...state,
        getRequestsCostStatistics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_COST_STATISTICS_SUCCESS: {
      return {
        ...state,
        requestsCostStatistics,
        getRequestsCostStatistics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_COST_STATISTICS_FAIL: {
      return {
        ...state,
        getRequestsCostStatistics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_WORKERS_HEAT_MAP: {
      return {
        ...state,
        getWorkersHeatMap: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_HEAT_MAP_SUCCESS: {
      return {
        ...state,
        workersLocation,
        getWorkersHeatMap: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_HEAT_MAP_FAIL: {
      return {
        ...state,
        getWorkersHeatMap: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_REQUESTS_LOCATION: {
      return {
        ...state,
        getRequestsLocation: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_LOCATION_SUCCESS: {
      return {
        ...state,
        requestsLocation,
        getRequestsLocation: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_REQUESTS_LOCATION_FAIL: {
      return {
        ...state,
        getRequestsLocation: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_WORKERS_RECHARGE_COST: {
      return {
        ...state,
        getWorkersRechargeCost: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_RECHARGE_COST_SUCCESS: {
      return {
        ...state,
        workersRechargeCost,
        getWorkersRechargeCost: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_RECHARGE_COST_FAIL: {
      return {
        ...state,
        getWorkersRechargeCost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_FINANCIAL_BY_CITIES: {
      return {
        ...state,
        getFinancialByCities: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_FINANCIAL_BY_CITIES_SUCCESS: {
      return {
        ...state,
        financialByCities,
        getFinancialByCities: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_FINANCIAL_BY_CITIES_FAIL: {
      return {
        ...state,
        getFinancialByCities: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_FINANCIAL_BY_FIELDS: {
      return {
        ...state,
        getFinancialByFields: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_FINANCIAL_BY_FIELDS_SUCCESS: {
      return {
        ...state,
        financialByFields,
        getFinancialByFields: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_FINANCIAL_BY_FIELDS_FAIL: {
      return {
        ...state,
        getFinancialByFields: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_WORKERS_CREDITS: {
      return {
        ...state,
        getWorkersCredits: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_CREDITS_SUCCESS: {
      return {
        ...state,
        workersCredits,
        getWorkersCredits: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_CREDITS_FAIL: {
      return {
        ...state,
        getWorkersCredits: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_WORKERS_IN_EACH_FIELD: {
      return {
        ...state,
        getWorkersForEachField: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_IN_EACH_FIELD_SUCCESS: {
      return {
        ...state,
        WorkersForEachField,
        getWorkersForEachField: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_IN_EACH_FIELD_FAIL: {
      return {
        ...state,
        getWorkersForEachField: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_WORKERS_IN_EACH_CITY: {
      return {
        ...state,
        getWorkersForEachCity: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_IN_EACH_CITY_SUCCESS: {
      return {
        ...state,
        WorkersForEachCity,
        getWorkersForEachCity: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKERS_IN_EACH_CITY_FAIL: {
      return {
        ...state,
        getWorkersForEachCity: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_USERS_FOR_EACH_TYPE: {
      return {
        ...state,
        getUsersForEachTypeCount: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_USERS_FOR_EACH_TYPE_SUCCESS: {
      return {
        ...state,
        UsersForEachTypeCount,
        getUsersForEachTypeCount: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_USERS_FOR_EACH_TYPE_FAIL: {
      return {
        ...state,
        getUsersForEachTypeCount: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_GENERAL_STATISTICS: {
      return {
        ...state,
        getGeneralStatistics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_GENERAL_STATISTICS_SUCCESS: {
      return {
        ...state,
        generalStatistics,
        getGeneralStatistics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_GENERAL_STATISTICS_FAIL: {
      return {
        ...state,
        getGeneralStatistics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_CUSTOMER_REQUESTS_STATISTICS: {
      return {
        ...state,
        getCustomerRequestsStatistics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CUSTOMER_REQUESTS_STATISTICS_SUCCESS: {
      return {
        ...state,
        customerRequestsStatistics,
        getCustomerRequestsStatistics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CUSTOMER_REQUESTS_STATISTICS_FAIL: {
      return {
        ...state,
        getCustomerRequestsStatistics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_CUSTOMERS_CREDITS: {
      return {
        ...state,
        getCustomersCreditsCount: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CUSTOMERS_CREDITS_SUCCESS: {
      return {
        ...state,
        CustomersCreditsCount,
        getCustomersCreditsCount: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CUSTOMERS_CREDITS_FAIL: {
      return {
        ...state,
        getCustomersCreditsCount: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_PAYMENT_METHODS_STATICS: {
      return {
        ...state,
        getPaymentMethodsStatics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_PAYMENT_METHODS_STATICS_SUCCESS: {
      return {
        ...state,
        paymentMethodsStatics,
        getPaymentMethodsStatics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }

    case GET_PAYMENT_METHODS_STATICS_FAIL: {
      return {
        ...state,
        getPaymentMethodsStatics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_CANCELLATION_REASONS_STATICS: {
      return {
        ...state,
        getCancellationReasonsStatics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_CANCELLATION_REASONS_STATICS_SUCCESS: {
      return {
        ...state,
        cancellationReasonsStatics,
        getCancellationReasonsStatics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }

    case GET_CANCELLATION_REASONS_STATICS_FAIL: {
      return {
        ...state,
        getCancellationReasonsStatics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_DONE_REQUESTS_PER_FIELD_STATICS: {
      return {
        ...state,
        getDoneRequestsPerFieldStatics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_DONE_REQUESTS_PER_FIELD_STATICS_SUCCESS: {
      return {
        ...state,
        doneRequestsPerFieldStatics,
        getDoneRequestsPerFieldStatics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }

    case GET_DONE_REQUESTS_PER_FIELD_STATICS_FAIL: {
      return {
        ...state,
        getDoneRequestsPerFieldStatics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_RECHARGE_METHODS_STATICS: {
      return {
        ...state,
        getRechargeMethodsStatics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_RECHARGE_METHODS_STATICS_SUCCESS: {
      return {
        ...state,
        rechargeMethodsStatics,
        getRechargeMethodsStatics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_RECHARGE_METHODS_STATICS_FAIL: {
      return {
        ...state,
        getRechargeMethodsStatics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case GET_THE_MOST_WORKERS_HAS_TICKETS: {
      return {
        ...state,
        getTheMostWorkersHasTickets: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_THE_MOST_WORKERS_HAS_TICKETS_SUCCESS: {
      return {
        ...state,
        theMostWorkersHasTickets,
        getTheMostWorkersHasTickets: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_THE_MOST_WORKERS_HAS_TICKETS_FAIL: {
      return {
        ...state,
        getTheMostWorkersHasTickets: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }


    case GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES: {
      return {
        ...state,
        applyNewFilter,
        getWorkerDoneRequestToTicketsHasCloseFees: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_SUCCESS: {
      return {
        ...state,
        workerDoneRequestToTicketsHasCloseFees:
          workerDoneRequestToTicketsHasCloseFeesState && !applyNewFilter ? unionBy(workerDoneRequestToTicketsHasCloseFeesState, workerDoneRequestToTicketsHasCloseFees, '_id'): workerDoneRequestToTicketsHasCloseFees,
        hasMoreWorkerDoneRequestToTicketsHasCloseFees: workerDoneRequestToTicketsHasCloseFees.length === 20,
        getWorkerDoneRequestToTicketsHasCloseFees: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_WORKER_DONE_REQUEST_TO_TICKETS_HAS_CLOSE_FEES_FAIL: {
      return {
        ...state,
        getWorkerDoneRequestToTicketsHasCloseFees: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    default:
      return state;
  }
};
