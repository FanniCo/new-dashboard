import { combineReducers } from 'redux';
import statics from './statics/reducers';
import configs from './configs/reducers';
import dashboard from './dashboard/reducers';
import user from './user/reducers';
import requests from './requests/reducers';
import tickets from './tickets/reducers';
import clients from './clients/reducers';
import workers from './workers/reducers';
import transactions from './transactions/reducers';
import reviews from './reviews/reducers';
import promoCodes from './promoCodes/reducers';
import notifications from './notifications/reducers';
import messages from './messages/reducers';
import activitiesLogs from './activitiesLogs/reducers';
import vendors from './vendors/reducers';
import workersAds from './workersAds/reducers';
import customersAds from './customersAds/reducers';
import offers from './offers/reducers';
import posts from './posts/reducers';
import commissions from './commissions/reducers';
import adminsActivities from './adminsActivities/reducers';
import vouchers from './vouchers/reducers';
import appConfigs from './appConfigs/reducers';
import loyaltyPointsPartners from './loyaltyPointsPartners/reducers';
import rechargeAwards from './rechargeAwards/reducers';

export default combineReducers({
  statics,
  configs,
  dashboard,
  user,
  requests,
  tickets,
  clients,
  workers,
  transactions,
  reviews,
  promoCodes,
  notifications,
  messages,
  activitiesLogs,
  vendors,
  workersAds,
  customersAds,
  offers,
  posts,
  commissions,
  adminsActivities,
  vouchers,
  appConfigs,
  loyaltyPointsPartners,
  rechargeAwards,
});
