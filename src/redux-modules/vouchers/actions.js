export const VOUCHERS = {
  GET_ALL_VOUCHERS: 'GET_ALL_VOUCHERS',
  GET_ALL_VOUCHERS_FAIL: 'GET_ALL_VOUCHERS_FAIL',
  GET_ALL_VOUCHERS_SUCCESS: 'GET_ALL_VOUCHERS_SUCCESS',

  ADD_VOUCHER: 'ADD_VOUCHER',
  ADD_VOUCHER_FAIL: 'ADD_VOUCHER_FAIL',
  ADD_VOUCHER_RESET: 'ADD_VOUCHER_RESET',
  ADD_VOUCHER_SUCCESS: 'ADD_VOUCHER_SUCCESS',

  DELETE_VOUCHER: 'DELETE_VOUCHER',
  DELETE_VOUCHER_FAIL: 'DELETE_VOUCHER_FAIL',
  DELETE_VOUCHER_SUCCESS: 'DELETE_VOUCHER_SUCCESS',

  UPDATE_VOUCHER: 'UPDATE_VOUCHER',
  UPDATE_VOUCHER_FAIL: 'UPDATE_VOUCHER_FAIL',
  UPDATE_VOUCHER_SUCCESS: 'UPDATE_VOUCHER_SUCCESS',

  TOGGLE_ADD_VOUCHER_MODAL: 'TOGGLE_ADD_VOUCHER_MODAL',
  TOGGLE_EDIT_VOUCHER_MODAL: 'TOGGLE_EDIT_VOUCHER_MODAL',
};

export const getAllVouchers = (options, applyNewFilter = false) => ({
  type: VOUCHERS.GET_ALL_VOUCHERS,
  options,
  applyNewFilter,
});

export const getAllVouchersFail = err => ({
  type: VOUCHERS.GET_ALL_VOUCHERS_FAIL,
  err,
});

export const getAllVouchersSuccess = (vouchers, applyNewFilter = false) => ({
  type: VOUCHERS.GET_ALL_VOUCHERS_SUCCESS,
  vouchers,
  applyNewFilter,
});

// ------------------------------------------------------

export const addVoucher = voucherInfo => ({
  type: VOUCHERS.ADD_VOUCHER,
  voucherInfo,
});

export const addVoucherFail = err => ({
  type: VOUCHERS.ADD_WORKER_FAIL,
  err,
});

export const addVoucherReset = () => ({
  type: VOUCHERS.ADD_VOUCHER_RESET,
});

export const addVoucherSuccess = voucherInfo => ({
  type: VOUCHERS.ADD_VOUCHER_SUCCESS,
  voucherInfo,
});

// ------------------------------------------------------

export const updateVoucher = (voucherInfo, voucherId, index) => ({
  type: VOUCHERS.UPDATE_VOUCHER,
  voucherInfo,
  voucherId,
  index,
});

export const updateVoucherFail = err => ({
  type: VOUCHERS.UPDATE_VOUCHER_FAIL,
  err,
});

export const updateVoucherSuccess = (voucherInfo, index) => ({
  type: VOUCHERS.UPDATE_VOUCHER_SUCCESS,
  voucherInfo,
  index,
});

// ------------------------------------------------------

export const deleteVoucher = (voucherId, index) => ({
  type: VOUCHERS.DELETE_VOUCHER,
  voucherId,
  index,
});

export const deleteVoucherFail = err => ({
  type: VOUCHERS.DELETE_VOUCHER_FAIL,
  err,
});

export const deleteVoucherSuccess = index => ({
  type: VOUCHERS.DELETE_VOUCHER_SUCCESS,
  index,
});

// ------------------------------------------------------

export const toggleAddVoucherModal = () => ({
  type: VOUCHERS.TOGGLE_ADD_VOUCHER_MODAL,
});

export const toggleEditVoucherModal = () => ({
  type: VOUCHERS.TOGGLE_EDIT_VOUCHER_MODAL,
});

// ------------------------------------------------------
