import { unionBy } from 'lodash';
import ROUTES from 'routes';
import update from 'immutability-helper';
import { VOUCHERS } from './actions';

const initialState = {
  vouchersList: undefined,
  hasMoreVouchers: true,
  applyNewFilter: false,
  confirmModal: { isOpen: false },
  addVoucherModal: { isOpen: false },
  editVoucherModal: { isOpen: false },
  getAllVouchers: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addVoucher: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editVoucher: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteVoucher: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_VOUCHERS,
    GET_ALL_VOUCHERS_FAIL,
    GET_ALL_VOUCHERS_SUCCESS,
    ADD_VOUCHER,
    ADD_VOUCHER_FAIL,
    ADD_VOUCHER_RESET,
    ADD_VOUCHER_SUCCESS,
    UPDATE_VOUCHER,
    UPDATE_VOUCHER_FAIL,
    UPDATE_VOUCHER_SUCCESS,
    DELETE_VOUCHER,
    DELETE_VOUCHER_FAIL,
    DELETE_VOUCHER_SUCCESS,
    TOGGLE_ADD_VOUCHER_MODAL,
    TOGGLE_EDIT_VOUCHER_MODAL,
  } = VOUCHERS;

  const { VOUCHERS: VOUCHERS_ROUTE } = ROUTES;
  const { vouchers, applyNewFilter, index, isOpen, voucherInfo, err } = payload;

  const {
    vouchersList: vouchersState,
    addVoucherModal: { isOpen: addVoucherModalIsOpenState },
    editVoucherModal: { isOpen: editVoucherModalIsOpenState },
  } = state;

  switch (type) {
    case GET_ALL_VOUCHERS: {
      return {
        ...state,
        applyNewFilter,
        getAllVouchers: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_VOUCHERS_SUCCESS: {
      return {
        ...state,
        vouchersList:
          vouchersState && !applyNewFilter ? unionBy(vouchersState, vouchers, '_id') : vouchers,
        hasMoreVouchers: vouchers.length === 20,
        getAllVouchers: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_VOUCHERS_FAIL: {
      return {
        ...state,
        getAllVouchers: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case ADD_VOUCHER: {
      return {
        ...state,
        addVoucher: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_VOUCHER_SUCCESS: {
      vouchersState.unshift(voucherInfo);

      return {
        ...state,
        vouchersList: vouchersState,
        addVoucher: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addVoucherModal: { isOpen: !addVoucherModalIsOpenState },
      };
    }
    case ADD_VOUCHER_FAIL: {
      return {
        ...state,
        addVoucher: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_VOUCHER_RESET: {
      return {
        ...state,
        addVoucher: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }

    case UPDATE_VOUCHER: {
      return {
        ...state,
        editVoucher: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_VOUCHER_SUCCESS: {
      const updatedVouchers = update(vouchersState, {
        [index]: { $merge: { ...voucherInfo } },
      });

      return {
        ...state,
        vouchersList: updatedVouchers,
        editVoucher: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editVoucherModal: { isOpen: !editVoucherModalIsOpenState },
      };
    }
    case UPDATE_VOUCHER_FAIL: {
      return {
        ...state,
        editVoucher: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case DELETE_VOUCHER: {
      return {
        ...state,
        deleteVoucher: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_VOUCHER_SUCCESS: {
      if (index !== undefined) vouchersState.splice(index, 1);
      else window.location.pathname = VOUCHERS_ROUTE;

      return {
        ...state,
        vouchersList: vouchersState,
        deleteVoucher: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case DELETE_VOUCHER_FAIL: {
      return {
        ...state,
        deleteVoucher: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case TOGGLE_ADD_VOUCHER_MODAL: {
      return {
        ...state,
        addVoucherModal: { isOpen: !addVoucherModalIsOpenState },
      };
    }
    case TOGGLE_EDIT_VOUCHER_MODAL: {
      return {
        ...state,
        editVoucherModal: { isOpen: !editVoucherModalIsOpenState },
      };
    }

    default:
      return state;
  }
};
