import { put, takeEvery, takeLatest } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Status, Urls } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { VOUCHERS } from './actions';

const {
  GET_ALL_VOUCHERS, GET_ALL_VOUCHERS_FAIL, GET_ALL_VOUCHERS_SUCCESS,
  ADD_VOUCHER, ADD_VOUCHER_FAIL, ADD_VOUCHER_SUCCESS,
  UPDATE_VOUCHER, UPDATE_VOUCHER_FAIL, UPDATE_VOUCHER_SUCCESS,
  DELETE_VOUCHER, DELETE_VOUCHER_FAIL, DELETE_VOUCHER_SUCCESS,
} = VOUCHERS;

function* getAllVouchers(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { general } = Urls.vouchers;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = general;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else if (options[key] && options[key] !== '') {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: vouchers } = response;

    yield put({
      type: GET_ALL_VOUCHERS_SUCCESS,
      vouchers,
      applyNewFilter,
    });
  } else {
    const { response: { message } } = response;
    yield put({
      type: GET_ALL_VOUCHERS_FAIL,
      err: message,
    });
  }
}

function* addVoucher(payload) {
  const { voucherInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { general } = Urls.vouchers;
  const url = `${general}`;
  const api = new Api();
  const body = {
    ...voucherInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: voucherInfo } = response;

    yield put({
      type: ADD_VOUCHER_SUCCESS,
      voucherInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_VOUCHER_FAIL,
      err: message,
    });
  }
}

function* updateVoucher(payload) {
  const { voucherInfo, voucherId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { general } = Urls.vouchers;
  const url = `${general}/${voucherId}`;
  const api = new Api();
  const body = { ...voucherInfo };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    voucherInfo._id = voucherId;
    yield put({
      type: UPDATE_VOUCHER_SUCCESS,
      voucherInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_VOUCHER_FAIL,
      err: message,
    });
  }
}

function* deleteVoucher(payload) {
  const { voucherId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { general } = Urls.vouchers;
  const url = `${general}/${voucherId}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.delete(url, undefined, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_VOUCHER_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_VOUCHER_FAIL,
      err: message,
    });
  }
}

function* workersSaga() {
  yield takeLatest(GET_ALL_VOUCHERS, getAllVouchers);
  yield takeEvery(ADD_VOUCHER, addVoucher);
  yield takeEvery(UPDATE_VOUCHER, updateVoucher);
  yield takeEvery(DELETE_VOUCHER, deleteVoucher);
}

export default workersSaga;
