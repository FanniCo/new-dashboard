import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { MESSAGES } from './actions';

const {
  GET_ALL_MESSAGES,
  GET_ALL_MESSAGES_SUCCESS,
  GET_ALL_MESSAGES_FAIL,
  ADD_NEW_MESSAGE,
  ADD_NEW_MESSAGE_SUCCESS,
  ADD_NEW_MESSAGE_FAIL,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_FAIL,
  UPDATE_MESSAGE,
  UPDATE_MESSAGE_SUCCESS,
  UPDATE_MESSAGE_FAIL,
} = MESSAGES;

function* getAllMessages(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { messages } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = messages.getAllMessages;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: messagesList } = response;

    yield put({
      type: GET_ALL_MESSAGES_SUCCESS,
      messages: messagesList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_MESSAGES_FAIL,
      err: message,
    });
  }
}

function* addNewMessage(payload) {
  const { newMessage } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { messages } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newMessage;

  const url = messages.addNewMessage;
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newMessageInfo } = response;

    yield put({
      type: ADD_NEW_MESSAGE_SUCCESS,
      newMessageInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_MESSAGE_FAIL,
      err: message,
    });
  }
}

function* deleteMessage(payload) {
  const { messageId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { messages } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = messages.delete;
  const response = yield api.delete(`${url}/${messageId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_MESSAGE_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_MESSAGE_FAIL,
      err: message,
    });
  }
}

function* updateMessage(payload) {
  const { newMessage, messageId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { messages } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newMessage;

  const url = `${messages.update}/${messageId}`;
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: UPDATE_MESSAGE_SUCCESS,
      newMessageInfo: { ...newMessage, _id: messageId },
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_MESSAGE_FAIL,
      err: message,
    });
  }
}

function* messagesSaga() {
  yield takeEvery(GET_ALL_MESSAGES, getAllMessages);
  yield takeEvery(ADD_NEW_MESSAGE, addNewMessage);
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
  yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export default messagesSaga;
