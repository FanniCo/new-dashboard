export const MESSAGES = {
  TOGGLE_ADD_NEW_MESSAGE_MODAL: 'TOGGLE_ADD_NEW_MESSAGE_MODAL',
  GET_ALL_MESSAGES: 'GET_ALL_MESSAGES',
  GET_ALL_MESSAGES_SUCCESS: 'GET_ALL_MESSAGES_SUCCESS',
  GET_ALL_MESSAGES_FAIL: 'GET_ALL_MESSAGES_FAIL',
  ADD_NEW_MESSAGE: 'ADD_NEW_MESSAGE',
  ADD_NEW_MESSAGE_SUCCESS: 'ADD_NEW_MESSAGE_SUCCESS',
  ADD_NEW_MESSAGE_FAIL: 'ADD_NEW_MESSAGE_FAIL',
  DELETE_MESSAGE: 'DELETE_MESSAGE',
  DELETE_MESSAGE_SUCCESS: 'DELETE_MESSAGE_SUCCESS',
  DELETE_MESSAGE_FAIL: 'DELETE_MESSAGE_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_MESSAGE_MODAL: 'OPEN_EDIT_MESSAGE_MODAL',
  UPDATE_MESSAGE: 'UPDATE_MESSAGE',
  UPDATE_MESSAGE_SUCCESS: 'UPDATE_MESSAGE_SUCCESS',
  UPDATE_MESSAGE_FAIL: 'UPDATE_MESSAGE_FAIL',
};

export const toggleAddNewMessageModal = () => ({
  type: MESSAGES.TOGGLE_ADD_NEW_MESSAGE_MODAL,
});

export const getAllMessages = (options, applyNewFilter = false) => ({
  type: MESSAGES.GET_ALL_MESSAGES,
  options,
  applyNewFilter,
});

export const getAllMessagesSuccess = (messagesList, applyNewFilter: false) => ({
  type: MESSAGES.GET_ALL_MESSAGES_SUCCESS,
  messagesList,
  applyNewFilter,
});

export const getAllMessagesFail = err => ({
  type: MESSAGES.GET_ALL_MESSAGES_FAIL,
  err,
});

export const addNewMessage = newMessage => ({
  type: MESSAGES.ADD_NEW_MESSAGE,
  newMessage,
});

export const addNewMessageSuccess = newMessageInfo => ({
  type: MESSAGES.ADD_NEW_MESSAGE_SUCCESS,
  newMessageInfo,
});

export const addNewMessageFail = err => ({
  type: MESSAGES.ADD_NEW_MESSAGE_FAIL,
  err,
});

export const deleteMessage = (messageId, index) => ({
  type: MESSAGES.DELETE_MESSAGE,
  messageId,
  index,
});

export const deleteMessageSuccess = index => ({
  type: MESSAGES.DELETE_MESSAGE_SUCCESS,
  index,
});

export const deleteMessageFail = err => ({
  type: MESSAGES.DELETE_MESSAGE_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: MESSAGES.OPEN_CONFIRM_MODAL,
});

export const openEditMessageModal = () => ({
  type: MESSAGES.OPEN_EDIT_MESSAGE_MODAL,
});

export const updateMessage = (newMessage, messageId, index) => ({
  type: MESSAGES.UPDATE_MESSAGE,
  newMessage,
  messageId,
  index,
});

export const updateMessageSuccess = (newMessageInfo, index) => ({
  type: MESSAGES.UPDATE_MESSAGE_SUCCESS,
  newMessageInfo,
  index,
});

export const updateMessageFail = err => ({
  type: MESSAGES.UPDATE_MESSAGE_FAIL,
  err,
});
