import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { MESSAGES } from './actions';

const initialState = {
  applyNewFilter: false,
  messages: undefined,
  hasMoreMessages: true,
  addNewMessageModal: {
    isOpen: false,
  },
  getAllMessages: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addMessage: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteMessage: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editMessageModal: { isOpen: false },
  updateMessage: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_MESSAGE_MODAL,
    GET_ALL_MESSAGES,
    GET_ALL_MESSAGES_SUCCESS,
    GET_ALL_MESSAGES_FAIL,
    ADD_NEW_MESSAGE,
    ADD_NEW_MESSAGE_SUCCESS,
    ADD_NEW_MESSAGE_FAIL,
    DELETE_MESSAGE,
    DELETE_MESSAGE_SUCCESS,
    DELETE_MESSAGE_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_MESSAGE_MODAL,
    UPDATE_MESSAGE,
    UPDATE_MESSAGE_SUCCESS,
    UPDATE_MESSAGE_FAIL,
  } = MESSAGES;
  const { messages, applyNewFilter, newMessageInfo, index, err } = payload;
  const {
    messages: messagesState,
    addNewMessageModal: { isOpen: addNewMessageModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editMessageModal: { isOpen: editMessageModalOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_MESSAGE_MODAL: {
      return {
        ...state,
        addNewMessageModal: {
          isOpen: !addNewMessageModalIsOpenState,
        },
      };
    }
    case GET_ALL_MESSAGES: {
      return {
        ...state,
        applyNewFilter,
        getAllMessages: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_MESSAGES_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        messages:
          messagesState && !applyNewFilter ? unionBy(messagesState, messages, '_id') : messages,
        hasMoreMessages: messages.length === 20,
        getAllMessages: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_MESSAGES_FAIL: {
      return {
        ...state,
        getAllMessages: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_MESSAGE: {
      return {
        ...state,
        addMessage: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_MESSAGE_SUCCESS: {
      messagesState.unshift(newMessageInfo);
      return {
        ...state,
        messages: messagesState,
        addMessage: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewMessageModal: {
          isOpen: !addNewMessageModalIsOpenState,
        },
      };
    }
    case ADD_NEW_MESSAGE_FAIL: {
      return {
        ...state,
        addMessage: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_MESSAGE_MODAL: {
      return {
        ...state,
        editMessageModal: { isOpen: !editMessageModalOpenState },
      };
    }
    case DELETE_MESSAGE: {
      return {
        ...state,
        deleteMessage: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_MESSAGE_SUCCESS: {
      if (index !== undefined) messagesState.splice(index, 1);
      return {
        ...state,
        messages: messagesState,
        deleteMessage: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_MESSAGE_FAIL: {
      return {
        ...state,
        deleteMessage: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_MESSAGE: {
      return {
        ...state,
        updateMessage: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_MESSAGE_SUCCESS: {
      const updatedMessages = update(messagesState, {
        [index]: { $set: { ...newMessageInfo } },
      });
      return {
        ...state,
        messages: updatedMessages,
        updateMessage: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editMessageModal: {
          isOpen: !editMessageModalOpenState,
        },
      };
    }
    case UPDATE_MESSAGE_FAIL: {
      return {
        ...state,
        updateMessage: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
