import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { VENDORS } from './actions';

const initialState = {
  applyNewFilter: false,
  vendors: undefined,
  hasMoreVendors: true,
  addNewVendorModal: {
    isOpen: false,
  },
  getAllVendors: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addVendor: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteVendor: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editVendorModal: { isOpen: false },
  updateVendor: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  changePassword: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  changePasswordModal: { isOpen: false },
  resetVendorCredits: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_VENDOR_MODAL,
    GET_ALL_VENDORS,
    GET_ALL_VENDORS_SUCCESS,
    GET_ALL_VENDORS_FAIL,
    ADD_NEW_VENDOR,
    ADD_NEW_VENDOR_SUCCESS,
    ADD_NEW_VENDOR_FAIL,
    DELETE_VENDOR,
    DELETE_VENDOR_SUCCESS,
    DELETE_VENDOR_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_VENDOR_MODAL,
    UPDATE_VENDOR,
    UPDATE_VENDOR_SUCCESS,
    UPDATE_VENDOR_FAIL,
    TOGGLE_CHANGE_PASSWORD_MODAL,
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAIL,
    CHANGE_PASSWORD_RESET,
    RESET_VENDOR_CREDITS,
    RESET_VENDOR_CREDITS_SUCCESS,
    RESET_VENDOR_CREDITS_FAIL,
  } = VENDORS;
  const { vendors, applyNewFilter, newVendorInfo, index, err } = payload;
  const {
    vendors: vendorsState,
    addNewVendorModal: { isOpen: addNewVendorModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editVendorModal: { isOpen: editVendorModalOpenState },
    changePasswordModal: { isOpen: changePasswordModalIsOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_VENDOR_MODAL: {
      return {
        ...state,
        addNewVendorModal: {
          isOpen: !addNewVendorModalIsOpenState,
        },
      };
    }
    case GET_ALL_VENDORS: {
      return {
        ...state,
        applyNewFilter,
        getAllVendors: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_VENDORS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        vendors: vendorsState && !applyNewFilter ? unionBy(vendorsState, vendors, '_id') : vendors,
        hasMoreVendors: vendors.length === 20,
        getAllVendors: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_VENDORS_FAIL: {
      return {
        ...state,
        getAllVendors: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_VENDOR: {
      return {
        ...state,
        addVendor: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, VENDOR: '' },
        },
      };
    }
    case ADD_NEW_VENDOR_SUCCESS: {
      vendorsState.unshift(newVendorInfo);
      return {
        ...state,
        vendors: vendorsState,
        addVendor: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewVendorModal: {
          isOpen: !addNewVendorModalIsOpenState,
        },
      };
    }
    case ADD_NEW_VENDOR_FAIL: {
      return {
        ...state,
        addVendor: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_VENDOR_MODAL: {
      return {
        ...state,
        editVendorModal: { isOpen: !editVendorModalOpenState },
      };
    }
    case DELETE_VENDOR: {
      return {
        ...state,
        deleteVendor: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_VENDOR_SUCCESS: {
      if (index !== undefined) vendorsState.splice(index, 1);
      return {
        ...state,
        Vendors: vendorsState,
        deleteVendor: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_VENDOR_FAIL: {
      return {
        ...state,
        deleteVendor: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_VENDOR: {
      return {
        ...state,
        updateVendor: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_VENDOR_SUCCESS: {
      const updatedVendors = update(vendorsState, {
        [index]: { $merge: { ...newVendorInfo } },
      });
      return {
        ...state,
        vendors: updatedVendors,
        updateVendor: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editVendorModal: {
          isOpen: !editVendorModalOpenState,
        },
      };
    }
    case UPDATE_VENDOR_FAIL: {
      return {
        ...state,
        updateVendor: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_CHANGE_PASSWORD_MODAL: {
      return {
        ...state,
        changePasswordModal: {
          isOpen: !changePasswordModalIsOpenState,
        },
      };
    }
    case CHANGE_PASSWORD: {
      return {
        ...state,
        changePassword: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CHANGE_PASSWORD_SUCCESS: {
      return {
        ...state,
        changePassword: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        changePasswordModal: {
          isOpen: !changePasswordModalIsOpenState,
        },
      };
    }
    case CHANGE_PASSWORD_FAIL: {
      return {
        ...state,
        changePassword: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case CHANGE_PASSWORD_RESET: {
      return {
        ...state,
        changePassword: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case RESET_VENDOR_CREDITS: {
      return {
        ...state,
        resetVendorCredits: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case RESET_VENDOR_CREDITS_SUCCESS: {
      return {
        ...state,
        resetVendorCredits: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case RESET_VENDOR_CREDITS_FAIL: {
      return {
        ...state,
        resetVendorCredits: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
