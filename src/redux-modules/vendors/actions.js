export const VENDORS = {
  TOGGLE_ADD_NEW_VENDOR_MODAL: 'TOGGLE_ADD_NEW_VENDOR_MODAL',
  GET_ALL_VENDORS: 'GET_ALL_VENDORS',
  GET_ALL_VENDORS_SUCCESS: 'GET_ALL_VENDORS_SUCCESS',
  GET_ALL_VENDORS_FAIL: 'GET_ALL_VENDORS_FAIL',
  ADD_NEW_VENDOR: 'ADD_NEW_VENDOR',
  ADD_NEW_VENDOR_SUCCESS: 'ADD_NEW_VENDOR_SUCCESS',
  ADD_NEW_VENDOR_FAIL: 'ADD_NEW_VENDOR_FAIL',
  DELETE_VENDOR: 'DELETE_VENDOR',
  DELETE_VENDOR_SUCCESS: 'DELETE_VENDOR_SUCCESS',
  DELETE_VENDOR_FAIL: 'DELETE_VENDOR_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_VENDOR_MODAL: 'OPEN_EDIT_VENDOR_MODAL',
  UPDATE_VENDOR: 'UPDATE_VENDOR',
  UPDATE_VENDOR_SUCCESS: 'UPDATE_VENDOR_SUCCESS',
  UPDATE_VENDOR_FAIL: 'UPDATE_VENDOR_FAIL',
  TOGGLE_CHANGE_PASSWORD_MODAL: 'TOGGLE_CHANGE_PASSWORD_MODAL',
  CHANGE_PASSWORD: 'CHANGE_PASSWORD',
  CHANGE_PASSWORD_SUCCESS: 'CHANGE_PASSWORD_SUCCESS',
  CHANGE_PASSWORD_FAIL: 'CHANGE_PASSWORD_FAIL',
  CHANGE_PASSWORD_RESET: 'CHANGE_PASSWORD_RESET',
  RESET_VENDOR_CREDITS: 'RESET_VENDOR_CREDITS',
  RESET_VENDOR_CREDITS_SUCCESS: 'RESET_VENDOR_CREDITS_SUCCESS',
  RESET_VENDOR_CREDITS_FAIL: 'RESET_VENDOR_CREDITS_FAIL',
};

export const toggleAddNewVendorModal = () => ({
  type: VENDORS.TOGGLE_ADD_NEW_VENDOR_MODAL,
});

export const getAllVendors = (options, applyNewFilter = false) => ({
  type: VENDORS.GET_ALL_VENDORS,
  options,
  applyNewFilter,
});

export const getAllVendorsSuccess = (vendorsList, applyNewFilter: false) => ({
  type: VENDORS.GET_ALL_VENDORS_SUCCESS,
  vendorsList,
  applyNewFilter,
});

export const getAllVendorsFail = err => ({
  type: VENDORS.GET_ALL_VENDORS_FAIL,
  err,
});

export const addNewVendor = newVendor => ({
  type: VENDORS.ADD_NEW_VENDOR,
  newVendor,
});

export const addNewVendorSuccess = newVendorInfo => ({
  type: VENDORS.ADD_NEW_VENDOR_SUCCESS,
  newVendorInfo,
});

export const addNewVendorFail = err => ({
  type: VENDORS.ADD_NEW_VENDOR_FAIL,
  err,
});

export const deleteVendor = (vendorId, index) => ({
  type: VENDORS.DELETE_VENDOR,
  vendorId,
  index,
});

export const deleteVendorSuccess = index => ({
  type: VENDORS.DELETE_VENDOR_SUCCESS,
  index,
});

export const deleteVendorFail = err => ({
  type: VENDORS.DELETE_VENDOR_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: VENDORS.OPEN_CONFIRM_MODAL,
});

export const openEditVendorModal = () => ({
  type: VENDORS.OPEN_EDIT_VENDOR_MODAL,
});

export const updateVendor = (newVendor, vendorId, index) => ({
  type: VENDORS.UPDATE_VENDOR,
  newVendor,
  vendorId,
  index,
});

export const updateVendorSuccess = (newVendorInfo, vendorId, index) => ({
  type: VENDORS.UPDATE_VENDOR_SUCCESS,
  newVendorInfo,
  vendorId,
  index,
});

export const updateVendorFail = err => ({
  type: VENDORS.UPDATE_VENDOR_FAIL,
  err,
});

export const toggleChangePasswordModal = () => ({
  type: VENDORS.TOGGLE_CHANGE_PASSWORD_MODAL,
});

export const changePassword = newPasswordInfo => ({
  type: VENDORS.CHANGE_PASSWORD,
  newPasswordInfo,
});

export const changePasswordSuccess = () => ({
  type: VENDORS.CHANGE_PASSWORD_SUCCESS,
});

export const changePasswordFail = err => ({
  type: VENDORS.CHANGE_PASSWORD_FAIL,
  err,
});

export const changePasswordReset = () => ({
  type: VENDORS.CHANGE_PASSWORD_RESET,
});

export const resetVendorCredits = (vendorId, index) => ({
  type: VENDORS.RESET_VENDOR_CREDITS,
  vendorId,
  index,
});

export const resetVendorCreditsSuccess = () => ({
  type: VENDORS.RESET_VENDOR_CREDITS_SUCCESS,
});

export const resetVendorCreditsFail = err => ({
  type: VENDORS.RESET_VENDOR_CREDITS_FAIL,
  err,
});
