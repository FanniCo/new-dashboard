import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { VENDORS } from './actions';

const {
  GET_ALL_VENDORS,
  GET_ALL_VENDORS_SUCCESS,
  GET_ALL_VENDORS_FAIL,
  ADD_NEW_VENDOR,
  ADD_NEW_VENDOR_SUCCESS,
  ADD_NEW_VENDOR_FAIL,
  DELETE_VENDOR,
  DELETE_VENDOR_SUCCESS,
  DELETE_VENDOR_FAIL,
  UPDATE_VENDOR,
  UPDATE_VENDOR_SUCCESS,
  UPDATE_VENDOR_FAIL,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAIL,
  RESET_VENDOR_CREDITS,
  RESET_VENDOR_CREDITS_SUCCESS,
  RESET_VENDOR_CREDITS_FAIL,
} = VENDORS;

function* getAllVendors(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { vendors } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = vendors.getAllVendors;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: vendorsList } = response;

    yield put({
      type: GET_ALL_VENDORS_SUCCESS,
      vendors: vendorsList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;
    yield put({
      type: GET_ALL_VENDORS_FAIL,
      err: message,
    });
  }
}

function* addNewVendor(payload) {
  const { newVendor } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { vendors } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newVendor;

  const url = vendors.addNewVendor;
  const response = yield api.post(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    const { response: newVendorInfo } = response;

    yield put({
      type: ADD_NEW_VENDOR_SUCCESS,
      newVendorInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_VENDOR_FAIL,
      err: message,
    });
  }
}

function* deleteVendor(payload) {
  const { vendorId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { vendors } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = vendors.delete;
  const response = yield api.delete(`${url}/${vendorId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_VENDOR_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_VENDOR_FAIL,
      err: message,
    });
  }
}

function* updateVendor(payload) {
  const { newVendor, vendorId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { vendors } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newVendor; //this is formData can't be used to update the state
  const url = `${vendors.update}/${vendorId}`;
  const response = yield api.patch(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    yield put({
      type: UPDATE_VENDOR_SUCCESS,
      newVendorInfo: { ...response.response, _id: vendorId }, //use the body response to update the state with the new values
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_VENDOR_FAIL,
      err: message,
    });
  }
}

function* changePassword(payload) {
  const { newPasswordInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { vendors } = Urls;
  const url = vendors.changePassword;
  const api = new Api();
  const body = {
    ...newPasswordInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: CHANGE_PASSWORD_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CHANGE_PASSWORD_FAIL,
      err: message,
    });
  }
}

function* resetVendorCredits(payload) {
  const { vendorId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { vendors } = Urls;
  const url = vendors.restVendorCredits;
  const api = new Api();
  const body = {
    vendorId,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: RESET_VENDOR_CREDITS_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: RESET_VENDOR_CREDITS_FAIL,
      err: message,
    });
  }
}

function* vendorsSaga() {
  yield takeEvery(GET_ALL_VENDORS, getAllVendors);
  yield takeEvery(ADD_NEW_VENDOR, addNewVendor);
  yield takeEvery(DELETE_VENDOR, deleteVendor);
  yield takeEvery(UPDATE_VENDOR, updateVendor);
  yield takeEvery(CHANGE_PASSWORD, changePassword);
  yield takeEvery(RESET_VENDOR_CREDITS, resetVendorCredits);
}

export default vendorsSaga;
