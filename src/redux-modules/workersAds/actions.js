export const WORKERS_ADS = {
  TOGGLE_ADD_NEW_WORKERS_AD_MODAL: 'TOGGLE_ADD_NEW_WORKERS_AD_MODAL',
  GET_ALL_WORKERS_ADS: 'GET_ALL_WORKERS_ADS',
  GET_ALL_WORKERS_ADS_SUCCESS: 'GET_ALL_WORKERS_ADS_SUCCESS',
  GET_ALL_WORKERS_ADS_FAIL: 'GET_ALL_WORKERS_ADS_FAIL',
  ADD_NEW_WORKERS_AD: 'ADD_NEW_WORKERS_AD',
  ADD_NEW_WORKERS_AD_SUCCESS: 'ADD_NEW_WORKERS_AD_SUCCESS',
  ADD_NEW_WORKERS_AD_FAIL: 'ADD_NEW_WORKERS_AD_FAIL',
  DELETE_WORKERS_AD: 'DELETE_WORKERS_AD',
  DELETE_WORKERS_AD_SUCCESS: 'DELETE_WORKERS_AD_SUCCESS',
  DELETE_WORKERS_AD_FAIL: 'DELETE_WORKERS_AD_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_WORKERS_AD_MODAL: 'OPEN_EDIT_WORKERS_AD_MODAL',
  UPDATE_WORKERS_AD: 'UPDATE_WORKERS_AD',
  UPDATE_WORKERS_AD_SUCCESS: 'UPDATE_WORKERS_AD_SUCCESS',
  UPDATE_WORKERS_AD_FAIL: 'UPDATE_WORKERS_AD_FAIL',
};

export const toggleAddNewWorkersAdModal = () => ({
  type: WORKERS_ADS.TOGGLE_ADD_NEW_WORKERS_AD_MODAL,
});

export const getAllWorkersAds = (options, applyNewFilter = false) => ({
  type: WORKERS_ADS.GET_ALL_WORKERS_ADS,
  options,
  applyNewFilter,
});

export const getAllWorkersAdsSuccess = (workersAdsList, applyNewFilter: false) => ({
  type: WORKERS_ADS.GET_ALL_WORKERS_ADS_SUCCESS,
  workersAdsList,
  applyNewFilter,
});

export const getAllWorkersAdsFail = err => ({
  type: WORKERS_ADS.GET_ALL_WORKERS_ADS_FAIL,
  err,
});

export const addNewWorkersAd = newWorkersAd => ({
  type: WORKERS_ADS.ADD_NEW_WORKERS_AD,
  newWorkersAd,
});

export const addNewWorkersAdSuccess = newWorkersAdInfo => ({
  type: WORKERS_ADS.ADD_NEW_WORKERS_AD_SUCCESS,
  newWorkersAdInfo,
});

export const addNewWorkersAdFail = err => ({
  type: WORKERS_ADS.ADD_NEW_WORKERS_AD_FAIL,
  err,
});

export const deleteWorkersAd = (workersAdId, index) => ({
  type: WORKERS_ADS.DELETE_WORKERS_AD,
  workersAdId,
  index,
});

export const deleteWorkersAdSuccess = index => ({
  type: WORKERS_ADS.DELETE_WORKERS_AD_SUCCESS,
  index,
});

export const deleteWorkersAdFail = err => ({
  type: WORKERS_ADS.DELETE_WORKERS_AD_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: WORKERS_ADS.OPEN_CONFIRM_MODAL,
});

export const openEditWorkersAdModal = () => ({
  type: WORKERS_ADS.OPEN_EDIT_WORKERS_AD_MODAL,
});

export const updateWorkersAd = (newWorkersAd, workersAdId, index) => ({
  type: WORKERS_ADS.UPDATE_WORKERS_AD,
  newWorkersAd,
  workersAdId,
  index,
});

export const updateWorkersAdSuccess = (newWorkersAdInfo, index) => ({
  type: WORKERS_ADS.UPDATE_WORKERS_AD_SUCCESS,
  newWorkersAdInfo,
  index,
});

export const updateWorkersAdFail = err => ({
  type: WORKERS_ADS.UPDATE_WORKERS_AD_FAIL,
  err,
});
