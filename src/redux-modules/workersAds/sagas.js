import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { WORKERS_ADS } from './actions';

const {
  GET_ALL_WORKERS_ADS,
  GET_ALL_WORKERS_ADS_SUCCESS,
  GET_ALL_WORKERS_ADS_FAIL,
  ADD_NEW_WORKERS_AD,
  ADD_NEW_WORKERS_AD_SUCCESS,
  ADD_NEW_WORKERS_AD_FAIL,
  DELETE_WORKERS_AD,
  DELETE_WORKERS_AD_SUCCESS,
  DELETE_WORKERS_AD_FAIL,
  UPDATE_WORKERS_AD,
  UPDATE_WORKERS_AD_SUCCESS,
  UPDATE_WORKERS_AD_FAIL,
} = WORKERS_ADS;

function* getAllWorkersAds(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = workersAds.getAllWorkersAds;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: workersAdsList } = response;

    yield put({
      type: GET_ALL_WORKERS_ADS_SUCCESS,
      workersAds: workersAdsList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_WORKERS_ADS_FAIL,
      err: message,
    });
  }
}

function* addNewWorkersAd(payload) {
  const { newWorkersAd } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newWorkersAd;

  const url = workersAds.addNewWorkersAd;
  const response = yield api.post(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    const { response: newWorkersAdInfo } = response;

    yield put({
      type: ADD_NEW_WORKERS_AD_SUCCESS,
      newWorkersAdInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_WORKERS_AD_FAIL,
      err: message,
    });
  }
}

function* deleteWorkersAd(payload) {
  const { workersAdId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = workersAds.delete;
  const response = yield api.delete(`${url}/${workersAdId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_WORKERS_AD_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_WORKERS_AD_FAIL,
      err: message,
    });
  }
}

function* updateWorkersAd(payload) {
  const { newWorkersAd, workersAdId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { workersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newWorkersAd;

  const url = `${workersAds.update}/${workersAdId}`;
  const response = yield api.patch(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    yield put({
      type: UPDATE_WORKERS_AD_SUCCESS,
      newWorkersAdInfo: { ...response.response, _id: workersAdId },
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_WORKERS_AD_FAIL,
      err: message,
    });
  }
}

function* workersAdsSaga() {
  yield takeEvery(GET_ALL_WORKERS_ADS, getAllWorkersAds);
  yield takeEvery(ADD_NEW_WORKERS_AD, addNewWorkersAd);
  yield takeEvery(DELETE_WORKERS_AD, deleteWorkersAd);
  yield takeEvery(UPDATE_WORKERS_AD, updateWorkersAd);
}

export default workersAdsSaga;
