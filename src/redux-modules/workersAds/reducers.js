import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { WORKERS_ADS } from './actions';

const initialState = {
  applyNewFilter: false,
  workersAds: undefined,
  hasMoreWorkersAds: true,
  addNewWorkersAdModal: {
    isOpen: false,
  },
  getAllWorkersAds: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addWorkersAd: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteWorkersAd: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editWorkersAdModal: { isOpen: false },
  updateWorkersAd: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_WORKERS_AD_MODAL,
    GET_ALL_WORKERS_ADS,
    GET_ALL_WORKERS_ADS_SUCCESS,
    GET_ALL_WORKERS_ADS_FAIL,
    ADD_NEW_WORKERS_AD,
    ADD_NEW_WORKERS_AD_SUCCESS,
    ADD_NEW_WORKERS_AD_FAIL,
    DELETE_WORKERS_AD,
    DELETE_WORKERS_AD_SUCCESS,
    DELETE_WORKERS_AD_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_WORKERS_AD_MODAL,
    UPDATE_WORKERS_AD,
    UPDATE_WORKERS_AD_SUCCESS,
    UPDATE_WORKERS_AD_FAIL,
  } = WORKERS_ADS;
  const { workersAds, applyNewFilter, newWorkersAdInfo, index, err } = payload;
  const {
    workersAds: workersAdsState,
    addNewWorkersAdModal: { isOpen: addNewWorkersAdModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editWorkersAdModal: { isOpen: editWorkersAdModalOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_WORKERS_AD_MODAL: {
      return {
        ...state,
        addNewWorkersAdModal: {
          isOpen: !addNewWorkersAdModalIsOpenState,
        },
        addWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updateWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_WORKERS_ADS: {
      return {
        ...state,
        applyNewFilter,
        getAllWorkersAds: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_WORKERS_ADS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        workersAds:
          workersAdsState && !applyNewFilter
            ? unionBy(workersAdsState, workersAds, '_id')
            : workersAds,
        hasMoreWorkersAds: workersAds.length === 20,
        getAllWorkersAds: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, WorkersAd: '' },
        },
      };
    }
    case GET_ALL_WORKERS_ADS_FAIL: {
      return {
        ...state,
        getAllWorkersAds: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_WORKERS_AD: {
      return {
        ...state,
        addWorkersAd: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_WORKERS_AD_SUCCESS: {
      workersAdsState.unshift(newWorkersAdInfo);
      return {
        ...state,
        workersAds: workersAdsState,
        addWorkersAd: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewWorkersAdModal: {
          isOpen: !addNewWorkersAdModalIsOpenState,
        },
      };
    }
    case ADD_NEW_WORKERS_AD_FAIL: {
      return {
        ...state,
        addWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_WORKERS_AD_MODAL: {
      return {
        ...state,
        editWorkersAdModal: { isOpen: !editWorkersAdModalOpenState },
        addWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updateWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_WORKERS_AD: {
      return {
        ...state,
        deleteWorkersAd: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_WORKERS_AD_SUCCESS: {
      if (index !== undefined) workersAdsState.splice(index, 1);
      return {
        ...state,
        workersAds: workersAdsState,
        deleteWorkersAd: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_WORKERS_AD_FAIL: {
      return {
        ...state,
        deleteWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_WORKERS_AD: {
      return {
        ...state,
        updateWorkersAd: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_WORKERS_AD_SUCCESS: {
      const updatedWorkersAds = update(workersAdsState, {
        [index]: { $set: { ...newWorkersAdInfo } },
      });
      return {
        ...state,
        workersAds: updatedWorkersAds,
        updateWorkersAd: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editWorkersAdModal: {
          isOpen: !editWorkersAdModalOpenState,
        },
      };
    }
    case UPDATE_WORKERS_AD_FAIL: {
      return {
        ...state,
        updateWorkersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
