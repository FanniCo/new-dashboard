export const ADMINS_ACTIVITIES = {
  GET_ALL_ADMINS_ACTIVITIES: 'GET_ALL_ADMINS_ACTIVITIES',
  GET_ALL_ADMINS_ACTIVITIES_SUCCESS: 'GET_ALL_ADMINS_ACTIVITIES_SUCCESS',
  GET_ALL_ADMINS_ACTIVITIES_FAIL: 'GET_ALL_ADMINS_ACTIVITIES_FAIL',
};

export const getAllAdminsActivities = (options, applyNewFilter = false) => ({
  type: ADMINS_ACTIVITIES.GET_ALL_ADMINS_ACTIVITIES,
  options,
  applyNewFilter,
});

export const getAllAdminsActivitiesSuccess = (postsList, applyNewFilter: false) => ({
  type: ADMINS_ACTIVITIES.GET_ALL_ADMINS_ACTIVITIES_SUCCESS,
  postsList,
  applyNewFilter,
});

export const getAllAdminsActivitiesFail = err => ({
  type: ADMINS_ACTIVITIES.GET_ALL_ADMINS_ACTIVITIES_FAIL,
  err,
});
