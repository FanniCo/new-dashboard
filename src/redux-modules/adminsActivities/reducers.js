import { unionBy } from 'lodash';
import { ADMINS_ACTIVITIES } from './actions';

const initialState = {
  applyNewFilter: false,
  adminsActivities: undefined,
  hasMoreAdminsActivities: true,
  getAllAdminsActivities: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_ADMINS_ACTIVITIES,
    GET_ALL_ADMINS_ACTIVITIES_SUCCESS,
    GET_ALL_ADMINS_ACTIVITIES_FAIL,
  } = ADMINS_ACTIVITIES;
  const { adminsActivities, applyNewFilter, err } = payload;
  const { adminsActivities: adminsActivitiesState } = state;

  switch (type) {
    case GET_ALL_ADMINS_ACTIVITIES: {
      return {
        ...state,
        applyNewFilter,
        getAllAdminsActivities: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_ADMINS_ACTIVITIES_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        adminsActivities:
          adminsActivitiesState && !applyNewFilter
            ? unionBy(adminsActivitiesState, adminsActivities, '_id')
            : adminsActivities,
        hasMoreAdminsActivities: adminsActivities.length === 20,
        getAllAdminsActivities: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_ADMINS_ACTIVITIES_FAIL: {
      return {
        ...state,
        getAllAdminsActivities: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
