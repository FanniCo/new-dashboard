import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { ADMINS_ACTIVITIES } from './actions';

const {
  GET_ALL_ADMINS_ACTIVITIES,
  GET_ALL_ADMINS_ACTIVITIES_SUCCESS,
  GET_ALL_ADMINS_ACTIVITIES_FAIL,
} = ADMINS_ACTIVITIES;

function* getAllAdminsActivities(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = reports.adminsActivities;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: adminsActivitiesList } = response;

    yield put({
      type: GET_ALL_ADMINS_ACTIVITIES_SUCCESS,
      adminsActivities: adminsActivitiesList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_ADMINS_ACTIVITIES_FAIL,
      err: message,
    });
  }
}

function* adminsActivitiesSaga() {
  yield takeEvery(GET_ALL_ADMINS_ACTIVITIES, getAllAdminsActivities);
}

export default adminsActivitiesSaga;
