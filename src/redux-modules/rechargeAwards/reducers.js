import { unionBy } from 'lodash';
import ROUTES from 'routes';
import update from 'immutability-helper';
import { RECHARGE_AWARDS } from './actions';

const initialState = {
  rechargeAwards: undefined,
  hasMoreRechargeAwards: false,
  applyNewFilter: false,
  getAllRechargeAwards: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addRechargeAward: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  editRechargeAward: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteRechargeAward: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  addNewRechargeAwardModal: { isOpen: false },
  editRechargeAwardModal: { isOpen: false },
  rechargeAwardsReportList: undefined,
  getRechargeAwardsReport: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_RECHARGE_AWARDS,
    GET_ALL_RECHARGE_AWARDS_SUCCESS,
    GET_ALL_RECHARGE_AWARDS_FAIL,
    ADD_RECHARGE_AWARD,
    ADD_RECHARGE_AWARD_SUCCESS,
    ADD_RECHARGE_AWARD_FAIL,
    ADD_RECHARGE_AWARD_RESET,
    DELETE_RECHARGE_AWARD,
    DELETE_RECHARGE_AWARD_SUCCESS,
    DELETE_RECHARGE_AWARD_FAIL,
    TOGGLE_ADD_NEW_RECHARGE_AWARD_MODAL,
    GET_RECHARGE_AWARDS,
    GET_RECHARGE_AWARDS_SUCCESS,
    GET_RECHARGE_AWARDS_FAIL,

    UPDATE_RECHARGE_AWARD,
    UPDATE_RECHARGE_AWARD_SUCCESS,
    UPDATE_RECHARGE_AWARD_FAIL,
    TOGGLE_EDIT_RECHARGE_AWARD_MODAL,
  } = RECHARGE_AWARDS;
  const { RECHARGE_AWARDS: RECHARGE_AWARDS_ROUTE } = ROUTES;
  const {
    rechargeAwards,
    applyNewFilter,
    index,
    isOpen,
    newRechargeAwardInfo,
    rechargeAwardsReportList,
    err,
  } = payload;
  const {
    rechargeAwards: rechargeAwardsState,
    rechargeAwardsReportList: rechargeAwardsReportListState,
    addNewRechargeAwardModal: { isOpen: addNewRechargeAwardModalIsOpenState },
    editRechargeAwardModal: { isOpen: editRechargeAwardModalOpenState },
  } = state;

  switch (type) {
    case GET_ALL_RECHARGE_AWARDS: {
      return {
        ...state,
        applyNewFilter,
        getAllRechargeAwards: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_RECHARGE_AWARDS_SUCCESS: {
      return {
        ...state,
        rechargeAwards:
          rechargeAwardsState && !applyNewFilter
            ? unionBy(rechargeAwardsState, rechargeAwards, '_id')
            : rechargeAwards,
        hasMoreRechargeAwards: rechargeAwards.length === 20,
        getAllRechargeAwards: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_RECHARGE_AWARDS_FAIL: {
      return {
        ...state,
        getAllRechargeAwards: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_RECHARGE_AWARD: {
      return {
        ...state,
        addRechargeAward: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_RECHARGE_AWARD_SUCCESS: {
      rechargeAwardsState.unshift(newRechargeAwardInfo);

      return {
        ...state,
        rechargeAwards: rechargeAwardsState,
        addRechargeAward: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewRechargeAwardModal: { isOpen },
      };
    }
    case ADD_RECHARGE_AWARD_FAIL: {
      return {
        ...state,
        addRechargeAward: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_RECHARGE_AWARD_RESET: {
      return {
        ...state,
        addRechargeAward: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_RECHARGE_AWARD: {
      return {
        ...state,
        deleteRechargeAward: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_RECHARGE_AWARD_SUCCESS: {
      if (index !== undefined) rechargeAwardsState.splice(index, 1);
      else window.location.pathname = RECHARGE_AWARDS_ROUTE;

      return {
        ...state,
        rechargeAwards: rechargeAwardsState,
        deleteRechargeAward: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case DELETE_RECHARGE_AWARD_FAIL: {
      return {
        ...state,
        deleteRechargeAward: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_ADD_NEW_RECHARGE_AWARD_MODAL: {
      return {
        ...state,
        addNewRechargeAwardModal: {
          isOpen: !addNewRechargeAwardModalIsOpenState,
        },
      };
    }
    case GET_RECHARGE_AWARDS: {
      return {
        ...state,
        applyNewFilter,
        getRechargeAwardsReport: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_RECHARGE_AWARDS_SUCCESS: {
      return {
        ...state,
        rechargeAwardsReportList:
          rechargeAwardsReportListState && !applyNewFilter
            ? unionBy(rechargeAwardsReportListState, rechargeAwardsReportList, '_id')
            : rechargeAwardsReportList,
        hasMoreRechargeAwards: rechargeAwardsReportList.length === 20,
        getRechargeAwardsReport: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_RECHARGE_AWARDS_FAIL: {
      return {
        ...state,
        getRechargeAwardsReport: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }

    case UPDATE_RECHARGE_AWARD: {
      return {
        ...state,
        editRechargeAward: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_RECHARGE_AWARD_SUCCESS: {
      const updatedRechargeAwards = update(rechargeAwardsState, {
        [index]: { $set: { ...newRechargeAwardInfo } },
      });
      return {
        ...state,
        posts: updatedRechargeAwards,
        editRechargeAward: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editRechargeAwardModal: {
          isOpen: !editRechargeAwardModalOpenState,
        },
      };
    }
    case UPDATE_RECHARGE_AWARD_FAIL: {
      return {
        ...state,
        editRechargeAward: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_EDIT_RECHARGE_AWARD_MODAL: {
      return {
        ...state,
        editRechargeAwardModal: { isOpen: !editRechargeAwardModalOpenState },
      };
    }
    default:
      return state;
  }
};
