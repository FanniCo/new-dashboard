import { takeEvery, takeLatest, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { RECHARGE_AWARDS } from './actions';

const {
  GET_ALL_RECHARGE_AWARDS,
  GET_ALL_RECHARGE_AWARDS_SUCCESS,
  GET_ALL_RECHARGE_AWARDS_FAIL,
  ADD_RECHARGE_AWARD,
  ADD_RECHARGE_AWARD_SUCCESS,
  ADD_RECHARGE_AWARD_FAIL,
  DELETE_RECHARGE_AWARD,
  DELETE_RECHARGE_AWARD_SUCCESS,
  DELETE_RECHARGE_AWARD_FAIL,
  GET_RECHARGE_AWARDS,
  GET_RECHARGE_AWARDS_SUCCESS,
  GET_RECHARGE_AWARDS_FAIL,

  UPDATE_RECHARGE_AWARD,
  UPDATE_RECHARGE_AWARD_SUCCESS,
  UPDATE_RECHARGE_AWARD_FAIL,
} = RECHARGE_AWARDS;

function* getAllRechargeAwards(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { rechargeAwards } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = rechargeAwards.general;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else if (options[key] && options[key] !== '') {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allRechargeAwards } = response;

    yield put({
      type: GET_ALL_RECHARGE_AWARDS_SUCCESS,
      rechargeAwards: allRechargeAwards,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;
    yield put({
      type: GET_ALL_RECHARGE_AWARDS_FAIL,
      err: message,
    });
  }
}

function* addRechargeAward(payload) {
  const { newRechargeAwardInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { rechargeAwards } = Urls;
  const url = `${rechargeAwards.general}`;
  const api = new Api();
  const body = {
    ...newRechargeAwardInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newRechargeAwardInfoResponse } = response;

    yield put({
      type: ADD_RECHARGE_AWARD_SUCCESS,
      newRechargeAwardInfo: newRechargeAwardInfoResponse,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_RECHARGE_AWARD_FAIL,
      err: message,
    });
  }
}
function* updateRechargeAward(payload) {
  const { newRechargeAwardInfo, rechargeAwardId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { rechargeAwards } = Urls;
  const url = `${rechargeAwards.general}/${rechargeAwardId}`;
  const api = new Api();
  const body = {
    ...newRechargeAwardInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: UPDATE_RECHARGE_AWARD_SUCCESS,
      newRechargeAwardInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_RECHARGE_AWARD_FAIL,
      err: message,
    });
  }
}

function* deleteRechargeAward(payload) {
  const { rechargeAwardId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { rechargeAwards } = Urls;
  const url = `${rechargeAwards.general}/${rechargeAwardId}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.delete(url, undefined, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_RECHARGE_AWARD_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_RECHARGE_AWARD_FAIL,
      err: message,
    });
  }
}

function* getRechargeAwardsReport(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { rechargeAwards } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = rechargeAwards.rechargeAwardReport;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else if (options[key] && options[key] !== '') {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: rechargeAwardsReportList } = response;

    yield put({
      type: GET_RECHARGE_AWARDS_SUCCESS,
      rechargeAwardsReportList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_RECHARGE_AWARDS_FAIL,
      err: message,
    });
  }
}

function* workersSaga() {
  yield takeLatest(GET_ALL_RECHARGE_AWARDS, getAllRechargeAwards);
  yield takeEvery(ADD_RECHARGE_AWARD, addRechargeAward);
  yield takeEvery(UPDATE_RECHARGE_AWARD, updateRechargeAward);
  yield takeEvery(DELETE_RECHARGE_AWARD, deleteRechargeAward);
  yield takeLatest(GET_RECHARGE_AWARDS, getRechargeAwardsReport);
}

export default workersSaga;
