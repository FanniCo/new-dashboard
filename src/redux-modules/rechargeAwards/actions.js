export const RECHARGE_AWARDS = {
  GET_ALL_RECHARGE_AWARDS: 'GET_ALL_RECHARGE_AWARDS',
  GET_ALL_RECHARGE_AWARDS_SUCCESS: 'GET_ALL_RECHARGE_AWARDS_SUCCESS',
  GET_ALL_RECHARGE_AWARDS_FAIL: 'GET_ALL_RECHARGE_AWARDS_FAIL',
  ADD_RECHARGE_AWARD: 'ADD_RECHARGE_AWARD',
  ADD_RECHARGE_AWARD_SUCCESS: 'ADD_RECHARGE_AWARD_SUCCESS',
  ADD_RECHARGE_AWARD_FAIL: 'ADD_RECHARGE_AWARD_FAIL',
  ADD_RECHARGE_AWARD_RESET: 'ADD_RECHARGE_AWARD_RESET',
  DELETE_RECHARGE_AWARD: 'DELETE_RECHARGE_AWARD',
  DELETE_RECHARGE_AWARD_SUCCESS: 'DELETE_RECHARGE_AWARD_SUCCESS',
  DELETE_RECHARGE_AWARD_FAIL: 'DELETE_RECHARGE_AWARD_FAIL',
  TOGGLE_ADD_NEW_RECHARGE_AWARD_MODAL: 'TOGGLE_ADD_NEW_RECHARGE_AWARD_MODAL',
  TOGGLE_EDIT_RECHARGE_AWARD_MODAL: 'TOGGLE_EDIT_RECHARGE_AWARD_MODAL',
  GET_RECHARGE_AWARDS: 'GET_RECHARGE_AWARDS',
  GET_RECHARGE_AWARDS_SUCCESS: 'GET_RECHARGE_AWARDS_SUCCESS',
  GET_RECHARGE_AWARDS_FAIL: 'GET_RECHARGE_AWARDS_FAIL',
  UPDATE_RECHARGE_AWARD: 'UPDATE_RECHARGE_AWARD',
  UPDATE_RECHARGE_AWARD_SUCCESS: 'UPDATE_RECHARGE_AWARD_SUCCESS',
  UPDATE_RECHARGE_AWARD_FAIL: 'UPDATE_RECHARGE_AWARD_FAIL',
};

export const getAllRechargeAwards = (options, applyNewFilter = false) => ({
  type: RECHARGE_AWARDS.GET_ALL_RECHARGE_AWARDS,
  options,
  applyNewFilter,
});

export const getAllRechargeAwardsSuccess = (rechargeAwards, applyNewFilter = false) => ({
  type: RECHARGE_AWARDS.GET_ALL_RECHARGE_AWARDS_SUCCESS,
  rechargeAwards,
  applyNewFilter,
});

export const getAllRechargeAwardsFail = err => ({
  type: RECHARGE_AWARDS.GET_ALL_RECHARGE_AWARDS_FAIL,
  err,
});

export const addRechargeAward = newRechargeAwardInfo => ({
  type: RECHARGE_AWARDS.ADD_RECHARGE_AWARD,
  newRechargeAwardInfo,
});

export const addRechargeAwardSuccess = newRechargeAwardInfo => ({
  type: RECHARGE_AWARDS.ADD_RECHARGE_AWARD_SUCCESS,
  newRechargeAwardInfo,
});

export const addRechargeAwardFail = err => ({
  type: RECHARGE_AWARDS.ADD_WORKER_FAIL,
  err,
});

export const updateRechargeAward = (newRechargeAwardInfo, rechargeAwardId, index) => ({
  type: RECHARGE_AWARDS.UPDATE_RECHARGE_AWARD,
  newRechargeAwardInfo,
  rechargeAwardId,
  index,
});

export const updateRechargeAwardSuccess = (newRechargeAwardInfo, index) => ({
  type: RECHARGE_AWARDS.UPDATE_RECHARGE_AWARD_SUCCESS,
  newRechargeAwardInfo,
  index,
});

export const updateRechargeAwardFail = err => ({
  type: RECHARGE_AWARDS.UPDATE_RECHARGE_AWARD_FAIL,
  err,
});

export const addRechargeAwardReset = () => ({
  type: RECHARGE_AWARDS.ADD_RECHARGE_AWARD_RESET,
});

export const deleteRechargeAward = (rechargeAwardId, index) => ({
  type: RECHARGE_AWARDS.DELETE_RECHARGE_AWARD,
  rechargeAwardId,
  index,
});

export const deleteRechargeAwardSuccess = index => ({
  type: RECHARGE_AWARDS.DELETE_RECHARGE_AWARD_SUCCESS,
  index,
});

export const deleteRechargeAwardFail = err => ({
  type: RECHARGE_AWARDS.DELETE_RECHARGE_AWARD_FAIL,
  err,
});

export const toggleAddNewRechargeAwardModal = () => ({
  type: RECHARGE_AWARDS.TOGGLE_ADD_NEW_RECHARGE_AWARD_MODAL,
});

export const openEditRechargeAwardModal = () => ({
  type: RECHARGE_AWARDS.TOGGLE_EDIT_RECHARGE_AWARD_MODAL,
});

export const getRechargeAwards = (options, applyNewFilter = false) => ({
  type: RECHARGE_AWARDS.GET_RECHARGE_AWARDS,
  options,
  applyNewFilter,
});

export const getRechargeAwardsSuccess = (rechargeAwardsReportList, applyNewFilter = false) => ({
  type: RECHARGE_AWARDS.GET_RECHARGE_AWARDS_SUCCESS,
  rechargeAwardsReportList,
  applyNewFilter,
});

export const getRechargeAwardsFail = err => ({
  type: RECHARGE_AWARDS.GET_RECHARGE_AWARDS_FAIL,
  err,
});
