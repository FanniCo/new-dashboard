import { takeEvery, takeLatest, put, call, race, fork, take } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import ls from 'local-storage';
import ROUTES from 'routes/index';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { COLORS } from 'components/theme/colors';
import { FONT_WEIGHTS } from 'components/theme/fonts';
import { fireNotification } from 'utils/shared/helpers';
import React from 'react';
import socketIOClient from 'socket.io-client';
import { delay, eventChannel } from 'redux-saga';
import { HyperLink, HyperLinkText } from 'components/shared';
import { USER } from './actions';

const {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  GET_ALL_ADMINS,
  GET_ALL_ADMINS_SUCCESS,
  GET_ALL_ADMINS_FAIL,
  CREATE_ADMIN,
  CREATE_ADMIN_SUCCESS,
  CREATE_ADMIN_FAIL,
  DELETE_ADMIN,
  DELETE_ADMIN_SUCCESS,
  DELETE_ADMIN_FAIL,
  CHANGE_ADMIN_PASSWORD,
  CHANGE_ADMIN_PASSWORD_SUCCESS,
  CHANGE_ADMIN_PASSWORD_FAIL,
  ADD_PERMISSIONS_TO_USER,
  ADD_PERMISSIONS_TO_USER_SUCCESS,
  ADD_PERMISSIONS_TO_USER_FAIL,
  EXPORT_FILE,
  EXPORT_FILE_SUCCESS,
  EXPORT_FILE_FAIL,
  SOCKET_IO_SERVER_ON,
  SOCKET_IO_SERVER_OFF,

  GET_ALL_HIS_NOTIFICATIONS,
  GET_ALL_HIS_NOTIFICATIONS_SUCCESS,
  GET_ALL_HIS_NOTIFICATIONS_FAIL,
  UPDATE_ALL_HIS_NOTIFICATIONS,
} = USER;

function* login(payload) {
  const { DASHBOARD } = ROUTES;
  const {
    userCredentials: { userName, password },
    historyPush,
  } = payload;
  const { user } = Urls;
  const api = new Api();
  const url = user.login;
  const body = {
    username: userName,
    password,
    IDFA: 'test',
  };
  const response = yield api.post(url, body);

  if (Status.isSuccess(response.status)) {
    const {
      response: { plainUser, token },
    } = response;
    const { TOKEN, PLAIN_USER } = COOKIES_KEYS;

    Cookies.set(TOKEN, token, { expires: 100 });
    ls.set(PLAIN_USER, plainUser);

    yield put({
      type: LOGIN_SUCCESS,
      user: plainUser,
    });

    yield historyPush(DASHBOARD);
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: LOGIN_FAIL,
      err: message,
    });
  }
}

function* getAllAdmins(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = `${user.general}?types=admin`;

  Object.keys(options).forEach(key => {
    url = `${url}&${key}=${options[key]}`;
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allAdmins } = response;

    yield put({
      type: GET_ALL_ADMINS_SUCCESS,
      admins: allAdmins,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_ADMINS_FAIL,
      err: message,
    });
  }
}

function* createAdmin(payload) {
  const { newAdminInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = {
    ...newAdminInfo,
  };
  const url = user.createAdmin;
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: admin } = response;
    yield put({
      type: CREATE_ADMIN_SUCCESS,
      admin,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CREATE_ADMIN_FAIL,
      err: message,
    });
  }
}

function* deleteAdmin(payload) {
  const { adminId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = user.deleteUser;
  const api = new Api();
  const body = {
    userId: adminId,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.delete(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_ADMIN_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_ADMIN_FAIL,
      err: message,
    });
  }
}

function* changeAdminPassword(payload) {
  const { newPasswordInfo } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const url = user.changeAdminPassword;
  const api = new Api();
  const body = {
    ...newPasswordInfo,
  };
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: CHANGE_ADMIN_PASSWORD_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CHANGE_ADMIN_PASSWORD_FAIL,
      err: message,
    });
  }
}

function* addPermissionsToUser(payload) {
  const { userId, permissions } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = {
    userId,
    permissions,
  };
  const url = user.addPermissionsToUser;
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: ADD_PERMISSIONS_TO_USER_SUCCESS,
      userId,
      permissions,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_PERMISSIONS_TO_USER_FAIL,
      err: message,
    });
  }
}

function* exportFile(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { exportUrl } = payload;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(exportUrl, header);

  if (Status.isSuccess(response.status)) {
    // download .csv file
    const downloadLink = document.createElement('a');
    const blob = new Blob(['\ufeff', response.response], {
      type: 'text/csv',
    });
    const url = URL.createObjectURL(blob);
    downloadLink.href = url;
    downloadLink.download = 'data.csv';

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);

    yield put({
      type: EXPORT_FILE_SUCCESS,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: EXPORT_FILE_FAIL,
      err: message,
    });
  }
}

function connectToSocketIo(payload) {
  const { baseUrl } = Urls;

  const socket = socketIOClient(baseUrl);
  return new Promise(resolve => {
    socket.on('connect', () => {
      socket.emit('room', payload.user.username);
      resolve(socket);
    });
  });
}

const reconnect = () => {
  const { requests } = Urls;
  const url = requests.getAllRequests;

  const socket = socketIOClient(url);
  return new Promise(resolve => {
    socket.on('reconnect', () => {
      resolve(socket);
    });
  });
};

const disconnect = () => {
  const { requests } = Urls;
  const url = requests.getAllRequests;

  const socket = socketIOClient(url);
  return new Promise(resolve => {
    socket.on('disconnect', () => {
      resolve(socket);
    });
  });
};
const createSocketChannel = sendSocket =>
  eventChannel(emit => {
    sendSocket.on('notification', data => {
      emit({
        type: UPDATE_ALL_HIS_NOTIFICATIONS,
        data: [
          {
            _id: Date.now(),
            title: { ar: data.body },
            createdAt: Date.now(),
            url: data.url,
          },
        ],
        key: 'notifications',
      });
      data.url = (
        <HyperLink href={`/${data.url}`} target="_blank">
          <HyperLinkText cursor="pointer" color={COLORS.SUCCESS} fontWeight={FONT_WEIGHTS.NORMAL}>
            اعرض التفاصيل{' '}
          </HyperLinkText>
        </HyperLink>
      );
      fireNotification(data, 'info');
    });

    return () => {
      sendSocket.off('notification', () => {});
    };
  });

const listenDisconnectSaga = function*() {
  while (true) {
    yield call(disconnect);
    yield put({ type: SOCKET_IO_SERVER_OFF });
  }
};

const listenConnectSaga = function*() {
  while (true) {
    yield call(reconnect);
    yield put({ type: SOCKET_IO_SERVER_ON });
  }
};

function* connect(payload) {
  try {
    const { timeout } = yield race({
      connected: call(connectToSocketIo, payload),
      timeout: delay(20000),
    });

    if (timeout) {
      yield put({ type: SOCKET_IO_SERVER_OFF });
    }

    const createdSocket = yield call(connectToSocketIo, payload);
    const socketChannel = yield call(createSocketChannel, createdSocket);
    yield fork(listenDisconnectSaga);
    yield fork(listenConnectSaga);
    yield put({ type: SOCKET_IO_SERVER_ON });
    while (true) {
      const payload = yield take(socketChannel);
      yield put({ type: payload.type, [payload.key]: payload.data });
    }
  } catch (error) {
    console.log(error);
  }
}

function* getAdminAllHisNotifications(payload) {
  const { options } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = user.getAdminMentions;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: notifications } = response;

    yield put({
      type: GET_ALL_HIS_NOTIFICATIONS_SUCCESS,
      notifications,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_HIS_NOTIFICATIONS_FAIL,
      err: message,
    });
  }
}

function* userSaga() {
  yield takeEvery(LOGIN, login);
  yield takeLatest(GET_ALL_ADMINS, getAllAdmins);
  yield takeEvery(CREATE_ADMIN, createAdmin);
  yield takeEvery(DELETE_ADMIN, deleteAdmin);
  yield takeEvery(CHANGE_ADMIN_PASSWORD, changeAdminPassword);
  yield takeEvery(ADD_PERMISSIONS_TO_USER, addPermissionsToUser);
  yield takeEvery(EXPORT_FILE, exportFile);
  yield takeEvery(LOGIN_SUCCESS, connect);
  yield takeEvery(GET_ALL_HIS_NOTIFICATIONS, getAdminAllHisNotifications);
}

export default userSaga;
export const getUserByUsername = async function(username) {
  if (!username) {
    return { name: '', _id: '' };
  }
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { user } = Urls;
  const api = new Api();
  const url = `${user.getUserByUsername}/${username}?role=worker`;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = await api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: users } = response;
    return users;
  }
  const {
    response: { message },
  } = response;

  return { name: message, _id: '' };
};
