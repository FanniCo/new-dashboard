import { unionBy } from 'lodash';
import update from 'immutability-helper';
import ROUTES from 'routes';
import { USER } from './actions';

const initialState = {
  user: undefined,
  admins: undefined,
  hasMoreAdmins: true,
  applyNewFilter: false,
  login: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getAllAdmins: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  createAdmin: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteAdmin: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  changeAdminPassword: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addPermissionsToUser: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  exportFile: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addEditAdminModal: { isOpen: false },
  changeAdminPasswordModal: { isOpen: false },
  getAllHisNotification: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  notificationsList: [],
};

export default (state = initialState, { type, ...payload }) => {
  const {
    LOGIN,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    GET_ALL_ADMINS,
    GET_ALL_ADMINS_SUCCESS,
    GET_ALL_ADMINS_FAIL,
    CREATE_ADMIN,
    CREATE_ADMIN_SUCCESS,
    CREATE_ADMIN_FAIL,
    DELETE_ADMIN,
    DELETE_ADMIN_SUCCESS,
    DELETE_ADMIN_FAIL,
    CHANGE_ADMIN_PASSWORD,
    CHANGE_ADMIN_PASSWORD_SUCCESS,
    CHANGE_ADMIN_PASSWORD_FAIL,
    CHANGE_ADMIN_PASSWORD_RESET,
    ADD_PERMISSIONS_TO_USER,
    ADD_PERMISSIONS_TO_USER_SUCCESS,
    ADD_PERMISSIONS_TO_USER_FAIL,
    EXPORT_FILE,
    EXPORT_FILE_SUCCESS,
    EXPORT_FILE_FAIL,
    TOGGLE_ADD_EDIT_ADMIN_MODAL,
    TOGGLE_CHANGE_ADMIN_PASSWORD_MODAL,

    GET_ALL_HIS_NOTIFICATIONS,
    GET_ALL_HIS_NOTIFICATIONS_SUCCESS,
    GET_ALL_HIS_NOTIFICATIONS_FAIL,
    UPDATE_ALL_HIS_NOTIFICATIONS,
  } = USER;
  const { ADMINS: ADMINS_ROUTE } = ROUTES;
  const {
    userCredentials,
    user,
    admins,
    permissions,
    applyNewFilter,
    admin,
    userId,
    isOpen,
    index,
    notifications,
    err,
  } = payload;
  const {
    admins: adminsState,
    addEditAdminModal: { isOpen: addEditAdminModalIsOpenState },
    changeAdminPasswordModal: { isOpen: changeAdminPasswordModalIsOpenState },
    notificationsList: notificationsListState,
  } = state;

  switch (type) {
    case LOGIN: {
      return {
        ...state,
        userCredentials,
        login: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        user,
        login: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case LOGIN_FAIL: {
      return {
        ...state,
        login: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_ALL_ADMINS: {
      return {
        ...state,
        applyNewFilter,
        getAllAdmins: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_ADMINS_SUCCESS: {
      return {
        ...state,
        admins: adminsState && !applyNewFilter ? unionBy(adminsState, admins, '_id') : admins,
        hasMoreAdmins: admins.length === 20,
        getAllAdmins: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_ADMINS_FAIL: {
      return {
        ...state,
        getAllAdmins: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case CREATE_ADMIN: {
      return {
        ...state,
        createAdmin: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CREATE_ADMIN_SUCCESS: {
      adminsState.unshift(admin);

      return {
        ...state,
        admins: adminsState,
        createAdmin: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addEditAdminModal: { isOpen },
      };
    }
    case CREATE_ADMIN_FAIL: {
      return {
        ...state,
        createAdmin: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case DELETE_ADMIN: {
      return {
        ...state,
        deleteAdmin: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_ADMIN_SUCCESS: {
      if (index !== undefined) adminsState.splice(index, 1);
      else window.location.pathname = ADMINS_ROUTE;

      return {
        ...state,
        admins: adminsState,
        deleteAdmin: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_ADMIN_FAIL: {
      return {
        ...state,
        deleteAdmin: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case CHANGE_ADMIN_PASSWORD: {
      return {
        ...state,
        changeAdminPassword: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CHANGE_ADMIN_PASSWORD_SUCCESS: {
      return {
        ...state,
        changeAdminPassword: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        changeAdminPasswordModal: { isOpen },
      };
    }
    case CHANGE_ADMIN_PASSWORD_FAIL: {
      return {
        ...state,
        changeAdminPassword: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case CHANGE_ADMIN_PASSWORD_RESET: {
      return {
        ...state,
        changeAdminPassword: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_PERMISSIONS_TO_USER: {
      return {
        ...state,
        addPermissionsToUser: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_PERMISSIONS_TO_USER_SUCCESS: {
      const updatedAdminIndex = adminsState.findIndex(adminState => adminState._id === userId);

      return {
        ...state,
        admins: update(adminsState, {
          [updatedAdminIndex]: { permissions: { adminPermissions: { $set: permissions } } },
        }),
        addPermissionsToUser: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addEditAdminModal: { isOpen },
      };
    }
    case ADD_PERMISSIONS_TO_USER_FAIL: {
      return {
        ...state,
        addPermissionsToUser: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case EXPORT_FILE: {
      return {
        ...state,
        exportFile: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EXPORT_FILE_SUCCESS: {
      return {
        ...state,
        exportFile: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case EXPORT_FILE_FAIL: {
      return {
        ...state,
        exportFile: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case TOGGLE_CHANGE_ADMIN_PASSWORD_MODAL: {
      return {
        ...state,
        changeAdminPasswordModal: {
          isOpen: !changeAdminPasswordModalIsOpenState,
        },
      };
    }
    case TOGGLE_ADD_EDIT_ADMIN_MODAL: {
      return {
        ...state,
        addEditAdminModal: {
          isOpen: !addEditAdminModalIsOpenState,
        },
      };
    }

    case GET_ALL_HIS_NOTIFICATIONS: {
      return {
        ...state,
        getAllHisNotification: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_HIS_NOTIFICATIONS_SUCCESS: {
      const LastNotificationsItem = {
        _id: 'last_mention',
        title: {
          ar: 'تم عرض كل السجلات',
          en: 'There is no notifications for you for now.',
        },
        createdAt: Date.now(),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
      };
      const allNotifications = unionBy(notificationsListState, notifications, '_id');
      let showViewMore = true;

      if (notifications.length < 5 && notificationsListState.length !== 0) {
        allNotifications.push(LastNotificationsItem);
        showViewMore = false;
      }

      if (notifications.length === 0 && notificationsListState.length === 0) {
        allNotifications.length = 0;
      }

      return {
        ...state,
        notificationsList: allNotifications,
        notificationCountBadge: allNotifications.length,
        showViewMore,
        getAllHisNotification: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_ALL_HIS_NOTIFICATIONS: {
      const allNotifications = unionBy(notifications, notificationsListState, '_id');

      return {
        ...state,
        notificationsList: allNotifications,
        notificationCountBadge: allNotifications.length,
        getAllHisNotification: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_HIS_NOTIFICATIONS_FAIL: {
      const allNotifications = unionBy(
        notificationsListState,
        [
          {
            _id: 'last_mention',
            title: { ar: err },
            createdAt: Date.now(),
          },
        ],
        '_id',
      );
      return {
        ...state,
        notificationsList: allNotifications,
        showViewMore: false,
        getAllHisNotification: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: err },
        },
      };
    }
    default:
      return state;
  }
};
