export const USER = {
  LOGIN: 'LOGIN',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAIL: 'LOGIN_FAIL',
  GET_ALL_ADMINS: 'GET_ALL_ADMINS',
  GET_ALL_ADMINS_SUCCESS: 'GET_ALL_ADMINS_SUCCESS',
  GET_ALL_ADMINS_FAIL: 'GET_ALL_ADMINS_FAIL',
  CREATE_ADMIN: 'CREATE_ADMIN',
  CREATE_ADMIN_SUCCESS: 'CREATE_ADMIN_SUCCESS',
  CREATE_ADMIN_FAIL: 'CREATE_ADMIN_FAIL',
  DELETE_ADMIN: 'DELETE_ADMIN',
  DELETE_ADMIN_SUCCESS: 'DELETE_ADMIN_SUCCESS',
  DELETE_ADMIN_FAIL: 'DELETE_ADMIN_FAIL',
  CHANGE_ADMIN_PASSWORD: 'CHANGE_ADMIN_PASSWORD',
  CHANGE_ADMIN_PASSWORD_SUCCESS: 'CHANGE_ADMIN_PASSWORD_SUCCESS',
  CHANGE_ADMIN_PASSWORD_FAIL: 'CHANGE_ADMIN_PASSWORD_FAIL',
  CHANGE_ADMIN_PASSWORD_RESET: 'CHANGE_ADMIN_PASSWORD_RESET',
  ADD_PERMISSIONS_TO_USER: 'ADD_PERMISSIONS_TO_USER',
  ADD_PERMISSIONS_TO_USER_SUCCESS: 'ADD_PERMISSIONS_TO_USER_SUCCESS',
  ADD_PERMISSIONS_TO_USER_FAIL: 'ADD_PERMISSIONS_TO_USER_FAIL',
  EXPORT_FILE: 'EXPORT_FILE',
  EXPORT_FILE_SUCCESS: 'EXPORT_FILE_SUCCESS',
  EXPORT_FILE_FAIL: 'EXPORT_FILE_FAIL',
  TOGGLE_ADD_EDIT_ADMIN_MODAL: 'TOGGLE_ADD_EDIT_ADMIN_MODAL',
  TOGGLE_CHANGE_ADMIN_PASSWORD_MODAL: 'TOGGLE_CHANGE_ADMIN_PASSWORD_MODAL',
  SOCKET_IO_SERVER_ON: 'SOCKET_IO_SERVER_ON',
  SOCKET_IO_SERVER_OFF: 'SOCKET_IO_SERVER_OFF',
  GET_ALL_HIS_NOTIFICATIONS: 'GET_ALL_HIS_NOTIFICATIONS',
  GET_ALL_HIS_NOTIFICATIONS_SUCCESS: 'GET_ALL_HIS_NOTIFICATIONS_SUCCESS',
  GET_ALL_HIS_NOTIFICATIONS_FAIL: 'GET_ALL_HIS_NOTIFICATIONS_FAIL',
  UPDATE_ALL_HIS_NOTIFICATIONS: 'UPDATE_ALL_HIS_NOTIFICATIONS',
};

export const login = (userCredentials, historyPush) => ({
  type: USER.LOGIN,
  userCredentials,
  historyPush,
});

export const loginSuccess = user => ({
  type: USER.LOGIN_SUCCESS,
  user,
});

export const loginFail = err => ({
  type: USER.LOGIN_FAIL,
  err,
});

export const getAllAdmins = (options, applyNewFilter: false) => ({
  type: USER.GET_ALL_ADMINS,
  options,
  applyNewFilter,
});

export const getAllAdminsSuccess = (admins, applyNewFilter: false) => ({
  type: USER.GET_ALL_ADMINS_SUCCESS,
  admins,
  applyNewFilter,
});

export const getAllAdminsFail = err => ({
  type: USER.GET_ALL_ADMINS_FAIL,
  err,
});

export const createAdmin = newAdminInfo => ({
  type: USER.CREATE_ADMIN,
  newAdminInfo,
});

export const createAdminSuccess = admin => ({
  type: USER.CREATE_ADMIN_SUCCESS,
  admin,
});

export const createAdminFail = err => ({
  type: USER.CREATE_ADMIN_FAIL,
  err,
});

export const deleteAdmin = (adminId, index) => ({
  type: USER.DELETE_ADMIN,
  adminId,
  index,
});

export const deleteAdminSuccess = index => ({
  type: USER.DELETE_ADMIN_SUCCESS,
  index,
});

export const deleteAdminFail = err => ({
  type: USER.DELETE_ADMIN_FAIL,
  err,
});

export const changeAdminPassword = newPasswordInfo => ({
  type: USER.CHANGE_ADMIN_PASSWORD,
  newPasswordInfo,
});

export const changeAdminPasswordSuccess = () => ({
  type: USER.CHANGE_ADMIN_PASSWORD_SUCCESS,
});

export const changeAdminPasswordFail = err => ({
  type: USER.CHANGE_ADMIN_PASSWORD_FAIL,
  err,
});

export const changeAdminPasswordReset = () => ({
  type: USER.CHANGE_ADMIN_PASSWORD_RESET,
});

export const addPermissionsToUser = (userId, permissions) => ({
  type: USER.ADD_PERMISSIONS_TO_USER,
  userId,
  permissions,
});

export const addPermissionsToUserSuccess = (userId, permissions) => ({
  type: USER.ADD_PERMISSIONS_TO_USER_SUCCESS,
  userId,
  permissions,
});

export const addPermissionsToUserFail = err => ({
  type: USER.ADD_PERMISSIONS_TO_USER_FAIL,
  err,
});

export const exportFile = exportUrl => ({
  type: USER.EXPORT_FILE,
  exportUrl,
});

export const exportFileSuccess = () => ({
  type: USER.EXPORT_FILE_SUCCESS,
});

export const exportFileFail = err => ({
  type: USER.EXPORT_FILE_FAIL,
  err,
});

export const toggleAddEditAdminModal = () => ({
  type: USER.TOGGLE_ADD_EDIT_ADMIN_MODAL,
});

export const toggleChangeAdminPasswordModal = () => ({
  type: USER.TOGGLE_CHANGE_ADMIN_PASSWORD_MODAL,
});

export const socketIoDidnotConnectToServer = () => ({
  type: USER.SOCKET_IO_SERVER_OFF,
});

export const socketIoConnectedToServer = () => ({
  type: USER.SOCKET_IO_SERVER_ON,
});

export const getAdminAllNotifications = options => ({
  type: USER.GET_ALL_HIS_NOTIFICATIONS,
  options,
});

export const getAdminAllNotificationsSuccess = notifications => ({
  type: USER.GET_ALL_HIS_NOTIFICATIONS_SUCCESS,
  notifications,
});

export const getAdminAllNotificationsFail = err => ({
  type: USER.GET_ALL_HIS_NOTIFICATIONS_FAIL,
  err,
});

export const UPDATEAdminAllNotifications = notifications => ({
  type: USER.UPDATE_ALL_HIS_NOTIFICATIONS,
  notifications,
});
