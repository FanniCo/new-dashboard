import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { CUSTOMERS_ADS } from './actions';

const {
  GET_ALL_CUSTOMERS_ADS,
  GET_ALL_CUSTOMERS_ADS_SUCCESS,
  GET_ALL_CUSTOMERS_ADS_FAIL,
  ADD_NEW_CUSTOMERS_AD,
  ADD_NEW_CUSTOMERS_AD_SUCCESS,
  ADD_NEW_CUSTOMERS_AD_FAIL,
  DELETE_CUSTOMERS_AD,
  DELETE_CUSTOMERS_AD_SUCCESS,
  DELETE_CUSTOMERS_AD_FAIL,
  UPDATE_CUSTOMERS_AD,
  UPDATE_CUSTOMERS_AD_SUCCESS,
  UPDATE_CUSTOMERS_AD_FAIL,
} = CUSTOMERS_ADS;

function* getAllCustomersAds(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { customersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = customersAds.getAllCustomersAds;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: customersAdsList } = response;

    yield put({
      type: GET_ALL_CUSTOMERS_ADS_SUCCESS,
      customersAds: customersAdsList.ads,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_CUSTOMERS_ADS_FAIL,
      err: message,
    });
  }
}

function* addNewCustomersAd(payload) {
  const { newCustomersAd } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { customersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newCustomersAd;

  const url = customersAds.addNewCustomersAd;
  const response = yield api.post(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    const { response: newCustomersAdInfo } = response;

    yield put({
      type: ADD_NEW_CUSTOMERS_AD_SUCCESS,
      newCustomersAdInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_CUSTOMERS_AD_FAIL,
      err: message,
    });
  }
}

function* deleteCustomersAd(payload) {
  const { customersAdId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { customersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = customersAds.delete;
  const response = yield api.delete(`${url}/${customersAdId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_CUSTOMERS_AD_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_CUSTOMERS_AD_FAIL,
      err: message,
    });
  }
}

function* updateCustomersAd(payload) {
  const { newCustomersAd, customersAdId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { customersAds } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newCustomersAd;

  const url = `${customersAds.update}/${customersAdId}`;
  const response = yield api.patch(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    const { response: newCustomersAdInfo } = response;
    yield put({
      type: UPDATE_CUSTOMERS_AD_SUCCESS,
      newCustomersAdInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_CUSTOMERS_AD_FAIL,
      err: message,
    });
  }
}

function* customersAdsSaga() {
  yield takeEvery(GET_ALL_CUSTOMERS_ADS, getAllCustomersAds);
  yield takeEvery(ADD_NEW_CUSTOMERS_AD, addNewCustomersAd);
  yield takeEvery(DELETE_CUSTOMERS_AD, deleteCustomersAd);
  yield takeEvery(UPDATE_CUSTOMERS_AD, updateCustomersAd);
}

export default customersAdsSaga;
