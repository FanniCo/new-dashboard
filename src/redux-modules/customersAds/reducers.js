import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { CUSTOMERS_ADS } from './actions';

const initialState = {
  applyNewFilter: false,
  customersAds: undefined,
  hasMoreCustomersAds: true,
  addNewCustomersAdModal: {
    isOpen: false,
  },
  getAllCustomersAds: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addCustomersAd: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteCustomersAd: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editCustomersAdModal: { isOpen: false },
  updateCustomersAd: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_CUSTOMERS_AD_MODAL,
    GET_ALL_CUSTOMERS_ADS,
    GET_ALL_CUSTOMERS_ADS_SUCCESS,
    GET_ALL_CUSTOMERS_ADS_FAIL,
    ADD_NEW_CUSTOMERS_AD,
    ADD_NEW_CUSTOMERS_AD_SUCCESS,
    ADD_NEW_CUSTOMERS_AD_FAIL,
    DELETE_CUSTOMERS_AD,
    DELETE_CUSTOMERS_AD_SUCCESS,
    DELETE_CUSTOMERS_AD_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_CUSTOMERS_AD_MODAL,
    UPDATE_CUSTOMERS_AD,
    UPDATE_CUSTOMERS_AD_SUCCESS,
    UPDATE_CUSTOMERS_AD_FAIL,
  } = CUSTOMERS_ADS;
  const { customersAds, applyNewFilter, newCustomersAdInfo, index, err } = payload;
  const {
    customersAds: customersAdsState,
    addNewCustomersAdModal: { isOpen: addNewCustomersAdModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editCustomersAdModal: { isOpen: editCustomersAdModalOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_CUSTOMERS_AD_MODAL: {
      return {
        ...state,
        addNewCustomersAdModal: {
          isOpen: !addNewCustomersAdModalIsOpenState,
        },
      };
    }
    case GET_ALL_CUSTOMERS_ADS: {
      return {
        ...state,
        applyNewFilter,
        getAllCustomersAds: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_CUSTOMERS_ADS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        customersAds:
          customersAdsState && !applyNewFilter
            ? unionBy(customersAdsState, customersAds, '_id')
            : customersAds,
        hasMoreCustomersAds: customersAds.length === 20,
        getAllCustomersAds: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, CustomersAd: '' },
        },
      };
    }
    case GET_ALL_CUSTOMERS_ADS_FAIL: {
      return {
        ...state,
        getAllCustomersAds: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_CUSTOMERS_AD: {
      return {
        ...state,
        addCustomersAd: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_CUSTOMERS_AD_SUCCESS: {
      customersAdsState.unshift(newCustomersAdInfo);
      return {
        ...state,
        customersAds: customersAdsState,
        addCustomersAd: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewCustomersAdModal: {
          isOpen: !addNewCustomersAdModalIsOpenState,
        },
      };
    }
    case ADD_NEW_CUSTOMERS_AD_FAIL: {
      return {
        ...state,
        addCustomersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_CUSTOMERS_AD_MODAL: {
      return {
        ...state,
        editCustomersAdModal: { isOpen: !editCustomersAdModalOpenState },
      };
    }
    case DELETE_CUSTOMERS_AD: {
      return {
        ...state,
        deleteCustomersAd: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_CUSTOMERS_AD_SUCCESS: {
      if (index !== undefined) customersAdsState.splice(index, 1);
      return {
        ...state,
        customersAds: customersAdsState,
        deleteCustomersAd: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_CUSTOMERS_AD_FAIL: {
      return {
        ...state,
        deleteCustomersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_CUSTOMERS_AD: {
      return {
        ...state,
        updateCustomersAd: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_CUSTOMERS_AD_SUCCESS: {
      const updatedCustomersAds = update(customersAdsState, {
        [index]: { $set: { ...newCustomersAdInfo } },
      });
      return {
        ...state,
        customersAds: updatedCustomersAds,
        updateCustomersAd: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editCustomersAdModal: {
          isOpen: !editCustomersAdModalOpenState,
        },
      };
    }
    case UPDATE_CUSTOMERS_AD_FAIL: {
      return {
        ...state,
        updateCustomersAd: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
