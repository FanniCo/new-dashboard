export const CUSTOMERS_ADS = {
  TOGGLE_ADD_NEW_CUSTOMERS_AD_MODAL: 'TOGGLE_ADD_NEW_CUSTOMERS_AD_MODAL',
  GET_ALL_CUSTOMERS_ADS: 'GET_ALL_CUSTOMERS_ADS',
  GET_ALL_CUSTOMERS_ADS_SUCCESS: 'GET_ALL_CUSTOMERS_ADS_SUCCESS',
  GET_ALL_CUSTOMERS_ADS_FAIL: 'GET_ALL_CUSTOMERS_ADS_FAIL',
  ADD_NEW_CUSTOMERS_AD: 'ADD_NEW_CUSTOMERS_AD',
  ADD_NEW_CUSTOMERS_AD_SUCCESS: 'ADD_NEW_CUSTOMERS_AD_SUCCESS',
  ADD_NEW_CUSTOMERS_AD_FAIL: 'ADD_NEW_CUSTOMERS_AD_FAIL',
  DELETE_CUSTOMERS_AD: 'DELETE_CUSTOMERS_AD',
  DELETE_CUSTOMERS_AD_SUCCESS: 'DELETE_CUSTOMERS_AD_SUCCESS',
  DELETE_CUSTOMERS_AD_FAIL: 'DELETE_CUSTOMERS_AD_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_CUSTOMERS_AD_MODAL: 'OPEN_EDIT_CUSTOMERS_AD_MODAL',
  UPDATE_CUSTOMERS_AD: 'UPDATE_CUSTOMERS_AD',
  UPDATE_CUSTOMERS_AD_SUCCESS: 'UPDATE_CUSTOMERS_AD_SUCCESS',
  UPDATE_CUSTOMERS_AD_FAIL: 'UPDATE_CUSTOMERS_AD_FAIL',
};

export const toggleAddNewCustomersAdModal = () => ({
  type: CUSTOMERS_ADS.TOGGLE_ADD_NEW_CUSTOMERS_AD_MODAL,
});

export const getAllCustomersAds = (options, applyNewFilter = false) => ({
  type: CUSTOMERS_ADS.GET_ALL_CUSTOMERS_ADS,
  options,
  applyNewFilter,
});

export const getAllCustomersAdsSuccess = (customersAdsList, applyNewFilter: false) => ({
  type: CUSTOMERS_ADS.GET_ALL_CUSTOMERS_ADS_SUCCESS,
  customersAdsList,
  applyNewFilter,
});

export const getAllCustomersAdsFail = err => ({
  type: CUSTOMERS_ADS.GET_ALL_CUSTOMERS_ADS_FAIL,
  err,
});

export const addNewCustomersAd = newCustomersAd => ({
  type: CUSTOMERS_ADS.ADD_NEW_CUSTOMERS_AD,
  newCustomersAd,
});

export const addNewCustomersAdSuccess = newCustomersAdInfo => ({
  type: CUSTOMERS_ADS.ADD_NEW_CUSTOMERS_AD_SUCCESS,
  newCustomersAdInfo,
});

export const addNewCustomersAdFail = err => ({
  type: CUSTOMERS_ADS.ADD_NEW_CUSTOMERS_AD_FAIL,
  err,
});

export const deleteCustomersAd = (customersAdId, index) => ({
  type: CUSTOMERS_ADS.DELETE_CUSTOMERS_AD,
  customersAdId,
  index,
});

export const deleteCustomersAdSuccess = index => ({
  type: CUSTOMERS_ADS.DELETE_CUSTOMERS_AD_SUCCESS,
  index,
});

export const deleteCustomersAdFail = err => ({
  type: CUSTOMERS_ADS.DELETE_CUSTOMERS_AD_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: CUSTOMERS_ADS.OPEN_CONFIRM_MODAL,
});

export const openEditCustomersAdModal = () => ({
  type: CUSTOMERS_ADS.OPEN_EDIT_CUSTOMERS_AD_MODAL,
});

export const updateCustomersAd = (newCustomersAd, customersAdId, index) => ({
  type: CUSTOMERS_ADS.UPDATE_CUSTOMERS_AD,
  newCustomersAd,
  customersAdId,
  index,
});

export const updateCustomersAdSuccess = (newCustomersAdInfo, index) => ({
  type: CUSTOMERS_ADS.UPDATE_CUSTOMERS_AD_SUCCESS,
  newCustomersAdInfo,
  index,
});

export const updateCustomersAdFail = err => ({
  type: CUSTOMERS_ADS.UPDATE_CUSTOMERS_AD_FAIL,
  err,
});
