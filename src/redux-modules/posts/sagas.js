import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { POSTS } from './actions';

const {
  GET_ALL_POSTS,
  GET_ALL_POSTS_SUCCESS,
  GET_ALL_POSTS_FAIL,
  ADD_NEW_POST,
  ADD_NEW_POST_SUCCESS,
  ADD_NEW_POST_FAIL,
  DELETE_POST,
  DELETE_POST_SUCCESS,
  DELETE_POST_FAIL,
  UPDATE_POST,
  UPDATE_POST_SUCCESS,
  UPDATE_POST_FAIL,
} = POSTS;

function* getAllPosts(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { posts } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = posts.getAllPosts;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: postsList } = response;

    yield put({
      type: GET_ALL_POSTS_SUCCESS,
      posts: postsList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_POSTS_FAIL,
      err: message,
    });
  }
}

function* addNewPost(payload) {
  const { newPost } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { posts } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newPost;

  const url = posts.addNewPost;
  const response = yield api.post(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    const { response: newPostInfo } = response;

    yield put({
      type: ADD_NEW_POST_SUCCESS,
      newPostInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_POST_FAIL,
      err: message,
    });
  }
}

function* deletePost(payload) {
  const { postId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { posts } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = posts.delete;
  const response = yield api.delete(`${url}/${postId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_POST_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_POST_FAIL,
      err: message,
    });
  }
}

function* updatePost(payload) {
  const { newPost, postId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { posts } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newPost;

  const url = `${posts.update}/${postId}`;
  const response = yield api.patch(url, body, header, false, 'multiPart');

  if (Status.isSuccess(response.status)) {
    const { response: newPostInfo } = response;
    yield put({
      type: UPDATE_POST_SUCCESS,
      newPostInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_POST_FAIL,
      err: message,
    });
  }
}

function* postsSaga() {
  yield takeEvery(GET_ALL_POSTS, getAllPosts);
  yield takeEvery(ADD_NEW_POST, addNewPost);
  yield takeEvery(DELETE_POST, deletePost);
  yield takeEvery(UPDATE_POST, updatePost);
}

export default postsSaga;
