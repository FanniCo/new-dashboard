import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { POSTS } from './actions';

const initialState = {
  applyNewFilter: false,
  posts: undefined,
  hasMorePosts: true,
  addNewPostModal: {
    isOpen: false,
  },
  getAllPosts: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addPost: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deletePost: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editPostModal: { isOpen: false },
  updatePost: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_POST_MODAL,
    GET_ALL_POSTS,
    GET_ALL_POSTS_SUCCESS,
    GET_ALL_POSTS_FAIL,
    ADD_NEW_POST,
    ADD_NEW_POST_SUCCESS,
    ADD_NEW_POST_FAIL,
    DELETE_POST,
    DELETE_POST_SUCCESS,
    DELETE_POST_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_POST_MODAL,
    UPDATE_POST,
    UPDATE_POST_SUCCESS,
    UPDATE_POST_FAIL,
  } = POSTS;
  const { posts, applyNewFilter, newPostInfo, index, err } = payload;
  const {
    posts: postsState,
    addNewPostModal: { isOpen: addNewPostModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editPostModal: { isOpen: editPostModalOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_POST_MODAL: {
      return {
        ...state,
        addNewPostModal: {
          isOpen: !addNewPostModalIsOpenState,
        },
        addPost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updatePost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_POSTS: {
      return {
        ...state,
        applyNewFilter,
        getAllPosts: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_POSTS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        posts:
          postsState && !applyNewFilter
            ? unionBy(postsState, posts, '_id')
            : posts,
        hasMorePosts: posts.length === 20,
        getAllPosts: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, Post: '' },
        },
      };
    }
    case GET_ALL_POSTS_FAIL: {
      return {
        ...state,
        getAllPosts: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_POST: {
      return {
        ...state,
        addPost: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_POST_SUCCESS: {
      postsState.unshift(newPostInfo);
      return {
        ...state,
        posts: postsState,
        addPost: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewPostModal: {
          isOpen: !addNewPostModalIsOpenState,
        },
      };
    }
    case ADD_NEW_POST_FAIL: {
      return {
        ...state,
        addPost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_POST_MODAL: {
      return {
        ...state,
        editPostModal: { isOpen: !editPostModalOpenState },
        addPost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updatePost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_POST: {
      return {
        ...state,
        deletePost: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_POST_SUCCESS: {
      if (index !== undefined) postsState.splice(index, 1);
      return {
        ...state,
        posts: postsState,
        deletePost: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_POST_FAIL: {
      return {
        ...state,
        deletePost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_POST: {
      return {
        ...state,
        updatePost: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_POST_SUCCESS: {
      const updatedPosts = update(postsState, {
        [index]: { $set: { ...newPostInfo } },
      });
      return {
        ...state,
        posts: updatedPosts,
        updatePost: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editPostModal: {
          isOpen: !editPostModalOpenState,
        },
      };
    }
    case UPDATE_POST_FAIL: {
      return {
        ...state,
        updatePost: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
