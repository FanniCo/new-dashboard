export const POSTS = {
  TOGGLE_ADD_NEW_POST_MODAL: 'TOGGLE_ADD_NEW_POST_MODAL',
  GET_ALL_POSTS: 'GET_ALL_POSTS',
  GET_ALL_POSTS_SUCCESS: 'GET_ALL_POSTS_SUCCESS',
  GET_ALL_POSTS_FAIL: 'GET_ALL_POSTS_FAIL',
  ADD_NEW_POST: 'ADD_NEW_POST',
  ADD_NEW_POST_SUCCESS: 'ADD_NEW_POST_SUCCESS',
  ADD_NEW_POST_FAIL: 'ADD_NEW_POST_FAIL',
  DELETE_POST: 'DELETE_POST',
  DELETE_POST_SUCCESS: 'DELETE_POST_SUCCESS',
  DELETE_POST_FAIL: 'DELETE_POST_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_POST_MODAL: 'OPEN_EDIT_POST_MODAL',
  UPDATE_POST: 'UPDATE_POST',
  UPDATE_POST_SUCCESS: 'UPDATE_POST_SUCCESS',
  UPDATE_POST_FAIL: 'UPDATE_POST_FAIL',
};

export const toggleAddNewPostModal = () => ({
  type: POSTS.TOGGLE_ADD_NEW_POST_MODAL,
});

export const getAllPosts = (options, applyNewFilter = false) => ({
  type: POSTS.GET_ALL_POSTS,
  options,
  applyNewFilter,
});

export const getAllPostsSuccess = (postsList, applyNewFilter: false) => ({
  type: POSTS.GET_ALL_POSTS_SUCCESS,
  postsList,
  applyNewFilter,
});

export const getAllPostsFail = err => ({
  type: POSTS.GET_ALL_POSTS_FAIL,
  err,
});

export const addNewPost = newPost => ({
  type: POSTS.ADD_NEW_POST,
  newPost,
});

export const addNewPostSuccess = newPostInfo => ({
  type: POSTS.ADD_NEW_POST_SUCCESS,
  newPostInfo,
});

export const addNewPostFail = err => ({
  type: POSTS.ADD_NEW_POST_FAIL,
  err,
});

export const deletePost = (postId, index) => ({
  type: POSTS.DELETE_POST,
  postId,
  index,
});

export const deletePostSuccess = index => ({
  type: POSTS.DELETE_POST_SUCCESS,
  index,
});

export const deletePostFail = err => ({
  type: POSTS.DELETE_POST_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: POSTS.OPEN_CONFIRM_MODAL,
});

export const openEditPostModal = () => ({
  type: POSTS.OPEN_EDIT_POST_MODAL,
});

export const updatePost = (newPost, postId, index) => ({
  type: POSTS.UPDATE_POST,
  newPost,
  postId,
  index,
});

export const updatePostSuccess = (newPostInfo, index) => ({
  type: POSTS.UPDATE_POST_SUCCESS,
  newPostInfo,
  index,
});

export const updatePostFail = err => ({
  type: POSTS.UPDATE_POST_FAIL,
  err,
});
