import { unionBy } from 'lodash';
import update from 'immutability-helper';
import { OFFERS } from './actions';

const initialState = {
  applyNewFilter: false,
  offers: undefined,
  hasMoreOffers: true,
  addNewOfferModal: {
    isOpen: false,
  },
  getAllOffers: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addOffer: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteOffer: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  confirmModal: { isOpen: false },
  editOfferModal: { isOpen: false },
  updateOffer: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    TOGGLE_ADD_NEW_OFFER_MODAL,
    GET_ALL_OFFERS,
    GET_ALL_OFFERS_SUCCESS,
    GET_ALL_OFFERS_FAIL,
    ADD_NEW_OFFER,
    ADD_NEW_OFFER_SUCCESS,
    ADD_NEW_OFFER_FAIL,
    DELETE_OFFER,
    DELETE_OFFER_SUCCESS,
    DELETE_OFFER_FAIL,
    OPEN_CONFIRM_MODAL,
    OPEN_EDIT_OFFER_MODAL,
    UPDATE_OFFER,
    UPDATE_OFFER_SUCCESS,
    UPDATE_OFFER_FAIL,
  } = OFFERS;
  const { offers, applyNewFilter, newOfferInfo, index, err } = payload;
  const {
    offers: offersState,
    addNewOfferModal: { isOpen: addNewOfferModalIsOpenState },
    confirmModal: { isOpen: isConfirmModalOpenState },
    editOfferModal: { isOpen: editOfferModalOpenState },
  } = state;

  switch (type) {
    case TOGGLE_ADD_NEW_OFFER_MODAL: {
      return {
        ...state,
        addNewOfferModal: {
          isOpen: !addNewOfferModalIsOpenState,
          addOffer: {
            isFetching: false,
            isSuccess: false,
            isFail: { isError: false, message: '' },
          },
          updateOffer: {
            isFetching: false,
            isSuccess: false,
            isFail: { isError: false, message: '' },
          },
        },
      };
    }
    case GET_ALL_OFFERS: {
      return {
        ...state,
        applyNewFilter,
        getAllOffers: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_OFFERS_SUCCESS: {
      return {
        ...state,
        applyNewFilter: false,
        offers: offersState && !applyNewFilter ? unionBy(offersState, offers, '_id') : offers,
        hasMoreOffers: offers.length === 20,
        getAllOffers: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, Offer: '' },
        },
      };
    }
    case GET_ALL_OFFERS_FAIL: {
      return {
        ...state,
        getAllOffers: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_NEW_OFFER: {
      return {
        ...state,
        addOffer: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case ADD_NEW_OFFER_SUCCESS: {
      offersState.unshift(newOfferInfo);
      return {
        ...state,
        offers: offersState,
        addOffer: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        addNewOfferModal: {
          isOpen: !addNewOfferModalIsOpenState,
        },
      };
    }
    case ADD_NEW_OFFER_FAIL: {
      return {
        ...state,
        addOffer: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case OPEN_EDIT_OFFER_MODAL: {
      return {
        ...state,
        editOfferModal: { isOpen: !editOfferModalOpenState },
        addOffer: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
        updateOffer: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_OFFER: {
      return {
        ...state,
        deleteOffer: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_OFFER_SUCCESS: {
      if (index !== undefined) offersState.splice(index, 1);
      return {
        ...state,
        offers: offersState,
        deleteOffer: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen: !isConfirmModalOpenState },
      };
    }
    case DELETE_OFFER_FAIL: {
      return {
        ...state,
        deleteOffer: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_OFFER: {
      return {
        ...state,
        updateOffer: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_OFFER_SUCCESS: {
      const updatedOffers = update(offersState, {
        [index]: { $set: { ...newOfferInfo } },
      });
      return {
        ...state,
        offers: updatedOffers,
        updateOffer: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        editOfferModal: {
          isOpen: !editOfferModalOpenState,
        },
      };
    }
    case UPDATE_OFFER_FAIL: {
      return {
        ...state,
        updateOffer: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
