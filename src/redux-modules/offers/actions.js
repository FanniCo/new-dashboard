export const OFFERS = {
  TOGGLE_ADD_NEW_OFFER_MODAL: 'TOGGLE_ADD_NEW_OFFER_MODAL',
  GET_ALL_OFFERS: 'GET_ALL_OFFERS',
  GET_ALL_OFFERS_SUCCESS: 'GET_ALL_OFFERS_SUCCESS',
  GET_ALL_OFFERS_FAIL: 'GET_ALL_OFFERS_FAIL',
  ADD_NEW_OFFER: 'ADD_NEW_OFFER',
  ADD_NEW_OFFER_SUCCESS: 'ADD_NEW_OFFER_SUCCESS',
  ADD_NEW_OFFER_FAIL: 'ADD_NEW_OFFER_FAIL',
  DELETE_OFFER: 'DELETE_OFFER',
  DELETE_OFFER_SUCCESS: 'DELETE_OFFER_SUCCESS',
  DELETE_OFFER_FAIL: 'DELETE_OFFER_FAIL',
  OPEN_CONFIRM_MODAL: 'OPEN_CONFIRM_MODAL',
  OPEN_EDIT_OFFER_MODAL: 'OPEN_EDIT_OFFER_MODAL',
  UPDATE_OFFER: 'UPDATE_OFFER',
  UPDATE_OFFER_SUCCESS: 'UPDATE_OFFER_SUCCESS',
  UPDATE_OFFER_FAIL: 'UPDATE_OFFER_FAIL',
};

export const toggleAddNewOfferModal = () => ({
  type: OFFERS.TOGGLE_ADD_NEW_OFFER_MODAL,
});

export const getAllOffers = (options, applyNewFilter = false) => ({
  type: OFFERS.GET_ALL_OFFERS,
  options,
  applyNewFilter,
});

export const getAllOffersSuccess = (offersList, applyNewFilter: false) => ({
  type: OFFERS.GET_ALL_OFFERS_SUCCESS,
  offersList,
  applyNewFilter,
});

export const getAllOffersFail = err => ({
  type: OFFERS.GET_ALL_OFFERS_FAIL,
  err,
});

export const addNewOffer = newOffer => ({
  type: OFFERS.ADD_NEW_OFFER,
  newOffer,
});

export const addNewOfferSuccess = newOfferInfo => ({
  type: OFFERS.ADD_NEW_OFFER_SUCCESS,
  newOfferInfo,
});

export const addNewOfferFail = err => ({
  type: OFFERS.ADD_NEW_OFFER_FAIL,
  err,
});

export const deleteOffer = (offerId, index) => ({
  type: OFFERS.DELETE_OFFER,
  offerId,
  index,
});

export const deleteOfferSuccess = index => ({
  type: OFFERS.DELETE_OFFER_SUCCESS,
  index,
});

export const deleteOfferFail = err => ({
  type: OFFERS.DELETE_OFFER_FAIL,
  err,
});

export const openConfirmModal = () => ({
  type: OFFERS.OPEN_CONFIRM_MODAL,
});

export const openEditOfferModal = () => ({
  type: OFFERS.OPEN_EDIT_OFFER_MODAL,
});

export const updateOffer = (newOffer, offerId, index) => ({
  type: OFFERS.UPDATE_OFFER,
  newOffer,
  offerId,
  index,
});

export const updateOfferSuccess = (newOfferInfo, index) => ({
  type: OFFERS.UPDATE_OFFER_SUCCESS,
  newOfferInfo,
  index,
});

export const updateOfferFail = err => ({
  type: OFFERS.UPDATE_OFFER_FAIL,
  err,
});
