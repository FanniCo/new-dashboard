import { takeEvery, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import { OFFERS } from './actions';

const {
  GET_ALL_OFFERS,
  GET_ALL_OFFERS_SUCCESS,
  GET_ALL_OFFERS_FAIL,
  ADD_NEW_OFFER,
  ADD_NEW_OFFER_SUCCESS,
  ADD_NEW_OFFER_FAIL,
  DELETE_OFFER,
  DELETE_OFFER_SUCCESS,
  DELETE_OFFER_FAIL,
  UPDATE_OFFER,
  UPDATE_OFFER_SUCCESS,
  UPDATE_OFFER_FAIL,
} = OFFERS;

function* getAllOffers(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { offers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  let url = offers.getAllOffers;
  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const {
      response: { offers: offersList },
    } = response;

    yield put({
      type: GET_ALL_OFFERS_SUCCESS,
      offers: offersList,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_OFFERS_FAIL,
      err: message,
    });
  }
}

function* addNewOffer(payload) {
  const { newOffer } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { offers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newOffer;

  const url = offers.addNewOffer;
  const response = yield api.post(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newOfferInfo } = response;

    yield put({
      type: ADD_NEW_OFFER_SUCCESS,
      newOfferInfo,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_NEW_OFFER_FAIL,
      err: message,
    });
  }
}

function* deleteOffer(payload) {
  const { offerId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { offers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const url = offers.delete;
  const response = yield api.delete(`${url}/${offerId}`, null, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_OFFER_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_OFFER_FAIL,
      err: message,
    });
  }
}

function* updateOffer(payload) {
  const { newOffer, offerId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { offers } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const body = newOffer;

  const url = `${offers.update}/${offerId}`;
  const response = yield api.patch(url, body, header);

  if (Status.isSuccess(response.status)) {
    const { response: newOfferInfo } = response;
    yield put({
      type: UPDATE_OFFER_SUCCESS,
      newOfferInfo,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_OFFER_FAIL,
      err: message,
    });
  }
}

function* offersSaga() {
  yield takeEvery(GET_ALL_OFFERS, getAllOffers);
  yield takeEvery(ADD_NEW_OFFER, addNewOffer);
  yield takeEvery(DELETE_OFFER, deleteOffer);
  yield takeEvery(UPDATE_OFFER, updateOffer);
}

export default offersSaga;
