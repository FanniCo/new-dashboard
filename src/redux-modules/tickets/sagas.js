import { takeEvery, takeLatest, put } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { Api, Urls, Status } from 'utils/api';
import { COOKIES_KEYS } from 'utils/constants';
import querystring from 'querystring';
import { TICKETS } from './actions';

const {
  GET_ALL_TICKETS,
  GET_ALL_TICKETS_SUCCESS,
  GET_ALL_TICKETS_FAIL,
  GET_TICKET_DETAILS,
  GET_TICKET_DETAILS_SUCCESS,
  GET_TICKET_DETAILS_FAIL,
  UPDATE_TICKET,
  UPDATE_TICKET_SUCCESS,
  UPDATE_TICKET_FAIL,
  DELETE_TICKET,
  DELETE_TICKET_SUCCESS,
  DELETE_TICKET_FAIL,
  ADD_TICKET_COMMENT,
  GET_TICKETS_COUNT_IN_EACH_STATUS,
  CLOSE_COMPLAINT,
  GET_TICKETS_REPORT,
  GET_TICKETS_STATISTICS,
} = TICKETS;

function* getAllTickets(payload) {
  const { options, applyNewFilter } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  let url = tickets.general;

  Object.keys(options).forEach((key, index) => {
    if (index === 0) {
      url = `${url}?${key}=${options[key]}`;
    } else {
      url = `${url}&${key}=${options[key]}`;
    }
  });

  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const { response: allWorkers } = response;

    yield put({
      type: GET_ALL_TICKETS_SUCCESS,
      tickets: allWorkers,
      applyNewFilter,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_ALL_TICKETS_FAIL,
      err: message,
    });
  }
}

function* getTicketDetails(payload) {
  const { ticketId } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const url = `${tickets.general}/${ticketId}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(url, header);

  if (Status.isSuccess(response.status)) {
    const ticketDetails = response.response;

    yield put({
      type: GET_TICKET_DETAILS_SUCCESS,
      ticketDetails,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_TICKET_DETAILS_FAIL,
      err: message,
    });
  }
}

function* updateTicket(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const api = new Api();
  const {
    ticketInfo: { _id, statuses, ...restTicketInfo },
  } = payload;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const url = `${tickets.general}/${_id}`;
  const body = { ...restTicketInfo };

  const response = yield api.patch(url, body, header);
  const responseStatus = response.status;
  const ticket = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: UPDATE_TICKET_SUCCESS,
      ticket,
    });

    window.location.reload();
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: UPDATE_TICKET_FAIL,
      err: message,
    });
  }
}

function* deleteTicket(payload) {
  const { ticketId, index } = payload;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const url = `${tickets.general}/${ticketId}`;
  const api = new Api();
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];

  const response = yield api.delete(url, undefined, header);

  if (Status.isSuccess(response.status)) {
    yield put({
      type: DELETE_TICKET_SUCCESS,
      index,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: DELETE_TICKET_FAIL,
      err: message,
    });
  }
}

function* addComment(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const { ticketId, commentInfo } = payload;
  const api = new Api();
  const url = `${tickets.general}/${ticketId}`;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, commentInfo, header);
  const responseStatus = response.status;
  const { comments } = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: ADD_TICKET_COMMENT.SUCCESS,
      comments,
      ticketId,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: ADD_TICKET_COMMENT.FAIL,
      err: message,
    });
  }
}

function* closeComplaint(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const { ticketId, complainId, data } = payload;
  const api = new Api();
  const url = `${tickets.general}/${ticketId}/complaints/${complainId}/close`;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.patch(url, data, header);
  const responseStatus = response.status;

  if (Status.isSuccess(responseStatus)) {
    yield put({ type: CLOSE_COMPLAINT.SUCCESS });
    window.location.reload(false);
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: CLOSE_COMPLAINT.FAIL,
      err: message,
    });
  }
}

function* getTicketsReport(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const { options } = payload;
  const api = new Api();
  const url = `${reports.tickets}?${querystring.stringify(options)}`;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(url, header);
  const responseStatus = response.status;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: GET_TICKETS_REPORT.SUCCESS,
      tickets: response.response,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_TICKETS_REPORT.FAIL,
      err: message,
    });
  }
}

function* getTicketsStatistics(payload) {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { reports } = Urls;
  const { options } = payload;
  const api = new Api();
  const url = `${reports.ticketsStatisticsUrl}?${querystring.stringify(options)}`;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(url, header);
  const responseStatus = response.status;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: GET_TICKETS_STATISTICS.SUCCESS,
      ticketsStatistics: response.response,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_TICKETS_STATISTICS.FAIL,
      err: message,
    });
  }
}

function* getTicketsCountInEachStatus() {
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const { tickets } = Urls;
  const api = new Api();
  const url = tickets.ticketsCountInEachStatus;
  const header = [
    {
      key: 'token',
      value: token,
    },
  ];
  const response = yield api.get(url, header);
  const responseStatus = response.status;
  const ticketsCountInEachStatus = response.response;

  if (Status.isSuccess(responseStatus)) {
    yield put({
      type: GET_TICKETS_COUNT_IN_EACH_STATUS.SUCCESS,
      ticketsCountInEachStatus,
    });
  } else {
    const {
      response: { message },
    } = response;

    yield put({
      type: GET_TICKETS_COUNT_IN_EACH_STATUS.FAIL,
      err: message,
    });
  }
}

function* ticketsSaga() {
  yield takeLatest(GET_ALL_TICKETS, getAllTickets);
  yield takeEvery(GET_TICKET_DETAILS, getTicketDetails);
  yield takeEvery(UPDATE_TICKET, updateTicket);
  yield takeEvery(DELETE_TICKET, deleteTicket);
  yield takeEvery(ADD_TICKET_COMMENT.LOAD, addComment);
  yield takeEvery(CLOSE_COMPLAINT.LOAD, closeComplaint);
  yield takeEvery(GET_TICKETS_COUNT_IN_EACH_STATUS.LOAD, getTicketsCountInEachStatus);
  yield takeEvery(GET_TICKETS_REPORT.LOAD, getTicketsReport);
  yield takeEvery(GET_TICKETS_STATISTICS.LOAD, getTicketsStatistics);
}

export default ticketsSaga;
