import { findIndex, unionBy } from 'lodash';
import update from 'immutability-helper';
import ROUTES from 'routes';
import { TICKETS } from './actions';

const initialState = {
  tickets: undefined,
  hasMoreTickets: true,
  applyNewFilter: false,
  ticketDetails: undefined,
  ticketsStatistics: undefined,
  getAllTickets: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getTicketDetails: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  updateTicket: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  deleteTicket: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  addComment: {
    commentInfo: undefined,
    addCommentState: undefined,
  },
  confirmModal: { isOpen: false },
  getTicketsCountInEachStatus: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  ticketId: undefined,
  closeComplaint: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getTicketsReport: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
  getTicketsStatistics: {
    isFetching: false,
    isSuccess: false,
    isFail: { isError: false, message: '' },
  },
};

export default (state = initialState, { type, ...payload }) => {
  const {
    GET_ALL_TICKETS,
    GET_ALL_TICKETS_SUCCESS,
    GET_ALL_TICKETS_FAIL,
    GET_TICKET_DETAILS,
    GET_TICKET_DETAILS_SUCCESS,
    GET_TICKET_DETAILS_FAIL,
    UPDATE_TICKET,
    UPDATE_TICKET_SUCCESS,
    UPDATE_TICKET_FAIL,
    DELETE_TICKET,
    DELETE_TICKET_SUCCESS,
    DELETE_TICKET_FAIL,
    ADD_TICKET_COMMENT,
    GET_TICKETS_COUNT_IN_EACH_STATUS,
    CLOSE_COMPLAINT,
    GET_TICKETS_REPORT,
    GET_TICKETS_STATISTICS,
  } = TICKETS;
  const { TICKETS: TICKETS_ROUTE } = ROUTES;
  const {
    tickets,
    ticketsStatistics,
    applyNewFilter,
    ticket,
    ticketDetails,
    comments,
    commentInfo,
    isOpen,
    index,
    ticketsCountInEachStatus,
    ticketId,
    err,
  } = payload;
  const { tickets: ticketsState, ticketDetails: ticketDetailsState } = state;

  switch (type) {
    case GET_ALL_TICKETS: {
      return {
        ...state,
        applyNewFilter,
        getAllTickets: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_TICKETS_SUCCESS: {
      return {
        ...state,
        tickets: ticketsState && !applyNewFilter ? unionBy(ticketsState, tickets, '_id') : tickets,
        hasMoreTickets: tickets.length === 20,
        getAllTickets: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_ALL_TICKETS_FAIL: {
      return {
        ...state,
        getAllTickets: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_TICKET_DETAILS: {
      return {
        ...state,
        getTicketDetails: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKET_DETAILS_SUCCESS: {
      return {
        ...state,
        ticketDetails,
        getTicketDetails: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKET_DETAILS_FAIL: {
      return {
        ...state,
        getTicketDetails: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case UPDATE_TICKET: {
      return {
        ...state,
        updateTicket: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_TICKET_SUCCESS: {
      return {
        ...state,
        ticketDetails: ticket,
        updateTicket: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case UPDATE_TICKET_FAIL: {
      return {
        ...state,
        updateTicket: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case DELETE_TICKET: {
      return {
        ...state,
        deleteTicket: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case DELETE_TICKET_SUCCESS: {
      if (index !== undefined) ticketsState.splice(index, 1);
      else window.location.pathname = TICKETS_ROUTE;

      return {
        ...state,
        tickets: ticketsState,
        deleteTicket: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
        confirmModal: { isOpen },
      };
    }
    case DELETE_TICKET_FAIL: {
      return {
        ...state,
        deleteTicket: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case ADD_TICKET_COMMENT.LOAD: {
      return {
        ...state,
        addComment: {
          commentInfo,
          addCommentState: ADD_TICKET_COMMENT.LOAD,
        },
      };
    }
    case ADD_TICKET_COMMENT.SUCCESS: {
      const foundIndex = findIndex(ticketsState, ticketState => ticketState._id === ticketId);

      const updatedTickets =
        foundIndex >= 0
          ? update(ticketsState, { [foundIndex]: { comments: { $set: comments } } })
          : ticketsState;

      const updatedTicketDetails =
        index !== undefined ? ticketDetailsState : { ...ticketDetailsState, comments };

      return {
        ...state,
        tickets: updatedTickets,
        ticketDetails: updatedTicketDetails,
        addComment: {
          commentInfo,
          addCommentState: ADD_TICKET_COMMENT.SUCCESS,
        },
      };
    }
    case ADD_TICKET_COMMENT.FAIL: {
      return {
        ...state,
        addComment: {
          commentInfo: undefined,
          addCommentState: ADD_TICKET_COMMENT.FAIL,
          error: err,
        },
      };
    }
    case GET_TICKETS_COUNT_IN_EACH_STATUS.LOAD: {
      return {
        ...state,
        getTicketsCountInEachStatus: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKETS_COUNT_IN_EACH_STATUS.SUCCESS: {
      return {
        ...state,
        ticketsCountInEachStatus,
        getTicketsCountInEachStatus: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKETS_COUNT_IN_EACH_STATUS.FAIL: {
      return {
        ...state,
        getTicketsCountInEachStatus: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case CLOSE_COMPLAINT.LOAD: {
      return {
        ...state,
        closeComplaint: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CLOSE_COMPLAINT.SUCCESS: {
      return {
        ...state,
        closeComplaint: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case CLOSE_COMPLAINT.FAIL: {
      return {
        ...state,
        closeComplaint: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_TICKETS_REPORT.LOAD: {
      return {
        ...state,
        getTicketsReport: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKETS_REPORT.SUCCESS: {
      return {
        ...state,
        hasMoreTickets: tickets.length === 20,
        tickets: ticketsState && !applyNewFilter ? unionBy(ticketsState, tickets, '_id') : tickets,
        getTicketsReport: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKETS_REPORT.FAIL: {
      return {
        ...state,
        getTicketsReport: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    case GET_TICKETS_STATISTICS.LOAD: {
      return {
        ...state,
        getTicketsStatistics: {
          isFetching: true,
          isSuccess: false,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKETS_STATISTICS.SUCCESS: {
      return {
        ...state,
        ticketsStatistics,
        getTicketsStatistics: {
          isFetching: false,
          isSuccess: true,
          isFail: { isError: false, message: '' },
        },
      };
    }
    case GET_TICKETS_STATISTICS.FAIL: {
      return {
        ...state,
        getTicketsStatistics: {
          isFetching: false,
          isSuccess: false,
          isFail: { isError: true, message: err },
        },
      };
    }
    default:
      return state;
  }
};
