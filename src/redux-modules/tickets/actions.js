export const TICKETS = {
  GET_ALL_TICKETS: 'GET_ALL_TICKETS',
  GET_ALL_TICKETS_SUCCESS: 'GET_ALL_TICKETS_SUCCESS',
  GET_ALL_TICKETS_FAIL: 'GET_ALL_TICKETS_FAIL',
  GET_TICKET_DETAILS: 'GET_TICKET_DETAILS',
  GET_TICKET_DETAILS_SUCCESS: 'GET_TICKET_DETAILS_SUCCESS',
  GET_TICKET_DETAILS_FAIL: 'GET_TICKET_DETAILS_FAIL',
  UPDATE_TICKET: 'UPDATE_TICKET',
  UPDATE_TICKET_SUCCESS: 'UPDATE_TICKET_SUCCESS',
  UPDATE_TICKET_FAIL: 'UPDATE_TICKET_FAIL',
  DELETE_TICKET: 'DELETE_TICKET',
  DELETE_TICKET_SUCCESS: 'DELETE_TICKET_SUCCESS',
  DELETE_TICKET_FAIL: 'DELETE_TICKET_FAIL',
  ADD_TICKET_COMMENT: {
    LOAD: 'TICKETS_ADD_TICKET_COMMENT_LOAD',
    SUCCESS: 'TICKETS_ADD_TICKET_COMMENT_SUCCESS',
    FAIL: 'TICKETS_ADD_TICKET_COMMENT_FAIL',
  },
  GET_TICKETS_COUNT_IN_EACH_STATUS: {
    LOAD: 'GET_TICKETS_COUNT_IN_EACH_STATUS_LOAD',
    SUCCESS: 'GET_TICKETS_COUNT_IN_EACH_STATUS_SUCCESS',
    FAIL: 'GET_TICKETS_COUNT_IN_EACH_STATUS_FAIL',
  },
  CLOSE_COMPLAINT: {
    LOAD: 'TICKETS_CLOSE_COMPLAINT_LOAD',
    SUCCESS: 'TICKETS_CLOSE_COMPLAINT_SUCCESS',
    FAIL: 'TICKETS_CLOSE_COMPLAINT_FAIL',
  },
  GET_TICKETS_REPORT: {
    LOAD: 'GET_TICKETS_REPORT_LOAD',
    SUCCESS: 'GET_TICKETS_REPORT_SUCCESS',
    FAIL: 'GET_TICKETS_REPORT_FAIL',
  },
  GET_TICKETS_STATISTICS: {
    LOAD: 'GET_TICKETS_STATISTICS_LOAD',
    SUCCESS: 'GET_TICKETS_STATISTICS_SUCCESS',
    FAIL: 'GET_TICKETS_STATISTICS_FAIL',
  },
};

export const getAllTickets = (options, applyNewFilter = false) => ({
  type: TICKETS.GET_ALL_TICKETS,
  options,
  applyNewFilter,
});

export const getAllTicketsSuccess = (tickets, applyNewFilter = false) => ({
  type: TICKETS.GET_ALL_TICKETS_SUCCESS,
  tickets,
  applyNewFilter,
});

export const getAllTicketsFail = err => ({
  type: TICKETS.GET_ALL_TICKETS_FAIL,
  err,
});

export const getTicketDetails = ticketId => ({
  type: TICKETS.GET_TICKET_DETAILS,
  ticketId,
});

export const getTicketDetailsSuccess = ticketDetails => ({
  type: TICKETS.GET_TICKET_DETAILS_SUCCESS,
  ticketDetails,
});

export const getTicketDetailsFail = err => ({
  type: TICKETS.GET_TICKET_DETAILS_FAIL,
  err,
});

export const updateTicket = ticketInfo => ({
  type: TICKETS.UPDATE_TICKET,
  ticketInfo,
});

export const updateTicketSuccess = ticket => ({
  type: TICKETS.UPDATE_TICKET_SUCCESS,
  ticket,
});

export const updateTicketFail = err => ({
  type: TICKETS.UPDATE_TICKET_FAIL,
  err,
});

export const deleteTicket = (ticketId, index) => ({
  type: TICKETS.DELETE_TICKET,
  ticketId,
  index,
});

export const deleteTicketSuccess = index => ({
  type: TICKETS.DELETE_TICKET_SUCCESS,
  index,
});

export const deleteTicketFail = err => ({
  type: TICKETS.DELETE_TICKET_FAIL,
  err,
});

export const addComment = (commentInfo, ticketId) => ({
  type: TICKETS.ADD_TICKET_COMMENT.LOAD,
  ticketId,
  commentInfo,
});

export const addCommentSuccess = (comments, ticketId) => ({
  type: TICKETS.ADD_TICKET_COMMENT.SUCCESS,
  comments,
  ticketId,
});

export const addCommentFail = err => ({
  type: TICKETS.ADD_TICKET_COMMENT.FAIL,
  err,
});

export const getTicketsCountInEachStatus = () => ({
  type: TICKETS.GET_TICKETS_COUNT_IN_EACH_STATUS.LOAD,
});

export const getTicketsCountInEachStatusSuccess = ticketsCountInEachStatus => ({
  type: TICKETS.GET_TICKETS_COUNT_IN_EACH_STATUS.SUCCESS,
  ticketsCountInEachStatus,
});

export const getTicketsCountInEachStatusFail = err => ({
  type: TICKETS.GET_TICKETS_COUNT_IN_EACH_STATUS.FAIL,
  err,
});

export const closeComplaint = (ticketId, complainId, data) => ({
  type: TICKETS.CLOSE_COMPLAINT.LOAD,
  ticketId,
  complainId,
  data,
});

export const closeComplaintSuccess = () => ({
  type: TICKETS.CLOSE_COMPLAINT.SUCCESS,
});

export const closeComplaintFail = err => ({
  type: TICKETS.CLOSE_COMPLAINT.FAIL,
  err,
});

export const getTicketsReport = (options, applyNewFilter = false) => ({
  type: TICKETS.GET_TICKETS_REPORT.LOAD,
  options,
  applyNewFilter,
});

export const getTicketsReportSuccess = (tickets, applyNewFilter = false) => ({
  type: TICKETS.GET_TICKETS_REPORT.SUCCESS,
  tickets,
  applyNewFilter,
});

export const getTicketsReportFail = err => ({
  type: TICKETS.GET_TICKETS_REPORT.FAIL,
  err,
});

export const getTicketsStatistics = (options, applyNewFilter = false) => ({
  type: TICKETS.GET_TICKETS_STATISTICS.LOAD,
  options,
  applyNewFilter,
});

export const getTicketsStatisticsSuccess = (ticketsStatistics, applyNewFilter = false) => ({
  type: TICKETS.GET_TICKETS_STATISTICS.SUCCESS,
  ticketsStatistics,
  applyNewFilter,
});

export const getTicketsStatisticsFail = err => ({
  type: TICKETS.GET_TICKETS_STATISTICS.FAIL,
  err,
});
