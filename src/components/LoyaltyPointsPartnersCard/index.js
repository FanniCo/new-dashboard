import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const LoyaltyPointsPartnerCard = props => {
  const {
    loyaltyPointsPartner: { consumedCustomer, partner, partnerCode, createdAt },
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const loyaltyPointsPartnerDate = new Date(createdAt);

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {loyaltyPointsPartnerDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اسم االعميل :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {consumedCustomer.name}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            رقم الهاتف :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {consumedCustomer.username}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اسم الشريك :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {partner}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            كود الخصم :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {partnerCode}
          </Text>
        </Flex>
      </CardBody>
    </Card>
  );
};
LoyaltyPointsPartnerCard.displayName = 'LoyaltyPointsPartnerCard';

LoyaltyPointsPartnerCard.propTypes = {
  loyaltyPointsPartner: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
};

LoyaltyPointsPartnerCard.defaultProps = {};

export default LoyaltyPointsPartnerCard;
