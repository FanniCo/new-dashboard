import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { CITIES, FIELDS } from 'utils/constants';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getFieldIcon } from 'utils/shared/style';
import ReactTooltip from 'react-tooltip';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const MessageCard = props => {
  const {
    message,
    message: { _id, title, description, fields, cities, createdBy, createdAt },
    index,
    user: { permissions },
    handleToggleConfirmModal,
    handleToggleEditWorkerModal,
  } = props;
  const {
    BRANDING_GREEN,
    GREY_LIGHT,
    DISABLED,
    GREY_DARK,
    PLACEHOLDER_GREY,
    BRANDING_BLUE,
    LIGHT_RED,
  } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const messageDate = new Date(createdAt);
  const deleteMessageTooltip = 'مسح الرسالة';
  const editMessageTooltip = 'تعديل الرسالة';

  const fieldsRenderer = fields.map(field => (
    <Flex key={field} ml={4} mb={1}>
      <Box>
        <FontAwesomeIcon
          icon={getFieldIcon(field)}
          color={COLORS_VALUES[BRANDING_GREEN]}
          size="sm"
        />
      </Box>
      <CardLabel
        py={1}
        px={2}
        ml={2}
        type={BODY}
        border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
        color={COLORS_VALUES[DISABLED]}
        fontWeight={NORMAL}
      >
        {FIELDS[field].ar}
      </CardLabel>
    </Flex>
  ));

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {messageDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العنوان :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {title.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الوصف :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {description.ar}
          </Text>
        </Flex>
        {cities && cities.length ? (
          <Flex flexDirection="row-reverse" flexWrap="wrap" py={1} px={2} mb={1}>
            {cities.map((city, index) => {
              const keyIndex = index;

              return (
                <CardLabel
                  key={`city_${keyIndex}`}
                  py={1}
                  px={2}
                  ml={1}
                  mb={1}
                  type={BODY}
                  color={COLORS_VALUES[DISABLED]}
                  bg={BRANDING_BLUE}
                >
                  {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
                </CardLabel>
              );
            })}
          </Flex>
        ) : (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <CardLabel
              py={1}
              px={2}
              ml={1}
              type={BODY}
              color={COLORS_VALUES[GREY_DARK]}
              bg={PLACEHOLDER_GREY}
            >
              لم يتم اختيار مدينة
            </CardLabel>
          </Flex>
        )}
        {fields && fields.length ? (
          <Flex flexDirection="row-reverse" flexWrap="wrap" py={1} px={2} mb={1}>
            {fieldsRenderer}
          </Flex>
        ) : (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <CardLabel
              py={1}
              px={2}
              ml={1}
              type={BODY}
              color={COLORS_VALUES[GREY_DARK]}
              bg={PLACEHOLDER_GREY}
            >
              لم يتم اختيار خدمة
            </CardLabel>
          </Flex>
        )}
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
      </CardBody>
      {permissions &&
        (permissions.includes('Delete Message') || permissions.includes('Update Message')) && (
          <CardFooter
            flexDirection="row-reverse"
            justifyContent="space-between"
            flexWrap="wrap"
            p={2}
          >
            <Flex px={2}>
              {permissions && permissions.includes('Delete Message') && (
                <>
                  <ReactTooltip id={`deleteMessage_${_id}`} type="error" effect="solid">
                    <span>{deleteMessageTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`deleteMessage_${_id}`}
                      data-tip
                      onClick={() => handleToggleConfirmModal(_id, index)}
                      icon={faTrashAlt}
                      color={COLORS_VALUES[LIGHT_RED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
              {permissions && permissions.includes('Update Message') && (
                <>
                  <ReactTooltip id={`editWorker_${_id}`} type="light" effect="solid">
                    <span>{editMessageTooltip}</span>
                  </ReactTooltip>
                  <Box>
                    <CallToActionIcon
                      data-for={`editWorker_${_id}`}
                      data-tip
                      onClick={() => handleToggleEditWorkerModal(index, message)}
                      icon={faPen}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
            </Flex>
          </CardFooter>
      )}
    </Card>
  );
};
MessageCard.displayName = 'MessageCard';

MessageCard.propTypes = {
  message: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditWorkerModal: PropTypes.func,
};

MessageCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditWorkerModal: () => {},
};

export default MessageCard;
