import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import CommissionCard from 'components/CommissionCard';
import CommissionFormModal from 'components/CommissionFormModal';
import ConfirmModal from 'components/ConfirmModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { FONT_TYPES } from 'components/theme/fonts';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class Commissions extends Component {
  getLoadingCommissions = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingCommissionsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingCommissions(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      commissionsList,
      isAddNewCommissionModalOpen,
      isConfirmModalOpen,
      isDeleteCommissionFetching,
      deleteCommissionAction,
      handleToggleConfirmModal,
      isEditCommissionModalOpen,
      handleToggleEditCommissionModal,
      commissionDetails,
      commissionIndex,
      isGetAllCommissionsFetching,
      isGetAllCommissionsSuccess,
      isGetAllCommissionsError,
      isGetAllCommissionsErrorMessage,
      applyNewFilter,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isDeleteCommissionError,
      isDeleteCommissionErrorMessage,
    } = this.props;

    let commissionsRenderer;

    if (
      (isGetAllCommissionsFetching && (!commissionsList || applyNewFilter)) ||
      (!isGetAllCommissionsFetching && !isGetAllCommissionsSuccess && !isGetAllCommissionsError)
    ) {
      commissionsRenderer = this.createLoadingCommissionsList();
    } else if (isGetAllCommissionsError) {
      commissionsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllCommissionsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!commissionsList.length) {
      commissionsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (commissionsList.length) {
      commissionsRenderer = commissionsList.map((commission, index) => {
        const { _id } = commission;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <CommissionCard
              m={2}
              index={index}
              user={user}
              commission={commission}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditCommissionModal={handleToggleEditCommissionModal}
              deleteCommissionAction={deleteCommissionAction}
            />
          </Box>
        );
      });
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {commissionsRenderer}
        {isAddNewCommissionModalOpen && <CommissionFormModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            isFail={isDeleteCommissionError}
            errorMessage={isDeleteCommissionErrorMessage}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={confirmModalFunction}
            isConfirming={isDeleteCommissionFetching}
          />
        )}
        {isEditCommissionModalOpen && (
          <CommissionFormModal
            commissionDetails={commissionDetails}
            commissionIndex={commissionIndex}
          />
        )}
      </Flex>
    );
  }
}

Commissions.displayName = 'CommissionsList';

Commissions.propTypes = {
  user: PropTypes.shape({}),
  commissionsList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewCommissionModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  deleteCommissionAction: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isDeleteCommissionFetching: PropTypes.bool,
  handleToggleEditCommissionModal: PropTypes.func,
  isEditCommissionModalOpen: PropTypes.bool,
  commissionDetails: PropTypes.shape({}),
  commissionIndex: PropTypes.number,
  isGetAllCommissionsFetching: PropTypes.bool,
  isGetAllCommissionsSuccess: PropTypes.bool,
  isGetAllCommissionsError: PropTypes.bool,
  isGetAllCommissionsErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  isDeleteCommissionError: PropTypes.bool,
  isDeleteCommissionErrorMessage: PropTypes.string,
};

Commissions.defaultProps = {
  user: undefined,
  commissionsList: undefined,
  isAddNewCommissionModalOpen: false,
  handleToggleConfirmModal: () => {},
  deleteCommissionAction: () => {},
  isConfirmModalOpen: false,
  // deleteCustomers: () => {},
  isDeleteCommissionFetching: false,
  handleToggleEditCommissionModal: () => {},
  isEditCommissionModalOpen: false,
  commissionDetails: {},
  commissionIndex: undefined,
  isGetAllCommissionsFetching: false,
  isGetAllCommissionsSuccess: false,
  isGetAllCommissionsError: false,
  isGetAllCommissionsErrorMessage: undefined,
  applyNewFilter: false,
  confirmModalHeader: undefined,
  confirmModalText: undefined,
  confirmModalFunction: () => {},
  isDeleteCommissionError: false,
  isDeleteCommissionErrorMessage: undefined,
};

export default Commissions;
