import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { FIELDS, CITIES } from 'utils/constants';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import {
  convertClientToWorker,
  toggleConvertClientToWorkerModal,
} from 'redux-modules/clients/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const fieldsKeys = Object.keys(FIELDS);
const citiesKeys = Object.keys(CITIES);
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));

const ConvertClientToWorkerFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class ConvertClientToWorkerModal extends Component {
  static propTypes = {
    clientDetails: PropTypes.shape({}),
    isConvertClientToWorkerFetching: PropTypes.bool,
    isConvertClientToWorkerError: PropTypes.bool,
    convertClientToWorkerErrorMessage: PropTypes.string,
    convertClientToWorker: PropTypes.func,
    toggleConvertClientToWorkerModal: PropTypes.func,
    isConvertClientToWorkerModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    clientDetails: undefined,
    isConvertClientToWorkerFetching: false,
    isConvertClientToWorkerError: false,
    convertClientToWorkerErrorMessage: '',
    convertClientToWorker: () => {},
    toggleConvertClientToWorkerModal: () => {},
    isConvertClientToWorkerModalOpen: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      field: null,
      governmentId: '',
      city: null,
      sponsor: '',
    };
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeFieldsSelect = field => {
    this.setState({ field });
  };

  handleChangeCitiesSelect = city => {
    this.setState({ city });
  };

  onSubmitConvertClientToWorkerForm = e => {
    e.preventDefault();

    const {
      clientDetails: { _id },
      convertClientToWorker: convertClientToWorkerAction,
    } = this.props;
    const { field, governmentId, city, sponsor } = this.state;
    const clientFields = field.value;
    const clientCity = city.value;
    const convertInfo = {
      customerId: _id,
      field: clientFields,
      governmentId: convertArabicNumbersToEnglish(governmentId),
      city: clientCity,
      sponsor,
    };

    convertClientToWorkerAction(convertInfo);
  };

  render() {
    const {
      isConvertClientToWorkerFetching,
      isConvertClientToWorkerModalOpen,
      toggleConvertClientToWorkerModal: toggleConvertClientToWorkerModalAction,
      isConvertClientToWorkerError,
      convertClientToWorkerErrorMessage,
    } = this.props;
    const { field, governmentId, city, sponsor } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const convertClientToWorkerModalHeader = 'تحويل إلي فني';

    return (
      <Modal
        toggleModal={toggleConvertClientToWorkerModalAction}
        header={convertClientToWorkerModalHeader}
        isOpened={isConvertClientToWorkerModalOpen}
      >
        <ConvertClientToWorkerFormContainer onSubmit={this.onSubmitConvertClientToWorkerForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الهوية
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الهوية"
                  ref={inputField => {
                    this.governmentIdField = inputField;
                  }}
                  width={1}
                  value={governmentId}
                  onChange={value => this.handleInputFieldChange('governmentId', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الكفيل
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم الكفيل"
                  ref={inputField => {
                    this.sponsorField = inputField;
                  }}
                  width={1}
                  value={sponsor}
                  onChange={value => this.handleInputFieldChange('sponsor', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={field}
                  onChange={this.handleChangeFieldsSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدينة
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={city}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitConvertClientToWorkerForm}
                disabled={
                  isConvertClientToWorkerFetching ||
                  !field ||
                  !city ||
                  governmentId === '' ||
                  sponsor === ''
                }
                isLoading={isConvertClientToWorkerFetching}
              >
                تحويل إلي فني
              </Button>
              {isConvertClientToWorkerError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {convertClientToWorkerErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </ConvertClientToWorkerFormContainer>
      </Modal>
    );
  }
}
ConvertClientToWorkerModal.displayName = 'ConvertClientToWorkerModal';

const mapStateToProps = state => ({
  isConvertClientToWorkerFetching: state.clients.convertClientToWorker.isFetching,
  isConvertClientToWorkerError: state.clients.convertClientToWorker.isFail.isError,
  convertClientToWorkerErrorMessage: state.clients.convertClientToWorker.isFail.message,
  isConvertClientToWorkerModalOpen: state.clients.convertClientToWorkerModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      convertClientToWorker,
      toggleConvertClientToWorkerModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ConvertClientToWorkerModal);
