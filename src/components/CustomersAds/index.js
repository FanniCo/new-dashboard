import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import CustomersAdCard from 'components/CustomersAdCard';
import CustomerAdFormModal from 'components/CustomerAdFormModal';
import ConfirmModal from 'components/ConfirmModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { FONT_TYPES } from 'components/theme/fonts';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class CustomersAds extends Component {
  getLoadingCustomersAds = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingCustomersAdsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingCustomersAds(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      customersAdsList,
      isAddNewCustomersAdModalOpen,
      isConfirmModalOpen,
      isDeleteCustomersAdFetching,
      deleteCustomersAdAction,
      handleToggleConfirmModal,
      isEditCustomersAdModalOpen,
      handleToggleEditCustomersAdModal,
      customersAdDetails,
      customersAdIndex,
      isGetAllCustomersAdsFetching,
      isGetAllCustomersAdsSuccess,
      isGetAllCustomersAdsError,
      isGetAllCustomersAdsErrorMessage,
      applyNewFilter,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isDeleteCustomersAdError,
      isDeleteCustomersAdErrorMessage,
    } = this.props;

    let customersAdsRenderer;

    if (
      (isGetAllCustomersAdsFetching && (!customersAdsList || applyNewFilter)) ||
      (!isGetAllCustomersAdsFetching && !isGetAllCustomersAdsSuccess && !isGetAllCustomersAdsError)
    ) {
      customersAdsRenderer = this.createLoadingCustomersAdsList();
    } else if (isGetAllCustomersAdsError) {
      customersAdsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllCustomersAdsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!customersAdsList.length) {
      customersAdsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (customersAdsList.length) {
      customersAdsRenderer = customersAdsList.map((customersAd, index) => {
        const { _id } = customersAd;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <CustomersAdCard
              m={2}
              index={index}
              user={user}
              customersAd={customersAd}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditCustomersAdModal={handleToggleEditCustomersAdModal}
              deleteCustomersAdAction={deleteCustomersAdAction}
            />
          </Box>
        );
      });
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {customersAdsRenderer}
        {isAddNewCustomersAdModalOpen && <CustomerAdFormModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            isFail={isDeleteCustomersAdError}
            errorMessage={isDeleteCustomersAdErrorMessage}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={confirmModalFunction}
            isConfirming={isDeleteCustomersAdFetching}
          />
        )}
        {isEditCustomersAdModalOpen && (
          <CustomerAdFormModal
            customersAdDetails={customersAdDetails}
            customersAdIndex={customersAdIndex}
          />
        )}
      </Flex>
    );
  }
}

CustomersAds.displayName = 'CustomersAdsList';

CustomersAds.propTypes = {
  user: PropTypes.shape({}),
  customersAdsList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewCustomersAdModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  deleteCustomersAdAction: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isDeleteCustomersAdFetching: PropTypes.bool,
  handleToggleEditCustomersAdModal: PropTypes.func,
  isEditCustomersAdModalOpen: PropTypes.bool,
  customersAdDetails: PropTypes.shape({}),
  customersAdIndex: PropTypes.number,
  isGetAllCustomersAdsFetching: PropTypes.bool,
  isGetAllCustomersAdsSuccess: PropTypes.bool,
  isGetAllCustomersAdsError: PropTypes.bool,
  isGetAllCustomersAdsErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  isDeleteCustomersAdError: PropTypes.bool,
  isDeleteCustomersAdErrorMessage: PropTypes.string,
};

CustomersAds.defaultProps = {
  user: undefined,
  customersAdsList: undefined,
  isAddNewCustomersAdModalOpen: false,
  handleToggleConfirmModal: () => {},
  deleteCustomersAdAction: () => {},
  isConfirmModalOpen: false,
  // deleteCustomers: () => {},
  isDeleteCustomersAdFetching: false,
  handleToggleEditCustomersAdModal: () => {},
  isEditCustomersAdModalOpen: false,
  customersAdDetails: {},
  customersAdIndex: undefined,
  isGetAllCustomersAdsFetching: false,
  isGetAllCustomersAdsSuccess: false,
  isGetAllCustomersAdsError: false,
  isGetAllCustomersAdsErrorMessage: undefined,
  applyNewFilter: false,
  confirmModalHeader: undefined,
  confirmModalText: undefined,
  confirmModalFunction: () => {},
  isDeleteCustomersAdError: false,
  isDeleteCustomersAdErrorMessage: undefined,
};

export default CustomersAds;
