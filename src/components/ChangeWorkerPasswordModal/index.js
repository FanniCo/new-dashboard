import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import {
  changeWorkerPassword,
  toggleChangeWorkerPasswordModal,
} from 'redux-modules/workers/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const ChangeWorkerPasswordFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class ChangeWorkerPasswordModal extends Component {
  static propTypes = {
    workerIndex: PropTypes.number,
    workerDetails: PropTypes.shape({}),
    isChangeWorkerPasswordFetching: PropTypes.bool,
    isChangeWorkerPasswordError: PropTypes.bool,
    changeWorkerPasswordErrorMessage: PropTypes.string,
    changeWorkerPassword: PropTypes.func,
    toggleChangeWorkerPasswordModal: PropTypes.func,
    isChangeWorkerPasswordModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    workerIndex: undefined,
    workerDetails: undefined,
    isChangeWorkerPasswordFetching: false,
    isChangeWorkerPasswordError: false,
    changeWorkerPasswordErrorMessage: '',
    changeWorkerPassword: () => {},
    toggleChangeWorkerPasswordModal: () => {},
    isChangeWorkerPasswordModalOpen: false,
  };

  state = {
    password: '',
    confirmPassword: '',
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitChangeWorkerPasswordForm = e => {
    const {
      workerIndex,
      workerDetails: { username },
      changeWorkerPassword: changeWorkerPasswordAction,
    } = this.props;
    const { password, confirmPassword } = this.state;

    e.preventDefault();

    const newPasswordInfo = {
      username,
      password,
      confirmPassword,
    };

    changeWorkerPasswordAction(workerIndex, newPasswordInfo);
  };

  render() {
    const {
      isChangeWorkerPasswordFetching,
      isChangeWorkerPasswordError,
      toggleChangeWorkerPasswordModal: toggleChangeWorkerPasswordModalAction,
      isChangeWorkerPasswordModalOpen,
      changeWorkerPasswordErrorMessage,
    } = this.props;
    const { password, confirmPassword } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const changeWorkerPasswordModalHeader = 'تغيير كلمة المرور';

    return (
      <Modal
        toggleModal={toggleChangeWorkerPasswordModalAction}
        header={changeWorkerPasswordModalHeader}
        isOpened={isChangeWorkerPasswordModalOpen}
      >
        <ChangeWorkerPasswordFormContainer onSubmit={this.onSubmitChangeWorkerPasswordForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  كلمة المرور الجديدة
                </Text>
                <InputField
                  type="password"
                  placeholder="كلمة المرور الجديدة"
                  ref={inputField => {
                    this.newPasswordField = inputField;
                  }}
                  width={1}
                  value={password}
                  onChange={value => this.handleInputFieldChange('password', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تأكيد كلمة المرور
                </Text>
                <InputField
                  type="password"
                  placeholder="تأكيد كلمة المرور"
                  ref={inputField => {
                    this.confirmPasswordField = inputField;
                  }}
                  width={1}
                  value={confirmPassword}
                  onChange={value => this.handleInputFieldChange('confirmPassword', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddCreditForm}
                disabled={
                  isChangeWorkerPasswordFetching || password === '' || confirmPassword === ''
                }
                isLoading={isChangeWorkerPasswordFetching}
              >
                تغيير كلمة المرور
              </Button>
              {isChangeWorkerPasswordError && (
                <Caution mx={2} isRtl bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {changeWorkerPasswordErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </ChangeWorkerPasswordFormContainer>
      </Modal>
    );
  }
}
ChangeWorkerPasswordModal.displayName = 'ChangeWorkerPasswordModal';

const mapStateToProps = state => ({
  isChangeWorkerPasswordFetching: state.workers.changeWorkerPassword.isFetching,
  isChangeWorkerPasswordError: state.workers.changeWorkerPassword.isFail.isError,
  changeWorkerPasswordErrorMessage: state.workers.changeWorkerPassword.isFail.message,
  isChangeWorkerPasswordModalOpen: state.workers.changeWorkerPasswordModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeWorkerPassword,
      toggleChangeWorkerPasswordModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ChangeWorkerPasswordModal);
