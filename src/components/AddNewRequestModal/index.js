import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { toggleAddNewRequestModal, addNewRequest } from 'redux-modules/clients/actions';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { FIELDS } from 'utils/constants';
import InputField from 'components/InputField';

const AddNewRequestFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const fieldsKeys = Object.keys(FIELDS);

const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));

class AddNewRequestModal extends Component {
  static propTypes = {
    isAddNewRequestFetching: PropTypes.bool,
    isAddNewRequestError: PropTypes.bool,
    addNewRequestErrorMessage: PropTypes.string,
    toggleAddNewRequestModal: PropTypes.func,
    isAddNewRequestModalOpen: PropTypes.bool,
    customerId: PropTypes.string,
    location: PropTypes.shape({}),
    ticketId: PropTypes.string,
    addNewRequest: PropTypes.func,
    isShowPromoSection: PropTypes.bool,
  };

  static defaultProps = {
    isAddNewRequestFetching: false,
    isAddNewRequestError: false,
    addNewRequestErrorMessage: '',
    toggleAddNewRequestModal: () => {},
    isAddNewRequestModalOpen: false,
    customerId: undefined,
    location: undefined,
    ticketId: undefined,
    addNewRequest: () => {},
    isShowPromoSection: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      field: '',
      promoCode: '',
    };
  }

  handleChangeFieldSelect = field => {
    this.setState({ field });
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitForm = e => {
    e.preventDefault();
    const {
      customerId,
      ticketId,
      workerId,
      location,
      location: { latitude, longitude },
    } = this.props;
    const { field, promoCode } = this.state;
    const data = {
      field: field ? field.value : '',
      customer: customerId,
      latitude,
      longitude,
    };

    if (promoCode) {
      data.promoCode = promoCode;
    }

    if (location) {
      data.location = location;
    }

    if (ticketId) {
      data.ticket = ticketId;
    }

    if (workerId) {
      data.worker = workerId;
    }

    const { addNewRequest: addNewRequestAction } = this.props;

    addNewRequestAction(data);
  };

  render() {
    const {
      isAddNewRequestFetching,
      isAddNewRequestError,
      addNewRequestErrorMessage,
      isAddNewRequestModalOpen,
      toggleAddNewRequestModal: toggleAddNewRequestModalAction,
      ticketId,
      workerId,
      isShowPromoSection,
    } = this.props;
    const { field, promoCode } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, WHITE, ERROR } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addEditAdminModalHeader = 'طلب خدمة';

    return (
      <Modal
        toggleModal={toggleAddNewRequestModalAction}
        header={addEditAdminModalHeader}
        isOpened={isAddNewRequestModalOpen}
      >
        <AddNewRequestFormContainer onSubmit={this.onSubmitForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={field}
                  onChange={this.handleChangeFieldSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            {isShowPromoSection && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    اسم البروموكود
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="اسم البروموكود"
                    width={1}
                    value={promoCode}
                    onChange={value => this.handleInputFieldChange('promoCode', value)}
                    mb={3}
                    autoComplete="off"
                  />
                </Flex>
              </Flex>
            )}

            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                disabled={isAddNewRequestFetching || (ticketId && !workerId)}
                isLoading={isAddNewRequestFetching}
              >
                طلب
              </Button>
              {isAddNewRequestError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addNewRequestErrorMessage}
                </Caution>
              )}
              {ticketId && !workerId && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  يجب اختيار الفنى المسئول عن حل الشكوى اولا
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewRequestFormContainer>
      </Modal>
    );
  }
}
AddNewRequestModal.displayName = 'AddNewRequestModal';

const mapStateToProps = state => ({
  isAddNewRequestFetching: state.clients.addNewRequest.isFetching,
  isAddNewRequestError: state.clients.addNewRequest.isFail.isError,
  addNewRequestErrorMessage: state.clients.addNewRequest.isFail.message,
  isAddNewRequestModalOpen: state.clients.addNewRequestModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewRequestModal,
      addNewRequest,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewRequestModal);
