import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import Text from '../Text';
import { FONT_TYPES, FONT_WEIGHTS } from '../theme/fonts';
import { COLORS_VALUES, COLORS } from '../theme/colors';

const LOADING = 'loading';
const NOTIFICATIONS = 'notifications';

const SnackBarWindow = styled.div`
  align-items: flex-end;
  display: flex;
  height: 100%;
  left: 0;
  pointer-events: none;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 9999;
`;

const SnackBarContainer = styled.div`
  display: flex;
  align-items: center;
  border-radius: 4px;
  background-color: ${COLORS_VALUES[COLORS.BRANDING_BLUE]};
  flex-direction: row;
  height: auto;
  justify-content: space-between;
  margin: 0 auto;
  margin-bottom: -90px;
  max-height: 80px;
  max-width: 50%;
  min-height: 35px;
  min-width: 300px;
  padding: ${props => props.theme.space[2]}px ${props => props.theme.space[4]}px
    ${props => props.theme.space[2]}px ${props => props.theme.space[6]}px;
  position: relative;
  transition: margin 200ms ease-in-out;
  width: auto;
`;

const CloseIconButton = styled(FontAwesomeIcon)`
  cursor: pointer;
`;

class SnackBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: props.message,
      messageType: props.messageType,
    };
  }

  componentDidMount() {
    const { show, message } = this.props;

    if (show === true) {
      this.showSnackBar(message, NOTIFICATIONS);
    }
  }

  componentDidUpdate(prevProps) {
    const { show, message } = this.props;

    if (prevProps.show !== show && show === true) {
      this.showSnackBar(message, NOTIFICATIONS);
    }
  }

  /**
   * Change state between show/hide snackBar
   * @param {bool} newState, true = show, false = hide
   * @param {string} message, message that will be displayed
   * @param {string} messageType, notifications or loading
   */
  toggleSnackBar = (newState, message, messageType = NOTIFICATIONS) => {
    if (newState === true) {
      this.showSnackBar(message, messageType);
    } else {
      this.hideSnackBar();
    }
  };

  /**
   * Show the snack-bar
   * @param {string} message, the message that will be displayed
   * @param {string} messageType, notifications or loading
   */
  showSnackBar = (message, messageType = NOTIFICATIONS) => {
    const { autoHide, autoHideTime } = this.props;

    this.setState(
      {
        message: messageType === NOTIFICATIONS ? message : `${message}...`,
      },
      () => {
        if (this.snackBar) {
          this.snackBar.style.marginBottom = '40px';
        }

        // Auto hide the snack-bar after set of time
        // Default autoHide = true
        // Default timeout = 5000ms
        if (autoHide) {
          setTimeout(() => {
            this.hideSnackBar();
          }, autoHideTime);
        }
      },
    );
  };

  /**
   * Hide the snack-bar
   */
  hideSnackBar = () => {
    const { onHide } = this.props;

    if (this.snackBar) {
      this.snackBar.style.marginBottom = '-90px';
    }

    if (onHide) {
      onHide();
    }
  };

  render() {
    const { message, messageType } = this.state;
    const { WHITE } = COLORS;
    const { SUBHEADING } = FONT_TYPES;
    const { NORMAL } = FONT_WEIGHTS;

    return (
      <SnackBarWindow>
        <SnackBarContainer
          ref={snackBar => {
            this.snackBar = snackBar;
          }}
        >
          <Text
            type={SUBHEADING}
            color={COLORS_VALUES[WHITE]}
            fontWeight={NORMAL}
            className="message-text"
          >
            {message}
          </Text>
          {messageType === NOTIFICATIONS && (
            <CloseIconButton
              onClick={this.hideSnackBar}
              icon={faTimes}
              color={COLORS_VALUES[WHITE]}
              size="sm"
            />
          )}
        </SnackBarContainer>
      </SnackBarWindow>
    );
  }
}
SnackBar.displayName = 'SnackBar';

SnackBar.propTypes = {
  onHide: PropTypes.func,
  autoHide: PropTypes.bool,
  autoHideTime: PropTypes.number,
  show: PropTypes.bool,
  message: PropTypes.string,
  messageType: PropTypes.oneOf([NOTIFICATIONS, LOADING]),
};

SnackBar.defaultProps = {
  autoHide: true,
  autoHideTime: 5000,
  show: false,
  message: '',
  messageType: NOTIFICATIONS,
  onHide: () => {},
};

export default SnackBar;
