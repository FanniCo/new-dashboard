import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { CardLabel } from 'components/shared';
import React from 'react';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import PropTypes from 'prop-types';
import InputField from 'components/InputField';
import TransactionsClassificationDropdown from 'containers/shared/TransactionsClassificationDropdown';
import _ from 'lodash';

const { DISABLED, BRANDING_GREEN } = COLORS;
const { BODY, SUBHEADING, HEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const AddCreditsFromWorkerToAnother = props => {
  const {
    creditsWillDeductFromWorker,
    creditsWillGrant,
    handleInputFieldChange,
    commentOfCreditsWillDeductFromWorker,
    commentOfCreditsWillGrant,
    whoCausedTheComplaint,
    whoWillSolveTheComplaint,
  } = props;

  return (
    <>
      <Flex flexDirection="row" width={1} px={4}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          سيتم خصم المبلغ من
        </Text>
        <CardLabel
          pt="6px"
          pb={1}
          px={2}
          mx={2}
          type={BODY}
          border="1px solid "
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          {_.get(whoCausedTheComplaint, 'name')}
        </CardLabel>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          و اضافته الى
        </Text>
        <CardLabel
          pt="6px"
          pb={1}
          px={2}
          mx={2}
          type={BODY}
          border="1px solid "
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          {_.get(whoWillSolveTheComplaint, 'name')}
        </CardLabel>
      </Flex>
      <Flex flexDirection="row">
        <Flex flexDirection="column" width={1 / 2}>
          <>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  ادخل الرصيد سيخصم من الفنى
                </Text>
                <InputField
                  type="text"
                  placeholder="الرصيد"
                  width={1}
                  value={creditsWillDeductFromWorker}
                  onChange={value => handleInputFieldChange('creditsWillDeductFromWorker', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اختار تصنيف عملية الخصم
                </Text>
                <TransactionsClassificationDropdown
                  mb={3}
                  isMulti={false}
                  handleInputChange={value =>
                    handleInputFieldChange(
                      'customTransactionClassificationCodeOfCreditsWillDeductFromWorker',
                      value,
                    )
                  }
                  isGetTransactionsForCustomer={false}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تعليق
                </Text>
                <InputField
                  type="textarea"
                  placeholder="تعليق"
                  width={1}
                  value={commentOfCreditsWillDeductFromWorker}
                  onChange={value =>
                    handleInputFieldChange('commentOfCreditsWillDeductFromWorker', value)
                  }
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
          </>
        </Flex>
        <hr />
        <Flex flexDirection="column" width={1 / 2}>
          <Flex pb={3} px={4}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                ادخل الرصيد سيمنح للفنى
              </Text>
              <InputField
                type="text"
                placeholder="الرصيد"
                width={1}
                value={creditsWillGrant}
                onChange={value => handleInputFieldChange('creditsWillGrant', value)}
                mb={2}
                autoComplete="on"
              />
            </Flex>
          </Flex>
          <Flex pb={3} px={4}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                اختار تصنيف العملية
              </Text>
              <TransactionsClassificationDropdown
                mb={3}
                isMulti={false}
                handleInputChange={value =>
                  handleInputFieldChange(
                    'customTransactionClassificationCodeOfCreditsWillGrant',
                    value,
                  )
                }
                isGetTransactionsForCustomer={false}
              />
            </Flex>
          </Flex>
          <Flex pb={3} px={4}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                تعليق
              </Text>
              <InputField
                type="textarea"
                placeholder="تعليق"
                width={1}
                value={commentOfCreditsWillGrant}
                onChange={value => handleInputFieldChange('commentOfCreditsWillGrant', value)}
                mb={2}
                autoComplete="on"
              />
            </Flex>
          </Flex>
        </Flex>
      </Flex>
    </>
  );
};

AddCreditsFromWorkerToAnother.displayName = 'AddCreditsFromWorkerToAnother';

AddCreditsFromWorkerToAnother.propTypes = {
  creditsWillDeductFromWorker: PropTypes.string,
  creditsWillGrant: PropTypes.string,
  commentOfCreditsWillDeductFromWorker: PropTypes.string,
  commentOfCreditsWillGrant: PropTypes.string,
  handleInputFieldChange: PropTypes.func,
  whoCausedTheComplaint: PropTypes.shape({}),
  whoWillSolveTheComplaint: PropTypes.shape({}),
};

AddCreditsFromWorkerToAnother.defaultProps = {
  creditsWillDeductFromWorker: undefined,
  creditsWillGrant: undefined,
  commentOfCreditsWillDeductFromWorker: undefined,
  commentOfCreditsWillGrant: undefined,
  handleInputFieldChange: () => {},
  whoCausedTheComplaint: undefined,
  whoWillSolveTheComplaint: undefined,
};

export default AddCreditsFromWorkerToAnother;
