import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { changeWorkerPassword } from 'redux-modules/workers/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const ChangeAdminPasswordFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class ChangeAdminPasswordModal extends Component {
  static propTypes = {
    adminDetails: PropTypes.shape({}),
    isChangeAdminPasswordFetching: PropTypes.bool,
    isChangeAdminPasswordError: PropTypes.bool,
    changeAdminPasswordErrorMessage: PropTypes.string,
    changeAdminPassword: PropTypes.func,
    handleToggleChangeAdminPasswordModal: PropTypes.func,
    isChangeAdminPasswordModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    adminDetails: undefined,
    isChangeAdminPasswordFetching: false,
    isChangeAdminPasswordError: false,
    changeAdminPasswordErrorMessage: '',
    changeAdminPassword: () => {},
    handleToggleChangeAdminPasswordModal: () => {},
    isChangeAdminPasswordModalOpen: false,
  };

  state = {
    password: '',
    confirmPassword: '',
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitChangeAdminPasswordForm = e => {
    e.preventDefault();
    const {
      adminDetails: { _id },
      changeAdminPassword: changeAdminPasswordAction,
    } = this.props;
    const { password, confirmPassword } = this.state;

    const newPasswordInfo = {
      adminId: _id,
      password,
      confirmPassword,
    };

    changeAdminPasswordAction(newPasswordInfo);
  };

  render() {
    const {
      isChangeAdminPasswordFetching,
      isChangeAdminPasswordError,
      handleToggleChangeAdminPasswordModal: toggleChangeAdminPasswordModalAction,
      isChangeAdminPasswordModalOpen,
      changeAdminPasswordErrorMessage,
    } = this.props;
    const { password, confirmPassword } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const changeAdminPasswordModalHeader = 'تغيير كلمة المرور';

    return (
      <Modal
        toggleModal={() => toggleChangeAdminPasswordModalAction({})}
        header={changeAdminPasswordModalHeader}
        isOpened={isChangeAdminPasswordModalOpen}
      >
        <ChangeAdminPasswordFormContainer onSubmit={this.onSubmitChangeAdminPasswordForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  كلمة المرور الجديدة
                </Text>
                <InputField
                  type="password"
                  placeholder="كلمة المرور الجديدة"
                  ref={inputField => {
                    this.newPasswordField = inputField;
                  }}
                  width={1}
                  value={password}
                  onChange={value => this.handleInputFieldChange('password', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تأكيد كلمة المرور
                </Text>
                <InputField
                  type="password"
                  placeholder="تأكيد كلمة المرور"
                  ref={inputField => {
                    this.confirmPasswordField = inputField;
                  }}
                  width={1}
                  value={confirmPassword}
                  onChange={value => this.handleInputFieldChange('confirmPassword', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                disabled={
                  isChangeAdminPasswordFetching || password === '' || confirmPassword === ''
                }
                isLoading={isChangeAdminPasswordFetching}
              >
                تغيير كلمة المرور
              </Button>
              {isChangeAdminPasswordError && (
                <Caution mx={2} isRtl bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {changeAdminPasswordErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </ChangeAdminPasswordFormContainer>
      </Modal>
    );
  }
}
ChangeAdminPasswordModal.displayName = 'ChangeAdminPasswordModal';

const mapStateToProps = state => ({
  isChangeAdminPasswordFetching: state.user.changeAdminPassword.isFetching,
  isChangeAdminPasswordError: state.user.changeAdminPassword.isFail.isError,
  changeAdminPasswordErrorMessage: state.user.changeAdminPassword.isFail.message,
  isChangeAdminPasswordModalOpen: state.user.changeAdminPasswordModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changeWorkerPassword,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ChangeAdminPasswordModal);
