import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Flex } from '@rebass/grid';
import moment from 'moment';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import DatePicker from 'components/Filter/DatePicker';
import { customSelectStyles, selectColors, StyledSelect } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { CITIES, CONFIRM_CHOICE } from 'utils/constants';
import _ from 'lodash';
import {
  addVoucher,
  toggleAddVoucherModal,
  toggleEditVoucherModal,
  updateVoucher,
  updateVoucherFail,
  updateVoucherSuccess,
} from 'redux-modules/vouchers/actions';

const citiesKeys = Object.keys(CITIES);
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));
citiesSelectOptions.unshift({
  value: 'ALL_CITIES',
  label: 'كل المدن',
});

const confirmChoiceKeys = Object.keys(CONFIRM_CHOICE);
const confirmationOptions = confirmChoiceKeys.map(key => ({
  value: key,
  label: CONFIRM_CHOICE[key].ar,
}));

const AddVoucherFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddVoucherModal extends Component {
  static propTypes = {
    isAddVoucherFetching: PropTypes.bool,
    isAddVoucherError: PropTypes.bool,
    addVoucherErrorMessage: PropTypes.string,
    updateVoucher: PropTypes.func,
    toggleAddVoucherModal: PropTypes.func,
    toggleEditVoucherModal: PropTypes.func,
    isAddVoucherModalOpen: PropTypes.bool,
    isEditVoucherFetching: PropTypes.bool,
    voucherDetails: PropTypes.shape({}),
    voucherIndex: PropTypes.number,
    isUpdateVoucherError: PropTypes.bool,
    updateVoucherErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    isAddVoucherFetching: false,
    isAddVoucherError: false,
    addVoucherErrorMessage: '',
    isAddVoucherModalOpen: false,
    isEditVoucherFetching: false,
    voucherDetails: undefined,
    voucherIndex: 0,
    isUpdateVoucherError: false,
    updateVoucherErrorMessage: undefined,
    updateVoucher: () => {},
    toggleAddVoucherModal: () => {},
    toggleEditVoucherModal: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      code: '',
      value: '',
      expireAt: '',
      cities: [],
      timesPerCustomerCanUse: '',
      isForNewCustomer: { label: CONFIRM_CHOICE.false.ar, value: false },
      forOneCustomerToUse: { label: CONFIRM_CHOICE.false.ar, value: false },
    };
  }

  componentDidMount(): void {
    const { voucherDetails } = this.props;

    if (voucherDetails) {
      const {
        cities,
        code,
        value,
        expireAt,
        timesPerCustomerCanUse,
        isForNewCustomer,
        forOneCustomerToUse,
      } = voucherDetails;

      this.setState({
        code,
        value,
        expireAt: moment(expireAt).format('YYYY-MM-DD'),
        cities: citiesSelectOptions.filter(options => cities.includes(options.value)),
        timesPerCustomerCanUse: timesPerCustomerCanUse.toString(),
        isForNewCustomer: confirmationOptions.find(
          option => option.value.toString() === isForNewCustomer.toString(),
        ),
        forOneCustomerToUse: confirmationOptions.find(
          option => option.value.toString() === forOneCustomerToUse.toString(),
        ),
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleExpireDateInputChange = expireAt => {
    const expireDateString = expireAt
      ? expireAt.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';

    this.setState({ expireAt: expireDateString });
  };

  handleChangeCitiesSelect = cities => {
    const all = cities.some(c => c.value === 'ALL_CITIES');
    if (all) {
      cities = [...citiesSelectOptions];
      cities.shift();
      cities.pop();
    }
    this.setState({ cities });
  };

  handleChangeIsForNewCustomerSelect = isForNewCustomer => {
    this.setState({ isForNewCustomer });
  };

  handleChangeForOneCustomerToUseSelect = forOneCustomerToUse => {
    this.setState({ forOneCustomerToUse });
  };

  onSubmitAddVoucherForm = e => {
    e.preventDefault();

    const {
      addVoucher: addVoucherAction,
      updateVoucher: updateVoucherAction,
      voucherDetails,
      voucherIndex,
    } = this.props;

    const {
      code,
      value,
      cities,
      expireAt,
      forOneCustomerToUse,
      timesPerCustomerCanUse,
      isForNewCustomer,
    } = this.state;

    const voucherCities = !_.isEmpty(cities) ? cities.map(record => record.value) : [];

    const voucherInfo = {
      code,
      value,
      expireAt,
      cities: voucherCities,
      timesPerCustomerCanUse: Number(convertArabicNumbersToEnglish(timesPerCustomerCanUse)),
      isForNewCustomer: isForNewCustomer ? isForNewCustomer.value : false,
      forOneCustomerToUse: forOneCustomerToUse ? forOneCustomerToUse.value : false,
    };

    if (voucherDetails) {
      const { _id } = voucherDetails;
      return updateVoucherAction(voucherInfo, _id, voucherIndex);
    }

    addVoucherAction(voucherInfo);
  };

  render() {
    const {
      isAddVoucherFetching,
      isAddVoucherModalOpen,
      toggleAddVoucherModal: toggleAddVoucherModalAction,
      toggleEditVoucherModal: toggleEditVoucherModalAction,
      isAddVoucherError,
      addVoucherErrorMessage,
      voucherDetails,
      isEditVoucherFetching,
      isUpdateVoucherError,
      updateVoucherErrorMessage,
    } = this.props;

    const {
      code,
      value,
      cities,
      expireAt,
      timesPerCustomerCanUse,
      isForNewCustomer,
      forOneCustomerToUse,
    } = this.state;

    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addVoucherModalHeader = voucherDetails ? 'تعديل فوتشر' : 'أضف فوتشر';
    const submitButtonText = voucherDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          voucherDetails ? toggleEditVoucherModalAction() : toggleAddVoucherModalAction()
        }
        header={addVoucherModalHeader}
        isOpened={isAddVoucherModalOpen}
      >
        <AddVoucherFormContainer onSubmit={this.onSubmitAddVoucherForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الفوتشر
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم الفوتشر"
                  ref={inputField => {
                    this.codeField = inputField;
                  }}
                  width={1}
                  value={code}
                  onChange={value => this.handleInputFieldChange('code', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ الانتهاء
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <DatePicker
                  mb={0}
                  minDate={new Date()}
                  value={expireAt}
                  handleDatePickerChange={this.handleExpireDateInputChange}
                  placeholder="MM/DD/YYYY"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" ml={2} width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  هل مخصص للعملاء الجداد
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={isForNewCustomer}
                  onChange={this.handleChangeIsForNewCustomerSelect}
                  options={confirmationOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" ml={2} width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  هل مخصص لعميل واحد فقط
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={forOneCustomerToUse}
                  onChange={this.handleChangeForOneCustomerToUseSelect}
                  options={confirmationOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  قيمة الفوتشر
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="قيمة الفوتشر"
                  width={1}
                  value={value}
                  onChange={value => this.handleInputFieldChange('value', value)}
                  mb={2}
                  autoComplete="on"
                  disabled={!_.isEmpty(voucherDetails)}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدينة
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={cities}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isMulti
                  isClearable
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  عدد المرات لكل عميل
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="عدد المرات لكل عميل"
                  ref={inputField => {
                    this.timesPerCustomerField = inputField;
                  }}
                  width={1}
                  value={timesPerCustomerCanUse}
                  onChange={value => this.handleInputFieldChange('timesPerCustomerCanUse', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddVoucherForm}
                disabled={isAddVoucherFetching}
                isLoading={isAddVoucherFetching || isEditVoucherFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddVoucherError || isUpdateVoucherError) && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addVoucherErrorMessage || updateVoucherErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddVoucherFormContainer>
      </Modal>
    );
  }
}

AddVoucherModal.displayName = 'AddVoucherModal';

const mapStateToProps = state => ({
  isAddVoucherFetching: state.vouchers.addVoucher.isFetching,
  isEditVoucherFetching: state.vouchers.editVoucher.isFetching,
  isAddVoucherError: state.vouchers.addVoucher.isFail.isError,
  isUpdateVoucherError: state.vouchers.editVoucher.isFail.isError,
  addVoucherErrorMessage: state.vouchers.addVoucher.isFail.message,
  isAddVoucherModalOpen:
    state.vouchers.addVoucherModal.isOpen || state.vouchers.editVoucherModal.isOpen,
  updateVoucherErrorMessage: state.vouchers.editVoucher.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addVoucher,
      updateVoucher,
      updateVoucherFail,
      updateVoucherSuccess,
      toggleAddVoucherModal,
      toggleEditVoucherModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddVoucherModal);
