import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ReactTooltip from 'react-tooltip';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const WorkersAdCard = props => {
  const {
    workersAd,
    workersAd: { _id, title, description, createdBy, createdAt },
    index,
    user: { permissions },
    handleToggleConfirmModal,
    handleToggleEditWorkersAdModal,
    deleteWorkersAdAction,
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, LIGHT_RED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const workersAdDate = new Date(createdAt);
  const deleteWorkersAdTooltip = 'مسح الاعلان';
  const editWorkersAdTooltip = 'تعديل الاعلان';

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {workersAdDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العنوان :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {title.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الوصف :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {description.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
      </CardBody>
      {permissions &&
        (permissions.includes('Delete Workers Ad') ||
          permissions.includes('Update Workers Ad')) && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
              {permissions && permissions.includes('Delete Workers Ad') && (
                <>
                  <ReactTooltip id={`deleteWorkersAd_${_id}`} type="error" effect="solid">
                    <span>{deleteWorkersAdTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`deleteWorkersAd_${_id}`}
                      data-tip
                      onClick={() =>
                        handleToggleConfirmModal(
                          'تأكيد مسح الاعلان',
                          'هل أنت متأكد أنك تريد مسح الاعلان؟',
                          () => {
                            deleteWorkersAdAction(_id, index);
                          },
                        )
                      }
                      icon={faTrashAlt}
                      color={COLORS_VALUES[LIGHT_RED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
              {permissions && permissions.includes('Update Workers Ad') && (
                <>
                  <ReactTooltip id={`editWorker_${_id}`} type="light" effect="solid">
                    <span>{editWorkersAdTooltip}</span>
                  </ReactTooltip>
                  <Box>
                    <CallToActionIcon
                      data-for={`editWorker_${_id}`}
                      data-tip
                      onClick={() => handleToggleEditWorkersAdModal(index, workersAd)}
                      icon={faPen}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
          </Flex>
        </CardFooter>
        )}
    </Card>
  );
};
WorkersAdCard.displayName = 'WorkersAdCard';

WorkersAdCard.propTypes = {
  workersAd: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditWorkersAdModal: PropTypes.func,
  deleteWorkersAdAction: PropTypes.func,
};

WorkersAdCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditWorkersAdModal: () => {},
  deleteWorkersAdAction: () => {},
};

export default WorkersAdCard;
