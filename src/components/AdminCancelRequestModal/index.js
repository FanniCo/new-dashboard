import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { cancelRequest } from 'redux-modules/requests/actions';
import { customSelectStylesIsDanger, selectColors, StyledSelect } from 'components/shared';
import { CANCELLATION_REASONS, CANCELLATION_REASON_TYPES } from 'utils/constants';
import Caution from '../Caution';
import InputField from '../InputField';

const AdminCancelRequestFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AdminCancelRequestModal extends Component {
  static propTypes = {
    requestId: PropTypes.string,
    requestIndex: PropTypes.number,
    isAdminCancelRequestFetching: PropTypes.bool,
    isAdminCancelRequestError: PropTypes.bool,
    adminCancelRequestErrorMessage: PropTypes.string,

    handleToggleAdminCancelRequestModal: PropTypes.func,
    isAdminCancelRequestModalOpen: PropTypes.bool,
    cancelRequest: PropTypes.func,
  };

  static defaultProps = {
    requestId: undefined,
    requestIndex: undefined,
    isAdminCancelRequestFetching: false,
    isAdminCancelRequestError: false,
    adminCancelRequestErrorMessage: '',

    isAdminCancelRequestModalOpen: false,
    handleToggleAdminCancelRequestModal: () => {},
    cancelRequest: () => {},
  };

  state = {
    cancellationReason: null,
    cancellationReasonStatus: null,
    cancellationReasonComment: '',
    isCancelationOtherReasonSelected: false,
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeCancellationReasonSelect = cancellationReason => {
    this.setState({ cancellationReason });

    if (cancellationReason.value === 'other') {
      this.setState({ isCancelationOtherReasonSelected: true });
    } else {
      this.setState({ isCancelationOtherReasonSelected: false });
    }
  };

  handleChangeCancellationReasonStatusSelect = cancellationReasonStatus => {
    this.setState({ cancellationReasonStatus });
  };

  onSubmitAdminCancelRequestForm = e => {
    e.preventDefault();

    const { requestId, requestIndex, cancelRequest: cancelRequestAction } = this.props;

    const {
      cancellationReason,
      cancellationReasonStatus,
      cancellationReasonComment,
      isCancelationOtherReasonSelected,
    } = this.state;

    const cancelRequestInfo = {
      requestId,
      requestIndex,
      cancellationReason: cancellationReason.value,
    };

    if (isCancelationOtherReasonSelected) {
      cancelRequestInfo.cancellationReasonComment = cancellationReasonComment;
      cancelRequestInfo.cancellationReasonStatus = cancellationReasonStatus.value;
    }

    cancelRequestAction(requestId, cancelRequestInfo);
  };

  render() {
    const {
      isAdminCancelRequestModalOpen,
      isAdminCancelRequestError,
      isAdminCancelRequestFetching,
      handleToggleAdminCancelRequestModal,
      adminCancelRequestErrorMessage,
    } = this.props;

    const {
      cancellationReason,
      cancellationReasonComment,
      cancellationReasonStatus,
      isCancelationOtherReasonSelected,
    } = this.state;

    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;

    const cancellationReasonsKeys = Object.keys(CANCELLATION_REASONS);
    const cancellationReasonsSelectOptions = cancellationReasonsKeys.map(key => ({
      value: key,
      label: CANCELLATION_REASONS[key].ar,
    }));

    const cancellationReasonsStatusSelectOptions = CANCELLATION_REASON_TYPES;

    const { HEADING } = FONT_TYPES;
    const AdminCancelRequestModalHeader = 'إالغاء طلب جاري';

    return (
      <Modal
        toggleModal={handleToggleAdminCancelRequestModal}
        header={AdminCancelRequestModalHeader}
        isOpened={isAdminCancelRequestModalOpen}
      >
        <AdminCancelRequestFormContainer onSubmit={this.onSubmitAdminCancelRequestForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  سبب الالغاء
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  placeholder="إختر سبب الالغاء"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  isMulti={false}
                  backspaceRemovesValue={false}
                  menuShouldScrollIntoView
                  value={cancellationReason}
                  onChange={value => {
                    this.handleChangeCancellationReasonSelect(value);
                  }}
                  maxMenuHeight={165}
                  isDisabled={false}
                  isLoading={false}
                  options={cancellationReasonsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStylesIsDanger(false)}
                />
              </Flex>
            </Flex>
            {cancellationReason && cancellationReason.value === 'other' && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    اختر نوع سبب الالغاء
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <StyledSelect
                    placeholder="اختر نوع سبب الالغاء"
                    noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                    isRtl
                    isMulti={false}
                    backspaceRemovesValue={false}
                    menuShouldScrollIntoView
                    value={cancellationReasonStatus}
                    onChange={value => {
                      this.handleChangeCancellationReasonStatusSelect(value);
                    }}
                    maxMenuHeight={165}
                    isDisabled={false}
                    isLoading={false}
                    options={cancellationReasonsStatusSelectOptions}
                    theme={selectColors}
                    styles={customSelectStylesIsDanger(false)}
                  />
                </Flex>
              </Flex>
            )}
            {cancellationReason && cancellationReason.value === 'other' && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تعليق بخصوص سبب الالغاء
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="textarea"
                    placeholder="اكتب تعليق بخصوص سبب الالغاء"
                    width={1}
                    value={cancellationReasonComment}
                    onChange={value =>
                      this.handleInputFieldChange('cancellationReasonComment', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            )}
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAdminCancelRequestForm}
                disabled={
                  isAdminCancelRequestFetching ||
                  !cancellationReason ||
                  (isCancelationOtherReasonSelected && !cancellationReasonStatus) ||
                  (isCancelationOtherReasonSelected && !cancellationReasonComment)
                }
                isLoading={isAdminCancelRequestFetching}
              >
                ألغ الطلب
              </Button>
              {/* error meassage goes here! */}
              {isAdminCancelRequestError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {adminCancelRequestErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AdminCancelRequestFormContainer>
      </Modal>
    );
  }
}

AdminCancelRequestModal.displayName = 'AdminCancelRequestModal';

const mapStateToProps = state => ({
  isAdminCancelRequestFetching: state.requests.cancelRequest.isFetching,
  isAdminCancelRequestError: state.requests.cancelRequest.isFail.isError,
  adminCancelRequestErrorMessage: state.requests.cancelRequest.isFail.message,
  isAdminCancelRequestModalOpen: state.requests.cancelRequest.isModalOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      cancelRequest,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AdminCancelRequestModal);
