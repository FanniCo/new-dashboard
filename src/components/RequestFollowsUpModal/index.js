import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { addRequestFollowUps } from 'redux-modules/requests/actions';
import RequestFollowsUpDropDown from 'containers/shared/RequestFollowsUpDropDown';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import _ from 'lodash';

const FormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class RequestFollowsUpModal extends Component {
  static propTypes = {
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
    isFetchingFollowUpsList: PropTypes.bool,
    isFetchingFollowUpsListError: PropTypes.bool,
    fetchingFollowUpsListErrorMessage: PropTypes.string,
    request: PropTypes.shape({}),
    addRequestFollowUps: PropTypes.func,
    isAddingRequestFollowUps: PropTypes.bool,
    isRequestFollowUpsError: PropTypes.bool,
    addRequestFollowUpsErrorMessage: PropTypes.string,
    user: PropTypes.shape({}),
    objectIndex: PropTypes.number,
  };

  static defaultProps = {
    isOpened: false,
    toggleFunction: () => {},
    isFetchingFollowUpsList: false,
    isFetchingFollowUpsListError: false,
    fetchingFollowUpsListErrorMessage: undefined,
    request: undefined,
    addRequestFollowUps: () => {},
    isAddingRequestFollowUps: false,
    isRequestFollowUpsError: false,
    addRequestFollowUpsErrorMessage: undefined,
    user: undefined,
    objectIndex: undefined,
  };

  state = {
    customerServiceFollowsUp: null,
    operationsFollowsUp: null,
    comment: '',
  };

  componentDidMount() {
    const {
      request: { customerServiceFollowUps, operationsFollowUps, commentFollowUps },
    } = this.props;
    const customerServiceFollowsUp = customerServiceFollowUps
      ? { value: customerServiceFollowUps, label: customerServiceFollowUps }
      : null;
    const operationsFollowsUp = operationsFollowUps
      ? { value: operationsFollowUps, label: operationsFollowUps }
      : null;

    this.setState({ customerServiceFollowsUp });
    this.setState({ operationsFollowsUp });
    this.setState({ comment: commentFollowUps || '' });
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeCustomerServiceFollowsUpSelect = customerServiceFollowsUp => {
    this.setState({ customerServiceFollowsUp });
  };

  handleChangeOperationsFollowsUpSelect = operationsFollowsUp => {
    this.setState({ operationsFollowsUp });
  };

  onSubmitForm = e => {
    e.preventDefault();

    const { addRequestFollowUps: addFollowUpsAction } = this.props;
    const {
      request: { _id: requestId },
    } = this.props;
    const { customerServiceFollowsUp, operationsFollowsUp, comment } = this.state;
    const followUpsInfo = {
      requestId,
      operationsFollowUps: _.get(operationsFollowsUp, 'value'),
      customerServiceFollowUps: _.get(customerServiceFollowsUp, 'value'),
      commentFollowUps: comment,
    };

    addFollowUpsAction(followUpsInfo);
  };

  render() {
    const {
      user: { permissions },
      isOpened,
      toggleFunction: toggleModalAction,
      isFetchingFollowUpsList,
      isFetchingFollowUpsListError,
      fetchingFollowUpsListErrorMessage,
      isAddingRequestFollowUps,
      isRequestFollowUpsError,
      addRequestFollowUpsErrorMessage,
      objectIndex,
    } = this.props;
    const { BRANDING_GREEN, LIGHT_RED, WHITE, ERROR } = COLORS;
    const { HEADING } = FONT_TYPES;
    const modalHeader = 'متابعة الخدمة';
    const { comment, customerServiceFollowsUp, operationsFollowsUp } = this.state;

    return (
      <Modal
        toggleModal={() => toggleModalAction(objectIndex)}
        header={modalHeader}
        isOpened={isOpened}
      >
        <FormContainer onSubmit={this.onSubmitForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4} flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                متابعة خدمة العميل
              </Text>
              <RequestFollowsUpDropDown
                mb={3}
                isMulti={false}
                type="requestCustomerServiceFollowups"
                value={customerServiceFollowsUp}
                handleInputChange={value => this.handleChangeCustomerServiceFollowsUpSelect(value)}
              />
            </Flex>
            <Flex pb={3} px={4} flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                متابعة عمليات
              </Text>
              <RequestFollowsUpDropDown
                mb={3}
                isMulti={false}
                type="requestOperationsFollowups"
                value={operationsFollowsUp}
                handleInputChange={value => this.handleChangeOperationsFollowsUpSelect(value)}
              />
            </Flex>
            <Flex pb={3} px={4} flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                تعليق
              </Text>
              <InputField
                type="textarea"
                placeholder="تعليق"
                width={1}
                value={comment}
                onChange={value => this.handleInputFieldChange('comment', value)}
                mb={2}
                autoComplete="on"
              />
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              {permissions && permissions.includes('Add Request Follow Ups') && (
                <Button
                  type="submit"
                  primary
                  color={BRANDING_GREEN}
                  icon={faChevronLeft}
                  reverse
                  onClick={this.onSubmitForm}
                  disabled={
                    isFetchingFollowUpsList ||
                    isAddingRequestFollowUps ||
                    (!_.get(customerServiceFollowsUp, 'value') &&
                      !_.get(operationsFollowsUp, 'value'))
                  }
                  isLoading={isAddingRequestFollowUps}
                >
                  تعديل
                </Button>
              )}
              {isFetchingFollowUpsListError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {fetchingFollowUpsListErrorMessage}
                </Caution>
              )}
              {isRequestFollowUpsError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addRequestFollowUpsErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </FormContainer>
      </Modal>
    );
  }
}

RequestFollowsUpModal.displayName = 'RequestFollowsUpModal';

const mapStateToProps = state => ({
  isFetchingFollowUpsList: state.statics.getFollowUpsList.isFetching,
  isFetchingFollowUpsListError: state.statics.getFollowUpsList.isFail.isError,
  fetchingFollowUpsListErrorMessage: state.statics.getFollowUpsList.isFail.message,
  isAddingRequestFollowUps: state.requests.addRequestFollowUps.isFetching,
  isRequestFollowUpsError: state.requests.addRequestFollowUps.isFail.isError,
  addRequestFollowUpsErrorMessage: state.requests.addRequestFollowUps.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addRequestFollowUps,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(RequestFollowsUpModal);
