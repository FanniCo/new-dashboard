import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import TransactionCard from 'components/TransactionCard';
import VendorDetailsModal from 'components/VendorDetailsModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { FONT_TYPES } from 'components/theme/fonts';

const { WHITE, GREY_LIGHT, GREY_DARK, BRANDING_GREEN } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class TransactionsList extends Component {
  createTransactionsList = transactionsList => {
    const { handleToggleOpenVendorDetailsModal } = this.props;
    if (transactionsList.length) {
      return transactionsList.map(transaction => {
        const { _id } = transaction;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <TransactionCard
              m={2}
              transaction={transaction}
              handleToggleOpenVendorDetailsModal={handleToggleOpenVendorDetailsModal}
            />
          </Box>
        );
      });
    }
    return null;
  };

  getLoadingTransactions = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingTransactionsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingTransactions(counter));
    }
    return list;
  };

  render() {
    const {
      transactionsList,
      activeVendorDetails,
      isVendorDetailsModalOpen,
      handleToggleOpenVendorDetailsModal,
      isGetAllTransactionsFetching,
      isGetAllTransactionsError,
      getAllTransactionsErrorMessage,
    } = this.props;

    if (isGetAllTransactionsFetching) {
      return this.createLoadingTransactionsList(3);
    }

    if (isGetAllTransactionsError) {
      return (
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <Flex m={2} justifyContent="center" alignItems="center">
              <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                <EmptyState
                  icon={faFile}
                  iconColor={BRANDING_GREEN}
                  iconSize="3x"
                  textColor={COLORS_VALUES[WHITE]}
                  textSize={BIG_TITLE}
                  text={getAllTransactionsErrorMessage}
                />
              </Card>
            </Flex>
          </Card>
        </Box>
      );
    }

    if (!transactionsList.length) {
      return (
        <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
          <EmptyState
            icon={faFile}
            iconColor={BRANDING_GREEN}
            iconSize="3x"
            textColor={COLORS_VALUES[WHITE]}
            textSize={BIG_TITLE}
            text="لا يوجد سجلات"
          />
        </Card>
      );
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {this.createTransactionsList(transactionsList)}
        {isVendorDetailsModalOpen && (
          <VendorDetailsModal
            isOpened={isVendorDetailsModalOpen}
            toggleFunction={handleToggleOpenVendorDetailsModal}
            vendorDetails={activeVendorDetails}
          />
        )}
      </Flex>
    );
  }
}
TransactionsList.displayName = 'TransactionsList';

TransactionsList.propTypes = {
  transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
  handleToggleOpenVendorDetailsModal: PropTypes.func,
  activeVendorDetails: PropTypes.shape({}),
  isVendorDetailsModalOpen: PropTypes.bool,
  isGetAllTransactionsFetching: PropTypes.bool,
  isGetAllTransactionsError: PropTypes.bool,
  getAllTransactionsErrorMessage: PropTypes.string,
};

TransactionsList.defaultProps = {
  transactionsList: undefined,
  handleToggleOpenVendorDetailsModal: () => {},
  activeVendorDetails: undefined,
  isVendorDetailsModalOpen: false,
  isGetAllTransactionsFetching: false,
  isGetAllTransactionsError: false,
  getAllTransactionsErrorMessage: undefined,
};

export default TransactionsList;
