import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import AdminCard from 'components/AdminCard';
import ConfirmModal from 'components/ConfirmModal';
import AddEditAdminModal from 'components/AddEditAdminModal';
import ChangeAdminPasswordModal from 'components/ChangeAdminPasswordModal';

const Admins = props => {
  const {
    admins,
    adminDetails,
    isConfirmModalOpen,
    handleToggleConfirmModal,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    isAddEditAdminModalOpen,
    handleToggleAddEditAdminModal,
    handleDeleteAdmin,
    isDeleteAdminFetching,
    handleChangeAdminPassword,
    handleToggleChangeAdminPasswordModal,
    isChangeAdminPasswordModalOpen,
  } = props;

  const adminsRenderer = admins.map((admin, index) => {
    const { _id } = admin;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <AdminCard
          m={2}
          index={index}
          admin={admin}
          handleDeleteAdmin={handleDeleteAdmin}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleToggleAddEditAdminModal={handleToggleAddEditAdminModal}
          handleToggleChangeAdminPasswordModal={handleToggleChangeAdminPasswordModal}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {adminsRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isDeleteAdminFetching}
          handleToggleChangeAdminPasswordModal={handleToggleChangeAdminPasswordModal}
        />
      )}
      {isAddEditAdminModalOpen && <AddEditAdminModal adminDetails={adminDetails} />}
      {isChangeAdminPasswordModalOpen && (
        <ChangeAdminPasswordModal
          adminDetails={adminDetails}
          changeAdminPassword={handleChangeAdminPassword}
          handleToggleChangeAdminPasswordModal={handleToggleChangeAdminPasswordModal}
        />
      )}
    </Flex>
  );
};
Admins.displayName = 'Admins';

Admins.propTypes = {
  admins: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  adminDetails: PropTypes.shape({}),
  isConfirmModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleDeleteAdmin: PropTypes.func,
  isDeleteAdminFetching: PropTypes.bool,
  handleToggleAddEditAdminModal: PropTypes.func,
  isAddEditAdminModalOpen: PropTypes.bool,
  handleToggleChangeAdminPasswordModal: PropTypes.func,
  isChangeAdminPasswordModalOpen: PropTypes.bool,
  isChangeAdminPasswordFetching: PropTypes.bool,
  isChangeAdminPasswordError: PropTypes.bool,
  handleChangeAdminPassword: PropTypes.func,
};

Admins.defaultProps = {
  adminDetails: undefined,
  isConfirmModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleDeleteAdmin: () => {},
  isDeleteAdminFetching: false,
  isAddEditAdminModalOpen: false,
  handleToggleAddEditAdminModal: () => {},
  handleToggleChangeAdminPasswordModal: () => {},
  isChangeAdminPasswordModalOpen: false,
  isChangeAdminPasswordFetching: false,
  isChangeAdminPasswordError: false,
  handleChangeAdminPassword: () => {},
};

export default Admins;
