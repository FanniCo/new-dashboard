import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { closeComplaint } from 'redux-modules/tickets/actions';
import Modal from 'components/Modal';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { Checkbox, Label } from '@rebass/forms';
import { get, isEmpty } from 'lodash';
import InputField from 'components/InputField';
import TransactionsClassificationDropdown from 'containers/shared/TransactionsClassificationDropdown';
import Button from 'components/Buttons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Caution from 'components/Caution';
import AddCreditsFromWorkerToAnother from '../AddCreditsFromWorkerToAnother';

const { BRANDING_GREEN, BRANDING_BLUE, LIGHT_RED, WHITE, ERROR } = COLORS;
const { HEADING } = FONT_TYPES;

class MarkComplainAsSolvedModal extends Component {
  static propTypes = {
    ticketId: PropTypes.string,
    complain: PropTypes.shape({}),
    customer: PropTypes.shape({}),
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
    closeComplaint: PropTypes.func,
    isClosingComplainFetching: PropTypes.bool,
    isClosingComplainSuccess: PropTypes.bool,
    isClosingComplainError: PropTypes.bool,
    closeComplainErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    ticketId: undefined,
    complain: undefined,
    customer: undefined,
    isOpened: false,
    toggleFunction: () => {},
    closeComplaint: () => {},
    isClosingComplainFetching: false,
    isClosingComplainSuccess: false,
    isClosingComplainError: false,
    closeComplainErrorMessage: false,
  };

  state = {
    memoForCustomerOnlineCompensation: '',
    creditsForCustomerOnlineCompensation: 0,
    customTransactionClassificationCodeForCustomerOnlineCompensation: undefined,

    creditsWillDeductFromWorker: '0',
    creditsWillGrant: '0',
    customTransactionClassificationCodeOfCreditsWillDeductFromWorker: undefined,
    customTransactionClassificationCodeOfCreditsWillGrant: undefined,
    commentOfCreditsWillDeductFromWorker: undefined,
    commentOfCreditsWillGrant: undefined,
    isComplaintResolutionFee: false,
    descriptionForHowTheComplaintSolved: undefined,

    memoForAddCreditsToCustomer: undefined,
    customTransactionClassificationCodeForAddCreditsToCustomer: undefined,
    creditsValueForAddCreditsToCustomer: undefined,

    memoForAddCreditsToWorker: undefined,
    customTransactionClassificationCodeForAddCreditsToWorker: undefined,
    creditsValueForAddCreditsToWorker: undefined,

    isAddCreditsForTheWorker: false,
    isAddCreditsForTheCustomer: false,
    isAddCreditsForCustomerInHisOnlineBank: false,
    isAddCreditsForWorkerToAnother: false,
    isLoadingAssignWorkerToTheTicketToSolve: false,
    assignedWorkerToTheTicketToSolve: undefined,
  };

  handleCheckBoxChange = type => {
    this.setState(prevState => ({
      [type]: !prevState[type],
    }));
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleClickSubmitButton = () => {
    const {
      isAddCreditsForTheCustomer,
      memoForAddCreditsToCustomer,
      customTransactionClassificationCodeForAddCreditsToCustomer,
      creditsValueForAddCreditsToCustomer,

      isAddCreditsForTheWorker,
      memoForAddCreditsToWorker,
      customTransactionClassificationCodeForAddCreditsToWorker,
      creditsValueForAddCreditsToWorker,

      isAddCreditsForWorkerToAnother,
      commentOfCreditsWillDeductFromWorker,
      customTransactionClassificationCodeOfCreditsWillDeductFromWorker,
      creditsWillDeductFromWorker,
      commentOfCreditsWillGrant,
      customTransactionClassificationCodeOfCreditsWillGrant,
      creditsWillGrant,

      isAddCreditsForCustomerInHisOnlineBank,
      memoForCustomerOnlineCompensation,
      creditsForCustomerOnlineCompensation,
      customTransactionClassificationCodeForCustomerOnlineCompensation,

      isComplaintResolutionFee,
      descriptionForHowTheComplaintSolved,
    } = this.state;
    const { ticketId, customer, complain, closeComplaint: closeComplaintAction } = this.props;

    const data = {
      isComplaintResolutionFee,
      descriptionForHowTheComplaintSolved,
    };

    if (isAddCreditsForTheCustomer) {
      data.customer = {
        customerId: customer._id,
        memo: memoForAddCreditsToCustomer,
        customTransactionClassificationCode: get(
          customTransactionClassificationCodeForAddCreditsToCustomer,
          'value',
        ),
        worth: creditsValueForAddCreditsToCustomer,
      };
    }

    if (isAddCreditsForTheWorker) {
      data.worker = {
        workerId: get(complain, 'whoCausedTheComplaint._id'),
        memo: memoForAddCreditsToWorker,
        customTransactionClassificationCode: get(
          customTransactionClassificationCodeForAddCreditsToWorker,
          'value',
        ),
        worth: creditsValueForAddCreditsToWorker,
      };
    }

    if (isAddCreditsForCustomerInHisOnlineBank) {
      data.customerCompensationInHisOnlineAccount = {
        customerId: customer._id,
        worth: creditsForCustomerOnlineCompensation,
        customTransactionClassificationCode: get(
          customTransactionClassificationCodeForCustomerOnlineCompensation,
          'value',
        ),
        memo: memoForCustomerOnlineCompensation,
      };
    }

    if (isAddCreditsForWorkerToAnother) {
      data.fromWorkerToAnother = {
        workerWillDeductFrom: {
          workerId: get(complain, 'whoCausedTheComplaint._id'),
          memo: commentOfCreditsWillDeductFromWorker,
          customTransactionClassificationCode: get(
            customTransactionClassificationCodeOfCreditsWillDeductFromWorker,
            'value',
          ),
          worth: creditsWillDeductFromWorker,
        },
        workerWillGrant: {
          workerId: get(complain, 'whoWillSolveTheComplaint._id'),
          memo: commentOfCreditsWillGrant,
          customTransactionClassificationCode: get(
            customTransactionClassificationCodeOfCreditsWillGrant,
            'value',
          ),
          worth: creditsWillGrant,
        },
      };
    }

    closeComplaintAction(ticketId, complain._id, data);
  };

  render() {
    const {
      isOpened,
      toggleFunction,
      complain: { whoCausedTheComplaint, whoWillSolveTheComplaint },
      isClosingComplainFetching,
      isClosingComplainSuccess,
      isClosingComplainError,
      closeComplainErrorMessage,
    } = this.props;

    const {
      memoForCustomerOnlineCompensation,
      creditsForCustomerOnlineCompensation,
      customTransactionClassificationCodeForCustomerOnlineCompensation,
      memoForAddCreditsToCustomer,
      customTransactionClassificationCodeForAddCreditsToCustomer,
      creditsValueForAddCreditsToCustomer,
      memoForAddCreditsToWorker,
      customTransactionClassificationCodeForAddCreditsToWorker,
      creditsValueForAddCreditsToWorker,
      creditsWillDeductFromWorker,
      creditsWillGrant,
      isAddCreditsForTheWorker,
      isAddCreditsForTheCustomer,
      isAddCreditsForCustomerInHisOnlineBank,
      isAddCreditsForWorkerToAnother,
      commentOfCreditsWillDeductFromWorker,
      commentOfCreditsWillGrant,
      isComplaintResolutionFee,
      descriptionForHowTheComplaintSolved,
    } = this.state;
    const OpenTicketModalHeader = 'تحويل حالة المشكلة الى تم الحل';

    return (
      <Modal
        toggleModal={() => toggleFunction('isOpenMarkTicketAsSolvedModal')}
        header={OpenTicketModalHeader}
        isOpened={isOpened && !isClosingComplainSuccess}
      >
        <Flex flexDirection="column" px={3}>
          <Flex pb={3} px={4}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                وصف حل الشكوى
                <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                  *
                </Text>
              </Text>
              <InputField
                type="textarea"
                placeholder="وصف حل الشكوى"
                width={1}
                value={descriptionForHowTheComplaintSolved}
                onChange={value =>
                  this.handleInputFieldChange('descriptionForHowTheComplaintSolved', value)
                }
                mb={2}
                autoComplete="on"
              />
            </Flex>
          </Flex>
          {!isAddCreditsForCustomerInHisOnlineBank && (
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1 / 2} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        اضافة رصيد للعميل
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={isAddCreditsForTheCustomer}
                        onChange={() => this.handleCheckBoxChange('isAddCreditsForTheCustomer')}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
          )}
          {isAddCreditsForTheCustomer && (
            <Flex flexDirection="column">
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    الرصيد
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="الرصيد"
                    width={1}
                    value={creditsValueForAddCreditsToCustomer}
                    onChange={value =>
                      this.handleInputFieldChange('creditsValueForAddCreditsToCustomer', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تصنيف العملية
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <TransactionsClassificationDropdown
                    mb={3}
                    isMulti={false}
                    value={customTransactionClassificationCodeForAddCreditsToCustomer}
                    handleInputChange={value =>
                      this.handleInputFieldChange(
                        'customTransactionClassificationCodeForAddCreditsToCustomer',
                        value,
                      )
                    }
                    isGetTransactionsForCustomer
                  />
                </Flex>
              </Flex>
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تعليق
                  </Text>
                  <InputField
                    type="textarea"
                    placeholder="تعليق"
                    width={1}
                    value={memoForAddCreditsToCustomer}
                    onChange={value =>
                      this.handleInputFieldChange('memoForAddCreditsToCustomer', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            </Flex>
          )}
          {!isAddCreditsForWorkerToAnother && (
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1 / 2} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        اضافة او خصم رصيد للفنى
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={isAddCreditsForTheWorker}
                        disabled={isEmpty(whoCausedTheComplaint)}
                        onChange={() => this.handleCheckBoxChange('isAddCreditsForTheWorker')}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
          )}
          {isAddCreditsForTheWorker && (
            <Flex flexDirection="column">
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    الرصيد
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="الرصيد"
                    width={1}
                    value={creditsValueForAddCreditsToWorker}
                    onChange={value =>
                      this.handleInputFieldChange('creditsValueForAddCreditsToWorker', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تصنيف العملية
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <TransactionsClassificationDropdown
                    mb={3}
                    isMulti={false}
                    value={customTransactionClassificationCodeForAddCreditsToWorker}
                    handleInputChange={value =>
                      this.handleInputFieldChange(
                        'customTransactionClassificationCodeForAddCreditsToWorker',
                        value,
                      )
                    }
                    isGetTransactionsForCustomer={false}
                  />
                </Flex>
              </Flex>
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تعليق
                  </Text>
                  <InputField
                    type="textarea"
                    placeholder="تعليق"
                    width={1}
                    value={memoForAddCreditsToWorker}
                    onChange={value =>
                      this.handleInputFieldChange('memoForAddCreditsToWorker', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            </Flex>
          )}
          {!isAddCreditsForTheWorker && (
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1 / 2} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        تحويل رصيد من فنى لى فنى أخر
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={isAddCreditsForWorkerToAnother}
                        disabled={
                          isEmpty(whoCausedTheComplaint) || isEmpty(whoWillSolveTheComplaint)
                        }
                        onChange={() => this.handleCheckBoxChange('isAddCreditsForWorkerToAnother')}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
          )}
          {isAddCreditsForWorkerToAnother && (
            <AddCreditsFromWorkerToAnother
              whoCausedTheComplaint={whoCausedTheComplaint}
              whoWillSolveTheComplaint={whoWillSolveTheComplaint}
              creditsWillDeductFromWorker={creditsWillDeductFromWorker}
              creditsWillGrant={creditsWillGrant}
              commentOfCreditsWillDeductFromWorker={commentOfCreditsWillDeductFromWorker}
              commentOfCreditsWillGrant={commentOfCreditsWillGrant}
              handleInputFieldChange={this.handleInputFieldChange}
            />
          )}
          {!isAddCreditsForTheCustomer && (
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1 / 2} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        تعويض العميل فى حسابه البنكى
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={isAddCreditsForCustomerInHisOnlineBank}
                        onChange={() =>
                          this.handleCheckBoxChange('isAddCreditsForCustomerInHisOnlineBank')
                        }
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
          )}
          {isAddCreditsForCustomerInHisOnlineBank && (
            <Flex flexDirection="column">
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    الرصيد
                  </Text>
                  <InputField
                    type="text"
                    placeholder="الرصيد"
                    width={1}
                    value={creditsForCustomerOnlineCompensation}
                    onChange={value =>
                      this.handleInputFieldChange('creditsForCustomerOnlineCompensation', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تصنيف العملية
                  </Text>
                  <TransactionsClassificationDropdown
                    mb={3}
                    isMulti={false}
                    value={customTransactionClassificationCodeForCustomerOnlineCompensation}
                    handleInputChange={value =>
                      this.handleInputFieldChange(
                        'customTransactionClassificationCodeForCustomerOnlineCompensation',
                        value,
                      )
                    }
                    isGetTransactionsForCustomer
                  />
                </Flex>
              </Flex>
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تعليق
                  </Text>
                  <InputField
                    type="textarea"
                    placeholder="تعليق"
                    width={1}
                    value={memoForCustomerOnlineCompensation}
                    onChange={value =>
                      this.handleInputFieldChange('memoForCustomerOnlineCompensation', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            </Flex>
          )}
          <Flex pb={3} px={4}>
            <Flex flexDirection="column" width={1 / 2} px={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                <Box>
                  <Label>
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                      اضافة رسوم حل الشكوى
                    </Text>
                    <Checkbox
                      name="isComplaintResolutionFee"
                      checked={isComplaintResolutionFee}
                      disabled={isEmpty(whoCausedTheComplaint)}
                      onChange={() => this.handleCheckBoxChange('isComplaintResolutionFee')}
                      sx={{ color: '#fcfffa' }}
                    />
                  </Label>
                </Box>
              </Text>
            </Flex>
          </Flex>
          <Flex
            flexDirection="row-reverse"
            alignItems="center"
            justifyContent="space-between"
            pb={5}
            px={4}
          >
            <Button
              color={BRANDING_BLUE}
              onClick={this.handleClickSubmitButton}
              isLoading={isClosingComplainFetching}
              disabled={isClosingComplainFetching || !descriptionForHowTheComplaintSolved}
              mx={1}
            >
              غلق الشكوى
            </Button>
            {isClosingComplainError && (
              <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                {closeComplainErrorMessage}
              </Caution>
            )}
          </Flex>
        </Flex>
      </Modal>
    );
  }
}
MarkComplainAsSolvedModal.displayName = 'MarkComplainAsSolvedModal';

const mapStateToProps = state => ({
  isClosingComplainFetching: state.tickets.closeComplaint.isFetching,
  isClosingComplainSuccess: state.tickets.closeComplaint.isSuccess,
  isClosingComplainError: state.tickets.closeComplaint.isFail.isError,
  closeComplainErrorMessage: state.tickets.closeComplaint.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      closeComplaint,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(MarkComplainAsSolvedModal);
