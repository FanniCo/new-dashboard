import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import update from 'immutability-helper';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import Text from 'components/Text';
import IconButton from 'components/Buttons/IconButton';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import Caution from 'components/Caution';
import ComplainCard from 'components/SingleTicket/ComplainCard';
import _ from 'lodash';

const { GREY_LIGHT, DISABLED, WHITE, BRANDING_GREEN, PRIMARY, LIGHT_RED, ERROR } = COLORS;
const { TITLE } = FONT_TYPES;

const newComplaint = {
  complainName: [],
  complainReceivedIn: null,
  procedures: [],
  whoCausedTheComplaint: undefined,
  whoWillSolveTheComplaint: undefined,
};

class Complaints extends Component {
  complaintsEndRef = React.createRef();

  constructor(props) {
    super(props);

    const { ticket } = props;

    this.state = {
      complaints: ticket.complaints,
    };
  }

  componentDidUpdate(prevProps) {
    const { adminAddedRequest, toggleAdminAddedRequest } = this.props;

    if (adminAddedRequest) {
      this.handleClickSaveComplainsButton();
      toggleAdminAddedRequest();
    }
  }

  handleComplaintNameSelectChange = (value, index) => {
    const { complaints } = this.state;

    this.setState({
      complaints: update(complaints, {
        [index]: { complainName: { $set: value ? value.value : undefined } },
      }),
    });
  };

  handleComplaintsReceivedInSelectChange = (val, index) => {
    const { complaints } = this.state;

    this.setState({
      complaints: update(complaints, {
        [index]: { complainReceivedIn: { $set: val ? val.value : '' } },
      }),
    });
  };

  handleComplaintProceduresChange = (val, index) => {
    const { complaints } = this.state;
    this.setState({
      complaints: update(complaints, {
        [index]: {
          procedures: { $set: val.map(item => ({ name: item.label, code: item.value })) },
        },
      }),
    });
  };

  handleWhoWillSolveTheComplaintChange = (val, index) => {
    const { complaints } = this.state;

    this.setState({
      complaints: update(complaints, {
        [index]: {
          whoWillSolveTheComplaint: {
            $set: _.get(val, 'value')
              ? { _id: _.get(val, 'value'), name: _.get(val, 'label') }
              : undefined,
          },
        },
      }),
    });
  };

  handleWhoCausedTheComplaintChange = (val, index) => {
    const { complaints } = this.state;

    this.setState({
      complaints: update(complaints, {
        [index]: {
          whoCausedTheComplaint: {
            $set: _.get(val, 'value')
              ? { _id: _.get(val, 'value'), name: _.get(val, 'label') }
              : undefined,
          },
        },
      }),
    });
  };

  addNewComplaint = () => {
    const { complaints } = this.state;

    this.setState({ complaints: [...complaints, newComplaint] });

    this.scrollToBottom();
  };

  loadComplaints = () => {
    const {
      user,
      ticket: { _id },
      ticket,
      toggleMarkTicketAsModal,
      handleToggleAddNewRequestModal,
    } = this.props;

    const { complaints } = this.state;

    return complaints.map((complaint, index) => (
      <ComplainCard
        key={complaint._id}
        index={index}
        user={user}
        ticketId={_id}
        ticket={ticket}
        complaint={complaint}
        handleComplaintProceduresChange={this.handleComplaintProceduresChange}
        handleComplaintNameSelectChange={this.handleComplaintNameSelectChange}
        handleComplaintsReceivedInSelectChange={this.handleComplaintsReceivedInSelectChange}
        toggleMarkTicketAsModal={toggleMarkTicketAsModal}
        handleWhoCausedTheComplaintChange={this.handleWhoCausedTheComplaintChange}
        handleWhoWillSolveTheComplaintChange={this.handleWhoWillSolveTheComplaintChange}
        handleToggleAddNewRequestModal={handleToggleAddNewRequestModal}
      />
    ));
  };

  handleClickSaveComplainsButton = () => {
    const {
      user,
      handleUpdateTicket,
      ticket: { _id: ticketId },
    } = this.props;
    const { complaints } = this.state;
    const computedComplaints = complaints.map(complain => {
      const {
        updatedAt,
        openedBy,
        procedures,
        whoCausedTheComplaint,
        whoWillSolveTheComplaint,
        complainName,
        complainReceivedIn,
        ...otherComplaintProps
      } = complain;

      const computedProcedures = procedures.map(procedure => {
        if (procedure._id) {
          return procedure;
        }

        return {
          procedureName: procedure.code,
          openedBy: user._id,
        };
      });

      const computedComplaintsProps = {
        complainName: complain.complainName.code,
        complainReceivedIn: complain.complainReceivedIn.code,
        procedures: computedProcedures,
        whoCausedTheComplaint: _.get(whoCausedTheComplaint, '_id', ''),
        whoWillSolveTheComplaint: _.get(whoWillSolveTheComplaint, '_id', ''),
        ...otherComplaintProps,
      };
      return { ...computedComplaintsProps };
    });

    handleUpdateTicket({ _id: ticketId, complaints: computedComplaints });
  };

  scrollToBottom = () => {
    this.complaintsEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  render() {
    const {
      user: { permissions },
      isUpdateTicketFetching,
      isUpdateTicketError,
      updateTicketErrorMessage,
    } = this.props;

    return (
      <Flex width={1} flexDirection="column" alignItems="flex-end" px={3}>
        <Flex
          mb={3}
          width={1}
          flexDirection="row-reverse"
          alignItems="center"
          justifyContent="space-between"
        >
          <Text type={TITLE} my={3} color={COLORS_VALUES[DISABLED]}>
            أنواع الشكاوي
          </Text>
          {permissions && permissions.includes('Update Request Ticket') && (
            <Flex>
              <Button
                reverse
                iconWidth="lg"
                mx={2}
                color={COLORS_VALUES[PRIMARY]}
                isLoading={isUpdateTicketFetching}
                disabled={isUpdateTicketFetching}
                onClick={() => this.handleClickSaveComplainsButton()}
              >
                حفظ التغييرات
              </Button>
              <IconButton
                iconSize="lg"
                mx={2}
                icon={faPlus}
                color={COLORS_VALUES[BRANDING_GREEN]}
                iconColor={BRANDING_GREEN}
                buttonBorderColor={WHITE}
                buttonBackgroundColor={GREY_LIGHT}
                onClick={() => {
                  this.addNewComplaint();
                }}
              />
              {isUpdateTicketError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {updateTicketErrorMessage}
                </Caution>
              )}
            </Flex>
          )}
        </Flex>
        {this.loadComplaints()}
        <div ref={this.complaintsEndRef} />
      </Flex>
    );
  }
}

Complaints.displayName = 'Complaints';

Complaints.propTypes = {
  user: PropTypes.shape({}),
  ticket: PropTypes.shape({}).isRequired,
  handleUpdateTicket: PropTypes.func,
  isUpdateTicketFetching: PropTypes.bool,
  isUpdateTicketError: PropTypes.bool,
  updateTicketErrorMessage: PropTypes.string,
  toggleMarkTicketAsModal: PropTypes.func,
  handleToggleAddNewRequestModal: PropTypes.func,
  adminAddedRequest: PropTypes.bool,
};

Complaints.defaultProps = {
  user: undefined,
  handleUpdateTicket: () => {},
  isUpdateTicketFetching: false,
  isUpdateTicketError: false,
  updateTicketErrorMessage: undefined,
  toggleMarkTicketAsModal: undefined,
  handleToggleAddNewRequestModal: () => {},
  adminAddedRequest: false,
};

export default Complaints;
