import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import _ from 'lodash';

const OpenedByRow = props => {
  const { openedBy, ticketCreatedAtDate } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const ticketOpenedBy = _.get(openedBy, 'name');

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          تم فتحها بواسطة
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {`${` [ ${ticketCreatedAtDate &&
            ticketCreatedAtDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
            })} ] `}`}{' '}
          {`${ticketOpenedBy}`}
        </Text>
      </Flex>
    </EvenRow>
  );
};
OpenedByRow.displayName = 'OpenedByRow';

OpenedByRow.propTypes = {
  openedBy: PropTypes.shape({}).isRequired,
  ticketCreatedAtDate: PropTypes.shape({}).isRequired,
};

export default OpenedByRow;
