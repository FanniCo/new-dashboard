import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { TICKETS_STATUS } from 'utils/constants';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import Text from 'components/Text';
import ConfirmModal from 'components/ConfirmModal';
import Comments from 'components/Comments';
import { SubTitle, StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import Caution from 'components/Caution';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import EmptyState from 'components/EmptyState';
import _ from 'lodash';
import AddNewRequestModal from 'components/AddNewRequestModal';
import Requests from 'components/SingleTicket/Requests';
import { borderBottom, color } from 'styled-system';
import Transactions from './Transactions/Transactions';
import CustomerNameRow from './CustomerNameRow';
import CustomerUserNameRow from './CustomerUserNameRow';
import FieldRow from './FieldRow';
import RequestPrettyIdRow from './RequestPrettyIdRow';
import OpenedByRow from './OpenedByRow';
import CreatedAtDateRow from './CreatedAtDateRow';
import UpdatedAtDateRow from './UpdatedAtDateRow';
import CallToActions from './CallToActions';
import Complaints from './Complaints';
import MarkComplainAsSolvedModal from './MarkComplainAsSolvedModal';
import DescriptionRow from './DescriptionRow';
import ActualInvoiceRow from './ActualInvoiceRow';
import ImagesRow from './ImagesRow';

const { GREY_DARK, GREY_LIGHT, BRANDING_GREEN, DISABLED, LIGHT_RED, WHITE, ERROR } = COLORS;
const { TITLE, BIG_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;
const TicketStatusKeys = Object.keys(TICKETS_STATUS);
const ticketStatusSelectOptions = TicketStatusKeys.map(key => ({
  value: key,
  label: TICKETS_STATUS[key].ar,
}));

const PageTitle = styled(Flex)`
  direction: rtl;
`;

const CommentsContainer = styled(Flex)`
  direction: rtl;
  background-color: ${COLORS_VALUES[GREY_LIGHT]};
`;

const TabsContainer = styled(Flex)`
  ${borderBottom};
`;
const Tab = styled(Flex)`
  cursor: pointer;
  width: max-content;
  ${color};
  ${borderBottom};
`;

const { HEADING } = FONT_TYPES;

const SingleTicket = props => {
  const {
    isGetTicketDetailsFetching,
    isGetTicketDetailsError,
    getTicketDetailsErrorMessage,
    ticketDetails,
    adminAddedRequest,
  } = props;

  if (isGetTicketDetailsFetching || !ticketDetails) {
    return (
      <ShimmerEffect width={1}>
        <Rect width={1 / 3} height={550} m={2} />
        <Rect width={2 / 3} height={550} m={2} />
      </ShimmerEffect>
    );
  }

  if (isGetTicketDetailsError) {
    return (
      <Box width={1}>
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text={getTicketDetailsErrorMessage}
            />
          </Card>
        </Flex>
      </Box>
    );
  }

  const {
    user,
    user: { permissions },
    ticketDetails: {
      _id,
      prettyId,
      openedBy,
      customer,
      customer: { name: customerName, username: customerUserName },
      request: { requestPrettyId, field, location },
      createdAt,
      updatedAt,
      latestStatus,
      description,
      images,
      actualInvoice,
    },
    updateTicket,
    isUpdateTicketFetching,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    deleteTicket,
    isDeleteTicketFetching,
    addedCommentValue,
    handleAddCommentInputChange,
    resetAddCommentInput,
    addComment,
    addCommentState,
    addCommentErrorMessage,
    isUpdateTicketError,
    updateTicketErrorMessage,
    admins,
    mentionData,
    isDeleteTicketError,
    isDeleteTicketErrorMessage,
    toggleMarkTicketAsModal,
    isOpenMarkTicketAsSolvedModal,
    isMarkingTicketAsError,
    markingTicketAsErrorMessage,
    isMarkingTicketAsFetching,
    activeComplain,
    isAddNewRequestModalOpen,
    handleToggleAddNewRequestModal,
    tabs,
    activeTab,
    handleTabChange,
    toggleAdminAddedRequest,
  } = props;
  const selectedTicketStatus = ticketStatusSelectOptions.find(
    option => option.value === latestStatus.code,
  );
  const ticketActionMaker = latestStatus.createdBy;
  const ticketActionUpdatedAt = new Date(updatedAt);
  const ticketCreatedAtDate = new Date(createdAt);
  const ticketUpdatedAtDate = new Date(updatedAt);
  const handleUpdateTicketFunction = value => {
    updateTicket({ _id, status: value.value });
  };

  return (
    <Flex flexDirection="column" alignItems="flex-end" width={1}>
      <Helmet>
        <title>رقم الشكوي : {prettyId.toString()}</title>
      </Helmet>
      <PageTitle mb={5} pt={5} alignItems="center" flexWrap="wrap">
        <Text type={TITLE} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
          رقم الشكوي : {prettyId}
        </Text>
        {isOpenMarkTicketAsSolvedModal && (
          <MarkComplainAsSolvedModal
            isOpened={isOpenMarkTicketAsSolvedModal}
            toggleFunction={toggleMarkTicketAsModal}
            complain={activeComplain}
            ticketId={_id}
            customer={customer}
            isMarkingTicketAsSolvedError={isMarkingTicketAsError}
            markingTicketAsSolvedErrorMessage={markingTicketAsErrorMessage}
            isMarkingTicketAsSolvedFetching={isMarkingTicketAsFetching}
          />
        )}
        {isAddNewRequestModalOpen && (
          <AddNewRequestModal
            customerId={customer._id}
            ticketId={_id}
            location={location}
            workerId={_.get(activeComplain, 'whoWillSolveTheComplaint._id')}
            isShowPromoSection={false}
          />
        )}
        <Flex alignItems="center" mr={2}>
          <StyledSelect
            width="180px"
            mx={2}
            placeholder="حالة المشكلة"
            noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
            isRtl
            backspaceRemovesValue={false}
            value={selectedTicketStatus}
            onChange={handleUpdateTicketFunction}
            options={ticketStatusSelectOptions}
            isDisabled={
              isUpdateTicketFetching ||
              !(permissions && permissions.includes('Update Request Ticket'))
            }
            isLoading={isUpdateTicketFetching}
            theme={selectColors}
            styles={customSelectStyles}
          />
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            - بواسطة :
          </SubTitle>
          <Text mx={1} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`${ticketActionMaker}`}
          </Text>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`${ticketActionUpdatedAt &&
              ` [ ${ticketActionUpdatedAt &&
                ticketActionUpdatedAt.toLocaleDateString('en-US', {
                  year: 'numeric',
                  month: 'numeric',
                  day: 'numeric',
                })} ] `}`}
          </Text>
        </Flex>
        {isUpdateTicketError && (
          <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
            {updateTicketErrorMessage}
          </Caution>
        )}
      </PageTitle>

      <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
        <Flex flexDirection="column" width={2 / 3}>
          <TabsContainer flexDirection="row-reverse" alignItems="center">
            {tabs.map((tab, tabIndex) => (
              <Tab
                key={tab.name}
                flexDirection="row-reverse"
                alignItems="center"
                px={5}
                py={3}
                borderBottom={
                  activeTab === tabIndex ? `2px solid ${COLORS_VALUES[BRANDING_GREEN]}` : 'none'
                }
                onClick={() => handleTabChange(tabIndex)}
              >
                <Text
                  cursor="pointer"
                  textAlign="center"
                  color={
                    activeTab === tabIndex ? COLORS_VALUES[BRANDING_GREEN] : COLORS_VALUES[DISABLED]
                  }
                  type={HEADING}
                  fontWeight={NORMAL}
                  mx={3}
                >
                  {tab.name}
                </Text>
              </Tab>
            ))}
          </TabsContainer>
          {activeTab === 0 && (
            <Box>
              <Card
                py={1}
                ml={[0, 0, 0, 0, 3]}
                mb={5}
                flexDirection="column"
                bgColor={COLORS_VALUES[GREY_DARK]}
              >
                <FieldRow field={field} />
                <RequestPrettyIdRow requestPrettyId={requestPrettyId} />
                <CustomerNameRow name={customerName} />
                <CustomerUserNameRow username={customerUserName} />
                <OpenedByRow openedBy={openedBy} ticketCreatedAtDate={ticketCreatedAtDate} />
                <CreatedAtDateRow ticketCreatedAtDate={ticketCreatedAtDate} />
                <UpdatedAtDateRow ticketUpdatedAtDate={ticketUpdatedAtDate} />
                <DescriptionRow description={description} />
                <ImagesRow images={images} />
                <ActualInvoiceRow actualInvoice={actualInvoice} />
              </Card>
            </Box>
          )}
          {activeTab === 1 && (
            <Box>
              <Card
                p={3}
                mb={2}
                flexDirection="column"
                alignItems="center"
                bgColor={COLORS_VALUES[GREY_DARK]}
                ml={[0, 0, 0, 0, 0]}
              >
                <Transactions user={user} ticketId={_id} />
              </Card>
            </Box>
          )}
          {activeTab === 2 && (
            <Box>
              <Card
                p={3}
                mb={2}
                flexDirection="column"
                alignItems="center"
                bgColor={COLORS_VALUES[GREY_DARK]}
                ml={[0, 0, 0, 0, 0]}
              >
                <Requests ticketId={_id} user={user} />
              </Card>
            </Box>
          )}
          <Complaints
            adminAddedRequest={adminAddedRequest}
            toggleAdminAddedRequest={toggleAdminAddedRequest}
            user={user}
            ticket={ticketDetails}
            handleUpdateTicket={updateTicket}
            isUpdateTicketFetching={isUpdateTicketFetching}
            isUpdateTicketError={isUpdateTicketError}
            updateTicketErrorMessage={updateTicketErrorMessage}
            toggleMarkTicketAsModal={toggleMarkTicketAsModal}
            handleToggleAddNewRequestModal={handleToggleAddNewRequestModal}
          />
        </Flex>
        <Flex flexDirection="column" width={1 / 3}>
          <Box>
            <CallToActions
              user={user}
              ticket={ticketDetails}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleDeleteTicket={deleteTicket}
            />
            <CommentsContainer width={1} mb={2}>
              <Comments
                isModal={false}
                objectType="ticket"
                object={ticketDetails}
                addedCommentValue={addedCommentValue}
                handleAddCommentInputChange={handleAddCommentInputChange}
                addComment={addComment}
                addCommentState={addCommentState}
                addCommentErrorMessage={addCommentErrorMessage}
                resetAddCommentInput={resetAddCommentInput}
                admins={admins}
                mentionData={mentionData}
              />
            </CommentsContainer>
          </Box>
        </Flex>
      </Flex>
      {isConfirmModalOpen && (
        <ConfirmModal
          isFail={isDeleteTicketError}
          errorMessage={isDeleteTicketErrorMessage}
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isDeleteTicketFetching}
        />
      )}
    </Flex>
  );
};
SingleTicket.displayName = 'SingleTicket';

SingleTicket.propTypes = {
  user: PropTypes.shape({}),
  ticketDetails: PropTypes.shape({}),
  isGetTicketDetailsFetching: PropTypes.bool,
  updateTicket: PropTypes.func,
  isUpdateTicketFetching: PropTypes.bool,
  isConfirmModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  deleteTicket: PropTypes.func,
  isDeleteTicketFetching: PropTypes.bool,
  addCommentState: PropTypes.string,
  addCommentErrorMessage: PropTypes.string,
  addComment: PropTypes.func,
  addedCommentValue: PropTypes.string,
  handleAddCommentInputChange: PropTypes.func,
  resetAddCommentInput: PropTypes.func,
  isUpdateTicketError: PropTypes.bool,
  updateTicketErrorMessage: PropTypes.string,
  admins: PropTypes.arrayOf(PropTypes.shape({})),
  mentionData: PropTypes.arrayOf(PropTypes.shape({})),
  isDeleteTicketError: PropTypes.bool,
  isDeleteTicketErrorMessage: PropTypes.string,
  toggleMarkTicketAsModal: PropTypes.func,
  isOpenMarkTicketAsSolvedModal: PropTypes.bool,
  isMarkingTicketAsError: PropTypes.bool,
  markingTicketAsErrorMessage: PropTypes.string,
  isMarkingTicketAsFetching: PropTypes.bool,
  isGetTicketDetailsError: PropTypes.bool,
  getTicketDetailsErrorMessage: PropTypes.string,
  activeComplain: PropTypes.shape({}),
  isAddNewRequestModalOpen: PropTypes.bool,
  handleToggleAddNewRequestModal: PropTypes.func,
  toggleAdminAddedRequest: PropTypes.func,
};

SingleTicket.defaultProps = {
  user: undefined,
  ticketDetails: undefined,
  isGetTicketDetailsFetching: false,
  updateTicket: () => {},
  isUpdateTicketFetching: false,
  isConfirmModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  deleteTicket: () => {},
  isDeleteTicketFetching: false,
  addCommentState: undefined,
  addCommentErrorMessage: undefined,
  addComment: () => {},
  addedCommentValue: '',
  handleAddCommentInputChange: () => {},
  resetAddCommentInput: () => {},
  isUpdateTicketError: false,
  updateTicketErrorMessage: undefined,
  admins: [],
  mentionData: [],
  isDeleteTicketError: false,
  isDeleteTicketErrorMessage: undefined,
  toggleMarkTicketAsModal: () => {},
  isOpenMarkTicketAsSolvedModal: false,
  isMarkingTicketAsError: false,
  markingTicketAsErrorMessage: undefined,
  isMarkingTicketAsFetching: false,
  isGetTicketDetailsError: false,
  getTicketDetailsErrorMessage: undefined,
  activeComplain: undefined,
  isAddNewRequestModalOpen: false,
  handleToggleAddNewRequestModal: () => {},
  toggleAdminAddedRequest: () => {},
};

export default SingleTicket;
