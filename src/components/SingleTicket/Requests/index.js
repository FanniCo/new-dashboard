import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faSpinner } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import {
  getAllRequests,
  cancelRequest,
  toggleRequestFollowUpsModal,
} from 'redux-modules/requests/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { FONT_TYPES } from 'components/theme/fonts';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import RequestsList from './RequestsList';

const { WHITE, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

const RequestsListFlex = styled(Flex)`
  ${minHeight};
`;

class RequestsContainer extends Component {
  static propTypes = {
    ticketId: PropTypes.string,
    isGetAllRequestsFetching: PropTypes.bool,
    isGetAllRequestsError: PropTypes.bool,
    getAllRequests: PropTypes.func,
    cancelRequest: PropTypes.func,
    isCancelRequestFetching: PropTypes.bool,
    isCancelRequestSuccess: PropTypes.bool,
    requests: PropTypes.arrayOf(PropTypes.shape({})),
    hasMoreRequests: PropTypes.bool,
    user: PropTypes.shape({}),
    isRequestFollowsUpModalOpen: PropTypes.bool,
    toggleRequestFollowUpsModal: PropTypes.func,
    getAllRequestsErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    ticketId: undefined,
    requests: [],
    hasMoreRequests: true,
    isGetAllRequestsFetching: false,
    isGetAllRequestsError: false,
    getAllRequests: () => {},
    cancelRequest: () => {},
    isCancelRequestFetching: false,
    isCancelRequestSuccess: false,
    user: undefined,
    isRequestFollowsUpModalOpen: false,
    toggleRequestFollowUpsModal: () => {},
    getAllRequestsErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    const { ticketId } = props;

    this.state = {
      isConfirmModalOpen: false,
      isOpsCommentsModalOpen: false,
      isFilterOpen: false,
      confirmModalHeader: '',
      confirmModalText: '',
      confirmModalFunction: () => {},
      filterQueries: {
        skip: 0,
        limit: 20,
        ticket: ticketId,
      },
      activeRequestModalIndex: undefined,
    };
  }

  componentDidMount() {
    const { getAllRequests: getAllRequestsAction } = this.props;
    const { filterQueries } = this.state;

    getAllRequestsAction(filterQueries, true);
  }

  componentDidUpdate(prevProps) {
    const { isCancelRequestSuccess } = this.props;

    if (isCancelRequestSuccess && isCancelRequestSuccess !== prevProps.isCancelRequestSuccess) {
      this.handleToggleConfirmModal('', '', () => {});
    }
  }

  /**
   * Creates lazy loading request block
   */
  getLoadingRequest = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 2} height={250} m={2} />
      <Rect width={1 / 2} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests blocks
   */
  createLoadingRequestsList = () => {
    const list = [];
    for (let counter = 0; counter < 2; counter += 1) {
      list.push(this.getLoadingRequest(counter));
    }
    return list;
  };

  loadMoreRequests = () => {
    const { getAllRequests: getAllRequestsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllRequestsAction(filterQueriesNextState);
      },
    );
  };

  handleToggleConfirmModal = (header, confirmText, confirmFunction) => {
    const { isConfirmModalOpen } = this.state;

    this.setState(
      {
        confirmModalHeader: !isConfirmModalOpen ? header : '',
        confirmModalText: !isConfirmModalOpen ? confirmText : '',
        confirmModalFunction: !isConfirmModalOpen ? confirmFunction : () => {},
      },
      () => {
        this.setState({
          isConfirmModalOpen: !isConfirmModalOpen,
        });
      },
    );
  };

  handleToggleOpsCommentsModal = index => {
    const { isOpsCommentsModalOpen } = this.state;

    this.setState({ activeRequestModalIndex: index }, () => {
      this.setState({
        isOpsCommentsModalOpen: !isOpsCommentsModalOpen,
      });
    });
  };

  handleToggleFilterModal = () => {
    const { isFilterOpen } = this.state;

    this.setState({ isFilterOpen: !isFilterOpen });
  };

  handleChangeFilterQueries = (type, val) => {
    const { filterQueries } = this.state;

    this.setState({ filterQueries: { ...filterQueries, [type]: val, skip: 0, limit: 20 } });
  };

  handleToggleRequestFollowsUpModal = index => {
    const { toggleRequestFollowUpsModal: toggleRequestFollowUpsModalAction } = this.props;
    this.setState({ activeRequestModalIndex: index }, () => {
      toggleRequestFollowUpsModalAction();
    });
  };

  render() {
    const {
      requests,
      hasMoreRequests,
      isGetAllRequestsFetching,
      isGetAllRequestsError,
      cancelRequest: cancelRequestAction,
      isCancelRequestFetching,
      user,
      isRequestFollowsUpModalOpen,
      getAllRequestsErrorMessage,
    } = this.props;
    const {
      isConfirmModalOpen,
      isOpsCommentsModalOpen,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      activeRequestModalIndex,
    } = this.state;

    if (isGetAllRequestsFetching) {
      return this.createLoadingRequestsList();
    }

    if (isGetAllRequestsError) {
      return (
        <Box width={1} p={3} flexWrap="wrap" flexDirection="column">
          <RequestsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
            <Card width={1} minHeight={340} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text={getAllRequestsErrorMessage}
              />
            </Card>
          </RequestsListFlex>
        </Box>
      );
    }

    if (!requests.length) {
      return (
        <Box width={1} p={3} flexWrap="wrap" flexDirection="column">
          <RequestsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
            <Card width={1} minHeight={340} alignItems="center" backgroundColor={GREY_LIGHT}>
              <EmptyState
                icon={faFile}
                iconColor={BRANDING_GREEN}
                iconSize="3x"
                textColor={COLORS_VALUES[WHITE]}
                textSize={BIG_TITLE}
                text="لا يوجد طلبات حل لهذه الشكوى"
              />
            </Card>
          </RequestsListFlex>
        </Box>
      );
    }

    return (
      <Box width={1} p={3} flexWrap="wrap" flexDirection="column">
        <RequestsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
          <Box width={1}>
            <InfiniteScroll
              dataLength={requests.length}
              next={this.loadMoreRequests}
              hasMore={hasMoreRequests}
              height={600}
              loader={
                <Flex m={2} justifyContent="center" alignItems="center">
                  <FontAwesomeIcon
                    icon={faSpinner}
                    size="lg"
                    spin
                    color={COLORS_VALUES[BRANDING_GREEN]}
                  />
                </Flex>
              }
            >
              <RequestsList
                requests={requests}
                isConfirmModalOpen={isConfirmModalOpen}
                confirmModalHeader={confirmModalHeader}
                confirmModalText={confirmModalText}
                confirmModalFunction={confirmModalFunction}
                handleToggleConfirmModal={this.handleToggleConfirmModal}
                handleCancelRequest={cancelRequestAction}
                isCancelRequestFetching={isCancelRequestFetching}
                isOpsCommentsModalOpen={isOpsCommentsModalOpen}
                handleToggleOpsCommentsModal={this.handleToggleOpsCommentsModal}
                activeRequestModalIndex={activeRequestModalIndex}
                user={user}
                isRequestFollowsUpModalOpen={isRequestFollowsUpModalOpen}
                handleToggleRequestFollowsUpModal={this.handleToggleRequestFollowsUpModal}
              />
            </InfiniteScroll>
          </Box>
        </RequestsListFlex>
      </Box>
    );
  }
}
RequestsContainer.displayName = 'RequestsContainer';

const mapStateToProps = state => ({
  requests: state.requests.requests,
  hasMoreRequests: state.requests.hasMoreRequests,
  applyNewFilter: state.requests.applyNewFilter,
  isGetAllRequestsFetching: state.requests.getAllRequests.isFetching,
  isGetAllRequestsError: state.requests.getAllRequests.isFail.isError,
  getAllRequestsErrorMessage: state.requests.getAllRequests.isFail.message,
  isGetAllRequestsSuccess: state.requests.getAllRequests.isSuccess,
  isCancelRequestFetching: state.requests.cancelRequest.isFetching,
  isCancelRequestSuccess: state.requests.cancelRequest.isSuccess,
  isConfirmModalOpen: state.requests.confirmModal.isOpen,
  isOpsCommentsModalOpen: state.requests.opsCommentsModal.isOpen,
  isRequestFollowsUpModalOpen: state.requests.requestFollowUpsModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllRequests,
      cancelRequest,
      toggleRequestFollowUpsModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(RequestsContainer);
