import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import RequestCard from 'components/RequestCard';
import ConfirmModal from 'components/ConfirmModal';
import OpsCommentsModal from 'components/OpsCommentsModal';
import RequestFollowsUpModal from 'components/RequestFollowsUpModal';

const RequestsList = props => {
  const {
    requests,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleCancelRequest,
    isCancelRequestFetching,
    isOpsCommentsModalOpen,
    handleToggleOpsCommentsModal,
    activeRequestModalIndex,
    user,
    isRequestFollowsUpModalOpen,
    handleToggleRequestFollowsUpModal,
  } = props;

  const requestsRenderer = requests.map((request, index) => {
    const { _id } = request;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 2]}>
        <RequestCard
          m={2}
          index={index}
          user={user}
          request={request}
          showWorkerName
          showClientName={false}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleCancelRequest={handleCancelRequest}
          isCancelRequestFetching={isCancelRequestFetching}
          handleToggleOpsCommentsModal={handleToggleOpsCommentsModal}
          handleToggleRequestFollowsUpModal={handleToggleRequestFollowsUpModal}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {requestsRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isCancelRequestFetching}
        />
      )}
      {isOpsCommentsModalOpen && (
        <OpsCommentsModal
          objectType="request"
          objectArray={requests}
          isOpened={isOpsCommentsModalOpen}
          toggleFunction={handleToggleOpsCommentsModal}
          objectIndex={activeRequestModalIndex}
        />
      )}
      {isRequestFollowsUpModalOpen && (
        <RequestFollowsUpModal
          user={user}
          objectType="request"
          request={requests[activeRequestModalIndex]}
          isOpened={isRequestFollowsUpModalOpen}
          toggleFunction={handleToggleRequestFollowsUpModal}
          objectIndex={activeRequestModalIndex}
        />
      )}
    </Flex>
  );
};
RequestsList.displayName = 'RequestsList';

RequestsList.propTypes = {
  requests: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isConfirmModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleCancelRequest: PropTypes.func,
  isCancelRequestFetching: PropTypes.bool,
  isOpsCommentsModalOpen: PropTypes.bool,
  handleToggleOpsCommentsModal: PropTypes.func,
  activeRequestModalIndex: PropTypes.number,
  user: PropTypes.shape({}),
  isRequestFollowsUpModalOpen: PropTypes.bool,
  handleToggleRequestFollowsUpModal: PropTypes.func,
};

RequestsList.defaultProps = {
  isConfirmModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleCancelRequest: () => {},
  isCancelRequestFetching: false,
  isOpsCommentsModalOpen: false,
  handleToggleOpsCommentsModal: () => {},
  activeRequestModalIndex: undefined,
  user: undefined,
  isRequestFollowsUpModalOpen: false,
  handleToggleRequestFollowsUpModal: () => {},
};

export default RequestsList;
