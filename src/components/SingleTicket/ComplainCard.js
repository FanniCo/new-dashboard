import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Box, Flex} from '@rebass/grid';
import Text from 'components/Text';
import {FONT_TYPES} from 'components/theme/fonts';
import {
  COLORS,
  COLORS_VALUES
} from 'components/theme/colors';
import {
  customSelectStyles,
  selectColors,
  SubTitle
} from 'components/shared';
import Card from 'components/Card';
import Button from 'components/Buttons';
import ComplaintsDropDown
  from 'containers/shared/ComplaintsDropDown';
import ComplaintReceivedInDropDown
  from 'containers/shared/ComplaintReceivedInDropDown';
import ComplaintProceduresDropDown
  from 'containers/shared/ComplaintProceduresDropDown';
import AsyncSelect
  from 'react-select/async/dist/react-select.esm';
import {getUserByUsername} from 'redux-modules/user/sagas';
import _ from 'lodash';

const {
  GREY_LIGHT,
  DISABLED,
  BRANDING_GREEN,
  LIGHT_RED,
  CYAN
} = COLORS;
const {HEADING} = FONT_TYPES;

class ComplainCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadingAssignWorkerToTheTicketToSolve: false,
    };
  }

  featchUsers = (inputValue, whoCausedTheComplaint) => {
    const defaultUser = {
      label: _.get(whoCausedTheComplaint, 'name'),
      value: _.get(whoCausedTheComplaint, '_id'),
    };

    return new Promise(async resolve => {
      const foundUsers = await getUserByUsername(inputValue);
      if (foundUsers && foundUsers.length) {
        const users = foundUsers
          .filter(user => user.role === 'worker')
          .map(user => ({
            label: user.name,
            value: user._id
          }));

        users.push(defaultUser);

        return resolve(users);
      }

      return resolve([defaultUser]);
    });
  };

  render() {
    const {
      user: {permissions},
      index,
      complaint,
      handleComplaintNameSelectChange,
      handleComplaintsReceivedInSelectChange,
      handleComplaintProceduresChange,
      toggleMarkTicketAsModal,
      handleWhoCausedTheComplaintChange,
      handleWhoWillSolveTheComplaintChange,
      handleToggleAddNewRequestModal,
      ticket,
    } = this.props;
    const {isLoadingAssignWorkerToTheTicketToSolve} = this.state;

    const {
      complainName,
      complainReceivedIn,
      procedures,
      openedBy,
      createdAt,
      whoCausedTheComplaint,
      whoWillSolveTheComplaint,
      isSolved,
    } = complaint;
    const complaintCreatedAtDate = new Date(createdAt);

    const getSelectValue = value => ({
      value: value.code,
      label: value.name
    });
    const complaintSelectedValue = complainName ? getSelectValue(complainName) : undefined;
    const complaintReceivedInSelectedValue = complainReceivedIn
      ? getSelectValue(complainReceivedIn)
      : complainReceivedIn;
    const complaintProceduresSelectedValue =
      procedures.length > 0 ? procedures.map(procedure => getSelectValue(procedure)) : undefined;
    const defaultOptions = [];

    if (!_.isEmpty(whoCausedTheComplaint) || _.get(ticket, 'request.worker')) {
      defaultOptions.push({
        label: _.get(whoCausedTheComplaint || _.get(ticket, 'request.worker'), 'name'),
        value: _.get(whoCausedTheComplaint || _.get(ticket, 'request.worker'), '_id'),
      });
    }

    return (
      <Flex flexDirection="row-reverse" width={1}>
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            key={`${complainName}_${index}`}
            width={1}
            p={3}
            mb={2}
            flexDirection="column"
            alignItems="center"
            bgColor={COLORS_VALUES[GREY_LIGHT]}
            ml={[0, 0, 0, 0, 0]}
          >
            {openedBy && (
              <Flex width={1}>
                {/*{isSolved && (*/}
                {/*  <Flex flexDirection="column" px={1}>*/}
                {/*    <Button mx={1} color={ILLUSTRATION_BACKGROUND} onClick={() => {}}>*/}
                {/*      هذة الشكوي مغلقة*/}
                {/*    </Button>*/}
                {/*  </Flex>*/}
                {/*)}*/}
                {!isSolved && (
                  <>
                    <Flex flexDirection="column" px={1}>
                      <Button
                        mx={1}
                        color={LIGHT_RED}
                        onClick={() =>
                          toggleMarkTicketAsModal('isOpenMarkTicketAsSolvedModal', complaint)
                        }
                      >
                        غلق هذة الشكوي
                      </Button>
                    </Flex>
                    <Flex flexDirection="column" px={1}>
                      <Button
                        minWidth="calc(50% - 8px)"
                        mx={1}
                        mb={2}
                        color={CYAN}
                        onClick={() => handleToggleAddNewRequestModal(complaint)}
                        iconWidth="lg"
                        reverse
                        xMargin={3}
                        isLoading={false}
                      >
                        طلب خدمة
                      </Button>
                    </Flex>
                  </>
                )}
                <Flex flexDirection="column" width={1}
                      px={1}>
                  <Flex width={1} px={1}
                        flexDirection="row-reverse"
                        justifyContent="flex-start">
                    <SubTitle
                      color={COLORS_VALUES[DISABLED]}>تم فتح
                      هذه الشكوى بواسطة :</SubTitle>
                    <Text mx={1}
                          color={COLORS_VALUES[BRANDING_GREEN]}>
                      {`${openedBy.name}`}
                    </Text>
                    <Text
                      color={COLORS_VALUES[BRANDING_GREEN]}>
                      {`[ ${complaintCreatedAtDate.toLocaleDateString('en-US', {
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                      })} ]`}
                    </Text>
                  </Flex>
                </Flex>
              </Flex>
            )}
            <Flex width={1} pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Flex width={1} px={1}
                      flexDirection="row-reverse"
                      justifyContent="flex-start">
                  <SubTitle
                    color={COLORS_VALUES[DISABLED]}> وصف حل
                    الشكوى</SubTitle>
                  <Text
                    color={COLORS_VALUES[BRANDING_GREEN]}>
                    {complaint.descriptionForHowTheComplaintSolved || '-'}
                  </Text>
                </Flex>
              </Flex>
            </Flex>
            <Flex width={1}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text textAlign="right" mb={2}
                      type={HEADING}
                      color={COLORS_VALUES[BRANDING_GREEN]}>
                  نوع الشكوي
                </Text>
                <ComplaintsDropDown
                  isMulti
                  complainSelectedValue={complaintSelectedValue}
                  handleInputChange={value => handleComplaintNameSelectChange(value, index)}
                  isDisabled={
                    !(permissions && permissions.includes('Update Request Ticket'))
                  }
                />
              </Flex>
              <Flex flexDirection="column" width={1} px={1}>
                <Text textAlign="right" mb={2}
                      type={HEADING}
                      color={COLORS_VALUES[BRANDING_GREEN]}>
                  طريقة استلام الشكوي
                </Text>
                <ComplaintReceivedInDropDown
                  isMulti={false}
                  complaintReceivedInSelectedValue={complaintReceivedInSelectedValue}
                  handleInputChange={value => handleComplaintsReceivedInSelectChange(value, index)}
                  isDisabled={
                    !(permissions && permissions.includes('Update Request Ticket'))
                  }
                />
              </Flex>
            </Flex>
            <Flex width={1} mt={4}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text textAlign="right" mb={2}
                      type={HEADING}
                      color={COLORS_VALUES[BRANDING_GREEN]}>
                  طرق التصرف تجاه الشكوي
                </Text>
                <ComplaintProceduresDropDown
                  complaintProceduresSelectedValue={complaintProceduresSelectedValue}
                  handleInputChange={value => handleComplaintProceduresChange(value, index)}
                  isDisabled={
                    !(permissions && permissions.includes('Update Request Ticket'))
                  }
                />
              </Flex>
            </Flex>
            <Flex width={1} mt={4}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text textAlign="right" mb={2}
                      type={HEADING}
                      color={COLORS_VALUES[BRANDING_GREEN]}>
                  المسئول عن حل المشكله
                </Text>
                <AsyncSelect
                  mx={2}
                  isRtl
                  isClearable
                  isMulti={false}
                  value={
                    whoWillSolveTheComplaint
                      ? {
                        label: _.get(whoWillSolveTheComplaint, 'name'),
                        value: _.get(whoWillSolveTheComplaint, '_id'),
                      }
                      : undefined
                  }
                  isLoading={isLoadingAssignWorkerToTheTicketToSolve}
                  cacheOptions
                  theme={selectColors}
                  styles={customSelectStyles}
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  placeholder="قم بكتابة رقم الفنى"
                  onChange={value => handleWhoWillSolveTheComplaintChange(value, index)}
                  defaultOptions={defaultOptions}
                  loadOptions={inputValue => this.featchUsers(inputValue, whoCausedTheComplaint)}
                  isDisabled={
                    !(permissions && permissions.includes('Update Request Ticket'))
                  }
                />
              </Flex>
              <Flex flexDirection="column" width={1} px={1}>
                <Text textAlign="right" mb={2}
                      type={HEADING}
                      color={COLORS_VALUES[BRANDING_GREEN]}>
                  المتسبب في المشكله
                </Text>
                <AsyncSelect
                  mx={2}
                  isRtl
                  isClearable
                  isMulti={false}
                  value={
                    whoCausedTheComplaint
                      ? {
                        label: _.get(whoCausedTheComplaint, 'name'),
                        value: _.get(whoCausedTheComplaint, '_id'),
                      }
                      : undefined
                  }
                  isLoading={isLoadingAssignWorkerToTheTicketToSolve}
                  cacheOptions
                  theme={selectColors}
                  styles={customSelectStyles}
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  placeholder="قم بكتابة رقم الفنى"
                  onChange={value => handleWhoCausedTheComplaintChange(value, index)}
                  defaultOptions={defaultOptions}
                  loadOptions={inputValue => this.featchUsers(inputValue, whoCausedTheComplaint)}
                  isDisabled={
                    !(permissions && permissions.includes('Update Request Ticket'))
                  }
                />
              </Flex>
            </Flex>
          </Card>
        </Box>
      </Flex>
    );
  }
}

ComplainCard.displayName = 'ComplainCard';

ComplainCard.propTypes = {
  user: PropTypes.shape({}),
  index: PropTypes.number,
  complaint: PropTypes.shape({}),
  handleComplaintProceduresChange: PropTypes.func,
  handleComplaintNameSelectChange: PropTypes.func,
  handleComplaintsReceivedInSelectChange: PropTypes.func,
  toggleMarkTicketAsModal: PropTypes.func,
  handleWhoCausedTheComplaintChange: PropTypes.func,
  handleWhoWillSolveTheComplaintChange: PropTypes.func,
  handleToggleAddNewRequestModal: PropTypes.func,
};

ComplainCard.defaultProps = {
  user: undefined,
  index: undefined,
  complaint: undefined,
  handleComplaintProceduresChange: () => {
  },
  handleComplaintNameSelectChange: () => {
  },
  handleComplaintsReceivedInSelectChange: () => {
  },
  toggleMarkTicketAsModal: () => {
  },
  handleWhoCausedTheComplaintChange: () => {
  },
  handleWhoWillSolveTheComplaintChange: () => {
  },
  handleToggleAddNewRequestModal: () => {
  },
};

export default ComplainCard;
