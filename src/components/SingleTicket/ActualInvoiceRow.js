import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const ActualInvoiceRow = props => {
  const { actualInvoice } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          الفاتورة
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          <Flex width={1}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجرة الفنى: {actualInvoice.workersWage || '-'}
            </Text>
          </Flex>
          <Flex width={1}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              قطع الغيار: {actualInvoice.partsCost || '-'}
            </Text>
          </Flex>
        </Text>
      </Flex>
    </OddRow>
  );
};
ActualInvoiceRow.displayName = 'ActualInvoiceRow';

ActualInvoiceRow.propTypes = {
  actualInvoice: PropTypes.string,
};

ActualInvoiceRow.defaultProps = {
  actualInvoice: '-',
};

export default ActualInvoiceRow;
