import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow, HyperLink, HyperLinkText } from 'components/shared';
import ROUTES from 'routes';

const { SINGLE_REQUEST } = ROUTES;

const RequestPrettyIdRow = props => {
  const { requestPrettyId } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          الرقم المختصر للطلب
        </Text>
      </Flex>
      <Flex width={0.5} flexDirection="row-reverse">
        <HyperLink href={`${SINGLE_REQUEST}/${requestPrettyId}`} target="_blank">
          <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {requestPrettyId}
          </HyperLinkText>
        </HyperLink>
      </Flex>
    </OddRow>
  );
};
RequestPrettyIdRow.displayName = 'RequestPrettyIdRow';

RequestPrettyIdRow.propTypes = {
  requestPrettyId: PropTypes.number.isRequired,
};

export default RequestPrettyIdRow;
