import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { getAllTransactions } from 'redux-modules/transactions/actions';
import TransactionsList from '../../TransactionsLogs';

class TransactionsContainer extends Component {
  static propTypes = {
    transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
    transactionsState: PropTypes.shape({}),
    isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
    getAllTransactions: PropTypes.func,
    isGetAllTransactionsFetching: PropTypes.bool,
    isGetAllTransactionsSuccess: PropTypes.bool,
    isGetAllTransactionsError: PropTypes.bool,
    applyNewFilter: PropTypes.bool,
    getAllTransactionsErrorMessage: PropTypes.string,
    ticketId: PropTypes.string,
  };

  static defaultProps = {
    transactionsList: [],
    transactionsState: undefined,
    getAllTransactions: () => {},
    isGetAllTransactionsFetching: false,
    isGetAllTransactionsSuccess: false,
    isGetAllTransactionsError: false,
    applyNewFilter: false,
    getAllTransactionsErrorMessage: undefined,
    ticketId: undefined,
  };

  constructor(props) {
    super(props);

    const { ticketId } = props;

    this.state = {
      filterQueries: {
        skip: 0,
        limit: 20,
        ticketId,
      },
    };
  }

  componentDidMount() {
    const { getAllTransactions: getAllTransactionsAction } = this.props;
    const { filterQueries } = this.state;

    getAllTransactionsAction(filterQueries, true);
  }

  loadMoreTransactions = () => {
    const { getAllTransactions: getAllTransactionsAction } = this.props;

    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getAllTransactionsAction(filterQueriesNextState);
      },
    );
  };

  render() {
    const {
      transactionsList,
      transactionsState,
      isLoadMoreTransactionsAvailable,
      isGetAllTransactionsFetching,
      isGetAllTransactionsSuccess,
      isGetAllTransactionsError,
      applyNewFilter,
      getAllTransactionsErrorMessage,
    } = this.props;
    return (
      <TransactionsList
        transactionsList={transactionsList}
        transactionsState={transactionsState}
        isLoadMoreTransactionsAvailable={isLoadMoreTransactionsAvailable}
        loadMoreTransactions={this.loadMoreTransactions}
        isGetAllTransactionsFetching={isGetAllTransactionsFetching}
        isGetAllTransactionsSuccess={isGetAllTransactionsSuccess}
        isGetAllTransactionsError={isGetAllTransactionsError}
        applyNewFilter={applyNewFilter}
        getAllTransactionsErrorMessage={getAllTransactionsErrorMessage}
      />
    );
  }
}
TransactionsContainer.displayName = 'TransactionsContainer';

const mapStateToProps = state => ({
  applyNewFilter: state.transactions.applyNewFilter,
  transactionsList: state.transactions.transactions,
  transactionsState: state.transactions.getAllTransactions,
  isGetAllTransactionsFetching: state.transactions.getAllTransactions.isFetching,
  isGetAllTransactionsSuccess: state.transactions.getAllTransactions.isSuccess,
  isGetAllTransactionsError: state.transactions.getAllTransactions.isFail.isError,
  getAllTransactionsErrorMessage: state.transactions.getAllTransactions.isFail.message,
  isLoadMoreTransactionsAvailable: state.transactions.hasMoreTransactions,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getAllTransactions }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(TransactionsContainer);
