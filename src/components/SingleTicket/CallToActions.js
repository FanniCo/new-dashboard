import React from 'react';
import PropTypes from 'prop-types';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import Card from 'components/Card';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';

const CallToActions = props => {
  const {
    user: { permissions },
    ticket: { _id },
    handleDeleteTicket,
    handleToggleConfirmModal,
  } = props;
  const { GREY_LIGHT, LIGHT_RED } = COLORS;
  const deleteTicketConfirmModalHeader = 'تأكيد مسح الشكوي';
  const deleteTicketConfirmModalConfirmText = 'هل أنت متأكد أنك تريد مسح الشكوي؟';
  const deleteTicketConfirmModalFunction = () => {
    handleDeleteTicket(_id, undefined);
  };

  return (
    permissions &&
    permissions.includes('Delete Request Ticket') && (
      <Card
        flexWrap="wrap"
        justifyContent="center"
        bgColor={COLORS_VALUES[GREY_LIGHT]}
        mb={2}
        px={2}
        py={3}
      >
        <Button
          minWidth="100%"
          ml={1}
          mr={1}
          color={LIGHT_RED}
          onClick={() =>
            handleToggleConfirmModal(
              deleteTicketConfirmModalHeader,
              deleteTicketConfirmModalConfirmText,
              deleteTicketConfirmModalFunction,
            )
          }
          icon={faTrashAlt}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          مسح الشكوي
        </Button>
      </Card>
    )
  );
};
CallToActions.displayName = 'CallToActions';

CallToActions.propTypes = {
  user: PropTypes.shape({}),
  ticket: PropTypes.shape({}).isRequired,
  handleDeleteTicket: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
};

CallToActions.defaultProps = {
  user: undefined,
  handleDeleteTicket: () => {},
  handleToggleConfirmModal: () => {},
};

export default CallToActions;
