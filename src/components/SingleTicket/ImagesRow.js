import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import styled from 'styled-components';

const Image = styled.img`
  width: calc(100% - 25px);
`;

const ImagesRow = props => {
  const { images } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.3}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          الصور
        </Text>
      </Flex>
      <Flex width={0.7}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {images.map(image => (
            <>
              {/* <Flex width={0.4}> */}
              {/*  <Text */}
              {/*    textAlign="right" */}
              {/*    width={1} */}
              {/*    color={COLORS_VALUES[DISABLED]} */}
              {/*    type={SUBHEADING} */}
              {/*    fontWeight={NORMAL} */}
              {/*  > */}
              <a href={image} rel="noreferrer" target="_blank">
                <Image src={image} style={{ width: '20%', height: '41%', 'margin-left': '6%' }} />
              </a>
              {/*  </Text> */}
              {/* </Flex> */}
            </>
          ))}
        </Text>
      </Flex>
    </EvenRow>
  );
};
ImagesRow.displayName = 'ImagesRow';

ImagesRow.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string),
};

ImagesRow.defaultProps = {
  images: '-',
};

export default ImagesRow;
