import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter, CallToActionIcon, CardLabel } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ReactTooltip from 'react-tooltip';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FIELDS } from 'utils/constants';
import { getFieldIcon } from 'utils/shared/style';

const CommissionCard = props => {
  const {
    commission,
    commission: { _id, field, rate, rateForOnline },
    index,
    user: { permissions },
    handleToggleConfirmModal,
    handleToggleEditCommissionModal,
    deleteCommissionAction,
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, LIGHT_RED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const deleteCommissionTooltip = 'مسح معدل العموله';
  const editCommissionTooltip = 'تعديل معدل العموله';
  const fieldIcon = getFieldIcon(field);

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Flex flexDirection="row-reverse" width={1 / 2} py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              قيمة العمولة الكاش :
            </SubTitle>
            <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {`${rate} %`}
            </Text>
          </Flex>
          <Flex
            key={field}
            mb={2}
            width={1 / 2}
            flexDirection="row-reverse"
            alignItems="center"
            justifyContent="flex-end"
          >
            <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
            <CardLabel
              py={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {FIELDS[field].ar}
            </CardLabel>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" width={1} py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            قيمة العمولة الاونلين :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`${rateForOnline || 0} %`}
          </Text>
        </Flex>
      </CardBody>
      {permissions &&
        (permissions.includes('Delete Commission') ||
          permissions.includes('Update Commission')) && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
            {permissions && permissions.includes('Delete Commission') && (
                <>
                  <ReactTooltip id={`deleteCommission_${_id}`} type="error" effect="solid">
                    <span>{deleteCommissionTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`deleteCommission_${_id}`}
                      data-tip
                      onClick={() =>
                        handleToggleConfirmModal(
                          'تأكيد مسح معدل العموله',
                          'هل أنت متأكد أنك تريد مسح معدل العموله؟',
                          () => {
                            deleteCommissionAction(_id, index);
                          },
                        )
                      }
                      icon={faTrashAlt}
                      color={COLORS_VALUES[LIGHT_RED]}
                      size="sm"
                    />
                  </Box>
                </>
            )}
            {permissions && permissions.includes('Update Commission') && (
                <>
                  <ReactTooltip id={`editCommission_${_id}`} type="light" effect="solid">
                    <span>{editCommissionTooltip}</span>
                  </ReactTooltip>
                  <Box>
                    <CallToActionIcon
                      data-for={`editCommission_${_id}`}
                      data-tip
                      onClick={() => handleToggleEditCommissionModal(index, commission)}
                      icon={faPen}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
            )}
          </Flex>
        </CardFooter>
      )}
    </Card>
  );
};
CommissionCard.displayName = 'CommissionCard';

CommissionCard.propTypes = {
  commission: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditCommissionModal: PropTypes.func,
  deleteCommissionAction: PropTypes.func,
};

CommissionCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditCommissionModal: () => {},
  deleteCommissionAction: () => {},
};

export default CommissionCard;
