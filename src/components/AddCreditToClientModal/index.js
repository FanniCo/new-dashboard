import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isNaN } from 'lodash';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import TransactionsClassificationDropdown from 'containers/shared/TransactionsClassificationDropdown';
import { clientAddCredit, toggleClientAddCreditModal } from 'redux-modules/clients/actions';
import { Checkbox, Label } from '@rebass/forms';
import DatePicker from 'react-datepicker/es';
import moment from 'moment';
import { space } from 'styled-system';
import InputField from '../InputField';
import Caution from '../Caution';

const AddCreditFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const StyledDatePicker = styled(DatePicker)`
  ${space};
  background-color: ${props => props.backgroundColor};
  display: block;
  border: 1px solid;
  border-color: ${props => (props.isDanger ? COLORS_VALUES[COLORS.LIGHT_RED] : props.borderColor)};
  border-radius: ${props => props.theme.space[1]}px;
  color: ${props => COLORS_VALUES[props.color ? props.color : COLORS.DISABLED]};
  font-weight: ${props => props.theme.fontWeights.NORMAL};
  line-height: normal;
  outline: 0;
  width: calc(100% - 16px);
  font-size: 16px;
  direction: ltr;

  &:lang(ar) {
    padding: ${props => props.theme.space[2]}px;
    text-align: right;
  }

  &[type='number'] {
    -moz-appearance: textfield;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;

class ClientAddCreditModal extends Component {
  static propTypes = {
    clientIndex: PropTypes.number,
    clientDetails: PropTypes.shape({}),
    isAddCreditFetching: PropTypes.bool,
    isAddCreditError: PropTypes.bool,
    addCreditErrorMessage: PropTypes.string,
    clientAddCredit: PropTypes.func,
    toggleClientAddCreditModal: PropTypes.func,
    isAddCreditModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    clientIndex: undefined,
    clientDetails: undefined,
    isAddCreditFetching: false,
    isAddCreditError: false,
    addCreditErrorMessage: '',
    clientAddCredit: () => {},
    toggleClientAddCreditModal: () => {},
    isAddCreditModalOpen: false,
  };

  state = {
    credits: '',
    customTransactionClassificationCode: null,
    memo: '',
    isTempCredit: false,
    expireAt: null,
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeTransactionsClassificationSelect = customTransactionClassificationCode => {
    this.setState({ customTransactionClassificationCode });
  };

  onSubmitAddCreditForm = e => {
    e.preventDefault();
    const {
      clientIndex,
      clientDetails: { _id: clientId },
      clientAddCredit: addCreditAction,
    } = this.props;
    const {
      credits,
      customTransactionClassificationCode,
      memo,
      isTempCredit,
      expireAt,
    } = this.state;
    const convertedCreditsNumbers = convertArabicNumbersToEnglish(credits);

    const addCreditInfo = {
      customerId: clientId,
      worth: isNaN(Number(convertedCreditsNumbers))
        ? convertedCreditsNumbers
        : Number(convertedCreditsNumbers),
      memo,
      customTransactionClassificationCode: customTransactionClassificationCode.value,
      isTempCredit,
      expireAt,
    };

    addCreditAction(clientIndex, addCreditInfo);
  };

  handleCheckBoxChange = type => {
    this.setState(prevState => ({
      [type]: !prevState[type],
    }));
  };

  render() {
    const {
      isAddCreditFetching,
      isAddCreditModalOpen,
      toggleClientAddCreditModal: toggleAddCreditModalAction,
      isAddCreditError,
      addCreditErrorMessage,
    } = this.props;
    const {
      credits,
      customTransactionClassificationCode,
      memo,
      isTempCredit,
      expireAt,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE, DISABLED, GREY_LIGHT } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addCreditModalHeader = 'أضف رصيد';

    return (
      <Modal
        toggleModal={toggleAddCreditModalAction}
        header={addCreditModalHeader}
        isOpened={isAddCreditModalOpen}
      >
        <AddCreditFormContainer onSubmit={this.onSubmitAddCreditForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الرصيد
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الرصيد"
                  width={1}
                  value={credits}
                  onChange={value => this.handleInputFieldChange('credits', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تصنيف العملية
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <TransactionsClassificationDropdown
                  mb={3}
                  isMulti={false}
                  handleInputChange={value =>
                    this.handleChangeTransactionsClassificationSelect(value)
                  }
                  isGetTransactionsForCustomer
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        إضافه كرصيد مؤقت؟
                      </Text>
                      <Checkbox
                        name="tempCredit"
                        checked={isTempCredit}
                        onChange={() => this.handleCheckBoxChange('isTempCredit')}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
            {isTempCredit && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1} px={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تاريخ انتهاء الرصيد المؤقت
                  </Text>
                  <StyledDatePicker
                    mb={5}
                    inline={false}
                    placeholderText="تاريخ انتهاء الرصيد المؤقت"
                    dateFormat="MM/dd/yyyy 00:00:00"
                    borderColor={COLORS_VALUES[DISABLED]}
                    backgroundColor={COLORS_VALUES[GREY_LIGHT]}
                    selected={expireAt || undefined}
                    onChange={value => this.handleInputFieldChange('expireAt', value)}
                    minDate={new Date(moment().add(1, 'days'))}
                    isClearable
                  />
                </Flex>
              </Flex>
            )}

            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تعليق
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="تعليق"
                  width={1}
                  value={memo}
                  onChange={value => this.handleInputFieldChange('memo', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddCreditForm}
                disabled={
                  isAddCreditFetching || !credits || !customTransactionClassificationCode || !memo
                }
                isLoading={isAddCreditFetching}
              >
                أضف رصيد
              </Button>
              {isAddCreditError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addCreditErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddCreditFormContainer>
      </Modal>
    );
  }
}
ClientAddCreditModal.displayName = 'ClientAddCreditModal';

const mapStateToProps = state => ({
  isAddCreditFetching: state.clients.addCredit.isFetching,
  isAddCreditError: state.clients.addCredit.isFail.isError,
  addCreditErrorMessage: state.clients.addCredit.isFail.message,
  isAddCreditModalOpen: state.clients.clientAddCreditsModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      clientAddCredit,
      toggleClientAddCreditModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ClientAddCreditModal);
