import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ReactTooltip from 'react-tooltip';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const PostCard = props => {
  const {
    post,
    post: { _id, title, description, createdBy, createdAt },
    index,
    user: { permissions },
    handleToggleConfirmModal,
    handleToggleEditPostModal,
    deletePostAction,
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, LIGHT_RED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const postDate = new Date(createdAt);
  const deletePostTooltip = 'مسح المقال';
  const editPostTooltip = 'تعديل المقال';

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {postDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العنوان :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {title.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الوصف :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {description.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
      </CardBody>
      {permissions && (permissions.includes('Delete Post') || permissions.includes('Update Post')) && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
            {permissions && permissions.includes('Delete Post') && (
              <>
                <ReactTooltip id={`deletePost_${_id}`} type="error" effect="solid">
                  <span>{deletePostTooltip}</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`deletePost_${_id}`}
                    data-tip
                    onClick={() =>
                      handleToggleConfirmModal(
                        'تأكيد مسح الاعلان',
                        'هل أنت متأكد أنك تريد مسح الاعلان؟',
                        () => {
                          deletePostAction(_id, index);
                        },
                      )
                    }
                    icon={faTrashAlt}
                    color={COLORS_VALUES[LIGHT_RED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
            {permissions && permissions.includes('Update Post') && (
              <>
                <ReactTooltip id={`editPost_${_id}`} type="light" effect="solid">
                  <span>{editPostTooltip}</span>
                </ReactTooltip>
                <Box>
                  <CallToActionIcon
                    data-for={`editPost_${_id}`}
                    data-tip
                    onClick={() => handleToggleEditPostModal(index, post)}
                    icon={faPen}
                    color={COLORS_VALUES[DISABLED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
          </Flex>
        </CardFooter>
      )}
    </Card>
  );
};
PostCard.displayName = 'PostCard';

PostCard.propTypes = {
  post: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditPostModal: PropTypes.func,
  deletePostAction: PropTypes.func,
};

PostCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditPostModal: () => {},
  deletePostAction: () => {},
};

export default PostCard;
