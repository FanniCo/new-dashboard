import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import PostCard from 'components/PostCard';
import PostFormModal from 'components/PostFormModal';
import ConfirmModal from 'components/ConfirmModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { FONT_TYPES } from 'components/theme/fonts';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class Posts extends Component {
  getLoadingPosts = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingPostsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingPosts(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      postsList,
      isAddNewPostModalOpen,
      isConfirmModalOpen,
      isDeletePostFetching,
      deletePostAction,
      handleToggleConfirmModal,
      isEditPostModalOpen,
      handleToggleEditPostModal,
      postDetails,
      postIndex,
      isGetAllPostsFetching,
      isGetAllPostsSuccess,
      isGetAllPostsError,
      isGetAllPostsErrorMessage,
      applyNewFilter,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isDeletePostError,
      isDeletePostErrorMessage,
    } = this.props;

    let postsRenderer;

    if (
      (isGetAllPostsFetching && (!postsList || applyNewFilter)) ||
      (!isGetAllPostsFetching && !isGetAllPostsSuccess && !isGetAllPostsError)
    ) {
      postsRenderer = this.createLoadingPostsList();
    } else if (isGetAllPostsError) {
      postsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllPostsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!postsList.length) {
      postsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (postsList.length) {
      postsRenderer = postsList.map((post, index) => {
        const { _id } = post;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <PostCard
              m={2}
              index={index}
              user={user}
              post={post}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditPostModal={handleToggleEditPostModal}
              deletePostAction={deletePostAction}
            />
          </Box>
        );
      });
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {postsRenderer}
        {isAddNewPostModalOpen && <PostFormModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            isFail={isDeletePostError}
            errorMessage={isDeletePostErrorMessage}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={confirmModalFunction}
            isConfirming={isDeletePostFetching}
          />
        )}
        {isEditPostModalOpen && <PostFormModal postDetails={postDetails} postIndex={postIndex} />}
      </Flex>
    );
  }
}

Posts.displayName = 'PostsList';

Posts.propTypes = {
  user: PropTypes.shape({}),
  postsList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewPostModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  deletePostAction: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isDeletePostFetching: PropTypes.bool,
  handleToggleEditPostModal: PropTypes.func,
  isEditPostModalOpen: PropTypes.bool,
  postDetails: PropTypes.shape({}),
  postIndex: PropTypes.number,
  isGetAllPostsFetching: PropTypes.bool,
  isGetAllPostsSuccess: PropTypes.bool,
  isGetAllPostsError: PropTypes.bool,
  isGetAllPostsErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  isDeletePostError: PropTypes.bool,
  isDeletePostErrorMessage: PropTypes.string,
};

Posts.defaultProps = {
  user: undefined,
  postsList: undefined,
  isAddNewPostModalOpen: false,
  handleToggleConfirmModal: () => {},
  deletePostAction: () => {},
  isConfirmModalOpen: false,
  // deleteCustomers: () => {},
  isDeletePostFetching: false,
  handleToggleEditPostModal: () => {},
  isEditPostModalOpen: false,
  postDetails: {},
  postIndex: undefined,
  isGetAllPostsFetching: false,
  isGetAllPostsSuccess: false,
  isGetAllPostsError: false,
  isGetAllPostsErrorMessage: undefined,
  applyNewFilter: false,
  confirmModalHeader: undefined,
  confirmModalText: undefined,
  confirmModalFunction: () => {},
  isDeletePostError: false,
  isDeletePostErrorMessage: undefined,
};

export default Posts;
