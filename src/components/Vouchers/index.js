import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import VoucherCard from 'components/VoucherCard';
import ConfirmModal from 'components/ConfirmModal';
import AddNewVoucherModal from 'components/VoucherModal';

const Vouchers = props => {
  const {
    user,
    vouchers,
    isConfirmModalOpen,
    isAddVoucherModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleDeleteVoucher,
    isDeleteVoucherFetching,
    isDeleteVoucherError,
    isDeleteVoucherErrorMessage,
    handleToggleEditVoucherModal,
    isEditVoucherModalOpen,
    voucherDetails,
    voucherIndex,
  } = props;

  const vouchersRenderer = vouchers.map((voucherCode, index: number) => {
    const { _id } = voucherCode;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <VoucherCard
          m={2}
          index={index}
          voucher={voucherCode}
          user={user}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleDeleteVoucher={handleDeleteVoucher}
          handleToggleEditVoucherModal={handleToggleEditVoucherModal}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {vouchersRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isFail={isDeleteVoucherError}
          errorMessage={isDeleteVoucherErrorMessage}
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isDeleteVoucherFetching}
        />
      )}
      {isAddVoucherModalOpen && <AddNewVoucherModal />}
      {isEditVoucherModalOpen && (
        <AddNewVoucherModal voucherDetails={voucherDetails} voucherIndex={voucherIndex} />
      )}
    </Flex>
  );
};

Vouchers.displayName = 'Vouchers';

Vouchers.propTypes = {
  user: PropTypes.shape({}),
  vouchers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isConfirmModalOpen: PropTypes.bool,
  isAddVoucherModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleDeleteVoucher: PropTypes.func,
  isDeleteVoucherFetching: PropTypes.bool,
  handleToggleEditVoucherModal: PropTypes.func,
  isEditVoucherModalOpen: PropTypes.bool,
  voucherDetails: PropTypes.shape({}),
  voucherIndex: PropTypes.number,
};

Vouchers.defaultProps = {
  user: undefined,
  isConfirmModalOpen: false,
  isAddVoucherModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleDeleteVoucher: () => {},
  isDeleteVoucherFetching: false,
  handleToggleEditVoucherModal: () => {},
  isEditVoucherModalOpen: false,
  voucherDetails: undefined,
  voucherIndex: false,
};

export default Vouchers;
