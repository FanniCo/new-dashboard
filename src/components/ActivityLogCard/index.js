import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { ACTIVITIES_LOGS } from 'utils/constants';

const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
const { BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const WorkerCard = props => {
  const {
    activityLog: { action, actionDescription, user, createdAt, adminCity },
  } = props;
  const activityDate = new Date(createdAt);
  return (
    <Card
      {...props}
      height="calc(100% - 16px)"
      minHeight={100}
      flexDirection="column"
      justifyContent="space-between"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={3} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            النشاط :
          </SubTitle>
          <Flex flexDirection="row" alignItems="center">
            <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {ACTIVITIES_LOGS[action].ar}
            </Text>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            التاريخ :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {activityDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الوصف :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {actionDescription}
          </Text>
        </Flex>
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="space-between" flexWrap="wrap" p={2}>
        <Flex px={2}>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {user.name}
          </Text>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اسم المستخدم :
          </SubTitle>
        </Flex>
        <Flex px={2}>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {user.username}
          </Text>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            (رقم الهاتف) :
          </SubTitle>
        </Flex>
        <Flex px={2}>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {adminCity || '-'}
          </Text>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            (المدينة) :
          </SubTitle>
        </Flex>
      </CardFooter>
    </Card>
  );
};
WorkerCard.displayName = 'WorkerCard';

WorkerCard.propTypes = {
  user: PropTypes.shape({}),
  activityLog: PropTypes.shape({}).isRequired,
};

WorkerCard.defaultProps = {
  user: undefined,
};

export default WorkerCard;
