import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { TRANSACTION_TYPES, FIELDS, TRANSACTION_STATUS, CITIES } from 'utils/constants';
import { getFieldIcon } from 'utils/shared/style';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, HyperLink, HyperLinkText } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import styled from 'styled-components';

const TestClickable = styled(Text)`
  cursor: pointer;
  color: #e0e0e0;
  &:hover {
    color: #25c281;
  }
`;

const TransactionCard = props => {
  const {
    transaction: {
      type,
      createdAt,
      status,
      prettyId,
      worth,
      balance,
      commissionRate,
      customTransactionClassification,
      memo,
      request,
      destinationBank,
      worker,
      createdBy,
      customer,
      vatValue,
      customerWalletRefund,
      promoCodeRefund,
      vendorCommission,
      vendor,
      city,
      deliveryCostOfPartsRefund,
      income,
      incomeWorth,
      donation,
    },
    showWorkerName,
    showClientName,
    handleToggleOpenVendorDetailsModal,
  } = props;
  const { SINGLE_REQUEST, SINGLE_WORKER, SINGLE_CLIENT } = ROUTES;
  const { BRANDING_GREEN, BRANDING_BLUE, GREY_LIGHT, DISABLED } = COLORS;
  const { SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const transactionDate = new Date(createdAt);
  const transactionClassification = customTransactionClassification
    ? `${customTransactionClassification.name.ar} - ${customTransactionClassification.code}`
    : 'لم يتم تحديدها';

  let fieldsContent;

  if (worker) {
    const { field, fields } = worker;

    if (fields && fields.length) {
      fieldsContent = fields.map(fieldKey => {
        const fieldIcon = getFieldIcon(fieldKey);

        return (
          <Flex key={fieldKey} ml={4} mb={1}>
            <Box>
              <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
            </Box>
            <CardLabel
              py={1}
              px={2}
              ml={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {FIELDS[fieldKey].ar}
            </CardLabel>
          </Flex>
        );
      });
    } else if (field) {
      const fieldIcon = getFieldIcon(field);

      fieldsContent = (
        <Flex alignItems="center" ml={4} mb={1}>
          <Box>
            <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
          </Box>
          <CardLabel
            py={1}
            px={2}
            ml={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            {FIELDS[field].ar}
          </CardLabel>
        </Flex>
      );
    } else {
      fieldsContent = (
        <Flex alignItems="center" ml={4} mb={1}>
          <CardLabel
            py={1}
            px={2}
            ml={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            لا يوجد تخصص
          </CardLabel>
        </Flex>
      );
    }
  }

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={1} px={2} width={1}>
          <Flex alignItems="center">
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={1}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {prettyId}
            </CardLabel>
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={1}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {TRANSACTION_STATUS[status].ar}
            </CardLabel>
          </Flex>
          <SubTitle type={SUBHEADING} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {(type !== TRANSACTION_TYPES.custom.en || type !== 'onlinePayment') &&
              `${TRANSACTION_TYPES[type].ar} ${((commissionRate || commissionRate === 0) &&
                `${commissionRate * 100}%`) ||
                ''}`}
            {(type === TRANSACTION_TYPES.custom.en || type === 'onlinePayment') &&
              transactionClassification}
          </SubTitle>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {transactionDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        {showWorkerName && worker && (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              الفني :
            </SubTitle>
            <HyperLink href={`${SINGLE_WORKER}/${worker.username}`} target="_blank">
              <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                [ {worker.username} ] - {worker.name}
              </HyperLinkText>
            </HyperLink>
          </Flex>
        )}
        {showClientName && customer && (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              العميل :
            </SubTitle>
            <HyperLink href={`${SINGLE_CLIENT}/${customer.username}`} target="_blank">
              <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                [ {customer.username} ] - {customer.name}
              </HyperLinkText>
            </HyperLink>
          </Flex>
        )}
        {vendor && (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              الشريك :
            </SubTitle>
            <TestClickable
              type={BODY}
              color={COLORS_VALUES[BRANDING_GREEN]}
              fontWeight={NORMAL}
              onClick={() => handleToggleOpenVendorDetailsModal(vendor)}
            >
              [ {vendor.username} ] - {vendor.name}
            </TestClickable>
          </Flex>
        )}
        {worker && (
          <Flex width={1} flexWrap="wrap" flexDirection="row-reverse" py={1} px={2}>
            {fieldsContent}
          </Flex>
        )}
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          {request ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الرقم المختصر للطلب :
              </SubTitle>
              <HyperLink href={`${SINGLE_REQUEST}/${request.requestPrettyId}`} target="_blank">
                <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {request.requestPrettyId}
                </HyperLinkText>
              </HyperLink>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد طلب
            </SubTitle>
          )}
        </Flex>
        <Flex flexDirection="row-reverse" flexWrap="wrap">
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              القيمة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {worth}
            </Text>
          </Flex>
          {vendorCommission > 0 && (
            <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                قيمة العمولة لى الشريك :
              </SubTitle>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {vendorCommission}
              </Text>
            </Flex>
          )}
          {type === TRANSACTION_TYPES.commission.en && (
            <>
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  قيمة الخصم :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {promoCodeRefund || '0'}
                </Text>
              </Flex>
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  متحصل من محفظة العميل :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {customerWalletRefund || '0'}
                </Text>
              </Flex>
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  قيمة الضريبة :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {vatValue || 0}
                </Text>
              </Flex>
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  قيمة تعويض الفنى الخاصة بتوصيل قطع الغيار :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {deliveryCostOfPartsRefund || 0}
                </Text>
              </Flex>
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  المبلغ تم اضافته فى محفظة الدخل للفنى :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {incomeWorth || 0}
                </Text>
              </Flex>
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  رصيد محفظة الدخل :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {income || 0},
                </Text>
              </Flex>
              {donation && (
                <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                  <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                    قيمة التبرع لجمعية خيرية المحصل :
                  </SubTitle>
                  <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                    {donation || 0},
                  </Text>
                </Flex>
              )}
            </>
          )}
          {(balance || balance === 0) && (
            <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الرصيد :
              </SubTitle>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {balance}
              </Text>
            </Flex>
          )}
        </Flex>
        {/* <Flex flexDirection="row-reverse" flexWrap="wrap">
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              معدل العمولة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {commissionRate || 'لم يتم تحديدها'}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              تصنيف العملية :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {transactionClassification}
            </Text>
          </Flex>
        </Flex> */}
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
        {destinationBank && (
          <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={2}>
            <CardLabel py={1} px={2} ml={1} type={BODY} bg={COLORS_VALUES[BRANDING_BLUE]}>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {destinationBank}
              </Text>
            </CardLabel>
          </Flex>
        )}
        {memo && (
          <Flex flexDirection="row-reverse" px={2} mb={2}>
            <CardLabel p={2} ml={1} type={BODY} bg={COLORS_VALUES[BRANDING_BLUE]}>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {memo}
              </Text>
            </CardLabel>
          </Flex>
        )}
        {city && (
          <Flex flexDirection="row-reverse" px={2} mb={2}>
            <CardLabel p={2} ml={1} type={BODY} bg={COLORS_VALUES[BRANDING_BLUE]}>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {CITIES[city].ar}
              </Text>
            </CardLabel>
          </Flex>
        )}
      </CardBody>
    </Card>
  );
};
TransactionCard.displayName = 'TransactionCard';

TransactionCard.propTypes = {
  transaction: PropTypes.shape({}).isRequired,
  showWorkerName: PropTypes.bool,
  showClientName: PropTypes.bool,
  handleToggleOpenVendorDetailsModal: PropTypes.func,
};

TransactionCard.defaultProps = {
  showWorkerName: true,
  showClientName: true,
  handleToggleOpenVendorDetailsModal: () => {},
};

export default TransactionCard;
