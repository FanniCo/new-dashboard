import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import RechargeAwardCard from 'components/RechargeAwardCard';
import ConfirmModal from 'components/ConfirmModal';
import AddNewRechargeAwardModal from 'components/AddNewRechargeAwardModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { FONT_TYPES } from 'components/theme/fonts';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

const RechargeAwards = props => {
  const {
    user,
    rechargeAwards,
    isConfirmModalOpen,
    isAddNewRechargeAwardModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleDeleteRechargeAward,
    isDeleteRechargeAwardFetching,
    handleToggleEditRechargeAwardModal,
    isEditRechargeAwardModalOpen,
    rechargeAwardDetails,
    rechargeAwardIndex,
    isDeleteRechargeAwardError,
    isDeleteRechargeAwardErrorMessage,

    isGetAllRechargeAwardsFetching,
    isGetAllRechargeAwardsSuccess,
    isGetAllRechargeAwardsError,
    getAllRechargeAwardsErrorMessage,
    applyNewFilter,
  } = props;

  /**
   * Creates lazy loading rechargeAwards block
   */
  const getLoadingRechargeAwards = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
      <Rect width={1 / 3} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of rechargeAwards blocks
   */
  const createLoadingRechargeAwardsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(getLoadingRechargeAwards(counter));
    }
    return list;
  };

  let rechargeAwardsRenderer;

  if (
    (isGetAllRechargeAwardsFetching && (!rechargeAwards || applyNewFilter)) ||
    (!isGetAllRechargeAwardsFetching &&
      !isGetAllRechargeAwardsSuccess &&
      !isGetAllRechargeAwardsError)
  ) {
    rechargeAwardsRenderer = createLoadingRechargeAwardsList();
  } else if (isGetAllRechargeAwardsError) {
    rechargeAwardsRenderer = (
      <Box width={1}>
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <Flex m={2} justifyContent="center" alignItems="center">
              <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                <EmptyState
                  icon={faFile}
                  iconColor={BRANDING_GREEN}
                  iconSize="3x"
                  textColor={COLORS_VALUES[WHITE]}
                  textSize={BIG_TITLE}
                  text={getAllRechargeAwardsErrorMessage}
                />
              </Card>
            </Flex>
          </Card>
        </Box>
      </Box>
    );
  } else if (!rechargeAwards.length) {
    rechargeAwardsRenderer = (
      <Box width={1}>
        <Box width={[1, 1, 1, 1, 1]}>
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <Flex m={2} justifyContent="center" alignItems="center">
              <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                <EmptyState
                  icon={faFile}
                  iconColor={BRANDING_GREEN}
                  iconSize="3x"
                  textColor={COLORS_VALUES[WHITE]}
                  textSize={BIG_TITLE}
                  text="لا يوجد سجلات"
                />
              </Card>
            </Flex>
          </Card>
        </Box>
      </Box>
    );
  } else {
    rechargeAwardsRenderer = rechargeAwards.map((rechargeAward, index) => {
      const { _id } = rechargeAward;

      return (
        <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
          <RechargeAwardCard
            m={2}
            index={index}
            user={user}
            rechargeAward={rechargeAward}
            handleToggleConfirmModal={handleToggleConfirmModal}
            handleDeleteRechargeAward={handleDeleteRechargeAward}
            handleToggleEditRechargeAwardModal={handleToggleEditRechargeAwardModal}
          />
        </Box>
      );
    });
  }

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {rechargeAwardsRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isFail={isDeleteRechargeAwardError}
          errorMessage={isDeleteRechargeAwardErrorMessage}
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isDeleteRechargeAwardFetching}
        />
      )}
      {isAddNewRechargeAwardModalOpen && <AddNewRechargeAwardModal />}
      {isEditRechargeAwardModalOpen && (
        <AddNewRechargeAwardModal
          rechargeAwardDetails={rechargeAwardDetails}
          rechargeAwardIndex={rechargeAwardIndex}
        />
      )}
    </Flex>
  );
};
RechargeAwards.displayName = 'RechargeAwards';

RechargeAwards.propTypes = {
  user: PropTypes.shape({}),
  rechargeAwards: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isConfirmModalOpen: PropTypes.bool,
  isAddNewRechargeAwardModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleDeleteRechargeAward: PropTypes.func,
  isDeleteRechargeAwardFetching: PropTypes.bool,

  handleToggleEditRechargeAwardModal: PropTypes.func,
  isEditRechargeAwardModalOpen: PropTypes.bool,
  rechargeAwardDetails: PropTypes.shape({}),
  rechargeAwardIndex: PropTypes.number,
  isDeleteRechargeAwardError: PropTypes.bool,
  isDeleteRechargeAwardErrorMessage: PropTypes.string,

  isGetAllRechargeAwardsFetching: PropTypes.bool,
  isGetAllRechargeAwardsSuccess: PropTypes.bool,
  isGetAllRechargeAwardsError: PropTypes.bool,
  getAllRechargeAwardsErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
};

RechargeAwards.defaultProps = {
  user: undefined,
  isConfirmModalOpen: false,
  isAddNewRechargeAwardModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleDeleteRechargeAward: () => {},
  isDeleteRechargeAwardFetching: false,

  handleToggleEditRechargeAwardModal: () => {},
  isEditRechargeAwardModalOpen: false,
  rechargeAwardDetails: undefined,
  rechargeAwardIndex: false,
  isDeleteRechargeAwardError: false,
  isDeleteRechargeAwardErrorMessage: undefined,

  isGetAllRechargeAwardsFetching: false,
  isGetAllRechargeAwardsSuccess: false,
  isGetAllRechargeAwardsError: false,
  getAllRechargeAwardsErrorMessage: undefined,
  applyNewFilter: false,
};

export default RechargeAwards;
