import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import { REQUESTS_STATUSES, FIELDS } from 'utils/constants';

const StatusRow = props => {
  const { status, field } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          الحالة
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {field !== FIELDS.reconditioning.en
            ? REQUESTS_STATUSES[status].ar
            : status === REQUESTS_STATUSES.pending.en.toLowerCase()
              ? REQUESTS_STATUSES.contactYou.ar
              : REQUESTS_STATUSES[status].ar}
        </Text>
      </Flex>
    </EvenRow>
  );
};
StatusRow.displayName = 'StatusRow';

StatusRow.propTypes = {
  status: PropTypes.string.isRequired,
};

export default StatusRow;
