import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const LocationRow = props => {
  const { location, openRequestLocation } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          موقع الطلب
        </Text>
      </Flex>
      <Flex flexDirection="row-reverse" width={0.5}>
        {location && (
          <Button
            color={COLORS.BRANDING_BLUE}
            onClick={() => openRequestLocation(location.latitude, location.longitude)}
          >
            <FontAwesomeIcon icon={faMapMarkedAlt} color={COLORS_VALUES[DISABLED]} size="lg" />
          </Button>
        )}
        {!location && (
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            لا يوجد موقع محدد
          </Text>
        )}
      </Flex>
    </OddRow>
  );
};
LocationRow.displayName = 'LocationRow';

LocationRow.propTypes = {
  location: PropTypes.shape({}),
  openRequestLocation: PropTypes.func.isRequired,
};

LocationRow.defaultProps = {
  location: undefined,
};

export default LocationRow;
