import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import { DISCOUNT_TYPES } from 'utils/constants';

const DiscountRow = props => {
  const { discount, promo } = props;
  const { DISABLED, BRANDING_GREEN } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          قيمة خصم البرموكود او كاش باك
        </Text>
      </Flex>
      <Flex width={0.25}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {discount || 0}
        </Text>
      </Flex>
      <Flex width={0.25} flexWrap="wrap" flexDirection="row-reverse">
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          :تفاصيل البرومو
        </Text>
        {promo && (
          <>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              الكود:
              <Text textAlign="right" width={1} pr={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                {promo.code}
              </Text>
            </Text>

            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              النوع:
              <Text textAlign="right" width={1} pr={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                {DISCOUNT_TYPES.find(promoType => promoType.value === promo.discount.type).name.ar}
              </Text>
            </Text>

            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              الخصم:
              <Text textAlign="right" width={1} pr={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                {`${promo.discount.value}${
                  promo.discount.type === 'percentage' ? '%' : ' ريال سعودي'
                }`}
              </Text>
            </Text>

            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              الحد الأعلي للخصم:
              <Text textAlign="right" width={1} pr={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                {promo.upperLimitForDiscount}
              </Text>
            </Text>

            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              الحد الأدني لأجرة الفني:
              <Text textAlign="right" width={1} pr={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                {promo.minimumWorkerWage}
              </Text>
            </Text>
          </>
        )}
      </Flex>
    </EvenRow>
  );
};
DiscountRow.displayName = 'DiscountRow';

DiscountRow.propTypes = {
  discount: PropTypes.number,
  promo: PropTypes.shape({}),
};

DiscountRow.defaultProps = {
  discount: null,
};

export default DiscountRow;
