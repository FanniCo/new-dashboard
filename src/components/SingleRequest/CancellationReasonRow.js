import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { lowerizeFirstLetter } from 'utils/shared/helpers';
import { CANCELLATION_REASONS } from 'utils/constants';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const CancellationReasonRow = props => {
  const { cancellationReason, cancellationReasonComment } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          سبب الإلغاء
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {cancellationReason
            ? CANCELLATION_REASONS[cancellationReason].ar
            : '-'}
          {cancellationReasonComment ? `- ${cancellationReasonComment}` : ''}
        </Text>
      </Flex>
    </OddRow>
  );
};
CancellationReasonRow.displayName = 'CancellationReasonRow';

CancellationReasonRow.propTypes = {
  cancellationReason: PropTypes.string,
  cancellationReasonComment: PropTypes.string,
};

CancellationReasonRow.defaultProps = {
  cancellationReason: undefined,
  cancellationReasonComment: undefined,
};

export default CancellationReasonRow;
