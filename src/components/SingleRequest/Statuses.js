import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import { REQUESTS_STATUSES } from 'utils/constants';

const Statuses = props => {
  const { statuses } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <>
      <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
        <Flex width={0.3}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            حالات الطلب
          </Text>
        </Flex>
        <Flex width={0.7} flexWrap="wrap" flexDirection="row-reverse">
          {(statuses || []).map(s => {
            const { status, createdBy: user, createdAt } = s;
            return (
              <>
                <Flex width={0.3}>
                  <Text
                    textAlign="right"
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {`${REQUESTS_STATUSES[status].ar}`}
                  </Text>
                </Flex>
                <Flex width={0.4}>
                  {user && user.name && (
                    <Text
                      textAlign="right"
                      width={1}
                      color={COLORS_VALUES[DISABLED]}
                      type={SUBHEADING}
                      fontWeight={NORMAL}
                    >
                      {`${user.name}` || ''}
                    </Text>
                  )}
                </Flex>
                <Flex width={0.3}>
                  {createdAt && (
                    <Text
                      textAlign="right"
                      width={1}
                      color={COLORS_VALUES[DISABLED]}
                      type={SUBHEADING}
                      fontWeight={NORMAL}
                    >
                      {new Date(createdAt).toLocaleDateString('en-US', {
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                        hour: 'numeric',
                        minute: 'numeric',
                        hour12: true,
                      })}
                    </Text>
                  )}
                </Flex>
              </>
            );
          })}
        </Flex>
      </EvenRow>
    </>
  );
};
Statuses.displayName = 'Statuses';

Statuses.propTypes = {
  statuses: PropTypes.array.isRequired,
};

Statuses.defaultProps = {};

export default Statuses;
