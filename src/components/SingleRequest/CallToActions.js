import React from 'react';
import PropTypes from 'prop-types';
import { faBan } from '@fortawesome/free-solid-svg-icons';
import Card from 'components/Card';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FIELDS, REQUESTS_STATUSES, CITIES } from 'utils/constants';

const CallToActions = props => {
  const {
    user: { permissions },
    requestId,
    request,
    request: { status },
    handleActivateCancelledRequest,
    handleToggleConfirmModal,
    handleActivateRequest,
    handleActivatePostponedRequest,
    handleToggleOpenFinishRequestModal,
    handleToggleAdminCancelRequestModal,
    handlePostponeRequest,
    handleToggleOpenChooseWorkerModal,
  } = props;
  const { GREY_LIGHT, LIGHT_RED, BRANDING_GREEN, WARNING } = COLORS;
  const activateCancelledRequestConfirmModalHeader = 'تأكيد تنشيط الطلب';
  const activateRequestConfirmModalHeader = 'تأكيد تنشيط الطلب';
  const activateRequestConfirmModalConfirmText = 'هل أنت متأكد أنك تريد تنشيط الطلب؟';
  const activateCancelledRequestConfirmModalConfirmText =
    'هل أنت متأكد أنك تريد تنشيط الطلب الملغى؟';

  const activatePostponedRequestConfirmModalHeader = 'تأكيد تنشيط الطلب المؤجل';
  const activatePostponedRequestConfirmModalConfirmText = 'هل أنت متأكد أنك تريد تنشيط الطلب المؤجل؟';

  const isRequestCanBeCanceled =
    status === REQUESTS_STATUSES.active.en.toLowerCase() ||
    status === REQUESTS_STATUSES.pending.en.toLowerCase() ||
    status === REQUESTS_STATUSES.postponed.en.toLowerCase();
  const isRequestCanBeReactivate = status === REQUESTS_STATUSES.canceled.en.toLowerCase();
  const activateRequestConfirmModalFunction = () => {
    let workerId = process.env.REACT_APP_WORKER_ID;
    if (request.city === CITIES.Riyadh.en) {
      workerId = process.env.REACT_APP_WORKER_ID_Riyadh;
    }
    handleActivateRequest({ requestId, workerId });
  };

  const activatePostponedRequestConfirmModalFunction = () => {
    handleActivatePostponedRequest(requestId );
  };
  const activateCancelledRequestConfirmModalFunction = () => {
    handleActivateCancelledRequest(requestId, undefined);
  };
  return (
    <Card
      flexWrap="wrap"
      justifyContent="center"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      mb={2}
      px={2}
      py={3}
    >
      {isRequestCanBeReactivate && (
        <Button
          minWidth="100%"
          ml={1}
          mr={1}
          color={LIGHT_RED}
          onClick={() =>
            handleToggleConfirmModal(
              activateCancelledRequestConfirmModalHeader,
              activateCancelledRequestConfirmModalConfirmText,
              activateCancelledRequestConfirmModalFunction,
            )
          }
          icon={faBan}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          تنشيط الطلب الملغى
        </Button>
      )}
      {isRequestCanBeCanceled && permissions && permissions.includes('Cancel Request') && (
        <Button
          minWidth="100%"
          ml={1}
          mr={1}
          color={LIGHT_RED}
          onClick={() => handleToggleAdminCancelRequestModal()}
          icon={faBan}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          إلغاء الطلب
        </Button>
      )}
      {request.field === FIELDS.reconditioning.en &&
        request.status === REQUESTS_STATUSES.pending.en.toLowerCase() && (
        <Button
            mt={2}
          minWidth="100%"
          ml={1}
          mr={1}
          color={BRANDING_GREEN}
            onClick={() =>
            handleToggleConfirmModal(
              activateRequestConfirmModalHeader,
              activateRequestConfirmModalConfirmText,
              activateRequestConfirmModalFunction,
              )
          }
          reverse
          xMargin={3}
          isLoading={false}
          >
            تنشيط الطلب
        </Button>
      )}
      {request.status === REQUESTS_STATUSES.postponed.en.toLowerCase() && (
          <Button
            mt={2}
            minWidth="100%"
            ml={1}
            mr={1}
            color={BRANDING_GREEN}
            onClick={() =>
              handleToggleConfirmModal(
                activatePostponedRequestConfirmModalHeader,
                activatePostponedRequestConfirmModalConfirmText,
                activatePostponedRequestConfirmModalFunction,
              )
            }
            reverse
            xMargin={3}
            isLoading={false}
          >
            تنشيط الطلب
          </Button>
        )}
      {request.field === FIELDS.reconditioning.en &&
        request.status === REQUESTS_STATUSES.active.en.toLowerCase() && (
          <Button
            mt={2}
            minWidth="100%"
            ml={1}
            mr={1}
            color={BRANDING_GREEN}
            onClick={() => handleToggleOpenFinishRequestModal()}
            reverse
            xMargin={3}
            isLoading={false}
          >
            انهاء الطلب
          </Button>
        )}

      {[REQUESTS_STATUSES.active.en.toLowerCase(), REQUESTS_STATUSES.pending.en.toLowerCase()].includes(request.status) && (
          <Button
            mt={2}
            minWidth="100%"
            ml={1}
            mr={1}
            color={BRANDING_GREEN}
            onClick={() => handleToggleOpenChooseWorkerModal()}
            reverse
            xMargin={3}
            isLoading={false}
          >
            اختيار فني
          </Button>
        )}

      {request.status === REQUESTS_STATUSES.active.en.toLowerCase() && (
        // permissions &&
        // permissions.includes('postpone Request') &&
        <Button
          minWidth="100%"
          ml={1}
          mr={1}
          mt={2}
          color={WARNING}
          onClick={() => {
            handleToggleConfirmModal(
              'تأكيد تآجيل الطلب',
              'هل أنت متأكد أنك تريد تآجيل الطلب النشط؟',
              () => {
                handlePostponeRequest(requestId);
              },
            );
          }}
          icon={faBan}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          تآجيل الطلب
        </Button>
      )}
    </Card>
  );
};
CallToActions.displayName = 'CallToActions';

CallToActions.propTypes = {
  user: PropTypes.shape({}),
  requestId: PropTypes.string.isRequired,
  request: PropTypes.shape({}).isRequired,
  handleActivateCancelledRequest: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleActivateRequest: PropTypes.func,
  handleActivatePostponedRequest: PropTypes.func,
  handleToggleOpenFinishRequestModal: PropTypes.func,
  handleToggleOpenChooseWorkerModal: PropTypes.func,
  handleToggleAdminCancelRequestModal: PropTypes.func,
  handlePostponeRequest: PropTypes.func,
};

CallToActions.defaultProps = {
  user: undefined,
  handleActivateCancelledRequest: () => {},
  handleToggleConfirmModal: () => {},
  handleActivateRequest: () => {},
  handleActivatePostponedRequest: () => {},
  handleToggleOpenFinishRequestModal: () => {},
  handleToggleOpenChooseWorkerModal: () => {},
  handleToggleAdminCancelRequestModal: () => {},
  handlePostponeRequest: () => {},
};

export default CallToActions;
