import React from 'react';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';
import PropTypes from 'prop-types';
import LightGallery from 'lightgallery/react';

// import styles
import 'lightgallery/css/lightgallery.css';

const PartsCostInvoiceRow = props => {
  const { partsCostInvoiceUrls = [] } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          فاتورة القطع
        </Text>
      </Flex>
      <Flex width={0.5} flexWrap="wrap" flexDirection="row-reverse">
        <LightGallery plugins={[]}>
          {partsCostInvoiceUrls.map((partsCostInvoiceUrl, index) => (
            <>
              <a
                data-lg-size="240-240"
                className="gallery-item"
                data-src={partsCostInvoiceUrl}
                href={partsCostInvoiceUrl}
              >
                <img
                  style={{ margin: '5px', maxWidth: '240px' }}
                  className="img-responsive"
                  alt={`invoice-${index}`}
                  src={partsCostInvoiceUrl}
                />
              </a>
            </>
          ))}
        </LightGallery>
      </Flex>
    </OddRow>
  );
};
PartsCostInvoiceRow.displayName = 'PartsCostInvoiceRow';

PartsCostInvoiceRow.propTypes = {
  partsCostInvoiceUrls: PropTypes.string.isRequired,
};

export default PartsCostInvoiceRow;
