import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const OperationsFollowUpsRow = props => {
  const { operationsFollowUpsText } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          متابعة العمليات
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {operationsFollowUpsText || '-'}
        </Text>
      </Flex>
    </OddRow>
  );
};
OperationsFollowUpsRow.displayName = 'OperationsFollowUpsRow';

OperationsFollowUpsRow.propTypes = {
  operationsFollowUpsText: PropTypes.string,
};

OperationsFollowUpsRow.defaultProps = {
  operationsFollowUpsText: undefined,
};

export default OperationsFollowUpsRow;
