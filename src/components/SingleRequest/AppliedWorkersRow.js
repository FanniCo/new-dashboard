import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const OddRowClickable = styled(OddRow)`
  cursor: ${props => props.appliedWorkersLength > 0 && 'pointer'};
`;

const AppliedWorkersRow = props => {
  const { appliedWorkers, onClick } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRowClickable
      appliedWorkersLength={appliedWorkers.length}
      onClick={appliedWorkers.length > 0 ? onClick : () => {}}
      flexDirection="row-reverse"
      alignItems="center"
      py={3}
      px={2}
    >
      <Flex width={0.5}>
        <Text
          cursor={appliedWorkers.length > 0 ? 'pointer' : 'initial'}
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          عدد المتقدمين
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          cursor={appliedWorkers.length > 0 ? 'pointer' : 'initial'}
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {appliedWorkers.length}
        </Text>
      </Flex>
    </OddRowClickable>
  );
};
AppliedWorkersRow.displayName = 'WorkerNameRow';

AppliedWorkersRow.propTypes = {
  appliedWorkers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default AppliedWorkersRow;
