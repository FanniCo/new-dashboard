import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';
import ReactTooltip from 'react-tooltip';
import { REQUESTS_STATUSES } from 'utils/constants';

const RemainingDaysForGuarantyRow = props => {
  const { paymentStatuses, status, paymentMethod, warrantyExpireDate } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const calculateDiffInDays = dateOfPayment => {

    if (!dateOfPayment || dateOfPayment === null) {
      return 'لايوجد ضمان';
    }

    const now = moment().utc();
    const exdate = moment(dateOfPayment);
    const differenceInTime = exdate.diff(now, 'days', true);
    const remainingDays = differenceInTime.toFixed(0);
    return remainingDays > 0 ? remainingDays : 'الضمان انتهي';
  };

  const findPaymentDate =
    _.findLast(paymentStatuses, paymentStatus => paymentStatus.status === 'requested') || {};


  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          الايام المتبقية على الضمان
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
            data-tip={(warrantyExpireDate != null) ? new Date(warrantyExpireDate).toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            }) : ''}
          >
            {calculateDiffInDays(warrantyExpireDate)}
          </Text>
        <ReactTooltip place="right" type="info" effect="solid" />
      </Flex>
    </OddRow>
  );
};
RemainingDaysForGuarantyRow.displayName = 'RemainingDaysForGuarantyRow';

RemainingDaysForGuarantyRow.propTypes = {
  paymentStatuses: PropTypes.arrayOf(PropTypes.string),
  status: PropTypes.string,
};

RemainingDaysForGuarantyRow.defaultProps = {
  paymentStatuses: [],
  status: undefined,
};

export default RemainingDaysForGuarantyRow;
