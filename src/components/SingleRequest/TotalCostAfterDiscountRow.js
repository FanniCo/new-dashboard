import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';

const TotalCostAfterDiscountRow = props => {
  const { totalAfterDiscount } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          اجمالي التكلفة بعد الخصم
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {totalAfterDiscount || '-'}
        </Text>
      </Flex>
    </EvenRow>
  );
};
TotalCostAfterDiscountRow.displayName = 'TotalCostAfterDiscountRow';

TotalCostAfterDiscountRow.propTypes = {
  totalAfterDiscount: PropTypes.number,
};

TotalCostAfterDiscountRow.defaultProps = {
  totalAfterDiscount: null,
};

export default TotalCostAfterDiscountRow;
