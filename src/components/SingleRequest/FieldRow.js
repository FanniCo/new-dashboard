import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow, CardLabel } from 'components/shared';
import { getFieldIcon } from 'utils/shared/style';
import { FIELDS } from 'utils/constants';

const FieldRow = props => {
  const { field } = props;
  const { DISABLED, BRANDING_GREEN } = COLORS;
  const { SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const fieldIcon = getFieldIcon(field);

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          التخصص
        </Text>
      </Flex>
      <Flex flexDirection="row-reverse" flexWrap="wrap" width={0.5}>
        <Flex alignItems="center">
          <Box>
            <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
          </Box>
          <CardLabel
            py={1}
            px={2}
            mx={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            {FIELDS[field].ar}
          </CardLabel>
        </Flex>
      </Flex>
    </EvenRow>
  );
};
FieldRow.displayName = 'FieldsRow';

FieldRow.propTypes = {
  field: PropTypes.string.isRequired,
};

export default FieldRow;
