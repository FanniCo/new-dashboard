import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import { PAYMENT_METHOD, REQUESTS_STATUSES } from 'utils/constants';

const FinalPaymentMethodRow = props => {
  const { paymentMethodHistory, status } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          تم الدفع بواسطة
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {[
            REQUESTS_STATUSES.done.en.toLowerCase(),
            REQUESTS_STATUSES.reviewed.en.toLowerCase(),
          ].includes(status) && paymentMethodHistory.length
            ? PAYMENT_METHOD[_.last(paymentMethodHistory)].ar
            : '-'}
        </Text>
      </Flex>
    </EvenRow>
  );
};
FinalPaymentMethodRow.displayName = 'FinalPaymentMethodRow';

FinalPaymentMethodRow.propTypes = {
  paymentMethodHistory: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
};

export default FinalPaymentMethodRow;
