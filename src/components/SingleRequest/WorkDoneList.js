import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';
import { WORK_LIST_FOR_EVERY_SERVICE } from 'utils/constants';

const WorkDoneList = props => {
  const { field, workDoneList, label, style } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const decode = str => {
    if (!str) {
      return '';
    }
    return str.replace(/&#(\d+);/g, (match, dec) => String.fromCharCode(dec));
  };

  return (
    <>
      <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2} style={{ style }}>
        <Flex width={0.5}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {label || ' قائمة الاعمال'}
          </Text>
        </Flex>
        <Flex width={0.5} flexWrap="wrap" flexDirection="row-reverse">
          {(workDoneList || []).map(item => {
            const { itemCode, quantity, text, maintenanceCode, amount } = item;
            const maintenance =
              WORK_LIST_FOR_EVERY_SERVICE.ar[field].find(
                e => e.code === (itemCode || maintenanceCode),
              ) || {};
            return (
              <>
                <Flex width={0.4}>
                  <Text
                    textAlign="right"
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {maintenance.name || '-'}
                  </Text>
                </Flex>
                <Flex width={0.4}>
                  <Text
                    textAlign="right"
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {decode(text)}
                  </Text>
                </Flex>
                <Flex width={0.2}>
                  <Text
                    textAlign="right"
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {quantity || amount}
                  </Text>
                </Flex>
              </>
            );
          })}
        </Flex>
      </OddRow>
    </>
  );
};
WorkDoneList.displayName = 'WorkDoneList';

WorkDoneList.propTypes = {
  field: PropTypes.string.isRequired,
  workDoneList: PropTypes.array.isRequired,
};

WorkDoneList.defaultProps = {};

export default WorkDoneList;
