import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';
import { PAYMENT_METHOD } from 'utils/constants';
import _ from 'lodash';

const PaymentMethodRow = props => {
  const { paymentMethodHistory } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          سيتم الدفع بواسطة
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {paymentMethodHistory && paymentMethodHistory.length ? PAYMENT_METHOD[_.first(paymentMethodHistory)].ar : '-'}
        </Text>
      </Flex>
    </OddRow>
  );
};
PaymentMethodRow.displayName = 'PaymentMethodRow';

PaymentMethodRow.propTypes = {
  paymentMethodHistory: PropTypes.string.isRequired,
};

export default PaymentMethodRow;
