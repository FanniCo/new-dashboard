import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { TICKETS_STATUS, REQUESTS_STATUSES, FIELDS } from 'utils/constants';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import Text from 'components/Text';
import Button from 'components/Buttons';
import ConfirmModal from 'components/ConfirmModal';
import OpenTicketModal from 'components/OpenTicketModal';
import EditInvoiceModal from 'components/EditInvoiceModal';
import AppliedWorkersModal from 'components/AppliedWorkersModal';
import OpenFinishRequestModal from 'components/OpenFinishRequestModal';
import {
  SubTitle,
  CardTitle,
  StyledSelect,
  selectColors,
  customSelectStyles,
} from 'components/shared';
import Comments from 'components/Comments';
import Caution from 'components/Caution';
import Statuses from 'components/SingleRequest/Statuses';
import WorkDoneList from 'components/SingleRequest/WorkDoneList';
import PaymentStatusesList from 'components/SingleRequest/PaymentStatusesList';
import AdminCancelRequestModal from 'components/AdminCancelRequestModal';
import CreatedAtDateRow from './CreatedAtDateRow';
import CustomerNameRow from './CustomerNameRow';
import CustomerUserNameRow from './CustomerUserNameRow';
import FieldRow from './FieldRow';
import WorkerNameRow from './WorkerNameRow';
import WorkerUserNameRow from './WorkerUserNameRow';
import AppliedWorkersRow from './AppliedWorkersRow';
import CityRow from './CityRow';
import StatusRow from './StatusRow';
import LocationRow from './LocationRow';
import CancellationReasonRow from './CancellationReasonRow';
import WorkersWageRow from './WorkersWageRow';
import PartsCostRow from './PartsCostRow';
import TotalCostRow from './TotalCostRow';
import CallToActions from './CallToActions';
import RequestMap from './Map';
import ActivityStatuses from './ActivityStatuses';
import CustomerServiceFollowUpsRow from './CustomerServiceFollowUpsRow';
import OperationsFollowUpsRow from './OperationsFollowUpsRow';
import PartsCostInvoiceRow from './PartsCostInvoiceRow';
import TotalCostAfterDiscountRow from './TotalCostAfterDiscountRow';
import DiscountRow from './DiscountRow';
import MoneyTakenFromCustomerWalletRow from './MoneyTakenFromCustomerWalletRow';
import RemainingDaysForGuarantyRow from './RemainingDaysForGuarantyRow';
import FollowUpsComment from './FollowUpsComment';
import RequirementsRow from './RequirementsRow';
import PaymentMethodRow from './PaymentMethodRow';
import FinalPaymentMethodRow from './FinalPaymentMethodRow';
import VatValueRow from './VatValueRow';
import DeliveryCostOfPartsRow from './DeliveryCostOfPartsRow';
import DonationRow from './DonationRow';
import OtherDetailsRow
  from "components/SingleRequest/OtherDetailsRow";
import CustomerUploadedImagesRow
  from "components/SingleRequest/CustomerUploadedImagesRow";
import OpenChooseWorkerModal
  from "components/OpenChooseWorkerModal";
import TimeConsumedNearRequestLocationRow
  from "components/SingleRequest/timeConsumedNearRequestLocationRow";
import AppliedWorkersCard
  from "components/AppliedWorkersCard";
import { getDatabase, onValue, ref } from "firebase/database";
import { initializeApp } from "firebase/app";
import {isEmpty, isEqual} from "lodash";

import ls from 'local-storage';
import { COOKIES_KEYS } from 'utils/constants';
import axios from 'axios';

import "@sendbird/uikit-react/dist/index.css";
import SendbirdProvider from '@sendbird/uikit-react/SendbirdProvider';
import ChannelSettingsUI from '@sendbird/uikit-react/ChannelSettings/components/ChannelSettingsUI';
import { ChannelSettingsProvider } from '@sendbird/uikit-react/ChannelSettings/context';
import { ChannelProvider } from '@sendbird/uikit-react/Channel/context';
import ChannelUI from '@sendbird/uikit-react/Channel/components/ChannelUI';


const { PLAIN_USER } = COOKIES_KEYS;
const plainUser = ls.get(PLAIN_USER);




const { DISABLED, GREY_LIGHT, BRANDING_GREEN, LIGHT_RED, WHITE, ERROR } = COLORS;
const { TITLE, HEADING, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;
const TicketStatusKeys = Object.keys(TICKETS_STATUS);
const ticketStatusSelectOptions = TicketStatusKeys.map(key => ({
  value: key,
  label: TICKETS_STATUS[key].ar,
}));

const PageTitle = styled(Flex)`
  direction: rtl;
`;

const CommentsContainer = styled(Flex)`
  direction: rtl;
  background-color: ${COLORS_VALUES[GREY_LIGHT]};
`;

const SingleRequest = props => {

  const [ActiveChatChannel,setActiveChannel] = useState('');
  const [SBAccessToken,setSBAccessToken] = useState('');
  
  const { requestDetails, isGetRequestDetailsFetching } = props;

  if (isGetRequestDetailsFetching || !requestDetails) {
    return (
      <ShimmerEffect width={1}>
        <Rect width={1 / 3} height={550} m={2} />
        <Rect width={2 / 3} height={550} m={2} />
      </ShimmerEffect>
    );
  }

  const {
    user,
    user: { permissions },
    appliedWorkersDetails: firebaseAppliedWorkersDetails,
    requestDetails: {
      _id,
      requestPrettyId,
      customer,
      customer: { name: customerName, username: customerUserName },
      field,
      worker,
      appliedWorkers,
      appliedWorkersDetails,
      city,
      status,
      createdAt,
      location,
      cancellationReason,
      workersWage,
      partsCost,
      donation,
      total,
      ticket,
      arrivalActivity,
      customerServiceFollowUps,
      operationsFollowUps,
      partsCostInvoiceUrls,
      totalAfterDiscount,
      discount,
      cashBack,
      moneyTakenFromCustomerCredits,
      commentFollowUps,
      cancellationReasonComment,
      paymentStatuses,
      fieldRequirements,
      paymentMethod,
      paymentMethodHistory,
      statuses,
      workDoneList,
      vatValue,
      deliveryCostOfParts,
      maintenanceListHistory,
      promoCode,
      customerDescription,
      images,
      timeConsumedNearRequestLocation,
      warrantyExpireDate,

    },
    openRequestLocation,
    isOpenTicketModalOpen,
    isEditInvoiceModalOpen,
    isEditInvoiceFetching,
    isEditInvoiceError,
    isEditInvoiceErrorMessage,
    isOpenFinishRequestModalOpen,
    isOpenChooseWorkerModalOpen,
    handleToggleOpenTicketModal,
    handleToggleEditInvoiceModal,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    isAppliedWorkersModalOpen,
    handleToggleAppliedWorkersModal,
    handleCancelRequest,
    handleActivateCancelledRequest,
    handleUpdateRequestTicketStatus,
    isCancelRequestFetching,
    isAddRequestTicketFetching,
    isUpdateRequestTicketStatusFetching,
    addedCommentValue,
    handleAddCommentInputChange,
    resetAddCommentInput,
    addComment,
    addCommentState,
    addCommentErrorMessage,
    handleToggleOpenFinishRequestModal,
    handleToggleOpenChooseWorkerModal,
    chooseWorkerAction,
    handleActivateRequest,
    handleActivatePostponedRequest,
    isActivateRequestFetching,
    isUpdateRequestTicketStatusError,
    updateRequestTicketStatusErrorMessage,
    admins,
    mentionData,
    isConfirmModalHasError,
    confirmModalErrorMessage,
    isAdminCancelRequestModalOpen,
    handleToggleAdminCancelRequestModal,
    handlePostponeRequest,
    isPostponeRequestFetching,
    isChooseWorkerFetching,
    isChooseWorkerError,
    chooseWorkerErrorMessage,
    isAcceptWorkerFetching,
    isAcceptWorkerError,
    acceptWorkerErrorMessage,
    acceptWorkerAction,
  } = props;
  const selectedTicketStatus =
    ticket && ticketStatusSelectOptions.find(option => option.value === ticket.latestStatus.code);
  const ticketActionMaker = ticket ? ticket.latestStatus.createdBy : '';
  const ticketActionUpdatedAt = ticket && ticket.updatedAt ? new Date(ticket.updatedAt) : '';
  const requestCreatedAtDate = new Date(createdAt);
  const handleUpdateRequestTicketStatusFunction = value => {
    handleUpdateRequestTicketStatus({ _id: ticket._id, status: value.value });
  };

  axios.get(`https://api-B231229C-D568-49DB-BC5B-03C8FD0605B6.sendbird.com/v3/users/${plainUser._id}`, { headers: { "Api-Token": "c08f8b4855c82942849a97d9b30bef957cd68fe3" } })
  .then(response => {

    if(response.data.access_token) {
      setSBAccessToken(response.data.access_token);
    } else {
      setSBAccessToken('');
    }
  })
  .catch((error) => {

  });


  if(worker) {
    axios.get(`https://api-B231229C-D568-49DB-BC5B-03C8FD0605B6.sendbird.com/v3/group_channels?show_member=true&show_empty=true&members_include_in=${customer._id},${worker._id}`, { headers: { "Api-Token": "c08f8b4855c82942849a97d9b30bef957cd68fe3" } })
 .then(response => {

  if(response.data.channels[0]) {
    setActiveChannel(response.data.channels[0].channel_url);
  } else {
    setActiveChannel(false);
  }
  })
 .catch((error) => {
    setActiveChannel(false);
  });
  }

  return (
    <Flex flexDirection="column" alignItems="flex-end" width={1}>
      <Helmet>
        <title>رقم الطلب : {requestPrettyId.toString()}</title>
      </Helmet>
      <PageTitle mb={5} pt={5} alignItems="center" flexWrap="wrap">
        <Text type={TITLE} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
          رقم الطلب : {requestPrettyId}
        </Text>
        {permissions && permissions.includes('Add New Request Ticket') && !ticket && (
          <Button
            mr={3}
            color={LIGHT_RED}
            onClick={() => handleToggleOpenTicketModal()}
            isLoading={false}
          >
            <Text
              cursor="pointer"
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              فتح تذكرة مشكلة
            </Text>
          </Button>
        )}
        {/* check on status to be done or reviewed */}
        {permissions &&
          permissions.includes('Update Request Invoice') &&
          (status === REQUESTS_STATUSES.done.en.toLowerCase() ||
            status === REQUESTS_STATUSES.reviewed.en.toLowerCase()) &&
          paymentMethod === 'cash' && (
          <Button
            mr={3}
            color={GREY_LIGHT}
            onClick={() => handleToggleEditInvoiceModal()}
            isLoading={false}
          >
            <Text
              cursor="pointer"
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
                تعديل الفاتورة
            </Text>
          </Button>
        )}
        {ticket && (
          <Flex alignItems="center" mr={5}>
            <Text type={HEADING} color={COLORS_VALUES[DISABLED]}>
              مشكلة :
            </Text>
            <StyledSelect
              width="180px"
              mx={2}
              placeholder="حالة المشكلة"
              noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
              isRtl
              backspaceRemovesValue={false}
              value={selectedTicketStatus}
              onChange={handleUpdateRequestTicketStatusFunction}
              options={ticketStatusSelectOptions}
              isDisabled={
                isUpdateRequestTicketStatusFetching ||
                !(permissions && permissions.includes('Update Request Ticket'))
              }
              isLoading={isUpdateRequestTicketStatusFetching}
              theme={selectColors}
              styles={customSelectStyles}
            />
            {ticket && (
              <>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  - بواسطة :
                </SubTitle>
                <Text mx={1} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                  {`${ticketActionMaker}`}
                </Text>
                <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                  {`${ticketActionUpdatedAt &&
                    ` [ ${ticketActionUpdatedAt &&
                      ticketActionUpdatedAt.toLocaleDateString('en-US', {
                        year: 'numeric',
                        month: 'numeric',
                        day: 'numeric',
                      })} ] `}`}
                </Text>
              </>
            )}
          </Flex>
        )}
        {isUpdateRequestTicketStatusError && (
          <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
            {updateRequestTicketStatusErrorMessage}
          </Caution>
        )}
      </PageTitle>
      <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
        <Box width={[1, 1, 1, 1, 2 / 3]}>
          <Card
            py={1}
            ml={[0, 0, 0, 0, 3]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_LIGHT]}
          >
            <CardTitle px={2} pt={2} pb={3}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={TITLE}
                fontWeight={NORMAL}
              >
                تفاصيل الطلب
              </Text>
            </CardTitle>
            <OtherDetailsRow customerDescription={customerDescription} />
            <CustomerUploadedImagesRow images={images} />
            <CreatedAtDateRow requestCreatedAtDate={requestCreatedAtDate} />
            <CustomerNameRow name={customerName} />
            <CustomerUserNameRow username={customerUserName} />
            <FieldRow field={field} />
            {fieldRequirements && <RequirementsRow fieldRequirements={fieldRequirements} />}
            <WorkerNameRow name={worker ? worker.name : undefined} />
            <WorkerUserNameRow username={worker ? worker.username : undefined} />
            <AppliedWorkersRow
              appliedWorkers={appliedWorkers}
              onClick={handleToggleAppliedWorkersModal}
            />
            <CityRow city={city} />
            <LocationRow location={location} openRequestLocation={openRequestLocation} />
            <StatusRow status={status} field={field} />
            <TimeConsumedNearRequestLocationRow timeConsumedNearRequestLocationRow={timeConsumedNearRequestLocation} status={status} />
            <CancellationReasonRow
              cancellationReason={cancellationReason}
              cancellationReasonComment={cancellationReasonComment}
            />
            <WorkersWageRow workersWage={workersWage} />
            <PartsCostRow partsCost={partsCost} />
            <VatValueRow vatValue={vatValue} />
            <DeliveryCostOfPartsRow deliveryCostOfParts={deliveryCostOfParts} />
            <DonationRow donation={donation} />
            <TotalCostRow total={total} />
            <DiscountRow discount={discount || cashBack} promo={promoCode} />
            <MoneyTakenFromCustomerWalletRow
              moneyTakenFromCustomerWallet={moneyTakenFromCustomerCredits}
            />
            <TotalCostAfterDiscountRow totalAfterDiscount={totalAfterDiscount} />
            <PaymentMethodRow paymentMethodHistory={paymentMethodHistory} />
            <FinalPaymentMethodRow paymentMethodHistory={paymentMethodHistory} status={status} />
            <RemainingDaysForGuarantyRow paymentStatuses={paymentStatuses} status={status} paymentMethod={paymentMethod} warrantyExpireDate={warrantyExpireDate} />
            <CustomerServiceFollowUpsRow customerServiceFollowUpsText={customerServiceFollowUps} />
            <OperationsFollowUpsRow operationsFollowUpsText={operationsFollowUps} />
            <FollowUpsComment commnet={commentFollowUps} />
            <PartsCostInvoiceRow partsCostInvoiceUrls={partsCostInvoiceUrls} />
            <Statuses statuses={statuses} />
            <WorkDoneList
              field={field}
              workDoneList={maintenanceListHistory || []}
              label="قائمة الاعمال بواسطة العميل"
            />
            <WorkDoneList field={field} workDoneList={workDoneList} />
            <PaymentStatusesList paymentStatuses={paymentStatuses} />
          </Card>
        </Box>
        <Box width={[1, 1, 1, 1, 1 / 3]}>
          <CallToActions
            user={user}
            requestId={_id}
            request={requestDetails}
            handleToggleConfirmModal={handleToggleConfirmModal}
            handleCancelRequest={handleCancelRequest}
            handleActivateCancelledRequest={handleActivateCancelledRequest}
            handleActivateRequest={handleActivateRequest}
            handleActivatePostponedRequest={handleActivatePostponedRequest}
            handleToggleOpenFinishRequestModal={handleToggleOpenFinishRequestModal}
            handleToggleAdminCancelRequestModal={handleToggleAdminCancelRequestModal}
            handlePostponeRequest={handlePostponeRequest}
            handleToggleOpenChooseWorkerModal={handleToggleOpenChooseWorkerModal}
          />
          <AppliedWorkersCard
            firebaseAppliedWorkersDetails={firebaseAppliedWorkersDetails}
            appliedWorkersDetails={appliedWorkersDetails}
            requestId={_id}
            handleToggleConfirmModal={handleToggleConfirmModal}
            acceptWorkerAction={acceptWorkerAction}
            requestStatus={status}
          />
          {arrivalActivity.length > 0 && <ActivityStatuses request={requestDetails} />}
          { ActiveChatChannel && <><div className=''>
          <SendbirdProvider 
      appId={'B231229C-D568-49DB-BC5B-03C8FD0605B6'}
      userId={plainUser._id}
      theme='dark'
      accessToken={SBAccessToken}
    >
      <div className='SingleSBChat'>
      <ChannelProvider channelUrl={ActiveChatChannel}>
          <ChannelUI />
      </ChannelProvider>
      </div>
      <div className='singleSBSettings'>
      <ChannelSettingsProvider channelUrl={ActiveChatChannel}>
          <ChannelSettingsUI />
      </ChannelSettingsProvider>
      </div>
    </SendbirdProvider>
          </div>
          </>}
          <CommentsContainer width={1} mt={3} mb={2}>
            <Comments
              isModal={false}
              objectType="request"
              object={requestDetails}
              addedCommentValue={addedCommentValue}
              handleAddCommentInputChange={handleAddCommentInputChange}
              addComment={addComment}
              addCommentState={addCommentState}
              addCommentErrorMessage={addCommentErrorMessage}
              resetAddCommentInput={resetAddCommentInput}
              hideAddCommentSection={
                !(permissions && permissions.includes('Add New Request Comments'))
              }
              admins={admins}
              mentionData={mentionData}
            />
          </CommentsContainer>
          <RequestMap location={location} />
        </Box>
      </Flex>
      <ConfirmModal
        isOpened={isConfirmModalOpen}
        header={confirmModalHeader}
        confirmText={confirmModalText}
        toggleFunction={handleToggleConfirmModal}
        confirmFunction={confirmModalFunction}
        isConfirming={
          isCancelRequestFetching ||
          isAddRequestTicketFetching ||
          isActivateRequestFetching ||
          isPostponeRequestFetching ||
          isAcceptWorkerFetching
        }
        isFail={isConfirmModalHasError || isChooseWorkerError || isAcceptWorkerError}
        errorMessage={confirmModalErrorMessage || chooseWorkerErrorMessage || acceptWorkerErrorMessage}
      />
      {isAppliedWorkersModalOpen && (
        <AppliedWorkersModal
          appliedWorkers={appliedWorkers}
          isOpened={isAppliedWorkersModalOpen}
          toggleFunction={handleToggleAppliedWorkersModal}
        />
      )}
      {isOpenTicketModalOpen && (
        <OpenTicketModal
          request={requestDetails}
          worker={worker}
          customer={customer}
          isOpened={isOpenTicketModalOpen}
          toggleFunction={handleToggleOpenTicketModal}
        />
      )}
      {isEditInvoiceModalOpen && (
        <EditInvoiceModal
          request={requestDetails}
          worker={worker}
          customer={customer}
          isOpened={isEditInvoiceModalOpen}
          toggleFunction={handleToggleEditInvoiceModal}
          isEditInvoiceFetching={isEditInvoiceFetching}
          isEditInvoiceError={isEditInvoiceError}
          isEditInvoiceErrorMessage={isEditInvoiceErrorMessage}
        />
      )}
      {status === REQUESTS_STATUSES.active.en.toLowerCase() &&
        ((paymentStatuses[paymentStatuses.length - 1] &&
          paymentStatuses[paymentStatuses.length - 1].status === 'requested') ||
          field === FIELDS.reconditioning.en) && (
          <OpenFinishRequestModal
            request={requestDetails}
            isOpened={isOpenFinishRequestModalOpen}
            toggleFunction={handleToggleOpenFinishRequestModal}
          />
      )}

      {[REQUESTS_STATUSES.postponed.en.toLowerCase(), REQUESTS_STATUSES.active.en.toLowerCase(), REQUESTS_STATUSES.pending.en.toLowerCase()].includes(status) && (
          <OpenChooseWorkerModal
            appliedWorkers={appliedWorkers}
            isOpened={isOpenChooseWorkerModalOpen}
            toggleFunction={handleToggleOpenChooseWorkerModal}
            chooseWorkerAction={chooseWorkerAction}
            requestId={requestDetails._id}
            isChooseWorkerFetching={isChooseWorkerFetching}
            isChooseWorkerHasError={isChooseWorkerError}
            chooseWorkerErrorMessage={chooseWorkerErrorMessage}
          />
      )}

      {isAdminCancelRequestModalOpen && (
        <AdminCancelRequestModal
          requestId={_id}
          isAdminCancelRequestModalOpen={isAdminCancelRequestModalOpen}
          handleToggleAdminCancelRequestModal={handleToggleAdminCancelRequestModal}
        />
      )}
    </Flex>
  );
};
SingleRequest.displayName = 'SingleRequest';

SingleRequest.propTypes = {
  user: PropTypes.shape({}),
  requestDetails: PropTypes.shape({}),
  isGetRequestDetailsFetching: PropTypes.bool,
  openRequestLocation: PropTypes.func,
  isOpenTicketModalOpen: PropTypes.bool,
  isEditInvoiceModalOpen: PropTypes.bool,
  isOpenFinishRequestModalOpen: PropTypes.bool,
  isOpenChooseWorkerModalOpen: PropTypes.bool,
  handleToggleOpenTicketModal: PropTypes.func,
  handleToggleEditInvoiceModal: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  isAppliedWorkersModalOpen: PropTypes.bool,
  handleToggleAppliedWorkersModal: PropTypes.func,
  handleCancelRequest: PropTypes.func,
  handleActivateCancelledRequest: PropTypes.func,
  handleActivateRequest: PropTypes.func,
  handleActivatePostponedRequest: PropTypes.func,
  handleUpdateRequestTicketStatus: PropTypes.func,
  isCancelRequestFetching: PropTypes.bool,
  isAddRequestTicketFetching: PropTypes.bool,
  isUpdateRequestTicketStatusFetching: PropTypes.bool,
  addCommentState: PropTypes.string,
  addCommentErrorMessage: PropTypes.string,
  addComment: PropTypes.func,
  addedCommentValue: PropTypes.string,
  handleAddCommentInputChange: PropTypes.func,
  resetAddCommentInput: PropTypes.func,
  handleToggleOpenFinishRequestModal: PropTypes.func,
  handleToggleOpenChooseWorkerModal: PropTypes.func,
  chooseWorkerAction: PropTypes.func,
  isActivateRequestFetching: PropTypes.bool,
  isUpdateRequestTicketStatusError: PropTypes.bool,
  updateRequestTicketStatusErrorMessage: PropTypes.string,
  admins: PropTypes.arrayOf(PropTypes.shape({})),
  mentionData: PropTypes.arrayOf(PropTypes.shape({})),
  isConfirmModalHasError: PropTypes.bool,
  confirmModalErrorMessage: PropTypes.string,
  isEditInvoiceFetching: PropTypes.bool,
  isEditInvoiceError: PropTypes.bool,
  isEditInvoiceErrorMessage: PropTypes.string,
  isAdminCancelRequestModalOpen: PropTypes.bool,
  handleToggleAdminCancelRequestModal: PropTypes.func,
  handlePostponeRequest: PropTypes.func,
  isPostponeRequestFetching: PropTypes.bool,
};

SingleRequest.defaultProps = {
  user: undefined,
  requestDetails: undefined,
  isGetRequestDetailsFetching: false,
  isOpenTicketModalOpen: false,
  isEditInvoiceModalOpen: false,
  isOpenFinishRequestModalOpen: false,
  isOpenChooseWorkerModalOpen: false,
  handleToggleOpenTicketModal: () => {},
  handleToggleEditInvoiceModal: () => {},
  isConfirmModalOpen: false,
  openRequestLocation: () => {},
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleAppliedWorkersModal: () => {},
  isAppliedWorkersModalOpen: false,
  handleCancelRequest: () => {},
  handleActivateCancelledRequest: () => {},
  handleActivateRequest: () => {},
  handleActivatePostponedRequest: () => {},
  handleUpdateRequestTicketStatus: () => {},
  isCancelRequestFetching: false,
  isAddRequestTicketFetching: false,
  isUpdateRequestTicketStatusFetching: false,
  addCommentState: undefined,
  addCommentErrorMessage: undefined,
  addComment: () => {},
  addedCommentValue: '',
  handleAddCommentInputChange: () => {},
  resetAddCommentInput: () => {},
  handleToggleOpenFinishRequestModal: () => {},
  handleToggleOpenChooseWorkerModal: () => {},
  chooseWorkerAction: () => {},
  isActivateRequestFetching: false,
  isUpdateRequestTicketStatusError: false,
  updateRequestTicketStatusErrorMessage: undefined,
  admins: [],
  mentionData: [],
  isConfirmModalHasError: false,
  confirmModalErrorMessage: undefined,
  isEditInvoiceFetching: false,
  isEditInvoiceError: false,
  isEditInvoiceErrorMessage: undefined,
  isAdminCancelRequestModalOpen: false,
  handleToggleAdminCancelRequestModal: () => {},
  isPostponeRequestFetching: false,
  handlePostponeRequest: () => {},
};

export default SingleRequest;
