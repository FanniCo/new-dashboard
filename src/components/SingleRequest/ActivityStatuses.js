import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShippingFast, faClipboardCheck } from '@fortawesome/free-solid-svg-icons';
import Card from 'components/Card';
import Text from 'components/Text';
import { SubTitle } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const { DISABLED, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const TRACKING_STEPS = [
  {
    key: 'StartMoving',
    ar: 'في الطريق',
    en: 'Start moving',
    icon: faShippingFast,
  },
  {
    key: 'Arrived',
    ar: 'وصل',
    en: 'Arrived',
    icon: faClipboardCheck,
  },
];

const ActivityStatuses = props => {
  const {
    request: { arrivalActivity, arrivalEstimationTime, timeTakenToFinishWork, delayDuration },
  } = props;
  const statusKey =
    arrivalActivity.length > 0 && arrivalActivity[arrivalActivity.length - 1].status;

  const getStepInfo = step => {
    const stepKey = step.key;
    const stepIcon = step.icon;
    let stepColor;
    let roadColor;

    if (statusKey && statusKey === stepKey) {
      stepColor = BRANDING_GREEN;
      roadColor = DISABLED;
    } else {
      stepColor = DISABLED;
      roadColor = DISABLED;
    }

    return {
      stepIcon,
      stepColor,
      roadColor,
    };
  };

  return (
    <Card
      flexWrap="wrap"
      justifyContent="flex-end"
      alignItems="center"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      mb={2}
      px={1}
      py={3}
    >
      <Flex
        width={1}
        mb={3}
        py={2}
        flexDirection="row-reverse"
        alignItems="center"
        justifyContent="center"
      >
        {TRACKING_STEPS.map((step, index) => {
          const stepInfo = getStepInfo(step, index);
          const { stepIcon, stepColor, roadColor } = stepInfo;
          const stepUpdatedAt =
            arrivalActivity[index] &&
            new Date(arrivalActivity[index].updatedAt).toLocaleTimeString('en-US', {
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            });
          const stepUpdatedAtLocaleString = stepUpdatedAt ? `( ${stepUpdatedAt} )` : '';

          return (
            <React.Fragment key={step.key}>
              <Flex flexDirection="column" alignItems="center" justifyContent="center">
                <FontAwesomeIcon icon={stepIcon} size="lg" color={COLORS_VALUES[stepColor]} />
                <Text
                  textAlign="center"
                  mt={2}
                  color={COLORS_VALUES[stepColor]}
                  type={BODY}
                  fontWeight={NORMAL}
                >
                  {step.ar}
                </Text>
                <Text
                  textAlign="center"
                  mt={2}
                  color={COLORS_VALUES[stepColor]}
                  type={BODY}
                  fontWeight={NORMAL}
                >
                  {stepUpdatedAtLocaleString}
                </Text>
              </Flex>
              {index !== TRACKING_STEPS.length - 1 && (
                <Flex
                  width={0.4}
                  justifyContent="center"
                  alignItems="center"
                  flexDirection="column"
                  mb={2}
                >
                  <hr width="90%" style={{ border: `1px solid ${COLORS_VALUES[roadColor]}` }} />
                </Flex>
              )}
            </React.Fragment>
          );
        })}
      </Flex>
      {arrivalEstimationTime && (
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            وقت تقدير الوصول :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`${arrivalEstimationTime.unit} / ${arrivalEstimationTime.time}`}
          </Text>
        </Flex>
      )}
      {timeTakenToFinishWork && (
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد ساعات العمل :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`${timeTakenToFinishWork.unit} / ${timeTakenToFinishWork.time}`}
          </Text>
        </Flex>
      )}
      {delayDuration && (
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تآخير الفني علي العميل :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`${delayDuration.unit} / ${delayDuration.time}`}
          </Text>
        </Flex>
      )}
    </Card>
  );
};
ActivityStatuses.displayName = 'ActivityStatuses';

ActivityStatuses.propTypes = {
  request: PropTypes.shape({}).isRequired,
};

export default ActivityStatuses;
