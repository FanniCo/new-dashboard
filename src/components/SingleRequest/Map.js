import React from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { DARK_MODE_MAP_STYLE } from 'utils/constants';

const GoogleMapWrapper = compose(
  withScriptjs,
  withGoogleMap,
)(props => (
  <GoogleMap
    defaultZoom={11}
    defaultCenter={props.defaultCenter}
    defaultOptions={{ styles: DARK_MODE_MAP_STYLE, disableDefaultUI: true }}
  >
    {props.location && (
      <Marker
        position={{ lat: Number(props.location.latitude), lng: Number(props.location.longitude) }}
      />
    )}
  </GoogleMap>
));

const RequestMap = props => {
  const { location } = props;
  const { GREY_LIGHT } = COLORS;
  const defaultMapCenter = {
    lat: Number(location.latitude),
    lng: Number(location.longitude),
  };

  return (
    <Card minHeight={260} justifyContent="center" bgColor={COLORS_VALUES[GREY_LIGHT]}>
      <GoogleMapWrapper
        location={location}
        defaultCenter={defaultMapCenter}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '260px', width: '100%' }} />}
        mapElement={<div style={{ height: '100%' }} />}
      />
    </Card>
  );
};
RequestMap.displayName = 'RequestMap';

RequestMap.propTypes = {
  location: PropTypes.shape({}),
};

RequestMap.defaultProps = {
  location: undefined,
};

export default RequestMap;
