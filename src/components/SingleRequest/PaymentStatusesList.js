import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import { PAYMENT_STATUSES } from 'utils/constants';

const PaymentStatusesList = props => {
  const { paymentStatuses } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <>
      <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
        <Flex width={0.5}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            قائمة حالات الدفع
          </Text>
        </Flex>
        <Flex width={0.5} flexWrap="wrap" flexDirection="row-reverse">
          {paymentStatuses.map(item => {
            const { status, invoice } = item;
            return (
              <>
                <Flex width={0.4}>
                  <Text
                    textAlign="right"
                    width={1}
                    color={COLORS_VALUES[DISABLED]}
                    type={SUBHEADING}
                    fontWeight={NORMAL}
                  >
                    {PAYMENT_STATUSES[status] ? PAYMENT_STATUSES[status].ar : ''}
                    {/* {status} */}
                  </Text>
                </Flex>
                <Flex width={0.6}>
                  {invoice && (
                    <>
                      <Text
                        textAlign="right"
                        width={1}
                        color={COLORS_VALUES[DISABLED]}
                        type={SUBHEADING}
                        fontWeight={NORMAL}
                      >
                        اجرة الفنى: {invoice.workerWage} ريال
                      </Text>
                      <Text
                        textAlign="right"
                        width={1}
                        color={COLORS_VALUES[DISABLED]}
                        type={SUBHEADING}
                        fontWeight={NORMAL}
                      >
                        ثمن القطع: {invoice.partsCost} ريال
                      </Text>
                    </>
                  )}
                </Flex>
              </>
            );
          })}
        </Flex>
      </EvenRow>
    </>
  );
};
PaymentStatusesList.displayName = 'PaymentStatusesList';

PaymentStatusesList.propTypes = {
  paymentStatuses: PropTypes.array.isRequired,
};

PaymentStatusesList.defaultProps = {};

export default PaymentStatusesList;
