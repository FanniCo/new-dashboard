import React from 'react';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import PropTypes from 'prop-types';
import LightGallery from 'lightgallery/react';

// import styles
import 'lightgallery/css/lightgallery.css';

const CustomerUploadedImagesRow = props => {
  const { images = [] } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          صور مضافة من العميل
        </Text>
      </Flex>
      <Flex width={0.5} flexWrap="wrap" flexDirection="row-reverse">
        <LightGallery plugins={[]}>
          {images.map((image, index) => (
            <>
              <a
                data-lg-size="240-240"
                className="gallery-item"
                data-src={image}
                href={image}
              >
                <img
                  style={{ margin: '5px', maxWidth: '240px' }}
                  className="img-responsive"
                  alt={`invoice-${index}`}
                  src={image}
                />
              </a>
            </>
          ))}
        </LightGallery>
      </Flex>
    </EvenRow>
  );
};
CustomerUploadedImagesRow.displayName = 'CustomerUploadedImagesRow';

CustomerUploadedImagesRow.propTypes = {
  images: PropTypes.string.isRequired,
};

export default CustomerUploadedImagesRow;
