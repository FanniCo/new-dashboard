import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { addComment as requestAddComment } from 'redux-modules/requests/actions';
import { addComment as userWorkerAddComment } from 'redux-modules/workers/actions';
import { addComment as userClientAddComment } from 'redux-modules/clients/actions';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import Modal from 'components/Modal';
import Comments from 'components/Comments';

class OpsCommentsModal extends Component {
  static propTypes = {
    admins: PropTypes.arrayOf(PropTypes.shape({})),
    objectType: PropTypes.string,
    object: PropTypes.shape({}),
    objectIndex: PropTypes.number,
    requestAddCommentState: PropTypes.string,
    userWorkerAddcommentState: PropTypes.string,
    userClientAddCommentState: PropTypes.string,
    requestAddComment: PropTypes.func,
    userWorkerAddComment: PropTypes.func,
    userClientAddComment: PropTypes.func,
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
    hideAddCommentSection: PropTypes.bool,
    addRequestsCommentErrorMessage: PropTypes.string,
    addWorkersCommentErrorMessage: PropTypes.string,
    addClientsCommentErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    admins: undefined,
    objectType: undefined,
    object: undefined,
    objectIndex: undefined,
    requestAddCommentState: undefined,
    userWorkerAddcommentState: undefined,
    userClientAddCommentState: undefined,
    requestAddComment: () => {},
    userWorkerAddComment: () => {},
    userClientAddComment: () => {},
    toggleFunction: () => {},
    isOpened: false,
    hideAddCommentSection: false,
    addRequestsCommentErrorMessage: undefined,
    addWorkersCommentErrorMessage: undefined,
    addClientsCommentErrorMessage: undefined,
  };

  state = {
    mentionData: [],
    addedCommentValue: '',
  };

  handleCommentChange = (event, newValue, newPlainTextValue, mentions) => {
    const { mentionData } = this.state;
    this.setState({
      addedCommentValue: newPlainTextValue,
      mentionData: mentionData.concat(mentions),
    });
  };

  handleAddCommentInputChange = val => {
    this.setState({ addedCommentValue: val });
  };

  resetAddCommentInput = () => {
    this.setState({ addedCommentValue: '', mentionData: [] });
  };

  render() {
    const {
      admins,
      isOpened,
      toggleFunction,
      objectType,
      object,
      objectIndex,
      requestAddComment: requestAddCommentAction,
      userWorkerAddComment: userWorkerAddCommentAction,
      userClientAddComment: userClientAddCommentAction,
      requestAddCommentState,
      userWorkerAddcommentState,
      userClientAddCommentState,
      hideAddCommentSection,
      addRequestsCommentErrorMessage,
      addWorkersCommentErrorMessage,
      addClientsCommentErrorMessage,
    } = this.props;
    const { addedCommentValue, mentionData } = this.state;
    const OpsCommentsModalHeader = 'تعليقات العمليات';
    let modalContent;

    if (objectIndex || objectIndex === 0) {
      let addCommentFunction;
      let addCommentStateType;

      if (objectType === 'request') {
        addCommentFunction = requestAddCommentAction;
        addCommentStateType = requestAddCommentState;
      } else if (objectType === 'worker') {
        addCommentFunction = userWorkerAddCommentAction;
        addCommentStateType = userWorkerAddcommentState;
      } else if (objectType === 'client') {
        addCommentFunction = userClientAddCommentAction;
        addCommentStateType = userClientAddCommentState;
      }

      modalContent = (
        <Comments
          isModal
          objectType={objectType}
          object={object}
          addedCommentValue={addedCommentValue}
          mentionData={mentionData}
          handleAddCommentInputChange={this.handleCommentChange}
          addComment={addCommentFunction}
          addCommentState={addCommentStateType}
          addCommentErrorMessage={
            addRequestsCommentErrorMessage ||
            addWorkersCommentErrorMessage ||
            addClientsCommentErrorMessage
          }
          resetAddCommentInput={this.resetAddCommentInput}
          hideAddCommentSection={hideAddCommentSection}
          admins={admins}
        />
      );
    } else {
      modalContent = (
        <ShimmerEffect width={1}>
          <Rect width={1} height={250} m={2} />
        </ShimmerEffect>
      );
    }

    return (
      <Modal
        toggleModal={() => toggleFunction(undefined)}
        header={OpsCommentsModalHeader}
        isOpened={isOpened}
      >
        {modalContent}
      </Modal>
    );
  }
}
OpsCommentsModal.displayName = 'OpsCommentsModal';

const mapStateToProps = state => ({
  admins: state.user.admins,
  requestAddCommentState: state.requests.addComment.addCommentState,
  userWorkerAddcommentState: state.workers.addComment.addCommentState,
  userClientAddCommentState: state.clients.addComment.addCommentState,
  addRequestsCommentErrorMessage: state.requests.addComment.error,
  addWorkersCommentErrorMessage: state.workers.addComment.error,
  addClientsCommentErrorMessage: state.clients.addComment.error,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAddComment,
      userWorkerAddComment,
      userClientAddComment,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(OpsCommentsModal);
