import React, { Component, useRef, useMemo } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  toggleAddNewPostModal,
  addNewPost,
  updatePost,
  openEditPostModal,
} from 'redux-modules/posts/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { Checkbox, Input, Label } from '@rebass/forms';
import JoditEditor from 'jodit-react';
import { Urls } from 'utils/api';
import Cookies from 'js-cookie';
import { COOKIES_KEYS } from 'utils/constants';
import { HyperLink } from 'components/shared';
import _ from 'lodash';

const TextEditor = props => {
  const editor2 = useRef(null);
  const { posts } = Urls;
  const { TOKEN } = COOKIES_KEYS;
  const config = {
    readonly: false,
    theme: 'summer',
    uploader: {
      headers: {
        token: Cookies.get(TOKEN),
      },
      insertImageAsBase64URI: false,
      url: posts.upload,
      isSuccess(resp) {
        return resp;
      },
      process(resp) {
        return {
          files: resp.data.files,
          path: resp.data.path,
          baseurl: resp.data.baseurl,
          error: resp.data.error,
          message: resp.data.message,
        };
      },
      defaultHandlerSuccess(data) {
        let i;
        const field = 'files';
        if (data[field] && data[field].length) {
          for (i = 0; i < data[field].length; i += 1) {
            this.selection.insertImage(data.baseurl + data[field][i]);
          }
        }
      },
      filesVariableName() {
        return 'file';
      },
    },
  };

  return useMemo(
    () => (
      <JoditEditor
        value={props.value}
        tabIndex="1"
        ref={editor2}
        config={config}
        onChange={props.onChange}
      />
    ),
    [],
  );
};

const imageStyle = {
  width: '200px',
  height: '150px',
  padding: '5px',
  display: 'inherit',
};

const AddNewPostFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddNewPostModal extends Component {
  static propTypes = {
    isAddPostFetching: PropTypes.bool,
    isAddPostError: PropTypes.bool,
    isEditPostError: PropTypes.bool,
    addPostErrorMessage: PropTypes.string,
    editPostErrorMessage: PropTypes.string,
    addNewPost: PropTypes.func,
    toggleAddNewPostModal: PropTypes.func,
    openEditPostModal: PropTypes.func,
    updatePost: PropTypes.func,
    isAddPostModalOpen: PropTypes.bool,
    postDetails: PropTypes.shape({}),
    postIndex: PropTypes.number,
    isEditPostFetching: PropTypes.bool,
  };

  static defaultProps = {
    isAddPostFetching: false,
    isAddPostError: false,
    isEditPostError: false,
    addPostErrorMessage: '',
    editPostErrorMessage: '',
    addNewPost: () => {},
    toggleAddNewPostModal: () => {},
    openEditPostModal: () => {},
    updatePost: () => {},
    isAddPostModalOpen: false,
    postDetails: undefined,
    postIndex: undefined,
    isEditPostFetching: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      titleAr: '',
      titleEn: '',
      contentAr: '',
      contentEn: '',
      imageUrlAr: '',
      imageUrlEn: '',
      active: true,
      fullImageUrlAr: null,
      fullImageUrlEn: null,
      imageUrlArKey: Date.now(),
      imageUrlEnKey: Date.now(),
    };
  }

  componentDidMount() {
    const { postDetails } = this.props;

    if (postDetails) {
      this.setState({
        titleAr: postDetails.title ? postDetails.title.ar : '',
        titleEn: postDetails.title ? postDetails.title.en : '',
        contentAr: postDetails.description ? postDetails.description.ar : '',
        contentEn: postDetails.description ? postDetails.description.en : '',
        imageUrlAr: postDetails.imageUrl ? postDetails.imageUrl.ar : '',
        imageUrlEn: postDetails.imageUrl ? postDetails.imageUrl.en : '',
        fullImageUrlAr: postDetails.imageUrl ? postDetails.imageUrl.ar : '',
        fullImageUrlEn: postDetails.imageUrl ? postDetails.imageUrl.en : '',
        active: postDetails.active,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitAddPostForm = e => {
    e.preventDefault();

    const {
      addNewPost: addNewPostAction,
      updatePost: updatePostAction,
      postDetails,
      postIndex,
    } = this.props;
    const { titleAr, titleEn, contentAr, contentEn, imageUrlAr, imageUrlEn, active } = this.state;

    const newPostInfo = new FormData();
    newPostInfo.append('fileAr', imageUrlAr);
    newPostInfo.append('fileEn', imageUrlEn);
    newPostInfo.append('title.ar', titleAr);
    newPostInfo.append('title.en', titleEn);
    newPostInfo.append('description.ar', contentAr);
    newPostInfo.append('description.en', contentEn);
    newPostInfo.append('active', active);

    if (postDetails) {
      const { _id } = postDetails;
      return updatePostAction(newPostInfo, _id, postIndex);
    }

    return addNewPostAction(newPostInfo);
  };

  handleActiveCheckBoxChange = event => {
    this.setState({ active: event.target.checked });
  };

  handleEditorChangeAr = value => {
    this.handleInputFieldChange('contentAr', value);
  };

  handleEditorChangeEn = value => {
    this.handleInputFieldChange('contentEn', value);
  };

  handleFileChange = (e, type) => {
    const file = e.target.files[0];
    this.setState({
      [type]: file,
      [_.camelCase(`full_${type}`)]: URL.createObjectURL(file),
    });
  };

  clearFile = type => {
    this.setState({
      [type]: '',
      [_.camelCase(`full_${type}`)]: null,
      [`${type}Key`]: Date.now(),
    });
  };

  render() {
    const {
      isAddPostFetching,
      isAddPostModalOpen,
      toggleAddNewPostModal: toggleAddNewPostModalAction,
      openEditPostModal: openEditPostModalAction,
      isAddPostError,
      isEditPostError,
      addPostErrorMessage,
      editPostErrorMessage,
      postDetails,
      isEditPostFetching,
    } = this.props;
    const {
      titleAr,
      titleEn,
      contentAr,
      contentEn,
      active,
      fullImageUrlAr,
      fullImageUrlEn,
      imageUrlArKey,
      imageUrlEnKey,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addPostModalHeader = postDetails ? 'تعديل المقال' : 'أضف مقال جديدة';
    const submitButtonText = postDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          postDetails ? openEditPostModalAction() : toggleAddNewPostModalAction()
        }
        header={addPostModalHeader}
        isOpened={isAddPostModalOpen}
      >
        <AddNewPostFormContainer onSubmit={this.onSubmitAddPostForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى العنوان (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان عربى"
                  width={1}
                  value={titleAr}
                  onChange={value => this.handleInputFieldChange('titleAr', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتوى العنوان(بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان انجليزى"
                  width={1}
                  value={titleEn}
                  onChange={value => this.handleInputFieldChange('titleEn', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>

                <TextEditor value={contentAr} onChange={this.handleEditorChangeAr} />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>

                <TextEditor value={contentEn} onChange={this.handleEditorChangeEn} />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                {fullImageUrlAr && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrlAr')} />
                    <HyperLink href={fullImageUrlAr} target="_blank">
                      <img style={imageStyle} src={fullImageUrlAr} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الصورة (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="الصورة (بالعربى)"
                  name="fileAr"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrlAr');
                  }}
                  key={imageUrlArKey}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                {fullImageUrlEn && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrlEn')} />
                    <HyperLink href={fullImageUrlEn} target="_blank">
                      <img style={imageStyle} src={fullImageUrlEn} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الصورة (بالانجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="الصورة (بالنجليزى)"
                  name="fileEn"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrlEn');
                  }}
                  key={imageUrlEnKey}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Box>
                <Label>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    مفعل
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <Checkbox
                    name="remember"
                    checked={active}
                    onChange={this.handleActiveCheckBoxChange}
                    sx={{ color: '#fcfffa' }}
                  />
                </Label>
              </Box>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddPostForm}
                disabled={
                  isAddPostFetching ||
                  titleAr === '' ||
                  titleEn === '' ||
                  contentAr === '' ||
                  contentEn === '' ||
                  _.isEmpty(fullImageUrlAr) ||
                  _.isEmpty(fullImageUrlEn)
                }
                isLoading={isAddPostFetching || isEditPostFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddPostError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addPostErrorMessage}
                </Caution>
              )) ||
                (isEditPostError && (
                  <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                    {editPostErrorMessage}
                  </Caution>
                ))}
            </Flex>
          </Flex>
        </AddNewPostFormContainer>
      </Modal>
    );
  }
}

AddNewPostModal.displayName = 'AddNewPostModal';

const mapStateToProps = state => ({
  isAddPostFetching: state.posts.addPost.isFetching,
  isEditPostFetching: state.posts.updatePost.isFetching,
  isAddPostError: state.posts.addPost.isFail.isError,
  addPostErrorMessage: state.posts.addPost.isFail.message,
  isEditPostError: state.posts.updatePost.isFail.isError,
  editPostErrorMessage: state.posts.updatePost.isFail.message,
  isAddPostModalOpen: state.posts.addNewPostModal.isOpen || state.posts.editPostModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewPostModal,
      addNewPost,
      updatePost,
      openEditPostModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPostModal);
