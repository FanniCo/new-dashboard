import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Modal from 'components/Modal';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import styled from 'styled-components';
import { OddRow, EvenRow } from 'components/shared';
import { CITIES } from 'utils/constants';

const AddNewRequestFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const VendorDetailsModal = props => {
  const { isOpened, toggleFunction, vendorDetails } = props;
  const { DISABLED } = COLORS;
  const { NORMAL } = FONT_WEIGHTS;
  const { SUBHEADING } = FONT_TYPES;
  return (
    <Modal toggleModal={() => toggleFunction({})} header="تفاصيل الشريك" isOpened={isOpened}>
      <AddNewRequestFormContainer>
        <Flex flexDirection="column">
          <EvenRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                رقم الجوال
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.username}
              </Text>
            </Flex>
          </EvenRow>

          <OddRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                اسم الشريك
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.name}
              </Text>
            </Flex>
          </OddRow>
          <EvenRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                نشط
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.active ? 'نشط' : 'غير نشط'}
              </Text>
            </Flex>
          </EvenRow>
          <OddRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                رقم الهوية/السجل
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.governmentId}
              </Text>
            </Flex>
          </OddRow>
          <EvenRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                الاميل
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.email}
              </Text>
            </Flex>
          </EvenRow>
          <OddRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                نسبة الشريك من العمولة
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.commissionPercentage}%
              </Text>
            </Flex>
          </OddRow>
          <EvenRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                تاريخ انتهاء التعاقد
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {new Date(vendorDetails.expiredAt).toLocaleDateString('en-US', {
                  year: 'numeric',
                  month: 'numeric',
                  day: 'numeric',
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: true,
                })}
              </Text>
            </Flex>
          </EvenRow>
          <OddRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                رقم التواصل
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.contactNumber}
              </Text>
            </Flex>
          </OddRow>
          <EvenRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                المدينة
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {vendorDetails.city ? CITIES[vendorDetails.city].ar : 'غير معرف'}
              </Text>
            </Flex>
          </EvenRow>
          <OddRow flexDirection="row" alignItems="center" py={3} px={2}>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                موقوف
              </Text>
            </Flex>
            <Flex width={0.5}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                {!vendorDetails.suspendedTo ||
                Date.parse(new Date(vendorDetails.suspendedTo)) < Date.parse(new Date())
                  ? 'لا'
                  : ` ${new Date(vendorDetails.suspendedTo).toLocaleDateString('en-US', {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: true,
                  })} موقوف حتى`}
              </Text>
            </Flex>
          </OddRow>
        </Flex>
      </AddNewRequestFormContainer>
    </Modal>
  );
};
VendorDetailsModal.displayName = 'VendorDetailsModal';

VendorDetailsModal.propTypes = {
  isOpened: PropTypes.bool,
  toggleFunction: PropTypes.func,
  vendorDetails: PropTypes.shape({}),
};

VendorDetailsModal.defaultProps = {
  toggleFunction: () => {},
  isOpened: false,
  vendorDetails: undefined,
};

export default VendorDetailsModal;
