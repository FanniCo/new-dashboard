import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import Caution from 'components/Caution';
import styled from 'styled-components';
import DatePicker from 'react-datepicker/es';
import { space } from 'styled-system';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

const { BRANDING_ORANGE, GREY_DARK } = COLORS;

const FormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const StyledDatePicker = styled(DatePicker)`
  ${space};
  background-color: ${props => props.backgroundColor};
  display: block;
  border: 1px solid;
  border-color: ${props => (props.isDanger ? COLORS_VALUES[COLORS.LIGHT_RED] : props.borderColor)};
  border-radius: ${props => props.theme.space[1]}px;
  color: ${props => COLORS_VALUES[props.color ? props.color : COLORS.DISABLED]};
  font-weight: ${props => props.theme.fontWeights.NORMAL};
  line-height: normal;
  outline: 0;
  width: calc(100% - 16px);
  font-size: 14px;
  direction: ltr;

  &:lang(ar) {
    padding: ${props => props.theme.space[2]}px;
    text-align: right;
  }

  &[type='number'] {
    -moz-appearance: textfield;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;

const SectionFlexContainer = styled(Flex)`
  .react-datepicker-popper {
    direction: ltr;
    &:lang(ar) {
      text-align: right;
      .react-datepicker__triangle {
        right: 50px;
        left: inherit;
      }
    }
  }
  .react-datepicker__input-container {
    display: block;
  }
  .react-datepicker__day--selected,
  .react-datepicker__day--in-selecting-range,
  .react-datepicker__day--in-range,
  .react-datepicker__month-text--selected,
  .react-datepicker__month-text--in-selecting-range,
  .react-datepicker__month-text--in-range {
    background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
    &:hover {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      opacity: 0.8;
    }
  }
  .react-datepicker__time-container,
  .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box {
    width: 120px;
  }
  .react-datepicker__time-container
    .react-datepicker__time
    .react-datepicker__time-box
    ul.react-datepicker__time-list
    li.react-datepicker__time-list-item {
    display: flex;
    justify-content: center;
    align-items: center;
    &.react-datepicker__time-list-item--selected {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      &:hover {
        background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
        opacity: 0.8;
      }
    }
  }
  .react-datepicker {
    font-family: 'english-font';
    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }
    .react-datepicker-time__header {
      color: ${COLORS_VALUES[GREY_DARK]};
      font-weight: normal;
      font-size: 0.8rem;
    }
  }
  .react-datepicker__close-icon {
    right: 80%;
  }
  .react-datepicker__close-icon::after {
    height: 13px;
    width: 15px;
  }
`;

class SuspendModal extends Component {
  static propTypes = {
    userIndex: PropTypes.number,
    userDetails: PropTypes.shape({}),
    handleToggleSuspendUserModal: PropTypes.func,
    suspendUser: PropTypes.func,
    isOpened: PropTypes.bool,
    isSuspendUserError: PropTypes.bool,
    suspendUserErrorMessage: PropTypes.string,
    isSuspendUserFetching: PropTypes.bool,
  };

  static defaultProps = {
    userIndex: undefined,
    userDetails: undefined,
    handleToggleSuspendUserModal: () => {},
    suspendUser: () => {},
    isOpened: false,
    isSuspendUserError: false,
    suspendUserErrorMessage: undefined,
    isSuspendUserFetching: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      suspendTo: '',
    };
  }

  onSubmitForm = e => {
    e.preventDefault();
    const { suspendUser: suspendUserAction, userDetails, userIndex } = this.props;
    const { suspendTo } = this.state;

    suspendUserAction(userDetails._id, userIndex, suspendTo);
  };

  handleToDateInputChange = value => {
    this.setState({ suspendTo: value });
  };

  render() {
    const {
      handleToggleSuspendUserModal,
      isOpened,
      isSuspendUserError,
      suspendUserErrorMessage,
      isSuspendUserFetching,
    } = this.props;
    const { suspendTo } = this.state;
    const { BRANDING_GREEN, WHITE, ERROR, LIGHT_RED, DISABLED, GREY_LIGHT } = COLORS;
    const { HEADING } = FONT_TYPES;
    return (
      <Modal
        toggleModal={() => handleToggleSuspendUserModal(undefined, undefined)}
        header="وقف المستخدم"
        isOpened={isOpened}
      >
        <FormContainer onSubmit={this.onSubmitForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ انتهاء الاشتراك
                </Text>
                <StyledDatePicker
                  mb={5}
                  inline={false}
                  placeholderText="تاريخ انتهاء وقف المستخدم"
                  dateFormat="MM/dd/yyyy"
                  borderColor={COLORS_VALUES[DISABLED]}
                  backgroundColor={COLORS_VALUES[GREY_LIGHT]}
                  selected={suspendTo ? new Date(suspendTo) : undefined}
                  onChange={this.handleToDateInputChange}
                  minDate={new Date(moment().add(1, 'days'))}
                  isClearable
                />
              </SectionFlexContainer>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                reverse
                // disabled={isSubscribeWorkerFetching || !suspendTo}
                isLoading={isSuspendUserFetching}
              >
                وقف الفني
              </Button>
              {isSuspendUserError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {suspendUserErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </FormContainer>
      </Modal>
    );
  }
}
SuspendModal.displayName = 'SuspendModal';

export default SuspendModal;
