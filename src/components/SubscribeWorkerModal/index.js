import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import 'react-datepicker/dist/react-datepicker.css';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { subscribeWorker } from 'redux-modules/workers/actions';

const FormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const { BRANDING_ORANGE, GREY_DARK } = COLORS;

const SectionFlexContainer = styled(Flex)`
  .react-datepicker-popper {
    direction: ltr;
    &:lang(ar) {
      text-align: right;
      .react-datepicker__triangle {
        right: 50px;
        left: inherit;
      }
    }
  }
  .react-datepicker__input-container {
    display: block;
  }
  .react-datepicker__day--selected,
  .react-datepicker__day--in-selecting-range,
  .react-datepicker__day--in-range,
  .react-datepicker__month-text--selected,
  .react-datepicker__month-text--in-selecting-range,
  .react-datepicker__month-text--in-range {
    background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
    &:hover {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      opacity: 0.8;
    }
  }
  .react-datepicker__time-container,
  .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box {
    width: 120px;
  }
  .react-datepicker__time-container
    .react-datepicker__time
    .react-datepicker__time-box
    ul.react-datepicker__time-list
    li.react-datepicker__time-list-item {
    display: flex;
    justify-content: center;
    align-items: center;
    &.react-datepicker__time-list-item--selected {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      &:hover {
        background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
        opacity: 0.8;
      }
    }
  }
  .react-datepicker {
    font-family: 'english-font';
    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }
    .react-datepicker-time__header {
      color: ${COLORS_VALUES[GREY_DARK]};
      font-weight: normal;
      font-size: 0.8rem;
    }
  }
  .react-datepicker__close-icon {
    right: 80%;
  }
  .react-datepicker__close-icon::after {
    height: 13px;
    width: 15px;
  }
`;

class SubscribeWorkerToAutoApplyModal extends Component {
  static propTypes = {
    workerDetails: PropTypes.shape({}),
    isSubscribeWorkerFetching: PropTypes.bool,
    isSubscribeWorkerError: PropTypes.bool,
    subscribeWorkerErrorMessage: PropTypes.string,
    endpoint: PropTypes.string,
    ModalHeader: PropTypes.string,
    subscribeWorker: PropTypes.func,
    handleToggleModal: PropTypes.func,
    isModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    workerDetails: undefined,
    isSubscribeWorkerFetching: false,
    isSubscribeWorkerError: false,
    subscribeWorkerErrorMessage: '',
    endpoint: '',
    ModalHeader: '',
    subscribeWorker: () => {},
    handleToggleModal: () => {},
    isModalOpen: false,
  };

  onSubmitForm = e => {
    e.preventDefault();
    const {
      workerDetails: { _id },
      subscribeWorker: subscribeWorkerAction,
      endpoint: subscribeEndpoint,
    } = this.props;

    const Info = {
      workerId: _id,
    };

    subscribeWorkerAction(undefined, subscribeEndpoint, Info);
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  render() {
    const {
      isSubscribeWorkerFetching,
      isSubscribeWorkerError,
      handleToggleModal,
      isModalOpen,
      subscribeWorkerErrorMessage,
      ModalHeader,
    } = this.props;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;

    return (
      <Modal toggleModal={() => handleToggleModal({})} header={ModalHeader} isOpened={isModalOpen}>
        <FormContainer onSubmit={this.onSubmitForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  متاكد انك تريد تجديد اشتراك لمدة شهر
                </Text>
                {/* <StyledDatePicker */}
                {/*  mb={5} */}
                {/*  inline={false} */}
                {/*  placeholderText="تاريخ انتهاء الاشتراك" */}
                {/*  dateFormat="MM/dd/yyyy" */}
                {/*  borderColor={COLORS_VALUES[DISABLED]} */}
                {/*  backgroundColor={COLORS_VALUES[GREY_LIGHT]} */}
                {/*  selected={dueDate ? new Date(dueDate) : undefined} */}
                {/*  onChange={this.handleToDateInputChange} */}
                {/*  isClearable */}
                {/* /> */}
              </SectionFlexContainer>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                reverse
                disabled={isSubscribeWorkerFetching}
                isLoading={isSubscribeWorkerFetching}
              >
                اشترك
              </Button>
              {isSubscribeWorkerError && (
                <Caution mx={2} isRtl bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {subscribeWorkerErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </FormContainer>
      </Modal>
    );
  }
}
SubscribeWorkerToAutoApplyModal.displayName = 'SubscribeWorkerToAutoApplyModal';

const mapStateToProps = state => ({
  isModalOpen:
    state.workers.subscribeWorkerToAutoApplyModal.isOpen ||
    state.workers.subscribeWorkerToMultiFieldsModal.isOpen,
  isSubscribeWorkerFetching: state.workers.subscribeWorker.isFetching,
  isSubscribeWorkerError: state.workers.subscribeWorker.isFail.isError,
  subscribeWorkerErrorMessage: state.workers.subscribeWorker.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      subscribeWorker,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(SubscribeWorkerToAutoApplyModal);
