import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import WorkerCard from 'components/WorkerCard';
import ConfirmModal from 'components/ConfirmModal';
import AddNewWorkerModal from 'components/AddNewWorkerModal';
import EditWorkerModal from 'components/EditWorkerModal';
import AddCreditToWorkerModal from 'components/AddCreditToWorkerModal';
import OpsCommentsModal from 'components/OpsCommentsModal';
import SuspendModal from 'components/SuspendModal';

const Workers = props => {
  const {
    user,
    user: { permissions },
    workers,
    workerIndex,
    workerDetails,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    isAddNewWorkerModalOpen,
    isEditWorkerModalOpen,
    isAddCreditModalOpen,
    handleToggleConfirmModal,
    handleToggleEditWorkerModal,
    handleToggleAddCreditModal,
    handleActivateWorker,
    // handleDeleteWorker,
    handleFeatureWorker,
    handleMakeWorkerElite,
    isActivateWorkerFetching,
    isActivateWorkerError,
    isFeatureWorkerFetching,
    isFeatureWorkerError,
    featureWorkerErrorMessage,
    isMakeWorkerEliteFetching,
    isMakeWorkerEliteError,
    isDeleteWorkerFetching,
    isOpsCommentsModalOpen,
    handleToggleOpsCommentsModal,
    handleToggleSuspendWorkerModal,
    isSuspendWorkerModalOpen,
    handleSuspendWorker,
    isSuspendWorkerFetching,
    isSuspendWorkerError,
    suspendWorkerErrorMessage,
    vendorsList,
  } = props;
  
  const workersRenderer = workers.map((worker, index) => {
    const { _id } = worker;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <WorkerCard
          m={2}
          index={index}
          user={user}
          worker={worker}
          vendorsList={vendorsList}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleToggleEditWorkerModal={handleToggleEditWorkerModal}
          handleToggleAddCreditModal={handleToggleAddCreditModal}
          handleToggleOpsCommentsModal={handleToggleOpsCommentsModal}
          handleActivateWorker={handleActivateWorker}
          handleFeatureWorker={handleFeatureWorker}
          handleMakeWorkerElite={handleMakeWorkerElite}
          // handleDeleteWorker={handleDeleteWorker}
          isActivateWorkerFetching={isActivateWorkerFetching}
          isActivateWorkerError={isActivateWorkerError}
          isFeatureWorkerFetching={isFeatureWorkerFetching}
          isFeatureWorkerError={isFeatureWorkerError}
          isMakeWorkerEliteFetching={isMakeWorkerEliteFetching}
          isMakeWorkerEliteError={isMakeWorkerEliteError}
          handleToggleSuspendWorkerModal={handleToggleSuspendWorkerModal}
          handleSuspendWorker={handleSuspendWorker}
          isSuspendWorkerFetching={isSuspendWorkerFetching}
          isSuspendWorkerError={isSuspendWorkerError}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {workersRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={
            isActivateWorkerFetching ||
            isDeleteWorkerFetching ||
            isSuspendWorkerFetching ||
            isFeatureWorkerFetching ||
            isMakeWorkerEliteFetching
          }
          isFail={isFeatureWorkerError}
          errorMessage={featureWorkerErrorMessage}
        />
      )}
      {isOpsCommentsModalOpen && (
        <OpsCommentsModal
          objectType="worker"
          object={workers[workerIndex]}
          isOpened={isOpsCommentsModalOpen}
          toggleFunction={handleToggleOpsCommentsModal}
          objectIndex={workerIndex}
          hideAddCommentSection={!(permissions && permissions.includes('Add New User Comments'))}
        />
      )}
      {isEditWorkerModalOpen && (
        <EditWorkerModal
          workerIndex={workerIndex}
          workerDetails={workerDetails}
          vendorsList={vendorsList}
        />
      )}
      {isAddNewWorkerModalOpen && (
        <AddNewWorkerModal workerDetails={workerDetails} vendorsList={vendorsList} />
      )}
      {isAddCreditModalOpen && (
        <AddCreditToWorkerModal workerIndex={workerIndex} workerDetails={workerDetails} />
      )}
      {isSuspendWorkerModalOpen && (
        <SuspendModal
          userIndex={workerIndex}
          userDetails={workerDetails}
          handleToggleSuspendUserModal={handleToggleSuspendWorkerModal}
          isOpened={isSuspendWorkerModalOpen}
          suspendUser={handleSuspendWorker}
          isSuspendUserError={isSuspendWorkerError}
          isSuspendUserFetching={isSuspendWorkerFetching}
          suspendUserErrorMessage={suspendWorkerErrorMessage}
        />
      )}
    </Flex>
  );
};
Workers.displayName = 'Workers';

Workers.propTypes = {
  user: PropTypes.shape({}),
  workers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  workerIndex: PropTypes.number,
  workerDetails: PropTypes.shape({}),
  isConfirmModalOpen: PropTypes.bool,
  isAddNewWorkerModalOpen: PropTypes.bool,
  isEditWorkerModalOpen: PropTypes.bool,
  isAddCreditModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditWorkerModal: PropTypes.func,
  handleToggleAddCreditModal: PropTypes.func,
  handleActivateWorker: PropTypes.func,
  // handleDeleteWorker: PropTypes.func,
  handleFeatureWorker: PropTypes.func,
  handleMakeWorkerElite: PropTypes.func,
  isActivateWorkerFetching: PropTypes.bool,
  isActivateWorkerError: PropTypes.bool,
  isFeatureWorkerFetching: PropTypes.bool,
  isFeatureWorkerError: PropTypes.bool,
  featureWorkerErrorMessage: PropTypes.string,
  isMakeWorkerEliteFetching: PropTypes.bool,
  isMakeWorkerEliteError: PropTypes.bool,
  isDeleteWorkerFetching: PropTypes.bool,
  isOpsCommentsModalOpen: PropTypes.bool,
  handleToggleOpsCommentsModal: PropTypes.func,
  handleToggleSuspendWorkerModal: PropTypes.func,
  isSuspendWorkerModalOpen: PropTypes.bool,
  handleSuspendWorker: PropTypes.func,
  isSuspendWorkerFetching: PropTypes.bool,
  isSuspendWorkerError: PropTypes.bool,
  suspendWorkerErrorMessage: PropTypes.string,
  vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
};

Workers.defaultProps = {
  user: undefined,
  workerIndex: 0,
  workerDetails: undefined,
  isConfirmModalOpen: false,
  isAddNewWorkerModalOpen: false,
  isEditWorkerModalOpen: false,
  isAddCreditModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleEditWorkerModal: () => {},
  handleToggleAddCreditModal: () => {},
  handleActivateWorker: () => {},
  // handleDeleteWorker: () => {},
  handleFeatureWorker: () => {},
  handleMakeWorkerElite: () => {},
  isActivateWorkerFetching: false,
  isActivateWorkerError: false,
  isFeatureWorkerFetching: false,
  isFeatureWorkerError: false,
  featureWorkerErrorMessage: undefined,
  isMakeWorkerEliteFetching: false,
  isMakeWorkerEliteError: false,
  isDeleteWorkerFetching: false,
  isOpsCommentsModalOpen: false,
  handleToggleOpsCommentsModal: () => {},
  handleToggleSuspendWorkerModal: () => {},
  isSuspendWorkerModalOpen: false,
  handleSuspendWorker: () => {},
  isSuspendWorkerFetching: false,
  isSuspendWorkerError: false,
  suspendWorkerErrorMessage: undefined,
  vendorsList: [],
};

export default Workers;
