import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import {
  toggleAddNewCommissionModal,
  addNewCommission,
  updateCommission,
  openEditCommissionModal,
} from 'redux-modules/commissions/actions';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { customSelectStyles, selectColors, StyledSelect } from 'components/shared';
import { FIELDS } from 'utils/constants';
import InputField from 'components/InputField';

const fieldsKeys = Object.keys(FIELDS);
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
const AddNewCommissionFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddNewCommissionModal extends Component {
  static propTypes = {
    isAddCommissionFetching: PropTypes.bool,
    isAddCommissionError: PropTypes.bool,
    isEditCommissionError: PropTypes.bool,
    addCommissionErrorMessage: PropTypes.string,
    editCommissionErrorMessage: PropTypes.string,
    addNewCommission: PropTypes.func,
    toggleAddNewCommissionModal: PropTypes.func,
    openEditCommissionModal: PropTypes.func,
    updateCommission: PropTypes.func,
    isAddCommissionModalOpen: PropTypes.bool,
    commissionDetails: PropTypes.shape({}),
    commissionIndex: PropTypes.number,
    isEditCommissionFetching: PropTypes.bool,
  };

  static defaultProps = {
    isAddCommissionFetching: false,
    isAddCommissionError: false,
    isEditCommissionError: false,
    addCommissionErrorMessage: '',
    editCommissionErrorMessage: '',
    addNewCommission: () => {},
    toggleAddNewCommissionModal: () => {},
    openEditCommissionModal: () => {},
    updateCommission: () => {},
    isAddCommissionModalOpen: false,
    commissionDetails: undefined,
    commissionIndex: undefined,
    isEditCommissionFetching: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      field: '',
      rate: '',
      rateForOnline: '',
    };
  }

  componentDidMount(): void {
    const { commissionDetails } = this.props;

    if (commissionDetails) {
      this.setState({
        field:
          fieldsSelectOptions.filter(
            fieldOption => fieldOption.value === commissionDetails.field,
          )[0] || '',
        rate: +commissionDetails.rate || '',
        rateForOnline: +commissionDetails.rateForOnline || '',
      });
    }
  }

  handleChangeFieldSelect = field => {
    this.setState({ field });
  };

  handleChangeTypeSelect = rate => {
    this.setState({ rate });
  };

  handleChangeRateForOnlineSelect = rate => {
    this.setState({ rateForOnline: rate });
  };

  onSubmitAddCommissionForm = e => {
    e.preventDefault();

    const {
      addNewCommission: addNewCommissionAction,
      updateCommission: updateCommissionAction,
      commissionDetails,
      commissionIndex,
    } = this.props;
    const { field, rate, rateForOnline } = this.state;

    const newCommissionInfo = {
      field: field ? field.value : '',
      rate,
      rateForOnline: +rateForOnline
    };

    if (commissionDetails) {
      const { _id } = commissionDetails;
      return updateCommissionAction(newCommissionInfo, _id, commissionIndex);
    }

    return addNewCommissionAction(newCommissionInfo);
  };

  render() {
    const {
      isAddCommissionFetching,
      isAddCommissionModalOpen,
      toggleAddNewCommissionModal: toggleAddNewCommissionModalAction,
      openEditCommissionModal: openEditCommissionModalAction,
      isAddCommissionError,
      isEditCommissionError,
      addCommissionErrorMessage,
      editCommissionErrorMessage,
      commissionDetails,
      isEditCommissionFetching,
    } = this.props;
    const { field, rate, rateForOnline } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addCommissionModalHeader = commissionDetails ? 'تعديل العمولة' : 'أضف معدل عمولة جديدة';
    const submitButtonText = commissionDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          commissionDetails ? openEditCommissionModalAction() : toggleAddNewCommissionModalAction()
        }
        header={addCommissionModalHeader}
        isOpened={isAddCommissionModalOpen}
      >
        <AddNewCommissionFormContainer onSubmit={this.onSubmitAddCommissionForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti={false}
                  isRtl
                  backspaceRemovesValue={false}
                  value={field}
                  onChange={this.handleChangeFieldSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  نسبة العمولة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="انسبة العمولة"
                  width={1}
                  value={rate}
                  onChange={this.handleChangeTypeSelect}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  نسبة العمولة الاونلين
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="انسبة العمولة الاونلين"
                  width={1}
                  value={rateForOnline}
                  onChange={this.handleChangeRateForOnlineSelect}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddCommissionForm}
                disabled={isAddCommissionFetching || field === '' || rate === ''}
                isLoading={isAddCommissionFetching || isEditCommissionFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddCommissionError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addCommissionErrorMessage}
                </Caution>
              )) ||
                (isEditCommissionError && (
                  <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                    {editCommissionErrorMessage}
                  </Caution>
                ))}
            </Flex>
          </Flex>
        </AddNewCommissionFormContainer>
      </Modal>
    );
  }
}

AddNewCommissionModal.displayName = 'AddNewCommissionModal';

const mapStateToProps = state => ({
  isAddCommissionFetching: state.commissions.addCommission.isFetching,
  isEditCommissionFetching: state.commissions.updateCommission.isFetching,
  isAddCommissionError: state.commissions.addCommission.isFail.isError,
  isEditCommissionError: state.commissions.updateCommission.isFail.isError,
  addCommissionErrorMessage: state.commissions.addCommission.isFail.message,
  editCommissionErrorMessage: state.commissions.updateCommission.isFail.message,
  isAddCommissionModalOpen:
    state.commissions.addNewCommissionModal.isOpen || state.commissions.editCommissionModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewCommissionModal,
      addNewCommission,
      updateCommission,
      openEditCommissionModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewCommissionModal);
