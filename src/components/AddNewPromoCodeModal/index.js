import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import moment from 'moment';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import {
  addPromoCode,
  updatePromoCode,
  toggleAddNewPromoCodeModal,
  openEditPromoModal,
} from 'redux-modules/promoCodes/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import DatePicker from 'components/Filter/DatePicker';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { FIELDS, CITIES, CONFIRM_CHOICE, DISCOUNT_TYPES } from 'utils/constants';
import _ from 'lodash';
import { Checkbox, Label } from '@rebass/forms';

const fieldsKeys = Object.keys(FIELDS);
const citiesKeys = Object.keys(CITIES);
const confirmChoiceKeys = Object.keys(CONFIRM_CHOICE);
const discountTypesSelectOptions = DISCOUNT_TYPES.map(type => ({
  value: type.value,
  label: type.name.ar,
}));
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
fieldsSelectOptions.unshift({
  value: 'ALL_FIELDS',
  label: 'كل التخصصات',
});
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));
citiesSelectOptions.unshift({
  value: 'ALL_CITIES',
  label: 'كل المدن',
});
const isForNewCustomerSelectOptions = confirmChoiceKeys.map(key => ({
  value: key,
  label: CONFIRM_CHOICE[key].ar,
}));

const AddNewPromoCodeFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddNewPromoCodeModal extends Component {
  static propTypes = {
    isAddPromoCodeFetching: PropTypes.bool,
    isAddPromoCodeError: PropTypes.bool,
    addPromoCodeErrorMessage: PropTypes.string,
    addPromoCode: PropTypes.func,
    toggleAddNewPromoCodeModal: PropTypes.func,
    isAddPromoCodeModalOpen: PropTypes.bool,

    isEditPromoCodeFetching: PropTypes.bool,
    promoDetails: PropTypes.shape({}),
    promoIndex: PropTypes.number,
    openEditPromoModal: PropTypes.func,
    isUpdatePromoCodeError: PropTypes.bool,
    updatePromoCodeErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    isAddPromoCodeFetching: false,
    isAddPromoCodeError: false,
    addPromoCodeErrorMessage: '',
    addPromoCode: () => {},
    toggleAddNewPromoCodeModal: () => {},
    isAddPromoCodeModalOpen: false,

    isEditPromoCodeFetching: false,
    promoDetails: undefined,
    promoIndex: 0,
    openEditPromoModal: () => {},
    isUpdatePromoCodeError: false,
    updatePromoCodeErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    this.state = {
      code: '',
      fields: [],
      cities: [],
      discountValue: '',
      discountType: '',
      upperLimitForDiscount: '',
      minimumWorkerWage: '1',
      timesPerCustomerCanUse: '',
      expireAt: '',
      isForNewCustomer: { label: CONFIRM_CHOICE.false.ar, value: false },
      workerRefundedMoneyForFixedDiscount: '',
      cashBackCreditsExpirationPeriodInDays: 0,
      isTempCredits: false,
      hasUpperLimitForDiscount: false,
    };
  }

  componentDidMount() {
    const { promoDetails } = this.props;

    if (promoDetails) {
      this.setState({
        code: promoDetails.code,
        fields: fieldsSelectOptions.filter(options => promoDetails.fields.includes(options.value)),
        cities: citiesSelectOptions.filter(options => promoDetails.cities.includes(options.value)),
        discountValue: promoDetails.discount.value.toString(),
        discountType: discountTypesSelectOptions.find(
          option => option.value === promoDetails.discount.type,
        ),
        upperLimitForDiscount: promoDetails.upperLimitForDiscount
          ? promoDetails.upperLimitForDiscount.toString()
          : '',
        minimumWorkerWage: promoDetails.minimumWorkerWage
          ? promoDetails.minimumWorkerWage.toString()
          : '',
        timesPerCustomerCanUse: promoDetails.timesPerCustomerCanUse.toString(),
        expireAt: moment(promoDetails.expireAt).format('YYYY-MM-DD'),
        isForNewCustomer: isForNewCustomerSelectOptions.find(
          option => option.value.toString() === promoDetails.isForNewCustomer.toString(),
        ),
        workerRefundedMoneyForFixedDiscount: promoDetails.workerRefundedMoneyForFixedDiscount
          ? promoDetails.workerRefundedMoneyForFixedDiscount
          : '',
        cashBackCreditsExpirationPeriodInDays:
          promoDetails.discount.cashBackCreditsExpirationPeriodInDays,
        isTempCredits: promoDetails.discount.isTempCredits,
        hasUpperLimitForDiscount: !!promoDetails.upperLimitForDiscount,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleActiveCheckBoxChange = event => {
    this.setState({ isTempCredits: event.target.checked });
  };

  handleHasUpperLimitForDiscountBoxChange = event => {
    this.setState({ hasUpperLimitForDiscount: event.target.checked });
  };

  handleExpireDateInputChange = expireAt => {
    const expireDateString = expireAt
      ? expireAt.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';

    this.setState({ expireAt: expireDateString });
  };

  handleChangeDiscountTypeSelect = discountType => {
    this.setState({ discountType });
  };

  handleChangeFieldSelect = fields => {
    const all = fields.some(f => f.value === 'ALL_FIELDS');
    if (all) {
      fields = [...fieldsSelectOptions];
      fields.shift();
    }
    this.setState({ fields });
  };

  handleChangeCitiesSelect = cities => {
    const all = cities.some(c => c.value === 'ALL_CITIES');
    if (all) {
      cities = [...citiesSelectOptions];
      cities.shift();
      cities.pop();
    }
    this.setState({ cities });
  };

  handleChangeIsForNewCustomerSelect = isForNewCustomer => {
    this.setState({ isForNewCustomer });
  };

  onSubmitAddPromoCodeForm = e => {
    e.preventDefault();

    const {
      addPromoCode: addPromoCodeAction,
      updatePromoCode: updatePromoCodeAction,
      promoDetails,
      promoIndex,
    } = this.props;
    const {
      code,
      fields,
      cities,
      discountValue,
      discountType,
      upperLimitForDiscount,
      minimumWorkerWage,
      timesPerCustomerCanUse,
      expireAt,
      isForNewCustomer,
      workerRefundedMoneyForFixedDiscount,
      cashBackCreditsExpirationPeriodInDays,
      isTempCredits,
      hasUpperLimitForDiscount,
    } = this.state;
    const promoCodeFields = !_.isEmpty(fields) ? fields.map(record => record.value) : [];
    const promoCodeCities = !_.isEmpty(cities) ? cities.map(record => record.value) : [];
    const promoCodeDiscountType = discountType ? discountType.value : '';

    let newPromoCodeInfo = {
      code,
      fields: promoCodeFields,
      cities: promoCodeCities,
      discount: {
        value: Number(convertArabicNumbersToEnglish(discountValue)),
        type: promoCodeDiscountType,
      },
      timesPerCustomerCanUse: Number(convertArabicNumbersToEnglish(timesPerCustomerCanUse)),
      expireAt,
      isForNewCustomer: _.get(isForNewCustomer, 'value')
        ? isForNewCustomer.value.toString()
        : 'false',
    };

    if (
      upperLimitForDiscount &&
      upperLimitForDiscount !== '' &&
      ['percentage'].includes(promoCodeDiscountType)
    ) {
      newPromoCodeInfo = {
        ...newPromoCodeInfo,
        upperLimitForDiscount: Number(convertArabicNumbersToEnglish(upperLimitForDiscount)),
      };
    }

    if (
      upperLimitForDiscount &&
      upperLimitForDiscount !== '' &&
      hasUpperLimitForDiscount &&
      ['cashBackPercentage'].includes(promoCodeDiscountType)
    ) {
      newPromoCodeInfo = {
        ...newPromoCodeInfo,
        upperLimitForDiscount: Number(convertArabicNumbersToEnglish(upperLimitForDiscount)),
      };
    }

    if (minimumWorkerWage && minimumWorkerWage !== '') {
      newPromoCodeInfo = {
        ...newPromoCodeInfo,
        minimumWorkerWage: Number(convertArabicNumbersToEnglish(minimumWorkerWage)),
      };
    }

    if (promoCodeDiscountType === 'fixed') {
      newPromoCodeInfo = {
        ...newPromoCodeInfo,
        workerRefundedMoneyForFixedDiscount,
      };
    }

    if (['cashBack', 'cashBackPercentage'].includes(promoCodeDiscountType) && isTempCredits) {
      newPromoCodeInfo.discount.cashBackCreditsExpirationPeriodInDays = cashBackCreditsExpirationPeriodInDays;
      newPromoCodeInfo.discount.isTempCredits = isTempCredits;
    }

    if (promoDetails) {
      const { _id } = promoDetails;
      return updatePromoCodeAction(newPromoCodeInfo, _id, promoIndex);
    }

    addPromoCodeAction(newPromoCodeInfo);
  };

  render() {
    const {
      isAddPromoCodeFetching,
      isAddPromoCodeModalOpen,
      toggleAddNewPromoCodeModal: toggleAddNewPromoCodeModalAction,
      openEditPromoModal: openEditPromoModalAction,
      isAddPromoCodeError,
      addPromoCodeErrorMessage,
      promoDetails,
      promoIndex,
      isEditPromoCodeFetching,
      isUpdatePromoCodeError,
      updatePromoCodeErrorMessage,
    } = this.props;
    const {
      code,
      fields,
      cities,
      discountValue,
      discountType,
      upperLimitForDiscount,
      minimumWorkerWage,
      timesPerCustomerCanUse,
      expireAt,
      isForNewCustomer,
      workerRefundedMoneyForFixedDiscount,
      cashBackCreditsExpirationPeriodInDays,
      isTempCredits,
      hasUpperLimitForDiscount,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addPromoCodeModalHeader = promoDetails ? 'تعديل برموكود' : 'أضف بروموكود';
    const submitButtonText = promoDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          promoDetails
            ? openEditPromoModalAction(promoIndex, promoDetails)
            : toggleAddNewPromoCodeModalAction()
        }
        header={addPromoCodeModalHeader}
        isOpened={isAddPromoCodeModalOpen}
      >
        <AddNewPromoCodeFormContainer onSubmit={this.onSubmitAddPromoCodeForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم البروموكود
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم البروموكود"
                  ref={inputField => {
                    this.codeField = inputField;
                  }}
                  width={1}
                  value={code}
                  onChange={value => this.handleInputFieldChange('code', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ الانتهاء
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <DatePicker
                  mb={0}
                  minDate={new Date()}
                  value={expireAt}
                  handleDatePickerChange={this.handleExpireDateInputChange}
                  placeholder="MM/DD/YYYY"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={fields}
                  onChange={this.handleChangeFieldSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isMulti
                  isClearable
                />
              </Flex>
              <Flex flexDirection="column" ml={5} width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  هل مخصص للعملاء الجداد
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={isForNewCustomer}
                  onChange={this.handleChangeIsForNewCustomerSelect}
                  options={isForNewCustomerSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} ml={5} pr={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  نوع الخصم
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="نوع الخصم"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={discountType}
                  onChange={this.handleChangeDiscountTypeSelect}
                  options={discountTypesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                  isMulti={false}
                  // isDisabled={!_.isEmpty(promoDetails)}
                />
              </Flex>
            </Flex>
            {discountType && ['cashBack', 'cashBackPercentage'].includes(discountType.value) && (
              <Flex pb={3} px={4}>
                <Box>
                  <Label>
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                      اضافة الكاش باك كرصيد مؤقت؟
                    </Text>
                    <Checkbox
                      name="remember"
                      checked={isTempCredits}
                      onChange={this.handleActiveCheckBoxChange}
                      sx={{ color: '#fcfffa' }}
                    />
                  </Label>
                </Box>
              </Flex>
            )}
            {discountType && ['cashBackPercentage'].includes(discountType.value) && (
              <Flex pb={3} px={4}>
                <Box>
                  <Label>
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                      اضافة الحد الأعلى للكاش باك؟
                    </Text>
                    <Checkbox
                      name="remember"
                      checked={hasUpperLimitForDiscount}
                      onChange={this.handleHasUpperLimitForDiscountBoxChange}
                      sx={{ color: '#fcfffa' }}
                    />
                  </Label>
                </Box>
              </Flex>
            )}
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  قيمة الخصم
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="قيمة الخصم"
                  ref={inputField => {
                    this.discountValueField = inputField;
                  }}
                  width={1}
                  value={discountValue}
                  onChange={value => this.handleInputFieldChange('discountValue', value)}
                  mb={2}
                  autoComplete="on"
                  // disabled={!_.isEmpty(promoDetails)}
                />
              </Flex>
              {discountType && discountType.value === 'fixed' && (
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    قيمة تعويد الفنى
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="قيمة تعويد الفنى"
                    width={1}
                    value={workerRefundedMoneyForFixedDiscount}
                    onChange={value =>
                      this.handleInputFieldChange('workerRefundedMoneyForFixedDiscount', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              )}
              {discountType &&
                ['cashBack', 'cashBackPercentage'].includes(discountType.value) &&
                isTempCredits && (
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                      مدة انتهاء الرصيد المؤقت للعميل بالأيام
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                        *
                    </Text>
                  </Text>
                  <InputField
                    type="number"
                    placeholder="مدة انتهاء الرصيد بالأيام"
                    width={1}
                    value={cashBackCreditsExpirationPeriodInDays}
                    onChange={value =>
                      this.handleInputFieldChange('cashBackCreditsExpirationPeriodInDays', value)
                    }
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              )}
            </Flex>
            <Flex pb={3} px={4}>
              {discountType &&
                (['percentage'].includes(discountType.value) ||
                  (['cashBackPercentage'].includes(discountType.value) &&
                    hasUpperLimitForDiscount)) && (
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                      الحد الأعلي للخصم
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                        *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="الحد الأعلي للخصم"
                    ref={inputField => {
                      this.upperLimitField = inputField;
                    }}
                    width={1}
                    value={upperLimitForDiscount}
                    onChange={value =>
                      this.handleInputFieldChange('upperLimitForDiscount', value)
                    }
                    mb={2}
                    autoComplete="on"
                    // disabled={!_.isEmpty(promoDetails)}
                  />
                </Flex>
              )}
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الحد الأدني لأجرة الفني
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الحد الأدني لأجرة الفني"
                  ref={inputField => {
                    this.minimumWorkerWageField = inputField;
                  }}
                  width={1}
                  value={minimumWorkerWage}
                  onChange={value => this.handleInputFieldChange('minimumWorkerWage', value)}
                  mb={2}
                  autoComplete="on"
                  // disabled={!_.isEmpty(promoDetails)}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدينة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={cities}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isMulti
                  isClearable
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  عدد المرات لكل عميل
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="عدد المرات لكل عميل"
                  ref={inputField => {
                    this.timesPerCustomerField = inputField;
                  }}
                  width={1}
                  value={timesPerCustomerCanUse}
                  onChange={value => this.handleInputFieldChange('timesPerCustomerCanUse', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddPromoCodeForm}
                disabled={isAddPromoCodeFetching}
                isLoading={isAddPromoCodeFetching || isEditPromoCodeFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddPromoCodeError || isUpdatePromoCodeError) && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addPromoCodeErrorMessage || updatePromoCodeErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewPromoCodeFormContainer>
      </Modal>
    );
  }
}
AddNewPromoCodeModal.displayName = 'AddNewPromoCodeModal';

const mapStateToProps = state => ({
  isAddPromoCodeFetching: state.promoCodes.addPromoCode.isFetching,
  isEditPromoCodeFetching: state.promoCodes.editPromoCode.isFetching,
  isAddPromoCodeError: state.promoCodes.addPromoCode.isFail.isError,
  isUpdatePromoCodeError: state.promoCodes.editPromoCode.isFail.isError,
  addPromoCodeErrorMessage: state.promoCodes.addPromoCode.isFail.message,
  isAddPromoCodeModalOpen:
    state.promoCodes.addNewPromoCodeModal.isOpen || state.promoCodes.editPromoModal.isOpen,
  updatePromoCodeErrorMessage: state.promoCodes.editPromoCode.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addPromoCode,
      updatePromoCode,
      toggleAddNewPromoCodeModal,
      openEditPromoModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPromoCodeModal);
