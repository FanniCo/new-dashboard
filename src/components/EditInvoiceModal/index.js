import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Flex } from '@rebass/grid';
import { editInvoice } from 'redux-modules/requests/actions';
import InputField from 'components/InputField';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import Caution from 'components/Caution';

class EditInvoiceModal extends Component {
  static propTypes = {
    request: PropTypes.shape({}),
    worker: PropTypes.shape({}),
    customer: PropTypes.shape({}),
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
    isEditInvoiceFetching: PropTypes.bool,
    isEditInvoiceError: PropTypes.bool,
    isEditInvoiceErrorMessage: PropTypes.string,
    editInvoice: PropTypes.func,
  };

  static defaultProps = {
    request: undefined,
    worker: undefined,
    customer: undefined,
    isOpened: false,
    isEditInvoiceFetching: false,
    isEditInvoiceError: false,
    isEditInvoiceErrorMessage: undefined,
    toggleFunction: () => {},
    editInvoice: () => {},
  };

  state = {
    workersWage: 0,
    partsCost: 0,
  };

  componentDidMount() {
    const {
      request: { workersWage, partsCost },
    } = this.props;

    this.setState({
      workersWage,
      partsCost,
    });
  }

  handleWorkersWageChange = value => {
    this.setState({ workersWage: value });
  };

  handlePartsCostChange = value => {
    this.setState({ partsCost: value });
  };

  handleEditInvoice = (request, worker) => {
    const requestId = request._id;
    const workerId = worker._id;
    const { workersWage, partsCost } = this.state;
    const { editInvoice: editInvoiceAction } = this.props;

    const invoice = {
      requestId,
      workerId,
      workersWage,
      partsCost,
    };

    editInvoiceAction(invoice);
  };

  render() {
    const {
      isOpened,
      request,
      worker,
      toggleFunction,
      isEditInvoiceFetching,
      isEditInvoiceError,
      isEditInvoiceErrorMessage,
    } = this.props;
    const { BRANDING_GREEN, BRANDING_BLUE, WHITE, LIGHT_RED, ERROR } = COLORS;
    const { HEADING } = FONT_TYPES;
    const { workersWage, partsCost } = this.state;
    const EditInvoiceModalHeader = 'تعديل فاتورة';

    return (
      <Modal toggleModal={toggleFunction} header={EditInvoiceModalHeader} isOpened={isOpened}>
        <Flex flexDirection="column" px={3}>
          <Flex pb={3} px={2}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                اجرة الفني
                <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                  *
                </Text>
              </Text>
              <InputField
                mb={2}
                type="text"
                width={1}
                value={workersWage}
                placeholder="اجرة الفني"
                onChange={this.handleWorkersWageChange}
              />
            </Flex>
          </Flex>
          <Flex pb={3} px={2}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                تكلفة القطع
                <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                  *
                </Text>
              </Text>
              <InputField
                mb={2}
                type="text"
                width={1}
                value={partsCost}
                placeholder="تكلفة القطع"
                onChange={this.handlePartsCostChange}
              />
            </Flex>
          </Flex>
          <Flex flexDirection="row-reverse" py={5} px={5}>
            <Button
              mx={1}
              color={BRANDING_BLUE}
              onClick={() => this.handleEditInvoice(request, worker)}
              isLoading={isEditInvoiceFetching}
              disabled={!workersWage}
            >
              تأكيد تعديل الفاتورة
            </Button>

            {isEditInvoiceError && (
              <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                {isEditInvoiceErrorMessage}
              </Caution>
            )}
          </Flex>
        </Flex>
      </Modal>
    );
  }
}

EditInvoiceModal.displayName = 'EditInvoiceModal';

const mapStateToProps = state => ({
  isEditInvoiceFetching: state.requests.editInvoice.isFetching,
  isEditInvoiceSuccess: state.requests.editInvoice.isSuccess,
  isEditInvoiceError: state.requests.editInvoice.isFail.isError,
  isEditInvoiceErrorMessage: state.requests.editInvoice.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      editInvoice,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(EditInvoiceModal);
