import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import ConfirmModal from 'components/ConfirmModal';
import OpsCommentsModal from 'components/OpsCommentsModal';
import RequestFollowsUpModal from 'components/RequestFollowsUpModal';
import RequestCard from 'components/RequestCard';
import AdminCancelRequestModal from 'components/AdminCancelRequestModal';
import ls from 'local-storage';
import { COOKIES_KEYS } from 'utils/constants';
import axios from 'axios';

import "@sendbird/uikit-react/dist/index.css";
import SendbirdProvider from '@sendbird/uikit-react/SendbirdProvider';
import ChannelSettingsUI from '@sendbird/uikit-react/ChannelSettings/components/ChannelSettingsUI';
import { ChannelSettingsProvider } from '@sendbird/uikit-react/ChannelSettings/context';
import { ChannelProvider } from '@sendbird/uikit-react/Channel/context';
import ChannelUI from '@sendbird/uikit-react/Channel/components/ChannelUI';

import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core';


    const { PLAIN_USER } = COOKIES_KEYS;
    const plainUser = ls.get(PLAIN_USER);

    

const Requests = props => {
  const {
    user,
    user: { permissions },
    requests,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    isCancelRequestFetching,
    isOpsCommentsModalOpen,
    handleToggleOpsCommentsModal,
    activeRequestModalIndex,
    activeRequest,
    handleToggleRequestFollowsUpModal,
    isRequestFollowsUpModalOpen,
    isCancelAllPendingRequestsFetching,
    isConfirmModalHasError,
    confirmModalErrorMessage,
    isAdminCancelRequestModalOpen,
    handleToggleAdminCancelRequestModal,
  } = props;

  const [OpenNoChat,setOpenNoChat] = useState(false);
  const [ActiveChatChannel,setActiveChannel] = useState('');
  const [SBAccessToken,setSBAccessToken] = useState('');

  const handleCloseNoChat = () => {
    setOpenNoChat(false);
  }
  
  const handleCloseChatBox = () => {
    setActiveChannel('');
  }
  
  axios.get(`https://api-B231229C-D568-49DB-BC5B-03C8FD0605B6.sendbird.com/v3/users/${plainUser._id}`, { headers: { "Api-Token": "c08f8b4855c82942849a97d9b30bef957cd68fe3" } })
  .then(response => {

    if(response.data.access_token) {
      setSBAccessToken(response.data.access_token);
    } else {
      setSBAccessToken('');
    }
  })
  .catch((error) => {

  });

  const handleClickChatIcon = (getIds) => {
    axios.get(`https://api-B231229C-D568-49DB-BC5B-03C8FD0605B6.sendbird.com/v3/group_channels?show_member=true&show_empty=true&members_include_in=${getIds.customer},${getIds.worker}`, { headers: { "Api-Token": "c08f8b4855c82942849a97d9b30bef957cd68fe3" } })
 .then(response => {

  if(response.data.channels[0]) {
    setActiveChannel(response.data.channels[0].channel_url);
  } else {
    setActiveChannel(false);
    setOpenNoChat(true);
  }
  })
 .catch((error) => {
    setActiveChannel(false); 
    setOpenNoChat(true);
  });

  console.log(SBAccessToken);

  }

  

  const requestsRenderer = requests.map((request, index) => {
    const { _id } = request;


    let ids;
    if(request.worker) {
      ids = {customer: request.customer._id, worker:request.worker._id};
    } else {
      ids = '';
    }
    
    

    
       




    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 4]}>
        <RequestCard
          m={2}
          index={index}
          user={user}
          request={request}
          chatChannelUrl={ids}
          handleClickChatIcon={handleClickChatIcon}
          isCancelRequestFetching={isCancelRequestFetching}
          handleToggleOpsCommentsModal={handleToggleOpsCommentsModal}
          handleToggleRequestFollowsUpModal={handleToggleRequestFollowsUpModal}
          handleToggleAdminCancelRequestModal={handleToggleAdminCancelRequestModal}
        />
      </Box>
    );
  });

  return (
    <>
    <Dialog
        className='DialogRoot'
        open={OpenNoChat}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        PaperProps={{
          style: {
            backgroundColor: '#23232d',
            color: '#fff'
          },
        }}
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <span style={{color:'#fff',direction:'rtl',fontFamily:'Tajawal'}}>لم تبدأ المحادثة بين العميل والفني</span>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseNoChat} autoFocus>
            <span style={{color:'#fff'}}>حسنا</span>
          </Button>
        </DialogActions>
      </Dialog>
    { ActiveChatChannel &&
    <div className='ChatWrap'>
      <div className="ChatBox">
        <div className='ChatClose' onClick={handleCloseChatBox}><svg
      viewBox="0 0 24 24"
      fill="#fff"
      height="1em"
      width="1em"
    >
      <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
    </svg></div>
    <SendbirdProvider 
      appId={'B231229C-D568-49DB-BC5B-03C8FD0605B6'}
      userId={plainUser._id}
      accessToken={SBAccessToken}
      theme='dark'
    >
      <div className='SBpopup'>
      <div className='popupSBChat'>
      <ChannelProvider channelUrl={ActiveChatChannel}>
          <ChannelUI />
      </ChannelProvider>
      </div>
      <div className='popupSBSettings'>
      <ChannelSettingsProvider channelUrl={ActiveChatChannel}>
          <ChannelSettingsUI />
      </ChannelSettingsProvider>
      </div>
      </div>
    </SendbirdProvider>
    </div>
    </div>
    }
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {requestsRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isCancelRequestFetching || isCancelAllPendingRequestsFetching}
          isFail={isConfirmModalHasError}
          errorMessage={confirmModalErrorMessage}
        />
      )}
      {isOpsCommentsModalOpen && (
        <OpsCommentsModal
          objectType="request"
          object={activeRequest}
          isOpened={isOpsCommentsModalOpen}
          toggleFunction={handleToggleOpsCommentsModal}
          objectIndex={activeRequestModalIndex}
          hideAddCommentSection={!(permissions && permissions.includes('Add New Request Comments'))}
        />
      )}
      {isRequestFollowsUpModalOpen && (
        <RequestFollowsUpModal
          user={user}
          objectType="request"
          request={activeRequest}
          isOpened={isRequestFollowsUpModalOpen}
          toggleFunction={handleToggleRequestFollowsUpModal}
          objectIndex={activeRequestModalIndex}
        />
      )}
      {isAdminCancelRequestModalOpen && (
        <AdminCancelRequestModal
          requestId={activeRequest._id}
          requestIndex={activeRequestModalIndex}
          isAdminCancelRequestModalOpen={isAdminCancelRequestModalOpen}
          handleToggleAdminCancelRequestModal={handleToggleAdminCancelRequestModal}
        />
      )}
    </Flex>
    </>
  );
};
Requests.displayName = 'Requests';

Requests.propTypes = {
  user: PropTypes.shape({}),
  requests: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isConfirmModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  isCancelRequestFetching: PropTypes.bool,
  isOpsCommentsModalOpen: PropTypes.bool,
  handleToggleOpsCommentsModal: PropTypes.func,
  activeRequestModalIndex: PropTypes.number,
  activeRequest: PropTypes.shape({}),
  handleToggleRequestFollowsUpModal: PropTypes.func,
  isRequestFollowsUpModalOpen: PropTypes.bool,
  isCancelAllPendingRequestsFetching: PropTypes.bool,
  isConfirmModalHasError: PropTypes.bool,
  confirmModalErrorMessage: PropTypes.string,
  isAdminCancelRequestModalOpen: PropTypes.bool,
  handleToggleAdminCancelRequestModal: PropTypes.func,
};

Requests.defaultProps = {
  user: undefined,
  isConfirmModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  isCancelRequestFetching: false,
  isOpsCommentsModalOpen: false,
  handleToggleOpsCommentsModal: () => {},
  activeRequestModalIndex: undefined,
  activeRequest: undefined,
  handleToggleRequestFollowsUpModal: () => {},
  isRequestFollowsUpModalOpen: false,
  isAdminCancelRequestModalOpen: false,
  isCancelAllPendingRequestsFetching: false,
  isConfirmModalHasError: false,
  confirmModalErrorMessage: undefined,
  handleToggleAdminCancelRequestModal: () => {},
};

export default Requests;
