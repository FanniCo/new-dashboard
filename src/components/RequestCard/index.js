import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBan, faCommentDots, faRss } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import { getRequestColor, getRequestIcon, getTicketStatusColor } from 'utils/shared/style';
import {
  CITIES,
  FIELDS,
  REQUESTS_STATUSES,
  TICKETS_STATUS,
  ARRIVAL_ACTIVITY,
  TIME_UNITES,
  CANCELLATION_REASONS,
  POSTPONE_REASONS,
  WORK_LIST_FOR_EVERY_SERVICE,
  PAYMENT_METHOD
} from 'utils/constants';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import {
  CardBody,
  SubTitle,
  CardLabel,
  CardFooter,
  CallToActionIcon,
  MoreDetailsLink,
  HyperLink,
  HyperLinkText,
} from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { get, findLast } from 'lodash';
import moment from 'moment';
import 'moment/locale/ar';


const RequestCard = props => {
  const {
    index,
    user: { permissions },
    request: {
      _id,
      createdAt,
      status,
      statuses,
      requestPrettyId,
      field,
      city,
      customer,
      appliedWorkers,
      worker,
      comments,
      ticket,
      arrivalActivity,
      arrivalEstimationTime,
      timeTakenToFinishWork,
      delayDuration,
      customerServiceFollowUps,
      operationsFollowUps,
      cancellationReason,
      cancellationReasonComment,
      isActivatedBySystem,
      workersWage,
      totalAfterDiscount,
      partsCost,
      postponeTheRequestReason,
      fieldRequirements,
      promoCode,
      paymentMethod,
      cancelledBy,
      maintenanceListHistory,
      timeConsumedNearRequestLocation,
    },
    chatChannelUrl,
    handleClickChatIcon,
    showWorkerName,
    showClientName,
    handleToggleOpsCommentsModal,
    handleToggleRequestFollowsUpModal,
    handleToggleAdminCancelRequestModal,
  } = props;
  const { SINGLE_REQUEST, SINGLE_WORKER, SINGLE_CLIENT } = ROUTES;
  const { GREY_LIGHT, DISABLED, GREY_DARK, BRANDING_GREEN, LIGHT_RED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const requestDate = new Date(createdAt);
  const requestColor = getRequestColor(status);
  const requestIcon = getRequestIcon(status);
  const cancelRequestTooltip = 'إلغاء الطلب';
  const isRequestCanBeCanceled =
    status === REQUESTS_STATUSES.active.en.toLowerCase() ||
    status === REQUESTS_STATUSES.pending.en.toLowerCase() ||
    status === REQUESTS_STATUSES.postponed.en.toLowerCase();
  const requestOpsCommentsTooltip = 'تعليقات العمليات';
  const ticketStatus = ticket && ticket.statuses[ticket.statuses.length - 1].status;
  const ticketStatusName = ticket ? TICKETS_STATUS[ticketStatus].ar : 'لا يوجد';
  const ticketActionMaker = ticket
    ? ticket.statuses[ticket.statuses.length - 1].createdBy.name
    : '';
  const ticketActionUpdatedAt =
    ticket && ticket.statuses[ticket.statuses.length - 1].updatedAt
      ? new Date(ticket.statuses[ticket.statuses.length - 1].updatedAt)
      : '';
  const ticketStatusColor = getTicketStatusColor(ticketStatus);
  const requestFollowsUpTooltip = 'متابعة الطلب';
  const todayDate = new Date();
  let isRequestCancelledBefore = false;
  let isRequestCancelledThenActivated = false;
  // TODO THEIR IS REQUEST WITHOUT STATUSES
  statuses.forEach(({ status }) => {
    if (status === REQUESTS_STATUSES.canceled.en.toLowerCase()) {
      isRequestCancelledBefore = true;
    }

    if (status === REQUESTS_STATUSES.active.en.toLowerCase() && isRequestCancelledBefore) {
      isRequestCancelledThenActivated = true;
    }
  });

  const activeStatusFromStatusesArray = findLast(
    statuses,
    ({ status }) => status === REQUESTS_STATUSES.active.en.toLowerCase(),
  );



  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      className='CardContainer'
    >
      <CardBody className="CardBody" flexDirection="column" justifyContent="space-between" py={1}>
        <Flex className='CardHeader' alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center" className="CardID" style={{ 'background': requestColor }}>
            <span className='boxleft' style={{ boxShadow: `0 -10px 0 0 ${requestColor}` }}></span>
            <span className='boxright' style={{ boxShadow: `0 -10px 0 0 ${requestColor}` }}></span>
            <Box key={`icon_${requestColor}_${requestPrettyId}`}>
              <FontAwesomeIcon icon={requestIcon} color={requestColor} size="sm" />
            </Box>
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${requestColor}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {requestPrettyId}
            </CardLabel>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <Text type={BODY} mx={1} color={requestColor} fontWeight={NORMAL}>
              {`[ ${requestDate.toLocaleDateString('en-GB', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })} ]`}
            </Text>
            <Text type={HEADING} color={requestColor} fontWeight={NORMAL}>
              - <b>{`${FIELDS[field].ar}`}</b>
            </Text>
          </Flex>
        </Flex>
        {showClientName && (
          <Flex className='CardBodyRow' flexDirection="row-reverse" py={1} px={2} mb={2}>
            {customer ? (
              <>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                  العميل :
                </SubTitle>
                <HyperLink href={`${SINGLE_CLIENT}/${customer.username}`} target="_blank">
                  <HyperLinkText
                    cursor="pointer"
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    [ {customer.username} ] - {customer.name}
                  </HyperLinkText>
                </HyperLink>
                {customer.doneRequestsCount === 0 && (
                  <Flex flexDirection="row-reverse" px={2}>
                    <SubTitle color={COLORS_VALUES[LIGHT_RED]} fontWeight={NORMAL}>
                      جديد
                    </SubTitle>
                  </Flex>
                )}
                {customer.lastFollowedUpDate &&
                  new Date(customer.lastFollowedUpDate).toDateString() ===
                    todayDate.toDateString() && (
                    <Flex flexDirection="row-reverse" px={2}>
                      <SubTitle color={COLORS_VALUES[LIGHT_RED]} fontWeight={NORMAL}>
                        تم متابعته
                    </SubTitle>
                    </Flex>
                  )}
              </>
            ) : (
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                لا يوجد عميل
              </SubTitle>
            )}
          </Flex>
        )}
        {showWorkerName && (
          <Flex className='CardBodyRow' flexDirection="row-reverse" py={1} px={2} mb={2}>
            {worker ? (
              <>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                  الفني :
                </SubTitle>
                <HyperLink href={`${SINGLE_WORKER}/${worker.username}`} target="_blank">
                  <HyperLinkText
                    cursor="pointer"
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    [ {worker.username} ] - {worker.name}
                  </HyperLinkText>
                </HyperLink>
              </>
            ) : (
              <SubTitle className="number_label" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                لم يتم اختيار فني
              </SubTitle>
            )}
          </Flex>
        )}
        {paymentMethod && (
          <Flex className='CardBodyRow' flexDirection="row-reverse" py={1} px={2} mb={2}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                  وسيلة الدفع :
                </SubTitle>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {paymentMethod && paymentMethod.length ? PAYMENT_METHOD[paymentMethod].ar : '-'}
              </SubTitle>
          </Flex>
        )}


        <Flex className='CardBodyRow' flexDirection="row-reverse" flexWrap="wrap" mb={2}>
          <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
              عدد المتقدمين :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} className="number_label">
              {appliedWorkers.length}
            </Text>
          </Flex>
          <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
              عدد التعليقات :
            </SubTitle>
            <CardLabel
            className="number_label"
              py={1}
              px={2}
              ml={1}
              type={BODY}
              color={COLORS_VALUES[GREY_DARK]}
              bg={ticketStatusColor}
            >
              {comments.length}
            </CardLabel>
          </Flex>
        </Flex>
        {fieldRequirements && (
          <Flex className='CardBodyRow' flexDirection="row-reverse" flexWrap="wrap" mb={2}>
            <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                المطلوب انجازه :
              </SubTitle>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {fieldRequirements}
              </Text>
            </Flex>
          </Flex>
        )}
        {promoCode && (
          <Flex className='CardBodyRow' flexDirection="row-reverse" flexWrap="wrap" mb={2}>
            <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                البروموكود :
              </SubTitle>
              <CardLabel
                py={1}
                px={2}
                ml={1}
                type={BODY}
                color={COLORS_VALUES[GREY_DARK]}
                bg={ticketStatusColor}
              >
                {promoCode.code}
              </CardLabel>
            </Flex>
          </Flex>
        )}
        <Flex className='CardBodyRow' flexDirection="row-reverse" width={1} flexWrap="wrap" mb={2}>
          <Flex flexDirection="column">
            {(arrivalActivity.length > 0 || arrivalEstimationTime) && (
              <Flex flexDirection="row-reverse" flexWrap="wrap" mb={2}>
                {arrivalActivity.length > 0 && (
                  <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                    <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                      حالة الفني :
                    </SubTitle>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      {ARRIVAL_ACTIVITY[arrivalActivity[arrivalActivity.length - 1].status].ar}
                    </Text>
                  </Flex>
                )}
                {arrivalEstimationTime && (
                  <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                    <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                      وقت تقدير الوصول :
                    </SubTitle>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      {Math.round(arrivalEstimationTime.time / 60)}
                    </Text>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} mr={1}>
                      {TIME_UNITES.minute.ar}
                    </Text>
                  </Flex>
                )}
              </Flex>
            )}
            {(timeTakenToFinishWork || delayDuration) && (
              <Flex flexDirection="row-reverse" flexWrap="wrap" mb={2}>
                {timeTakenToFinishWork && (
                  <Flex flexDirection="row-reverse" py={1} px={2} mb={1} ml={3}>
                    <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                      عدد ساعات العمل :
                    </SubTitle>
                    <Text className="number_label"color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      {timeTakenToFinishWork.time} {TIME_UNITES[timeTakenToFinishWork.unit].ar}
                    </Text>
                    
                  </Flex>
                )}
                {delayDuration && (
                  <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                    <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                      تآخير الفني علي العميل :
                    </SubTitle>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                      {delayDuration.time}
                    </Text>
                    <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL} mr={1}>
                      {TIME_UNITES[delayDuration.unit].ar}
                    </Text>
                  </Flex>
                )}
              </Flex>
            )}
            <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                مشكلة :
              </SubTitle>
              <CardLabel
              className="number_label"
                py={1}
                px={2}
                ml={1}
                type={BODY}
                color={COLORS_VALUES[GREY_DARK]}
                bg={ticketStatusColor}
              >
                {ticketStatusName}
              </CardLabel>
              {ticket && (
                <>
                  <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                    - بواسطة :
                  </SubTitle>
                  <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                    {`${ticketActionUpdatedAt &&
                      ` [ ${ticketActionUpdatedAt &&
                        ticketActionUpdatedAt.toLocaleDateString('en-GB', {
                          year: 'numeric',
                          month: 'numeric',
                          day: 'numeric',
                        })} ] `}`}{' '}
                    {`${ticketActionMaker}`}
                  </Text>
                </>
              )}
            </Flex>
            {isRequestCancelledThenActivated &&
              get(activeStatusFromStatusesArray, 'createdBy.name') && (
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                    تم تنشيط الطلب بعد ما تم الغائه بواسطة:
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {get(activeStatusFromStatusesArray, 'createdBy.name')}
                </Text>
              </Flex>
            )}
            {cancellationReason && (
              <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={'bold'}>
                  سبب الالغاء :
                </SubTitle>
                <CardLabel
                  className="FullWidthLabel"
                  py={1}
                  px={2}
                  ml={1}
                  type={BODY}
                  color={COLORS_VALUES[GREY_DARK]}
                  bg={requestColor}
                >
                  {CANCELLATION_REASONS[cancellationReason]
                    ? CANCELLATION_REASONS[cancellationReason].ar
                    : cancellationReason}
                  {cancelledBy ? `-- ${cancelledBy.name}` : ''}
                  {cancellationReasonComment ? `- ${cancellationReasonComment}` : ''}
                </CardLabel>
              </Flex>
            )}
            <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={2}>
              <CardLabel
                py={1}
                px={2}
                type={BODY}
                color={COLORS_VALUES[GREY_DARK]}
                bg={requestColor}
              >
                {field !== FIELDS.reconditioning.en
                  ? REQUESTS_STATUSES[status].ar
                  : status === REQUESTS_STATUSES.pending.en.toLowerCase()
                    ? REQUESTS_STATUSES.contactYou.ar
                    : REQUESTS_STATUSES[status].ar}
              </CardLabel>
              <CardLabel
                py={1}
                px={2}
                mr={1}
                type={BODY}
                color={COLORS_VALUES[GREY_DARK]}
                bg={requestColor}
              >
                {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
              </CardLabel>
            </Flex>
            {(customerServiceFollowUps || operationsFollowUps) && (
              <Flex flexDirection="row-reverse" py={1} px={1} mt={1} mb={2}>
                {customerServiceFollowUps && (
                  <CardLabel
                    py={1}
                    px={2}
                    mr={1}
                    type={BODY}
                    color={COLORS_VALUES[GREY_DARK]}
                    bg={requestColor}
                  >
                    {customerServiceFollowUps}
                  </CardLabel>
                )}
                {operationsFollowUps && (
                  <CardLabel
                    py={1}
                    px={2}
                    mr={1}
                    type={BODY}
                    color={COLORS_VALUES[GREY_DARK]}
                    bg={requestColor}
                  >
                    {operationsFollowUps}
                  </CardLabel>
                )}
              </Flex>
            )}
            {isActivatedBySystem && (
              <Flex flexDirection="row-reverse" py={1} px={1} mt={1} mb={2}>
                <CardLabel
                  py={1}
                  px={2}
                  mr={1}
                  type={BODY}
                  color={COLORS_VALUES[GREY_DARK]}
                  bg={requestColor}
                  className="status_note"
                >
                  تم اختيار الفنى وتنشيط الطلب عن طريق السيستم
                </CardLabel>
              </Flex>
            )}
            {postponeTheRequestReason && (
              <Flex flexDirection="row-reverse" py={1} px={1} mt={1} mb={2}>
                <CardLabel
                  py={1}
                  px={2}
                  mr={1}
                  type={BODY}
                  color={COLORS_VALUES[GREY_DARK]}
                  bg={requestColor}
                >
                  {POSTPONE_REASONS[postponeTheRequestReason].ar}
                </CardLabel>
              </Flex>
            )}
          </Flex>

        </Flex>
        
        <Flex flexDirection="row-reverse" width={1} flexWrap="wrap" mb={2}>

        {!_.isEmpty(maintenanceListHistory) && (
  <Flex flexDirection="row-reverse" py={1} px={1} mt={1} mb={2} className='CardServices'>
  <span className='CardServicesTitle'>الخدمات</span>
 <table>
  <tbody>
  <tr><th>الخدمة</th><th>الكمية</th><th>التكلفة</th></tr>
 
              {maintenanceListHistory.map(item => {
                  const { cost, maintenanceCode, amount } = item;
                  const maintenance = WORK_LIST_FOR_EVERY_SERVICE.ar[field].find(e => e.code === (maintenanceCode)) || {};
                  return (
                    
                      <tr><td>{maintenance.name}</td><td>{amount.toLocaleString('ar-EG')}</td><td>{`${cost} ريال`}</td></tr>
                     
                  );
                })}
                </tbody>
            </table>
                        </Flex>
            )}



{(status === REQUESTS_STATUSES.done.en.toLowerCase() ||
            status === REQUESTS_STATUSES.reviewed.en.toLowerCase()) && (

              <Flex flexDirection="row-reverse" py={1} px={1} mt={1} mb={2} className='CardServices check'>
  <span className='CardServicesTitle'>الفاتورة</span>
 <table>
  <tbody>
  <tr><th>اجرة اليد</th><td>{workersWage}</td></tr>
  <tr><th>قطع الغيار</th><td>{partsCost}</td></tr>
  <tr><th>اجمالي التكلفة بعد الخصم</th><td>{totalAfterDiscount}</td></tr>
  </tbody>
  </table>
  </Flex>

            
          )}

        </Flex>
        
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="space-between" p={2} className='CardTools'>
        <Flex px={2}>
        {chatChannelUrl && 
        <>
        <ReactTooltip id={`sendbird_${_id}`} type="info" effect="solid">
                <span>عرض الشات</span>
              </ReactTooltip>
        <div className='chatIcon' data-for={`sendbird_${_id}`}
                  data-tip onClick={() => handleClickChatIcon(chatChannelUrl)}>
          <svg
      fill="#fff"
      viewBox="0 0 16 16"
      height="1em"
      width="1em"
    >
      <path d="M14 1a1 1 0 011 1v8a1 1 0 01-1 1H4.414A2 2 0 003 11.586l-2 2V2a1 1 0 011-1h12zM2 0a2 2 0 00-2 2v12.793a.5.5 0 00.854.353l2.853-2.853A1 1 0 014.414 12H14a2 2 0 002-2V2a2 2 0 00-2-2H2z" />
      <path d="M3 3.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 6a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 6zm0 2.5a.5.5 0 01.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5z" />
    </svg>
          </div></> }


          {isRequestCanBeCanceled && permissions && permissions.includes('Cancel Request') && (
            <>
              <ReactTooltip id={`deleteWorker_${_id}`} type="error" effect="solid">
                <span>{cancelRequestTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`deleteWorker_${_id}`}
                  data-tip
                  onClick={() => handleToggleAdminCancelRequestModal(index)}
                  icon={faBan}
                  color={COLORS_VALUES[LIGHT_RED]}
                  size="sm"
                />
              </Box>
            </>
          )}

          
          <ReactTooltip id={`requestOpsComment${_id}`} type="info" effect="solid">
            <span>{requestOpsCommentsTooltip}</span>
          </ReactTooltip>
          <Box mr={3}>
            <CallToActionIcon
              data-for={`requestOpsComment${_id}`}
              data-tip
              onClick={() => handleToggleOpsCommentsModal(index)}
              icon={faCommentDots}
              color={COLORS_VALUES[DISABLED]}
              size="sm"
            />
          </Box>

          <ReactTooltip id={`requestFollowsUp${_id}`} type="info" effect="solid">
            <span>{requestFollowsUpTooltip}</span>
          </ReactTooltip>
          <Box>
            <CallToActionIcon
              data-for={`requestFollowsUp${_id}`}
              data-tip
              onClick={() => handleToggleRequestFollowsUpModal(index)}
              icon={faRss}
              color={COLORS_VALUES[DISABLED]}
              size="sm"
            />
          </Box>
        </Flex>
        {permissions && permissions.includes('Get Requests By Its Pretty Id') && (
          <MoreDetailsLink
            to={`${SINGLE_REQUEST}/${requestPrettyId}`}
            href={`${SINGLE_REQUEST}/${requestPrettyId}`}
          >
            <Text
              ml={1}
              cursor="pointer"
              type={BODY}
              color={COLORS_VALUES[BRANDING_GREEN]}
              fontWeight={NORMAL}
            >
              {'عرض التفاصيل'}
            </Text>
          </MoreDetailsLink>
        )}
      </CardFooter>
    </Card>
  );
};
RequestCard.displayName = 'RequestCard';

RequestCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  request: PropTypes.shape({}).isRequired,
  showWorkerName: PropTypes.bool,
  showClientName: PropTypes.bool,
  handleToggleOpsCommentsModal: PropTypes.func,
  handleToggleRequestFollowsUpModal: PropTypes.func,
  handleToggleAdminCancelRequestModal: PropTypes.func,
  chatChannelUrl: PropTypes.shape({}),
  handleClickChatIcon: PropTypes.func,
};

RequestCard.defaultProps = {
  user: undefined,
  showWorkerName: true,
  showClientName: true,
  handleToggleOpsCommentsModal: () => {},
  handleToggleRequestFollowsUpModal: () => {},
  handleToggleAdminCancelRequestModal: () => {},
  chatChannelUrl: '',
  handleClickChatIcon: () => {},
};

export default RequestCard;
