import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import PromoCodeCard from 'components/PromoCodeCard';
import ConfirmModal from 'components/ConfirmModal';
import AddNewPromoCodeModal from 'components/AddNewPromoCodeModal';

const PromoCodes = props => {
  const {
    user,
    promoCodes,
    isConfirmModalOpen,
    isAddNewPromoCodeModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleDeletePromoCode,
    isDeletePromoCodeFetching,
    handleToggleEditPromoModal,
    isEditPromoModalOpen,
    promoDetails,
    promoIndex,
    isDeletePromoCodeError,
    isDeletePromoCodeErrorMessage,
  } = props;

  const promoCodesRenderer = promoCodes.map((promoCode, index) => {
    const { _id } = promoCode;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <PromoCodeCard
          m={2}
          index={index}
          user={user}
          promoCode={promoCode}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleDeletePromoCode={handleDeletePromoCode}
          handleToggleEditPromoModal={handleToggleEditPromoModal}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {promoCodesRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isFail={isDeletePromoCodeError}
          errorMessage={isDeletePromoCodeErrorMessage}
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isDeletePromoCodeFetching}
        />
      )}
      {isAddNewPromoCodeModalOpen && <AddNewPromoCodeModal />}
      {isEditPromoModalOpen && (
        <AddNewPromoCodeModal promoDetails={promoDetails} promoIndex={promoIndex} />
      )}
    </Flex>
  );
};
PromoCodes.displayName = 'PromoCodes';

PromoCodes.propTypes = {
  user: PropTypes.shape({}),
  promoCodes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  isConfirmModalOpen: PropTypes.bool,
  isAddNewPromoCodeModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleDeletePromoCode: PropTypes.func,
  isDeletePromoCodeFetching: PropTypes.bool,

  handleToggleEditPromoModal: PropTypes.func,
  isEditPromoModalOpen: PropTypes.bool,
  promoDetails: PropTypes.shape({}),
  promoIndex: PropTypes.number,
  isDeletePromoCodeError: PropTypes.bool,
  isDeletePromoCodeErrorMessage: PropTypes.string,
};

PromoCodes.defaultProps = {
  user: undefined,
  isConfirmModalOpen: false,
  isAddNewPromoCodeModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleDeletePromoCode: () => {},
  isDeletePromoCodeFetching: false,

  handleToggleEditPromoModal: () => {},
  isEditPromoModalOpen: false,
  promoDetails: undefined,
  promoIndex: false,
  isDeletePromoCodeError: false,
  isDeletePromoCodeErrorMessage: undefined,
};

export default PromoCodes;
