import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Checkbox, Label, Input } from '@rebass/forms';
import styled from 'styled-components';
import { isEmpty } from 'lodash';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import {
  toggleAddNewVendorModal,
  openEditVendorModal,
  addNewVendor,
  updateVendor,
} from 'redux-modules/vendors/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import DatePicker from 'react-datepicker/es';
import { space } from 'styled-system';
import 'react-datepicker/dist/react-datepicker.css';
import { CITIES } from 'utils/constants';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';

const { DISABLED, GREY_LIGHT, GREY_DARK, BRANDING_ORANGE } = COLORS;
const AddNewVendorFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const StyledDatePicker = styled(DatePicker)`
  ${space};
  background-color: ${props => props.backgroundColor};
  display: block;
  border: 1px solid;
  border-color: ${props => (props.isDanger ? COLORS_VALUES[COLORS.LIGHT_RED] : props.borderColor)};
  border-radius: ${props => props.theme.space[1]}px;
  color: ${props => COLORS_VALUES[props.color ? props.color : COLORS.DISABLED]};
  font-weight: ${props => props.theme.fontWeights.NORMAL};
  line-height: normal;
  outline: 0;
  width: calc(100% - 16px);
  font-size: 14px;
  direction: ltr;

  &:lang(ar) {
    padding: ${props => props.theme.space[2]}px;
    text-align: right;
  }

  &[type='number'] {
    -moz-appearance: textfield;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;

const SectionFlexContainer = styled(Flex)`
  .react-datepicker-popper {
    direction: ltr;
    &:lang(ar) {
      text-align: right;
      .react-datepicker__triangle {
        right: 50px;
        left: inherit;
      }
    }
  }
  .react-datepicker__input-container {
    display: block;
  }
  .react-datepicker__day--selected,
  .react-datepicker__day--in-selecting-range,
  .react-datepicker__day--in-range,
  .react-datepicker__month-text--selected,
  .react-datepicker__month-text--in-selecting-range,
  .react-datepicker__month-text--in-range {
    background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
    &:hover {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      opacity: 0.8;
    }
  }
  .react-datepicker__time-container,
  .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box {
    width: 120px;
  }
  .react-datepicker__time-container
    .react-datepicker__time
    .react-datepicker__time-box
    ul.react-datepicker__time-list
    li.react-datepicker__time-list-item {
    display: flex;
    justify-content: center;
    align-items: center;
    &.react-datepicker__time-list-item--selected {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      &:hover {
        background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
        opacity: 0.8;
      }
    }
  }
  .react-datepicker {
    font-family: 'english-font';
    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }
    .react-datepicker-time__header {
      color: ${COLORS_VALUES[GREY_DARK]};
      font-weight: normal;
      font-size: 0.8rem;
    }
  }
  .react-datepicker__close-icon {
    right: 80%;
  }
  .react-datepicker__close-icon::after {
    height: 13px;
    width: 15px;
  }
`;
const citiesKeys = Object.keys(CITIES);
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));

class AddNewVendorModal extends Component {
  static propTypes = {
    isAddVendorFetching: PropTypes.bool,
    isAddVendorError: PropTypes.bool,
    addVendorErrorMessage: PropTypes.string,
    addNewVendor: PropTypes.func,
    toggleAddNewVendorModal: PropTypes.func,
    isAddVendorModalOpen: PropTypes.bool,
    vendorDetails: PropTypes.shape({}),
    vendorIndex: PropTypes.number,
    updateVendor: PropTypes.func,
    openEditVendorModal: PropTypes.func,
  };

  static defaultProps = {
    isAddVendorFetching: false,
    isAddVendorError: false,
    addVendorErrorMessage: '',
    addNewVendor: () => {},
    toggleAddNewVendorModal: () => {},
    isAddVendorModalOpen: false,
    vendorDetails: undefined,
    vendorIndex: undefined,
    updateVendor: () => {},
    openEditVendorModal: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      governmentId: '',
      commissionPercentage: '',
      email: '',
      suspendedTo: '',
      expiredAt: '',
      active: true,
      suspendWorkers: false,
      contactNumber: '',
      username: '',
      city: '',
      file: null,
      enableStcPayment: true
    };
  }

  componentDidMount(): void {
    const { vendorDetails } = this.props;

    if (vendorDetails) {
      this.setState({
        name: vendorDetails.name || '',
        governmentId: vendorDetails.governmentId || '',
        commissionPercentage: vendorDetails.commissionPercentage || '',
        email: vendorDetails.email || '',
        suspendedTo: vendorDetails.suspendedTo || '',
        expiredAt: vendorDetails.expiredAt || '',
        active: vendorDetails.active,
        suspendWorkers: vendorDetails.suspendWorkers || false,
        contactNumber: vendorDetails.username || '',
        username: vendorDetails.username || '',
        city: vendorDetails.city
          ? citiesSelectOptions.filter(option => option.value === vendorDetails.city)[0]
          : '',
        enableStcPayment: vendorDetails.enableStcPayment,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleToDateInputChange = (value, type) => {
    const expireDateString = value
      ? value.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';
    this.setState({ [type]: expireDateString });
  };

  handleActiveCheckBoxChange = event => {
    this.setState({ active: event.target.checked });
  };

  handleStcPaymentCheckBoxChange = event => {
    this.setState({ enableStcPayment: event.target.checked });
  };

  handleFileChange = event => {
    this.setState({ file: event.target.files[0] });
  };

  handleSuspendWorkersCheckBoxChange = event => {
    this.setState({ suspendWorkers: event.target.checked });
  };

  handleChangeCitiesSelect = city => {
    this.setState({ city });
  };

  Submit = e => {
    e.preventDefault();
    const { vendorDetails } = this.props;

    const {
      addNewVendor: addNewVendorAction,
      updateVendor: updateVendorAction,
      vendorIndex,
    } = this.props;
    const {
      name,
      governmentId,
      email,
      commissionPercentage,
      suspendedTo,
      expiredAt,
      active,
      suspendWorkers,
      contactNumber,
      username,
      city,
      file,
      enableStcPayment
    } = this.state;

    const newVendorInfo = new FormData();
    newVendorInfo.append('file', file);
    newVendorInfo.append('name', name);
    newVendorInfo.append('governmentId', convertArabicNumbersToEnglish(governmentId.toString()));
    newVendorInfo.append(
      'commissionPercentage',
      convertArabicNumbersToEnglish(commissionPercentage.toString()),
    );
    newVendorInfo.append('email', email);
    newVendorInfo.append('suspendedTo', suspendedTo);
    newVendorInfo.append('expiredAt', expiredAt);
    newVendorInfo.append('active', active);
    newVendorInfo.append('suspendWorkers', suspendWorkers);
    newVendorInfo.append('contactNumber', convertArabicNumbersToEnglish(contactNumber.toString()));
    newVendorInfo.append('username', convertArabicNumbersToEnglish(username.toString()));
    newVendorInfo.append('city', city.value);
    newVendorInfo.append('enableStcPayment', enableStcPayment);

    if (vendorDetails) {
      const { _id } = vendorDetails;
      return updateVendorAction(newVendorInfo, _id, vendorIndex);
    }

    return addNewVendorAction(newVendorInfo);
  };

  render() {
    const {
      isAddVendorFetching,
      isAddVendorModalOpen,
      toggleAddNewVendorModal: toggleAddNewVendorModalAction,
      isAddVendorError,
      addVendorErrorMessage,
      vendorDetails,
      openEditVendorModal: openEditVendorModalAction,
    } = this.props;
    const {
      name,
      governmentId,
      email,
      commissionPercentage,
      suspendedTo,
      expiredAt,
      active,
      suspendWorkers,
      contactNumber,
      username,
      file,
      city,
      enableStcPayment
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addWorkerModalHeader = vendorDetails ? 'تعديل بيانات الشريك' : 'أضف شريك جديد';
    const submitButtonText = vendorDetails ? 'تعديل' : 'أضف';

    return (
      <Modal
        toggleModal={() =>
          vendorDetails ? openEditVendorModalAction() : toggleAddNewVendorModalAction()
        }
        header={addWorkerModalHeader}
        isOpened={isAddVendorModalOpen}
      >
        <AddNewVendorFormContainer onSubmit={this.Submit}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الاسم
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الاسم"
                  width={1}
                  value={name}
                  onChange={value => this.handleInputFieldChange('name', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الهوية/السجل
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الهوية/السجل"
                  width={1}
                  value={governmentId}
                  onChange={value => this.handleInputFieldChange('governmentId', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الجوال
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الاسم"
                  width={1}
                  value={username}
                  onChange={value => this.handleInputFieldChange('username', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم التواصل
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الهوية/السجل"
                  width={1}
                  value={contactNumber}
                  onChange={value => this.handleInputFieldChange('contactNumber', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  نسبة الشريك من اجمالى العمولة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="نسبة من شحن الفنين"
                  width={1}
                  value={commissionPercentage}
                  onChange={value => this.handleInputFieldChange('commissionPercentage', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الاميل
                </Text>
                <InputField
                  type="text"
                  placeholder="الإميل الالكترونى"
                  width={1}
                  value={email}
                  onChange={value => this.handleInputFieldChange('email', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدينة
                </Text>
                <StyledSelect
                  mb={3}
                  ml={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={city}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </SectionFlexContainer>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text width={1 / 2} mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ انتهاء العقد
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledDatePicker
                  width={1 / 2}
                  mb={1}
                  inline={false}
                  placeholderText="تاريخ انتهاء العقد"
                  dateFormat="MM/dd/yyyy"
                  borderColor={COLORS_VALUES[DISABLED]}
                  backgroundColor={COLORS_VALUES[GREY_LIGHT]}
                  selected={expiredAt ? new Date(expiredAt) : undefined}
                  onChange={value => this.handleToDateInputChange(value, 'expiredAt')}
                  isClearable
                />
              </SectionFlexContainer>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        تاريخ انتهاء ايقاف الشريك و ايقاف الفنين
                        <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                          *
                        </Text>
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={suspendWorkers}
                        onChange={this.handleSuspendWorkersCheckBoxChange}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
                <StyledDatePicker
                  mb={5}
                  inline={false}
                  placeholderText="تاريخ انتهاء وقف المستخدم"
                  dateFormat="MM/dd/yyyy"
                  borderColor={COLORS_VALUES[DISABLED]}
                  backgroundColor={COLORS_VALUES[GREY_LIGHT]}
                  selected={suspendedTo ? new Date(suspendedTo) : undefined}
                  onChange={value => this.handleToDateInputChange(value, 'suspendedTo')}
                  isClearable
                />
              </SectionFlexContainer>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        رفع صورة السجل التجاري
                        <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                          *
                        </Text>
                      </Text>
                    </Label>
                  </Box>
                </Text>
                <Input
                  ml={3}
                  type="file"
                  name="file"
                  onChange={this.handleFileChange}
                  sx={{ color: '#fcfffa' }}
                  placeholder="صورة السجل التجاري"
                />
              </SectionFlexContainer>
            </Flex>
            <Flex pb={3} px={4}>
              <Box>
                <Label>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    تنشيط الحساب
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <Checkbox
                    name="remember"
                    checked={active}
                    onChange={this.handleActiveCheckBoxChange}
                    sx={{ color: '#fcfffa' }}
                  />
                </Label>
              </Box>
            </Flex>

            <Flex pb={3} px={4}>
              <Box>
                <Label>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    التحويل الى STC
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <Checkbox
                    name="stcPayment"
                    checked={enableStcPayment}
                    onChange={this.handleStcPaymentCheckBoxChange}
                    sx={{ color: '#fcfffa' }}
                  />
                </Label>
              </Box>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.Submit}
                disabled={
                  isAddVendorFetching ||
                  name === '' ||
                  governmentId === '' ||
                  expiredAt === '' ||
                  expiredAt === null ||
                  active === '' ||
                  commissionPercentage === '' ||
                  (isEmpty(vendorDetails) && file === null)
                }
                isLoading={isAddVendorFetching}
              >
                {submitButtonText}
              </Button>
              {isAddVendorError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addVendorErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewVendorFormContainer>
      </Modal>
    );
  }
}

AddNewVendorModal.displayName = 'AddNewVendorModal';

const mapStateToProps = state => ({
  isAddVendorFetching: state.vendors.addVendor.isFetching,
  isAddVendorError:
    state.vendors.addVendor.isFail.isError || state.vendors.updateVendor.isFail.isError,
  addVendorErrorMessage:
    state.vendors.addVendor.isFail.message || state.vendors.updateVendor.isFail.message,
  isAddVendorModalOpen:
    state.vendors.addNewVendorModal.isOpen || state.vendors.editVendorModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewVendorModal,
      openEditVendorModal,
      addNewVendor,
      updateVendor,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewVendorModal);
