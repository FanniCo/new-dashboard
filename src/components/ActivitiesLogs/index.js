import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import ActivityLogCard from 'components/ActivityLogCard';

const ActivitiesLogs = props => {
  const { user, activitiesLogs } = props;

  const activitiesLogsRenderer = activitiesLogs.map((activityLog, index) => {
    const { _id } = activityLog;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <ActivityLogCard m={2} index={index} user={user} activityLog={activityLog} />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {activitiesLogsRenderer}
    </Flex>
  );
};
ActivitiesLogs.displayName = 'activitiesLogs';

ActivitiesLogs.propTypes = {
  user: PropTypes.shape({}),
  activitiesLogs: PropTypes.arrayOf(PropTypes.shape({})),
};

ActivitiesLogs.defaultProps = {
  user: undefined,
  activitiesLogs: [],
};

export default ActivitiesLogs;
