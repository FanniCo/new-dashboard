import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import ReviewCard from 'components/ReviewCard';
import ConfirmModal from 'components/ConfirmModal';

const Reviews = props => {
  const {
    user,
    reviews,
    // reviewIndex,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleToggleShowingReview,
    isToggleShowingReviewFetching,
    handleDeleteReview,
    isDeleteReviewFetching,
    isDeleteReviewError,
    isDeleteReviewErrorMessage,
    isToggleShowingReviewError,
    isToggleShowingReviewErrorMessage,
  } = props;

  const reviewsRenderer = reviews.map((review, index) => {
    const {
      review: { _id },
    } = review;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <ReviewCard
          m={2}
          index={index}
          user={user}
          review={review}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleToggleShowingReview={handleToggleShowingReview}
          isToggleShowingReviewFetching={isToggleShowingReviewFetching}
          handleDeleteReview={handleDeleteReview}
          isDeleteReviewFetching={isDeleteReviewFetching}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {reviewsRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isFail={isDeleteReviewError || isToggleShowingReviewError}
          errorMessage={isDeleteReviewErrorMessage || isToggleShowingReviewErrorMessage}
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isToggleShowingReviewFetching || isDeleteReviewFetching}
        />
      )}
    </Flex>
  );
};
Reviews.displayName = 'Reviews';

Reviews.propTypes = {
  user: PropTypes.shape({}),
  reviews: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  // reviewIndex: PropTypes.number,
  isConfirmModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleShowingReview: PropTypes.func,
  isToggleShowingReviewFetching: PropTypes.bool,
  handleDeleteReview: PropTypes.func,
  isDeleteReviewFetching: PropTypes.bool,
  isDeleteReviewError: PropTypes.bool,
  isDeleteReviewErrorMessage: PropTypes.string,
  isToggleShowingReviewError: PropTypes.bool,
  isToggleShowingReviewErrorMessage: PropTypes.string,
};

Reviews.defaultProps = {
  user: undefined,
  // reviewIndex: 0,
  isConfirmModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleShowingReview: () => {},
  isToggleShowingReviewFetching: false,
  handleDeleteReview: () => {},
  isDeleteReviewFetching: false,
  isDeleteReviewError: false,
  isDeleteReviewErrorMessage: undefined,
  isToggleShowingReviewError: false,
  isToggleShowingReviewErrorMessage: undefined,
};

export default Reviews;
