import React from 'react';
import { Flex } from '@rebass/grid';
import TransactionsClassificationDropdown from 'containers/shared/TransactionsClassificationDropdown';
import DeductionReasonsDropDown from 'containers/shared/DeductionReasonsDropDown';
import InputField from 'components/InputField';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import PropTypes from 'prop-types';
import Button from 'components/Buttons';
import Caution from 'components/Caution';

const AddCreditForm = props => {
  const {
    credits,
    memo,
    handleInputFieldChange,
    handleChangeTransactionsClassificationSelect,
    handleChangeDeductionReasonsSelect,
    isGetTransactionsForCustomer,
    handleClickSubmitButton,
    isError,
    errorMessage,
    isLoading,
    isDisabled,
    isHiddenDeduction,
    creditsLabelTest,
    transactionsClassificationLabelTest,
    DeductionReasonsLabel,
  } = props;
  const { BRANDING_GREEN, BRANDING_BLUE, LIGHT_RED, WHITE, ERROR } = COLORS;
  const { HEADING } = FONT_TYPES;

  return (
    <>
      <Flex pb={3} px={4}>
        <Flex flexDirection="column" width={1}>
          <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
            {creditsLabelTest}
          </Text>
          <InputField
            type="text"
            placeholder="الرصيد"
            width={1}
            value={credits}
            onChange={value => handleInputFieldChange('credits', value)}
            mb={2}
            autoComplete="on"
          />
        </Flex>
      </Flex>
      <Flex pb={3} px={4}>
        <Flex flexDirection="column" width={1}>
          <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
            {transactionsClassificationLabelTest}
          </Text>
          <TransactionsClassificationDropdown
            mb={3}
            isMulti={false}
            handleInputChange={value => handleChangeTransactionsClassificationSelect(value)}
            isGetTransactionsForCustomer={isGetTransactionsForCustomer}
          />
        </Flex>
      </Flex>
      <Flex pb={3} px={4} hidden={isHiddenDeduction}>
        <Flex flexDirection="column" width={1}>
          <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
            {DeductionReasonsLabel}
          </Text>
          <DeductionReasonsDropDown
            mb={3}
            isMulti={false}
            handleInputChange={value => handleChangeDeductionReasonsSelect(value)}
          />
        </Flex>
      </Flex>
      <Flex pb={3} px={4}>
        <Flex flexDirection="column" width={1}>
          <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
            تعليق
          </Text>
          <InputField
            type="textarea"
            placeholder="تعليق"
            width={1}
            value={memo}
            onChange={value => handleInputFieldChange('memo', value)}
            mb={2}
            autoComplete="on"
          />
        </Flex>
      </Flex>
      <Flex
        flexDirection="row-reverse"
        alignItems="center"
        justifyContent="space-between"
        pb={5}
        px={4}
      >
        <Button
          color={BRANDING_BLUE}
          onClick={handleClickSubmitButton}
          isLoading={isLoading}
          disabled={isDisabled}
          mx={1}
        >
          تاكيد
        </Button>
        {isError && (
          <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
            {errorMessage}
          </Caution>
        )}
      </Flex>
    </>
  );
};
AddCreditForm.displayName = 'AddCreditForm';

AddCreditForm.propTypes = {
  credits: PropTypes.string,
  memo: PropTypes.string,
  handleInputFieldChange: PropTypes.func,
  handleChangeTransactionsClassificationSelect: PropTypes.func,
  handleChangeDeductionReasonsSelect: PropTypes.func,
  isGetTransactionsForCustomer: PropTypes.bool,
  handleClickSubmitButton: PropTypes.func,
  isError: PropTypes.bool,
  errorMessage: PropTypes.string,
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isHiddenDeduction: PropTypes.bool,
  creditsLabelTest: PropTypes.string,
  transactionsClassificationLabelTest: PropTypes.string,
  DeductionReasonsLabel: PropTypes.string,
};

AddCreditForm.defaultProps = {
  credits: undefined,
  memo: undefined,
  handleInputFieldChange: () => {},
  handleChangeTransactionsClassificationSelect: () => {},
  handleChangeDeductionReasonsSelect: () => {},
  isGetTransactionsForCustomer: false,
  handleClickSubmitButton: () => {},
  isError: false,
  errorMessage: undefined,
  isLoading: false,
  isDisabled: false,
  isHiddenDeduction: true,
  creditsLabelTest: undefined,
  transactionsClassificationLabelTest: undefined,
  DeductionReasonsLabel: undefined,
};

export default AddCreditForm;
