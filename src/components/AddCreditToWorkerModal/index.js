import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isNaN } from 'lodash';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import { addCredit, toggleAddCreditModal } from 'redux-modules/workers/actions';
import Modal from 'components/Modal';
import AddCreditFrom from 'components/AddCreditFrom';

const AddCreditFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddCreditToWorkerModal extends Component {
  static propTypes = {
    workerIndex: PropTypes.number,
    workerDetails: PropTypes.shape({}),
    isAddCreditFetching: PropTypes.bool,
    isAddCreditError: PropTypes.bool,
    addCreditErrorMessage: PropTypes.string,
    addCredit: PropTypes.func,
    toggleAddCreditModal: PropTypes.func,
    isAddCreditModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    workerIndex: undefined,
    workerDetails: undefined,
    isAddCreditFetching: false,
    isAddCreditError: false,
    addCreditErrorMessage: '',
    addCredit: () => {},
    toggleAddCreditModal: () => {},
    isAddCreditModalOpen: false,
  };

  state = {
    credits: '',
    customTransactionClassificationCode: null,
    selectedDeductionReasons: null,
    memo: '',
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeTransactionsClassificationSelect = customTransactionClassificationCode => {
    this.setState({ customTransactionClassificationCode });
  };

  handleChangeDeductionReasonsSelect = selectedDeductionReasons => {
    this.setState({ selectedDeductionReasons });
  };

  onSubmitAddCreditForm = e => {
    const {
      workerIndex,
      workerDetails: { _id: workerId },
      addCredit: addCreditAction,
    } = this.props;
    const { credits, customTransactionClassificationCode, selectedDeductionReasons, memo } = this.state;
    const convertedCreditsNumbers = convertArabicNumbersToEnglish(credits);
    // const selectedDeductionReasonsValues = selectedDeductionReasons.map(reason =>{
    //   return reason.value
    // });

    e.preventDefault();

    const addCreditInfo = {
      workerId,
      worth: isNaN(Number(convertedCreditsNumbers))
        ? convertedCreditsNumbers
        : Number(convertedCreditsNumbers),
      memo,
      customTransactionClassificationCode: customTransactionClassificationCode.value,
      // deductionReason: selectedDeductionReasons.length > 1 
      //   ? selectedDeductionReasonsValues
      //   : selectedDeductionReasons[0].value,
      // deductionReason: selectedDeductionReasons.value
    };
    if(selectedDeductionReasons && credits < 0) {
      addCreditInfo.deductionReason = selectedDeductionReasons.value
    }

    addCreditAction(workerIndex, addCreditInfo);
  };

  render() {
    const {
      isAddCreditFetching,
      isAddCreditModalOpen,
      toggleAddCreditModal: toggleAddCreditModalAction,
      isAddCreditError,
      addCreditErrorMessage,
    } = this.props;
    const { credits, customTransactionClassificationCode, memo, selectedDeductionReasons } = this.state;
    const addCreditModalHeader = 'أضف رصيد';

    return (
      <Modal
        toggleModal={toggleAddCreditModalAction}
        header={addCreditModalHeader}
        isOpened={isAddCreditModalOpen}
      >
        <AddCreditFormContainer onSubmit={this.onSubmitAddCreditForm}>
          <Flex flexDirection="column">
            <AddCreditFrom
              credits={credits}
              memo={memo}
              handleInputFieldChange={this.handleInputFieldChange}
              handleChangeTransactionsClassificationSelect={
                this.handleChangeTransactionsClassificationSelect
              }
              handleChangeDeductionReasonsSelect={
                this.handleChangeDeductionReasonsSelect
              }
              handleClickSubmitButton={this.onSubmitAddCreditForm}
              isDisabled={
                isAddCreditFetching || credits === '' || !customTransactionClassificationCode || ( credits < 0 &&  !selectedDeductionReasons  )
              }
              isHiddenDeduction={credits > 0 || credits === ''}
              isLoading={isAddCreditFetching}
              isError={isAddCreditError}
              errorMessage={addCreditErrorMessage}
              creditsLabelTest="الرصيد"
              transactionsClassificationLabelTest="تصنيف العملية"
              DeductionReasonsLabel="أسباب الخصم"
            />
          </Flex>
        </AddCreditFormContainer>
      </Modal>
    );
  }
}
AddCreditToWorkerModal.displayName = 'AddCreditToWorkerModal';

const mapStateToProps = state => ({
  isAddCreditFetching: state.workers.addCredit.isFetching,
  isAddCreditError: state.workers.addCredit.isFail.isError,
  addCreditErrorMessage: state.workers.addCredit.isFail.message,
  isAddCreditModalOpen: state.workers.addCreditModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addCredit,
      toggleAddCreditModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddCreditToWorkerModal);
