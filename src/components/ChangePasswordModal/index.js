import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const ChangePasswordFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class ChangePasswordModal extends Component {
  static propTypes = {
    adminDetails: PropTypes.shape({}),
    isChangePasswordFetching: PropTypes.bool,
    isChangePasswordError: PropTypes.bool,
    changePasswordErrorMessage: PropTypes.string,
    handleToggleChangePasswordModal: PropTypes.func,
    isChangePasswordModalOpen: PropTypes.bool,
    onSubmitChangePasswordForm: PropTypes.func,
  };

  static defaultProps = {
    adminDetails: undefined,
    isChangePasswordFetching: false,
    isChangePasswordError: false,
    changePasswordErrorMessage: '',
    handleToggleChangePasswordModal: () => {},
    isChangePasswordModalOpen: false,
    onSubmitChangePasswordForm: () => {},
  };

  state = {
    password: '',
    confirmPassword: '',
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitForm = e => {
    e.preventDefault();
    const { onSubmitChangePasswordForm } = this.props;
    const { password, confirmPassword } = this.state;

    onSubmitChangePasswordForm(password, confirmPassword);
  };

  render() {
    const {
      isChangePasswordFetching,
      isChangePasswordError,
      handleToggleChangePasswordModal: toggleChangePasswordModalAction,
      isChangePasswordModalOpen,
      changePasswordErrorMessage,
    } = this.props;
    const { password, confirmPassword } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const changePasswordModalHeader = 'تغيير كلمة المرور';

    return (
      <Modal
        toggleModal={() => toggleChangePasswordModalAction({})}
        header={changePasswordModalHeader}
        isOpened={isChangePasswordModalOpen}
      >
        <ChangePasswordFormContainer onSubmit={this.onSubmitForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  كلمة المرور الجديدة
                </Text>
                <InputField
                  type="password"
                  placeholder="كلمة المرور الجديدة"
                  ref={inputField => {
                    this.newPasswordField = inputField;
                  }}
                  width={1}
                  value={password}
                  onChange={value => this.handleInputFieldChange('password', value)}
                  mb={2}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تأكيد كلمة المرور
                </Text>
                <InputField
                  type="password"
                  placeholder="تأكيد كلمة المرور"
                  ref={inputField => {
                    this.confirmPasswordField = inputField;
                  }}
                  width={1}
                  value={confirmPassword}
                  onChange={value => this.handleInputFieldChange('confirmPassword', value)}
                  mb={2}
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                disabled={isChangePasswordFetching || password === '' || confirmPassword === ''}
                isLoading={isChangePasswordFetching}
              >
                تغيير كلمة المرور
              </Button>
              {isChangePasswordError && (
                <Caution mx={2} isRtl bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {changePasswordErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </ChangePasswordFormContainer>
      </Modal>
    );
  }
}
ChangePasswordModal.displayName = 'ChangePasswordModal';
export default ChangePasswordModal;
