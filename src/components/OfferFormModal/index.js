import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import querystring from 'query-string';
import { faChevronLeft, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  toggleAddNewOfferModal,
  addNewOffer,
  updateOffer,
  openEditOfferModal,
} from 'redux-modules/offers/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import DatePicker from 'react-datepicker';
import { space } from 'styled-system';
import 'react-datepicker/dist/react-datepicker.css';
import { customSelectStyles, selectColors, StyledSelect, HyperLink } from 'components/shared';
import { FIELDS, ACTIVE_STATUS } from 'utils/constants';
import moment from 'moment';
import _ from 'lodash';
import { Input } from '@rebass/forms';

import { initializeApp, getApps, getApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage, ref, getDownloadURL, uploadBytes } from "firebase/storage";


const firebaseConfig = {
  apiKey: "AIzaSyCr6qJuyOgqLtNjcbVNV09x7eMdgYPCCZI",
  authDomain: "fanniapp-bdcf4.firebaseapp.com",
  databaseURL: "https://fanniapp-bdcf4.firebaseio.com",
  projectId: "fanniapp-bdcf4",
  storageBucket: "fanniapp-bdcf4.appspot.com",
  messagingSenderId: "591978559350",
  appId: "1:591978559350:web:1ce628a34f84beb41c6c57",
  measurementId: "G-8NZWXSXHQS"
};

const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();

const analytics = getAnalytics(app);

const storage = getStorage(app);




const activeStatusKeys = Object.keys(ACTIVE_STATUS);
const activeStatusSelectOptions = activeStatusKeys.map(key => ({
  value: key,
  label: ACTIVE_STATUS[key].ar,
}));

const { GREY_DARK, BRANDING_ORANGE, DISABLED, GREY_LIGHT } = COLORS;

const SectionFlexContainer = styled(Flex)`
  .react-datepicker-popper {
    direction: ltr;
    &:lang(ar) {
      text-align: right;
      .react-datepicker__triangle {
        right: 50px;
        left: inherit;
      }
    }
  }
  .react-datepicker__input-container {
    display: block;
  }
  .react-datepicker__day--selected,
  .react-datepicker__day--in-selecting-range,
  .react-datepicker__day--in-range,
  .react-datepicker__month-text--selected,
  .react-datepicker__month-text--in-selecting-range,
  .react-datepicker__month-text--in-range {
    background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
    &:hover {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      opacity: 0.8;
    }
  }
  .react-datepicker__time-container,
  .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box {
    width: 120px;
  }
  .react-datepicker__time-container
    .react-datepicker__time
    .react-datepicker__time-box
    ul.react-datepicker__time-list
    li.react-datepicker__time-list-item {
    display: flex;
    justify-content: center;
    align-items: center;
    &.react-datepicker__time-list-item--selected {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      &:hover {
        background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
        opacity: 0.8;
      }
    }
  }
  .react-datepicker {
    font-family: 'english-font';
    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }
    .react-datepicker-time__header {
      color: ${COLORS_VALUES[GREY_DARK]};
      font-weight: normal;
      font-size: 0.8rem;
    }
  }
  .react-datepicker__close-icon {
    right: 80%;
  }
  .react-datepicker__close-icon::after {
    height: 13px;
    width: 15px;
  }
`;

const StyledDatePicker = styled(DatePicker)`
  ${space};
  background-color: ${props => props.backgroundColor};
  display: block;
  border: 1px solid;
  border-color: ${props => (props.isDanger ? COLORS_VALUES[COLORS.LIGHT_RED] : props.borderColor)};
  border-radius: ${props => props.theme.space[1]}px;
  color: ${props => COLORS_VALUES[props.color ? props.color : COLORS.DISABLED]};
  font-weight: ${props => props.theme.fontWeights.NORMAL};
  line-height: normal;
  outline: 0;
  width: calc(100% - 16px);
  font-size: 14px;
  direction: ltr;

  &:lang(ar) {
    padding: ${props => props.theme.space[2]}px;
    text-align: right;
  }

  &[type='number'] {
    -moz-appearance: textfield;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;

const AddNewOfferFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const imageStyle = {
  width: '200px',
  height: '150px',
  padding: '5px',
  display: 'inherit',
};



const fieldsKeys = Object.keys(FIELDS);
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
fieldsSelectOptions.unshift({
  value: 'ALL_FIELDS',
  label: 'كل التخصصات',
});
class AddNewOfferModal extends Component {
  static propTypes = {
    isAddOfferFetching: PropTypes.bool,
    isAddOfferError: PropTypes.bool,
    isEditOfferError: PropTypes.bool,
    addOfferErrorMessage: PropTypes.string,
    editOfferErrorMessage: PropTypes.string,
    addNewOffer: PropTypes.func,
    toggleAddNewOfferModal: PropTypes.func,
    openEditOfferModal: PropTypes.func,
    updateOffer: PropTypes.func,
    isAddOfferModalOpen: PropTypes.bool,
    offerDetails: PropTypes.shape({}),
    offerIndex: PropTypes.number,
    isEditOfferFetching: PropTypes.bool,
  };

  static defaultProps = {
    isAddOfferFetching: false,
    isAddOfferError: false,
    isEditOfferError: false,
    addOfferErrorMessage: '',
    editOfferErrorMessage: '',
    addNewOffer: () => {},
    toggleAddNewOfferModal: () => {},
    openEditOfferModal: () => {},
    updateOffer: () => {},
    isAddOfferModalOpen: false,
    offerDetails: undefined,
    offerIndex: undefined,
    isEditOfferFetching: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      titleAr: '',
      titleEn: '',
      bodyAr: '',
      bodyEn: '',
      fields: [],
      expireAt: '',
      isActive: activeStatusSelectOptions[0],
      originalStatus: true,
      url: process.env.REACT_APP_OFFERS_URL || '',
      promoCode: '',
      imageUrl: '',
    };
  }

  componentDidMount(): void {
    const { offerDetails } = this.props;

    if (offerDetails) {
      const parsedUrl = querystring.parseUrl(offerDetails.url || '');
      const fields = parsedUrl.query.fields || [];

      this.setState({
        titleAr: offerDetails.title ? offerDetails.title.ar : '',
        titleEn: offerDetails.title ? offerDetails.title.en : '',
        bodyAr: offerDetails.body ? offerDetails.body.ar : '',
        bodyEn: offerDetails.body ? offerDetails.body.en : '',
        fields: fieldsSelectOptions.filter(field => fields.includes(field.value)),
        expireAt: offerDetails.expireAt,
        originalStatus: offerDetails.isActive,
        isActive: offerDetails.isActive ? activeStatusSelectOptions[0] : activeStatusSelectOptions[1],
        url: parsedUrl.url,
        promoCode: parsedUrl.query.promoCode || '',
        imageUrl: offerDetails.imageUrl || '',
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitAddOfferForm = e => {
    e.preventDefault();

    const {
      addNewOffer: addNewOfferAction,
      updateOffer: updateOfferAction,
      offerDetails,
      offerIndex,
    } = this.props;
    const { titleAr, titleEn, bodyAr, bodyEn, expireAt, originalStatus, isActive, promoCode, fields, imageUrl } = this.state;
    let { url } = this.state;

    url = `https://fanni.sa/services?${querystring.stringify({
      fields: fields.map(f => f.value).join(','),
      promoCode,
    })}`;



    


    const newOfferInfo = {
      title: {
        ar: titleAr,
        en: titleEn,
      },
      body: {
        ar: bodyAr,
        en: bodyEn,
      },
      promoCode,
      url,
      imageUrl,
    };

    newOfferInfo.isActive = isActive? JSON.parse(isActive.value) : '';


    let currentActiveStatus = JSON.parse(newOfferInfo.isActive);

    if(currentActiveStatus != originalStatus) {
      this.setState({ originalStatus, currentActiveStatus });
      if(currentActiveStatus == true) {
        newOfferInfo.expireAt =  moment().locale('en').add(1, 'years').format('YYYY-MM-DD');
      } else {
        newOfferInfo.expireAt =  moment().locale('en').subtract(1, 'days').format('YYYY-MM-DD');
      }
    } else {
      newOfferInfo.expireAt = moment(expireAt).locale('en').format('YYYY-MM-DD');
    }

    if (offerDetails) {
      const { _id } = offerDetails;
      return updateOfferAction(newOfferInfo, _id, offerIndex);
    }

    return addNewOfferAction(newOfferInfo);
  };

  handleToDateInputChange = (value, type) => {
    this.setState({ [type]: value });
  };

  handleChangeFieldSelect = fields => {
    const isAllFieldsSelected = fields.some(f => f.value === 'ALL_FIELDS');

    if (isAllFieldsSelected) {
      const AllFields = fieldsSelectOptions.filter(field => field.value !== 'ALL_FIELDS');

      return this.setState({ fields: AllFields });
    }

    return this.setState({ fields });
  };

  handleChangeActiveStatusSelect = isActive => {
    this.setState({ isActive }, () => {
      this.setState({ isActive: isActive });
    });
  };

  
  handleFileChange = (e, type) => {

    const file = e.target.files[0];
    
    const time = Date.parse(moment().locale('en').format('YYYY-MM-DD HH:mm:ss')) / 1000;

    uploadBytes(ref(storage, 'OffersImages/'+file.name+'_'+time+'.jpg'), file).then((snapshot) => {
      getDownloadURL(snapshot.ref).then((url) => {
        this.setState({
          imageUrl: url
        });
      });
    });

  };

  render() {
    const {
      isAddOfferFetching,
      isAddOfferModalOpen,
      toggleAddNewOfferModal: toggleAddNewOfferModalAction,
      openEditOfferModal: openEditOfferModalAction,
      isAddOfferError,
      isEditOfferError,
      addOfferErrorMessage,
      editOfferErrorMessage,
      offerDetails,
      isEditOfferFetching,
    } = this.props;
    const { titleAr, titleEn, bodyAr, bodyEn, fields, expireAt, isActive, promoCode, imageUrl } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addCustomerModalHeader = offerDetails ? 'تعديل العرض' : 'أضف عرض جديد';
    const submitButtonText = offerDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          offerDetails ? openEditOfferModalAction() : toggleAddNewOfferModalAction()
        }
        header={addCustomerModalHeader}
        isOpened={isAddOfferModalOpen}
      >
        <AddNewOfferFormContainer onSubmit={this.onSubmitAddOfferForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى العنوان (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان عربى"
                  width={1}
                  value={titleAr}
                  onChange={value => this.handleInputFieldChange('titleAr', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتوى العنوان(بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان انجليزى"
                  width={1}
                  value={titleEn}
                  onChange={value => this.handleInputFieldChange('titleEn', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="الوصف عربى"
                  width={1}
                  value={bodyAr}
                  onChange={value => this.handleInputFieldChange('bodyAr', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="الوصف انجليزى"
                  width={1}
                  value={bodyEn}
                  onChange={value => this.handleInputFieldChange('bodyEn', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti
                  isRtl
                  backspaceRemovesValue={false}
                  value={fields}
                  onChange={this.handleChangeFieldSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  بروموكود
                </Text>
                <InputField
                  type="text"
                  placeholder="بروموكود"
                  width={1}
                  value={promoCode}
                  onChange={value => this.handleInputFieldChange('promoCode', value)}
                  mb={2}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text width={1 / 2} mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ انتهاء العرض
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledDatePicker
                  width={1 / 2}
                  mb={1}
                  inline={false}
                  placeholderText="تاريخ انتهاء العرض"
                  dateFormat="MM/dd/yyyy"
                  borderColor={COLORS_VALUES[DISABLED]}
                  backgroundColor={COLORS_VALUES[GREY_LIGHT]}
                  selected={expireAt ? new Date(expireAt) : undefined}
                  onChange={value => this.handleToDateInputChange(value, 'expireAt')}
                  isClearable
                />
              </SectionFlexContainer>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                {imageUrl && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrl')} />
                    <HyperLink href={imageUrl} target="_blank">
                      <img style={imageStyle} src={imageUrl} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  االعنوان الالكترونى للصورة 
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="الصوره"
                  name="fileAr"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrl');
                  }}
                  // key={imageUrlArKey}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الحالة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
        mb={3}
        placeholder="الحالة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        backspaceRemovesValue={false}
        value={isActive}
        options={activeStatusSelectOptions}
        onChange={this.handleChangeActiveStatusSelect}
        theme={selectColors}
        styles={customSelectStyles}
      />
                </SectionFlexContainer>
              </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                disabled={
                  isAddOfferFetching ||
                  titleAr === '' ||
                  titleEn === '' ||
                  bodyAr === '' ||
                  bodyEn === '' ||
                  fields === '' ||
                  expireAt === ''
                }
                isLoading={isAddOfferFetching || isEditOfferFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddOfferError || isEditOfferError) && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addOfferErrorMessage || editOfferErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewOfferFormContainer>
      </Modal>
    );
  }
}

AddNewOfferModal.displayName = 'AddNewOfferModal';

const mapStateToProps = state => ({
  isAddOfferFetching: state.offers.addOffer.isFetching,
  isEditOfferFetching: state.offers.updateOffer.isFetching,
  isAddOfferError: state.offers.addOffer.isFail.isError,
  addOfferErrorMessage: state.offers.addOffer.isFail.message,
  isEditOfferError: state.offers.updateOffer.isFail.isError,
  editOfferErrorMessage: state.offers.updateOffer.isFail.message,
  isAddOfferModalOpen: state.offers.addNewOfferModal.isOpen || state.offers.editOfferModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewOfferModal,
      addNewOffer,
      updateOffer,
      openEditOfferModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewOfferModal);
