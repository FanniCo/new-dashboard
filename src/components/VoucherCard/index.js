import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Card from 'components/Card';
import Text from 'components/Text';
import { CallToActionIcon, CardBody, CardFooter, CardLabel, SubTitle } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CITIES } from 'utils/constants';
import { getFieldIcon } from 'utils/shared/style';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const { GREY_LIGHT, DISABLED, BRANDING_GREEN, BRANDING_BLUE, LIGHT_RED } = COLORS;
const { CAPTION, BODY, SUPER_TITLE } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const VoucherCard = props => {
  const {
    index,
    user: { permissions },
    voucher,
    voucher: {
      _id,
      code,
      cities,
      value,
      isForNewCustomer,
      forOneCustomerToUse,
      timesPerCustomerCanUse,
      forSpecificCustomers,
      expireAt,
      createdAt,
    },
    handleToggleConfirmModal,
    handleDeleteVoucher,
    handleToggleEditVoucherModal,
  } = props;

  const isShowCardFooter =
    permissions &&
    (permissions.includes('Update Voucher') || permissions.includes('Delete Voucher'));
  const voucherDate = new Date(createdAt);
  const expireDate = new Date(expireAt);
  const deleteVoucherTooltip = 'مسح الفوتشر';
  const editVoucherTooltip = 'تعديل الفوتشر';
  const deleteVoucherConfirmModalHeader = 'تأكيد مسح الفوتشر';
  const deleteVoucherConfirmModalConfirmText = 'هل أنت متأكد أنك تريد مسح الفوتشر؟';
  const deleteVoucherConfirmModalFunction = () => {
    handleDeleteVoucher(_id, index);
  };

  const customersContent =
    forSpecificCustomers &&
    forSpecificCustomers.map(customer => (
      <Flex alignItems="center" justifyContent="flex-end" key={customer}>
        <FontAwesomeIcon
          icon={getFieldIcon(customer)}
          color={COLORS_VALUES[BRANDING_GREEN]}
          size="sm"
        />
        <CardLabel
          py={1}
          px={2}
          mx={2}
          type={CAPTION}
          border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          customer.name
        </CardLabel>
      </Flex>
    ));

  const citiesContent =
    cities &&
    cities.map(city => (
      <CardLabel
        key={city}
        py={1}
        px={2}
        ml={1}
        mb={1}
        type={BODY}
        color={COLORS_VALUES[DISABLED]}
        bg={BRANDING_BLUE}
      >
        {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
      </CardLabel>
    ));

  return (
    <Card
      {...props}
      height="calc(100% - 16px)"
      minHeight={100}
      flexDirection="column"
      justifyContent="space-between"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody
        py={3}
        px={1}
        justifyContent="space-between"
        flexDirection={['column', 'row', 'row', 'row', 'row']}
      >
        <Flex ml={1}>
          <Flex
            width={1}
            mb={[3, 0, 0, 0, 0]}
            flexWrap={['wrap', 'initial', 'initial', 'initial', 'initial']}
            flexDirection={['row-reverse', 'column', 'column', 'column', 'column']}
          >
            {customersContent}
          </Flex>
        </Flex>
        <Flex flexDirection="column" justifyContent="space-between">
          <Flex alignItems="center" flexDirection="row-reverse" pb={2} px={2} width={1}>
            <Flex flexDirection="row" alignItems="center">
              <Text type={SUPER_TITLE} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                {`[ ${voucherDate.toLocaleDateString('en-US', {
                  year: 'numeric',
                  month: 'numeric',
                  day: 'numeric',
                  hour12: true,
                })} ] - ${code}`}
              </Text>
            </Flex>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              القيمه:
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {value}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              ينتهي في :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {expireDate.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour12: true,
              })}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد المرات لكل عميل :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {timesPerCustomerCanUse}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              هل مخصص للعملاء الجداد :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {isForNewCustomer == 'true' ? 'نعم' : 'لا'}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              هل مخصص لعميل واحد فقط :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {forOneCustomerToUse == 'true' ? 'نعم' : 'لا'}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" flexWrap="wrap" pt={1} px={2} mt={1}>
            {citiesContent}
          </Flex>
        </Flex>
      </CardBody>
      {isShowCardFooter && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
            {permissions && permissions.includes('Delete Promo Code') && (
              <>
                <ReactTooltip id={`deleteWorker_${_id}`} type="error" effect="solid">
                  <span>{deleteVoucherTooltip}</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`deleteWorker_${_id}`}
                    data-tip
                    onClick={() =>
                      handleToggleConfirmModal(
                        deleteVoucherConfirmModalHeader,
                        deleteVoucherConfirmModalConfirmText,
                        deleteVoucherConfirmModalFunction,
                      )
                    }
                    icon={faTrashAlt}
                    color={COLORS_VALUES[LIGHT_RED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
            {permissions && permissions.includes('Update Promo Code') && (
              <>
                <ReactTooltip id={`editVoucher_${_id}`} type="light" effect="solid">
                  <span>{editVoucherTooltip}</span>
                </ReactTooltip>
                <Box>
                  <CallToActionIcon
                    data-for={`editVoucher_${_id}`}
                    data-tip
                    onClick={() => handleToggleEditVoucherModal(index, voucher)}
                    icon={faPen}
                    color={COLORS_VALUES[DISABLED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
          </Flex>
        </CardFooter>
      )}
    </Card>
  );
};

VoucherCard.displayName = 'VoucherCard';

VoucherCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  voucher: PropTypes.shape({}).isRequired,
  handleDeleteVoucher: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
};

VoucherCard.defaultProps = {
  user: undefined,
  handleDeleteVoucher: () => {},
  handleToggleConfirmModal: () => {},
};

export default VoucherCard;
