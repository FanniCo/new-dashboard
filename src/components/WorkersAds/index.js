import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import WorkersAdCard from 'components/WorkersAdCard';
import WorkerAdFormModal from 'components/WorkerAdFormModal';
import ConfirmModal from 'components/ConfirmModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { FONT_TYPES } from 'components/theme/fonts';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class WorkersAds extends Component {
  getLoadingWorkersAds = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingWorkersAdsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingWorkersAds(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      workersAdsList,
      isAddNewWorkersAdModalOpen,
      isConfirmModalOpen,
      isDeleteWorkersAdFetching,
      deleteWorkersAdAction,
      handleToggleConfirmModal,
      isEditWorkersAdModalOpen,
      handleToggleEditWorkersAdModal,
      workersAdDetails,
      workersAdIndex,
      isGetAllWorkersAdsFetching,
      isGetAllWorkersAdsSuccess,
      isGetAllWorkersAdsError,
      isGetAllWorkersAdsErrorMessage,
      applyNewFilter,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isDeleteWorkersAdError,
      isDeleteWorkersAdErrorMessage,
    } = this.props;

    let workersAdsRenderer;

    if (
      (isGetAllWorkersAdsFetching && (!workersAdsList || applyNewFilter)) ||
      (!isGetAllWorkersAdsFetching && !isGetAllWorkersAdsSuccess && !isGetAllWorkersAdsError)
    ) {
      workersAdsRenderer = this.createLoadingWorkersAdsList();
    } else if (isGetAllWorkersAdsError) {
      workersAdsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllWorkersAdsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!workersAdsList.length) {
      workersAdsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (workersAdsList.length) {
      workersAdsRenderer = workersAdsList.map((workersAd, index) => {
        const { _id } = workersAd;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <WorkersAdCard
              m={2}
              index={index}
              user={user}
              workersAd={workersAd}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditWorkersAdModal={handleToggleEditWorkersAdModal}
              deleteWorkersAdAction={deleteWorkersAdAction}
            />
          </Box>
        );
      });
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {workersAdsRenderer}
        {isAddNewWorkersAdModalOpen && <WorkerAdFormModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            isFail={isDeleteWorkersAdError}
            errorMessage={isDeleteWorkersAdErrorMessage}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={confirmModalFunction}
            isConfirming={isDeleteWorkersAdFetching}
          />
        )}
        {isEditWorkersAdModalOpen && (
          <WorkerAdFormModal workersAdDetails={workersAdDetails} workersAdIndex={workersAdIndex} />
        )}
      </Flex>
    );
  }
}

WorkersAds.displayName = 'WorkersAdsList';

WorkersAds.propTypes = {
  user: PropTypes.shape({}),
  workersAdsList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewWorkersAdModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  deleteWorkersAdAction: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  // deleteWorkers: PropTypes.func,
  isDeleteWorkersAdFetching: PropTypes.bool,
  handleToggleEditWorkersAdModal: PropTypes.func,
  isEditWorkersAdModalOpen: PropTypes.bool,
  workersAdDetails: PropTypes.shape({}),
  workersAdIndex: PropTypes.number,
  isGetAllWorkersAdsFetching: PropTypes.bool,
  isGetAllWorkersAdsSuccess: PropTypes.bool,
  isGetAllWorkersAdsError: PropTypes.bool,
  isGetAllWorkersAdsErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  isDeleteWorkersAdError: PropTypes.bool,
  isDeleteWorkersAdErrorMessage: PropTypes.string,
};

WorkersAds.defaultProps = {
  user: undefined,
  workersAdsList: undefined,
  isAddNewWorkersAdModalOpen: false,
  handleToggleConfirmModal: () => {},
  deleteWorkersAdAction: () => {},
  isConfirmModalOpen: false,
  // deleteWorkers: () => {},
  isDeleteWorkersAdFetching: false,
  handleToggleEditWorkersAdModal: () => {},
  isEditWorkersAdModalOpen: false,
  workersAdDetails: {},
  workersAdIndex: undefined,
  isGetAllWorkersAdsFetching: false,
  isGetAllWorkersAdsSuccess: false,
  isGetAllWorkersAdsError: false,
  isGetAllWorkersAdsErrorMessage: undefined,
  applyNewFilter: false,
  confirmModalHeader: undefined,
  confirmModalText: undefined,
  confirmModalFunction: () => {},
  isDeleteWorkersAdError: false,
  isDeleteWorkersAdErrorMessage: undefined,
};

export default WorkersAds;
