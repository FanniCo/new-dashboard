import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import moment from 'moment';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import {
  addRechargeAward,
  updateRechargeAward,
  toggleAddNewRechargeAwardModal,
  openEditRechargeAwardModal,
} from 'redux-modules/rechargeAwards/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import DatePicker from 'components/Filter/DatePicker';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import {
  CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES,
  DISCOUNT_TYPES,
} from 'utils/constants';

const awardTypeSelectOptions = [DISCOUNT_TYPES[0], DISCOUNT_TYPES[1]].map(type => ({
  value: type.value,
  label: type.name.ar,
}));

const workerRechargeMethodsSelectOptions = CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES.reCharge.map(
  reChargeMethod => ({
    value: Number(reChargeMethod.code),
    label: reChargeMethod.name.ar,
  }),
);

const AddNewRechargeAwardFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddNewRechargeAwardModal extends Component {
  static propTypes = {
    isAddRechargeAwardFetching: PropTypes.bool,
    isAddRechargeAwardError: PropTypes.bool,
    addRechargeAwardErrorMessage: PropTypes.string,
    addRechargeAward: PropTypes.func,
    updateRechargeAward: PropTypes.func,
    toggleAddNewRechargeAwardModal: PropTypes.func,
    isAddRechargeAwardModalOpen: PropTypes.bool,
    isEditRechargeAwardFetching: PropTypes.bool,
    rechargeAwardDetails: PropTypes.shape({}),
    rechargeAwardIndex: PropTypes.number,
    openEditRechargeAwardModal: PropTypes.func,
    isUpdateRechargeAwardError: PropTypes.bool,
    updateRechargeAwardErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    isAddRechargeAwardFetching: false,
    isAddRechargeAwardError: false,
    addRechargeAwardErrorMessage: '',
    addRechargeAward: () => {},
    updateRechargeAward: () => {},
    toggleAddNewRechargeAwardModal: () => {},
    isAddRechargeAwardModalOpen: false,
    isEditRechargeAwardFetching: false,
    rechargeAwardDetails: undefined,
    rechargeAwardIndex: 0,
    openEditRechargeAwardModal: () => {},
    isUpdateRechargeAwardError: false,
    updateRechargeAwardErrorMessage: undefined,
  };

  constructor(props) {
    super(props);

    this.state = {
      awardValue: 0,
      awardType: null,
      minimumRechargeValue: 0,
      maximumRechargeValue: undefined,
      expireAt: null,
      workerRechargeMethods: [],
    };
  }

  componentDidMount() {
    const { rechargeAwardDetails } = this.props;

    if (rechargeAwardDetails) {
      this.setState({
        awardValue: rechargeAwardDetails.awardValue.toString(),
        awardType: awardTypeSelectOptions.find(
          option => option.value === rechargeAwardDetails.awardType,
        ),
        workerRechargeMethods: workerRechargeMethodsSelectOptions.filter(options => rechargeAwardDetails.workerRechargeMethods.includes(options.value)),
        maximumRechargeValue: rechargeAwardDetails.maximumRechargeValue
          ? rechargeAwardDetails.maximumRechargeValue.toString()
          : '',
        minimumRechargeValue: rechargeAwardDetails.minimumRechargeValue
          ? rechargeAwardDetails.minimumRechargeValue.toString()
          : '',
        expireAt: moment(rechargeAwardDetails.expireAt).format('YYYY-MM-DD'),
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleExpireDateInputChange = expireAt => {
    const expireDateString = expireAt
      ? expireAt.toLocaleDateString('en-US', {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
        })
      : '';

    this.setState({ expireAt: expireDateString });
  };

  handleChangeAwardTypeSelect = awardType => {
    this.setState({ awardType });
  };

  handleChangeWorkerRechargeMethodsSelect = workerRechargeMethods => {
    this.setState({ workerRechargeMethods });
  };

  onSubmitAddRechargeAwardForm = e => {
    e.preventDefault();

    const {
      addRechargeAward: addRechargeAwardAction,
      updateRechargeAward: updateRechargeAwardAction,
      rechargeAwardDetails,
      rechargeAwardIndex,
    } = this.props;
    const {
      awardValue,
      awardType,
      minimumRechargeValue,
      maximumRechargeValue,
      expireAt,
      workerRechargeMethods,
    } = this.state;

    const newRechargeAwardInfo = {
      awardValue,
      awardType: awardType.value,
      workerRechargeMethods: workerRechargeMethods.map(method => Number(method.value)),
      minimumRechargeValue: Number(convertArabicNumbersToEnglish(minimumRechargeValue)),
      maximumRechargeValue: maximumRechargeValue
        ? Number(convertArabicNumbersToEnglish(maximumRechargeValue))
        : null,
      expireAt,
    };

    if (rechargeAwardDetails) {
      const { _id } = rechargeAwardDetails;
      return updateRechargeAwardAction(newRechargeAwardInfo, _id, rechargeAwardIndex);
    }

    addRechargeAwardAction(newRechargeAwardInfo);
  };

  render() {
    const {
      isAddRechargeAwardFetching,
      isAddRechargeAwardModalOpen,
      toggleAddNewRechargeAwardModal: toggleAddNewRechargeAwardModalAction,
      openEditRechargeAwardModal: openEditRechargeAwardModalAction,
      isAddRechargeAwardError,
      addRechargeAwardErrorMessage,
      rechargeAwardDetails,
      rechargeAwardIndex,
      isEditRechargeAwardFetching,
      isUpdateRechargeAwardError,
      updateRechargeAwardErrorMessage,
    } = this.props;
    const {
      awardValue,
      awardType,
      minimumRechargeValue,
      maximumRechargeValue,
      expireAt,
      workerRechargeMethods,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addRechargeAwardModalHeader = rechargeAwardDetails
      ? 'تعديل مكافآة شحن الفنيين'
      : 'أضف مكافآة شحن الفنيين';
    const submitButtonText = rechargeAwardDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          rechargeAwardDetails
            ? openEditRechargeAwardModalAction(rechargeAwardIndex, rechargeAwardDetails)
            : toggleAddNewRechargeAwardModalAction()
        }
        header={addRechargeAwardModalHeader}
        isOpened={isAddRechargeAwardModalOpen}
      >
        <AddNewRechargeAwardFormContainer onSubmit={this.onSubmitAddRechargeAwardForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الحد الأدني للشحن
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الحد الأدني للشحن"
                  width={1}
                  value={minimumRechargeValue}
                  onChange={value => this.handleInputFieldChange('minimumRechargeValue', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الحد الأعلي للشحن
                </Text>
                <InputField
                  type="text"
                  placeholder="الحد الأعلي للشحن"
                  width={1}
                  value={maximumRechargeValue}
                  onChange={value => this.handleInputFieldChange('maximumRechargeValue', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} ml={5} pr={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  نوع قيمة المنحة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="نوع قيمة المنحة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={awardType}
                  onChange={this.handleChangeAwardTypeSelect}
                  options={awardTypeSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                  isMulti={false}
                />
              </Flex>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  قيمة المنحة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="قيمة المنحة"
                  ref={inputField => {
                    this.discountValueField = inputField;
                  }}
                  width={1}
                  value={awardValue}
                  onChange={value => this.handleInputFieldChange('awardValue', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  وسائل الشحن
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="وسائل الشحن"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={workerRechargeMethods}
                  onChange={this.handleChangeWorkerRechargeMethodsSelect}
                  options={workerRechargeMethodsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                  isMulti
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ الانتهاء
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <DatePicker
                  mb={0}
                  minDate={new Date()}
                  value={expireAt}
                  handleDatePickerChange={this.handleExpireDateInputChange}
                  placeholder="MM/DD/YYYY"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddRechargeAwardForm}
                disabled={isAddRechargeAwardFetching}
                isLoading={isAddRechargeAwardFetching || isEditRechargeAwardFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddRechargeAwardError || isUpdateRechargeAwardError) && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addRechargeAwardErrorMessage || updateRechargeAwardErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewRechargeAwardFormContainer>
      </Modal>
    );
  }
}
AddNewRechargeAwardModal.displayName = 'AddNewRechargeAwardModal';

const mapStateToProps = state => ({
  isAddRechargeAwardFetching: state.rechargeAwards.addRechargeAward.isFetching,
  isEditRechargeAwardFetching: state.rechargeAwards.editRechargeAward.isFetching,
  isAddRechargeAwardError: state.rechargeAwards.addRechargeAward.isFail.isError,
  isUpdateRechargeAwardError: state.rechargeAwards.editRechargeAward.isFail.isError,
  addRechargeAwardErrorMessage: state.rechargeAwards.addRechargeAward.isFail.message,
  isAddRechargeAwardModalOpen:
    state.rechargeAwards.addNewRechargeAwardModal.isOpen ||
    state.rechargeAwards.editRechargeAwardModal.isOpen,
  updateRechargeAwardErrorMessage: state.rechargeAwards.editRechargeAward.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addRechargeAward,
      updateRechargeAward,
      toggleAddNewRechargeAwardModal,
      openEditRechargeAwardModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewRechargeAwardModal);
