import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const { GREY_LIGHT, DISABLED, BRANDING_GREEN, LIGHT_RED } = COLORS;
const { BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const RechargeAwardCard = props => {
  const {
    index,
    user: { permissions },
    rechargeAward,
    rechargeAward: {
      _id,
      awardType,
      awardValue,
      minimumRechargeValue,
      maximumRechargeValue,
      expireAt,
      createdAt,
    },
    handleToggleConfirmModal,
    handleDeleteRechargeAward,
    handleToggleEditRechargeAwardModal,
  } = props;
  const isShowCardFooter =
    (permissions &&
      (permissions.includes('Update Recharge Award') ||
        permissions.includes('Delete Recharge Award'))) ||
    true;
  const rechargeAwardDate = new Date(createdAt);
  const expireDate = new Date(expireAt);
  const deleteRechargeAwardTooltip = 'مسح مكافآة شحن الفنيين';
  const editRechargeAwardTooltip = 'تعديل مكافآة شحن الفنيين';
  const deleteRechargeAwardConfirmModalHeader = 'تأكيد مسح مكافآة شحن الفنيين';
  const deleteRechargeAwardConfirmModalConfirmText = 'هل أنت متأكد أنك تريد مسح مكافآة شحن الفنيين';
  const deleteRechargeAwardConfirmModalFunction = () => {
    handleDeleteRechargeAward(_id, index);
  };

  return (
    <Card
      {...props}
      height="calc(100% - 16px)"
      minHeight={100}
      flexDirection="column"
      justifyContent="space-between"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody
        justifyContent="space-between"
        flexDirection={['column', 'column', 'column', 'column', 'column']}
        py={3}
        px={1}
      >
        <Flex flexDirection="column" justifyContent="space-between">
          <Flex flexDirection="row-reverse" pb={1}>
            <Flex flexDirection="row-reverse" alignItems="center">
              <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                {`[ ${rechargeAwardDate.toLocaleDateString('en-US', {
                  year: 'numeric',
                  month: 'numeric',
                  day: 'numeric',
                  hour12: true,
                })} ]`}
              </Text>
            </Flex>
          </Flex>
          <Flex flexDirection="row-reverse" pb={2}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              ينتهي في :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {expireDate.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour12: true,
              })}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" pb={2}>
            <Flex flexDirection="row-reverse">
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                المنحة
              </SubTitle>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {`${awardValue}${awardType === 'percentage' ? '%' : ' ريال سعودي'}`}
              </SubTitle>
            </Flex>
          </Flex>
          <Flex flexDirection="row-reverse" pb={2}>
            {maximumRechargeValue && (
              <Flex flexDirection="row-reverse">
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  الحد الأعلي لمبلغ الشحن :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {maximumRechargeValue}
                </Text>
              </Flex>
            )}
          </Flex>
          <Flex flexDirection="row-reverse" pb={2}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              الحد الادني لمبلغ الشحن :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {minimumRechargeValue}
            </Text>
          </Flex>
        </Flex>
      </CardBody>
      {isShowCardFooter && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
            {permissions && permissions.includes('Delete Recharge Award') && (
              <>
                <ReactTooltip id={`deleteWorker_${_id}`} type="error" effect="solid">
                  <span>{deleteRechargeAwardTooltip}</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`deleteWorker_${_id}`}
                    data-tip
                    onClick={() =>
                      handleToggleConfirmModal(
                        deleteRechargeAwardConfirmModalHeader,
                        deleteRechargeAwardConfirmModalConfirmText,
                        deleteRechargeAwardConfirmModalFunction,
                      )
                    }
                    icon={faTrashAlt}
                    color={COLORS_VALUES[LIGHT_RED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
            {permissions && permissions.includes('Update Recharge Award') && (
              <>
                <ReactTooltip id={`editRechargeAward_${_id}`} type="light" effect="solid">
                  <span>{editRechargeAwardTooltip}</span>
                </ReactTooltip>
                <Box>
                  <CallToActionIcon
                    data-for={`editRechargeAward_${_id}`}
                    data-tip
                    onClick={() => handleToggleEditRechargeAwardModal(index, rechargeAward)}
                    icon={faPen}
                    color={COLORS_VALUES[DISABLED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
          </Flex>
        </CardFooter>
      )}
    </Card>
  );
};
RechargeAwardCard.displayName = 'RechargeAwardCard';

RechargeAwardCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  rechargeAward: PropTypes.shape({}).isRequired,
  handleDeleteRechargeAward: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
};

RechargeAwardCard.defaultProps = {
  user: undefined,
  handleDeleteRechargeAward: () => {},
  handleToggleConfirmModal: () => {},
};

export default RechargeAwardCard;
