import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { TICKETS_STATUS } from 'utils/constants';
import { getFieldIcon, getTicketStatusColor } from 'utils/shared/style';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import _ from 'lodash';
import {
  CardBody,
  SubTitle,
  CardLabel,
  CardFooter,
  MoreDetailsLink,
  HyperLink,
  HyperLinkText,
} from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const TicketCard = props => {
  const {
    ticket: {
      _id,
      openedBy,
      prettyId,
      createdAt,
      updatedAt,
      latestStatus,
      request,
      customer,
      complaints,
    },
    showWorkerName,
  } = props;
  const { SINGLE_REQUEST, SINGLE_WORKER, SINGLE_CLIENT, SINGLE_TICKET } = ROUTES;
  const { GREY_LIGHT, DISABLED, GREY_DARK, BRANDING_GREEN } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const ticketUpdateAt = new Date(updatedAt);
  const ticketStatusName = TICKETS_STATUS[latestStatus.code].ar;
  const ticketOpenedBy = openedBy.name;
  const ticketOpenedByCreatedAt = new Date(createdAt);
  const ticketStatusColor = getTicketStatusColor(latestStatus.code);
  const fieldIcon = getFieldIcon(request.field);
  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {prettyId}
            </CardLabel>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <Text type={BODY} mx={1} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {`[ ${ticketUpdateAt.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })} ]`}
            </Text>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          {customer ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                العميل :
              </SubTitle>
              <HyperLink href={`${SINGLE_CLIENT}/${customer.username}`} target="_blank">
                <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  [ {customer.username} ] - {customer.name}
                </HyperLinkText>
              </HyperLink>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد عميل
            </SubTitle>
          )}
        </Flex>
        {showWorkerName && (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
            {_.get(complaints, '0.whoCausedTheComplaint') ? (
              <>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  الفني :
                </SubTitle>
                <HyperLink
                  href={`${SINGLE_WORKER}/${complaints[0].whoCausedTheComplaint.username}`}
                  target="_blank"
                >
                  <HyperLinkText
                    cursor="pointer"
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    [ {complaints[0].whoCausedTheComplaint.username} ] -{' '}
                    {complaints[0].whoCausedTheComplaint.name}
                  </HyperLinkText>
                </HyperLink>
              </>
            ) : (
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                لم يتم اختيار فني
              </SubTitle>
            )}
          </Flex>
        )}
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          {request ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الرقم المختصر للطلب :
              </SubTitle>
              <HyperLink href={`${SINGLE_REQUEST}/${request.requestPrettyId}`} target="_blank">
                <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {request.requestPrettyId}
                </HyperLinkText>
              </HyperLink>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد طلب
            </SubTitle>
          )}
        </Flex>
        {request && (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              المدينة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {request.city}
            </Text>
          </Flex>
        )}
        {request && (
          <Flex width={1} flexWrap="wrap" flexDirection="row-reverse" py={1} px={2}>
            <Flex key={request.field} ml={4} mb={1}>
              <Box>
                <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
              </Box>
              <CardLabel
                py={1}
                px={2}
                ml={2}
                type={BODY}
                border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                {request.field}
              </CardLabel>
            </Flex>
          </Flex>
        )}
        {/* <Flex flexDirection="row-reverse" flexWrap="wrap" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد التعليقات :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {comments ? comments.length : 0}
          </Text>
        </Flex> */}
        <Flex alignItems="center" flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            مشكلة :
          </SubTitle>
          <CardLabel
            py={1}
            px={2}
            ml={1}
            type={BODY}
            color={COLORS_VALUES[GREY_DARK]}
            bg={ticketStatusColor}
          >
            {ticketStatusName}
          </CardLabel>
          {openedBy && (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                - تم فتحها بواسطة :
              </SubTitle>
              <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                {`${` [ ${ticketOpenedByCreatedAt &&
                  ticketOpenedByCreatedAt.toLocaleDateString('en-US', {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                  })} ] `}`}{' '}
                {`${ticketOpenedBy}`}
              </Text>
            </>
          )}
        </Flex>
        <Flex flexDirection="row-reverse" flexWrap="wrap" py={1} px={2} mt={1} mb={2}>
          {complaints.map(complain => {
            const { _id: complainId, complainName, complainReceivedIn } = complain;

            return (
              <CardLabel
                key={complainId}
                py={1}
                px={2}
                ml={1}
                mb={1}
                type={BODY}
                color={COLORS_VALUES[GREY_DARK]}
                bg={BRANDING_GREEN}
              >
                {`${complainName.name} - ${complainReceivedIn.name}`}
              </CardLabel>
            );
          })}
        </Flex>
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="flex-end" p={2}>
        <MoreDetailsLink to={`${SINGLE_TICKET}/${_id}`} href={`${SINGLE_TICKET}/${_id}`}>
          <Text
            ml={1}
            cursor="pointer"
            type={BODY}
            color={COLORS_VALUES[BRANDING_GREEN]}
            fontWeight={NORMAL}
          >
            {'<<<< للتفاصيل'}
          </Text>
        </MoreDetailsLink>
      </CardFooter>
    </Card>
  );
};
TicketCard.displayName = 'TicketCard';

TicketCard.propTypes = {
  index: PropTypes.number.isRequired,
  ticket: PropTypes.shape({}).isRequired,
  showWorkerName: PropTypes.bool,
};

TicketCard.defaultProps = {
  showWorkerName: true,
};

export default TicketCard;
