import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import OfferCard from 'components/OfferCard';
import OfferFormModal from 'components/OfferFormModal';
import ConfirmModal from 'components/ConfirmModal';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { FONT_TYPES } from 'components/theme/fonts';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class OffersList extends Component {
  getLoadingOffers = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingOffersList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingOffers(counter));
    }
    return list;
  };

  render() {
    const {
      user,
      offersList,
      isAddNewOfferModalOpen,
      isConfirmModalOpen,
      isDeleteOfferFetching,
      deleteOfferAction,
      handleToggleConfirmModal,
      isEditOfferModalOpen,
      handleToggleEditOfferModal,
      offerDetails,
      offerIndex,
      isGetAllOffersFetching,
      isGetAllOffersSuccess,
      isGetAllOffersError,
      isGetAllOffersErrorMessage,
      applyNewFilter,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isDeleteOfferError,
      isDeleteOfferErrorMessage,
    } = this.props;

    let offersRenderer;

    if (
      (isGetAllOffersFetching && (!offersList || applyNewFilter)) ||
      (!isGetAllOffersFetching && !isGetAllOffersSuccess && !isGetAllOffersError)
    ) {
      offersRenderer = this.createLoadingOffersList();
    } else if (isGetAllOffersError) {
      offersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllOffersErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!offersList.length) {
      offersRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (offersList.length) {
      offersRenderer = offersList.map((offer, index) => {
        const { _id } = offer;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <OfferCard
              m={2}
              index={index}
              user={user}
              offer={offer}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditOfferModal={handleToggleEditOfferModal}
              deleteOfferAction={deleteOfferAction}
            />
          </Box>
        );
      });
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {offersRenderer}
        {isAddNewOfferModalOpen && <OfferFormModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            errorMessage={isDeleteOfferErrorMessage}
            isFail={isDeleteOfferError}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={confirmModalFunction}
            isConfirming={isDeleteOfferFetching}
          />
        )}
        {isEditOfferModalOpen && (
          <OfferFormModal offerDetails={offerDetails} offerIndex={offerIndex} />
        )}
      </Flex>
    );
  }
}

OffersList.displayName = 'OffersList';

OffersList.propTypes = {
  user: PropTypes.shape({}),
  offersList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewOfferModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  deleteOfferAction: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  // deleteOffers: PropTypes.func,
  isDeleteOfferFetching: PropTypes.bool,
  handleToggleEditOfferModal: PropTypes.func,
  isEditOfferModalOpen: PropTypes.bool,
  offerDetails: PropTypes.shape({}),
  offerIndex: PropTypes.number,
  isGetAllOffersFetching: PropTypes.bool,
  isGetAllOffersSuccess: PropTypes.bool,
  isGetAllOffersError: PropTypes.bool,
  isGetAllOffersErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  isDeleteOfferError: PropTypes.bool,
  isDeleteOfferErrorMessage: PropTypes.string,
};

OffersList.defaultProps = {
  user: undefined,
  offersList: undefined,
  isAddNewOfferModalOpen: false,
  handleToggleConfirmModal: () => {},
  deleteOfferAction: () => {},
  isConfirmModalOpen: false,
  // deleteOffers: () => {},
  isDeleteOfferFetching: false,
  handleToggleEditOfferModal: () => {},
  isEditOfferModalOpen: false,
  offerDetails: {},
  offerIndex: undefined,
  isGetAllOffersFetching: false,
  isGetAllOffersSuccess: false,
  isGetAllOffersError: false,
  isGetAllOffersErrorMessage: undefined,
  applyNewFilter: false,
  confirmModalHeader: undefined,
  confirmModalText: undefined,
  confirmModalFunction: () => {},
  isDeleteOfferError: false,
  isDeleteOfferErrorMessage: undefined,
};

export default OffersList;
