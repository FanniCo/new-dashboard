import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faHandPaper, faStar } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import { CITIES, FIELDS, REVIEWS_TAGS } from 'utils/constants';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import {
  CardBody,
  SubTitle,
  CardLabel,
  CardFooter,
  CallToActionIcon,
  HyperLink,
  HyperLinkText,
} from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const reviewsTagsKeys = Object.keys(REVIEWS_TAGS);
const { SINGLE_REQUEST, SINGLE_WORKER, SINGLE_CLIENT } = ROUTES;

const ReviewCard = props => {
  const {
    index,
    user: { permissions },
    review: {
      _id: workerId,
      requestPrettyId,
      field,
      city,
      name,
      username,
      review,
      review: { _id, createdAt, reviewer, reviewerUsername, comment, rating, isHidden },
    },
    handleToggleConfirmModal,
    handleToggleShowingReview,
    handleDeleteReview,
  } = props;
  const {
    GREY_LIGHT,
    DISABLED,
    GREY_DARK,
    BRANDING_BLUE,
    BRANDING_GREEN,
    LIGHT_RED,
    WARNING,
  } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const reviewDate = new Date(createdAt);
  const toggleShowingReviewTooltip = isHidden ? 'إظهار التقييم' : 'إخفاء التقييم';
  const toggleShowingReviewConfirmModalHeader = isHidden
    ? 'تأكيد إظهار التقييم'
    : 'تأكيد إخفاء التقييم';
  const toggleShowingReviewConfirmModalConfirmText = isHidden
    ? 'هل أنت متأكد أنك تريد إظهار التقييم؟'
    : 'هل أنت متأكد أنك تريد إخفاء التقييم؟';
  const toggleShowingReviewConfirmModalFunction = () => {
    handleToggleShowingReview(_id, workerId, index, !isHidden);
  };
  const deleteReviewTooltip = 'حذف التقييم';
  const deleteReviewConfirmModalHeader = 'تأكيد حذف التقييم';
  const deleteReviewConfirmModalConfirmText = 'هل أنت متأكد أنك تريد حذف التقييم؟';
  const deleteReviewConfirmModalFunction = () => {
    handleDeleteReview(_id, workerId, index);
  };

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} mb={1} width={1}>
          {requestPrettyId && (
            <Flex alignItems="center">
              <Link
                to={`${SINGLE_REQUEST}/${requestPrettyId}`}
                href={`${SINGLE_REQUEST}/${requestPrettyId}`}
                style={{ textDecoration: 'none', cursor: 'pointer' }}
              >
                <CardLabel
                  cursor="pointer"
                  pt="6px"
                  pb={1}
                  px={2}
                  mx={2}
                  type={BODY}
                  border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                  color={COLORS_VALUES[DISABLED]}
                  fontWeight={NORMAL}
                >
                  {requestPrettyId}
                </CardLabel>
              </Link>
            </Flex>
          )}
          <Flex alignItems="center">
            <Text type={BODY} mx={1} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {`[ ${reviewDate.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })} ]`}
            </Text>
            {field && (
              <Text type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                {` - ${FIELDS[field].ar}`}
              </Text>
            )}
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          {reviewerUsername ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                العميل :
              </SubTitle>
              <HyperLink href={`${SINGLE_CLIENT}/${reviewerUsername}`} target="_blank">
                <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  [ {reviewerUsername} ] - {reviewer}
                </HyperLinkText>
              </HyperLink>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد عميل
            </SubTitle>
          )}
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الفني :
          </SubTitle>
          <HyperLink href={`${SINGLE_WORKER}/${username}`} target="_blank">
            <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              [ {username} ] - {name}
            </HyperLinkText>
          </HyperLink>
        </Flex>
        <Flex flexDirection="row-reverse" alignItems="center" py={1} px={2} mb={2}>
          {rating ? (
            <>
              {rating >= 1 && (
                <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[WARNING]} size="sm" />
              )}
              {rating >= 2 && (
                <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[WARNING]} size="sm" />
              )}
              {rating >= 3 && (
                <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[WARNING]} size="sm" />
              )}
              {rating >= 4 && (
                <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[WARNING]} size="sm" />
              )}
              {rating >= 5 && (
                <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[WARNING]} size="sm" />
              )}
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد تقييم
            </SubTitle>
          )}
        </Flex>
        {comment && (
          <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
            <CardLabel p={2} ml={1} type={BODY} bg={COLORS_VALUES[BRANDING_BLUE]}>
              <Text textAlign="right" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {comment}
              </Text>
            </CardLabel>
          </Flex>
        )}
        {city && (
          <Flex flexDirection="row-reverse" flexWrap="wrap" py={1} px={2} mb={2}>
            <CardLabel
              py={1}
              px={2}
              ml={1}
              type={BODY}
              color={COLORS_VALUES[DISABLED]}
              bg={BRANDING_BLUE}
            >
              {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
            </CardLabel>
          </Flex>
        )}
        <Flex flexDirection="row-reverse" flexWrap="wrap" py={1} px={2} mb={2}>
          {reviewsTagsKeys.map(key => {
            if (review[key]) {
              return (
                <CardLabel
                  key={key}
                  py={1}
                  px={2}
                  ml={1}
                  mb={1}
                  type={BODY}
                  color={COLORS_VALUES[GREY_DARK]}
                  bg={BRANDING_GREEN}
                >
                  {REVIEWS_TAGS[key].ar}
                </CardLabel>
              );
            }
            return null;
          })}
        </Flex>
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="space-between" p={2}>
        <Flex px={2}>
          {permissions && permissions.includes('Delete Review') && (
            <>
              <ReactTooltip id={`deleteWorker_${_id}`} type="error" effect="solid">
                <span>{deleteReviewTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`deleteWorker_${_id}`}
                  data-tip
                  onClick={() =>
                    handleToggleConfirmModal(
                      deleteReviewConfirmModalHeader,
                      deleteReviewConfirmModalConfirmText,
                      deleteReviewConfirmModalFunction,
                    )
                  }
                  icon={faTrashAlt}
                  color={COLORS_VALUES[LIGHT_RED]}
                  size="sm"
                />
              </Box>
            </>
          )}
          {permissions && permissions.includes('Hide Review') && (
            <>
              <ReactTooltip id={`requestOpsComment${_id}`} type="info" effect="solid">
                <span>{toggleShowingReviewTooltip}</span>
              </ReactTooltip>
              <Box>
                <CallToActionIcon
                  data-for={`requestOpsComment${_id}`}
                  data-tip
                  onClick={() =>
                    handleToggleConfirmModal(
                      toggleShowingReviewConfirmModalHeader,
                      toggleShowingReviewConfirmModalConfirmText,
                      toggleShowingReviewConfirmModalFunction,
                    )
                  }
                  icon={faHandPaper}
                  color={COLORS_VALUES[isHidden ? DISABLED : BRANDING_GREEN]}
                  size="sm"
                />
              </Box>
            </>
          )}
        </Flex>
      </CardFooter>
    </Card>
  );
};
ReviewCard.displayName = 'ReviewCard';

ReviewCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  review: PropTypes.shape({}).isRequired,
  handleToggleShowingReview: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleDeleteReview: PropTypes.func,
};

ReviewCard.defaultProps = {
  user: undefined,
  handleToggleShowingReview: () => {},
  handleToggleConfirmModal: () => {},
  handleDeleteReview: () => {},
};

export default ReviewCard;
