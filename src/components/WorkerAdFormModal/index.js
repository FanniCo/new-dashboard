import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  toggleAddNewWorkersAdModal,
  addNewWorkersAd,
  updateWorkersAd,
  openEditWorkersAdModal,
} from 'redux-modules/workersAds/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { Checkbox, Input, Label } from '@rebass/forms';
import { HyperLink } from 'components/shared';
import _ from "lodash";

const imageStyle = {
  width: '200px',
  height: '150px',
  padding: '5px',
  display: 'inherit',
};

const AddNewWorkersAdFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddNewWorkersAdModal extends Component {
  static propTypes = {
    isAddWorkersAdFetching: PropTypes.bool,
    isAddWorkersAdError: PropTypes.bool,
    addWorkersAdErrorMessage: PropTypes.string,
    isEditWorkersAdError: PropTypes.bool,
    editWorkersAdErrorMessage: PropTypes.string,
    addNewWorkersAd: PropTypes.func,
    toggleAddNewWorkersAdModal: PropTypes.func,
    openEditWorkersAdModal: PropTypes.func,
    updateWorkersAd: PropTypes.func,
    isAddWorkersAdModalOpen: PropTypes.bool,
    workersAdDetails: PropTypes.shape({}),
    workersAdIndex: PropTypes.number,
    isEditWorkersAdFetching: PropTypes.bool,
  };

  static defaultProps = {
    isAddWorkersAdFetching: false,
    isAddWorkersAdError: false,
    addWorkersAdErrorMessage: '',
    isEditWorkersAdError: false,
    editWorkersAdErrorMessage: '',
    addNewWorkersAd: () => {},
    toggleAddNewWorkersAdModal: () => {},
    openEditWorkersAdModal: () => {},
    updateWorkersAd: () => {},
    isAddWorkersAdModalOpen: false,
    workersAdDetails: undefined,
    workersAdIndex: undefined,
    isEditWorkersAdFetching: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      arabicTitle: '',
      englishTitle: '',
      arabicDescription: '',
      englishDescription: '',
      imageUrlAr: '',
      imageUrlEn: '',
      fullImageUrlAr: null,
      fullImageUrlEn: null,
      externalUrl: '',
      active: true,
    };
  }

  componentDidMount() {
    const { workersAdDetails } = this.props;

    if (workersAdDetails) {
      this.setState({
        arabicTitle: workersAdDetails.title ? workersAdDetails.title.ar : '',
        englishTitle: workersAdDetails.title ? workersAdDetails.title.en : '',
        arabicDescription: workersAdDetails.description ? workersAdDetails.description.ar : '',
        englishDescription: workersAdDetails.description ? workersAdDetails.description.ar : '',
        imageUrlAr: workersAdDetails.imageUrl ? workersAdDetails.imageUrl.ar : '',
        imageUrlEn: workersAdDetails.imageUrl ? workersAdDetails.imageUrl.en : '',
        fullImageUrlAr: workersAdDetails.imageUrl ? workersAdDetails.imageUrl.ar : '',
        fullImageUrlEn: workersAdDetails.imageUrl ? workersAdDetails.imageUrl.en : '',
        externalUrl: workersAdDetails.externalUrl || '',
        active: workersAdDetails.active,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitAddWorkersAdForm = e => {
    e.preventDefault();

    const {
      addNewWorkersAd: addNewWorkersAdAction,
      updateWorkersAd: updateWorkersAdAction,
      workersAdDetails,
      workersAdIndex,
    } = this.props;
    const {
      arabicTitle,
      englishTitle,
      arabicDescription,
      englishDescription,
      imageUrlAr,
      imageUrlEn,
      externalUrl,
      active,
    } = this.state;

    const newWorkersAdInfo = new FormData();
    newWorkersAdInfo.append('active', active);

    if (arabicTitle && englishTitle) {
      newWorkersAdInfo.append('title.ar', arabicTitle);
      newWorkersAdInfo.append('title.en', englishTitle);
    }
    if (arabicDescription && englishDescription) {
      newWorkersAdInfo.append('description.ar', arabicDescription);
      newWorkersAdInfo.append('description.en', englishDescription);
    }
    if (imageUrlAr && imageUrlEn) {
      newWorkersAdInfo.append('imageUrlAr', imageUrlAr);
      newWorkersAdInfo.append('imageUrlEn', imageUrlEn);
    }
    if (externalUrl) {
      newWorkersAdInfo.append('externalUrl', externalUrl);
    }

    if (workersAdDetails) {
      const { _id } = workersAdDetails;
      return updateWorkersAdAction(newWorkersAdInfo, _id, workersAdIndex);
    }

    return addNewWorkersAdAction(newWorkersAdInfo);
  };

  handleActiveCheckBoxChange = event => {
    this.setState({ active: event.target.checked });
  };

  handleFileChange = (e, type) => {
    const file = e.target.files[0];
    this.setState({
      [type]: file,
      [_.camelCase(`full_${type}`)]: URL.createObjectURL(file),
    });
  };

  clearFile = type => {
    this.setState({
      [type]: '',
      [_.camelCase(`full_${type}`)]: null,
      [`${type}Key`]: Date.now(),
    });
  };

  render() {
    const {
      isAddWorkersAdFetching,
      isAddWorkersAdModalOpen,
      toggleAddNewWorkersAdModal: toggleAddNewWorkersAdModalAction,
      openEditWorkersAdModal: openEditWorkersAdModalAction,
      isAddWorkersAdError,
      addWorkersAdErrorMessage,
      isEditWorkersAdError,
      editWorkersAdErrorMessage,
      workersAdDetails,
      isEditWorkersAdFetching,
    } = this.props;
    const {
      arabicTitle,
      englishTitle,
      arabicDescription,
      englishDescription,
      imageUrlAr,
      imageUrlEn,
      fullImageUrlAr,
      fullImageUrlEn,
      externalUrl,
      active,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addWorkerModalHeader = workersAdDetails ? 'تعديل الاعلان' : 'أضف اعلان جديدة';
    const submitButtonText = workersAdDetails ? 'تعديل' : 'أضف';
    return (
      <Modal
        toggleModal={() =>
          workersAdDetails ? openEditWorkersAdModalAction() : toggleAddNewWorkersAdModalAction()
        }
        header={addWorkerModalHeader}
        isOpened={isAddWorkersAdModalOpen}
      >
        <AddNewWorkersAdFormContainer onSubmit={this.onSubmitAddWorkersAdForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى العنوان (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان عربى"
                  width={1}
                  value={arabicTitle}
                  onChange={value => this.handleInputFieldChange('arabicTitle', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتوى العنوان(بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان انجليزى"
                  width={1}
                  value={englishTitle}
                  onChange={value => this.handleInputFieldChange('englishTitle', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="الوصف عربى"
                  width={1}
                  value={arabicDescription}
                  onChange={value => this.handleInputFieldChange('arabicDescription', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالانجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="الوصف انجليزى"
                  width={1}
                  value={englishDescription}
                  onChange={value => this.handleInputFieldChange('englishDescription', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                {fullImageUrlAr && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrlAr')} />
                    <HyperLink href={fullImageUrlAr} target="_blank">
                      <img style={imageStyle} src={fullImageUrlAr} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  العنوان الالكترونى للصورة (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="العنوان الالكترونى للصورة (بالعربى)"
                  name="fileAr"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrlAr');
                  }}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                {fullImageUrlEn && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrlEn')} />
                    <HyperLink href={fullImageUrlEn} target="_blank">
                      <img style={imageStyle} src={fullImageUrlEn} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  العنوان الالكترونى للصورة (بالانجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="العنوان الالكترونى للصورة (بالانجليزى)"
                  name="fileAr"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrlEn');
                  }}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  االعنوان الالكترونى خارجى
                </Text>
                <InputField
                  type="text"
                  placeholder="العنوان الالكترونى خارجى"
                  width={1}
                  value={externalUrl}
                  onChange={value => this.handleInputFieldChange('externalUrl', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Box>
                <Label>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    مفعل
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <Checkbox
                    name="remember"
                    checked={active}
                    onChange={this.handleActiveCheckBoxChange}
                    sx={{ color: '#fcfffa' }}
                  />
                </Label>
              </Box>
            </Flex>

            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddWorkerForm}
                disabled={
                  isAddWorkersAdFetching ||
                  arabicTitle === '' ||
                  englishTitle === '' ||
                  arabicDescription === '' ||
                  englishDescription === '' ||
                  imageUrlAr === '' ||
                  imageUrlEn === ''
                }
                isLoading={isAddWorkersAdFetching || isEditWorkersAdFetching}
              >
                {submitButtonText}
              </Button>
              {(isAddWorkersAdError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addWorkersAdErrorMessage}
                </Caution>
              )) ||
                (isEditWorkersAdError && (
                  <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                    {editWorkersAdErrorMessage}
                  </Caution>
                ))}
            </Flex>
          </Flex>
        </AddNewWorkersAdFormContainer>
      </Modal>
    );
  }
}

AddNewWorkersAdModal.displayName = 'AddNewWorkersAdModal';

const mapStateToProps = state => ({
  isAddWorkersAdFetching: state.workersAds.addWorkersAd.isFetching,
  isEditWorkersAdFetching: state.workersAds.updateWorkersAd.isFetching,
  isAddWorkersAdError: state.workersAds.addWorkersAd.isFail.isError,
  addWorkersAdErrorMessage: state.workersAds.addWorkersAd.isFail.message,
  isEditWorkersAdError: state.workersAds.updateWorkersAd.isFail.isError,
  editWorkersAdErrorMessage: state.workersAds.updateWorkersAd.isFail.message,
  isAddWorkersAdModalOpen:
    state.workersAds.addNewWorkersAdModal.isOpen || state.workersAds.editWorkersAdModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewWorkersAdModal,
      addNewWorkersAd,
      updateWorkersAd,
      openEditWorkersAdModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewWorkersAdModal);
