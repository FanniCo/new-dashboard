import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPen,
  faPowerOff,
  faStar,
  faCrown,
  faDollarSign,
  faCommentDots,
  faHandPaper,
} from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Card from 'components/Card';
import Text from 'components/Text';
import {
  CardBody,
  SubTitle,
  CardLabel,
  CardFooter,
  CallToActionIcon,
  MoreDetailsLink,
} from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { FIELDS, CITIES } from 'utils/constants';
import { getFieldIcon } from 'utils/shared/style';
import ROUTES from 'routes';

const { SINGLE_WORKER } = ROUTES;
const { PRIMARY, GREY_LIGHT, DISABLED, BRANDING_GREEN, BRANDING_BLUE, LIGHT_RED, WARNING } = COLORS;
const { CAPTION, BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const WorkerCard = props => {
  const {
    index,
    user: { permissions },
    worker,
    worker: {
      _id,
      name,
      username,
      field,
      fields,
      credits,
      city,
      suspendedTo,
      featuredSubscriptionValidTo,
      elite,
      active,
      createdAt,
      comments,
      vendor,
      requestsCount,
      doneRequestsCount,
      canceledRequestsCount,
      postponedRequestsCount,
      activeRequestsCount,
      reviewedRequestsCount,
      ticketsCount,
      ticketsHasFeeCount,
    },
    handleToggleConfirmModal,
    handleToggleOpsCommentsModal,
    handleToggleEditWorkerModal,
    handleToggleAddCreditModal,
    handleActivateWorker,
    // handleDeleteWorker,
    handleFeatureWorker,
    handleMakeWorkerElite,
    handleToggleSuspendWorkerModal,
    handleSuspendWorker,
  } = props;
  const workerDate = new Date(createdAt);
  const editWorkerTooltip = 'تعديل الفني';
  const addCreditTooltip = 'أضف رصيد';
  const workerOpsCommentsTooltip = 'تعليقات العمليات';
  const activateWorkerTooltip = active ? 'إلغاء تنشيط الفني' : 'تنشيط الفني';
  const unSuspendWorkerConfirmModalHeader = 'تاكيد الغاء وقف الفنى';
  const unSuspendWorkerConfirmModalConfirmText = 'هل انت  متاكيد انك تريد الغاء وقف الفنى';
  const unSuspendWorkerConfirmModalFunction = () => {
    handleSuspendWorker(_id, index, new Date());
  };
  // const deleteWorkerTooltip = 'مسح الفني';
  const featureWorkerTooltip = moment(featuredSubscriptionValidTo).isSameOrAfter(moment().utc())
    ? 'إلغاء تمييز الفني'
    : 'تمييز الفني';
  const eliteWorkerTooltip = elite ? 'إلغاء جعل الفني من النخبة' : 'جعل الفني من النخبة';
  const activateWorkerConfirmModalHeader = !active ? 'تأكيد تنشيط الفني' : 'تأكيد عدم تنشيط الفني';
  const activateWorkerConfirmModalConfirmText = !active
    ? 'هل أنت متأكد أنك تريد تنشيط الفني؟'
    : 'هل أنت متأكد أنك تريد عدم تنشيط الفني؟';
  const activateWorkerConfirmModalFunction = () => {
    handleActivateWorker(_id, index, !active);
  };
  const featureWorkerConfirmModalHeader = moment(featuredSubscriptionValidTo).isBefore(
    moment().utc(),
  )
    ? 'تأكيد تمييز الفني'
    : 'تأكيد عدم تمييز الفني';
  const featureWorkerConfirmModalConfirmText = moment(featuredSubscriptionValidTo).isBefore(
    moment().utc(),
  )
    ? 'هل أنت متأكد أنك تريد تمييز الفني؟'
    : 'هل أنت متأكد أنك تريد عدم تمييز الفني؟';
  const featureWorkerConfirmModalFunction = () => {
    handleFeatureWorker(_id, index, moment(featuredSubscriptionValidTo).isBefore(moment().utc()));
  };
  const eliteWorkerConfirmModalHeader = !elite
    ? 'تأكيد جعل الفني من النخبة'
    : 'تأكيد عدم جعل الفني من النخبة';
  const eliteWorkerConfirmModalConfirmText = !elite
    ? 'هل أنت متأكد أنك تريد جعل الفني من النخبة؟'
    : 'هل أنت متأكد أنك تريد عدم جعل الفني من النخبة؟';
  const eliteWorkerConfirmModalFunction = () => {
    handleMakeWorkerElite(_id, index, !elite);
  };
  // const deleteWorkerConfirmModalHeader = 'تأكيد مسح الفني';
  // const deleteWorkerConfirmModalConfirmText = 'هل أنت متأكد أنك تريد مسح الفني؟';
  // const deleteWorkerConfirmModalFunction = () => {
  //   handleDeleteWorker(_id, index);
  // };

  let fieldsContent;

  if (fields.length) {
    fieldsContent = fields.map(fieldKey => {
      const fieldIcon = getFieldIcon(fieldKey);

      return (
        <Flex key={fieldKey} mb={2} alignItems="center" justifyContent="flex-end">
          <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
          <CardLabel
            py={1}
            px={2}
            mx={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            {FIELDS[fieldKey].ar}
          </CardLabel>
        </Flex>
      );
    });
  } else if (!fields.length && field) {
    const fieldIcon = getFieldIcon(field);

    fieldsContent = (
      <Flex alignItems="center" justifyContent="flex-end">
        <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
        <CardLabel
          py={1}
          px={2}
          mx={2}
          type={CAPTION}
          border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          {FIELDS[field].ar}
        </CardLabel>
      </Flex>
    );
  }
  return (
    <Card
      {...props}
      height="calc(100% - 16px)"
      minHeight={100}
      flexDirection="column"
      justifyContent="space-between"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody
        justifyContent="space-between"
        flexDirection={['column', 'row', 'row', 'row', 'row']}
        py={3}
        px={1}
      >
        <Flex ml={1}>
          <Flex
            width={1}
            flexWrap={['wrap', 'initial', 'initial', 'initial', 'initial']}
            flexDirection={['row-reverse', 'column', 'column', 'column', 'column']}
            mb={[3, 0, 0, 0, 0]}
          >
            {fieldsContent}
          </Flex>
        </Flex>
        <Flex flexDirection="column" justifyContent="space-between">
          <Flex alignItems="center" flexDirection="row-reverse" pb={2} px={2} width={1}>
            <Flex flexDirection="row" alignItems="center">
              <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                {`[ ${username} ] - ${name}`}
              </Text>
            </Flex>
          </Flex>
          <Flex alignItems="center" flexDirection="row-reverse" px={2} mb={1}>
            <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {workerDate.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })}
            </Text>
          </Flex>
          {vendor && (
            <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                اسم الشريك :
              </SubTitle>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {vendor.name}
              </Text>
            </Flex>
          )}
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              المدينة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              الرصيد :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {credits}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              متوسط التقييمات :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {(
                worker.rateSum / (worker.numberOfReviews === 0 ? 1 : worker.numberOfReviews) || 0
              ).toFixed(2)}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد التعليقات :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {comments ? comments.length : 0}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الطلبات :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {requestsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الطلبات المنجزة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {doneRequestsCount - reviewedRequestsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الطلبات تم تقيمها :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {reviewedRequestsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الطلبات الملغية :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {canceledRequestsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الطلبات الموجلة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {postponedRequestsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الطلبات النشطة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {activeRequestsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد الشكاوي :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {ticketsCount}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
               نسبة الشكاوي/الطلبات المكتملة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {(ticketsCount/ (doneRequestsCount || 1)).toFixed(1)}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              نسبة الشكاوي برسوم/الطلبات المكتملة :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {(ticketsHasFeeCount/ (doneRequestsCount || 1)).toFixed(1)}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" pt={1} px={2} mt={1}>
            <CardLabel py={1} px={2} type={BODY} color={COLORS_VALUES[DISABLED]} bg={BRANDING_BLUE}>
              {suspendedTo && Date.parse(new Date(suspendedTo)) >= Date.parse(new Date())
                ? 'متوقف'
                : 'غير متوقف'}
            </CardLabel>
            <CardLabel
              py={1}
              px={2}
              mr={1}
              type={BODY}
              color={COLORS_VALUES[DISABLED]}
              bg={BRANDING_BLUE}
            >
              {moment(featuredSubscriptionValidTo).isSameOrAfter(moment().utc())
                ? 'متميز'
                : 'غير متميز'}
            </CardLabel>
            <CardLabel
              py={1}
              px={2}
              mr={1}
              type={BODY}
              color={COLORS_VALUES[DISABLED]}
              bg={BRANDING_BLUE}
            >
              {active ? 'نشط' : 'غير نشط'}
            </CardLabel>
          </Flex>
        </Flex>
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="space-between" flexWrap="wrap" p={2}>
        <Flex px={2}>
          {/* {permissions && permissions.includes('Delete Workers') && ( */}
          {/*  <> */}
          {/*    <ReactTooltip id={`deleteWorker_${_id}`} type="error" effect="solid"> */}
          {/*      <span>{deleteWorkerTooltip}</span> */}
          {/*    </ReactTooltip> */}
          {/*    <Box mr={3}> */}
          {/*      <CallToActionIcon */}
          {/*        data-for={`deleteWorker_${_id}`} */}
          {/*        data-tip */}
          {/*        onClick={() => */}
          {/*          handleToggleConfirmModal( */}
          {/*            deleteWorkerConfirmModalHeader, */}
          {/*            deleteWorkerConfirmModalConfirmText, */}
          {/*            deleteWorkerConfirmModalFunction, */}
          {/*          ) */}
          {/*        } */}
          {/*        icon={faTrashAlt} */}
          {/*        color={COLORS_VALUES[LIGHT_RED]} */}
          {/*        size="sm" */}
          {/*      /> */}
          {/*    </Box> */}
          {/*  </> */}
          {/* )} */}
          {permissions && permissions.includes('Add Credits To Worker') && (
            <>
              <ReactTooltip id={`addCredit_${_id}`} type="light" effect="solid">
                <span>{addCreditTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`addCredit_${_id}`}
                  data-tip
                  onClick={() => handleToggleAddCreditModal(index, worker)}
                  icon={faDollarSign}
                  color={COLORS_VALUES[DISABLED]}
                  size="sm"
                />
              </Box>
            </>
          )}
          {permissions && permissions.includes('Change Worker Featured') && (
            <>
              <ReactTooltip id={`featureWorker_${_id}`} type="light" effect="solid">
                <span>{featureWorkerTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`featureWorker_${_id}`}
                  data-tip
                  onClick={() =>
                    handleToggleConfirmModal(
                      featureWorkerConfirmModalHeader,
                      featureWorkerConfirmModalConfirmText,
                      featureWorkerConfirmModalFunction,
                    )
                  }
                  icon={faStar}
                  color={
                    COLORS_VALUES[
                      moment(featuredSubscriptionValidTo).isSameOrAfter(moment().utc())
                        ? DISABLED
                        : WARNING
                    ]
                  }
                  size="sm"
                />
              </Box>
            </>
          )}
          <ReactTooltip id={`eliteWorker_${_id}`} type="light" effect="solid">
            <span>{eliteWorkerTooltip}</span>
          </ReactTooltip>
          <Box mr={3}>
            <CallToActionIcon
              data-for={`eliteWorker_${_id}`}
              data-tip
              onClick={() =>
                handleToggleConfirmModal(
                  eliteWorkerConfirmModalHeader,
                  eliteWorkerConfirmModalConfirmText,
                  eliteWorkerConfirmModalFunction,
                )
              }
              icon={faCrown}
              color={COLORS_VALUES[elite ? DISABLED : BRANDING_GREEN]}
              size="sm"
            />
          </Box>
          {permissions && permissions.includes('Activate Worker') && (
            <>
              <ReactTooltip id={`activateWorker_${_id}`} type="light" effect="solid">
                <span>{activateWorkerTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`activateWorker_${_id}`}
                  data-tip
                  onClick={() =>
                    handleToggleConfirmModal(
                      activateWorkerConfirmModalHeader,
                      activateWorkerConfirmModalConfirmText,
                      activateWorkerConfirmModalFunction,
                    )
                  }
                  icon={faPowerOff}
                  color={COLORS_VALUES[active ? DISABLED : PRIMARY]}
                  size="sm"
                />
              </Box>
            </>
          )}
          {permissions &&
            permissions.includes('Suspend or unSuspend Workers') &&
            (!suspendedTo || Date.parse(new Date(suspendedTo)) < Date.parse(new Date())) && (
              <>
                <ReactTooltip id={`suspendWorker_${_id}`} type="error" effect="solid">
                  <span>وقف الفني</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`suspendWorker_${_id}`}
                    data-tip
                    onClick={() => handleToggleSuspendWorkerModal(index, worker)}
                    icon={faHandPaper}
                    color={
                      COLORS_VALUES[
                        suspendedTo && Date.parse(new Date(suspendedTo)) >= Date.parse(new Date())
                          ? DISABLED
                          : LIGHT_RED
                      ]
                    }
                    size="sm"
                  />
                </Box>
              </>
          )}
          {permissions &&
            permissions.includes('Suspend or unSuspend Workers') &&
            suspendedTo &&
            Date.parse(new Date(suspendedTo)) > Date.parse(new Date()) && (
              <>
                <ReactTooltip id={`suspendWorker_${_id}`} type="error" effect="solid">
                  <span>الغاء وقف الفني</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`suspendWorker_${_id}`}
                    data-tip
                    onClick={() =>
                      handleToggleConfirmModal(
                        unSuspendWorkerConfirmModalHeader,
                        unSuspendWorkerConfirmModalConfirmText,
                        unSuspendWorkerConfirmModalFunction,
                      )
                    }
                    icon={faHandPaper}
                    color={
                      COLORS_VALUES[
                        suspendedTo && Date.parse(new Date(suspendedTo)) >= Date.parse(new Date())
                          ? DISABLED
                          : LIGHT_RED
                      ]
                    }
                    size="sm"
                  />
                </Box>
              </>
          )}
          <ReactTooltip id={`userOpsComment${_id}`} type="info" effect="solid">
            <span>{workerOpsCommentsTooltip}</span>
          </ReactTooltip>
          <Box mr={3}>
            <CallToActionIcon
              data-for={`userOpsComment${_id}`}
              data-tip
              onClick={() => handleToggleOpsCommentsModal(index)}
              icon={faCommentDots}
              color={COLORS_VALUES[DISABLED]}
              size="sm"
            />
          </Box>
          {permissions && permissions.includes('Update Workers') && (
            <>
              <ReactTooltip id={`editWorker_${_id}`} type="light" effect="solid">
                <span>{editWorkerTooltip}</span>
              </ReactTooltip>
              <Box>
                <CallToActionIcon
                  data-for={`editWorker_${_id}`}
                  data-tip
                  onClick={() => handleToggleEditWorkerModal(index, worker)}
                  icon={faPen}
                  color={COLORS_VALUES[DISABLED]}
                  size="sm"
                />
              </Box>
            </>
          )}
        </Flex>
        <MoreDetailsLink to={`${SINGLE_WORKER}/${username}`} href={`${SINGLE_WORKER}/${username}`}>
          <Text
            ml={1}
            cursor="pointer"
            type={BODY}
            color={COLORS_VALUES[BRANDING_GREEN]}
            fontWeight={NORMAL}
          >
            {'<<<< للتفاصيل'}
          </Text>
        </MoreDetailsLink>
      </CardFooter>
    </Card>
  );
};
WorkerCard.displayName = 'WorkerCard';

WorkerCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  worker: PropTypes.shape({}).isRequired,
  handleActivateWorker: PropTypes.func,
  // handleDeleteWorker: PropTypes.func,
  handleFeatureWorker: PropTypes.func,
  handleMakeWorkerElite: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditWorkerModal: PropTypes.func,
  handleToggleAddCreditModal: PropTypes.func,
  handleToggleOpsCommentsModal: PropTypes.func,
  handleToggleSuspendWorkerModal: PropTypes.func,
  handleSuspendWorker: PropTypes.func,
};

WorkerCard.defaultProps = {
  user: undefined,
  handleActivateWorker: () => {},
  // handleDeleteWorker: () => {},
  handleFeatureWorker: () => {},
  handleMakeWorkerElite: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleEditWorkerModal: () => {},
  handleToggleAddCreditModal: () => {},
  handleToggleOpsCommentsModal: () => {},
  handleToggleSuspendWorkerModal: () => {},
  handleSuspendWorker: () => {},
};

export default WorkerCard;
