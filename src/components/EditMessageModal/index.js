import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { updateMessage } from 'redux-modules/messages/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { FIELDS, CITIES } from 'utils/constants';

const fieldsKeys = Object.keys(FIELDS);
const citiesKeys = Object.keys(CITIES);

const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));

const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));

const AddNewMessageFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class EditMessageModal extends Component {
  static propTypes = {
    isEditMessageFetching: PropTypes.bool,
    isEditMessageError: PropTypes.bool,
    editMessageErrorMessage: PropTypes.string,
    handleToggleEditWorkerModal: PropTypes.func,
    isEditMessageModalOpen: PropTypes.bool,
    messageDetails: PropTypes.shape({}),
    updateMessage: PropTypes.func,
    messageIndex: PropTypes.number,
  };

  static defaultProps = {
    isEditMessageFetching: false,
    isEditMessageError: false,
    editMessageErrorMessage: '',
    handleToggleEditWorkerModal: () => {},
    isEditMessageModalOpen: false,
    messageDetails: {},
    updateMessage: () => {},
    messageIndex: undefined,
  };

  constructor(props) {
    super(props);

    this.state = {
      _id: '',
      arabicTitle: '',
      englishTitle: '',
      arabicDescription: '',
      englishDescription: '',
      fields: [],
      cities: [],
    };
  }

  componentDidMount() {
    const { messageDetails } = this.props;
    const fields = [];
    const cities = [];
    messageDetails.fields.forEach(messageFild => {
      fieldsKeys.forEach(key => {
        if (messageFild === key) {
          fields.push({
            value: messageFild,
            label: FIELDS[messageFild].ar,
          });
        }
      });
    });

    messageDetails.cities.forEach(messageCity => {
      citiesKeys.forEach(key => {
        if (messageCity === key) {
          cities.push({
            value: messageCity,
            label: CITIES[messageCity].ar,
          });
        }
      });
    });

    this.setState({
      _id: messageDetails._id,
      arabicTitle: messageDetails.title.ar,
      englishTitle: messageDetails.title.en,
      arabicDescription: messageDetails.description.ar,
      englishDescription: messageDetails.description.en,
      fields,
      cities,
    });
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeFieldsSelect = fields => {
    this.setState({ fields });
  };

  handleChangeCitiesSelect = cities => {
    this.setState({ cities });
  };

  onSubmitEditMessageForm = e => {
    e.preventDefault();

    const { updateMessage: updateMessageAction, messageIndex } = this.props;
    const {
      _id,
      arabicTitle,
      englishTitle,
      arabicDescription,
      englishDescription,
      fields,
      cities,
    } = this.state;
    const messageFields = fields.map(field => field.value);
    const messageCitties = cities.map(city => city.value);

    const newMessageInfo = {
      title: {
        ar: arabicTitle,
        en: englishTitle,
      },
      description: {
        ar: arabicDescription,
        en: englishDescription,
      },
      fields: messageFields || [],
      cities: messageCitties || [],
    };

    updateMessageAction(newMessageInfo, _id, messageIndex);
  };

  render() {
    const {
      isEditMessageFetching,
      isEditMessageModalOpen,
      handleToggleEditWorkerModal: handleToggleEditWorkerModalAction,
      isEditMessageError,
      editMessageErrorMessage,
    } = this.props;
    const {
      arabicTitle,
      englishTitle,
      arabicDescription,
      englishDescription,
      fields,
      cities,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const editMessageModalHeader = 'تعديل الرسالة';
    return (
      <Modal
        toggleModal={handleToggleEditWorkerModalAction}
        header={editMessageModalHeader}
        isOpened={isEditMessageModalOpen}
      >
        <AddNewMessageFormContainer onSubmit={this.onSubmitEditMessageForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى العنوان (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="العنوان عربى"
                  width={1}
                  value={arabicTitle}
                  onChange={value => this.handleInputFieldChange('arabicTitle', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتوى العنوان(بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="العنوان انجليزى"
                  width={1}
                  value={englishTitle}
                  onChange={value => this.handleInputFieldChange('englishTitle', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الوصف عربى"
                  width={1}
                  value={arabicDescription}
                  onChange={value => this.handleInputFieldChange('arabicDescription', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="الوصف انجليزى"
                  width={1}
                  value={englishDescription}
                  onChange={value => this.handleInputFieldChange('englishDescription', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصصات
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti
                  isRtl
                  backspaceRemovesValue={false}
                  value={fields}
                  onChange={this.handleChangeFieldsSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدن
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti
                  isRtl
                  backspaceRemovesValue={false}
                  value={cities}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddWorkerForm}
                disabled={
                  isEditMessageFetching ||
                  arabicTitle === '' ||
                  englishTitle === '' ||
                  arabicDescription === '' ||
                  englishDescription === '' ||
                  (!fields && !cities)
                }
                isLoading={isEditMessageFetching}
              >
                تعديل
              </Button>
              {isEditMessageError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {editMessageErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewMessageFormContainer>
      </Modal>
    );
  }
}

EditMessageModal.displayName = 'EditMessageModal';

const mapStateToProps = state => ({
  isEditMessageModalOpen: state.messages.editMessageModal.isOpen,
  isEditMessageFetching: state.messages.updateMessage.isFetching,
  isEditMessageError: state.messages.updateMessage.isFail.isError,
  editMessageErrorMessage: state.messages.updateMessage.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateMessage,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);
