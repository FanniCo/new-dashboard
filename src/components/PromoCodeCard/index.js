import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { FIELDS, CITIES } from 'utils/constants';
import { getFieldIcon } from 'utils/shared/style';
import { CopyToClipboard } from 'pivotal-ui/react/copy-to-clipboard';
import { DefaultButton } from 'pivotal-ui/react/buttons';
import { Icon } from 'pivotal-ui/react/iconography';

const { GREY_LIGHT, DISABLED, BRANDING_GREEN, BRANDING_BLUE, LIGHT_RED } = COLORS;
const { CAPTION, BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const PromoCodeCard = props => {
  const {
    index,
    user: { permissions },
    promoCode,
    promoCode: {
      _id,
      code,
      fields,
      cities,
      discount,
      upperLimitForDiscount,
      minimumWorkerWage,
      timesPerCustomerCanUse,
      expireAt,
      createdAt,
      isForNewCustomer,
      workerRefundedMoneyForFixedDiscount,
    },
    handleToggleConfirmModal,
    handleDeletePromoCode,
    handleToggleEditPromoModal,
  } = props;
  const isShowCardFooter =
    permissions &&
    (permissions.includes('Update Promo Code') || permissions.includes('Delete Promo Code'));
  const promoCodeDate = new Date(createdAt);
  const expireDate = new Date(expireAt);
  const deletePromoCodeTooltip = 'مسح البروموكود';
  const editPromoCodeTooltip = 'تعديل البروموكود';
  const deletePromoCodeConfirmModalHeader = 'تأكيد مسح البروموكود';
  const deletePromoCodeConfirmModalConfirmText = 'هل أنت متأكد أنك تريد مسح البروموكود';
  const deletePromoCodeConfirmModalFunction = () => {
    handleDeletePromoCode(_id, index);
  };

  const fieldsContent =
    fields &&
    fields.map(field => (
      <Flex alignItems="center" justifyContent="flex-end" key={field}>
        <FontAwesomeIcon
          icon={getFieldIcon(field)}
          color={COLORS_VALUES[BRANDING_GREEN]}
          size="sm"
        />
        <CardLabel
          py={1}
          px={2}
          mx={2}
          type={CAPTION}
          border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          {FIELDS[field].ar}
        </CardLabel>
      </Flex>
    ));

  const citiesContent =
    cities &&
    cities.map(city => (
      <CardLabel
        key={city}
        py={1}
        px={2}
        ml={1}
        mb={1}
        type={BODY}
        color={COLORS_VALUES[DISABLED]}
        bg={BRANDING_BLUE}
      >
        {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
      </CardLabel>
    ));

  return (
    <Card
      {...props}
      height="calc(100% - 16px)"
      minHeight={100}
      flexDirection="column"
      justifyContent="space-between"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody
        justifyContent="space-between"
        flexDirection={['column', 'row', 'row', 'row', 'row']}
        py={3}
        px={1}
      >
        <Flex ml={1}>
          <Flex
            width={1}
            flexWrap={['wrap', 'initial', 'initial', 'initial', 'initial']}
            flexDirection={['row-reverse', 'column', 'column', 'column', 'column']}
            mb={[3, 0, 0, 0, 0]}
          >
            {fieldsContent}
          </Flex>
        </Flex>
        <Flex flexDirection="column" justifyContent="space-between">
          <Flex alignItems="center" flexDirection="row-reverse" pb={2} px={2} width={1}>
            <Flex flexDirection="row" alignItems="center">
              <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
                {`[ ${promoCodeDate.toLocaleDateString('en-US', {
                  year: 'numeric',
                  month: 'numeric',
                  day: 'numeric',
                  hour12: true,
                })} ] - ${code}`}
              </Text>
            </Flex>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              ينتهي في :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {expireDate.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour12: true,
              })}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" flexWrap="wrap">
            <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {discount.type === 'voucher' ? 'المبلغ سوف يضاف لى العميل فى المحفظة' : 'الخصم'} :
              </SubTitle>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {`${discount.value}${discount.type === 'percentage' ? '%' : ' ريال سعودي'}`}
              </SubTitle>
            </Flex>
            {upperLimitForDiscount && (
              <Flex flexDirection="row-reverse" py={1} px={4} mb={1}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  الحد الأعلي للخصم :
                </SubTitle>
                <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {upperLimitForDiscount}
                </Text>
              </Flex>
            )}
          </Flex>
          {discount.type === 'fixed' && (
            <Flex flexDirection="row-reverse" flexWrap="wrap">
              <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  قيمة تعويد الفني :
                </SubTitle>
                <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {`${workerRefundedMoneyForFixedDiscount} ريال سعودي `}
                </SubTitle>
              </Flex>
            </Flex>
          )}
          {minimumWorkerWage && (
            <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الحد الأدني لأجرة الفني :
              </SubTitle>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {minimumWorkerWage}
              </Text>
            </Flex>
          )}
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              عدد المرات لكل عميل :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {timesPerCustomerCanUse}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              هل مخصص للعملاء الجداد :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {isForNewCustomer ? 'نعم' : 'لا'}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              مدة انتهاء الرصيد المؤقت للعميل :
            </SubTitle>
            <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {discount.cashBackCreditsExpirationPeriodInDays || '-'}
            </Text>
          </Flex>
          <Flex flexDirection="row-reverse" w={1} py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              رابط المشاركة:
            </SubTitle>
            <CopyToClipboard
              text={`https://fanni.sa/promos/?promoCode=${code}`}
              tooltip="نسخ بنجاح"
            >
              <DefaultButton
                {...{
                  iconOnly: true,
                  icon: <Icon src="copy" />,
                }}
              />
            </CopyToClipboard>
          </Flex>
          <Flex flexDirection="row-reverse" flexWrap="wrap" pt={1} px={2} mt={1}>
            {citiesContent}
          </Flex>
        </Flex>
      </CardBody>
      {isShowCardFooter && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
            {permissions && permissions.includes('Delete Promo Code') && (
              <>
                <ReactTooltip id={`deleteWorker_${_id}`} type="error" effect="solid">
                  <span>{deletePromoCodeTooltip}</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`deleteWorker_${_id}`}
                    data-tip
                    onClick={() =>
                      handleToggleConfirmModal(
                        deletePromoCodeConfirmModalHeader,
                        deletePromoCodeConfirmModalConfirmText,
                        deletePromoCodeConfirmModalFunction,
                      )
                    }
                    icon={faTrashAlt}
                    color={COLORS_VALUES[LIGHT_RED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
            {permissions && permissions.includes('Update Promo Code') && (
              <>
                <ReactTooltip id={`editPromoCode_${_id}`} type="light" effect="solid">
                  <span>{editPromoCodeTooltip}</span>
                </ReactTooltip>
                <Box>
                  <CallToActionIcon
                    data-for={`editPromoCode_${_id}`}
                    data-tip
                    onClick={() => handleToggleEditPromoModal(index, promoCode)}
                    icon={faPen}
                    color={COLORS_VALUES[DISABLED]}
                    size="sm"
                  />
                </Box>
              </>
            )}
          </Flex>
        </CardFooter>
      )}
    </Card>
  );
};
PromoCodeCard.displayName = 'PromoCodeCard';

PromoCodeCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  promoCode: PropTypes.shape({}).isRequired,
  handleDeletePromoCode: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
};

PromoCodeCard.defaultProps = {
  user: undefined,
  handleDeletePromoCode: () => {},
  handleToggleConfirmModal: () => {},
};

export default PromoCodeCard;
