import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import {
  createAdmin,
  addPermissionsToUser,
  toggleAddEditAdminModal,
} from 'redux-modules/user/actions';
import UserPermissionsDropDown from 'containers/shared/UserPermissionsDropDown';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const AddEditAdminFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddEditAdminModal extends Component {
  static propTypes = {
    configs: PropTypes.shape({}),
    adminDetails: PropTypes.shape({}),
    isCreateAdminFetching: PropTypes.bool,
    isCreateAdminError: PropTypes.bool,
    createAdminErrorMessage: PropTypes.string,
    createAdmin: PropTypes.func,
    isAddPermissionsToUserFetching: PropTypes.bool,
    isAddPermissionsToUserError: PropTypes.bool,
    addPermissionsToUserErrorMessage: PropTypes.string,
    addPermissionsToUser: PropTypes.func,
    toggleAddEditAdminModal: PropTypes.func,
    isAddEditAdminModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    configs: undefined,
    adminDetails: undefined,
    isCreateAdminFetching: false,
    isCreateAdminError: false,
    createAdminErrorMessage: '',
    createAdmin: () => {},
    isAddPermissionsToUserFetching: false,
    isAddPermissionsToUserError: false,
    addPermissionsToUserErrorMessage: '',
    addPermissionsToUser: () => {},
    toggleAddEditAdminModal: () => {},
    isAddEditAdminModalOpen: false,
  };

  constructor(props) {
    super(props);

    const { adminDetails, configs } = props;
    const isAdminHasPermissiosn = adminDetails ? !!adminDetails.permissions : false;
    const permissionsArray = isAdminHasPermissiosn ? adminDetails.permissions.adminPermissions : [];
    const selectedPermissions =
      permissionsArray.length > 0 &&
      permissionsArray.map(permission => {
        const permissionObject = configs.enums.adminPermissions.find(
          adminPermission => adminPermission.en === permission,
        );
        const permissionLabel = permissionObject.ar ? permissionObject.ar : permissionObject.en;

        return {
          value: permission,
          label: permissionLabel,
        };
      });

    this.state = {
      name: '',
      username: '',
      email: '',
      password: '',
      permissions: selectedPermissions || null,
    };
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangePermissionsSelect = permissions => {
    this.setState({ permissions });
  };

  onSubmitCreateAdminForm = e => {
    e.preventDefault();

    const { createAdmin: createAdminAction } = this.props;
    const { name, username, email, password, permissions } = this.state;
    const adminPermissions = permissions && permissions.map(permission => permission.value);

    const newAdminInfo = {
      name,
      username: convertArabicNumbersToEnglish(username),
      email,
      password,
      permissions: adminPermissions || [],
    };

    createAdminAction(newAdminInfo);
  };

  onSubmitAddPermissionsToUserForm = e => {
    e.preventDefault();

    const {
      addPermissionsToUser: addPermissionsToUserAction,
      adminDetails: { _id },
    } = this.props;
    const { permissions } = this.state;
    const adminPermissions = permissions && permissions.map(permission => permission.value);

    addPermissionsToUserAction(_id, adminPermissions || []);
  };

  render() {
    const {
      adminDetails,
      isCreateAdminFetching,
      isAddPermissionsToUserFetching,
      isAddEditAdminModalOpen,
      toggleAddEditAdminModal: toggleAddEditAdminModalAction,
      isCreateAdminError,
      isAddPermissionsToUserError,
      createAdminErrorMessage,
      addPermissionsToUserErrorMessage,
    } = this.props;
    const { name, username, email, password, permissions } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addEditAdminModalHeader = adminDetails
      ? 'تعديل صلاحيات مدير النظام'
      : 'أضف مدير نظام جديد';
    const sumbitFn = adminDetails
      ? this.onSubmitAddPermissionsToUserForm
      : this.onSubmitCreateAdminForm;

    return (
      <Modal
        toggleModal={toggleAddEditAdminModalAction}
        header={addEditAdminModalHeader}
        isOpened={isAddEditAdminModalOpen}
      >
        <AddEditAdminFormContainer onSubmit={sumbitFn}>
          <Flex flexDirection="column">
            {!adminDetails && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    اسم مدير النظام
                  </Text>
                  <InputField
                    type="text"
                    placeholder="اسم مدير النظام"
                    ref={inputField => {
                      this.nameField = inputField;
                    }}
                    width={1}
                    value={name}
                    onChange={value => this.handleInputFieldChange('name', value)}
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    رقم الجوال
                  </Text>
                  <InputField
                    type="text"
                    placeholder="رقم الجوال"
                    ref={inputField => {
                      this.userNameField = inputField;
                    }}
                    width={1}
                    value={username}
                    onChange={value => this.handleInputFieldChange('username', value)}
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            )}
            {!adminDetails && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    البريد الالكتروني
                  </Text>
                  <InputField
                    type="text"
                    placeholder="البريد الالكتروني"
                    ref={inputField => {
                      this.emailField = inputField;
                    }}
                    width={1}
                    value={email}
                    onChange={value => this.handleInputFieldChange('email', value)}
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
                <Flex flexDirection="column" width={1}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    كلمة المرور
                  </Text>
                  <InputField
                    type="password"
                    placeholder="كلمة المرور"
                    ref={inputField => {
                      this.passwordField = inputField;
                    }}
                    width={1}
                    value={password}
                    onChange={value => this.handleInputFieldChange('password', value)}
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            )}
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الصلاحيات
                </Text>
                <UserPermissionsDropDown
                  mb={3}
                  isMulti
                  userPermissionsSelectedValue={permissions}
                  handleInputChange={value => this.handleChangePermissionsSelect(value)}
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={sumbitFn}
                disabled={isCreateAdminFetching || isAddPermissionsToUserFetching}
                isLoading={isCreateAdminFetching || isAddPermissionsToUserFetching}
              >
                حفظ
              </Button>
              {isCreateAdminError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {createAdminErrorMessage}
                </Caution>
              )}
              {adminDetails && isAddPermissionsToUserError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addPermissionsToUserErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddEditAdminFormContainer>
      </Modal>
    );
  }
}
AddEditAdminModal.displayName = 'AddEditAdminModal';

const mapStateToProps = state => ({
  configs: state.configs.configs,
  isCreateAdminFetching: state.user.createAdmin.isFetching,
  isCreateAdminError: state.user.createAdmin.isFail.isError,
  createAdminErrorMessage: state.user.createAdmin.isFail.message,
  isAddPermissionsToUserFetching: state.user.addPermissionsToUser.isFetching,
  isAddPermissionsToUserError: state.user.addPermissionsToUser.isFail.isError,
  addPermissionsToUserErrorMessage: state.user.addPermissionsToUser.isFail.message,
  isAddEditAdminModalOpen: state.user.addEditAdminModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createAdmin,
      addPermissionsToUser,
      toggleAddEditAdminModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddEditAdminModal);
