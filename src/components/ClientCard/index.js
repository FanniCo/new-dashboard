import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import {
  faHandPaper,
  faCommentDots,
  faPen,
  faExchangeAlt,
  faDollarSign,
} from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ROUTES from 'routes';
import { CITIES } from 'utils/constants';
import {
  CardBody,
  SubTitle,
  CardLabel,
  CardFooter,
  CallToActionIcon,
  MoreDetailsLink,
} from '../shared';

const ClientCard = props => {
  const {
    index,
    user: { permissions },
    client,
    client: {
      _id,
      active,
      name,
      username,
      lastRequestCity,
      requestsCount,
      doneRequestsCount,
      suspendedTo,
      comments,
      createdAt,
      canceledRequestsCount,
      postponedRequestsCount,
      activeRequestsCount,
      reviewedRequestsCount,
      pendingRequestsCount,
      timedOutRequestsCount,
    },
    handleToggleConfirmModal,
    handleToggleOpsCommentsModal,
    handleToggleEditClientModal,
    handleToggleConvertClientToWorkerModal,
    handleSuspendClient,
    handleToggleAddCreditModal,
    handleToggleSuspendModal,
  } = props;
  const { SINGLE_CLIENT } = ROUTES;
  const { GREY_LIGHT, DISABLED, BRANDING_BLUE, BRANDING_GREEN, LIGHT_RED, PRIMARY } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const unSuspendConfirmModalHeader = 'تاكيد الغاء وقف الفنى';
  const unSuspendConfirmModalConfirmText = 'هل انت  متاكيد انك تريد الغاء وقف الفنى';
  const unSuspendConfirmModalFunction = () => {
    handleSuspendClient(_id, index, new Date());
  };
  const clientOpsCommentsTooltip = 'تعليقات العمليات';
  const editClientTooltip = 'تعديل العميل';
  const convertClientToWorkerTooltip = 'تحويل إلي فني';
  const addCreditTooltip = 'أضف رصيد';
  const createdAtDate = new Date(createdAt);
  return (
    <Card
      {...props}
      flexDirection="column"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} mb={1} width={1}>
          <Flex alignItems="center">
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {username}
            </CardLabel>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <Text type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {name}
            </Text>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تاريخ الإنشاء :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {`[ ${createdAtDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })} ]`}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={2}>
          {lastRequestCity ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                مدينة أخر طلب :
              </SubTitle>
              <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                {CITIES[lastRequestCity].ar}
              </Text>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد مدينة
            </SubTitle>
          )}
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {requestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المنجزة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {doneRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات تم تقيمها :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {reviewedRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {canceledRequestsCount + timedOutRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المؤجلة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {postponedRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات النشطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {activeRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات فى انتظار اعتمادها :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {pendingRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد التعليقات :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {comments ? comments.length : 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={2}>
          <CardLabel py={1} px={2} type={BODY} color={COLORS_VALUES[DISABLED]} bg={BRANDING_BLUE}>
            {suspendedTo && Date.parse(new Date(suspendedTo)) >= Date.parse(new Date())
              ? 'متوقف'
              : 'غير متوقف'}
          </CardLabel>
          <CardLabel
            py={1}
            px={2}
            mr={1}
            type={BODY}
            color={COLORS_VALUES[DISABLED]}
            bg={BRANDING_BLUE}
          >
            {active ? 'نشط' : 'غير نشط'}
          </CardLabel>
        </Flex>
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="space-between" p={2}>
        <Flex px={2}>
          {permissions &&
            permissions.includes('Suspend or unSuspend Customers') &&
            (!suspendedTo || Date.parse(new Date(suspendedTo)) < Date.parse(new Date())) && (
              <>
                <ReactTooltip id={`suspendClient_${_id}`} type="error" effect="solid">
                  <span>وقف العميل</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`suspendWorker_${_id}`}
                    data-tip
                    onClick={() => handleToggleSuspendModal(index, client)}
                    icon={faHandPaper}
                    color={COLORS_VALUES[!suspendedTo ? DISABLED : LIGHT_RED]}
                    size="sm"
                  />
                </Box>
              </>
          )}
          {permissions &&
            permissions.includes('Suspend or unSuspend Customers') &&
            suspendedTo &&
            Date.parse(new Date(suspendedTo)) > Date.parse(new Date()) && (
              <>
                <ReactTooltip id={`suspendClient${_id}`} type="error" effect="solid">
                  <span>الغاء وقف العميل</span>
                </ReactTooltip>
                <Box mr={3}>
                  <CallToActionIcon
                    data-for={`suspendClient${_id}`}
                    data-tip
                    onClick={() =>
                      handleToggleConfirmModal(
                        unSuspendConfirmModalHeader,
                        unSuspendConfirmModalConfirmText,
                        unSuspendConfirmModalFunction,
                      )
                    }
                    icon={faHandPaper}
                    color={
                      COLORS_VALUES[
                        suspendedTo && Date.parse(new Date(suspendedTo)) > Date.parse(new Date())
                          ? DISABLED
                          : LIGHT_RED
                      ]
                    }
                    size="sm"
                  />
                </Box>
              </>
          )}
          <ReactTooltip id={`userOpsComment_${_id}`} type="info" effect="solid">
            <span>{clientOpsCommentsTooltip}</span>
          </ReactTooltip>
          <Box mr={3}>
            <CallToActionIcon
              data-for={`userOpsComment_${_id}`}
              data-tip
              onClick={() => handleToggleOpsCommentsModal(index)}
              icon={faCommentDots}
              color={COLORS_VALUES[DISABLED]}
              size="sm"
            />
          </Box>
          {permissions && permissions.includes('Convert Customer To Worker') && (
            <>
              <ReactTooltip id={`convertClientToWorker_${_id}`} type="info" effect="solid">
                <span>{convertClientToWorkerTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`convertClientToWorker_${_id}`}
                  data-tip
                  onClick={() => handleToggleConvertClientToWorkerModal(index, client)}
                  icon={faExchangeAlt}
                  color={COLORS_VALUES[PRIMARY]}
                  size="sm"
                />
              </Box>
            </>
          )}
          {permissions && permissions.includes('Update Customers') && (
            <>
              <ReactTooltip id={`editClient_${_id}`} type="light" effect="solid">
                <span>{editClientTooltip}</span>
              </ReactTooltip>
              <Box mr={3}>
                <CallToActionIcon
                  data-for={`editClient_${_id}`}
                  data-tip
                  onClick={() => handleToggleEditClientModal(index, client)}
                  icon={faPen}
                  color={COLORS_VALUES[DISABLED]}
                  size="sm"
                />
              </Box>
            </>
          )}
          {permissions && permissions.includes('Add Credits To Customer') && (
            <>
              <ReactTooltip id={`addCredit_${_id}`} type="light" effect="solid">
                <span>{addCreditTooltip}</span>
              </ReactTooltip>
              <Box>
                <CallToActionIcon
                  data-for={`addCredit_${_id}`}
                  data-tip
                  onClick={() => handleToggleAddCreditModal(index, client)}
                  icon={faDollarSign}
                  color={COLORS_VALUES[DISABLED]}
                  size="sm"
                />
              </Box>
            </>
          )}
        </Flex>
        <MoreDetailsLink to={`${SINGLE_CLIENT}/${username}`} href={`${SINGLE_CLIENT}/${username}`}>
          <Text
            ml={1}
            cursor="pointer"
            type={BODY}
            color={COLORS_VALUES[BRANDING_GREEN]}
            fontWeight={NORMAL}
          >
            {'<<<< للتفاصيل'}
          </Text>
        </MoreDetailsLink>
      </CardFooter>
    </Card>
  );
};
ClientCard.displayName = 'ClientCard';

ClientCard.propTypes = {
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}),
  client: PropTypes.shape({}).isRequired,
  handleSuspendClient: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleOpsCommentsModal: PropTypes.func,
  handleToggleEditClientModal: PropTypes.func,
  handleToggleConvertClientToWorkerModal: PropTypes.func,
  handleToggleAddCreditModal: PropTypes.func,

  handleToggleSuspendModal: PropTypes.func,
};

ClientCard.defaultProps = {
  user: undefined,
  handleSuspendClient: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleOpsCommentsModal: () => {},
  handleToggleEditClientModal: () => {},
  handleToggleConvertClientToWorkerModal: () => {},
  handleToggleAddCreditModal: () => {},

  handleToggleSuspendModal: () => {},
};

export default ClientCard;
