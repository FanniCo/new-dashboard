import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { width } from 'styled-system';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import Cookies from 'js-cookie';
import { COOKIES_KEYS } from 'utils/constants';
import { decodeJwtToken } from 'utils/shared/encryption';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { Subheading } from 'components/Text/BackwardTypography';
import DropDown from 'components/DropDown';
import { ShimmerEffect, Rect, Circle } from 'components/ShimmerEffect';
import { NoAnimationFlex } from 'components/ShimmerEffect/NoAnimationContainer';
import UserPreview from 'components/UserPreview';
import Image from 'components/Image';
import Logo from 'static/images/logo.png';
import NotificationCenter from 'components/NotificationCenter';

const HeaderStyle = styled.header`
  align-items: center;
  background-color: ${props => props.theme.colors.greyDark};
  box-shadow: 0 0px 8px 0px rgba(0,0,0,0.2);
  /*border-bottom: 1px solid ${props => props.theme.colors.greyLight};*/
  display: flex;
  flex-direction: row-reverse;
  height: 65px;
  justify-content: flex-end;
  padding-left: ${props => props.theme.space[4]}px;
  position: fixed;
  z-index: 2;
  width: 100%;
`;

const HeaderLogo = styled.div`
  cursor: pointer;
  background-color: ${props => props.theme.colors.greyDark};
  display: flex;
  align-items: center;
  justify-content: flex-end;
  height: 100%;
  width:100%;
  padding: 0;
  position: relative;
  /*border-left: 1px solid ${props => props.theme.colors.greyLight};*/
`;

const HeaderLogoImage = styled.img`
  ${width}
  margin: 0 15px;
`;

const UserName = styled(Subheading)`
  color: ${props => props.theme.colors.disabled};
  font-weight: ${props => props.theme.fontWeights.NORMAL};
  margin: 0 ${props => props.theme.space[3]}px;
`;

const UserInfoContainer = styled.div`
  display: flex;
  align-items: center;
`;

const UserInfoContainerLoading = styled(NoAnimationFlex)`
  display: flex;
  align-items: center;
`;

const UserPhotoLoading = styled(Circle)`
  display: flex;
  align-items: center;
`;

const UserNameLoading = styled(Rect)`
  display: flex;
  align-items: center;
  margin-left: ${props => props.theme.space[3]}px;
`;

const Header = props => {
  const { onLogoClick, logout } = props;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);
  const {
    payLoad: { name, username },
  } = decodeJwtToken(token);
  const userName = name;
  const userNumber = username;

  let userDetails;

  if (userName && !userName.includes('undefined')) {
    const items = [
      {
        component: <UserPreview name={userName} username={userNumber} showImage={false} />,
      },
      {
        isDivider: true,
      },
      {
        icon: faSignOutAlt,
        text: 'تسجيل الخروج',
        onClick: logout,
      },
    ];

    userDetails = (
      <DropDown
        items={items}
        component={
          <Fragment>
            <Image
              alt="admin"
              bgColor={COLORS_VALUES[COLORS.DISABLED]}
              showImage={false}
              radius={30}
              name={userName}
            />
            <UserName>{userName}</UserName>
          </Fragment>
        }
      />
    );
  } else {
    userDetails = (
      <ShimmerEffect>
        <UserInfoContainerLoading>
          <UserPhotoLoading />
          <UserNameLoading />
        </UserInfoContainerLoading>
      </ShimmerEffect>
    );
  }

  return (
    <HeaderStyle className='HeaderContainer'>
      <HeaderLogo className='HeaderLogo'>
        <HeaderLogoImage src={Logo}  onClick={onLogoClick} alt="logo" width={['25px', '50px', '50px', '50px', '50px']} />
      </HeaderLogo>
      <NotificationCenter />
      <UserInfoContainer className='header_user'>{userDetails}</UserInfoContainer>
    </HeaderStyle>
  );
};

Header.propTypes = {
  onLogoClick: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

export default Header;
