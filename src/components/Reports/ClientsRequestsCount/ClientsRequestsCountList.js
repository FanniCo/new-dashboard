import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import ClientRequestsCountCard from './ClientRequestsCountCard';

const { BRANDING_GREEN } = COLORS;

const ClientsRequestsCountListFlex = styled(Flex)`
  ${minHeight};
`;

class ClientsRequestsCountList extends Component {
  getLoadingClientsRequestsCount = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingClientsRequestsCountList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingClientsRequestsCount(counter));
    }
    return list;
  };

  createClientsRequestsCountList = clients => {
    if (clients.length) {
      return clients.map(client => {
        const { _id } = client;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <ClientRequestsCountCard m={2} client={client} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const {
      clientsRequestsCountList,
      loadMoreClientsRequestsCount,
      isLoadMoreClientsRequestsCountAvailable,
      applyNewFilter,
    } = this.props;
    const ClientsRequestsCountLoadingList = this.createLoadingClientsRequestsCountList();
    const isDataLoaded = clientsRequestsCountList && !applyNewFilter;

    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        {isDataLoaded ? (
          <InfiniteScroll
            dataLength={clientsRequestsCountList.length}
            next={loadMoreClientsRequestsCount}
            hasMore={isLoadMoreClientsRequestsCountAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <ClientsRequestsCountListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createClientsRequestsCountList(clientsRequestsCountList)}
            </ClientsRequestsCountListFlex>
          </InfiniteScroll>
        ) : (
          ClientsRequestsCountLoadingList
        )}
      </Box>
    );
  }
}
ClientsRequestsCountList.displayName = 'ClientsRequestsCountList';

ClientsRequestsCountList.propTypes = {
  clientsRequestsCountList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreClientsRequestsCountAvailable: PropTypes.bool,
  loadMoreClientsRequestsCount: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

ClientsRequestsCountList.defaultProps = {
  clientsRequestsCountList: undefined,
  isLoadMoreClientsRequestsCountAvailable: true,
  loadMoreClientsRequestsCount: () => {},
  applyNewFilter: false,
};

export default ClientsRequestsCountList;
