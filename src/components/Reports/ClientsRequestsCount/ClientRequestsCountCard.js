import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, HyperLink } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const ClientRequestsCountCard = props => {
  const {
    client: {
      date,
      username,
      name,
      doneRequestsCount,
      cancelledRequestsCount,
      requestsCount,
      privateRequestsCount,
      donePrivateRequestsCount,
      cancelledPrivateRequestsCount,
    },
  } = props;
  const { SINGLE_CLIENT } = ROUTES;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <HyperLink href={`${SINGLE_CLIENT}/${username}`} target="_blank">
              <CardLabel
                cursor="pointer"
                pt="6px"
                pb={1}
                px={2}
                mx={2}
                type={BODY}
                border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                {username}
              </CardLabel>
            </HyperLink>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <HyperLink href={`${SINGLE_CLIENT}/${username}`} target="_blank">
              <Text
                cursor="pointer"
                type={HEADING}
                color={COLORS_VALUES[BRANDING_GREEN]}
                fontWeight={NORMAL}
              >
                {name}
              </Text>
            </HyperLink>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            سجل بتاريخ :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {date}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المكتملة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {doneRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledRequestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى عدد الطلبات :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {requestsCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الخاصة المكتملة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {donePrivateRequestsCount || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الخاصة الملغية :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledPrivateRequestsCount || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى عدد الطلبات الخاصة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {privateRequestsCount || 0}
          </Text>
        </Flex>
      </CardBody>
    </Card>
  );
};
ClientRequestsCountCard.displayName = 'ClientRequestsCountCard';

ClientRequestsCountCard.propTypes = {
  client: PropTypes.shape({}).isRequired,
};

export default ClientRequestsCountCard;
