import React from 'react';
import PropTypes from 'prop-types';
import ClientsRequestsCountList from './ClientsRequestsCountList';

const ClientsRequestsCount = props => {
  const {
    clientsRequestsCountList,
    isLoadMoreClientsRequestsCountAvailable,
    loadMoreClientsRequestsCount,
    applyNewFilter,
  } = props;

  return (
    <ClientsRequestsCountList
      clientsRequestsCountList={clientsRequestsCountList}
      isLoadMoreClientsRequestsCountAvailable={isLoadMoreClientsRequestsCountAvailable}
      loadMoreClientsRequestsCount={loadMoreClientsRequestsCount}
      applyNewFilter={applyNewFilter}
    />
  );
};
ClientsRequestsCount.displayName = 'ClientsRequestsCount';

ClientsRequestsCount.propTypes = {
  clientsRequestsCountList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreClientsRequestsCountAvailable: PropTypes.bool,
  loadMoreClientsRequestsCount: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

ClientsRequestsCount.defaultProps = {
  clientsRequestsCountList: undefined,
  isLoadMoreClientsRequestsCountAvailable: true,
  loadMoreClientsRequestsCount: () => {},
  applyNewFilter: false,
};

export default ClientsRequestsCount;
