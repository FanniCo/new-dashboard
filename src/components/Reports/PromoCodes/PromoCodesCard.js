import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, CardLabel, HyperLink, SubTitle } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getFieldIcon } from 'utils/shared/style';
import { CITIES, FIELDS } from 'utils/constants';

const PromoCodesCard = props => {
  const {
    promo: {
      code,
      promoCodeUseCount,
      totalOrdersCompleted,
      totalWorkerWages,
      totalCompanyCommission,
      workerCount,
      cities,
      fields,
      workersRefund,
      companyBenefitsAfterRefund,
      discount,
      totalCustomersBenefitsFromPromo,
    },
  } = props;
  const { SINGLE_CLIENT } = ROUTES;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, BRANDING_BLUE } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const fieldsContent = fields.map(fieldKey => {
    const fieldIcon = getFieldIcon(fieldKey);

    return (
      <Flex key={fieldKey} ml={4} mb={1}>
        <Box>
          <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
        </Box>
        <CardLabel
          py={1}
          px={2}
          ml={2}
          type={BODY}
          border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          {FIELDS[fieldKey].ar}
        </CardLabel>
      </Flex>
    );
  });
  const citiesContent = cities.map((city, index) => (
    <CardLabel
      key={`city_${index}`}
      py={1}
      px={2}
      ml={1}
      mb={1}
      type={BODY}
      color={COLORS_VALUES[DISABLED]}
      bg={BRANDING_BLUE}
    >
      {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
    </CardLabel>
  ));
  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <HyperLink href={`${SINGLE_CLIENT}/${code}`} target="_blank">
              <CardLabel
                cursor="pointer"
                pt="6px"
                pb={1}
                px={2}
                mx={2}
                type={BODY}
                border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                {code}
              </CardLabel>
            </HyperLink>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {`${discount.value}${discount.type === 'percentage' ? '%' : ' ريال'}`}
            </SubTitle>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              {discount.type === 'voucher' ? 'المبلغ سوف يضاف لى العميل فى المحفظة' : 'الخصم'} :
            </SubTitle>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد مرات استخدام الكود :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {promoCodeUseCount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المكتملة باستخدام الكود :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalOrdersCompleted}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى اجرة الفنين :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalWorkerWages}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى عمولة الشركة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {Math.abs(totalCompanyCommission)}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الفنين :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {workerCount || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى تعويضات الفنين :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {workersRefund || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى عدد المستفادين من البرومو :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalCustomersBenefitsFromPromo || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عمولة الشركة بعد تعويض الفنين :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {companyBenefitsAfterRefund || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            المدن :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {citiesContent}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            التخصوصات :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {fieldsContent}
          </Text>
        </Flex>
      </CardBody>
    </Card>
  );
};
PromoCodesCard.displayName = 'PromoCodesCard';

PromoCodesCard.propTypes = {
  promo: PropTypes.shape({}).isRequired,
};

export default PromoCodesCard;
