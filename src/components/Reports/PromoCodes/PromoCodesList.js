import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import PromoCodesCard from './PromoCodesCard';

const { BRANDING_GREEN } = COLORS;

const ClientsPaymentsListFlex = styled(Flex)`
  ${minHeight};
`;

class PromoCodesList extends Component {
  getLoadingPromoCodes = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of clients payments block
   */
  createLoadingPromoCodesList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingPromoCodes(counter));
    }
    return list;
  };

  createPromoCodesList = promoCodes => {
    if (promoCodes.length) {
      return promoCodes.map(promo => {
        const { _id } = promo;
        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <PromoCodesCard m={2} promo={promo} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const { promoCodesList, loadMorePromoCodes, isLoadMorePromoCodes } = this.props;
    const ClientsRequestsCountLoadingList = this.createLoadingPromoCodesList();
    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        {promoCodesList ? (
          <InfiniteScroll
            dataLength={promoCodesList.length}
            next={loadMorePromoCodes}
            hasMore={isLoadMorePromoCodes}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <ClientsPaymentsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createPromoCodesList(promoCodesList)}
            </ClientsPaymentsListFlex>
          </InfiniteScroll>
        ) : (
          ClientsRequestsCountLoadingList
        )}
      </Box>
    );
  }
}
PromoCodesList.displayName = 'PromoCodesList';

PromoCodesList.propTypes = {
  promoCodesList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMorePromoCodes: PropTypes.bool,
  loadMorePromoCodes: PropTypes.func,
};

PromoCodesList.defaultProps = {
  promoCodesList: undefined,
  isLoadMorePromoCodes: true,
  loadMorePromoCodes: () => {},
};

export default PromoCodesList;
