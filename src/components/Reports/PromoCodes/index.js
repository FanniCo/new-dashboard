import React from 'react';
import PropTypes from 'prop-types';
import PromoCodesList from './PromoCodesList';

const PromoCodesComponent = props => {
  const { promoCodesList, isLoadMorePromoCodes, loadMorePromoCodes, applyNewFilter } = props;
  return (
    <PromoCodesList
      promoCodesList={promoCodesList}
      isLoadMorePromoCodes={isLoadMorePromoCodes}
      loadMorePromoCodes={loadMorePromoCodes}
      applyNewFilter={applyNewFilter}
    />
  );
};
PromoCodesComponent.displayName = 'PromoCodesComponent';

PromoCodesComponent.propTypes = {
  promoCodesList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMorePromoCodes: PropTypes.bool,
  loadMorePromoCodes: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

PromoCodesComponent.defaultProps = {
  promoCodesList: undefined,
  isLoadMorePromoCodes: true,
  loadMorePromoCodes: () => {},
  applyNewFilter: false,
};

export default PromoCodesComponent;
