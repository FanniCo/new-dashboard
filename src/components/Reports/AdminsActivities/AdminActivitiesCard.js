import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, CardLabel, SubTitle } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const AdminActivitiesCard = props => {
  const {
    adminActivities: {
      username,
      name,
      activateWorker,
      addCommentOnClient,
      addCommentOnRequests,
      addCommentOnWorker,
      addCompensationToWorker,
      addCreditsToClient,
      addCreditsToWorker,
      cancelRequest,
      changeAdminPassword,
      changeClientName,
      changeWorkerCity,
      changeWorkerField,
      changeWorkerName,
      changeWorkerPassword,
      createRequest,
      deleteReview,
      deleteWorker,
      disableAutoApplyToRequests,
      enableAutoApplyToRequests,
      featuredWorker,
      followUpsRequest,
      hideReview,
      openTickets,
      signUpWorker,
      solvedTickets,
      suspendClient,
      suspendWorker,
      unsuspendWorker,
    },
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} mb={1} width={1}>
          <Flex alignItems="center">
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {username}
            </CardLabel>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <Text type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {name}
            </Text>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تنشيط فنى :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {activateWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة تعليق على حساب العميل :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {addCommentOnClient}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة تعليق على حساب الفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {addCommentOnWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة تعليق على الطلب :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {addCommentOnRequests}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة تعويض للفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {addCompensationToWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة رصيد للعميل:
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {addCreditsToClient}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة رصيد للفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {addCreditsToWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الغاء طلب :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {cancelRequest}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تغير باسورد الادمن :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {changeAdminPassword}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تغير اسم العميل :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {changeClientName}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تغير مدينة الفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {changeWorkerCity}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تغيير تخصص الفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {changeWorkerField}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تغير اسم الفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {changeWorkerName}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تغير باسورد الفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {changeWorkerPassword}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اضافة طلب جديد من لوحة التحكم :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {createRequest}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            حذف تعليق :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {deleteReview}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            حذف فنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {deleteWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الغاء التقديم التلقائى على الطلبات للفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {disableAutoApplyToRequests}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تفعيل التقديم التلقائى على الطلبات للفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {enableAutoApplyToRequests}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تمييز الفنى :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {featuredWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            متابعة الطلب :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {followUpsRequest}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اخفاء تعليق :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {hideReview}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            فتح مشكلة جديده :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {openTickets}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تسجيل فنى جديد :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {signUpWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            حل المشكلة :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {solvedTickets}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            ايقاف العميل من استخدام النظام :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {suspendClient}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            ايقاف الفنى من استخدام النظام:
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {suspendWorker}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الغاء ايقاف الفنى من استخدام النظام :
          </SubTitle>
          <Text color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {unsuspendWorker}
          </Text>
        </Flex>
      </CardBody>
    </Card>
  );
};
AdminActivitiesCard.displayName = 'AdminActivitiesCard';

AdminActivitiesCard.propTypes = {
  adminActivities: PropTypes.shape({}).isRequired,
};

AdminActivitiesCard.defaultProps = {};

export default AdminActivitiesCard;
