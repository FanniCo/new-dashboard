import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import EmptyState from 'components/EmptyState';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { FONT_TYPES } from 'components/theme/fonts';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import AdminActivitiesCard from './AdminActivitiesCard';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class AdminsActivities extends Component {
  getLoadingAdminsActivities = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingAdminsActivitiesList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingAdminsActivities(counter));
    }
    return list;
  };

  render() {
    const {
      adminsActivitiesList,
      isGetAllAdminsActivitiesFetching,
      isGetAllAdminsActivitiesSuccess,
      isGetAllAdminsActivitiesError,
      isGetAllAdminsActivitiesErrorMessage,
      applyNewFilter,
    } = this.props;

    let adminActivitiesRenderer;

    if (
      (isGetAllAdminsActivitiesFetching && (!adminsActivitiesList || applyNewFilter)) ||
      (!isGetAllAdminsActivitiesFetching &&
        !isGetAllAdminsActivitiesSuccess &&
        !isGetAllAdminsActivitiesError)
    ) {
      adminActivitiesRenderer = this.createLoadingAdminsActivitiesList();
    } else if (isGetAllAdminsActivitiesError) {
      adminActivitiesRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={isGetAllAdminsActivitiesErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!adminsActivitiesList.length) {
      adminActivitiesRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (adminsActivitiesList.length) {
      adminActivitiesRenderer = adminsActivitiesList.map(adminActivities => {
        const { _id } = adminActivities;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <AdminActivitiesCard m={2} adminActivities={adminActivities} />
          </Box>
        );
      });
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {adminActivitiesRenderer}
      </Flex>
    );
  }
}

AdminsActivities.displayName = 'AdminsActivitiesList';

AdminsActivities.propTypes = {
  adminsActivitiesList: PropTypes.arrayOf(PropTypes.shape({})),
  isGetAllAdminsActivitiesFetching: PropTypes.bool,
  isGetAllAdminsActivitiesSuccess: PropTypes.bool,
  isGetAllAdminsActivitiesError: PropTypes.bool,
  isGetAllAdminsActivitiesErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
};

AdminsActivities.defaultProps = {
  adminsActivitiesList: undefined,
  isGetAllAdminsActivitiesFetching: false,
  isGetAllAdminsActivitiesSuccess: false,
  isGetAllAdminsActivitiesError: false,
  isGetAllAdminsActivitiesErrorMessage: undefined,
  applyNewFilter: false,
};

export default AdminsActivities;
