import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import TransactionCard from 'components/TransactionCard';

const { BRANDING_GREEN } = COLORS;

const TransactionsListFlex = styled(Flex)`
  ${minHeight};
`;

class TransactionsList extends Component {
  getLoadingTransactions = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests payments block
   */
  createLoadingTransactionsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingTransactions(counter));
    }
    return list;
  };

  createTransactionsList = transactionsList => {
    if (transactionsList.length) {
      return transactionsList.map(transaction => {
        const { _id } = transaction;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <TransactionCard m={2} transaction={transaction} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const {
      transactionsList,
      loadMoreTransactions,
      isLoadMoreTransactionsAvailable,
      applyNewFilter,
    } = this.props;
    const shipmentsLoadingList = this.createLoadingTransactionsList(3);
    const isDataLoaded = transactionsList && !applyNewFilter;

    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        {isDataLoaded ? (
          <InfiniteScroll
            dataLength={transactionsList.length}
            next={loadMoreTransactions}
            hasMore={isLoadMoreTransactionsAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <TransactionsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createTransactionsList(transactionsList)}
            </TransactionsListFlex>
          </InfiniteScroll>
        ) : (
          shipmentsLoadingList
        )}
      </Box>
    );
  }
}
TransactionsList.displayName = 'TransactionsList';

TransactionsList.propTypes = {
  transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
  loadMoreTransactions: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

TransactionsList.defaultProps = {
  transactionsList: undefined,
  loadMoreTransactions: () => {},
  applyNewFilter: false,
};

export default TransactionsList;
