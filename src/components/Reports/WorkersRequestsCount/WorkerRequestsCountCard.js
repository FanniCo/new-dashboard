import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { CITIES, FIELDS } from 'utils/constants';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, HyperLink } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const WorkerRequestsCountCard = props => {
  const {
    workerRequestsCount: {
      cancelledHighPrice,
      cancelledJustCheckingTheApp,
      cancelledNeedNewField,
      cancelledTechnicianNotSuitable,
      cancelledTechnicianRequestedCancellation,
      cancelledTechnicianWasANoShow,
      postponedThenDone,
      postponedThenCancelled,
      city,
      fields,
      createdAt,
      name: workerName,
      numberOfMotivations,
      numberOfPunishments,
      totalOrdersCompleted,
      totalPaid,
      username,
      _id: workerId,
      reviewed,
      rate,
      reviewLessThanTwoStar,
      suspendedTo,
    },
  } = props;
  const { SINGLE_WORKER } = ROUTES;
  const { BRANDING_GREEN, GREY_DARK, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const createdAtDateString = new Date(createdAt).toLocaleDateString('ar-EG', {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  });

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {FIELDS[fields[0]].ar}
            </CardLabel>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <HyperLink href={`${SINGLE_WORKER}/${workerId}`} target="_blank">
              <Text
                cursor="pointer"
                type={BODY}
                color={COLORS_VALUES[BRANDING_GREEN]}
                fontWeight={NORMAL}
              >
                {`[${createdAtDateString}] - [ ${username} ] - ${workerName}`}
              </Text>
            </HyperLink>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المكتملة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalOrdersCompleted}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية بسبب السعر مرتفع :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledHighPrice}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية بسبب تجربة التطبيق :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledJustCheckingTheApp}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية بسبب يحتاج خدمة جديدة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledNeedNewField}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية بسبب الفنى غير مناسب :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledTechnicianNotSuitable}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية بسبب الفنى طلب الالغاء :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledTechnicianRequestedCancellation}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات الملغية بسبب الفنى تاخر :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {cancelledTechnicianWasANoShow}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المؤجلة ثم اكتملت :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {postponedThenDone}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المؤجلة ثم الغت :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {postponedThenCancelled}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الحوافز :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {numberOfMotivations}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد العقبات :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {numberOfPunishments}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            إجمالي المبلغ المدفوع مقابل الطلبات المكتملة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalPaid}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد التقيمات:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {reviewed}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            التقيم:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {rate}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد التقيمات اقل من 2:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {reviewLessThanTwoStar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الفنى موقوف حتى تاريخ:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {suspendedTo &&
              new Date(suspendedTo).toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={2}>
          <CardLabel py={1} px={2} type={BODY} color={COLORS_VALUES[GREY_DARK]} bg={BRANDING_GREEN}>
            {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
          </CardLabel>
        </Flex>
      </CardBody>
    </Card>
  );
};
WorkerRequestsCountCard.displayName = 'WorkerRequestsCountCard';

WorkerRequestsCountCard.propTypes = {
  workerRequestsCount: PropTypes.shape({}).isRequired,
};

export default WorkerRequestsCountCard;
