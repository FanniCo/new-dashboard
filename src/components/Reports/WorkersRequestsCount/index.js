import React from 'react';
import PropTypes from 'prop-types';
import WorkersRequestsCountList from './WorkersRequestsCountList';

const WorkersRequestsCount = props => {
  const {
    workersRequestsCountList,
    workersRequestsCountListState,
    isLoadMoreWorkersRequestsCountAvailable,
    loadMoreWorkersRequestsCount,
    applyNewFilter,
  } = props;

  return (
    <WorkersRequestsCountList
      workersRequestsCountList={workersRequestsCountList}
      workersRequestsCountListState={workersRequestsCountListState}
      isLoadMoreWorkersRequestsCountAvailable={isLoadMoreWorkersRequestsCountAvailable}
      loadMoreWorkersRequestsCount={loadMoreWorkersRequestsCount}
      applyNewFilter={applyNewFilter}
    />
  );
};
WorkersRequestsCount.displayName = 'WorkersRequestsCount';

WorkersRequestsCount.propTypes = {
  workersRequestsCountList: PropTypes.arrayOf(PropTypes.shape({})),
  workersRequestsCountListState: PropTypes.shape({}),
  isLoadMoreWorkersRequestsCountAvailable: PropTypes.bool,
  loadMoreWorkersRequestsCount: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

WorkersRequestsCount.defaultProps = {
  workersRequestsCountList: undefined,
  workersRequestsCountListState: undefined,
  isLoadMoreWorkersRequestsCountAvailable: true,
  loadMoreWorkersRequestsCount: () => {},
  applyNewFilter: false,
};

export default WorkersRequestsCount;
