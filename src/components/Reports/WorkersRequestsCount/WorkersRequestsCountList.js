import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import WorkerRequestsCountCard from './WorkerRequestsCountCard';

const { BRANDING_GREEN } = COLORS;

const WorkersPaymentsListFlex = styled(Flex)`
  ${minHeight};
`;

class WorkersRequestsCountList extends Component {
  getLoadingWorkersRequests = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests payments block
   */
  createLoadingWorkersRequestsCountList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingWorkersRequests(counter));
    }
    return list;
  };

  createWorkersRequestsCountList = workersRequestsList => {
    if (workersRequestsList.length) {
      return workersRequestsList.map(workerRequestsCount => {
        const { _id } = workerRequestsCount;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <WorkerRequestsCountCard m={2} workerRequestsCount={workerRequestsCount} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const {
      workersRequestsCountList,
      loadMoreWorkersRequestsCount,
      isLoadMoreWorkersRequestsCountAvailable,
      applyNewFilter,
    } = this.props;
    const requestsPaymentsLoadingList = this.createLoadingWorkersRequestsCountList();
    const isDataLoaded = workersRequestsCountList && !applyNewFilter;

    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        {isDataLoaded ? (
          <InfiniteScroll
            dataLength={workersRequestsCountList.length}
            next={loadMoreWorkersRequestsCount}
            hasMore={isLoadMoreWorkersRequestsCountAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <WorkersPaymentsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createWorkersRequestsCountList(workersRequestsCountList)}
            </WorkersPaymentsListFlex>
          </InfiniteScroll>
        ) : (
          requestsPaymentsLoadingList
        )}
      </Box>
    );
  }
}
WorkersRequestsCountList.displayName = 'WorkersRequestsCountList';

WorkersRequestsCountList.propTypes = {
  workersRequestsCountList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreWorkersRequestsCountAvailable: PropTypes.bool,
  loadMoreWorkersRequestsCount: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

WorkersRequestsCountList.defaultProps = {
  workersRequestsCountList: undefined,
  isLoadMoreWorkersRequestsCountAvailable: true,
  loadMoreWorkersRequestsCount: () => {},
  applyNewFilter: false,
};

export default WorkersRequestsCountList;
