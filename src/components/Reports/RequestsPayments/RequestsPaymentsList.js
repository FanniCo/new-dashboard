import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import RequestPaymentCard from './RequestPaymentCard';

const { BRANDING_GREEN } = COLORS;

const RequestsPaymentsListFlex = styled(Flex)`
  ${minHeight};
`;

class RequestsPaymentsList extends Component {
  getLoadingRequestsPayments = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of requests payments block
   */
  createLoadingRequestsPaymentsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingRequestsPayments(counter));
    }
    return list;
  };

  createRequestsPaymentsList = requestsPaymentsList => {
    if (requestsPaymentsList.length) {
      return requestsPaymentsList.map(requestPayment => {
        const { _id } = requestPayment;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <RequestPaymentCard m={2} requestPayment={requestPayment} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const {
      requestsPaymentsList,
      loadMoreRequestsPayments,
      isLoadMoreRequestsPaymentsAvailable,
      applyNewFilter,
    } = this.props;
    const requestsPaymentsLoadingList = this.createLoadingRequestsPaymentsList();
    const isDataLoaded = requestsPaymentsList && !applyNewFilter;

    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        {isDataLoaded ? (
          <InfiniteScroll
            dataLength={requestsPaymentsList.length}
            next={loadMoreRequestsPayments}
            hasMore={isLoadMoreRequestsPaymentsAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <RequestsPaymentsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createRequestsPaymentsList(requestsPaymentsList)}
            </RequestsPaymentsListFlex>
          </InfiniteScroll>
        ) : (
          requestsPaymentsLoadingList
        )}
      </Box>
    );
  }
}
RequestsPaymentsList.displayName = 'RequestsPaymentsList';

RequestsPaymentsList.propTypes = {
  requestsPaymentsList: PropTypes.arrayOf(PropTypes.shape({})),
  requestsPaymentsListState: PropTypes.shape({}),
  isLoadMoreRequestsPaymentsAvailable: PropTypes.bool,
  loadMoreRequestsPayments: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

RequestsPaymentsList.defaultProps = {
  requestsPaymentsList: undefined,
  requestsPaymentsListState: undefined,
  isLoadMoreRequestsPaymentsAvailable: true,
  loadMoreRequestsPayments: () => {},
  applyNewFilter: false,
};

export default RequestsPaymentsList;
