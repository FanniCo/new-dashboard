import React from 'react';
import PropTypes from 'prop-types';
import RequestsPaymentsList from './RequestsPaymentsList';

const RequestsPayments = props => {
  const {
    requestsPaymentsList,
    requestsPaymentsListState,
    isLoadMoreRequestsPaymentsAvailable,
    loadMoreRequestsPayments,
    applyNewFilter,
  } = props;

  return (
    <RequestsPaymentsList
      requestsPaymentsList={requestsPaymentsList}
      requestsPaymentsListState={requestsPaymentsListState}
      isLoadMoreRequestsPaymentsAvailable={isLoadMoreRequestsPaymentsAvailable}
      loadMoreRequestsPayments={loadMoreRequestsPayments}
      applyNewFilter={applyNewFilter}
    />
  );
};
RequestsPayments.displayName = 'RequestsPayments';

RequestsPayments.propTypes = {
  requestsPaymentsList: PropTypes.arrayOf(PropTypes.shape({})),
  requestsPaymentsListState: PropTypes.shape({}),
  isLoadMoreRequestsPaymentsAvailable: PropTypes.bool,
  loadMoreRequestsPayments: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

RequestsPayments.defaultProps = {
  requestsPaymentsList: undefined,
  requestsPaymentsListState: undefined,
  isLoadMoreRequestsPaymentsAvailable: true,
  loadMoreRequestsPayments: () => {},
  applyNewFilter: false,
};

export default RequestsPayments;
