import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { CITIES, FIELDS, PAYMENT_METHOD } from 'utils/constants';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, HyperLink, HyperLinkText } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const RequestPaymentCard = props => {
  const {
    requestPayment: {
      field,
      date,
      requestPrettyId,
      customerId,
      customerName,
      workerId,
      workerName,
      city,
      workersWage,
      partsCost,
      total,
      paymentStatus,
      paymentStatuses,
      paymentMethod,
      vatValue,
      commission,
      totalAfterDiscount,
      onlinePaymentRef,
      moneyTakenFromCustomerCredits,
      discount,
      deliveryCostOfParts,
    },
  } = props;
  const { SINGLE_REQUEST, SINGLE_WORKER, SINGLE_CLIENT } = ROUTES;
  const { BRANDING_GREEN, GREY_DARK, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const requestPaymentDate = new Date(date);

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <HyperLink href={`${SINGLE_REQUEST}/${requestPrettyId}`} target="_blank">
              <CardLabel
                cursor="pointer"
                pt="6px"
                pb={1}
                px={2}
                mx={2}
                type={BODY}
                border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                {requestPrettyId}
              </CardLabel>
            </HyperLink>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <Text type={BODY} mx={1} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {`[ ${requestPaymentDate.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })} ]`}
            </Text>
            <Text type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {` - ${FIELDS[field].ar}`}
            </Text>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          {workerId ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الفني :
              </SubTitle>
              <HyperLink href={`${SINGLE_WORKER}/${workerId}`} target="_blank">
                <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  [ {workerId} ] - {workerName}
                </HyperLinkText>
              </HyperLink>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لم يتم اختيار فني
            </SubTitle>
          )}
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          {customerId ? (
            <>
              <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                العميل :
              </SubTitle>
              <HyperLink href={`${SINGLE_CLIENT}/${customerId}`} target="_blank">
                <HyperLinkText cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  [ {customerId} ] - {customerName}
                </HyperLinkText>
              </HyperLink>
            </>
          ) : (
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              لا يوجد عميل
            </SubTitle>
          )}
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            أجرة الفني :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {workersWage}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تكلفة القطع :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {partsCost}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            قيمة الضريبة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {vatValue}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالي التكلفة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {total}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            قيمة الخصم :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {discount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            المبلغ المحصل من محفظة العميل :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {moneyTakenFromCustomerCredits}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            المبلغ الذي قام العميل بدفعه :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalAfterDiscount}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العمولة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {commission}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            مبلغ توصيل قطع الغيار :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {deliveryCostOfParts}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عمولة الشركة فى توصيل قطع الغيار :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {deliveryCostOfParts / 2}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            مرجع الدفع عبر الإنترنت :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {onlinePaymentRef}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={2}>
          <CardLabel py={1} px={2} type={BODY} color={COLORS_VALUES[GREY_DARK]} bg={BRANDING_GREEN}>
            {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
          </CardLabel>
          <CardLabel
            py={1}
            px={2}
            mr={1}
            type={BODY}
            color={COLORS_VALUES[GREY_DARK]}
            bg={BRANDING_GREEN}
          >
            {(paymentStatuses.length &&
              paymentStatuses[paymentStatuses.length - 1].status === 'Done') ||
            paymentStatus
              ? 'تم الدفع'
              : 'لم يتم الدفع'}
          </CardLabel>
          {paymentMethod && (
            <CardLabel
              py={1}
              px={2}
              mr={1}
              type={BODY}
              color={COLORS_VALUES[GREY_DARK]}
              bg={BRANDING_GREEN}
            >
              {PAYMENT_METHOD[paymentMethod].ar}
            </CardLabel>
          )}
        </Flex>
      </CardBody>
    </Card>
  );
};
RequestPaymentCard.displayName = 'RequestPaymentCard';

RequestPaymentCard.propTypes = {
  requestPayment: PropTypes.shape({}).isRequired,
};

export default RequestPaymentCard;
