import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import _ from 'lodash';
import { CITIES, FIELDS, TICKETS_STATUS } from 'utils/constants';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, HyperLink } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getFieldIcon } from 'utils/shared/style';

const TicketCard = props => {
  const {
    ticket: { request, statuses, ticketPrettyId },
  } = props;
  const { SINGLE_TICKET, SINGLE_REQUEST } = ROUTES;
  const { BRANDING_GREEN, GREY_DARK, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <HyperLink href={`${SINGLE_TICKET}/${ticketPrettyId}`} target="_blank">
              <CardLabel
                cursor="pointer"
                pt="6px"
                pb={1}
                px={2}
                mx={2}
                type={BODY}
                border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                {ticketPrettyId}
              </CardLabel>
            </HyperLink>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            رقم الطلب:
          </SubTitle>
          <HyperLink href={`${SINGLE_REQUEST}/${request.requestPrettyId}`} target="_blank">
            <Text
              cursor="pointer"
              type={HEADING}
              color={COLORS_VALUES[BRANDING_GREEN]}
              fontWeight={NORMAL}
            >
              {request.requestPrettyId}
            </Text>
          </HyperLink>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            وقت و تاريخ فتح الشكوى:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {statuses.find(status => status.status === TICKETS_STATUS.open.code)
              ? new Date(
                _.get(
                  statuses.find(status => status.status === TICKETS_STATUS.open.code),
                  'createdAt',
                ),
              ).toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })
              : '-'}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            وقت وتاريخ تحويل الشكوى لى جارى الحل:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {statuses.find(status => status.status === TICKETS_STATUS.solving.code)
              ? new Date(
                _.get(
                  statuses.find(status => status.status === TICKETS_STATUS.solving.code),
                  'createdAt',
                ),
              ).toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
              })
              : '-'}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            وقت وتاريخ اغلاق الشكوى:
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {statuses.find(status => status.status === TICKETS_STATUS.solved.code)
              ? new Date(
                _.get(
                  _.findLast(statuses, status => status.status === TICKETS_STATUS.solved.code),
                  'createdAt',
                ),
              ).toLocaleDateString('en-US', {
                  year: 'numeric',
                month: 'numeric',
                  day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
                })
              : '-'}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Flex ml={4} mb={1}>
            <Box>
              <FontAwesomeIcon
                icon={getFieldIcon(request.field)}
                color={COLORS_VALUES[BRANDING_GREEN]}
                size="sm"
              />
            </Box>
            <CardLabel
              py={1}
              px={2}
              ml={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {FIELDS[request.field] ? FIELDS[request.field].ar : 'غير معرف'}
            </CardLabel>
          </Flex>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            <CardLabel
              py={1}
              px={2}
              type={BODY}
              color={COLORS_VALUES[GREY_DARK]}
              bg={BRANDING_GREEN}
            >
              {CITIES[request.city] ? CITIES[request.city].ar : 'غير معرف'}
            </CardLabel>
          </Text>
        </Flex>
      </CardBody>
    </Card>
  );
};
TicketCard.displayName = 'TicketCard';

TicketCard.propTypes = {
  ticket: PropTypes.shape({}).isRequired,
};

export default TicketCard;
