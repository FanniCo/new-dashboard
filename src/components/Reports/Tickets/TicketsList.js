import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import EmptyState from 'components/EmptyState';
import { FONT_TYPES } from 'components/theme/fonts';
import TicketCard from './TicketCard';

const { BRANDING_GREEN, GREY_DARK, WHITE, GREY_LIGHT } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

const TicketsListFlex = styled(Flex)`
  ${minHeight};
`;

class TicketsList extends Component {
  getLoadingTickets = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of clients payments block
   */
  createLoadingTicketsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingTickets(counter));
    }
    return list;
  };

  createTicketsList = ticketsList => {
    if (ticketsList.length) {
      return ticketsList.map(ticket => {
        const { _id } = ticket;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <TicketCard m={2} ticket={ticket} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const {
      ticketsList,
      loadMoreTickets,
      isLoadMoreTicketsAvailable,
      ticketsListState,
    } = this.props;

    if (ticketsListState.isFetching) {
      return (
        <Box width={1} flexWrap="wrap" flexDirection="column">
          {this.createLoadingTicketsList()}
        </Box>
      );
    }

    if (ticketsListState.isFail.isError) {
      return (
        <Box width={1} flexWrap="wrap" flexDirection="column">
          <Card
            minHeight={500}
            ml={[0, 0, 0, 0, 0]}
            mb={3}
            flexDirection="column"
            bgColor={COLORS_VALUES[GREY_DARK]}
          >
            <Flex m={2} justifyContent="center" alignItems="center">
              <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                <EmptyState
                  icon={faFile}
                  iconColor={BRANDING_GREEN}
                  iconSize="3x"
                  textColor={COLORS_VALUES[WHITE]}
                  textSize={BIG_TITLE}
                  text={ticketsListState.isFail.message}
                />
              </Card>
            </Flex>
          </Card>
        </Box>
      );
    }

    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        <InfiniteScroll
          dataLength={ticketsList.length}
          next={loadMoreTickets}
          hasMore={isLoadMoreTicketsAvailable}
          loader={
            <Flex m={2} justifyContent="center" alignItems="center">
              <FontAwesomeIcon
                icon={faSpinner}
                size="lg"
                spin
                color={COLORS_VALUES[BRANDING_GREEN]}
              />
            </Flex>
          }
        >
          <TicketsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
            {this.createTicketsList(ticketsList)}
          </TicketsListFlex>
        </InfiniteScroll>
      </Box>
    );
  }
}
TicketsList.displayName = 'TicketsList';

TicketsList.propTypes = {
  ticketsList: PropTypes.arrayOf(PropTypes.shape({})),
  ticketsListState: PropTypes.shape({}),
  isLoadMoreTicketsAvailable: PropTypes.bool,
  loadMoreTickets: PropTypes.func,
};

TicketsList.defaultProps = {
  ticketsList: [],
  ticketsListState: undefined,
  isLoadMoreTicketsAvailable: true,
  loadMoreTickets: () => {},
};

export default TicketsList;
