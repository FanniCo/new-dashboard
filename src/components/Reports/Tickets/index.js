import React from 'react';
import PropTypes from 'prop-types';
import TicketsList from './TicketsList';

const Tickets = props => {
  const {
    ticketsList,
    ticketsListState,
    isLoadMoreTicketsAvailable,
    loadMoreTickets,
    applyNewFilter,
  } = props;

  return (
    <TicketsList
      ticketsList={ticketsList}
      ticketsListState={ticketsListState}
      isLoadMoreTicketsAvailable={isLoadMoreTicketsAvailable}
      loadMoreTickets={loadMoreTickets}
      applyNewFilter={applyNewFilter}
    />
  );
};
Tickets.displayName = 'Tickets';

Tickets.propTypes = {
  ticketsList: PropTypes.arrayOf(PropTypes.shape({})),
  ticketsListState: PropTypes.shape({}),
  isLoadMoreTicketsAvailable: PropTypes.bool,
  loadMoreTickets: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

Tickets.defaultProps = {
  ticketsList: undefined,
  ticketsListState: undefined,
  isLoadMoreTicketsAvailable: true,
  loadMoreTickets: () => {},
  applyNewFilter: false,
};

export default Tickets;
