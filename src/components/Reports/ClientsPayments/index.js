import React from 'react';
import PropTypes from 'prop-types';
import ClientsPaymentsList from './ClientsPaymentsList';

const ClientsPayments = props => {
  const {
    clientsPaymentsList,
    clientsPaymentsListState,
    isLoadMoreClientsPaymentsAvailable,
    loadMoreClientsPayments,
    applyNewFilter,
  } = props;

  return (
    <ClientsPaymentsList
      clientsPaymentsList={clientsPaymentsList}
      clientsPaymentsListState={clientsPaymentsListState}
      isLoadMoreClientsPaymentsAvailable={isLoadMoreClientsPaymentsAvailable}
      loadMoreClientsPayments={loadMoreClientsPayments}
      applyNewFilter={applyNewFilter}
    />
  );
};
ClientsPayments.displayName = 'ClientsPayments';

ClientsPayments.propTypes = {
  clientsPaymentsList: PropTypes.arrayOf(PropTypes.shape({})),
  clientsPaymentsListState: PropTypes.shape({}),
  isLoadMoreClientsPaymentsAvailable: PropTypes.bool,
  loadMoreClientsPayments: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

ClientsPayments.defaultProps = {
  clientsPaymentsList: undefined,
  clientsPaymentsListState: undefined,
  isLoadMoreClientsPaymentsAvailable: true,
  loadMoreClientsPayments: () => {},
  applyNewFilter: false,
};

export default ClientsPayments;
