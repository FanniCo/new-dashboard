import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { CITIES } from 'utils/constants';
import ROUTES from 'routes';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardLabel, HyperLink } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const ClientPaymentCard = props => {
  const {
    clientPayment: { city, completedRequests, customerId, customerName, totalPaid },
  } = props;
  const { SINGLE_CLIENT } = ROUTES;
  const { BRANDING_GREEN, GREY_DARK, GREY_LIGHT, DISABLED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={1} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} width={1}>
          <Flex alignItems="center">
            <HyperLink href={`${SINGLE_CLIENT}/${customerId}`} target="_blank">
              <CardLabel
                cursor="pointer"
                pt="6px"
                pb={1}
                px={2}
                mx={2}
                type={BODY}
                border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                {customerId}
              </CardLabel>
            </HyperLink>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <HyperLink href={`${SINGLE_CLIENT}/${customerId}`} target="_blank">
              <Text
                cursor="pointer"
                type={HEADING}
                color={COLORS_VALUES[BRANDING_GREEN]}
                fontWeight={NORMAL}
              >
                {customerName}
              </Text>
            </HyperLink>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            عدد الطلبات المكتملة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {completedRequests}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            إجمالي المبلغ المدفوع مقابل الطلبات المكتملة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {totalPaid}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mt={1} mb={2}>
          <CardLabel py={1} px={2} type={BODY} color={COLORS_VALUES[GREY_DARK]} bg={BRANDING_GREEN}>
            {CITIES[city] ? CITIES[city].ar : 'غير معرف'}
          </CardLabel>
        </Flex>
      </CardBody>
    </Card>
  );
};
ClientPaymentCard.displayName = 'ClientPaymentCard';

ClientPaymentCard.propTypes = {
  clientPayment: PropTypes.shape({}).isRequired,
};

export default ClientPaymentCard;
