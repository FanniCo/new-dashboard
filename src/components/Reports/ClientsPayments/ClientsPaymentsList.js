import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import ClientPaymentCard from './ClientPaymentCard';

const { BRANDING_GREEN } = COLORS;

const ClientsPaymentsListFlex = styled(Flex)`
  ${minHeight};
`;

class ClientsPaymentsList extends Component {
  getLoadingClientsPayments = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  /**
   * Render multiple loading list of clients payments block
   */
  createLoadingClientsPaymentsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingClientsPayments(counter));
    }
    return list;
  };

  createClientsPaymentsList = clientsPaymentsList => {
    if (clientsPaymentsList.length) {
      return clientsPaymentsList.map(clientPayment => {
        const { _id } = clientPayment;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <ClientPaymentCard m={2} clientPayment={clientPayment} />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const {
      clientsPaymentsList,
      loadMoreClientsPayments,
      isLoadMoreClientsPaymentsAvailable,
      applyNewFilter,
    } = this.props;
    const clientsPaymentsLoadingList = this.createLoadingClientsPaymentsList();
    const isDataLoaded = clientsPaymentsList && !applyNewFilter;

    return (
      <Box width={1} flexWrap="wrap" flexDirection="column">
        {isDataLoaded ? (
          <InfiniteScroll
            dataLength={clientsPaymentsList.length}
            next={loadMoreClientsPayments}
            hasMore={isLoadMoreClientsPaymentsAvailable}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <ClientsPaymentsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createClientsPaymentsList(clientsPaymentsList)}
            </ClientsPaymentsListFlex>
          </InfiniteScroll>
        ) : (
          clientsPaymentsLoadingList
        )}
      </Box>
    );
  }
}
ClientsPaymentsList.displayName = 'ClientsPaymentsList';

ClientsPaymentsList.propTypes = {
  clientsPaymentsList: PropTypes.arrayOf(PropTypes.shape({})),
  clientsPaymentsListState: PropTypes.shape({}),
  isLoadMoreClientsPaymentsAvailable: PropTypes.bool,
  loadMoreClientsPayments: PropTypes.func,
  applyNewFilter: PropTypes.bool,
};

ClientsPaymentsList.defaultProps = {
  clientsPaymentsList: undefined,
  clientsPaymentsListState: undefined,
  isLoadMoreClientsPaymentsAvailable: true,
  loadMoreClientsPayments: () => {},
  applyNewFilter: false,
};

export default ClientsPaymentsList;
