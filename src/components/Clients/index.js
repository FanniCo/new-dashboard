import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import ClientCard from 'components/ClientCard';
import ConfirmModal from 'components/ConfirmModal';
import OpsCommentsModal from 'components/OpsCommentsModal';
import EditClientModal from 'components/EditClientModal';
import ConvertClientToWorkerModal from 'components/ConvertClientToWorkerModal';
import AddCreditToClientModal from 'components/AddCreditToClientModal';
import SuspendModal from 'components/SuspendModal';

const Clients = props => {
  const {
    user,
    user: { permissions },
    clients,
    clientIndex,
    clientDetails,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleToggleEditClientModal,
    handleToggleConvertClientToWorkerModal,
    handleSuspendClient,
    isSuspendClientFetching,
    isSuspendClientError,
    isOpsCommentsModalOpen,
    handleToggleOpsCommentsModal,
    isEditClientModalOpen,
    isConvertClientToWorkerModalOpen,
    handleToggleAddCreditModal,
    isAddCreditModalOpen,

    handleToggleSuspendModal,
    isSuspendModalOpen,
    suspendClientErrorMessage,
  } = props;

  const clientsRenderer = clients.map((client, index) => {
    const { _id } = client;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <ClientCard
          m={2}
          index={index}
          user={user}
          client={client}
          handleToggleConfirmModal={handleToggleConfirmModal}
          handleToggleOpsCommentsModal={handleToggleOpsCommentsModal}
          handleToggleEditClientModal={handleToggleEditClientModal}
          handleToggleConvertClientToWorkerModal={handleToggleConvertClientToWorkerModal}
          handleSuspendClient={handleSuspendClient}
          isSuspendClientFetching={isSuspendClientFetching}
          isSuspendClientError={isSuspendClientError}
          handleToggleAddCreditModal={handleToggleAddCreditModal}
          handleToggleSuspendModal={handleToggleSuspendModal}
        />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {clientsRenderer}
      {isConfirmModalOpen && (
        <ConfirmModal
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={isSuspendClientFetching}
        />
      )}
      {isOpsCommentsModalOpen && (
        <OpsCommentsModal
          objectType="client"
          object={clients[clientIndex]}
          hideAddComment
          isOpened={isOpsCommentsModalOpen}
          toggleFunction={handleToggleOpsCommentsModal}
          objectIndex={clientIndex}
          hideAddCommentSection={!(permissions && permissions.includes('Add New User Comments'))}
        />
      )}
      {isEditClientModalOpen && (
        <EditClientModal clientIndex={clientIndex} clientDetails={clientDetails} />
      )}
      {isConvertClientToWorkerModalOpen && (
        <ConvertClientToWorkerModal clientIndex={clientIndex} clientDetails={clientDetails} />
      )}
      {isAddCreditModalOpen && (
        <AddCreditToClientModal clientIndex={clientIndex} clientDetails={clientDetails} />
      )}
      {isSuspendModalOpen && (
        <SuspendModal
          userIndex={clientIndex}
          userDetails={clientDetails}
          handleToggleSuspendUserModal={handleToggleSuspendModal}
          isOpened={isSuspendModalOpen}
          suspendUser={handleSuspendClient}
          suspendUserErrorMessage={suspendClientErrorMessage}
          isSuspendUserFetching={isSuspendClientFetching}
          isSuspendUserError={isSuspendClientError}
        />
      )}
    </Flex>
  );
};
Clients.displayName = 'Clients';

Clients.propTypes = {
  user: PropTypes.shape({}),
  clients: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  clientIndex: PropTypes.number,
  clientDetails: PropTypes.shape({}),
  isConfirmModalOpen: PropTypes.bool,
  isEditClientModalOpen: PropTypes.bool,
  isConvertClientToWorkerModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleSuspendClient: PropTypes.func,
  handleToggleOpsCommentsModal: PropTypes.func,
  handleToggleEditClientModal: PropTypes.func,
  handleToggleConvertClientToWorkerModal: PropTypes.func,
  isSuspendClientFetching: PropTypes.bool,
  isSuspendClientError: PropTypes.bool,
  isOpsCommentsModalOpen: PropTypes.bool,
  handleToggleAddCreditModal: PropTypes.func,
  isAddCreditModalOpen: PropTypes.bool,
  handleToggleSuspendModal: PropTypes.func,
  isSuspendModalOpen: PropTypes.bool,
  suspendClientErrorMessage: PropTypes.string,
};

Clients.defaultProps = {
  user: undefined,
  clientDetails: undefined,
  clientIndex: 0,
  isConfirmModalOpen: false,
  isEditClientModalOpen: false,
  isConvertClientToWorkerModalOpen: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleSuspendClient: () => {},
  handleToggleOpsCommentsModal: () => {},
  handleToggleEditClientModal: () => {},
  handleToggleConvertClientToWorkerModal: () => {},
  isSuspendClientFetching: false,
  isSuspendClientError: false,
  isOpsCommentsModalOpen: false,
  handleToggleAddCreditModal: () => {},
  isAddCreditModalOpen: false,
  handleToggleSuspendModal: () => {},
  isSuspendModalOpen: false,
  suspendClientErrorMessage: undefined,
};

export default Clients;
