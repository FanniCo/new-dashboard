import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { updateAppConfig } from 'redux-modules/appConfigs/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const EditAppConfigsFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class EditAppConfigsModal extends Component {
  static propTypes = {
    appConfigDetails: PropTypes.object,
    isUpdateAppConfigsModalOpen: PropTypes.bool,
    updateAppConfig: PropTypes.func,
    isUpdateAppConfigsFetching: PropTypes.bool,
    isUpdateAppConfigsSuccess: PropTypes.bool,
    isUpdateAppConfigsError: PropTypes.bool,
    updateAppConfigsErrorMessage: PropTypes.string,
    handleToggleEditAppConfigModal: PropTypes.func,
  };

  static defaultProps = {
    appConfigDetails: undefined,
    isUpdateAppConfigsModalOpen: false,
    updateAppConfig: () => {},
    isUpdateAppConfigsFetching: false,
    isUpdateAppConfigsSuccess: false,
    isUpdateAppConfigsError: false,
    updateAppConfigsErrorMessage: undefined,
    handleToggleEditAppConfigModal: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      iosForceUpdate: null,
      iosRecommendedUpdate: null,
      androidForceUpdate: null,
      androidRecommendedUpdate: null,
    };
  }

  componentDidMount(): void {
    const { appConfigDetails } = this.props;

    if (appConfigDetails) {
      const { iosFanni, androidFanni } = appConfigDetails;
      this.setState({
        iosForceUpdate: iosFanni.forceUpdate,
        iosRecommendedUpdate: iosFanni.recommendedUpdate,
        androidForceUpdate: androidFanni.forceUpdate,
        androidRecommendedUpdate: androidFanni.recommendedUpdate,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitAddAppConfigsForm = e => {
    e.preventDefault();

    const { updateAppConfig: updateAppConfigsAction } = this.props;

    const {
      iosForceUpdate,
      iosRecommendedUpdate,
      androidForceUpdate,
      androidRecommendedUpdate,
    } = this.state;

    const appConfigsInfo = {
      iosFanni: {
        forceUpdate: iosForceUpdate,
        recommendedUpdate: iosRecommendedUpdate,
      },
      androidFanni: {
        forceUpdate: androidForceUpdate,
        recommendedUpdate: androidRecommendedUpdate,
      },
    };

    return updateAppConfigsAction(appConfigsInfo);
  };

  render() {
    const {
      isUpdateAppConfigsModalOpen,
      isUpdateAppConfigsFetching,
      isUpdateAppConfigsSuccess,
      isUpdateAppConfigsError,
      updateAppConfigsErrorMessage,
      handleToggleEditAppConfigModal,
    } = this.props;

    const {
      iosForceUpdate,
      iosRecommendedUpdate,
      androidForceUpdate,
      androidRecommendedUpdate,
    } = this.state;

    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE, SUCCESS } = COLORS;
    const { HEADING } = FONT_TYPES;
    const updateAppConfigsModalHeader = 'تعديل اصدارات التطبيقات';
    const submitButtonText = 'تعديل';

    return (
      <Modal
        toggleModal={() => {
          handleToggleEditAppConfigModal();
        }}
        header={updateAppConfigsModalHeader}
        isOpened={isUpdateAppConfigsModalOpen}
      >
        <EditAppConfigsFormContainer onSubmit={this.onSubmitAddAppConfigsForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اصدار التحديث الاجباري لتطبيق الاندرويد
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="اصدار التحديث الاجباري لتطبيق الاندرويد"
                  width={1}
                  mb={2}
                  value={androidForceUpdate}
                  onChange={value => this.handleInputFieldChange('androidForceUpdate', value)}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اصدار التحديث الاختياري لتطبيق الاندرويد
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="اصدار التحديث الاختياري لتطبيق الاندرويد"
                  width={1}
                  mb={2}
                  value={androidRecommendedUpdate}
                  onChange={value => this.handleInputFieldChange('androidRecommendedUpdate', value)}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اصدار التحديث الاجباري لتطبيق IOS
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="اصدار التحديث الاجباري لتطبيق IOS"
                  width={1}
                  mb={2}
                  value={iosForceUpdate}
                  onChange={value => this.handleInputFieldChange('iosForceUpdate', value)}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اصدار التحديث الاختياري لتطبيق IOS
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="text"
                  placeholder="اصدار التحديث الاختياري لتطبيق IOS"
                  width={1}
                  mb={2}
                  value={iosRecommendedUpdate}
                  onChange={value => this.handleInputFieldChange('iosRecommendedUpdate', value)}
                  autoComplete="on"
                />
              </Flex>
            </Flex>

            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddAppConfigsForm}
                disabled={isUpdateAppConfigsFetching}
                isLoading={isUpdateAppConfigsFetching}
              >
                {submitButtonText}
              </Button>
              {isUpdateAppConfigsError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {updateAppConfigsErrorMessage}
                </Caution>
              )}
              {isUpdateAppConfigsSuccess && (
                <Caution mx={2} bgColor={SUCCESS} textColor={WHITE} borderColorProp={SUCCESS}>
                  تم التعديل بنجاح
                </Caution>
              )}
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={LIGHT_RED}
                icon={faChevronLeft}
                reverse
                onClick={handleToggleEditAppConfigModal}
                isLoading={isUpdateAppConfigsFetching}
              >
                اغلاق
              </Button>
            </Flex>
          </Flex>
        </EditAppConfigsFormContainer>
      </Modal>
    );
  }
}

EditAppConfigsModal.displayName = 'EditAppConfigsModal';

const mapStateToProps = state => ({
  isUpdateAppConfigsFetching: state.appConfigs.editAppConfig.isFetching,
  isUpdateAppConfigsError: state.appConfigs.editAppConfig.isFail.isError,
  updateAppConfigsErrorMessage: state.appConfigs.editAppConfig.isFail.message,
  isUpdateAppConfigsSuccess: state.appConfigs.editAppConfig.isSuccess,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateAppConfig,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(EditAppConfigsModal);
