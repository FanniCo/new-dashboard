import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faFile } from '@fortawesome/free-solid-svg-icons';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import TransactionCard from 'components/TransactionCard';

const { WHITE, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

const TransactionsListFlex = styled(Flex)`
  ${minHeight};
`;

class TransactionsList extends Component {
  getLoadingTransactions = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 2} height={250} m={2} />
      <Rect width={1 / 2} height={250} m={2} />
    </ShimmerEffect>
  );

  createTransactionsList = transactionsList => {
    if (transactionsList.length) {
      return transactionsList.map(transaction => {
        const { _id } = transaction;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 2]}>
            <TransactionCard m={2} transaction={transaction} showClientName={false} />
          </Box>
        );
      });
    }

    return (
      <Card width={1} minHeight={600} alignItems="center" backgroundColor={GREY_LIGHT}>
        <EmptyState
          icon={faFile}
          iconColor={BRANDING_GREEN}
          iconSize="3x"
          textColor={COLORS_VALUES[WHITE]}
          textSize={BIG_TITLE}
          text="لا يوجد سجل للعمليات لهذا العميل"
        />
      </Card>
    );
  };

  render() {
    const { transactionsList, loadMoreTransactions, isLoadMoreTransactionsAvailable } = this.props;
    const shipmentsLoadingList = this.getLoadingTransactions(3);
    const isDataLoaded = transactionsList;

    return (
      <Box width={1} p={3} flexWrap="wrap" flexDirection="column">
        {isDataLoaded ? (
          <InfiniteScroll
            dataLength={transactionsList.length}
            next={loadMoreTransactions}
            hasMore={isLoadMoreTransactionsAvailable}
            height={600}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <TransactionsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createTransactionsList(transactionsList)}
            </TransactionsListFlex>
          </InfiniteScroll>
        ) : (
          shipmentsLoadingList
        )}
      </Box>
    );
  }
}
TransactionsList.displayName = 'TransactionsList';

TransactionsList.propTypes = {
  transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
  loadMoreTransactions: PropTypes.func,
};

TransactionsList.defaultProps = {
  transactionsList: undefined,
  loadMoreTransactions: () => {},
};

export default TransactionsList;
