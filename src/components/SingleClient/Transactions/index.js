import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { getClientTransactions } from 'redux-modules/transactions/actions';
import TransactionsList from './TransactionsList';

class TransactionsContainer extends Component {
  static propTypes = {
    clientId: PropTypes.string,
    transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
    transactionsState: PropTypes.shape({}),
    isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
    getClientTransactions: PropTypes.func,
  };

  static defaultProps = {
    clientId: undefined,
    transactionsList: undefined,
    transactionsState: undefined,
    getClientTransactions: () => {},
  };

  constructor(props) {
    super(props);

    const { clientId } = props;

    this.state = {
      filterQueries: {
        skip: 0,
        limit: 20,
        clientId,
      },
    };
  }

  componentDidMount() {
    const { getClientTransactions: getClientTransactionsAction } = this.props;
    const { filterQueries } = this.state;

    getClientTransactionsAction(filterQueries, true);
  }

  loadMoreTransactions = () => {
    const { getClientTransactions: getClientTransactionsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getClientTransactionsAction(filterQueriesNextState);
      },
    );
  };

  render() {
    const { transactionsList, transactionsState, isLoadMoreTransactionsAvailable } = this.props;

    return (
      <TransactionsList
        transactionsList={transactionsList}
        transactionsState={transactionsState}
        isLoadMoreTransactionsAvailable={isLoadMoreTransactionsAvailable}
        loadMoreTransactions={this.loadMoreTransactions}
      />
    );
  }
}
TransactionsContainer.displayName = 'TransactionsContainer';

const mapStateToProps = state => ({
  transactionsList: state.transactions.clientTransactions,
  transactionsState: state.transactions.getClientTransactions,
  isLoadMoreTransactionsAvailable: state.transactions.hasMoreClientTransactions,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getClientTransactions }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(TransactionsContainer);
