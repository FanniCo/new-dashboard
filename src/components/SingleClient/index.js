import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { borderBottom, color } from 'styled-system';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import Text from 'components/Text';
import ConfirmModal from 'components/ConfirmModal';
import Comments from 'components/Comments';
import EditClientModal from 'components/EditClientModal';
import ConvertClientToWorkerModal from 'components/ConvertClientToWorkerModal';
import defaultProfileImage from 'static/images/defaultProfileImage.png';
import SuspendModal from 'components/SuspendModal';
import AddCreditToClientModal from '../AddCreditToClientModal';
import AddNewRequestModal from '../AddNewRequestModal';
import UserNameRow from './Details/UserNameRow';
import EmailRow from './Details/EmailRow';
import RequestsCountRow from './Details/RequestsCountRow';
import CityRow from './Details/CityRow';
import SuspendedRow from './Details/SuspendedRow';
import CreatedAtDateRow from './Details/CreatedAtDateRow';
import UpdatedAtDateRow from './Details/UpdatedAtDateRow';
import LocationRow from './Details/LocationRow';
import ClientPointsRow from './Details/ClientPointsRow';
import CallToActions from './Details/CallToActions';
import ClientMap from './Details/Map';
import CreditsRow from './Details/CreditsRow';
import ClientRequests from './Requests';
import GenderRow from './Details/GenderRow';
import DateOfBirthRow from './Details/DateOfBirthRow';
import ClientTransactions from './Transactions';

const PageTitle = styled(Flex)`
  direction: rtl;
`;

const TabsContainer = styled(Flex)`
  ${borderBottom};
`;

const Tab = styled(Flex)`
  cursor: pointer;
  width: max-content;
  ${color};
  ${borderBottom};
`;

const CommentsContainer = styled(Flex)`
  direction: rtl;
  background-color: ${COLORS_VALUES[COLORS.GREY_LIGHT]};
`;

const SingleClient = props => {
  const { clientDetails, isGetClientDetailsFetching } = props;
  const { DISABLED, GREY_DARK, BRANDING_GREEN } = COLORS;
  const { TITLE, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  if (isGetClientDetailsFetching || !clientDetails) {
    return (
      <ShimmerEffect width={1}>
        <Rect width={1 / 3} height={550} m={2} />
        <Rect width={2 / 3} height={550} m={2} />
      </ShimmerEffect>
    );
  }

  const {
    handleTabChange,
    tabs,
    activeTab,
    user,
    user: { permissions },
    clientDetails: {
      _id,
      name,
      username,
      email,
      requestsCount,
      lastRequestCity,
      suspendedTo,
      createdAt,
      updatedAt,
      location,
      gender,
      birthDate,
      picURL,
      credits,
      loyaltyPoints,
    },
    openClientLocation,
    isConfirmModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleToggleEditClientModal,
    handleToggleConvertClientToWorkerModal,
    handleSuspendClient,
    isSuspendClientFetching,
    isSuspendClientError,
    addedCommentValue,
    handleAddCommentInputChange,
    resetAddCommentInput,
    addComment,
    addCommentState,
    addCommentErrorMessage,
    isEditClientModalOpen,
    isConvertClientToWorkerModalOpen,
    handleToggleAddCreditModal,
    isAddCreditModalOpen,
    handleToggleAddNewRequestModal,
    isAddNewRequestModalOpen,
    handleToggleSuspendModal,
    isSuspendModalOpen,
    suspendClientErrorMessage,
    admins,
    mentionData,
  } = props;
  const clientCreatedAtDate = new Date(createdAt);
  const clientUpdatedAtDate = new Date(updatedAt);
  const ProfileImage = styled.img`
    width: calc(50% - 25px);
    border-radius: 50%;
  `;
  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      <Helmet>
        <title>اسم العميل : {name}</title>
      </Helmet>
      <Flex flexDirection="column" flexWrap="wrap" width={[1, 1, 1, 2 / 3]} pl={[0, 0, 0, 3, 3]}>
        <Flex flexDirection="row-reverse" width={1}>
          <PageTitle mb={5} pt={5}>
            <Text type={TITLE} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              اسم العميل : {name}
            </Text>
          </PageTitle>
        </Flex>
        <Flex flexDirection="row-reverse" width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <TabsContainer flexDirection="row-reverse" alignItems="center">
              {tabs.map((tab, index) => (
                <Tab
                  key={tab.name}
                  flexDirection="row-reverse"
                  alignItems="center"
                  px={5}
                  py={3}
                  borderBottom={
                    activeTab === index ? `2px solid ${COLORS_VALUES[BRANDING_GREEN]}` : 'none'
                  }
                  onClick={() => handleTabChange(index, tab.drawMap)}
                >
                  <Text
                    cursor="pointer"
                    textAlign="center"
                    color={
                      activeTab === index ? COLORS_VALUES[BRANDING_GREEN] : COLORS_VALUES[DISABLED]
                    }
                    type={HEADING}
                    fontWeight={NORMAL}
                    mx={3}
                  >
                    {tab.name}
                  </Text>
                </Tab>
              ))}
            </TabsContainer>
            <Card
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              {activeTab === 0 && (
                <>
                  <UserNameRow username={username} />
                  <EmailRow email={email} />
                  <RequestsCountRow requestsCount={requestsCount} />
                  <CityRow city={lastRequestCity} />
                  <SuspendedRow suspended={suspendedTo} />
                  <CreatedAtDateRow clientCreatedAtDate={clientCreatedAtDate} />
                  <UpdatedAtDateRow clientUpdatedAtDate={clientUpdatedAtDate} />
                  <LocationRow location={location} openClientLocation={openClientLocation} />
                  <ClientPointsRow clientPoints={loyaltyPoints} />
                  <GenderRow gender={gender} />
                  <DateOfBirthRow dateOfBirth={birthDate} />
                  <CreditsRow credits={credits} />
                </>
              )}
              {activeTab === 1 && <ClientRequests clientId={_id} user={user} />}
              {activeTab === 2 && <ClientTransactions clientId={_id} user={user} />}
            </Card>
          </Box>
        </Flex>
      </Flex>
      <Flex flexDirecion="column" flexWrap="wrap" width={[1, 1, 1, 1 / 3]}>
        <Box width={[1, 1, 1, 1, 1]}>
          <Flex
            flexDirection="row"
            justifyContent="center"
            alignItems="center"
            mb={2}
            px={2}
            py={3}
          >
            <ProfileImage src={picURL || defaultProfileImage} alt="Profile Image" />
          </Flex>
          <CallToActions
            id={_id}
            user={user}
            client={clientDetails}
            suspendedTo={suspendedTo}
            handleToggleConfirmModal={handleToggleConfirmModal}
            handleToggleEditClientModal={handleToggleEditClientModal}
            handleToggleConvertClientToWorkerModal={handleToggleConvertClientToWorkerModal}
            handleSuspendClient={handleSuspendClient}
            isSuspendClientFetching={isSuspendClientFetching}
            isSuspendClientError={isSuspendClientError}
            handleToggleAddCreditModal={handleToggleAddCreditModal}
            handleToggleAddNewRequestModal={handleToggleAddNewRequestModal}
            handleToggleSuspendModal={handleToggleSuspendModal}
            isSuspendModalOpen={isSuspendModalOpen}
          />
          <CommentsContainer width={1} mb={2}>
            <Comments
              isModal={false}
              objectType="client"
              object={clientDetails}
              addedCommentValue={addedCommentValue}
              handleAddCommentInputChange={handleAddCommentInputChange}
              addComment={addComment}
              addCommentState={addCommentState}
              addCommentErrorMessage={addCommentErrorMessage}
              resetAddCommentInput={resetAddCommentInput}
              hideAddCommentSection={
                !(permissions && permissions.includes('Add New User Comments'))
              }
              admins={admins}
              mentionData={mentionData}
            />
          </CommentsContainer>
          {location && <ClientMap location={location} />}
        </Box>
      </Flex>
      <ConfirmModal
        isOpened={isConfirmModalOpen}
        header={confirmModalHeader}
        confirmText={confirmModalText}
        toggleFunction={handleToggleConfirmModal}
        confirmFunction={confirmModalFunction}
        isConfirming={isSuspendClientFetching}
      />
      {isEditClientModalOpen && <EditClientModal clientDetails={clientDetails} />}
      {isConvertClientToWorkerModalOpen && (
        <ConvertClientToWorkerModal clientDetails={clientDetails} />
      )}
      {isAddCreditModalOpen && <AddCreditToClientModal clientDetails={clientDetails} />}
      {isAddNewRequestModalOpen && (
        <AddNewRequestModal location={location} customerId={_id} isShowPromoSection />
      )}
      {isSuspendModalOpen && (
        <SuspendModal
          userIndex={undefined}
          userDetails={clientDetails}
          handleToggleSuspendUserModal={handleToggleSuspendModal}
          isOpened={isSuspendModalOpen}
          suspendUser={handleSuspendClient}
          isSuspendUserError={isSuspendClientError}
          suspendUserErrorMessage={suspendClientErrorMessage}
          isSuspendUserFetching={isSuspendClientFetching}
        />
      )}
    </Flex>
  );
};
SingleClient.displayName = 'SingleClient';

SingleClient.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.shape({})),
  activeTab: PropTypes.number,
  handleTabChange: PropTypes.func,
  user: PropTypes.shape({}),
  clientDetails: PropTypes.shape({}),
  isGetClientDetailsFetching: PropTypes.bool,
  openClientLocation: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isEditClientModalOpen: PropTypes.bool,
  isConvertClientToWorkerModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditClientModal: PropTypes.func,
  handleToggleConvertClientToWorkerModal: PropTypes.func,
  handleSuspendClient: PropTypes.func,
  isSuspendClientFetching: PropTypes.bool,
  isSuspendClientError: PropTypes.bool,
  addCommentState: PropTypes.string,
  addCommentErrorMessage: PropTypes.string,
  addComment: PropTypes.func,
  addedCommentValue: PropTypes.string,
  handleAddCommentInputChange: PropTypes.func,
  resetAddCommentInput: PropTypes.func,
  isAddCreditModalOpen: PropTypes.bool,
  handleToggleAddCreditModal: PropTypes.func,
  handleToggleAddNewRequestModal: PropTypes.func,
  isAddNewRequestModalOpen: PropTypes.bool,
  handleToggleSuspendModal: PropTypes.func,
  isSuspendModalOpen: PropTypes.bool,
  suspendClientErrorMessage: PropTypes.string,
  admins: PropTypes.arrayOf(PropTypes.shape({})),
  mentionData: PropTypes.arrayOf(PropTypes.shape({})),
};

SingleClient.defaultProps = {
  tabs: undefined,
  activeTab: 0,
  handleTabChange: () => {},
  isConfirmModalOpen: false,
  isEditClientModalOpen: false,
  isConvertClientToWorkerModalOpen: false,
  user: undefined,
  clientDetails: undefined,
  isGetClientDetailsFetching: false,
  openClientLocation: () => {},
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleEditClientModal: () => {},
  handleToggleConvertClientToWorkerModal: () => {},
  handleSuspendClient: () => {},
  isSuspendClientFetching: false,
  isSuspendClientError: false,
  addCommentState: undefined,
  addCommentErrorMessage: undefined,
  addComment: () => {},
  addedCommentValue: '',
  handleAddCommentInputChange: () => {},
  resetAddCommentInput: () => {},
  isAddCreditModalOpen: false,
  handleToggleAddCreditModal: () => {},
  handleToggleAddNewRequestModal: () => {},
  isAddNewRequestModalOpen: false,
  handleToggleSuspendModal: () => {},
  isSuspendModalOpen: false,
  suspendClientErrorMessage: undefined,
  admins: [],
  mentionData: [],
};

export default SingleClient;
