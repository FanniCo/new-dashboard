import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { faHandPaper, faPen, faDollarSign } from '@fortawesome/free-solid-svg-icons';
import Card from 'components/Card';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';

const CallToActions = props => {
  const {
    id,
    user: { permissions },
    suspendedTo,
    handleSuspendClient,
    handleToggleConfirmModal,
    handleToggleEditClientModal,
    handleToggleAddCreditModal,
    handleToggleAddNewRequestModal,
    handleToggleSuspendModal,
    client,
  } = props;
  const { GREY_LIGHT, LIGHT_RED, PRIMARY, BRANDING_GREEN, CYAN } = COLORS;
  const suspendClientConfirmModalHeader = 'تأكيد عدم وقف العميل';
  const suspendClientConfirmModalConfirmText = 'هل أنت متأكد أنك تريد عدم وقف العميل؟';
  const suspendClientConfirmModalFunction = () => {
    handleSuspendClient(id, undefined, new Date());
  };

  return (
    <Card
      flexWrap="wrap"
      justifyContent="center"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      mb={2}
      px={2}
      py={3}
    >
      <Flex width={1} mb={2} flexWrap="wrap" flexDirection="row-reverse" alignItems="center">
        <Button
          minWidth="calc(50% - 8px)"
          mx={1}
          mb={2}
          color={PRIMARY}
          onClick={handleToggleEditClientModal}
          icon={faPen}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          تعديل العميل
        </Button>
        {(!suspendedTo || Date.parse(new Date(suspendedTo)) < Date.parse(new Date())) && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={LIGHT_RED}
            onClick={() => handleToggleSuspendModal(undefined, client)}
            icon={faHandPaper}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            وقف العميل
          </Button>
        )}
        {suspendedTo && Date.parse(new Date(suspendedTo)) > Date.parse(new Date()) && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={LIGHT_RED}
            onClick={() =>
              handleToggleConfirmModal(
                suspendClientConfirmModalHeader,
                suspendClientConfirmModalConfirmText,
                suspendClientConfirmModalFunction,
              )
            }
            icon={faHandPaper}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            إلغاء وقف العميل
          </Button>
        )}
        <Button
          minWidth="calc(50% - 8px)"
          mx={1}
          mb={2}
          color={BRANDING_GREEN}
          onClick={handleToggleAddCreditModal}
          icon={faDollarSign}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          أضف رصيد
        </Button>
        {permissions && permissions.includes('Add New Request') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={CYAN}
            onClick={handleToggleAddNewRequestModal}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            طلب خدمة
          </Button>
        )}
      </Flex>
    </Card>
  );
};
CallToActions.displayName = 'CallToActions';

CallToActions.propTypes = {
  id: PropTypes.string.isRequired,
  user: PropTypes.shape({}),
  suspendedTo: PropTypes.string.isRequired,
  handleSuspendClient: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditClientModal: PropTypes.func,
  handleToggleAddCreditModal: PropTypes.func,
  handleToggleAddNewRequestModal: PropTypes.func,
  client: PropTypes.shape({}),
  handleToggleSuspendModal: PropTypes.func,
};

CallToActions.defaultProps = {
  user: undefined,
  handleSuspendClient: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleEditClientModal: () => {},
  handleToggleAddCreditModal: () => {},
  handleToggleAddNewRequestModal: () => {},
  client: undefined,
  handleToggleSuspendModal: () => {},
};

export default CallToActions;
