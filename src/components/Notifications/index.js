import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { USER_TARGETS, OPERATING_SYSTEMS, FIELDS, CITIES } from 'utils/constants';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import Text from 'components/Text';
import InputField from 'components/InputField';
import Button from 'components/Buttons';
import SnackBar from 'components/SnackBar';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';

const targetsKeys = Object.keys(USER_TARGETS);
const targetsSelectOptions = targetsKeys.map(key => ({
  value: key,
  label: USER_TARGETS[key].ar,
}));
const osKeys = Object.keys(OPERATING_SYSTEMS);
const osSelectOptions = osKeys.map(key => ({
  value: key,
  label: OPERATING_SYSTEMS[key].ar,
}));
const citiesKeys = Object.keys(CITIES);
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));
const fieldsKeys = Object.keys(FIELDS);
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
const { DISABLED, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const RtlContainer = styled(Flex)`
  direction: rtl;
`;

const Notifications = props => {
  const {
    targets,
    os,
    cities,
    fields,
    arabicMessage,
    englishMessage,
    isSendNotificationsFetching,
    handleChangeSelect,
    handleChangeMessage,
    sendNotifications,
    notificationsSnackBar,
  } = props;
  const isShowFieldSelect = targets === null || targets.value === 'worker';

  return (
    <RtlContainer flexDirection="column" alignItems="flex-end" width={1}>
      <Helmet>
        <title>التنبيهات</title>
      </Helmet>
      <Flex width={1} py={2} alignItems="center">
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
          التنبيهات
        </Text>
      </Flex>
      <Card flexWrap="wrap" width={1} mb={3} p={4} bgColor={COLORS_VALUES[GREY_LIGHT]}>
        <Box width={[1, 1, 1, 1, 'calc(50% - 8px)']} ml={2}>
          <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
            <Text
              mx={2}
              mb={4}
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              نوع المستخدم
            </Text>
            <StyledSelect
              mb={3}
              placeholder="نوع المستخدم"
              noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
              isRtl
              isClearable
              backspaceRemovesValue={false}
              value={targets}
              onChange={val => handleChangeSelect('targets', val)}
              options={targetsSelectOptions}
              theme={selectColors}
              styles={customSelectStyles}
            />
          </Flex>
          <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
            <Text
              mx={2}
              mb={4}
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              نظام التشغيل
            </Text>
            <StyledSelect
              mb={3}
              placeholder="نظام التشغيل"
              noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
              isMulti
              isRtl
              backspaceRemovesValue={false}
              value={os}
              onChange={val => handleChangeSelect('os', val)}
              options={osSelectOptions}
              theme={selectColors}
              styles={customSelectStyles}
              isClearable
            />
          </Flex>
          <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
            <Text
              mx={2}
              mb={4}
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              المدينة
            </Text>
            <StyledSelect
              mb={3}
              placeholder="المدينة"
              noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
              isMulti
              isRtl
              backspaceRemovesValue={false}
              value={cities}
              onChange={val => handleChangeSelect('cities', val)}
              options={citiesSelectOptions}
              theme={selectColors}
              styles={customSelectStyles}
              isClearable
            />
          </Flex>
          {isShowFieldSelect && (
            <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
              <Text
                mx={2}
                mb={4}
                type={SUBHEADING}
                color={COLORS_VALUES[DISABLED]}
                fontWeight={NORMAL}
              >
                التخصص
              </Text>
              <StyledSelect
                mb={3}
                placeholder="التخصص"
                noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                isMulti
                isRtl
                backspaceRemovesValue={false}
                value={fields}
                onChange={val => handleChangeSelect('fields', val)}
                options={fieldsSelectOptions}
                theme={selectColors}
                styles={customSelectStyles}
                isClearable
              />
            </Flex>
          )}
        </Box>
        <Box width={[1, 1, 1, 1, 'calc(50% - 8px)']} mr={2}>
          <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
            <Text
              mx={2}
              mb={4}
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              محتوي التنبيه (عربي)
            </Text>
            <InputField
              type="textarea"
              placeholder="محتوي التنبيه (عربي)"
              borderColor={DISABLED}
              width={1}
              value={arabicMessage}
              onChange={val => handleChangeMessage('arabicMessage', val)}
              autoComplete="on"
            />
            <Text
              mx={2}
              mt={2}
              mb={4}
              type={SUBHEADING}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              محتوي التنبيه (إنجليزي)
            </Text>
            <InputField
              type="textarea"
              placeholder="محتوي التنبيه (إنجليزي)"
              borderColor={DISABLED}
              width={1}
              value={englishMessage}
              onChange={val => handleChangeMessage('englishMessage', val)}
              mb={2}
              autoComplete="on"
            />
            <Flex flexDirection="row-reverse" width={1}>
              <Button
                primary
                reverse
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                onClick={sendNotifications}
                disabled={
                  isSendNotificationsFetching ||
                  !targets ||
                  !os ||
                  (os && !os.length) ||
                  ((!cities || !cities.length) &&
                    isShowFieldSelect &&
                    (!fields || (fields && !fields.length))) ||
                  !arabicMessage ||
                  !englishMessage
                }
                isLoading={isSendNotificationsFetching}
              >
                إرسال التنبيه
              </Button>
            </Flex>
          </Flex>
        </Box>
      </Card>
      <SnackBar
        show={notificationsSnackBar.show}
        message={notificationsSnackBar.message}
        autoHideTime={3000}
      />
    </RtlContainer>
  );
};
Notifications.displayName = 'Notifications';

Notifications.propTypes = {
  targets: PropTypes.shape({}),
  os: PropTypes.arrayOf(PropTypes.shape({})),
  cities: PropTypes.arrayOf(PropTypes.shape({})),
  fields: PropTypes.arrayOf(PropTypes.shape({})),
  arabicMessage: PropTypes.string,
  englishMessage: PropTypes.string,
  isSendNotificationsFetching: PropTypes.bool,
  handleChangeSelect: PropTypes.func,
  handleChangeMessage: PropTypes.func,
  sendNotifications: PropTypes.func,
  notificationsSnackBar: PropTypes.shape({}),
};

Notifications.defaultProps = {
  targets: undefined,
  os: undefined,
  cities: undefined,
  fields: undefined,
  arabicMessage: '',
  englishMessage: '',
  isSendNotificationsFetching: false,
  handleChangeSelect: () => {},
  handleChangeMessage: () => {},
  sendNotifications: () => {},
  notificationsSnackBar: undefined,
};

export default Notifications;
