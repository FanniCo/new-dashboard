import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Box, Flex} from '@rebass/grid';
import Modal from 'components/Modal';
import Text from 'components/Text';
import {
  COLORS,
  COLORS_VALUES
} from 'components/theme/colors';
import {
  FONT_TYPES, FONT_WEIGHTS,
} from 'components/theme/fonts';
import {Label, Radio} from "@rebass/forms";
import AsyncSelect from "react-select/async";
import {
  customSelectStyles,
  selectColors
} from "components/shared";
import {getUserByUsername} from "redux-modules/user/sagas";
import _ from "lodash";
import Button from "components/Buttons";
import Caution from "components/Caution";
import {connect} from "react-redux";

class OpenChooseWorkerModel extends Component {
  static propTypes = {
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
    appliedWorkers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    requestId: PropTypes.string,
    chooseWorkerAction: PropTypes.func
  };

  static defaultProps = {
    toggleFunction: () => {
    },
    isOpened: false,
    requestId: undefined,
    chooseWorkerAction: () => {
    },
  };

  state = {
    chosenWorker: null,
    isLoadingChooseWorkerForTheRequest: false,
  };

  handleRadioChange = (value) => {
    this.setState({chosenWorker: value.target.value});
  };

  handleChooseWorkerChange = (val) => {
    this.setState({chosenWorker: _.get(val, 'value')});
  };

  fetchWorkers = (inputValue) => new Promise(async resolve => {
    const foundUsers = await getUserByUsername(inputValue);

    if (foundUsers && foundUsers.length) {
      const users = foundUsers
        .filter(user => user.role === 'worker')
        .map(user => ({label: `${user.name}  - ${user.averageRating}/5`, value: user._id}));

      return resolve(users);
    }

    return resolve([]);
  });

  handleClickSubmitButton = () => {
    const {
      chosenWorker
    } = this.state;
    const {requestId, chooseWorkerAction} = this.props;

    chooseWorkerAction(requestId, chosenWorker);
  };

  render() {
    const {
      isOpened,
      toggleFunction,
      appliedWorkers,
      isChooseWorkerFetching,
      isChooseWorkerHasError,
      chooseWorkerErrorMessage,
    } = this.props;
    console.log('-----', {
      isChooseWorkerHasError,
      chooseWorkerErrorMessage,
    })
    const {NORMAL} = FONT_WEIGHTS;
    const {
      BRANDING_GREEN,
      BRANDING_BLUE,
      LIGHT_RED,
      WHITE,
      ERROR,
      DISABLED
    } = COLORS;
    const {HEADING, SUBHEADING} = FONT_TYPES;

    return (
      <Modal
        width="400px"
        toggleModal={toggleFunction}
        header="اختيار فني للطلب"
        isOpened={isOpened}
      >
        <Flex flexDirection="column">
          <Box m={2}>
            <Text textAlign="right" mb={2} type={HEADING}
                  color={COLORS_VALUES[BRANDING_GREEN]}>
              البحث عن فني اخر
            </Text>
            <AsyncSelect
              mx={2}
              isRtl
              isClearable
              isMulti={false}
              isLoading={this.state.isLoadingChooseWorkerForTheRequest}
              cacheOptions
              theme={selectColors}
              styles={customSelectStyles}
              noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
              placeholder="قم بكتابة رقم الفنى"
              onChange={this.handleChooseWorkerChange}
              defaultOptions={[]}
              loadOptions={inputValue => this.fetchWorkers(inputValue)}
            />
          </Box>
          <Box m={2}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              قائمة الفنيين المتقدمين:
            </Text>
            {(appliedWorkers || []).map(worker => (
              <Box mt={2}>
                <Label>
                  <Radio
                    name="chooseWorker"
                    id={worker._id}
                    value={worker._id}
                    onChange={this.handleRadioChange}
                    sx={{color: '#fcfffa'}}
                  />
                  <Text mb={2} mr={2} type={HEADING}
                        color={COLORS_VALUES[BRANDING_GREEN]}>
                    <label>الفني : [ {worker.username} ] </label> - <label>{worker.name}</label> - <label>5/{worker.averageRating || 0}</label>
                  </Text>
                </Label>
              </Box>
            ))}
          </Box>
        </Flex>

        <Flex
          flexDirection="row-reverse"
          alignItems="center"
          justifyContent="space-between"
          pb={5}
          px={4}
        >
          <Button
            color={BRANDING_BLUE}
            onClick={this.handleClickSubmitButton}
            isLoading={isChooseWorkerFetching}
            disabled={isChooseWorkerFetching || !this.state.chosenWorker}
            mx={1}
          >
            حفظ
          </Button>
          {isChooseWorkerHasError && (
            <Caution mx={2} bgColor={LIGHT_RED}
                     textColor={WHITE}
                     borderColorProp={ERROR}>
              {chooseWorkerErrorMessage}
            </Caution>
          )}
        </Flex>
      </Modal>
    );
  }
}

OpenChooseWorkerModel.displayName = 'OpenChooseWorkerModel';


const mapStateToProps = state => ({
  isChooseWorkerFetching: state.requests.chooseWorker.isFetching,
  isChooseWorkerSuccess: state.requests.chooseWorker.isSuccess,
  isChooseWorkerHasError: state.requests.chooseWorker.isFail.isError,
  chooseWorkerErrorMessage: state.requests.chooseWorker.isFail.message,
});


export default connect(mapStateToProps)(OpenChooseWorkerModel);

