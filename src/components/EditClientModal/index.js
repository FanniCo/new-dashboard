import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { editClient, toggleEditClientModal } from 'redux-modules/clients/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';

const EditClientFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class EditClientModal extends Component {
  static propTypes = {
    clientIndex: PropTypes.number,
    clientDetails: PropTypes.shape({}),
    isEditClientFetching: PropTypes.bool,
    isEditClientError: PropTypes.bool,
    editClientErrorMessage: PropTypes.string,
    editClient: PropTypes.func,
    toggleEditClientModal: PropTypes.func,
    isEditClientModalOpen: PropTypes.bool,
  };

  static defaultProps = {
    clientIndex: undefined,
    clientDetails: undefined,
    isEditClientFetching: false,
    isEditClientError: false,
    editClientErrorMessage: '',
    editClient: () => {},
    toggleEditClientModal: () => {},
    isEditClientModalOpen: false,
  };

  constructor(props) {
    super(props);

    const {
      clientDetails: { name },
    } = props;

    this.state = {
      name,
    };
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  onSubmitEditClientForm = e => {
    e.preventDefault();

    const {
      clientIndex,
      clientDetails: { _id },
      editClient: editClientAction,
    } = this.props;
    const { name } = this.state;
    const editParams = {
      name,
    };

    editClientAction(_id, clientIndex, editParams);
  };

  render() {
    const {
      isEditClientFetching,
      isEditClientModalOpen,
      toggleEditClientModal: toggleEditClientModalAction,
      isEditClientError,
      editClientErrorMessage,
    } = this.props;
    const { name } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const editClientModalHeader = 'تعديل العميل';

    return (
      <Modal
        toggleModal={toggleEditClientModalAction}
        header={editClientModalHeader}
        isOpened={isEditClientModalOpen}
      >
        <EditClientFormContainer onSubmit={this.onSubmitEditClientForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم العميل
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم العميل"
                  ref={inputField => {
                    this.nameField = inputField;
                  }}
                  width={1}
                  value={name}
                  onChange={value => this.handleInputFieldChange('name', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitEditClientForm}
                disabled={isEditClientFetching || name === ''}
                isLoading={isEditClientFetching}
              >
                تعديل العميل
              </Button>
              {isEditClientError && (
                <Caution
                  mx={2}
                  isRtl={false}
                  bgColor={LIGHT_RED}
                  textColor={WHITE}
                  borderColorProp={ERROR}
                >
                  {editClientErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </EditClientFormContainer>
      </Modal>
    );
  }
}
EditClientModal.displayName = 'EditClientModal';

const mapStateToProps = state => ({
  isEditClientFetching: state.clients.editClient.isFetching,
  isEditClientError: state.clients.editClient.isFail.isError,
  editClientErrorMessage: state.clients.editClient.isFail.message,
  isEditClientModalOpen: state.clients.editClientModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      editClient,
      toggleEditClientModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(EditClientModal);
