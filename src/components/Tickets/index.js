import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import TicketCard from 'components/TicketCard';

const Tickets = props => {
  const { tickets } = props;

  const ticketsRenderer = tickets.map((ticket, index) => {
    const { _id } = ticket;

    return (
      <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
        <TicketCard m={2} index={index} ticket={ticket} />
      </Box>
    );
  });

  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      {ticketsRenderer}
    </Flex>
  );
};
Tickets.displayName = 'Tickets';

Tickets.propTypes = {
  tickets: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default Tickets;
