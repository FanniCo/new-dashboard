import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex } from '@rebass/grid';
import Cookies from 'js-cookie';
import { COOKIES_KEYS } from 'utils/constants';
import Header from 'components/Header';
import SideMenu from 'components/SideMenu';
import ScrollToTop from 'react-scroll-up';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Button from 'components/Buttons';

const { PRIMARY } = COLORS;

const LayoutContainer = styled.div`
  min-height: 100vh;
  width: 100%;
`;


const ContentContainer = styled(Flex)`
  width: calc(100% - 75px);
  background-color: ${props => props.theme.colors.greyMedium};
`;

const FlexContainer = styled(Flex)`
  padding-top: 65px;
  flex-direction: row-reverse;
  min-height: 100vh;
`;


const Layout = props => {
  const { children, history } = props;
  const { TOKEN } = COOKIES_KEYS;
  const token = Cookies.get(TOKEN);

  return (
    <LayoutContainer className='PageContainer'>
      {token && (
        <Header
          onLogoClick={() => {
            history.push('/');
          }}
          {...props}
        />
      )}
      <FlexContainer>
        <ScrollToTop showUnder={160} style={{zIndex: "999", right: "15px", bottom: "15px"}}>
          <Button className='ToTop'
            ml={1}
            mr={1}
            color={COLORS_VALUES[COLORS.GREY_DARK]}
            icon={faArrowUp}
            iconWidth="sm"
            xMargin={3}
            isLoading={false}
            reverse
          >
            <div />
          </Button>
        </ScrollToTop>
        <SideMenu {...props} />
        <ContentContainer className='ContentContainer'
          px={[3, 3, 3, 3, 3, 3]}
          pb={3}
          mr={['35px', '75px', '75px', '75px', '75px']}
        >
          {children}
        </ContentContainer>
      </FlexContainer>
    </LayoutContainer>
  );
};

Layout.propTypes = {
  user: PropTypes.shape({}),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  history: PropTypes.shape({
    action: PropTypes.string,
    block: PropTypes.func,
    createHref: PropTypes.func,
    go: PropTypes.func,
    goBack: PropTypes.func,
    goForward: PropTypes.func,
    length: PropTypes.number,
    listen: PropTypes.func,
    location: PropTypes.shape({
      hash: PropTypes.string,
      pathname: PropTypes.string,
      search: PropTypes.string,
    }),
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
};

Layout.defaultProps = {
  user: undefined,
};

export default Layout;
