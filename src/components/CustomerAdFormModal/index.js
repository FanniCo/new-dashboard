import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  toggleAddNewCustomersAdModal,
  addNewCustomersAd,
  updateCustomersAd,
  openEditCustomersAdModal,
} from 'redux-modules/customersAds/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { Input } from '@rebass/forms';
import { customSelectStyles, HyperLink, selectColors, StyledSelect } from 'components/shared';
import { ACTIONS, FIELDS, ACTIVE_STATUS } from 'utils/constants';
import _ from 'lodash';
import DatePicker from 'react-datepicker/es';
import { space } from 'styled-system';
import moment from 'moment';


const activeStatusKeys = Object.keys(ACTIVE_STATUS);
const activeStatusSelectOptions = activeStatusKeys.map(key => ({
  value: key,
  label: ACTIVE_STATUS[key].ar,
}));

const { GREY_DARK, BRANDING_ORANGE, DISABLED, GREY_LIGHT } = COLORS;
const SectionFlexContainer = styled(Flex)`
  .react-datepicker-popper {
    direction: ltr;

    &:lang(ar) {
      text-align: right;

      .react-datepicker__triangle {
        right: 50px;
        left: inherit;
      }
    }
  }

  .react-datepicker__input-container {
    display: block;
  }

  .react-datepicker__day--selected,
  .react-datepicker__day--in-selecting-range,
  .react-datepicker__day--in-range,
  .react-datepicker__month-text--selected,
  .react-datepicker__month-text--in-selecting-range,
  .react-datepicker__month-text--in-range {
    background-color: ${COLORS_VALUES[BRANDING_ORANGE]};

    &:hover {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
      opacity: 0.8;
    }
  }

  .react-datepicker__time-container,
  .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box {
    width: 120px;
  }

  .react-datepicker__time-container
    .react-datepicker__time
    .react-datepicker__time-box
    ul.react-datepicker__time-list
    li.react-datepicker__time-list-item {
    display: flex;
    justify-content: center;
    align-items: center;

    &.react-datepicker__time-list-item--selected {
      background-color: ${COLORS_VALUES[BRANDING_ORANGE]};

      &:hover {
        background-color: ${COLORS_VALUES[BRANDING_ORANGE]};
        opacity: 0.8;
      }
    }
  }

  .react-datepicker {
    font-family: 'english-font';

    :lang(ar) {
      font-family: 'Tajawal', sans-serif;
    }

    .react-datepicker-time__header {
      color: ${COLORS_VALUES[GREY_DARK]};
      font-weight: normal;
      font-size: 0.8rem;
    }
  }

  .react-datepicker__close-icon {
    right: 80%;
  }

  .react-datepicker__close-icon::after {
    height: 13px;
    width: 15px;
  }
`;

const AddNewCustomersAdFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const imageStyle = {
  width: '200px',
  height: '150px',
  padding: '5px',
  display: 'inherit',
};

const StyledDatePicker = styled(DatePicker)`
  ${space};
  background-color: ${props => props.backgroundColor};
  display: block;
  border: 1px solid;
  border-color: ${props => (props.isDanger ? COLORS_VALUES[COLORS.LIGHT_RED] : props.borderColor)};
  border-radius: ${props => props.theme.space[1]}px;
  color: ${props => COLORS_VALUES[props.color ? props.color : COLORS.DISABLED]};
  font-weight: ${props => props.theme.fontWeights.NORMAL};
  line-height: normal;
  outline: 0;
  width: calc(100% - 16px);
  font-size: 14px;
  direction: ltr;

  &:lang(ar) {
    padding: ${props => props.theme.space[2]}px;
    text-align: right;
  }

  &[type='number'] {
    -moz-appearance: textfield;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;

const actionsKeys = Object.keys(ACTIONS);
const actionsOptions = actionsKeys.map(key => ({
  value: key,
  label: ACTIONS[key].ar,
}));

const fieldsKeys = Object.keys(FIELDS);
const fieldsOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));

class AddNewCustomersAdModal extends Component {
  static propTypes = {
    isAddCustomersAdFetching: PropTypes.bool,
    isAddCustomersAdError: PropTypes.bool,
    isEditCustomersAdError: PropTypes.bool,
    isEditCustomersAdErrorMessage: PropTypes.string,
    addCustomersAdErrorMessage: PropTypes.string,
    addNewCustomersAd: PropTypes.func,
    toggleAddNewCustomersAdModal: PropTypes.func,
    openEditCustomersAdModal: PropTypes.func,
    updateCustomersAd: PropTypes.func,
    isAddCustomersAdModalOpen: PropTypes.bool,
    customersAdDetails: PropTypes.shape({}),
    customersAdIndex: PropTypes.number,
    isEditCustomersAdFetching: PropTypes.bool,
  };

  static defaultProps = {
    isAddCustomersAdFetching: false,
    isAddCustomersAdError: false,
    isEditCustomersAdError: false,
    isEditCustomersAdErrorMessage: '',
    addCustomersAdErrorMessage: '',
    addNewCustomersAd: () => {},
    toggleAddNewCustomersAdModal: () => {},
    openEditCustomersAdModal: () => {},
    updateCustomersAd: () => {},
    isAddCustomersAdModalOpen: false,
    customersAdDetails: undefined,
    customersAdIndex: undefined,
    isEditCustomersAdFetching: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      titleAr: '',
      titleEn: '',
      contentAr: '',
      contentEn: '',
      imageUrlAr: '',
      imageUrlEn: '',
      fullImageUrlAr: null,
      fullImageUrlEn: null,
      externalUrl: '',
      action: '',
      active: activeStatusSelectOptions[0],
      originalStatus: true,
      expireAt: null,
      displayFrequency: 0,
      services: [],
      promoCode: '',
      order: 1,
    };
  }

  componentDidMount() {
    const { customersAdDetails } = this.props;

    if (customersAdDetails) {
      this.setState({
        titleAr: customersAdDetails.title ? customersAdDetails.title.ar : '',
        titleEn: customersAdDetails.title ? customersAdDetails.title.en : '',
        contentAr: customersAdDetails.content ? customersAdDetails.content.ar : '',
        contentEn: customersAdDetails.content ? customersAdDetails.content.en : '',
        imageUrlAr: customersAdDetails.imageUrl ? customersAdDetails.imageUrl.ar : '',
        imageUrlEn: customersAdDetails.imageUrl ? customersAdDetails.imageUrl.en : '',
        fullImageUrlAr: customersAdDetails.imageUrl ? customersAdDetails.imageUrl.ar : '',
        fullImageUrlEn: customersAdDetails.imageUrl ? customersAdDetails.imageUrl.en : '',

        externalUrl: customersAdDetails.externalUrl || '',
        action: customersAdDetails.action
          ? actionsOptions.filter(option => option.value === customersAdDetails.action)[0]
          : '',
        originalStatus: customersAdDetails.active,
        active: customersAdDetails.active ? activeStatusSelectOptions[0] : activeStatusSelectOptions[1],
        expireAt: customersAdDetails.expireAt,
        displayFrequency: customersAdDetails.displayFrequency || 0,
        order: customersAdDetails.order || 1,
      });
    }
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeActionSelect = action => {
    const externalUrl = '';

    this.setState({ action, externalUrl });
  };

  onSubmitAddCustomersAdForm = e => {
    e.preventDefault();

    const {
      addNewCustomersAd: addNewCustomersAdAction,
      updateCustomersAd: updateCustomersAdAction,
      customersAdDetails,
      customersAdIndex,
    } = this.props;
    const {
      titleAr,
      titleEn,
      contentAr,
      contentEn,
      imageUrlAr,
      imageUrlEn,
      originalStatus,
      active,
      expireAt,
      action,
      displayFrequency,
      order,
    } = this.state;
    let { externalUrl } = this.state;

    const newCustomersAdInfo = new FormData();
    newCustomersAdInfo.append('titleAr', titleAr);
    newCustomersAdInfo.append('titleEn', titleEn);
    newCustomersAdInfo.append('contentAr', contentAr);
    newCustomersAdInfo.append('contentEn', contentEn);

    newCustomersAdInfo.append('imageUrlAr', imageUrlAr);
    newCustomersAdInfo.append('imageUrlEn', imageUrlEn);

    newCustomersAdInfo.append('action', action ? action.value : '');
    newCustomersAdInfo.append('active', active? active.value : '');

    let currentActiveStatus = JSON.parse(newCustomersAdInfo.get('active').toLowerCase());

    if(currentActiveStatus != originalStatus) {
      this.setState({ originalStatus, currentActiveStatus });
      if(currentActiveStatus == true) {
        newCustomersAdInfo.append('expireAt', moment().locale('en').add(1, 'years').format('YYYY-MM-DD'));
      } else {
        newCustomersAdInfo.append('expireAt', moment().locale('en').subtract(1, 'days').format('YYYY-MM-DD'));
      }
    } else {
      newCustomersAdInfo.append('expireAt', moment(expireAt).locale('en').format('YYYY-MM-DD'));
    }
    
    newCustomersAdInfo.append('displayFrequency', displayFrequency || 0);
    newCustomersAdInfo.append('order', order || 1);
    
    if (action.value === ACTIONS.serviceRequest.en) {
      const { services, promoCode } = this.state;
      const selectedServices = services.map(s => s.value).join('.');
      externalUrl = `https://fanni.sa/services?fields=${selectedServices}&promoCode=${promoCode}`;
    }

    if (externalUrl) {
      newCustomersAdInfo.append('externalUrl', externalUrl);
    }

    if (customersAdDetails) {
      const { _id } = customersAdDetails;
      return updateCustomersAdAction(newCustomersAdInfo, _id, customersAdIndex);
    }

    return addNewCustomersAdAction(newCustomersAdInfo);
  };

  handleActiveCheckBoxChange = event => {
    this.setState({ active: event.target.checked });
  };

  handleFileChange = (e, type) => {
    const file = e.target.files[0];
    this.setState({
      [type]: file,
      [_.camelCase(`full_${type}`)]: URL.createObjectURL(file),
    });
  };

  clearFile = type => {
    this.setState({
      [type]: '',
      [_.camelCase(`full_${type}`)]: null,
      [`${type}Key`]: Date.now(),
    });
  };

  handleToDateInputChange = value => {
    this.setState({ expireAt: value });
  };


  handleChangeActiveStatusSelect = active => {
    this.setState({ active }, () => {
      this.setState({ active: active });
    });
  };

  render() {
    const {
      isAddCustomersAdFetching,
      isAddCustomersAdModalOpen,
      toggleAddNewCustomersAdModal: toggleAddNewCustomersAdModalAction,
      openEditCustomersAdModal: openEditCustomersAdModalAction,
      isAddCustomersAdError,
      isEditCustomersAdError,
      isEditCustomersAdErrorMessage,
      addCustomersAdErrorMessage,
      customersAdDetails,
      isEditCustomersAdFetching,
    } = this.props;
    const {
      titleAr,
      titleEn,
      contentAr,
      contentEn,
      imageUrlAr,
      imageUrlEn,
      fullImageUrlAr,
      fullImageUrlEn,
      externalUrl,
      active,
      action,
      displayFrequency,
      services,
      promoCode,
      order,
      expireAt,
    } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addCustomerModalHeader = customersAdDetails ? 'تعديل الاعلان' : 'أضف اعلان جديدة';
    const submitButtonText = customersAdDetails ? 'تعديل' : 'أضف';
    
    return (
      <Modal
        toggleModal={() =>
          customersAdDetails
            ? openEditCustomersAdModalAction()
            : toggleAddNewCustomersAdModalAction()
        }
        header={addCustomerModalHeader}
        isOpened={isAddCustomersAdModalOpen}
      >
        <AddNewCustomersAdFormContainer onSubmit={this.onSubmitAddCustomersAdForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى العنوان (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان عربى"
                  width={1}
                  value={titleAr}
                  onChange={value => this.handleInputFieldChange('titleAr', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتوى العنوان(بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="العنوان انجليزى"
                  width={1}
                  value={titleEn}
                  onChange={value => this.handleInputFieldChange('titleEn', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="الوصف عربى"
                  width={1}
                  value={contentAr}
                  onChange={value => this.handleInputFieldChange('contentAr', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  محتواى الوصف (بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="textarea"
                  placeholder="الوصف انجليزى"
                  width={1}
                  value={contentEn}
                  onChange={value => this.handleInputFieldChange('contentEn', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                {fullImageUrlAr && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrlAr')} />
                    <HyperLink href={fullImageUrlAr} target="_blank">
                      <img style={imageStyle} src={fullImageUrlAr} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  االعنوان الالكترونى للصورة (بالعربى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="الصورة (بالعربى)"
                  name="fileAr"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrlAr');
                  }}
                  // key={imageUrlArKey}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                {fullImageUrlEn && (
                  <>
                    <Button icon={faTimes} onClick={() => this.clearFile('imageUrlEn')} />
                    <HyperLink href={fullImageUrlEn} target="_blank">
                      <img style={imageStyle} src={fullImageUrlEn} alt="" />
                    </HyperLink>
                  </>
                )}
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  االعنوان الالكترونى للصورة (بالنجليزى)
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <Input
                  mb={2}
                  type="file"
                  placeholder="الصورة (بالانجليزى)"
                  name="fileAr"
                  onChange={e => {
                    this.handleFileChange(e, 'imageUrlEn');
                  }}
                  // key={imageUrlArKey}
                  sx={{ color: '#fcfffa' }}
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  عدد مرات تكرار العرض
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="number"
                  placeholder="عدد مرات تكرار العرض"
                  width={1}
                  value={displayFrequency}
                  onChange={value => this.handleInputFieldChange('displayFrequency', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  ترتيب الاعلان
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <InputField
                  type="number"
                  placeholder="ترتيب الاعلان"
                  width={1}
                  value={order}
                  onChange={value => this.handleInputFieldChange('order', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  مكان العرض
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="مكان العرض"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={action}
                  onChange={this.handleChangeActionSelect}
                  options={actionsOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </SectionFlexContainer>
            </Flex>
            {action && action.value === ACTIONS.externalUrl.en && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    االعنوان الالكترونى خارجى
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="العنوان الالكترونى خارجى"
                    width={1}
                    value={externalUrl}
                    onChange={value => this.handleInputFieldChange('externalUrl', value)}
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            )}
            {action && action.value === ACTIONS.serviceRequest.en && (
              <Flex pb={3} px={4}>
                <SectionFlexContainer
                  flexDirection="column"
                  width={1}
                  flexWrap="wrap"
                  px={1}
                  mb={2}
                >
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    اختر الخدمة
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <StyledSelect
                    mb={3}
                    placeholder="اختر الخدمة"
                    noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                    isRtl
                    backspaceRemovesValue={false}
                    value={services}
                    onChange={value => this.handleInputFieldChange('services', value)}
                    options={fieldsOptions}
                    theme={selectColors}
                    styles={customSelectStyles}
                    isClearable
                    isMulti
                  />
                </SectionFlexContainer>
              </Flex>
            )}
            {action && action.value === ACTIONS.serviceRequest.en && (
              <Flex pb={3} px={4}>
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    البروموكود
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="string"
                    placeholder="البروموكود"
                    width={1}
                    value={promoCode}
                    onChange={value => this.handleInputFieldChange('promoCode', value)}
                    mb={2}
                    autoComplete="on"
                  />
                </Flex>
              </Flex>
            )}
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  تاريخ انتهاء الاعلان
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledDatePicker
                  mb={5}
                  inline={false}
                  placeholderText="تاريخ انتهاء الاعلان"
                  dateFormat="MM/dd/yyyy"
                  borderColor={COLORS_VALUES[DISABLED]}
                  backgroundColor={COLORS_VALUES[GREY_LIGHT]}
                  selected={expireAt ? new Date(expireAt) : undefined}
                  onChange={this.handleToDateInputChange}
                  isClearable
                />
              </SectionFlexContainer>
              {/* <Box> */}
              {/*  <Label> */}
              {/*    <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}> */}
              {/*      مفعل */}
              {/*      <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}> */}
              {/*        * */}
              {/*      </Text> */}
              {/*    </Text> */}
              {/*    <Checkbox */}
              {/*      name="remember" */}
              {/*      checked={active} */}
              {/*      onChange={this.handleActiveCheckBoxChange} */}
              {/*      sx={{ color: '#fcfffa' }} */}
              {/*    /> */}
              {/*  </Label> */}
              {/* </Box> */}
            </Flex>
            <Flex pb={3} px={4}>
              <SectionFlexContainer flexDirection="column" width={1} flexWrap="wrap" px={1} mb={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  الحالة
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                    *
                  </Text>
                </Text>
                <StyledSelect
        mb={3}
        placeholder="الحالة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        backspaceRemovesValue={false}
        value={active}
        options={activeStatusSelectOptions}
        onChange={this.handleChangeActiveStatusSelect}
        theme={selectColors}
        styles={customSelectStyles}
      />
                </SectionFlexContainer>
              </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddCustomersAdForm}
                disabled={
                  isAddCustomersAdFetching ||
                  titleAr === '' ||
                  titleEn === '' ||
                  contentAr === '' ||
                  contentEn === '' ||
                  imageUrlAr === '' ||
                  imageUrlEn === '' ||
                  !expireAt ||
                  (action.value === ACTIONS.externalUrl.en && externalUrl === '') ||
                  (action.value === ACTIONS.serviceRequest.en &&
                    (_.isEmpty(services) || _.isEmpty(promoCode)))
                }
                isLoading={isAddCustomersAdFetching || isEditCustomersAdFetching}
              >
                {submitButtonText}
              </Button>
              {isAddCustomersAdError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addCustomersAdErrorMessage}
                </Caution>
              )}
              {isEditCustomersAdError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {isEditCustomersAdErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewCustomersAdFormContainer>
      </Modal>
    );
  }
}

AddNewCustomersAdModal.displayName = 'AddNewCustomersAdModal';

const mapStateToProps = state => ({
  isAddCustomersAdFetching: state.customersAds.addCustomersAd.isFetching,
  isEditCustomersAdFetching: state.customersAds.updateCustomersAd.isFetching,
  isAddCustomersAdError: state.customersAds.addCustomersAd.isFail.isError,
  isEditCustomersAdError: state.customersAds.updateCustomersAd.isFail.isError,
  isEditCustomersAdErrorMessage: state.customersAds.updateCustomersAd.isFail.message,
  addCustomersAdErrorMessage: state.customersAds.addCustomersAd.isFail.message,
  isAddCustomersAdModalOpen:
    state.customersAds.addNewCustomersAdModal.isOpen ||
    state.customersAds.editCustomersAdModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      toggleAddNewCustomersAdModal,
      addNewCustomersAd,
      updateCustomersAd,
      openEditCustomersAdModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewCustomersAdModal);
