import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex } from '@rebass/grid';
import _ from 'lodash';
import { faCommentDots } from '@fortawesome/free-solid-svg-icons';
import { REQUESTS } from 'redux-modules/requests/actions';
import { WORKERS } from 'redux-modules/workers/actions';
import { CLIENTS } from 'redux-modules/clients/actions';
import { TICKETS } from 'redux-modules/tickets/actions';
import Text from 'components/Text';
import Card from 'components/Card';
import Button from 'components/Buttons';
import EmptyState from 'components/EmptyState';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle } from 'components/shared';
import Caution from 'components/Caution';
import { MentionsInput, Mention } from 'react-mentions';
import './mentions-styles.css';

const { BRANDING_GREEN, DISABLED, GREY_MEDIUM, WHITE, LIGHT_RED, ERROR } = COLORS;
const { HEADING, SUBHEADING, BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;
const { ADD_REQUEST_COMMENT } = REQUESTS;
const { ADD_WORKER_COMMENT } = WORKERS;
const { ADD_CLIENT_COMMENT } = CLIENTS;
const { ADD_TICKET_COMMENT } = TICKETS;

const Container = styled(Flex)`
  height: 100%;
`;

const CommentsList = styled(Flex)`
  min-height: 150px;
  max-height: 220px;
  overflow-y: auto;
`;

const CommentRow = styled(Flex)`
  margin-top: ${props => props.theme.space[3]}px;

  &:first-child {
    margin-top: 0;
  }
`;

const CommentBody = styled(Flex)`
  background-color: ${COLORS_VALUES[COLORS.PLACEHOLDER_GREY]};
  border-radius: ${props => props.theme.space[1]}px;
`;

const CommentText = styled(Text)`
  white-space: pre-line;
`;

class DetailsComments extends React.Component {
  constructor(props) {
    super(props);

    this.commentsList = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const { addCommentState, resetAddCommentInput } = this.props;

    if (
      addCommentState !== prevProps.addCommentState &&
      (addCommentState === ADD_REQUEST_COMMENT.SUCCESS ||
        addCommentState === ADD_WORKER_COMMENT.SUCCESS ||
        addCommentState === ADD_CLIENT_COMMENT.SUCCESS ||
        addCommentState === ADD_TICKET_COMMENT.SUCCESS)
    ) {
      const {
        current: { scrollHeight },
      } = this.commentsList;

      resetAddCommentInput();
      this.commentsList.current.scrollTop = scrollHeight;
    }
  }

  render() {
    const {
      mentionData,
      hideAddCommentSection,
      isModal,
      objectType,
      object: { _id, comments },
      addedCommentValue,
      handleAddCommentInputChange,
      addComment,
      addCommentState,
      addCommentErrorMessage,
      admins,
    } = this.props;
    let userMentionData = [];
    if (admins && admins.length) {
      userMentionData = admins.map(({ username, name }) => ({
        id: username,
        display: `${name.toUpperCase()}`,
      }));
    }

    let commentsView;

    if (!comments || !comments.length) {
      commentsView = (
        <EmptyState
          icon={faCommentDots}
          iconSize="3x"
          iconColor={BRANDING_GREEN}
          textColor={COLORS_VALUES[WHITE]}
          text="لا توجد تعليقات"
          textSize={SUBHEADING}
        />
      );
    } else {
      commentsView = (
        <CommentsList ref={this.commentsList} width={1} flexDirection="column">
          {comments.map(comment => {
            const { _id: commentId, createdAt } = comment;
            const commentDate = new Date(createdAt);
            const commentDateString = commentDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            });

            return (
              <CommentRow key={commentId} flexDirection="column">
                <Flex justifyContent="space-between">
                  <Text
                    color={COLORS_VALUES[BRANDING_GREEN]}
                    type={HEADING}
                    fontWeight={NORMAL}
                    mx={2}
                    mb={1}
                  >
                    {_.get(comment, 'commenter.name', null) ||
                      _.get(comment, 'commenterInfo.name', 'هذا الادمن تم حفظفه')}
                  </Text>
                </Flex>
                <CommentBody flexDirection="column" px={1} py={2} mx={2}>
                  <CommentText
                    textAlign="right"
                    color={COLORS_VALUES[DISABLED]}
                    type={HEADING}
                    fontWeight={NORMAL}
                    mx={2}
                  >
                    {comment.text}
                  </CommentText>
                  <Text
                    textAlign="left"
                    color={COLORS_VALUES[DISABLED]}
                    type={BODY}
                    fontWeight={NORMAL}
                    mx={2}
                    mt={1}
                  >
                    {`[${commentDateString}]`}
                  </Text>
                </CommentBody>
              </CommentRow>
            );
          })}
        </CommentsList>
      );
    }

    const isSubmitButtonLoading =
      addCommentState === ADD_REQUEST_COMMENT.LOAD ||
      addCommentState === ADD_WORKER_COMMENT.LOAD ||
      addCommentState === ADD_CLIENT_COMMENT.LOAD ||
      addCommentState === ADD_TICKET_COMMENT.LOAD;
    let objectTypeId;
    let commentInfo;

    if (objectType === 'request') {
      objectTypeId = 'requestId';
    } else {
      objectTypeId = 'userId';
    }

    if (objectType === 'ticket') {
      commentInfo = {
        comment: addedCommentValue,
        mentions: mentionData,
      };
    } else {
      commentInfo = {
        mentions: mentionData,
        text: addedCommentValue,
        [objectTypeId]: _id,
      };
    }

    return (
      <Card
        width="calc(100% - 16px)"
        minHeight={100}
        height="100%"
        flexDirection="column"
        mt={isModal ? 0 : 2}
        m={2}
        backgroundColor={GREY_MEDIUM}
      >
        {!isModal && (
          <CardTitle px={4} py={2} width={1}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              التعليقات {`(${comments ? comments.length : 0})`}
            </Text>
          </CardTitle>
        )}
        <Container width={1} p={2} flexDirection="column" alignItems="center">
          {commentsView}
          {!hideAddCommentSection && (
            <Flex width={1} pt={4} p={2} flexDirection="column">
              <MentionsInput
                value={addedCommentValue}
                onChange={handleAddCommentInputChange}
                markup="@{{__id__||__display__}}"
                placeholder="اضف تعليق"
                className="mentions"
              >
                <Mention
                  type="user"
                  trigger="@"
                  data={userMentionData}
                  appendSpaceOnAdd
                  className="mentions__mention"
                  style={{
                    backgroundColor: '#daf4fa',
                  }}
                />
              </MentionsInput>
              <Flex flexDirection="row-reverse">
                <Button
                  mt={2}
                  isLoading={isSubmitButtonLoading}
                  color={BRANDING_GREEN}
                  onClick={() => addComment(commentInfo, _id)}
                  disabled={!addedCommentValue || addedCommentValue === ''}
                >
                  أضف تعليق
                </Button>
                {(addCommentState === ADD_REQUEST_COMMENT.FAIL ||
                  addCommentState === ADD_WORKER_COMMENT.FAIL ||
                  addCommentState === ADD_CLIENT_COMMENT.FAIL ||
                  addCommentState === ADD_TICKET_COMMENT.FAIL) && (
                  <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                    {addCommentErrorMessage}
                  </Caution>
                )}
              </Flex>
            </Flex>
          )}
        </Container>
      </Card>
    );
  }
}
DetailsComments.displayName = 'DetailsComments';

DetailsComments.propTypes = {
  hideAddCommentSection: PropTypes.bool,
  isModal: PropTypes.bool,
  objectType: PropTypes.string,
  object: PropTypes.shape({}),
  addedCommentValue: PropTypes.string,
  mentionData: PropTypes.arrayOf(PropTypes.shape({})),
  addComment: PropTypes.func,
  addCommentState: PropTypes.string,
  addCommentErrorMessage: PropTypes.string,
  handleAddCommentInputChange: PropTypes.func,
  resetAddCommentInput: PropTypes.func,
  admins: PropTypes.arrayOf(PropTypes.shape({})),
};

DetailsComments.defaultProps = {
  hideAddCommentSection: false,
  isModal: false,
  objectType: undefined,
  object: undefined,
  addedCommentValue: '',
  mentionData: [],
  addComment: () => {},
  addCommentState: undefined,
  handleAddCommentInputChange: () => {},
  resetAddCommentInput: () => {},
  addCommentErrorMessage: undefined,
  admins: [],
};

export default DetailsComments;
