import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex } from '@rebass/grid';
import Modal from 'components/Modal';
import Text from 'components/Text';
import { HyperLink } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS } from 'components/theme/fonts';
import ROUTES from 'routes';

const AppliedWorkerRow = styled(Flex)`
  cursor: pointer;
  border-bottom: 1px solid ${COLORS_VALUES[COLORS.GREY_LIGHT]};
  padding: ${props => props.theme.space[4]}px ${props => props.theme.space[2]}px;

  &:hover {
    background-color: ${COLORS_VALUES[COLORS.GREY_LIGHT]};
  }
`;

const AppliedWorkersModal = props => {
  const { isOpened, toggleFunction, appliedWorkers } = props;
  const { SINGLE_WORKER } = ROUTES;
  const { DISABLED } = COLORS;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Modal
      width="400px"
      toggleModal={toggleFunction}
      header="الفنيين المتقدمين"
      isOpened={isOpened}
    >
      <Flex flexDirection="column">
        {appliedWorkers.map(worker => (
          <HyperLink
            key={worker.username}
            width={1}
            href={`${SINGLE_WORKER}/${worker.username}`}
            target="_blank"
          >
            <AppliedWorkerRow width={1}>
              <Text cursor="pointer" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                الفني : [ {worker.username} ] - {worker.name}
              </Text>
            </AppliedWorkerRow>
          </HyperLink>
        ))}
      </Flex>
    </Modal>
  );
};
AppliedWorkersModal.displayName = 'AppliedWorkersModal';

AppliedWorkersModal.propTypes = {
  isOpened: PropTypes.bool,
  toggleFunction: PropTypes.func,
  appliedWorkers: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

AppliedWorkersModal.defaultProps = {
  toggleFunction: () => {},
  isOpened: false,
};

export default AppliedWorkersModal;
