import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import ReviewCard from 'components/ReviewCard';

const { WHITE, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

const ReviewsListFlex = styled(Flex)`
  ${minHeight};
`;

class ReviewsList extends Component {
  getLoadingReviews = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 2} height={250} m={2} />
      <Rect width={1 / 2} height={250} m={2} />
    </ShimmerEffect>
  );

  createReviewsList = reviewsList => {
    const { user } = this.props;
    if (reviewsList.length) {
      return reviewsList.map((review, index) => {
        const { _id } = review.review;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 2]}>
            <ReviewCard m={2} index={index} review={review} user={user} />
          </Box>
        );
      });
    }

    return (
      <Card width={1} minHeight={600} alignItems="center" backgroundColor={GREY_LIGHT}>
        <EmptyState
          icon={faFile}
          iconColor={BRANDING_GREEN}
          iconSize="3x"
          textColor={COLORS_VALUES[WHITE]}
          textSize={BIG_TITLE}
          text="لا يوجد تقيمات لهذا الفني"
        />
      </Card>
    );
  };

  render() {
    const { reviewsList, loadMoreReviews, isLoadMoreReviewsAvailable, reviewsState } = this.props;
    const shipmentsLoadingList = this.getLoadingReviews(3);

    if (reviewsState.isFail.isError) {
      return (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text={reviewsState.isFail.message}
            />
          </Card>
        </Flex>
      );
    }
    return (
      <Box width={1} p={3} flexWrap="wrap" flexDirection="column">
        {reviewsList ? (
          <InfiniteScroll
            dataLength={reviewsList.length}
            next={loadMoreReviews}
            hasMore={isLoadMoreReviewsAvailable}
            height={600}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <ReviewsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createReviewsList(reviewsList)}
            </ReviewsListFlex>
          </InfiniteScroll>
        ) : (
          shipmentsLoadingList
        )}
      </Box>
    );
  }
}
ReviewsList.displayName = 'ReviewsList';

ReviewsList.propTypes = {
  reviewsList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreReviewsAvailable: PropTypes.bool.isRequired,
  loadMoreReviews: PropTypes.func,
  user: PropTypes.shape({}),
  reviewsState: PropTypes.shape({}),
};

ReviewsList.defaultProps = {
  reviewsList: undefined,
  user: undefined,
  loadMoreReviews: () => {},
  reviewsState: undefined,
};

export default ReviewsList;
