import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { getAllReviews } from 'redux-modules/reviews/actions';
import ReviewsList from './ReviewsList';

class ReviewsContainer extends Component {
  static propTypes = {
    workerId: PropTypes.string,
    user: PropTypes.shape({}),
    reviewsList: PropTypes.arrayOf(PropTypes.shape({})),
    reviewsState: PropTypes.shape({}),
    isLoadMoreReviewsAvailable: PropTypes.bool.isRequired,
    getAllReviews: PropTypes.func,
  };

  static defaultProps = {
    workerId: undefined,
    user: undefined,
    reviewsList: undefined,
    reviewsState: undefined,
    getAllReviews: () => {},
  };

  constructor(props) {
    super(props);

    const { workerId } = props;

    this.state = {
      filterQueries: {
        skip: 0,
        limit: 20,
        workerId,
      },
    };
  }

  componentDidMount() {
    const { getAllReviews: getWorkerReviewsAction } = this.props;
    const { filterQueries } = this.state;

    getWorkerReviewsAction(filterQueries, true);
  }

  loadMoreReviews = () => {
    const { getAllReviews: getWorkerReviewsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getWorkerReviewsAction(filterQueriesNextState);
      },
    );
  };

  render() {
    const { reviewsList, reviewsState, isLoadMoreReviewsAvailable, user } = this.props;

    return (
      <ReviewsList
        reviewsList={reviewsList}
        user={user}
        reviewsState={reviewsState}
        isLoadMoreReviewsAvailable={isLoadMoreReviewsAvailable}
        loadMoreReviews={this.loadMoreReviews}
      />
    );
  }
}
ReviewsContainer.displayName = 'ReviewsContainer';

const mapStateToProps = state => ({
  reviewsList: state.reviews.reviews,
  reviewsState: state.reviews.getAllReviews,
  isLoadMoreReviewsAvailable: state.reviews.hasMoreReviews,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getAllReviews }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(ReviewsContainer);
