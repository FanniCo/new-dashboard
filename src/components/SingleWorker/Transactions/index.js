import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { getWorkerTransactions } from 'redux-modules/transactions/actions';
import TransactionsList from './TransactionsList';

class TransactionsContainer extends Component {
  static propTypes = {
    workerId: PropTypes.string,
    transactionsList: PropTypes.arrayOf(PropTypes.shape({})),
    transactionsState: PropTypes.shape({}),
    isLoadMoreTransactionsAvailable: PropTypes.bool.isRequired,
    getWorkerTransactions: PropTypes.func,
  };

  static defaultProps = {
    workerId: undefined,
    transactionsList: undefined,
    transactionsState: undefined,
    getWorkerTransactions: () => {},
  };

  constructor(props) {
    super(props);

    const { workerId } = props;

    this.state = {
      filterQueries: {
        skip: 0,
        limit: 20,
        workerId,
      },
    };
  }

  componentDidMount() {
    const { getWorkerTransactions: getWorkerTransactionsAction } = this.props;
    const { filterQueries } = this.state;

    getWorkerTransactionsAction(filterQueries, true);
  }

  loadMoreTransactions = () => {
    const { getWorkerTransactions: getWorkerTransactionsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getWorkerTransactionsAction(filterQueriesNextState);
      },
    );
  };

  render() {
    const { transactionsList, transactionsState, isLoadMoreTransactionsAvailable } = this.props;
    return (
      <TransactionsList
        transactionsList={transactionsList}
        transactionsState={transactionsState}
        isLoadMoreTransactionsAvailable={isLoadMoreTransactionsAvailable}
        loadMoreTransactions={this.loadMoreTransactions}
      />
    );
  }
}
TransactionsContainer.displayName = 'TransactionsContainer';

const mapStateToProps = state => ({
  transactionsList: state.transactions.workerTransactions,
  transactionsState: state.transactions.getWorkerTransactions,
  isLoadMoreTransactionsAvailable: state.transactions.hasMoreWorkerTransactions,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getWorkerTransactions }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(TransactionsContainer);
