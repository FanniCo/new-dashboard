import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const SuspendedRow = props => {
  const { suspended } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          موقوف
        </Text>
      </Flex>
      <Flex flexDirection="row-reverse" width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {!suspended || Date.parse(new Date(suspended)) < Date.parse(new Date())
            ? 'لا'
            : ` ${new Date(suspended).toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })} موقوف حتى`}
        </Text>
      </Flex>
    </OddRow>
  );
};
SuspendedRow.displayName = 'SuspendedRow';

SuspendedRow.propTypes = {
  suspended: PropTypes.string,
};

SuspendedRow.defaultProps = {
  suspended: undefined,
};

export default SuspendedRow;
