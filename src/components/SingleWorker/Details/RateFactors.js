import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import { CardLabel } from 'components/shared';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { REVIEWS_TAGS } from 'utils/constants';
import styled from 'styled-components';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';

const { BODY } = FONT_TYPES;
const reviewsTagsKeys = Object.keys(REVIEWS_TAGS);

const RateFactorImage = styled.img`
  border-radius: 50%;
`;
const RateFactors = props => {
  const { workerStatistics, isGetWorkerStatisticsFetching, isGetWorkerStatisticsSuccess } = props;
  const { BRANDING_GREEN, GREY_LIGHT, GREY_DARK } = COLORS;
  let RateFactorRender = (
    <ShimmerEffect width={1}>
      <Rect width={1} height={550} m={2} />
    </ShimmerEffect>
  );

  if (!isGetWorkerStatisticsFetching && isGetWorkerStatisticsSuccess) {
    RateFactorRender = reviewsTagsKeys.map(key => (
      <Flex key={key} flexDirection="column" p={1} justifyContent="center" alignItems="center">
        <Box display="inline-block" p="1" color="white" borderRadius="9999" border>
          {workerStatistics.statistics.rateFactors
            ? workerStatistics.statistics.rateFactors[key]
            : 0}
        </Box>

        <RateFactorImage
          width={30}
          src={require(`../../../static/images/${key}.png`)}
          alt="Profile Image"
        />
        <CardLabel
          key={key}
          py={1}
          px={2}
          ml={1}
          mb={1}
          type={BODY}
          color={COLORS_VALUES[GREY_DARK]}
          bg={BRANDING_GREEN}
        >
          {REVIEWS_TAGS[key].ar}
        </CardLabel>
      </Flex>
    ));
  }

  return (
    <Card
      flexWrap="wrap"
      justifyContent="center"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      color="white"
      mb={2}
      px={2}
      pt={4}
      pb={3}
    >
      عوامل التقيم
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse" alignItems="center">
        {RateFactorRender}
      </Flex>
    </Card>
  );
};
RateFactors.displayName = 'RateFactors';

RateFactors.propTypes = {
  workerStatistics: PropTypes.shape({}),
  isGetWorkerStatisticsFetching: PropTypes.bool,
  isGetWorkerStatisticsSuccess: PropTypes.bool,
};

RateFactors.defaultProps = {
  workerStatistics: undefined,
  isGetWorkerStatisticsFetching: false,
  isGetWorkerStatisticsSuccess: false,
};

export default RateFactors;
