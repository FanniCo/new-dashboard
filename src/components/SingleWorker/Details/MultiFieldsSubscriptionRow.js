import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';

const MultiFieldsSubscriptionRow = props => {
  const { multiFieldsSubscriptionValidTo } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const parseMultiFieldsSubscriptionValidToObject = new Date(multiFieldsSubscriptionValidTo);
  const parseMultiFieldsSubscriptionValidTo = new Date(
    multiFieldsSubscriptionValidTo,
  ).toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  });

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          مشترك فى العمل باكتر من مهنه
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {!multiFieldsSubscriptionValidTo ? 'لا' : ''}
          {Date.parse(parseMultiFieldsSubscriptionValidToObject) < Date.parse(new Date())
            ? 'الاشتراك منتهى'
            : ''}

          {Date.parse(parseMultiFieldsSubscriptionValidToObject) > Date.parse(new Date())
            ? ` ${parseMultiFieldsSubscriptionValidTo}    نعم و ينتهى الاشترك`
            : ''}
        </Text>
      </Flex>
    </EvenRow>
  );
};
MultiFieldsSubscriptionRow.displayName = 'MultiFieldsSubscriptionRow';

MultiFieldsSubscriptionRow.propTypes = {
  multiFieldsSubscriptionValidTo: PropTypes.string,
};

MultiFieldsSubscriptionRow.defaultProps = {
  multiFieldsSubscriptionValidTo: undefined,
};

export default MultiFieldsSubscriptionRow;
