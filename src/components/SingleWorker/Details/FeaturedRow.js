import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import moment from 'moment';

const FeaturedRow = props => {
  const { featuredSubscriptionValidTo } = props;
  const { DISABLED, WARNING } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          متميز
        </Text>
      </Flex>
      <Flex flexDirection="row-reverse" width={0.5}>
        {moment(featuredSubscriptionValidTo).isBefore(moment().utc()) && (
          <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[DISABLED]} size="sm" />
        )}
        {moment(featuredSubscriptionValidTo).isSameOrAfter(moment().utc()) && (
          <FontAwesomeIcon icon={faStar} color={COLORS_VALUES[WARNING]} size="sm" />
        )}
      </Flex>
    </EvenRow>
  );
};
FeaturedRow.displayName = 'FeaturedRow';

FeaturedRow.propTypes = {
  featuredSubscriptionValidTo: PropTypes.bool.isRequired,
};

export default FeaturedRow;
