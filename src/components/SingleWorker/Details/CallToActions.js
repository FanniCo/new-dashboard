import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import moment from 'moment';
import {
  faPen,
  faPowerOff,
  faStar,
  faCrown,
  faDollarSign,
  faLock,
  faHandPaper,
  faMoneyBillWave,
} from '@fortawesome/free-solid-svg-icons';
import Card from 'components/Card';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';

const CallToActions = props => {
  const {
    user: { permissions },
    worker,
    worker: {
      _id,
      featuredSubscriptionValidTo,
      elite,
      active,
      suspendedTo,
      autoApplyToRequestsSubscriptionValidTo,
      multiFieldsSubscriptionValidTo,
      isAllowedToRecharge: isWorkerAllowedToRecharge,
      residenceInfo,
    },
    handleActivateWorker,
    handleSuspendWorker,
    handleFeatureWorker,
    handleMakeWorkerElite,
    handleToggleConfirmModal,
    handleToggleChangeWorkerPasswordModal,
    handleToggleSubscribeWorkerToAutoApplyToRequestsModal,
    handleToggleSubscribeWorkerToMultiFieldsModal,
    handleToggleEditWorkerModal,
    handleToggleAddCreditModal,
    finishWorkerAutoApplySubscription,
    finishWorkerMultiFieldsSubscription,
    handleWorkerAllowedToRecharge,
    isAllowWorkerToRechargeFetching,
    handleToggleSuspendWorkerModal,
    approveTheWorkerResidenceInfo,
  } = props;
  const {
    BRANDING_GREEN,
    GREY_LIGHT,
    GREY_DARK,
    WARNING,
    PRIMARY,
    LIGHT_RED,
    BRANDING_BLUE,
  } = COLORS;
  const activateWorkerConfirmModalHeader = !active ? 'تأكيد تنشيط الفني' : 'تأكيد عدم تنشيط الفني';
  const activateWorkerConfirmModalConfirmText = !active
    ? 'هل أنت متأكد أنك تريد تنشيط الفني؟'
    : 'هل أنت متأكد أنك تريد عدم تنشيط الفني؟';
  const activateWorkerConfirmModalFunction = () => {
    handleActivateWorker(_id, undefined, !active);
  };
  const suspendWorkerConfirmModalHeader = 'تأكيد عدم وقف الفني؟';
  const suspendWorkerConfirmModalConfirmText = 'هل أنت متأكد أنك تريد عدم وقف الفني؟';
  const suspendWorkerConfirmModalFunction = () => {
    handleSuspendWorker(_id, undefined, new Date());
  };
  const featureWorkerConfirmModalHeader = moment(featuredSubscriptionValidTo).isBefore(
    moment().utc(),
  )
    ? 'تأكيد تمييز الفني'
    : 'تأكيد عدم تمييز الفني';
  const featureWorkerConfirmModalConfirmText = moment(featuredSubscriptionValidTo).isBefore(
    moment().utc(),
  )
    ? 'هل أنت متأكد أنك تريد تمييز الفني؟'
    : 'هل أنت متأكد أنك تريد عدم تمييز الفني؟';
  const featureWorkerConfirmModalFunction = () => {
    handleFeatureWorker(
      _id,
      undefined,
      moment(featuredSubscriptionValidTo).isBefore(moment().utc()),
    );
  };
  const finishWorkerAutoApplySubscriptionConfirmModalFunction = () => {
    finishWorkerAutoApplySubscription(_id);
  };
  const finishWorkerMultiFieldsSubscriptionConfirmModalFunction = () => {
    finishWorkerMultiFieldsSubscription(_id);
  };
  const eliteWorkerConfirmModalHeader = !elite
    ? 'تأكيد جعل الفني من النخبة'
    : 'تأكيد عدم جعل الفني من النخبة';
  const eliteWorkerConfirmModalConfirmText = !elite
    ? 'هل أنت متأكد أنك تريد جعل الفني من النخبة؟'
    : 'هل أنت متأكد أنك تريد عدم جعل الفني من النخبة؟';
  const eliteWorkerConfirmModalFunction = () => {
    handleMakeWorkerElite(_id, undefined, !elite);
  };
  const isWorkerAllowedToRechargeConfirmModalHeader = isWorkerAllowedToRecharge
    ? 'ايقاف خاصية شحن الرصيد للفني'
    : 'تشغيل خاصية شحن الرصيد للفني';
  const isWorkerAllowedToRechargeConfirmModalText = isWorkerAllowedToRecharge
    ? 'هل أنت متأكد أنك تريد ايقاف خاصية شحن الرصيد للفني؟'
    : 'هل أنت متأكد أنك تريد تشغيل خاصية شحن الرصيد للفني؟';
  const isWorkerAllowedToRechargeConfirmModalFunction = () => {
    handleWorkerAllowedToRecharge(_id, !isWorkerAllowedToRecharge);
  };

  const parsedAutoApplyToRequestsSubscriptionValidTo = new Date(
    autoApplyToRequestsSubscriptionValidTo,
  );
  const approveResidenceInfoConfirmModalFunction = () => {
    approveTheWorkerResidenceInfo(_id);
  };
  const parsedMultiFieldsSubscriptionValidTo = new Date(multiFieldsSubscriptionValidTo);
  return (
    <Card
      flexWrap="wrap"
      justifyContent="center"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      mb={2}
      px={2}
      pt={4}
      pb={3}
    >
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse" alignItems="center">
        {permissions && permissions.includes('Add Credits To Worker') && isWorkerAllowedToRecharge && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={BRANDING_GREEN}
            onClick={handleToggleAddCreditModal}
            icon={faDollarSign}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            أضف رصيد
          </Button>
        )}
        {permissions && permissions.includes('Update Workers') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={BRANDING_BLUE}
            onClick={handleToggleEditWorkerModal}
            icon={faPen}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            تعديل الفني
          </Button>
        )}
        {permissions && permissions.includes('Change Worker Featured') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={WARNING}
            onClick={() =>
              handleToggleConfirmModal(
                featureWorkerConfirmModalHeader,
                featureWorkerConfirmModalConfirmText,
                featureWorkerConfirmModalFunction,
              )
            }
            icon={faStar}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            {moment(featuredSubscriptionValidTo).isSameOrAfter(moment().utc())
              ? 'إلغاء تمييز الفني'
              : 'تمييز الفني'}
          </Button>
        )}
        <Button
          minWidth="calc(50% - 8px)"
          mx={1}
          mb={2}
          color={GREY_DARK}
          onClick={() =>
            handleToggleConfirmModal(
              eliteWorkerConfirmModalHeader,
              eliteWorkerConfirmModalConfirmText,
              eliteWorkerConfirmModalFunction,
            )
          }
          icon={faCrown}
          iconWidth="lg"
          reverse
          xMargin={3}
          isLoading={false}
        >
          {elite ? 'إلغاء جعل الفني من النخبة' : 'جعل الفني من النخبة'}
        </Button>
        {permissions && permissions.includes('disable or re-enable worker recharge') && (
          <Button
            m={1}
            reverse
            color={isWorkerAllowedToRecharge ? LIGHT_RED : BRANDING_GREEN}
            icon={faMoneyBillWave}
            iconWidth="lg"
            xMargin={3}
            isLoading={isAllowWorkerToRechargeFetching}
            onClick={() =>
              handleToggleConfirmModal(
                isWorkerAllowedToRechargeConfirmModalHeader,
                isWorkerAllowedToRechargeConfirmModalText,
                isWorkerAllowedToRechargeConfirmModalFunction,
              )
            }
          >
            {isWorkerAllowedToRechargeConfirmModalHeader}
          </Button>
        )}
        {permissions && permissions.includes('Activate Worker') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={PRIMARY}
            onClick={() =>
              handleToggleConfirmModal(
                activateWorkerConfirmModalHeader,
                activateWorkerConfirmModalConfirmText,
                activateWorkerConfirmModalFunction,
              )
            }
            icon={faPowerOff}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            {active ? 'إلغاء تنشيط الفني' : 'تنشيط الفني'}
          </Button>
        )}
        {/* {permissions && permissions.includes('Delete Workers') && ( */}
        {/*  <Button */}
        {/*    minWidth="calc(50% - 8px)" */}
        {/*    mx={1} */}
        {/*    mb={2} */}
        {/*    color={LIGHT_RED} */}
        {/*    onClick={() => */}
        {/*      handleToggleConfirmModal( */}
        {/*        deleteWorkerConfirmModalHeader, */}
        {/*        deleteWorkerConfirmModalConfirmText, */}
        {/*        deleteWorkerConfirmModalFunction, */}
        {/*      ) */}
        {/*    } */}
        {/*    icon={faTrashAlt} */}
        {/*    iconWidth="lg" */}
        {/*    reverse */}
        {/*    xMargin={3} */}
        {/*    isLoading={false} */}
        {/*  > */}
        {/*    مسح الفني */}
        {/*  </Button> */}
        {/* )} */}
        {permissions &&
          permissions.includes('Suspend Workers') &&
          (!suspendedTo || Date.parse(new Date(suspendedTo)) < Date.parse(new Date())) && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={LIGHT_RED}
            onClick={() => handleToggleSuspendWorkerModal(undefined, worker)}
            icon={faHandPaper}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
              وقف الفني
          </Button>
          )}
        {permissions &&
          permissions.includes('unSuspend Workers') &&
          suspendedTo &&
          Date.parse(new Date(suspendedTo)) > Date.parse(new Date()) && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={LIGHT_RED}
            onClick={() =>
              handleToggleConfirmModal(
                suspendWorkerConfirmModalHeader,
                suspendWorkerConfirmModalConfirmText,
                suspendWorkerConfirmModalFunction,
              )
            }
            icon={faHandPaper}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
              إلغاء وقف الفني
          </Button>
          )}
        {permissions && permissions.includes('Set Worker Password') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={BRANDING_BLUE}
            onClick={handleToggleChangeWorkerPasswordModal}
            icon={faLock}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
            تغيير كلمة المرور
          </Button>
        )}
        {(!autoApplyToRequestsSubscriptionValidTo ||
          Date.parse(parsedAutoApplyToRequestsSubscriptionValidTo) < Date.parse(new Date())) &&
          permissions &&
          permissions.includes('Subscribe To Auto Apply') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={BRANDING_BLUE}
            onClick={handleToggleSubscribeWorkerToAutoApplyToRequestsModal}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
              الاشتراك فى التقديم التلقائى على الطلبات
          </Button>
          )}
        {autoApplyToRequestsSubscriptionValidTo &&
          Date.parse(parsedAutoApplyToRequestsSubscriptionValidTo) > Date.parse(new Date()) &&
          permissions &&
          permissions.includes('Subscribe To Auto Apply') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={LIGHT_RED}
            onClick={() =>
              handleToggleConfirmModal(
                'تاكيد انهاء اشتراك التقديم التلقائى على الطلبات',
                'هل متاكد انك تريد انهاء اشتراك التقديم التلقائى على الطلبات للفنى',
                finishWorkerAutoApplySubscriptionConfirmModalFunction,
              )
            }
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
              انهاء التقديم التلقائى على الطلبات
          </Button>
          )}
        {(!multiFieldsSubscriptionValidTo ||
          Date.parse(parsedMultiFieldsSubscriptionValidTo) < Date.parse(new Date())) &&
          permissions &&
          permissions.includes('Subscribe To Multi Fields') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={BRANDING_BLUE}
            onClick={handleToggleSubscribeWorkerToMultiFieldsModal}
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
              الاشتراك فى العمل باكتر من مهنه
          </Button>
          )}
        {multiFieldsSubscriptionValidTo &&
          Date.parse(parsedMultiFieldsSubscriptionValidTo) > Date.parse(new Date()) &&
          permissions &&
          permissions.includes('Subscribe To Multi Fields') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
            mb={2}
            color={LIGHT_RED}
            onClick={() =>
              handleToggleConfirmModal(
                'تاكيد انهاء اشتراك العمل باكتر من خدمة',
                'هل متاكد انك تريد انهاء اشتراك العمل باكتر من خدمة للفنى',
                finishWorkerMultiFieldsSubscriptionConfirmModalFunction,
              )
            }
            iconWidth="lg"
            reverse
            xMargin={3}
            isLoading={false}
          >
              انهاء العمل باكثر من مهنه
          </Button>
          )}

        {residenceInfo &&
          !residenceInfo.approved &&
          permissions &&
          permissions.includes('approve workers residence info') && (
          <Button
            minWidth="calc(50% - 8px)"
            mx={1}
              mb={2}
              color={BRANDING_BLUE}
              onClick={() =>
                handleToggleConfirmModal(
                  'تاكيد الموافقة على بيانات الاقامة الخاصة بى الفنى',
                  'هل متاكد انك تريد الموافقة على بيانات الاقامة الخاصة بى الفنى',
                approveResidenceInfoConfirmModalFunction,
                )
            }
              iconWidth="lg"
              reverse
              xMargin={3}
              isLoading={false}
          >
              الموافقة على بيانات الاقامة
          </Button>
          )}
      </Flex>
    </Card>
  );
};
CallToActions.displayName = 'CallToActions';

CallToActions.propTypes = {
  user: PropTypes.shape({}),
  worker: PropTypes.shape({}).isRequired,
  handleActivateWorker: PropTypes.func,
  handleSuspendWorker: PropTypes.func,
  // handleDeleteWorker: PropTypes.func,
  handleFeatureWorker: PropTypes.func,
  handleMakeWorkerElite: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleChangeWorkerPasswordModal: PropTypes.func,
  handleToggleSubscribeWorkerToAutoApplyToRequestsModal: PropTypes.func,
  handleToggleSubscribeWorkerToMultiFieldsModal: PropTypes.func,
  handleToggleEditWorkerModal: PropTypes.func,
  handleToggleAddCreditModal: PropTypes.func,
  finishWorkerAutoApplySubscription: PropTypes.func,
  finishWorkerMultiFieldsSubscription: PropTypes.func,
  handleWorkerAllowedToRecharge: PropTypes.func,
  isAllowWorkerToRechargeFetching: PropTypes.bool,
  isAllowWorkerToRechargeError: PropTypes.bool,
  isAllowWorkerToRechargeErrorMessage: PropTypes.string,
  isAllowWorkerToRechargeSuccess: PropTypes.bool,
  handleToggleSuspendWorkerModal: PropTypes.func,
  approveTheWorkerResidenceInfo: PropTypes.func,
};

CallToActions.defaultProps = {
  user: undefined,
  handleActivateWorker: () => {},
  handleSuspendWorker: () => {},
  // handleDeleteWorker: () => {},
  handleFeatureWorker: () => {},
  handleMakeWorkerElite: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleChangeWorkerPasswordModal: () => {},
  handleToggleSubscribeWorkerToAutoApplyToRequestsModal: () => {},
  handleToggleSubscribeWorkerToMultiFieldsModal: () => {},
  handleToggleEditWorkerModal: () => {},
  handleToggleAddCreditModal: () => {},
  finishWorkerAutoApplySubscription: () => {},
  finishWorkerMultiFieldsSubscription: () => {},
  handleWorkerAllowedToRecharge: () => {},
  isAllowWorkerToRechargeFetching: false,
  isAllowWorkerToRechargeError: false,
  isAllowWorkerToRechargeErrorMessage: undefined,
  isAllowWorkerToRechargeSuccess: false,
  handleToggleSuspendWorkerModal: () => {},
  approveTheWorkerResidenceInfo: () => {},
};

export default CallToActions;
