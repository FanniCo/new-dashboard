import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';
import styled from 'styled-components';

const IdentityImageRow = props => {
  const { identityImage } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const Image = styled.img`
    width: calc(100% - 25px);
  `;

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          صورة الهوية
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {identityImage ? <Image src={identityImage} /> : '-'}
        </Text>
      </Flex>
    </OddRow>
  );
};
IdentityImageRow.displayName = 'IdentityImageRow';

IdentityImageRow.propTypes = {
  identityImage: PropTypes.string,
};

IdentityImageRow.defaultProps = {
  identityImage: null,
};
export default IdentityImageRow;
