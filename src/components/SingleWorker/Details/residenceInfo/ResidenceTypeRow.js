import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { EvenRow } from 'components/shared';
import { RESIDENCE_TYPE } from 'utils/constants';

const ResidenceTypeRow = props => {
  const { residenceType } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <EvenRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          نوع الاقامة
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {residenceType ? RESIDENCE_TYPE[residenceType].ar : '-'}
        </Text>
      </Flex>
    </EvenRow>
  );
};
ResidenceTypeRow.displayName = 'ResidenceTypeRow';

ResidenceTypeRow.propTypes = {
  residenceType: PropTypes.string,
};

ResidenceTypeRow.defaultProps = {
  residenceType: null,
};

export default ResidenceTypeRow;
