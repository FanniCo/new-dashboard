import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { Label } from '@rebass/forms';
import Slider from 'rc-slider';
import Text from 'components/Text';
import 'rc-slider/assets/index.css';

const RateFactors = props => {
  const {
    workerMotivationAndPunishments,
    isGetWorkerMotivationAndPunishmentsFetching,
    isGetWorkerMotivationAndPunishmentsSuccess,
  } = props;
  const { GREY_LIGHT } = COLORS;
  let motivationRender = (
    <ShimmerEffect width={1}>
      <Rect width={1} height={550} m={2} />
    </ShimmerEffect>
  );
  let punishmentsRender = (
    <ShimmerEffect width={1}>
      <Rect width={1} height={550} m={2} />
    </ShimmerEffect>
  );

  if (!isGetWorkerMotivationAndPunishmentsFetching && isGetWorkerMotivationAndPunishmentsSuccess) {
    motivationRender = workerMotivationAndPunishments.motivations.map(motivation => (
      <Flex key={motivation.title.en} flexDirection="column" width={1} p={1}>
        <Box>
          <Flex flexWrap="wrap" flexDirection="row-reverse">
            <Text htmlFor="percent">{motivation.title}</Text>
          </Flex>
          <Flex flexWrap="wrap" flexDirection="row">
            <Text htmlFor="percent">{motivation.upperLimit}</Text>
            <Text htmlFor="percent"> &nbsp; من &nbsp;</Text>
            <Text htmlFor="percent">{motivation.counter}</Text>
          </Flex>
          <Flex flexWrap="wrap" flexDirection="row">
            <Slider
              reverse
              min={0}
              max={motivation.upperLimit}
              value={motivation.counter}
              trackStyle={{ backgroundColor: 'green' }}
            />
          </Flex>
          <Flex
            flexWrap="wrap"
            flexDirection="row-reverse"
            justifyContent="center"
            alignItems="center"
          >
            <Text htmlFor="percent">{motivation.actionText}</Text>
          </Flex>
        </Box>
      </Flex>
    ));
  }

  if (!isGetWorkerMotivationAndPunishmentsFetching && isGetWorkerMotivationAndPunishmentsSuccess) {
    punishmentsRender = workerMotivationAndPunishments.punishments.map(punishment => (
      <Flex key={punishment.title.en} flexDirection="column" width={1} p={1}>
        <Box>
          <Flex flexWrap="wrap" flexDirection="row-reverse">
            <Text htmlFor="percent">{punishment.title}</Text>
          </Flex>
          <Flex flexWrap="wrap" flexDirection="row">
            <Text htmlFor="percent">{punishment.upperLimit}</Text>
            <Text htmlFor="percent"> &nbsp; من &nbsp;</Text>
            <Text htmlFor="percent">{punishment.counter}</Text>
          </Flex>
          <Flex flexWrap="wrap" flexDirection="row">
            <Slider
              reverse
              min={0}
              max={punishment.upperLimit}
              value={punishment.counter}
              trackStyle={{ backgroundColor: 'green' }}
            />
          </Flex>
          <Flex
            flexWrap="wrap"
            flexDirection="row-reverse"
            justifyContent="center"
            alignItems="center"
          >
            <Text htmlFor="percent">{punishment.actionText}</Text>
          </Flex>
        </Box>
      </Flex>
    ));
  }

  return (
    <Flex width={1} flexDirection="column">
      <Card
        flexWrap="wrap"
        justifyContent="center"
        bgColor={COLORS_VALUES[GREY_LIGHT]}
        color="white"
        mb={2}
        px={2}
        pt={4}
        pb={3}
      >
        <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
          <Label htmlFor="percent" alignItems="center" justifyContent="center">
            الحوافر
          </Label>
          {motivationRender}
        </Flex>
      </Card>
      <Card
        flexWrap="wrap"
        justifyContent="center"
        bgColor={COLORS_VALUES[GREY_LIGHT]}
        color="white"
        mb={2}
        px={2}
        pt={4}
        pb={3}
      >
        <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
          <Label htmlFor="percent" alignItems="center" justifyContent="center">
            العواقب
          </Label>
          {punishmentsRender}
        </Flex>
      </Card>
    </Flex>
  );
};
RateFactors.displayName = 'RateFactors';

RateFactors.propTypes = {
  workerMotivationAndPunishments: PropTypes.shape({}),
  isGetWorkerMotivationAndPunishmentsFetching: PropTypes.bool,
  isGetWorkerMotivationAndPunishmentsSuccess: PropTypes.bool,
};

RateFactors.defaultProps = {
  workerMotivationAndPunishments: undefined,
  isGetWorkerMotivationAndPunishmentsFetching: false,
  isGetWorkerMotivationAndPunishmentsSuccess: false,
};

export default RateFactors;
