import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { OddRow } from 'components/shared';

const AutoApplyToRequestRow = props => {
  const { autoApplyToRequestsSubscriptionValidTo } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const AutoApplyToRequestsSubscriptionValidToObject = new Date(
    autoApplyToRequestsSubscriptionValidTo,
  );
  const parseAutoApplyToRequestsSubscriptionValidTo = new Date(
    autoApplyToRequestsSubscriptionValidTo,
  ).toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    hour12: true,
  });

  return (
    <OddRow flexDirection="row-reverse" alignItems="center" py={3} px={2}>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          مشترك فى التقديم التلقائى على الطلبات
        </Text>
      </Flex>
      <Flex width={0.5}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          {!autoApplyToRequestsSubscriptionValidTo ? 'لا' : ''}
          {Date.parse(AutoApplyToRequestsSubscriptionValidToObject) < Date.parse(new Date())
            ? 'الاشتراك منتهى'
            : ''}

          {Date.parse(AutoApplyToRequestsSubscriptionValidToObject) > Date.parse(new Date())
            ? ` ${parseAutoApplyToRequestsSubscriptionValidTo}    نعم و ينتهى الاشترك`
            : ''}
        </Text>
      </Flex>
    </OddRow>
  );
};
AutoApplyToRequestRow.displayName = 'AutoApplyToRequestRow';

AutoApplyToRequestRow.propTypes = {
  autoApplyToRequestsSubscriptionValidTo: PropTypes.string,
};

AutoApplyToRequestRow.defaultProps = {
  autoApplyToRequestsSubscriptionValidTo: undefined,
};

export default AutoApplyToRequestRow;
