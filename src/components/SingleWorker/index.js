import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { borderBottom, color } from 'styled-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FIELDS } from 'utils/constants';
import { getFieldIcon } from 'utils/shared/style';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import Text from 'components/Text';
import ConfirmModal from 'components/ConfirmModal';
import ChangeWorkerPasswordModal from 'components/ChangeWorkerPasswordModal';
import SubscribeWorkerModal from 'components/SubscribeWorkerModal';
import EditWorkerModal from 'components/EditWorkerModal';
import AddCreditToWorkerModal from 'components/AddCreditToWorkerModal';
import { CardLabel } from 'components/shared';
import Comments from 'components/Comments';
import defaultProfileImage from 'static/images/defaultProfileImage.png';
import { Urls } from 'utils/api';
import SuspendModal from 'components/SuspendModal';
import UserNameRow from './Details/UserNameRow';
import GovernmentIdRow from './Details/GovernmentIdRow';
import SponsorRow from './Details/SponsorRow';
import FieldsRow from './Details/FieldsRow';
import CreditsRow from './Details/CreditsRow';
import IncomeRow from './Details/IncomeRow';
import CityRow from './Details/CityRow';
import SuspendedRow from './Details/SuspendedRow';
import FeaturedRow from './Details/FeaturedRow';
import EliteRow from './Details/EliteRow';
import AverageRatingRow from './Details/AverageRatingRow';
import WorkerLevelRow from './Details/WorkerLevelRow';
import CreatedAtDateRow from './Details/CreatedAtDateRow';
import UpdatedAtDateRow from './Details/UpdatedAtDateRow';
import LocationRow from './Details/LocationRow';
import CallToActions from './Details/CallToActions';
import WorkerRequests from './Requests';
import WorkerTransactions from './Transactions';
import WorkerReviews from './Reviews';
import GenderRow from './Details/GenderRow';
import DateOfBirthRow from './Details/DateOfBirthRow';
import AutoApplyToRequestRow from './Details/AutoApplyToRequestRow';
import MultiFieldsSubscriptionRow from './Details/MultiFieldsSubscriptionRow';
import TotalWorkerWageRow from './Details/TotalWorkerWageRow';
import CompletedRequestsRow from './Details/CompletedRequestsRow';
import CanceledRequestsRow from './Details/CanceledRequestsRow';
import PostponedRequestsRow from './Details/PostponedRequestsRow';
import RateFactors from './Details/RateFactors';
import VendorRow from './Details/VendorRow';
import MotivationsAndPunishmentsFactors from './Details/MotivationsAndPunishmentsFactors';
import AllowedNumberOfRequestToPostponeRow from './Details/AllowedNumberOfRequestToPostponeRow';
import TheLastTimeWorkerAppliedToRequestsRow from './Details/TheLastTimeWorkerAppliedToRequests';

import CareerAccordingToResidenceRow from './Details/residenceInfo/CareerAccordingToResidenceRow';
import CommercialLicenseImageRow from './Details/residenceInfo/CommercialLicenseImageRow';
import IdentityImageRow from './Details/residenceInfo/IdentityImageRow';
import NationalityRow from './Details/residenceInfo/NationalityRow';
import OrganizationNameRow from './Details/residenceInfo/OrganizationNameRow';
import ResidenceTypeRow from './Details/residenceInfo/ResidenceTypeRow';
import TermsAndConditionsFanniImageRow from './Details/residenceInfo/TermsAndConditionsFanniImageRow';
import ResidenceInfoSponsorRow from './Details/residenceInfo/SponsorRow';
import SponsorPhoneNumberRow from './Details/residenceInfo/SponsorPhoneNumberRow';
import WorkerTickets from './WorkerTickets';
import TicketsCountRow from "components/SingleWorker/Details/ticketsCountRow";
import TicketsCountWithDoneRequestsRow from "components/SingleWorker/Details/ticketsCountWithDoneRequetsRow";
import ReceivedShirtRow
  from "components/SingleWorker/Details/ReceivedShirtRow";
import AdditionalUserNameRow
  from "components/SingleWorker/Details/AdditionalUserNameRow";

const { DISABLED, GREY_DARK, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { TITLE, HEADING, BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const PageTitle = styled(Flex)`
  direction: rtl;
`;

const TabsContainer = styled(Flex)`
  ${borderBottom};
`;

const Tab = styled(Flex)`
  cursor: pointer;
  width: max-content;
  ${color};
  ${borderBottom};
`;

const CommentsContainer = styled(Flex)`
  direction: rtl;
  background-color: ${COLORS_VALUES[GREY_LIGHT]};
`;

const SingleWorker = props => {
  const { workerDetails, isGetWorkerDetailsFetching } = props;

  if (isGetWorkerDetailsFetching || !workerDetails) {
    return (
      <ShimmerEffect width={1}>
        <Rect width={1 / 3} height={550} m={2} />
        <Rect width={2 / 3} height={550} m={2} />
      </ShimmerEffect>
    );
  }

  const {
    handleTabChange,
    tabs,
    activeTab,
    user,
    user: { permissions },
    workerDetails: {
      _id,
      name,
      username,
      fields,
      field,
      credits,
      city,
      suspendedTo,
      featuredSubscriptionValidTo,
      elite,
      sponsor,
      governmentId,
      workerLevel,
      createdAt,
      updatedAt,
      location,
      active,
      gender,
      birthDate,
      picURL,
      autoApplyToRequestsSubscriptionValidTo,
      multiFieldsSubscriptionValidTo,
      rateSum,
      numberOfReviews,
      vendor,
      allowedNumberOfRequestsWorkerCanPostpone,
      income,
      lastTimeWorkerAppliedToRequests,
      residenceInfo = {},
      ticketsCount,
      workerReceivedShirt,
      additionalUsername,
    },
    openWorkerLocation,
    isConfirmModalOpen,
    isChangeWorkerPasswordModalOpen,
    isSubscribeWorkerToAutoApplyModalOpen,
    isSubscribeWorkerToMultiFieldsModalOpen,
    isEditWorkerModalOpen,
    isAddCreditModalOpen,
    confirmModalHeader,
    confirmModalText,
    confirmModalFunction,
    handleToggleConfirmModal,
    handleToggleChangeWorkerPasswordModal,
    handleToggleSubscribeWorkerToAutoApplyToRequestsModal,
    handleToggleSubscribeWorkerToMultiFieldsModal,
    handleToggleEditWorkerModal,
    handleToggleAddCreditModal,
    handleActivateWorker,
    handleSuspendWorker,
    handleChangeWorkerPassword,
    // handleDeleteWorker,
    handleFeatureWorker,
    handleMakeWorkerElite,
    isActivateWorkerFetching,
    isActivateWorkerError,
    isSuspendWorkerFetching,
    isSuspendWorkerError,
    isChangeWorkerPasswordFetching,
    isChangeWorkerPasswordError,
    isFeatureWorkerFetching,
    isFeatureWorkerError,
    isMakeWorkerEliteFetching,
    isMakeWorkerEliteError,
    isMakeWorkerEliteErrorMessage,
    isDeleteWorkerFetching,
    addedCommentValue,
    handleAddCommentInputChange,
    resetAddCommentInput,
    addComment,
    addCommentState,
    addCommentErrorMessage,
    workerStatistics,
    isGetWorkerStatisticsSuccess,
    isGetWorkerStatisticsFetching,
    workerMotivationAndPunishments,
    isGetWorkerMotivationAndPunishmentsFetching,
    isGetWorkerMotivationAndPunishmentsSuccess,
    finishWorkerAutoApplySubscription,
    finishWorkerMultiFieldsSubscription,
    isFinishWorkerAutoApplySubscriptionFetching,
    isFinishWorkerMultiFieldsSubscriptionFetching,
    handleToggleSuspendWorkerModal,
    isSuspendWorkerModalOpen,
    suspendWorkerErrorMessage,
    vendorsList,
    admins,
    mentionData,
    featureWorkerErrorMessage,
    handleWorkerAllowedToRecharge,
    isAllowWorkerToRechargeFetching,
    isAllowWorkerToRechargeError,
    isAllowWorkerToRechargeErrorMessage,
    isAllowWorkerToRechargeSuccess,

    approveTheWorkerResidenceInfo,
    isApprovingTheWorkerResidenceInfoFetching,
    isApprovingTheWorkerResidenceInfoError,
    approvingTheWorkerResidenceInfoErrorMessage,
  } = props;
  const workerCreatedAtDate = new Date(createdAt);
  const workerUpdatedAtDate = new Date(updatedAt);
  const { workers } = Urls;
  const ProfileImage = styled.img`
    width: calc(50% - 25px);
    border-radius: 50%;
  `;

  let fieldsContent;

  if (fields && fields.length) {
    fieldsContent = fields.map(fieldKey => {
      const fieldIcon = getFieldIcon(fieldKey);

      return (
        <Flex key={fieldKey} ml={4} mb={1}>
          <Box>
            <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
          </Box>
          <CardLabel
            py={1}
            px={2}
            ml={2}
            type={BODY}
            border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
            color={COLORS_VALUES[DISABLED]}
            fontWeight={NORMAL}
          >
            {FIELDS[fieldKey].ar}
          </CardLabel>
        </Flex>
      );
    });
  } else if (field) {
    const fieldIcon = getFieldIcon(field);

    fieldsContent = (
      <Flex alignItems="center" ml={4} mb={1}>
        <Box>
          <FontAwesomeIcon icon={fieldIcon} color={COLORS_VALUES[BRANDING_GREEN]} size="sm" />
        </Box>
        <CardLabel
          py={1}
          px={2}
          ml={2}
          type={BODY}
          border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
          color={COLORS_VALUES[DISABLED]}
          fontWeight={NORMAL}
        >
          {FIELDS[field].ar}
        </CardLabel>
      </Flex>
    );
  }
  
  return (
    <Flex flexDirection="row-reverse" flexWrap="wrap" width={1}>
      <Helmet>
        <title>اسم الفني : {name}</title>
      </Helmet>
      <Flex flexDirection="column" flexWrap="wrap" width={[1, 1, 1, 2 / 3]} pl={[0, 0, 0, 3, 3]}>
        <Flex flexDirection="row-reverse" width={1}>
          <PageTitle mb={5} pt={5}>
            <Text type={TITLE} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {name} - {active ? 'نشط' : 'غير نشط'}
            </Text>
          </PageTitle>
        </Flex>
        <Flex flexDirection="row-reverse" width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <TabsContainer flexDirection="row-reverse" alignItems="center">
              {tabs.map((tab, index) => (
                <Tab
                  key={tab.name}
                  flexDirection="row-reverse"
                  alignItems="center"
                  px={5}
                  py={3}
                  borderBottom={
                    activeTab === index ? `2px solid ${COLORS_VALUES[BRANDING_GREEN]}` : 'none'
                  }
                  onClick={() => handleTabChange(index, tab.drawMap)}
                >
                  <Text
                    cursor="pointer"
                    textAlign="center"
                    color={
                      activeTab === index ? COLORS_VALUES[BRANDING_GREEN] : COLORS_VALUES[DISABLED]
                    }
                    type={HEADING}
                    fontWeight={NORMAL}
                    mx={3}
                  >
                    {tab.name}
                  </Text>
                </Tab>
              ))}
            </TabsContainer>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              {activeTab === 0 && (
                <>
                  <UserNameRow username={username} />
                  <AdditionalUserNameRow additionalUsername={additionalUsername} />
                  <VendorRow vendor={vendor} />
                  <GovernmentIdRow governmentId={governmentId} />
                  <SponsorRow sponsor={sponsor} />
                  <FieldsRow fieldsContent={fieldsContent} />
                  <CreditsRow credits={credits} />
                  <IncomeRow income={income} />
                  <CityRow city={city} />
                  <SuspendedRow suspended={suspendedTo} />
                  <FeaturedRow featuredSubscriptionValidTo={featuredSubscriptionValidTo} />
                  <EliteRow elite={elite} />
                  <AverageRatingRow rateSum={rateSum} numberOfReviews={numberOfReviews} />
                  <WorkerLevelRow workerLevel={workerLevel} />
                  <CreatedAtDateRow workerCreatedAtDate={workerCreatedAtDate} />
                  <UpdatedAtDateRow workerUpdatedAtDate={workerUpdatedAtDate} />
                  <LocationRow location={location} openWorkerLocation={openWorkerLocation} />
                  <ReceivedShirtRow workerReceivedShirt={workerReceivedShirt} />
                  {gender && <GenderRow gender={gender} />}
                  {birthDate && <DateOfBirthRow dateOfBirth={birthDate} />}
                  <AutoApplyToRequestRow
                    autoApplyToRequestsSubscriptionValidTo={autoApplyToRequestsSubscriptionValidTo}
                  />
                  <MultiFieldsSubscriptionRow
                    multiFieldsSubscriptionValidTo={multiFieldsSubscriptionValidTo}
                  />
                  {isGetWorkerStatisticsSuccess && (
                    <TotalWorkerWageRow
                      totalWorkerWage={workerStatistics.statistics.totalWorkerWage}
                    />
                  )}
                  {isGetWorkerStatisticsSuccess && (
                    <CompletedRequestsRow
                      done={workerStatistics.statistics.requestsCounts.done}
                      reviewed={workerStatistics.statistics.requestsCounts.reviewed}
                    />
                  )}
                  {isGetWorkerStatisticsSuccess && (
                    <CanceledRequestsRow
                      canceled={workerStatistics.statistics.requestsCounts.canceled}
                    />
                  )}
                  {isGetWorkerStatisticsSuccess && (
                    <PostponedRequestsRow
                      postponed={workerStatistics.statistics.requestsCounts.postponed}
                    />
                  )}
                  <AllowedNumberOfRequestToPostponeRow
                    allowedNumberOfRequestsWorkerCanPostpone={
                      allowedNumberOfRequestsWorkerCanPostpone
                    }
                  />
                  <TheLastTimeWorkerAppliedToRequestsRow
                    lastTimeWorkerAppliedToRequests={lastTimeWorkerAppliedToRequests}
                  />
                  {isGetWorkerStatisticsSuccess && (
                    <>
                      <TicketsCountRow ticketsCount={ticketsCount} />
                      <TicketsCountWithDoneRequestsRow
                        ticketsCount={ticketsCount}
                        doneRequestsCount={workerStatistics.statistics.requestsCounts.done + workerStatistics.statistics.requestsCounts.reviewed}
                      />
                    </>
                  )}
                </>
              )}
              {activeTab === 1 && (
                <>
                  <NationalityRow nationality={residenceInfo.nationality} />
                  <ResidenceTypeRow residenceType={residenceInfo.residenceType} />
                  <IdentityImageRow identityImage={workerDetails.identificationUrl} />
                  <CareerAccordingToResidenceRow
                    careerAccordingToResidence={residenceInfo.careerAccordingToResidence}
                  />
                  <CommercialLicenseImageRow
                    commercialLicenseImage={residenceInfo.commercialLicenseImageUrl}
                  />
                  <ResidenceInfoSponsorRow sponsor={residenceInfo.sponsorName} />
                  <SponsorPhoneNumberRow sponsorPhoneNumber={residenceInfo.sponsorPhoneNumber} />
                  <OrganizationNameRow organizationName={residenceInfo.organizationName} />
                  <TermsAndConditionsFanniImageRow
                    termsAndConditionsFanniImage={residenceInfo.TermsAndconditionsFanniUrl}
                  />
                </>
              )}
              {activeTab === 2 && <WorkerTransactions workerId={_id} user={user} />}
              {activeTab === 3 && <WorkerRequests workerId={_id} user={user} />}
              {activeTab === 4 && <WorkerReviews workerId={_id} user={user} />}
              {activeTab === 5 && <WorkerTickets workerId={_id} user={user} />}
            </Card>
          </Box>
        </Flex>
      </Flex>
      <Flex flexDirection="column" flexWrap="wrap" width={[1, 1, 1, 1 / 3]}>
        <Box width={[1, 1, 1, 1, 1]}>
          <Flex
            flexDirection="row"
            flexWrap="wrap"
            justifyContent="center"
            alignItems="center"
            mb={2}
            px={2}
            py={3}
          >
            <ProfileImage src={picURL || defaultProfileImage} alt="Profile Image" />
          </Flex>
          <CallToActions
            user={user}
            worker={workerDetails}
            handleToggleConfirmModal={handleToggleConfirmModal}
            handleToggleChangeWorkerPasswordModal={handleToggleChangeWorkerPasswordModal}
            handleToggleEditWorkerModal={handleToggleEditWorkerModal}
            handleToggleAddCreditModal={handleToggleAddCreditModal}
            handleActivateWorker={handleActivateWorker}
            handleSuspendWorker={handleSuspendWorker}
            handleChangeWorkerPassword={handleChangeWorkerPassword}
            handleFeatureWorker={handleFeatureWorker}
            handleMakeWorkerElite={handleMakeWorkerElite}
            // handleDeleteWorker={handleDeleteWorker}
            isActivateWorkerFetching={isActivateWorkerFetching}
            isActivateWorkerError={isActivateWorkerError}
            isSuspendWorkerFetching={isSuspendWorkerFetching}
            isSuspendWorkerError={isSuspendWorkerError}
            isChangeWorkerPasswordFetching={isChangeWorkerPasswordFetching}
            isChangeWorkerPasswordError={isChangeWorkerPasswordError}
            isFeatureWorkerFetching={isFeatureWorkerFetching}
            isFeatureWorkerError={isFeatureWorkerError}
            isMakeWorkerEliteFetching={isMakeWorkerEliteFetching}
            isMakeWorkerEliteError={isMakeWorkerEliteError}
            handleToggleSubscribeWorkerToAutoApplyToRequestsModal={
              handleToggleSubscribeWorkerToAutoApplyToRequestsModal
            }
            handleToggleSubscribeWorkerToMultiFieldsModal={
              handleToggleSubscribeWorkerToMultiFieldsModal
            }
            finishWorkerAutoApplySubscription={finishWorkerAutoApplySubscription}
            finishWorkerMultiFieldsSubscription={finishWorkerMultiFieldsSubscription}
            handleToggleSuspendWorkerModal={handleToggleSuspendWorkerModal}
            isSuspendWorkerModalOpen={isSuspendWorkerModalOpen}
            handleWorkerAllowedToRecharge={handleWorkerAllowedToRecharge}
            isAllowWorkerToRechargeFetching={isAllowWorkerToRechargeFetching}
            isAllowWorkerToRechargeError={isAllowWorkerToRechargeError}
            isAllowWorkerToRechargeErrorMessage={isAllowWorkerToRechargeErrorMessage}
            isAllowWorkerToRechargeSuccess={isAllowWorkerToRechargeSuccess}
            approveTheWorkerResidenceInfo={approveTheWorkerResidenceInfo}
          />
          <RateFactors
            workerStatistics={workerStatistics}
            isGetWorkerStatisticsFetching={isGetWorkerStatisticsFetching}
            isGetWorkerStatisticsSuccess={isGetWorkerStatisticsSuccess}
          />
          <MotivationsAndPunishmentsFactors
            workerMotivationAndPunishments={workerMotivationAndPunishments}
            isGetWorkerMotivationAndPunishmentsFetching={
              isGetWorkerMotivationAndPunishmentsFetching
            }
            isGetWorkerMotivationAndPunishmentsSuccess={isGetWorkerMotivationAndPunishmentsSuccess}
          />
          <CommentsContainer width={1} mb={2}>
            <Comments
              isModal={false}
              objectType="worker"
              object={workerDetails}
              addedCommentValue={addedCommentValue}
              handleAddCommentInputChange={handleAddCommentInputChange}
              addComment={addComment}
              addCommentState={addCommentState}
              addCommentErrorMessage={addCommentErrorMessage}
              resetAddCommentInput={resetAddCommentInput}
              hideAddCommentSection={
                !(permissions && permissions.includes('Add New User Comments'))
              }
              admins={admins}
              mentionData={mentionData}
            />
          </CommentsContainer>
        </Box>
      </Flex>
      {isConfirmModalOpen && (
        <ConfirmModal
          isOpened={isConfirmModalOpen}
          header={confirmModalHeader}
          confirmText={confirmModalText}
          toggleFunction={handleToggleConfirmModal}
          confirmFunction={confirmModalFunction}
          isConfirming={
            isActivateWorkerFetching ||
            isSuspendWorkerFetching ||
            isDeleteWorkerFetching ||
            isFeatureWorkerFetching ||
            isMakeWorkerEliteFetching ||
            isFinishWorkerAutoApplySubscriptionFetching ||
            isFinishWorkerMultiFieldsSubscriptionFetching ||
            isAllowWorkerToRechargeFetching ||
            isApprovingTheWorkerResidenceInfoFetching
          }
          isFail={
            isFeatureWorkerError ||
            isMakeWorkerEliteError ||
            isAllowWorkerToRechargeError ||
            isApprovingTheWorkerResidenceInfoError
          }
          errorMessage={
            featureWorkerErrorMessage ||
            isMakeWorkerEliteErrorMessage ||
            isAllowWorkerToRechargeErrorMessage ||
            approvingTheWorkerResidenceInfoErrorMessage
          }
        />
      )}
      {isEditWorkerModalOpen && (
        <EditWorkerModal workerDetails={workerDetails} vendorsList={vendorsList} />
      )}
      {isChangeWorkerPasswordModalOpen && (
        <ChangeWorkerPasswordModal workerDetails={workerDetails} />
      )}
      {isSubscribeWorkerToAutoApplyModalOpen && (
        <SubscribeWorkerModal
          workerDetails={workerDetails}
          ModalHeader="الاشتراك فى التقديم التلقائى على الطلبات"
          handleToggleModal={handleToggleSubscribeWorkerToAutoApplyToRequestsModal}
          endpoint={workers.subscribeToAutoApply}
        />
      )}
      {isSubscribeWorkerToMultiFieldsModalOpen && (
        <SubscribeWorkerModal
          workerDetails={workerDetails}
          ModalHeader="الاشتراك فى العمل باكتر من مهنه"
          handleToggleModal={handleToggleSubscribeWorkerToMultiFieldsModal}
          endpoint={workers.subscribeToMultiFields}
        />
      )}
      {isAddCreditModalOpen && <AddCreditToWorkerModal workerDetails={workerDetails} />}
      {isSuspendWorkerModalOpen && (
        <SuspendModal
          userIndex={undefined}
          userDetails={workerDetails}
          handleToggleSuspendUserModal={handleToggleSuspendWorkerModal}
          isOpened={isSuspendWorkerModalOpen}
          suspendUser={handleSuspendWorker}
          isSuspendUserError={isSuspendWorkerError}
          suspendUserErrorMessage={suspendWorkerErrorMessage}
          isSuspendUserFetching={isSuspendWorkerFetching}
        />
      )}
    </Flex>
  );
};
SingleWorker.displayName = 'SingleWorker';

SingleWorker.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.shape({})),
  activeTab: PropTypes.number,
  handleTabChange: PropTypes.func,
  user: PropTypes.shape({}),
  workerDetails: PropTypes.shape({}),
  isGetWorkerDetailsFetching: PropTypes.bool,
  openWorkerLocation: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isChangeWorkerPasswordModalOpen: PropTypes.bool,
  isSubscribeWorkerToAutoApplyModalOpen: PropTypes.bool,
  isSubscribeWorkerToMultiFieldsModalOpen: PropTypes.bool,
  isEditWorkerModalOpen: PropTypes.bool,
  isAddCreditModalOpen: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleChangeWorkerPasswordModal: PropTypes.func,
  handleToggleSubscribeWorkerToAutoApplyToRequestsModal: PropTypes.func,
  handleToggleSubscribeWorkerToMultiFieldsModal: PropTypes.func,
  handleToggleEditWorkerModal: PropTypes.func,
  handleToggleAddCreditModal: PropTypes.func,
  handleActivateWorker: PropTypes.func,
  // handleDeleteWorker: PropTypes.func,
  handleFeatureWorker: PropTypes.func,
  handleMakeWorkerElite: PropTypes.func,
  isActivateWorkerFetching: PropTypes.bool,
  isActivateWorkerError: PropTypes.bool,
  isSuspendWorkerFetching: PropTypes.bool,
  isSuspendWorkerError: PropTypes.bool,
  handleSuspendWorker: PropTypes.func,
  isChangeWorkerPasswordFetching: PropTypes.bool,
  isChangeWorkerPasswordError: PropTypes.bool,
  handleChangeWorkerPassword: PropTypes.func,
  isFeatureWorkerFetching: PropTypes.bool,
  isFeatureWorkerError: PropTypes.bool,
  featureWorkerErrorMessage: PropTypes.string,
  isMakeWorkerEliteFetching: PropTypes.bool,
  isMakeWorkerEliteError: PropTypes.bool,
  isDeleteWorkerFetching: PropTypes.bool,
  addCommentState: PropTypes.string,
  addCommentErrorMessage: PropTypes.string,
  addComment: PropTypes.func,
  addedCommentValue: PropTypes.string,
  handleAddCommentInputChange: PropTypes.func,
  resetAddCommentInput: PropTypes.func,
  workerStatistics: PropTypes.shape({}),
  isGetWorkerStatisticsSuccess: PropTypes.bool,
  isGetWorkerStatisticsFetching: PropTypes.bool,
  workerMotivationAndPunishments: PropTypes.shape({}),
  isGetWorkerMotivationAndPunishmentsFetching: PropTypes.bool,
  isGetWorkerMotivationAndPunishmentsSuccess: PropTypes.bool,
  finishWorkerAutoApplySubscription: PropTypes.func,
  finishWorkerMultiFieldsSubscription: PropTypes.func,
  isFinishWorkerAutoApplySubscriptionFetching: PropTypes.bool,
  isFinishWorkerMultiFieldsSubscriptionFetching: PropTypes.bool,
  handleToggleSuspendWorkerModal: PropTypes.func,
  isSuspendWorkerModalOpen: PropTypes.bool,
  suspendWorkerErrorMessage: PropTypes.string,
  vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
  admins: PropTypes.arrayOf(PropTypes.shape({})),
  mentionData: PropTypes.arrayOf(PropTypes.shape({})),
  handleWorkerAllowedToRecharge: PropTypes.func,
  isAllowWorkerToRechargeFetching: PropTypes.bool,
  isAllowWorkerToRechargeError: PropTypes.bool,
  isAllowWorkerToRechargeErrorMessage: PropTypes.string,
  isAllowWorkerToRechargeSuccess: PropTypes.bool,

  approveTheWorkerResidenceInfo: PropTypes.func,
  isApprovingTheWorkerResidenceInfoFetching: PropTypes.bool,
  isApprovingTheWorkerResidenceInfoError: PropTypes.bool,
  approvingTheWorkerResidenceInfoErrorMessage: PropTypes.string,
};

SingleWorker.defaultProps = {
  tabs: undefined,
  activeTab: 0,
  handleTabChange: () => {},
  user: undefined,
  workerDetails: undefined,
  isConfirmModalOpen: false,
  isChangeWorkerPasswordModalOpen: false,
  isSubscribeWorkerToAutoApplyModalOpen: false,
  isSubscribeWorkerToMultiFieldsModalOpen: false,
  isEditWorkerModalOpen: false,
  isAddCreditModalOpen: false,
  isGetWorkerDetailsFetching: false,
  openWorkerLocation: () => {},
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleToggleChangeWorkerPasswordModal: () => {},
  handleToggleSubscribeWorkerToAutoApplyToRequestsModal: () => {},
  handleToggleSubscribeWorkerToMultiFieldsModal: () => {},
  handleToggleEditWorkerModal: () => {},
  handleToggleAddCreditModal: () => {},
  handleToggleConfirmModal: () => {},
  handleActivateWorker: () => {},
  handleSuspendWorker: () => {},
  handleChangeWorkerPassword: () => {},
  // handleDeleteWorker: () => {},
  handleFeatureWorker: () => {},
  handleMakeWorkerElite: () => {},
  isActivateWorkerFetching: false,
  isActivateWorkerError: false,
  isSuspendWorkerFetching: false,
  isSuspendWorkerError: false,
  isChangeWorkerPasswordFetching: false,
  isChangeWorkerPasswordError: false,
  isFeatureWorkerFetching: false,
  isFeatureWorkerError: false,
  featureWorkerErrorMessage: undefined,
  isMakeWorkerEliteFetching: false,
  isMakeWorkerEliteError: false,
  isMakeWorkerEliteErrorMessage: undefined,
  isDeleteWorkerFetching: false,
  addCommentState: undefined,
  addCommentErrorMessage: undefined,
  addComment: () => {},
  addedCommentValue: '',
  handleAddCommentInputChange: () => {},
  resetAddCommentInput: () => {},
  workerStatistics: undefined,
  isGetWorkerStatisticsSuccess: false,
  isGetWorkerStatisticsFetching: false,
  workerMotivationAndPunishments: undefined,
  isGetWorkerMotivationAndPunishmentsFetching: false,
  isGetWorkerMotivationAndPunishmentsSuccess: false,
  finishWorkerAutoApplySubscription: () => {},
  finishWorkerMultiFieldsSubscription: () => {},
  isFinishWorkerAutoApplySubscriptionFetching: false,
  isFinishWorkerMultiFieldsSubscriptionFetching: false,
  handleToggleSuspendWorkerModal: () => {},
  isSuspendWorkerModalOpen: false,
  suspendWorkerErrorMessage: undefined,
  vendorsList: [],
  admins: [],
  mentionData: [],
  handleWorkerAllowedToRecharge: () => {},
  isAllowWorkerToRechargeFetching: false,
  isAllowWorkerToRechargeError: false,
  isAllowWorkerToRechargeErrorMessage: undefined,
  isAllowWorkerToRechargeSuccess: false,

  approveTheWorkerResidenceInfo: () => {},
  isApprovingTheWorkerResidenceInfoFetching: false,
  isApprovingTheWorkerResidenceInfoError: false,
  approvingTheWorkerResidenceInfoErrorMessage: undefined,
};

export default SingleWorker;
