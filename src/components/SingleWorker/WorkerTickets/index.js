import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { getAllTickets } from 'redux-modules/tickets/actions';
import TicketsList from './TicketsList';

class TicketsContainer extends Component {
  static propTypes = {
    workerId: PropTypes.string,
    user: PropTypes.shape({}),
    ticketsList: PropTypes.arrayOf(PropTypes.shape({})),
    ticketsState: PropTypes.shape({}),
    isLoadMoreTicketsAvailable: PropTypes.bool.isRequired,
    getAllTickets: PropTypes.func,
  };

  static defaultProps = {
    workerId: undefined,
    user: undefined,
    ticketsList: undefined,
    ticketsState: undefined,
    getAllTickets: () => {},
  };

  constructor(props) {
    super(props);

    const { workerId } = props;

    this.state = {
      filterQueries: {
        skip: 0,
        limit: 20,
        worker: workerId,
      },
    };
  }

  componentDidMount() {
    const { getAllTickets: getWorkerTicketsAction } = this.props;
    const { filterQueries } = this.state;

    getWorkerTicketsAction(filterQueries, true);
  }

  loadMoreTickets = () => {
    const { getAllTickets: getWorkerTicketsAction } = this.props;
    const {
      filterQueries,
      filterQueries: { skip, limit },
    } = this.state;

    this.setState(
      {
        filterQueries: { ...filterQueries, skip: skip + 20, limit },
      },
      () => {
        const { filterQueries: filterQueriesNextState } = this.state;

        getWorkerTicketsAction(filterQueriesNextState);
      },
    );
  };

  render() {
    const { ticketsList, ticketsState, isLoadMoreTicketsAvailable, user } = this.props;

    return (
      <TicketsList
        ticketsList={ticketsList}
        user={user}
        ticketsState={ticketsState}
        isLoadMoreTicketsAvailable={isLoadMoreTicketsAvailable}
        loadMoreTickets={this.loadMoreTickets}
      />
    );
  }
}
TicketsContainer.displayName = 'TicketsContainer';

const mapStateToProps = state => ({
  ticketsList: state.tickets.tickets,
  ticketsState: state.tickets.getAllTickets,
  isLoadMoreTicketsAvailable: state.tickets.hasMoreTickets,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getAllTickets }, dispatch);

export default compose(connect(mapStateToProps, mapDispatchToProps))(TicketsContainer);
