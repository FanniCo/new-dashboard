import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import styled from 'styled-components';
import { minHeight } from 'styled-system';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Rect, ShimmerEffect } from 'components/ShimmerEffect';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import TicketCard from 'components/TicketCard';

const { WHITE, GREY_LIGHT, BRANDING_GREEN } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

const TicketsListFlex = styled(Flex)`
  ${minHeight};
`;

class TicketsList extends Component {
  getLoadingTickets = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={1 / 2} height={250} m={2} />
      <Rect width={1 / 2} height={250} m={2} />
    </ShimmerEffect>
  );

  createTicketsList = ticketsList => {
    if (ticketsList.length) {
      return ticketsList.map((ticket, index) => {
        const { _id } = ticket;

        return (
          <Box key={_id} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <TicketCard m={2} index={index} ticket={ticket} />
          </Box>
        );
      });
    }

    return (
      <Card width={1} minHeight={600} alignItems="center" backgroundColor={GREY_LIGHT}>
        <EmptyState
          icon={faFile}
          iconColor={BRANDING_GREEN}
          iconSize="3x"
          textColor={COLORS_VALUES[WHITE]}
          textSize={BIG_TITLE}
          text="لا يوجد شكاوى لهذا الفني"
        />
      </Card>
    );
  };

  render() {
    const { ticketsList, loadMoreTickets, isLoadMoreTicketsAvailable, ticketsState } = this.props;
    const shipmentsLoadingList = this.getLoadingTickets(3);

    if (ticketsState.isFail.isError) {
      return (
        <Flex m={2} justifyContent="center" alignItems="center">
          <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
            <EmptyState
              icon={faFile}
              iconColor={BRANDING_GREEN}
              iconSize="3x"
              textColor={COLORS_VALUES[WHITE]}
              textSize={BIG_TITLE}
              text={ticketsState.isFail.message}
            />
          </Card>
        </Flex>
      );
    }
    return (
      <Box width={1} p={3} flexWrap="wrap" flexDirection="column">
        {ticketsList ? (
          <InfiniteScroll
            dataLength={ticketsList.length}
            next={loadMoreTickets}
            hasMore={isLoadMoreTicketsAvailable}
            height={600}
            loader={
              <Flex m={2} justifyContent="center" alignItems="center">
                <FontAwesomeIcon
                  icon={faSpinner}
                  size="lg"
                  spin
                  color={COLORS_VALUES[BRANDING_GREEN]}
                />
              </Flex>
            }
          >
            <TicketsListFlex width={1} flexWrap="wrap" flexDirection="row-reverse">
              {this.createTicketsList(ticketsList)}
            </TicketsListFlex>
          </InfiniteScroll>
        ) : (
          shipmentsLoadingList
        )}
      </Box>
    );
  }
}
TicketsList.displayName = 'TicketsList';

TicketsList.propTypes = {
  ticketsList: PropTypes.arrayOf(PropTypes.shape({})),
  isLoadMoreTicketsAvailable: PropTypes.bool.isRequired,
  loadMoreTickets: PropTypes.func,
  user: PropTypes.shape({}),
  ticketsState: PropTypes.shape({}),
};

TicketsList.defaultProps = {
  ticketsList: undefined,
  user: undefined,
  loadMoreTickets: () => {},
  ticketsState: undefined,
};

export default TicketsList;
