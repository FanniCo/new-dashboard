import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {Box, Flex} from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { FIELDS, CITIES } from 'utils/constants';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import { editWorker, toggleEditWorkerModal } from 'redux-modules/workers/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import {Checkbox, Label} from "@rebass/forms";

const fieldsKeys = Object.keys(FIELDS);
const citiesKeys = Object.keys(CITIES);
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));

const EditWorkerFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class EditWorkerModal extends Component {
  static propTypes = {
    user: PropTypes.shape({}),
    workerIndex: PropTypes.number,
    workerDetails: PropTypes.shape({}),
    isEditWorkerFetching: PropTypes.bool,
    isEditWorkerError: PropTypes.bool,
    editWorkerErrorMessage: PropTypes.string,
    editWorker: PropTypes.func,
    toggleEditWorkerModal: PropTypes.func,
    isEditWorkerModalOpen: PropTypes.bool,
    vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
  };

  static defaultProps = {
    user: undefined,
    workerIndex: undefined,
    workerDetails: undefined,
    isEditWorkerFetching: false,
    isEditWorkerError: false,
    editWorkerErrorMessage: '',
    editWorker: () => {},
    toggleEditWorkerModal: () => {},
    isEditWorkerModalOpen: false,
    vendorsList: [],
  };

  constructor(props) {
    super(props);

    const {
      workerDetails: { name, field, fields, username, workerReceivedShirt= false, additionalUsername = null, governmentId, city, sponsor, vendor },
      vendorsList,
    } = props;
    const workerFields =
      fields.length !== 0
        ? fields.map(fieldKey => fieldsSelectOptions.find(option => option.value === fieldKey))
        : fieldsSelectOptions.filter(option => option.value === field);
    const workerCity = citiesSelectOptions.filter(option => option.value === city)[0];
    const vendorsSelectOptions = vendorsList.map(vendor => ({
      value: vendor,
      label: `${vendor.name}`,
    }));
    let selectedVendor;
    if (vendor) {
      [selectedVendor] = vendorsSelectOptions.filter(option => option.value._id === vendor._id);
    }
    this.state = {
      name,
      fields: workerFields,
      username,
      workerReceivedShirt,
      additionalUsername,
      governmentId,
      city: workerCity,
      sponsor,
      vendorId: selectedVendor,
    };
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeFieldsSelect = fields => {
    this.setState({ fields });
  };

  handleChangeCitiesSelect = city => {
    this.setState({ city });
  };

  handleChangeVendorSelect = vendorId => {
    this.setState({ vendorId });
  };

  handleCheckBoxChange = type => {
    this.setState(prevState => ({
      [type]: !prevState[type],
    }));
  };

  onSubmitEditWorkerForm = e => {
    const {
      workerIndex,
      workerDetails: { _id },
      editWorker: editWorkerAction,
    } = this.props;
    const { name, username, additionalUsername, workerReceivedShirt, fields, governmentId, city, sponsor, vendorId } = this.state;
    const workerFields = fields.map(field => field.value);
    const workerCity = city.value;

    e.preventDefault();

    const editParams = {
      name,
      username: convertArabicNumbersToEnglish(username),
      additionalUsername: additionalUsername? convertArabicNumbersToEnglish(additionalUsername): null,
      workerReceivedShirt: workerReceivedShirt,
      fields: workerFields,
      governmentId: convertArabicNumbersToEnglish(governmentId),
      city: workerCity,
      sponsor,
      vendor: vendorId ? vendorId.value._id : null,
    };

    editWorkerAction(_id, workerIndex, editParams);
  };

  render() {
    const {
      isEditWorkerFetching,
      isEditWorkerModalOpen,
      toggleEditWorkerModal: toggleEditWorkerModalAction,
      isEditWorkerError,
      editWorkerErrorMessage,
      vendorsList,
    } = this.props;
    const { name, username, additionalUsername, workerReceivedShirt, fields, governmentId, city, sponsor, vendorId } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const editWorkerModalHeader = 'تعديل الفني';
    const vendorsSelectOptions = vendorsList.map(vendor => ({
      value: vendor,
      label: `${vendor.name}`,
    }));

    return (
      <Modal
        toggleModal={toggleEditWorkerModalAction}
        header={editWorkerModalHeader}
        isOpened={isEditWorkerModalOpen}
      >
        <EditWorkerFormContainer onSubmit={this.onSubmitEditWorkerForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الفني
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم الفني"
                  ref={inputField => {
                    this.nameField = inputField;
                  }}
                  width={1}
                  value={name}
                  onChange={value => this.handleInputFieldChange('name', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الجوال
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الجوال"
                  ref={inputField => {
                    this.userNameField = inputField;
                  }}
                  width={1}
                  value={username}
                  onChange={value => this.handleInputFieldChange('username', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الجوال اضافي
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الجوال اضافي"
                  ref={inputField => {
                    this.userNameField = inputField;
                  }}
                  width={1}
                  value={additionalUsername}
                  onChange={value => this.handleInputFieldChange('additionalUsername', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الهوية
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الهوية"
                  ref={inputField => {
                    this.governmentIdField = inputField;
                  }}
                  width={1}
                  value={governmentId}
                  onChange={value => this.handleInputFieldChange('governmentId', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الكفيل
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم الكفيل"
                  ref={inputField => {
                    this.sponsorField = inputField;
                  }}
                  width={1}
                  value={sponsor}
                  onChange={value => this.handleInputFieldChange('sponsor', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        الفني استلم التشيرت
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={workerReceivedShirt}
                        onChange={() => this.handleCheckBoxChange('workerReceivedShirt')}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الشريك
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="اسم الشريك"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti={false}
                  isRtl
                  backspaceRemovesValue={false}
                  value={vendorId}
                  onChange={this.handleChangeVendorSelect}
                  options={vendorsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti
                  isRtl
                  backspaceRemovesValue={false}
                  value={fields}
                  onChange={this.handleChangeFieldsSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدينة
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  isMulti={false}
                  backspaceRemovesValue={false}
                  value={city}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitEditWorkerForm}
                disabled={isEditWorkerFetching || governmentId === '' || sponsor === ''}
                isLoading={isEditWorkerFetching}
              >
                تعديل الفني
              </Button>
              {isEditWorkerError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {editWorkerErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </EditWorkerFormContainer>
      </Modal>
    );
  }
}
EditWorkerModal.displayName = 'EditWorkerModal';

const mapStateToProps = state => ({
  user: state.user.user,
  isEditWorkerFetching: state.workers.editWorker.isFetching,
  isEditWorkerError: state.workers.editWorker.isFail.isError,
  editWorkerErrorMessage: state.workers.editWorker.isFail.message,
  isEditWorkerModalOpen: state.workers.editWorkerModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      editWorker,
      toggleEditWorkerModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(EditWorkerModal);
