import { compose } from 'redux';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps';
import { MarkerClusterer } from 'react-google-maps/lib/components/addons/MarkerClusterer';
import React from 'react';
import { DARK_MODE_MAP_STYLE } from 'utils/constants';
import PropTypes from 'prop-types';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import Card from 'components/Card';
import ROUTES from 'routes';

const GoogleMapWrapper = compose(
  withScriptjs,
  withGoogleMap,
)(props => (
  <GoogleMap
    defaultZoom={4}
    defaultCenter={props.defaultCenter}
    defaultOptions={{ styles: DARK_MODE_MAP_STYLE, disableDefaultUI: true }}
  >
    <MarkerClusterer averageCenter enableRetinaIcons gridSize={60}>
      {props.locations.map(
        record =>
          record.location && (
            <Marker
              key={record.location._id}
              position={{
                lat: Number(record.location.latitude),
                lng: Number(record.location.longitude),
              }}
              clickable
              onClick={() => props.navigateToWorkerPage(record)}
              labelAnchor={{ lat: 0, lng: 0 }}
              labelStyle={{ backgroundColor: 'yellow', fontSize: '32px', padding: '16px' }}
              labelVisible={false}
            />
          ),
      )}
    </MarkerClusterer>
  </GoogleMap>
));

const RequestsHeatMap = props => {
  const { locations } = props;
  const { GREY_LIGHT } = COLORS;
  const { SINGLE_REQUEST } = ROUTES;
  const defaultMapCenter = locations.length
    ? {
      lat: Number(locations[0].location.latitude),
      lng: Number(locations[0].location.longitude),
      }
    : {
      lat: Number('21.52743611032966'),
      lng: Number('39.192946404218674'),
    };

  const navigateToRequestPage = record => {
    window.open(`${SINGLE_REQUEST}/${record.requestPrettyId}`, '_blank');
  };

  return (
    <Card minHeight={500} justifyContent="center" bgColor={COLORS_VALUES[GREY_LIGHT]}>
      <GoogleMapWrapper
        locations={locations || []}
        defaultCenter={defaultMapCenter}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '500px', width: '100%' }} />}
        mapElement={<div style={{ height: '100%' }} />}
        navigateToWorkerPage={navigateToRequestPage}
      />
    </Card>
  );
};

RequestsHeatMap.displayName = 'RequestsHeatMap';

RequestsHeatMap.propTypes = {
  locations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

export default RequestsHeatMap;
