import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { USER_TARGETS } from 'utils/constants';
import { map, omit } from 'lodash';

const UsersCountContainer = props => {
  const { UsersForEachTypeCount } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const createUsersForEachTypeCountCards = map(omit(UsersForEachTypeCount, ['suspendedWorkersCount', 'activeWorkersCount']), (userType, key) => (
    <Box key={key + userType} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
        className='StatictesCardContent'
      >
        <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
            
          >
            عدد جميع {USER_TARGETS[key].ar}
          </Text>
        </CardTitle>
        <FlexContainer flexDirection="column" width={1} p={3} className='StatictesCardNumber'>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {userType.count}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              مستخدم
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  ));
  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {createUsersForEachTypeCountCards}
    </Flex>
  );
};
UsersCountContainer.displayName = 'UsersCountContainer';

UsersCountContainer.propTypes = {
  UsersForEachTypeCount: PropTypes.shape({}),
};

UsersCountContainer.defaultProps = {
  UsersForEachTypeCount: undefined,
};

export default UsersCountContainer;
