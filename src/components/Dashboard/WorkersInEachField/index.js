import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { FIELDS } from 'utils/constants';
import { map } from 'lodash';

const WorkersInEachField = props => {
  const { WorkersForEachField } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const createWorkersForEachFieldCards = map(FIELDS, (field, key) => (
    <Box key={field.en} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
      >
        <CardTitle px={4} py={2} width={1}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {field.ar}
          </Text>
        </CardTitle>
        <FlexContainer flexDirection="column" width={1} p={3}>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              عدد الفنين الفنين
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {WorkersForEachField[key] ? WorkersForEachField[key].count : 0}
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  ));
  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {createWorkersForEachFieldCards}
    </Flex>
  );
};
WorkersInEachField.displayName = 'WorkersInEachField';

WorkersInEachField.propTypes = {
  WorkersForEachField: PropTypes.shape({}),
};

WorkersInEachField.defaultProps = {
  WorkersForEachField: undefined,
};

export default WorkersInEachField;
