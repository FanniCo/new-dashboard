import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { PieChart,  Pie, Cell, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


const RequestsStatistics = props => {
  const {
    requestsCounts: {
      done,
      reviewed,
      active,
      pending,
      contactYou,
      postponed,
      postponedDone,
      PostponedCancelled: postponedCanceled,
      timedOut,
      customer,
      canceled: {
        noAppliedWorker,
        requestedByMistake,
        highPrice,
        technicianNotSuitable,
        technicianWasANoShow,
        technicianRequestedCancellation,
        needNewField,
        otherAfterWorker,
        otherBeforeWorker,
        cancelledByAdmin,
        theTechnicalPriceHigherThanTheApplication,
        feelThePriceOfTheApplicationIsHigh,
        TheTechnicianRequestedToDelayTheAppointment,
        lateForTheAppointment,
        TheTechnicianWantsToWorkOutsideOfTheApp,
      },
    },
    statsDaily
  } = props;

  const { GREY_LIGHT, DISABLED, BRANDING_GREEN, WARNING, CYAN, PRIMARY } = COLORS;
  const primaryColor = COLORS_VALUES[PRIMARY];
  const cyanColor = COLORS_VALUES[CYAN];
  const { SUPER_TITLE, BODY, SUBHEADING } = FONT_TYPES;
  const { NORMAL, BOLD } = FONT_WEIGHTS;

  const totalPostponedPendingRequests = Number(postponed) + Number(pending) + Number(contactYou);
  const totalCanceledBeforeFanniShowUp =
    Number(noAppliedWorker) +
    Number(requestedByMistake) +
    Number(otherBeforeWorker) +
    Number(timedOut);

  const totalCanceledAfterFanniShowUp =
    Number(highPrice) +
    Number(technicianNotSuitable) +
    Number(technicianWasANoShow) +
    Number(technicianRequestedCancellation) +
    Number(needNewField) +
    Number(otherAfterWorker) +
    Number(cancelledByAdmin) +
    Number(theTechnicalPriceHigherThanTheApplication)+
    Number(feelThePriceOfTheApplicationIsHigh) +
    Number(TheTechnicianRequestedToDelayTheAppointment) +
    Number(lateForTheAppointment) +
    Number(TheTechnicianWantsToWorkOutsideOfTheApp);

  const totalCanceled =
    Number(totalCanceledBeforeFanniShowUp) + Number(totalCanceledAfterFanniShowUp);
  const totalCompletedAndReviewed = Number(done) + Number(reviewed);
  const totalCounts =
    Number(totalCanceled) +
    Number(totalCompletedAndReviewed) +
    Number(totalPostponedPendingRequests) +
    Number(active);

  const totalAfterChooseWorkerCounts =
    Number(totalCanceledAfterFanniShowUp) +
    Number(totalCompletedAndReviewed) +
    Number(totalPostponedPendingRequests) +
    Number(active);

    const data = [];
    statsDaily.map((entry, index) => (
      data.push(
        {
          name: entry.day,
        'ملغي بدون فني': entry.canceledBeforeWorker,
        'ملغي مرتبط': entry.canceledAfterWorker,
        'مكتمل': entry.completed,
        }
      )
    ));


    const labels = ['ملغي مرتبط' , 'ملغي بدون فني'];
  
    const total = totalCanceledBeforeFanniShowUp + totalCanceledAfterFanniShowUp + totalCompletedAndReviewed;
    const value1 = totalCanceledAfterFanniShowUp;
    const value2 =  totalCanceledBeforeFanniShowUp;
    const value3 = totalCompletedAndReviewed;


    const data01 = [];
   
    if(value1 > 0) {
      data01.push({ name: "ملغي مرتبط", value: value1 });
    }
    if( value2 > 0) {
      data01.push({ name: "ملغي بدون فني", value: value2 });
    }
    if( value3 > 0) {
      data01.push({ name: "مكتمل", value: value3 });
    }

    const data_COLORS = ["#0088FE", "#00C49F", "#FFBB28"];
   



  const createCard = (count, title, color = COLORS_VALUES[GREY_LIGHT], isPercentage = false, isCustomer = false) => (
    <Box key={`${count}-${title}`} width={[1, 1 / 2, 1 / 3, 1 / 4]} className='StatictesCard RequestStatictesCard'>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={color}
        className='StatictesCardContent'
      >
        <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
          <Text
            textAlign="center"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {title}
          </Text>
        </CardTitle>
        <FlexContainer
          flexDirection="row-reverse"
          width={1}
          p={3}
          alignItems="center"
          justifyContent="center"
          className='StatictesCardNumber'
        >
          <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {count}
            </Text>
            { isCustomer 
             ? <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>عميل</Text>
             : <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>{isPercentage ? '%' : 'طلب'}</Text>
            }
          </Flex>
        </FlexContainer>
        
      </Card>
    </Box>
  );

  return (
    <>
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse" className='DataBoxsContainer'>
      {createCard(pending, 'طلبات في انتظار اعتماد الطلب')}
      {createCard(contactYou, 'طلبات سوف يتم الاتصال بك')}
      {createCard(active, 'طلبات نشطة')}
      {createCard(postponed, 'طلبات مؤجلة')}

      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
      />
      {createCard(postponedDone, 'طلبات مؤجلة تحولت إلي ملغية')}
      {createCard(postponedCanceled, 'طلبات مؤجلة تحولت إلي مكتملة')}

      {/*  ---------------------------------------------- */}

      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          طلبات ملغية قبل اختيار الفني
        </Text>
      </Flex>

      {createCard(noAppliedWorker, 'طلبات ملغيه بسبب - لا يوجد فني')}
      {createCard(requestedByMistake, 'طلبات ملغيه بسبب - طلبت بالخطأ')}
      {createCard(otherBeforeWorker, 'طلبات ملغيه بسبب اخر')}
      {createCard(timedOut, 'طلبات ملغيه بسبب نفاذ الوقت')}
      {createCard(
        totalCanceledBeforeFanniShowUp,
        'مجموع الطلبات الملغيه قبل اختيار الفني',
        primaryColor,
      )}
      {/*  ---------------------------------------------- */}

      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          طلبات ملغية بعد اختيار الفني
        </Text>
      </Flex>
      {createCard(highPrice, 'طلبات ملغيه بسبب - السعر مرتفع')}
      {createCard(technicianNotSuitable, 'طلبات ملغيه بسبب - الفني غير مناسب')}
      {createCard(technicianWasANoShow, 'طلبات ملغيه بسبب - تأخر الفني عن الموعد')}
      {createCard(technicianRequestedCancellation, 'طلبات ملغيه بسبب - الفني طلب الالغاء')}
      {createCard(needNewField, 'طلبات ملغيه بسبب - أحتاج تخصص أخر')}
      {createCard(otherAfterWorker, 'طلبات ملغيه بسبب - أرسل السبب')}
      {createCard(cancelledByAdmin, 'طلبات ملغيه بسبب - ملغي من قبل المشرف')}

      {createCard(theTechnicalPriceHigherThanTheApplication, 'طلبات ملغيه بسبب - سعر الفني اعلي من التطبيق')}
      {createCard(feelThePriceOfTheApplicationIsHigh, 'طلبات ملغيه بسبب - سعر التطبيق  مرتفع')}
      {createCard(TheTechnicianRequestedToDelayTheAppointment, 'طلبات ملغيه بسبب - الفني  طلب تأخير الموعد')}
      {createCard(lateForTheAppointment, 'طلبات ملغيه بسبب - انا بتأخر على الفني')}
      {createCard(TheTechnicianWantsToWorkOutsideOfTheApp, 'طلبات ملغيه بسبب - الفني  يريد العمل  خارج التطبيق')}

      {createCard(
        totalCanceledAfterFanniShowUp,
        'مجموع الطلبات الملغيه بعد اختيار الفني',
        primaryColor,
      )}
      {/*  ---------------------------------------------- */}

      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          طلبات مكتملة
        </Text>
      </Flex>

      {createCard(done, 'طلبات مكتملة')}
      {createCard(reviewed, 'طلبات مكتملة وتم تقييمها')}
      {createCard(
        totalCompletedAndReviewed,
        'مجموع الطلبات المكتملة والتي تم تقييمها',
        primaryColor,
      )}

      {/*  ---------------------------------------------- */}

      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          إحصائيات الطلبات
        </Text>
      </Flex>

      {createCard(totalCounts, 'إجمالي عدد الطلبات', cyanColor)}
      {createCard(
        ((totalCompletedAndReviewed / totalCounts) * 100).toFixed(2),
        'نسبة الطلبات المكتملة من إجمالي الطلبات',
        cyanColor,
        true,
      )}
      {createCard(
        ((totalCanceled / totalCounts) * 100).toFixed(2),
        'نسبة الطلبات الملغية من إجمالي الطلبات',
        cyanColor,
        true,
      )}

      {/*  ---------------------------------------------- */}

      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          إحصائيات الطلبات بعد اختيار الفني
        </Text>
      </Flex>

      {createCard(totalAfterChooseWorkerCounts, 'إجمالي عدد الطلبات', cyanColor)}
      {createCard(
        ((totalCompletedAndReviewed / totalAfterChooseWorkerCounts) * 100).toFixed(2),
        'نسبة الطلبات المكتملة من إجمالي الطلبات',
        cyanColor,
        true,
      )}
      {createCard(
        ((totalCanceledAfterFanniShowUp / totalAfterChooseWorkerCounts) * 100).toFixed(2),
        'نسبة الطلبات الملغية من إجمالي الطلبات',
        cyanColor,
        true,
      )}

<Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          إحصائيات العملاء
        </Text>
      </Flex>

      {createCard(customer, 'اجمالي عدد العملاء للطلبات', cyanColor,false,true)}

      {/*  ---------------------------------------------- */}
      
    </Flex>

    <Flex className='ChartContainer chart1'>
        <ResponsiveContainer width="100%" height="100%" className="PieChartContainer BarChart">
        <BarChart
          barGap={0}
          width={500}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="ملغي بدون فني" fill="#edad6c" background={{ fill: '#171721' }} />
          <Bar dataKey="ملغي مرتبط" fill="#f1807e" background={{ fill: '#171721' }} />
          <Bar dataKey="مكتمل" fill="#82ca9d" background={{ fill: '#171721' }} />
        </BarChart>
      </ResponsiveContainer>
        </Flex>

        <Flex className='ChartContainer chart2'>
        <ResponsiveContainer width={1200} className="PieChartContainer">
        <PieChart>
          <Pie
            data={data01}
            dataKey="value"
            cx="50%"
            cy="50%"
            outerRadius={300}
            label={({
              cx,
              cy,
              midAngle,
              innerRadius,
              outerRadius,
              value,
              index
            }) => {
              const RADIAN = Math.PI / 180;
              // eslint-disable-next-line
              const radius = 25 + innerRadius + (outerRadius - innerRadius);
              // eslint-disable-next-line
              const x = cx + radius * Math.cos(-midAngle * RADIAN);
              // eslint-disable-next-line
              const y = cy + radius * Math.sin(-midAngle * RADIAN);
    
              return (
                <text
                  x={x}
                  y={y}
                  fill={data_COLORS[index % data_COLORS.length]}
                  textAnchor={x > cx ? "start" : "end"}
                  dominantBaseline="central"
                >
                  {data01[index].name} ({((value / total) * 100).toFixed(2)}%)
                </text>
              );
            }}
          >
            {data01.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                fill={data_COLORS[index % data_COLORS.length]}
              />
            ))}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
        </Flex>
</>
  );
};
RequestsStatistics.displayName = 'RequestsStatistics';

RequestsStatistics.propTypes = {
  requestsCounts: PropTypes.shape({}),
  statsDaily: PropTypes.shape({}),
};

RequestsStatistics.defaultProps = {
  requestsCounts: { canceled: {} },
  statsDaily : {},
};

export default RequestsStatistics;
