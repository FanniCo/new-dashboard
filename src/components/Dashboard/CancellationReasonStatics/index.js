import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { VictoryLabel, VictoryPie, VictoryPortal } from 'victory';
import _ from 'lodash';
import {
  CANCELLATION_REASONS,
  CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES,
} from 'utils/constants';

const CancellationReasonsStaticsContainer = props => {
  const { cancellationReasonsStatics } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const createCancellationReasonsStaticsCards = (cancellationReasonsName, count) => (
    <Box key={cancellationReasonsName} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
      >
        <CardTitle px={4} py={2} width={1}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {cancellationReasonsName}
          </Text>
        </CardTitle>
        <FlexContainer flexDirection="column" width={1} p={3}>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {count}
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  );

  const total = _.map(cancellationReasonsStatics, count => count).reduce(
    (partialSum, a) => partialSum + a,
    0,
  );
  const pieChartData = _.map(cancellationReasonsStatics, (count, code) => ({
    x: 1,
    y: code,
    label: `${((count / total) * 100).toFixed(2)}% \n ${CANCELLATION_REASONS[code].ar
      .split(' ')
      .join('\n')} `,
  }));

  return (
    <>
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {_.map(cancellationReasonsStatics, (count, code) =>
          createCancellationReasonsStaticsCards(CANCELLATION_REASONS[code].ar, count),
        )}
      </Flex>
      <Flex
        width={1}
        flexWrap="c"
        flexDirection="row-reverse"
        alignItems="center"
        style={{ paddingTop: '5%' }}
      >
        <Box width={[1, 1, 1 / 2, 1 / 2]}>
          <VictoryPie
            style={{ labels: { fontSize: 10, fill: '#ffffff' } }}
            innerRadius={1}
            padAngle={15}
            colorScale={['tomato', 'orange', 'gold', 'cyan', 'rgb(0 255 63)', 'navy']}
            data={pieChartData}
            labelComponent={
              <VictoryPortal>
                <VictoryLabel />
              </VictoryPortal>
            }
          />
        </Box>
      </Flex>
    </>
  );
};
CancellationReasonsStaticsContainer.displayName = 'CancellationReasonsStaticsContainer';

CancellationReasonsStaticsContainer.propTypes = {
  cancellationReasonsStatics: PropTypes.shape({}),
};

CancellationReasonsStaticsContainer.defaultProps = {
  cancellationReasonsStatics: undefined,
};

export default CancellationReasonsStaticsContainer;
