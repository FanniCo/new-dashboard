import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { VictoryLabel, VictoryPie, VictoryPortal } from 'victory';
import {  PieChart,  Pie,  Legend,  Cell,  ResponsiveContainer,  Label } from "recharts";



const PaymentMethodsStaticsContainer = props => {
  const { paymentMethodsStatics } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const createPaymentMethodsStaticsCards = (paymentMethodsName, count) => (
    <Box key={paymentMethodsName} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
        className="StatictesCardContent"
      >
        <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {paymentMethodsName}
          </Text>
        </CardTitle>
        <FlexContainer flexDirection="column" width={1} p={3} className='StatictesCardNumber'>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {count}
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  );

  const labels = ['كاش', 'ابل باي', 'STC Pay', 'كريدت كارد', 'مدي', 'تمارا', 'محفظة'];
  const total =
    paymentMethodsStatics.cash +
    paymentMethodsStatics.applePay +
    paymentMethodsStatics.stcPay +
    paymentMethodsStatics.creditCard +
    paymentMethodsStatics.mada +
    paymentMethodsStatics.tamara +
    paymentMethodsStatics.wallet;


    const data01 = [];

    if(paymentMethodsStatics.cash > 0) {
      data01.push({ name: "كاش", value: paymentMethodsStatics.cash });
    }
    if(paymentMethodsStatics.creditCard > 0) {
      data01.push({ name: "كريدت كارد", value: paymentMethodsStatics.creditCard });
    }
    if(paymentMethodsStatics.applePay > 0) {
      data01.push({ name: "ابل باي", value: paymentMethodsStatics.applePay });
    }
    if(paymentMethodsStatics.stcPay > 0) {
      data01.push({ name: "STC Pay", value: paymentMethodsStatics.stcPay });
    }
    if(paymentMethodsStatics.mada > 0) {
      data01.push({ name: "مدي", value: paymentMethodsStatics.mada });
    }
    if(paymentMethodsStatics.tamara > 0) {
      data01.push({ name: "تمارا", value: paymentMethodsStatics.tamara });
    }
    if(paymentMethodsStatics.wallet > 0) {
      data01.push({ name: "محفظة", value: paymentMethodsStatics.wallet });
    }

    const data_COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042" , "#2da22d", "#a22d80", "#babf17"];


  return (
    <>
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse" className="DataBoxsContainer">
        {createPaymentMethodsStaticsCards('كاش', paymentMethodsStatics.cash || 0)}
        {createPaymentMethodsStaticsCards('ابل باي', paymentMethodsStatics.applePay || 0)}
        {createPaymentMethodsStaticsCards('STC Pay', paymentMethodsStatics.stcPay || 0)}
        {createPaymentMethodsStaticsCards('كريدت كارد', paymentMethodsStatics.creditCard || 0)}
        {createPaymentMethodsStaticsCards('مدي', paymentMethodsStatics.mada || 0)}
        {createPaymentMethodsStaticsCards('تقسيط بواسطة تمارا', paymentMethodsStatics.tamara || 0)}
        {createPaymentMethodsStaticsCards('الدفع بالمحفظة', paymentMethodsStatics.wallet || 0)}
      </Flex>
      <Flex
        width={1}
        flexWrap="c"
        flexDirection="row-reverse"
        alignItems="center"
        style={{ paddingTop: '5%' }}
        className="ChartContainer"
      >
        <Box width={[1 / 1 / 1]}>
        <ResponsiveContainer width={1200} className="PieChartContainer">
        <PieChart>
          <Pie
            data={data01}
            dataKey="value"
            cx="50%"
            cy="50%"
            outerRadius={300}
            label={({
              cx,
              cy,
              midAngle,
              innerRadius,
              outerRadius,
              value,
              index
            }) => {
              const RADIAN = Math.PI / 180;
              // eslint-disable-next-line
              const radius = 25 + innerRadius + (outerRadius - innerRadius);
              // eslint-disable-next-line
              const x = cx + radius * Math.cos(-midAngle * RADIAN);
              // eslint-disable-next-line
              const y = cy + radius * Math.sin(-midAngle * RADIAN);

              return (
                <text
                  x={x}
                  y={y}
                  fill={data_COLORS[index % data_COLORS.length]}
                  textAnchor={x > cx ? "start" : "end"}
                  dominantBaseline="central"
                >
                  {data01[index].name} ({((value / total) * 100).toFixed(2)}%)
                </text>
              );
            }}
          >
            {data01.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                fill={data_COLORS[index % data_COLORS.length]}
              />
            ))}
          </Pie>
        </PieChart>
      </ResponsiveContainer>


        </Box>


      </Flex>
    </>
  );
};
PaymentMethodsStaticsContainer.displayName = 'PaymentMethodsStaticsContainer';

PaymentMethodsStaticsContainer.propTypes = {
  paymentMethodsStatics: PropTypes.shape({}),
};

PaymentMethodsStaticsContainer.defaultProps = {
  paymentMethodsStatics: undefined,
};

export default PaymentMethodsStaticsContainer;
