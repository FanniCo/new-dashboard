import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { CITIES } from 'utils/constants';

const FinancialByCities = props => {
  const { financialByCities } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const createFinancialByCityCards = financialByCities.map(city => (
    <Box key={city._id} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
      >
        <CardTitle px={4} py={2} width={1}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {CITIES[city._id].ar}
          </Text>
        </CardTitle>
        <FlexContainer flexDirection="column" width={1} p={3}>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              اجرة الفنين
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {city.workersWages}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              قطع الغيار
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {city.partsCosts}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              الضرايب
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {city.vatValue}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              الاجمالى قبل الخصم
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {city.total}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              الخصم
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {city.discount}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              محفظة العملاء
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {city.moneyTakenFromCustomerCredits}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              العمولة
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {Math.abs(city.commissions)}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              مبلغ توصيل فطع الغيار
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {Math.abs(city.deliveryCostOfParts)}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  ));

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {createFinancialByCityCards}
    </Flex>
  );
};
FinancialByCities.displayName = 'FinancialByCities';

FinancialByCities.propTypes = {
  financialByCities: PropTypes.arrayOf(PropTypes.shape({})),
};

FinancialByCities.defaultProps = {
  financialByCities: undefined,
};

export default FinancialByCities;
