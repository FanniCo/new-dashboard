import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { FIELDS } from 'utils/constants';

const FinancialByFields = props => {
  const { financialByFields } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  const createFinancialByFieldCards = financialByFields.map(field => (
    <Box key={field._id} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        flexDirection="column"
        alignItems="center"
        mx={2}
        mb={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
      >
        <CardTitle px={4} py={2} width={1}>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {FIELDS[field._id].ar}
          </Text>
        </CardTitle>
        <FlexContainer flexDirection="column" width={1} p={3}>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              اجرة الفنين
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {field.workersWages}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              قطع الغيار
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {field.partsCosts}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              الضرايب
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {field.vatValue}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              الاجمالى قبل الخصم
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {field.total}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              الخصم
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {field.discount}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              محفظة العملاء
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {field.moneyTakenFromCustomerCredits}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              العمولة
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {Math.abs(field.commissions)}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
          <Flex alignItems="baseline" flexDirection="row-reverse">
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              مبلغ توصيل قطع الغيار
            </Text>
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {Math.abs(field.deliveryCostOfParts)}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
              ريال
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  ));
  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {createFinancialByFieldCards}
    </Flex>
  );
};
FinancialByFields.displayName = 'FinancialByFields';

FinancialByFields.propTypes = {
  financialByFields: PropTypes.arrayOf(PropTypes.shape({})),
};

FinancialByFields.defaultProps = {
  financialByFields: undefined,
};

export default FinancialByFields;
