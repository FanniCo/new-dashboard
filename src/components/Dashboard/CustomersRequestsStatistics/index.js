import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Flex, Box } from '@rebass/grid';
import { FIELDS } from 'utils/constants';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';

const CustomersRequestsStatistics = props => {
  const { customerRequestsStatistics } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN, WARNING } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL, BOLD } = FONT_WEIGHTS;

  const fieldsKeys = Object.keys(FIELDS);

  const createCard = (count, percentage, title) => (
    <Box key={`${count}-${title}`} width={[1, 1 / 2, 1 / 3, 1 / 5]}>
      <Card
        minHeight="100px"
        flexDirection="column"
        alignItems="center"
        m={2}
        bgColor={COLORS_VALUES[GREY_LIGHT]}
        className="StatictesCardContent twoCol"
      >
        <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
          <Text
            textAlign="center"
            width={1}
            color={COLORS_VALUES[BRANDING_GREEN]}
            type={SUBHEADING}
            fontWeight={NORMAL}
            
          >
            {title}
          </Text>
        </CardTitle>
        <FlexContainer
          flexDirection="row-reverse"
          width={1}
          p={3}
          alignItems="center"
          justifyContent="center"
          className='StatictesCardNumber'
        >
          <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
            <Text
              mx={1}
              color={COLORS_VALUES[BRANDING_GREEN]}
              type={SUPER_TITLE}
              fontWeight={NORMAL}
            >
              {count}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
            {` (${Math.round((percentage * 100 || 0) * 100) / 100}%)`}
            </Text>
          </Flex>
        </FlexContainer>
        
      </Card>
    </Box>
  );

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {fieldsKeys.map(key => {
        const fieldCustomersStatistics = _.find(
          customerRequestsStatistics.statsPerField,
          fieldStats => fieldStats._id === key,
        );

        const totalCustomers = customerRequestsStatistics.totalNumberOfCustomers;
        const customerMadeRequestsInField = _.get(
          fieldCustomersStatistics,
          'customerMadeRequestsInField',
          0,
        );
        const doneFromFirst = _.get(fieldCustomersStatistics, 'doneFromFirst', 0);
        const cancelledAfterChooseWorker = _.get(
          fieldCustomersStatistics,
          'cancelledAfterChooseWorker',
          0,
        );
        const cancelledBeforeChooseWorker = _.get(
          fieldCustomersStatistics,
          'cancelledBeforeChooseWorker',
          0,
        );
        const doneAfterCancelledRequest = _.get(
          fieldCustomersStatistics,
          'doneAfterCancelledRequest',
          0,
        );

        return (
          <Flex
            key={key}
            width={1}
            py={2}
            flexDirection="row-reverse"
            flexWrap="wrap"
            justifyContent="space-between"
            alignItems="center"
            className='StatictesBigTitle'
          >
            <Text m={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
              {`إحصائيــات خدمــة (${FIELDS[key].ar})`}
            </Text>

            <Flex
              width={1}
              py={2}
              flexDirection="row-reverse"
              flexWrap="wrap"
              justifyContent="space-between"
              alignItems="center"
            >
              {createCard(
                customerMadeRequestsInField,
                customerMadeRequestsInField / totalCustomers,
                'إجمالي العملاء',
              )}
              {createCard(
                doneFromFirst,
                doneFromFirst / customerMadeRequestsInField,
                'مكتمل من أول مرة',
              )}
              {createCard(
                cancelledAfterChooseWorker,
                cancelledAfterChooseWorker / customerMadeRequestsInField,
                'ملغي بعد ما أختار الفني',
              )}
              {createCard(
                cancelledBeforeChooseWorker,
                cancelledBeforeChooseWorker / customerMadeRequestsInField,
                'ملغي قبل ما يختار الفني',
              )}
              {createCard(
                doneAfterCancelledRequest,
                doneAfterCancelledRequest / customerMadeRequestsInField,
                'مكتمل بعد طلب ملغي',
              )}
            </Flex>
          </Flex>
        );
      })}
    </Flex>
  );
};
CustomersRequestsStatistics.displayName = 'CustomersRequestsStatistics';

CustomersRequestsStatistics.propTypes = {
  customerRequestsStatistics: PropTypes.shape({}),
};

CustomersRequestsStatistics.defaultProps = {
  customerRequestsStatistics: undefined,
};

export default CustomersRequestsStatistics;
