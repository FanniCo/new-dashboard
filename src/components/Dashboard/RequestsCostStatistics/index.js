import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';

const RequestsCostStatistics = props => {
  const {
    requestsCostStatistics: {
      totalWorkersWageSum,
      totalPartsCostSum,
      totalCostSum,
      totalVatSum,
      totalDeliveryCostOfPartsSum,
      donations,
    },
  } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
          className="StatictesCardContent"
        >
          <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              أجرة الفني
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
            className='StatictesCardNumber'
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {totalWorkersWageSum}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
          className="StatictesCardContent"
        >
          <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              تكلفة القطع
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
            className='StatictesCardNumber'
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {totalPartsCostSum}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
          className="StatictesCardContent"
        >
          <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              إجمالي الضرائب
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
            className='StatictesCardNumber'
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {Math.abs(totalVatSum)}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
          className="StatictesCardContent"
        >
          <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجمالي عمولة توصيل قطع الغيار
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
            className='StatictesCardNumber'
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {totalDeliveryCostOfPartsSum}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
          className="StatictesCardContent"
        >
          <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجمالي التبرعات لجمعية الخيرية
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
            className='StatictesCardNumber'
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {donations || 0}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
          className="StatictesCardContent"
        >
          <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجمالي التكلفة
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
            className='StatictesCardNumber'
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {totalCostSum}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
    </Flex>
  );
};
RequestsCostStatistics.displayName = 'RequestsCostStatistics';

RequestsCostStatistics.propTypes = {
  requestsCostStatistics: PropTypes.shape({}),
};

RequestsCostStatistics.defaultProps = {
  requestsCostStatistics: {},
};

export default RequestsCostStatistics;
