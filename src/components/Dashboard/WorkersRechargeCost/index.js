import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';

const WorkersRechargeCost = props => {
  const {
    costStatistics: { worth },
  } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      <Box width={[1, 1 / 2, 1 / 3, 1 / 3]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
        >
          <CardTitle px={4} py={2} width={1}>
            <Text
              textAlign="center"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجمالى شحن الفنين في الشهر الحالي
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {worth}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                ريال
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
    </Flex>
  );
};
WorkersRechargeCost.displayName = 'WorkersRechargeCost';

WorkersRechargeCost.propTypes = {
  costStatistics: PropTypes.shape({}),
};

WorkersRechargeCost.defaultProps = {
  costStatistics: {},
};

export default WorkersRechargeCost;
