import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { REQUESTS_STATUSES, CANCELLATION_REASONS } from 'utils/constants';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';


const RequestsCounts = props => {
  const { requestsCounts } = props;
  const { GREY_LIGHT, DISABLED, BRANDING_GREEN } = COLORS;
  const { SUPER_TITLE, BODY, SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const requestsCountsKeys = requestsCounts ? Object.keys(requestsCounts) : [];



  const createCanceledAndTimedOutRequestCards = canceledRequests => {
    const cancellationCards = [];
    let total = requestsCounts.timedOut || 0;
    Object.keys(canceledRequests).forEach(key => {
      total += canceledRequests[key];
      cancellationCards.push(
        <Box key={key} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
          <Card
            minHeight="95px"
            maxHeight="250px"
            flexDirection="column"
            alignItems="center"
            mx={2}
            mb={2}
            bgColor={COLORS_VALUES[GREY_LIGHT]}
          >
            <CardTitle px={4} py={2} width={1}>
              <Text
                textAlign="right"
                width={1}
                color={COLORS_VALUES[DISABLED]}
                type={SUBHEADING}
                fontWeight={NORMAL}
              >
                ملغى
                <Text mx={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                  {`- ${CANCELLATION_REASONS[key].ar}`}
                </Text>
              </Text>
            </CardTitle>
            <FlexContainer
              flexDirection="column"
              width={1}
              p={3}
              alignItems="center"
              justifyContent="center"
            >
              <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
                <Text
                  mx={1}
                  color={COLORS_VALUES[BRANDING_GREEN]}
                  type={SUPER_TITLE}
                  fontWeight={NORMAL}
                >
                  {canceledRequests[key]}
                </Text>
                <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                  طلب
                </Text>
              </Flex>
            </FlexContainer>
          </Card>
        </Box>,
      );
    });
    cancellationCards.push(
      <Box key="timedOut" width={[1, 1 / 2, 1 / 3, 1 / 4]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
        >
          <CardTitle px={4} py={2} width={1}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              ملغى
              <Text mx={1} color={COLORS_VALUES[BRANDING_GREEN]}>
                {`- ${REQUESTS_STATUSES['timed-out'].ar}`}
              </Text>
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="column"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {requestsCounts.timedOut}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                طلب
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>,
    );
    cancellationCards.push(
      <Box key="totalCancellations" width={[1, 1 / 2, 1 / 3, 1 / 4]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
        >
          <CardTitle px={4} py={2} width={1}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجمالى الطلبات الملغية
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="column"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {total}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                طلب
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>,
    );
    return cancellationCards;
  };

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {requestsCountsKeys.map(key => {
        if (key === 'timedOut' || key === 'reviewed' || key === 'done') {
          return <div key={key} />;
        }
        if (key === 'canceled') {
          return createCanceledAndTimedOutRequestCards(requestsCounts[key]);
        }
        const requestStatus = REQUESTS_STATUSES[key].ar;

        return (
          <Box key={key} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
            <Card
              minHeight="95px"
              maxHeight="250px"
              flexDirection="column"
              alignItems="center"
              mx={2}
              mb={2}
              bgColor={COLORS_VALUES[GREY_LIGHT]}
            >
              <CardTitle px={4} py={2} width={1}>
                <Text
                  textAlign="right"
                  width={1}
                  color={COLORS_VALUES[DISABLED]}
                  type={SUBHEADING}
                  fontWeight={NORMAL}
                >
                  {requestStatus}
                </Text>
              </CardTitle>
              <FlexContainer
                flexDirection="row-reverse"
                width={1}
                p={3}
                alignItems="center"
                justifyContent="center"
              >
                <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
                  <Text
                    mx={1}
                    color={COLORS_VALUES[BRANDING_GREEN]}
                    type={SUPER_TITLE}
                    fontWeight={NORMAL}
                  >
                    {requestsCounts[key]}
                  </Text>
                  <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                    طلب
                  </Text>
                </Flex>
              </FlexContainer>
            </Card>
          </Box>
        );
      })}
      <Box key="totalCompletedRequests" width={[1, 1 / 2, 1 / 3, 1 / 4]}>
        <Card
          minHeight="95px"
          maxHeight="250px"
          flexDirection="column"
          alignItems="center"
          mx={2}
          mb={2}
          bgColor={COLORS_VALUES[GREY_LIGHT]}
        >
          <CardTitle px={4} py={2} width={1}>
            <Text
              textAlign="right"
              width={1}
              color={COLORS_VALUES[DISABLED]}
              type={SUBHEADING}
              fontWeight={NORMAL}
            >
              اجمالى الطلبات المكتملة
            </Text>
          </CardTitle>
          <FlexContainer
            flexDirection="row-reverse"
            width={1}
            p={3}
            alignItems="center"
            justifyContent="center"
          >
            <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
              <Text
                mx={1}
                color={COLORS_VALUES[BRANDING_GREEN]}
                type={SUPER_TITLE}
                fontWeight={NORMAL}
              >
                {Number(requestsCounts.done) + Number(requestsCounts.reviewed)}
              </Text>
              <Text color={COLORS_VALUES[DISABLED]} type={BODY} fontWeight={NORMAL}>
                طلب
              </Text>
            </Flex>
          </FlexContainer>
        </Card>
      </Box>
      
    </Flex>
  );
};
RequestsCounts.displayName = 'RequestsCounts';

RequestsCounts.propTypes = {
  requestsCounts: PropTypes.shape({}),
};

RequestsCounts.defaultProps = {
  requestsCounts: undefined,
};

export default RequestsCounts;
