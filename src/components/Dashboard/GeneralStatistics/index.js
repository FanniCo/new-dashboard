import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { fontSize } from 'styled-system';

const GeneralStatistics = props => {
  const {
    generalStatistics: {
      averagePartsCosts,
      averageWorkersWage,
      totalCommission,
      totalDeliveryCostCommission,
      totalInvoices,
      totalNumberOfActiveWorkingWorkers,
      totalNumberOfAutoAppliedWorkers,
      totalNumberOfCustomers,
      retentionRate,
      totalNumberOfCustomersThatMadeDoneRequests,
      totalNumberOfDoneRequests,
      totalNumberOfFeaturedWorkers,
      totalNumberOfMultiFieldsWorkers,
      totalNumberOfNotWorkingWorkers,
      totalNumberOfSuspendedWorkers,
      totalVAT,
    },
  } = props;
  const { DISABLED, WARNING, CYAN } = COLORS;
  const { SUPER_TITLE, SUBHEADING, TITLE } = FONT_TYPES;
  const { NORMAL, BOLD } = FONT_WEIGHTS;

  const createCard = (count, title, color = COLORS_VALUES[CYAN]) => (
    <Box key={`${title}-${count}`} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        m={2}
        bgColor={color}
        className="StatictesCardContent"
      >
        <CardTitle px={4} py={2} width={1} className='StatictesCardTitle'>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {title}
          </Text>
        </CardTitle>
        <FlexContainer
          flexDirection="row-reverse"
          width={1}
          p={3}
          alignItems="center"
          justifyContent="center"
          className='StatictesCardNumber'
        >
          <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
            <Text mx={1} color={COLORS_VALUES[WARNING]} type={SUPER_TITLE} fontWeight={NORMAL}>
              {count}
            </Text>
            <Text color={COLORS_VALUES[DISABLED]} type={TITLE} fontWeight={NORMAL}>
              فني
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  );

  const createFullCard = (count, title, color = COLORS_VALUES[CYAN]) => (
    <Box key={`${title}-${count}`} width={[1, 1 / 2, 1 / 3, 1 / 3]}>
      <Card
        minHeight="95px"
        maxHeight="350px"
        flexDirection="column"
        alignItems="center"
        m={2}
        bgColor={color}
        className="StatictesCardContent"
      >
        <CardTitle px={4} py={2} width={1}  className='StatictesCardTitle'>
          <Text
            textAlign="right"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {title}
          </Text>
        </CardTitle>
        <FlexContainer
          flexDirection="row-reverse"
          width={1}
          p={3}
          alignItems="center"
          justifyContent="center"
          className='StatictesCardNumber'
        >
          <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
            <Text mx={1} color={COLORS_VALUES[WARNING]} type={SUPER_TITLE} fontWeight={NORMAL} style={{fontSize:'32px'}}>
              {count}
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  );

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text mx={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          
        </Text>
      </Flex>
      {createCard(totalNumberOfActiveWorkingWorkers, 'عدد الفنيين النشيطيين')}
      {createCard(totalNumberOfNotWorkingWorkers, 'عدد الفنيين الخاملين')}
      {createCard(totalNumberOfSuspendedWorkers, 'عدد الفنيين المتوقفين')}
      {createCard(totalNumberOfFeaturedWorkers, 'عدد اشترالك النجمة الذهبيه')}
      {createCard(totalNumberOfAutoAppliedWorkers, 'عدد اشتراك التلقائي')}
      {createCard(totalNumberOfMultiFieldsWorkers, 'عدد اشتراك إضافة مهنة')}
      {/*  ---------------------------------------------- */}
      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
        className='StatictesBigTitle'
      >
        <Text mx={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          إحصائيات عامة - معادلات
        </Text>
      </Flex>
      {createFullCard(
        // العملاء اللي عندهم أكثر من طلب مكتمل / العملاء اللي طلبوا طلبات مكتملة *
        retentionRate,
        'Retention Rate',
      )}
      {createFullCard(
        (totalNumberOfDoneRequests / totalNumberOfCustomersThatMadeDoneRequests).toFixed(2),
        'Average Done Requests',
      )}
      {createFullCard(totalCommission, 'Revenue = إجمالي عمولة الشركة من كل طلب')}
      {createFullCard(totalVAT, 'إجمالي الضريبة من كل طلب = Revenue')}
      {createFullCard(
        totalDeliveryCostCommission.toFixed(2),
        'عمولة الشركة من توصيل قطع الغيار = Delivery',
      )}
      {createFullCard(averageWorkersWage.toFixed(2), '(متوسط شغل اليد) = Basket')}
      {createFullCard(averagePartsCosts.toFixed(2), ' (متوسط قطع الغيار) = Basket')}
      {createFullCard(
        (totalInvoices / totalNumberOfDoneRequests).toFixed(2),
        ' (اجمالى مبلغ الفواتير / عدد الطلبات المكتملة) = Basket',
      )}
      {createFullCard(
        (totalNumberOfCustomersThatMadeDoneRequests / totalNumberOfCustomers).toFixed(2),
        'Conversion',
      )}
    </Flex>
  );
};
GeneralStatistics.displayName = 'GeneralStatistics';

GeneralStatistics.propTypes = {
  generalStatistics: PropTypes.shape({}),
};

GeneralStatistics.defaultProps = {
  generalStatistics: {},
};

export default GeneralStatistics;
