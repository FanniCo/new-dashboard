import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import { CardTitle, FlexContainer } from 'components/shared';
import { CITIES, FIELDS, TICKETS_STATUS } from 'utils/constants';
import _ from 'lodash';

const TicketsStatistics = props => {
  const {
    ticketsStatistics: {
      transactions: {
        deductionFromWorker,
        paymentsToCustomer,
        paymentsToWorker,
        ticketsCountHasFees,
      },
      ticketsForEachStatus,
      cities,
      fields,
    },
  } = props;

  const { DISABLED, WARNING, CYAN } = COLORS;
  const { SUPER_TITLE, SUBHEADING } = FONT_TYPES;
  const { NORMAL, BOLD } = FONT_WEIGHTS;

  const createCard = (count, title, color = COLORS_VALUES[CYAN]) => (
    <Box key={`${title}-${count}`} width={[1, 1 / 2, 1 / 3, 1 / 4]}>
      <Card
        minHeight="95px"
        maxHeight="250px"
        flexDirection="column"
        alignItems="center"
        m={2}
        bgColor={color}
      >
        <CardTitle px={4} py={2} width={1}>
          <Text
            textAlign="center"
            width={1}
            color={COLORS_VALUES[DISABLED]}
            type={SUBHEADING}
            fontWeight={NORMAL}
          >
            {title}
          </Text>
        </CardTitle>
        <FlexContainer
          flexDirection="row-reverse"
          width={1}
          p={3}
          alignItems="center"
          justifyContent="center"
        >
          <Flex alignItems="baseline" justifyContent="center" flexDirection="row-reverse">
            <Text mx={1} color={COLORS_VALUES[WARNING]} type={SUPER_TITLE} fontWeight={NORMAL}>
              {count}
            </Text>
          </Flex>
        </FlexContainer>
      </Card>
    </Box>
  );

  const createSection = (title, cards) => (
    <>
      <Flex
        width={1}
        py={2}
        flexDirection="row-reverse"
        flexWrap="wrap"
        justifyContent="space-between"
        alignItems="center"
      >
        <Text mx={2} type={SUPER_TITLE} color={COLORS_VALUES[WARNING]} fontWeight={'bold'}>
          {title}
        </Text>
      </Flex>

      {cards}
    </>
  );

  const ticketsGeneralStatistics = (
    <>
      {createCard(deductionFromWorker, 'إجمالي المبلغ المخصوم من الفنيين')}
      {createCard(paymentsToCustomer, 'إجمالي المدفوعات للعملاء')}
      {createCard(paymentsToWorker, 'إجمالي المدفوعات للفنيين')}
      {createCard(ticketsCountHasFees, 'عدد الشكاوي برسوم')}
    </>
  );

  const ticketsStatisticsForStatusCards = _.map(TICKETS_STATUS, value => {
    const count =
      _.get(
        _.find(ticketsForEachStatus, status => status.status === value.en),
        'count',
      ) || 0;
    return createCard(count, value.ar);
  });

  const ticketsStatisticsForCitiesCards = _.map(CITIES, value => {
    const count =
      _.get(
        _.find(cities, city => city._id === value.en),
        'count',
      ) || 0;
    return createCard(count, value.ar);
  });

  const ticketsStatisticsForFieldsCards = _.map(FIELDS, (value, key) => {
    const count =
      _.get(
        _.find(fields, field => field._id === key),
        'count',
      ) || 0;
    return createCard(count, value.ar);
  });

  return (
    <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
      {createSection('إحصائيات الشكاوي', ticketsGeneralStatistics)}
      {createSection('إحصائيات عدد الشكاوي في كل حالة', ticketsStatisticsForStatusCards)}
      {createSection('إحصائيات عدد الشكاوي في كل مدينة', ticketsStatisticsForCitiesCards)}
      {createSection('إحصائيات عدد الشكاوي في كل خدمة', ticketsStatisticsForFieldsCards)}
    </Flex>
  );
};

TicketsStatistics.displayName = 'TicketsStatistics';

TicketsStatistics.propTypes = {
  ticketsStatistics: PropTypes.shape({}),
};

TicketsStatistics.defaultProps = {
  ticketsStatistics: {},
};

export default TicketsStatistics;
