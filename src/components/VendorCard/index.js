import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import {
  CardBody,
  SubTitle,
  CardFooter,
  CallToActionIcon,
  CardLabel,
  HyperLink,
} from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ReactTooltip from 'react-tooltip';
import { faDollarSign, faLock, faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const VendorCard = props => {
  const {
    user: { permissions },
    vendor,
    vendor: {
      _id,
      name,
      username,
      governmentId,
      createdBy,
      createdAt,
      expiredAt,
      doneRequestsCount,
      canceledRequestsCount,
      workersCount,
      totalWorkersCreditCharge,
      totalWorkersWage,
      commercialRecordUrl,
    },
    index,
    handleToggleConfirmModal,
    handleToggleEditVendorModal,
    handleToggleChangePasswordModal,
    handleDeleteVendor,
    handleResetVendorCredits,
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, LIGHT_RED } = COLORS;
  const { BODY, HEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const vendorDate = new Date(createdAt);
  const deleteVendorTooltip = 'مسح الشريك';
  const editVendorTooltip = 'تعديل بيانات الشريك';
  const changePasswordTooltip = 'تغير الباسورد';

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex alignItems="center" justifyContent="space-between" py={2} px={2} mb={1} width={1}>
          <Flex alignItems="center">
            <CardLabel
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {username}
            </CardLabel>
          </Flex>
          <Flex flexDirection="row" alignItems="center">
            <Text type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
              {name}
            </Text>
          </Flex>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            رقم الهوية :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {governmentId}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى الطلبات المكتملة :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {doneRequestsCount || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى الطبات الملغية :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {canceledRequestsCount || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى عدد الفنين :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {workersCount || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى شحن الفنين :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {totalWorkersCreditCharge || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            اجمالى دخل الفنين :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {totalWorkersWage || 0}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            رابط السجل التجارى:
          </SubTitle>
          <HyperLink href={commercialRecordUrl} target="_blank">
            <CardLabel
              cursor="pointer"
              pt="6px"
              pb={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[BRANDING_GREEN]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              التفاصيل
            </CardLabel>
          </HyperLink>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تاريخ الانشاء :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {vendorDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            تاريخ انتهاء التعاقد :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {new Date(expiredAt).toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
      </CardBody>
      {permissions &&
        (permissions.includes('Delete Vendor') ||
          permissions.includes('Update Vendor') ||
          permissions.includes('Change Vendor Password') ||
          permissions.includes('Reset Vendor Credits')) && (
          <CardFooter
            flexDirection="row-reverse"
            justifyContent="space-between"
            flexWrap="wrap"
            p={2}
          >
            <Flex px={2}>
              {permissions && permissions.includes('Delete Vendor') && (
                <>
                  <ReactTooltip id={`deleteVendor_${_id}`} type="error" effect="solid">
                    <span>{deleteVendorTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`deleteVendor_${_id}`}
                      data-tip
                      onClick={() =>
                        handleToggleConfirmModal(
                          'تأكيد مسح الشريك',
                          'هل أنت متأكد أنك تريد مسح الشريك؟',
                          () => {
                            handleDeleteVendor(_id, index);
                          },
                        )
                      }
                      icon={faTrashAlt}
                      color={COLORS_VALUES[LIGHT_RED]}
                      size="sm"
                    />
                  </Box>
                </>
            )}
              {permissions && permissions.includes('Update Vendor') && (
                <>
                  <ReactTooltip id={`editWorker_${_id}`} type="light" effect="solid">
                    <span>{editVendorTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`editWorker_${_id}`}
                      data-tip
                      onClick={() => handleToggleEditVendorModal(index, vendor)}
                      icon={faPen}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
            )}
            {permissions && permissions.includes('Change Vendor Password') && (
                <>
                  <ReactTooltip id={`changeAdminPassword_${_id}`} type="light" effect="solid">
                    <span>{changePasswordTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`changeAdminPassword_${_id}`}
                      data-tip
                      onClick={() => {
                        handleToggleChangePasswordModal(vendor);
                      }}
                      icon={faLock}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
              {permissions && permissions.includes('Reset Vendor Credits') && (
                <>
                  <ReactTooltip id={`resetCredit_${_id}`} type="light" effect="solid">
                    <span> تصفير مبلغ التحصيل لى الشريك</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`resetCredit_${_id}`}
                      data-tip
                      onClick={() =>
                        handleToggleConfirmModal(
                          'اعدة تعيين رصيد الشريك',
                          'هل انتا متاكد من اعادة تععين رصيد الشريك',
                          () => {
                            handleResetVendorCredits(_id, index);
                          },
                        )
                      }
                      icon={faDollarSign}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
            </Flex>
          </CardFooter>
      )}
    </Card>
  );
};
VendorCard.displayName = 'VendorCard';

VendorCard.propTypes = {
  vendor: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditVendorModal: PropTypes.func,
  handleToggleChangePasswordModal: PropTypes.func,
  handleDeleteVendor: PropTypes.func,
  handleResetVendorCredits: PropTypes.func,
};

VendorCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditVendorModal: () => {},
  handleToggleChangePasswordModal: () => {},
  handleDeleteVendor: () => {},
  handleResetVendorCredits: () => {},
};

export default VendorCard;
