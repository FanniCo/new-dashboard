import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { width, display } from 'styled-system';
import {
  faExchangeAlt,
  faUsers,
  faTicketAlt,
  faWrench,
  faBell,
  faTags,
  faFileAlt,
  faPenSquare,
  faCog,
  faShoppingBasket,
  faFileInvoiceDollar,
  faChartBar,
  faCommentDots,
  faFileArchive,
  faUserSecret,
  faAd,
  faGifts,
  faThLarge,
  faMoneyBillAlt,
  faAppleAlt,
} from '@fortawesome/free-solid-svg-icons';
import ROUTES from 'routes';
import Items from './Items';

const {
  DASHBOARD,
  REQUESTS,
  TICKETS,
  CLIENTS,
  WORKERS,
  REVIEWS,
  NOTIFICATIONS,
  TRANSACTIONS_LOGS,
  REPORTS,
  REPORT_REQUESTS_PAYMENTS,
  REPORT_CLIENTS_PAYMENTS,
  REPORT_WORKERS_REQUESTS_COUNT,
  REPORT_WORKERS_TRANSACTIONS,
  SETTINGS,
  ADMINS,
  OFFERS,
  PROMO_CODES,
  MESSAGES,
  ACTIVITIES_LOGS,
  REPORT_CLIENTS_REQUESTS,
  REPORT_PROMO_CODES,
  WORKERS_ADS,
  CUSTOMERS_ADS,
  VENDORS,
  POSTS,
  COMMISSIONS,
  ADMINS_ACTIVITIES,
  VOUCHERS,
  APP_CONFIGS,
  TICKETS_REPORT,
  THE_MONTH_SUMMARY_REPORT,
  LOYALTY_POINTS_PARTNERS_REPORT,
  RECHARGE_AWARDS,
  THE_MOST_WORKERS_HAS_TICKETS_REPORT
} = ROUTES;

const SideMenuContainer = styled.div`
  ${display};
  ${width};
  ${props => (props.isRtl ? 'right: 0;' : 'left: 0;')}
  background-color: ${props => props.theme.colors.greyDark};
  border-right: 1px solid ${props => props.theme.colors.greyLight};
  justify-content: space-around;
  padding: 0;
  top: 65px;
  z-index: 0;
`;

const VerticalHeader = props => {
  const {
    user: { permissions },
    location: { pathname },
  } = props;
  const activeNav = pathname;
  const navs = [
    {
      type: 'nav',
      name: {
        ar: 'الرئيسية',
        en: 'Dashboard',
      },
      icon: faChartBar,
      url: DASHBOARD,
      enable: !!(permissions && permissions.includes('Dashboard')),
    },
    {
      type: 'nav',
      name: {
        ar: 'الطلبات',
        en: 'Requests',
      },
      icon: faExchangeAlt,
      url: REQUESTS,
      enable: !!(permissions && permissions.includes('Get All Requests')),
    },
    {
      type: 'nav',
      name: {
        ar: 'الشكاوي',
        en: 'Tickets',
      },
      icon: faTicketAlt,
      url: TICKETS,
      enable: !!(permissions && permissions.includes('Get All Tickets')),
    },
    {
      type: 'nav',
      name: {
        ar: 'الشركاء',
        en: 'Vendors',
      },
      icon: faUserSecret,
      url: VENDORS,
      enable: !!(permissions && permissions.includes('Get All Vendors')),
    },
    {
      type: 'nav',
      name: {
        ar: 'العملاء',
        en: 'Clients',
      },
      icon: faUsers,
      url: CLIENTS,
      enable: !!(permissions && permissions.includes('Get All Users')),
    },
    {
      type: 'nav',
      name: {
        ar: 'الفنيين',
        en: 'Map',
      },
      icon: faWrench,
      url: WORKERS,
      enable: !!(permissions && permissions.includes('Get All Workers')),
    },
    {
      type: 'nav',
      name: {
        ar: 'التقييمات',
        en: 'Reviews',
      },
      icon: faPenSquare,
      url: REVIEWS,
      enable: !!(permissions && permissions.includes('Get Reviews')),
    },
    {
      type: 'nav',
      name: {
        ar: 'المعاملات',
        en: 'Transcations Logs',
      },
      icon: faFileInvoiceDollar,
      url: TRANSACTIONS_LOGS,
      enable: !!(permissions && permissions.includes('Get Transactions')),
    },
    {
      type: 'nav',
      name: {
        ar: 'التنبيهات',
        en: 'Notifications',
      },
      icon: faBell,
      url: NOTIFICATIONS,
      enable: !!(permissions && permissions.includes('Send Target Notification')),
    },
    {
      type: 'nav',
      name: {
        ar: 'البروموكود',
        en: 'Promo Codes',
      },
      icon: faTags,
      url: PROMO_CODES,
      enable: !!(permissions && permissions.includes('Get All Promo Codes')),
    },
    {
      type: 'nav',
      name: {
        ar: 'الفوتشرز',
        en: 'Vouchers',
      },
      icon: faGifts,
      url: VOUCHERS,
      enable: !!(permissions && permissions.includes('Get All Vouchers')),
    },
    {
      type: 'nav',
      name: {
        ar: 'الرسائل',
        en: 'Messages',
      },
      icon: faCommentDots,
      url: MESSAGES,
      enable: !!(permissions && permissions.includes('Get All Messages')),
    },
    {
      type: 'nav',
      name: {
        ar: 'اعلانات الفنين',
        en: 'Workers Ads',
      },
      icon: faAd,
      url: WORKERS_ADS,
      enable: !!(permissions && permissions.includes('Get All Workers Ads')),
    },
    {
      type: 'nav',
      name: {
        ar: 'اعلانات العملاء',
        en: 'Customers Ads',
      },
      icon: faAd,
      url: CUSTOMERS_ADS,
      enable: !!(permissions && permissions.includes('Get All Customers Ads')),
    },
    {
      type: 'nav',
      name: {
        ar: 'النشاطات',
        en: 'activities',
      },
      icon: faFileArchive,
      url: ACTIVITIES_LOGS,
      enable: !!(permissions && permissions.includes('Get Activity Logs')),
    },
    {
      type: 'nav',
      name: {
        ar: 'العروض',
        en: 'Offers',
      },
      icon: faGifts,
      url: OFFERS,
      enable: !!(permissions && permissions.includes('Get All Offers')),
    },
    {
      type: 'nav',
      name: {
        ar: 'المقالات',
        en: 'Posts',
      },
      icon: faThLarge,
      url: POSTS,
      enable: !!(permissions && permissions.includes('Get All Posts')),
    },
    {
      type: 'nav',
      name: {
        ar: 'معدلات عمولة الطلبات',
        en: 'Commissions Rate',
      },
      icon: faMoneyBillAlt,
      url: COMMISSIONS,
      enable: !!(permissions && permissions.includes('Get All Commissions')),
    },
    {
      type: 'nav',
      name: {
        ar: 'مكافآت شحن الفنيين',
        en: 'Recharge Award',
      },
      icon: faGifts,
      url: RECHARGE_AWARDS,
      enable: !!(permissions && permissions.includes('Get All Recharge Awards')),
    },
    {
      type: 'submenu',
      name: {
        ar: 'التقارير',
        en: 'Reports',
      },
      icon: faFileAlt,
      url: REPORTS,
      enable:
        (permissions && permissions.includes('Get Request Payment Report')) ||
        (permissions && permissions.includes('Get Client Payment Report')) ||
        (permissions && permissions.includes('Get Worker Requests Count Report')) ||
        (permissions && permissions.includes('Get Transactions')) ||
        (permissions && permissions.includes('Get Admins Activities')) ||
        (permissions && permissions.includes('Get the most worker has tickets report')),
      submenuItems: [
        {
          type: 'nav',
          name: {
            ar: 'تقرير التكلفة لكل طلب',
            en: 'Requests Payment',
          },
          icon: faFileAlt,
          url: REPORT_REQUESTS_PAYMENTS,
          enable: !!(permissions && permissions.includes('Get Request Payment Report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير اجمالى المدفوعات للعملاء',
            en: 'Clients Payment',
          },
          icon: faFileAlt,
          url: REPORT_CLIENTS_PAYMENTS,
          enable: !!(permissions && permissions.includes('Get Client Payment Report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير احصائى للفنيين',
            en: 'Workers Requests Count',
          },
          icon: faFileAlt,
          url: REPORT_WORKERS_REQUESTS_COUNT,
          enable: !!(permissions && permissions.includes('Get Worker Requests Count Report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير العمليات المالية لجميع الفنيين',
            en: 'Workers Transactions',
          },
          icon: faFileAlt,
          url: REPORT_WORKERS_TRANSACTIONS,
          enable: !!(permissions && permissions.includes('Get Transactions')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير احصائى للعملاء',
            en: 'Clients Requests',
          },
          icon: faFileAlt,
          url: REPORT_CLIENTS_REQUESTS,
          enable: !!(permissions && permissions.includes('Get Customers Requests Count Report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير الخصومات',
            en: 'promo coded Report',
          },
          icon: faFileAlt,
          url: REPORT_PROMO_CODES,
          enable: !!(permissions && permissions.includes('Get Promo Code Report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير سجل النظام',
            en: 'Admins Activities Report',
          },
          icon: faFileAlt,
          url: ADMINS_ACTIVITIES,
          enable: !!(permissions && permissions.includes('Get Admins Activities')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير الشكاوى',
            en: 'Tickets Report',
          },
          icon: faFileAlt,
          url: TICKETS_REPORT,
          enable: !!(permissions && permissions.includes('Get Tickets Report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير ملخص الشهر',
            en: 'The month summary report',
          },
          icon: faFileAlt,
          url: THE_MONTH_SUMMARY_REPORT,
          enable: !!(permissions && permissions.includes('The month summary report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير اكواد شركاء نقاط الولاء',
            en: 'loyalty points partners report',
          },
          icon: faFileAlt,
          url: LOYALTY_POINTS_PARTNERS_REPORT,
          enable: !!(permissions && permissions.includes('loyalty points partners report')),
        },
        {
          type: 'nav',
          name: {
            ar: 'تقرير اكثر الفنيين شكوي',
            en: 'the most workers has tickets report',
          },
          icon: faFileAlt,
          url: THE_MOST_WORKERS_HAS_TICKETS_REPORT,
          enable: !!(permissions && permissions.includes('Get the most worker has tickets report')),
        },
      ],
    },
    {
      type: 'submenu',
      name: {
        ar: 'الإعدادات',
        en: 'Settings',
      },
      icon: faCog,
      url: SETTINGS,
      enable: permissions && permissions.includes('Get All Settings'),
      submenuItems: [
        {
          type: 'nav',
          name: {
            ar: 'إعدادات تحديثات الهاتف',
            en: 'App Configs',
          },
          icon: faAppleAlt,
          url: APP_CONFIGS,
          // enable: !!(permissions && permissions.includes('Get App Configs')),
          enable: true,
        },
        {
          type: 'nav',
          name: {
            ar: 'مديرين النظام',
            en: 'Admins',
          },
          icon: faShoppingBasket,
          url: ADMINS,
          enable: true,
        },
        {
          type: 'nav',
          name: {
            ar: 'العروض',
            en: 'Offers',
          },
          icon: faShoppingBasket,
          url: OFFERS,
          enable: false,
        },
      ],
    },
  ];

  return (
    <SideMenuContainer isRtl width={['35px', '75px', '75px', '75px', '75px']} className="SideMenuContainer">
      <Items isLoading={false} activeNav={activeNav} navs={navs} />
    </SideMenuContainer>
  );
};

VerticalHeader.propTypes = {
  user: PropTypes.shape({}),
  location: PropTypes.shape({
    hash: PropTypes.string,
    pathname: PropTypes.string,
    search: PropTypes.string,
  }).isRequired,
};

VerticalHeader.defaultProps = {
  user: undefined,
};

export default VerticalHeader;
