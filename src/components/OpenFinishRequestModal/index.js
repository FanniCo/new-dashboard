import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { Flex } from '@rebass/grid';
import { finishRequest, finishRequestForReconditioningField } from 'redux-modules/requests/actions';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { FIELDS, PAYMENT_METHOD } from 'utils/constants';
import ContentEditable from 'components/ContentEditable';
import Caution from 'components/Caution';
import { customSelectStyles, selectColors, StyledSelect } from 'components/shared';
import InputField from 'components/InputField';
import _ from 'lodash';

const paymentMethodKeys = Object.keys(PAYMENT_METHOD);
const paymentMethodSelectOptions = paymentMethodKeys.map(key => ({
  value: key,
  label: PAYMENT_METHOD[key].ar,
}));

class OpenFinishRequestModal extends Component {
  static propTypes = {
    request: PropTypes.shape({}),
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
    finishRequest: PropTypes.func,
    finishRequestForReconditioningField: PropTypes.func,
    isFinishRequestError: PropTypes.string,
    isFinishRequestErrorMessage: PropTypes.string,
  };

  static defaultProps = {
    request: undefined,
    isOpened: false,
    toggleFunction: () => {},
    finishRequest: () => {},
    finishRequestForReconditioningField: () => {},
    isFinishRequestError: undefined,
    isFinishRequestErrorMessage: undefined,
  };

  state = {
    invoiceText: '',
    paymentMethod: undefined,
    onlinePaymentRef: undefined,
    hasOnlinePaymentRef: false,
  };

  handleFinishRequest = status => {
    const { finishRequest: finishRequestAction, request } = this.props;
    const { paymentMethod, onlinePaymentRef, hasOnlinePaymentRef } = this.state;

    const info = {
      requestId: request._id,
      paymentStatus: status,
    };

    if (status === 'proceed') {
      info.paymentMethod = _.get(paymentMethod, 'value');
    }

    if (hasOnlinePaymentRef) {
      info.onlinePaymentRef = onlinePaymentRef;
    }

    finishRequestAction(info);
  };

  handleFinishRequestForReconditioningField = () => {
    const {
      finishRequestForReconditioningField: finishRequestForReconditioningFieldAction,
      request,
    } = this.props;
    const { invoiceText } = this.state;

    const info = {
      requestId: request._id,
      invoiceText,
    };

    finishRequestForReconditioningFieldAction(info);
  };

  handleAddCommentInputChange = value => {
    this.setState({ invoiceText: value });
  };

  handleChangePaymentMethodSelect = paymentMethod => {
    if (paymentMethod.value === PAYMENT_METHOD.cash.en.toLowerCase()) {
      this.setState({ paymentMethod, hasOnlinePaymentRef: false });
    } else {
      this.setState({ paymentMethod, hasOnlinePaymentRef: true });
    }
  };

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  render() {
    const {
      request,
      isOpened,
      toggleFunction,
      isFinishRequestError,
      isFinishRequestErrorMessage,
      isFinishingRequest,
    } = this.props;
    const { invoiceText, hasOnlinePaymentRef, onlinePaymentRef } = this.state;
    const { BRANDING_GREEN, WHITE, LIGHT_RED, ERROR, WARNING } = COLORS;
    const { HEADING } = FONT_TYPES;
    let renderContent = '';

    if (request.field === FIELDS.reconditioning.en.toLowerCase()) {
      const finishRequestModalHeader = 'انهاء واصدار الفاتور لهذا الطلب';
      renderContent = (
        <Modal toggleModal={toggleFunction} header={finishRequestModalHeader} isOpened={isOpened}>
          <Flex flexDirection="column" px={3}>
            <Flex width={1} pt={4} p={2} flexDirection="column">
              <Text mb={2} color={COLORS_VALUES[WHITE]}>
                اضف تفاصيل الفاتورة
              </Text>
              <ContentEditable
                placeholder="اضف تفاصيل الفاتورة"
                onChange={(e, value) => {
                  this.handleAddCommentInputChange(value);
                }}
                html={invoiceText}
              />
              <Flex flexDirection="row-reverse">
                <Button
                  mt={2}
                  isLoading={false}
                  color={BRANDING_GREEN}
                  onClick={() => this.handleFinishRequestForReconditioningField()}
                  disabled={!invoiceText}
                >
                  انهاء الطلب
                </Button>
              </Flex>
            </Flex>
          </Flex>
        </Modal>
      );
    }

    if (request.field !== FIELDS.reconditioning.en.toLowerCase()) {
      const finishRequestModalHeader = 'تم اصدار الفاتورة لهذا الطلب من قبل الفنى';
      renderContent = (
        <Modal toggleModal={toggleFunction} header={finishRequestModalHeader} isOpened={isOpened}>
          <Flex flexDirection="column" px={3}>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  هل تريد انهاء هذا الطلب؟
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  اجرة الفنى :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.workersWage}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  تكلفة القطع :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.partsCost}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  الضريبة :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.vatValue}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  مبلغ توصيل قطع الغيار :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.deliveryCostOfParts}
                  </Text>
                </Text>
              </Flex>
            </Flex>

            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  متبرع لجمعية خيرية بقيمة :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.donation}
                  </Text>
                </Text>
              </Flex>
            </Flex>

            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  اجمالى التكلفة :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.total}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  قيمة خصم البرموكود :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.discount}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  المبلغ تم تحصيله من محفظة العميل :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.moneyTakenFromCustomerCredits}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} px={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WHITE]}>
                  الاجمالى ما يجب على العميل دفعه :
                  <Text mr={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    {request.totalAfterDiscount}
                  </Text>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اختر طريقة الدفع
                </Text>
                <StyledSelect
                  mb={3}
                  isRtl
                  isMulti={false}
                  isClearable
                  placeholder="اختر طريقة الدفع"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  backspaceRemovesValue={false}
                  onChange={this.handleChangePaymentMethodSelect}
                  options={paymentMethodSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                />
              </Flex>
            </Flex>
            <Flex pb={3}>
              {hasOnlinePaymentRef && (
                <Flex flexDirection="column" width={1} pl={2}>
                  <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                    الرقم المرجعي لطريقة الدفع
                    <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                      *
                    </Text>
                  </Text>
                  <InputField
                    type="text"
                    placeholder="الرقم المرجعي لطريقة الدفع"
                    width={1}
                    mb={2}
                    value={onlinePaymentRef}
                    onChange={value => this.handleInputFieldChange('onlinePaymentRef', value)}
                    autoComplete="on"
                    isRequired={hasOnlinePaymentRef}
                  />
                </Flex>
              )}
            </Flex>
            <Flex pb={3}>
              <Flex flexDirection="column" alignItems="center" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[WARNING]}>
                  برجاء الانتظار قليلا بعد الضغط علي إنهاء الطلب وعدم تكرار العملية أكثر من مرة
                </Text>
              </Flex>
            </Flex>
            <Flex flexDirection="row-reverse" py={3}>
              <Button
                color={BRANDING_GREEN}
                onClick={() => this.handleFinishRequest('proceed')}
                isLoading={isFinishingRequest}
                disabled={isFinishingRequest}
                mx={1}
              >
                انهاء الطلب
              </Button>
              <Button
                color={LIGHT_RED}
                onClick={() => this.handleFinishRequest('rejected')}
                isLoading={isFinishingRequest}
                disabled={isFinishingRequest}
                mx={1}
              >
                رفض انهاء الطلب
              </Button>
              {isFinishRequestError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {isFinishRequestErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </Modal>
      );
    }

    return renderContent;
  }
}

OpenFinishRequestModal.displayName = 'OpenFinishRequestModal';

const mapStateToProps = state => ({
  isFinishingRequest: state.requests.finishRequest.isFetching,
  isFinishRequestSuccess: state.requests.finishRequest.isSuccess,
  isFinishRequestError: state.requests.finishRequest.isFail.isError,
  isFinishRequestErrorMessage: state.requests.finishRequest.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      finishRequest,
      finishRequestForReconditioningField,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(OpenFinishRequestModal);
