import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from '@rebass/grid';
import { faPen, faTrashAlt, faLock } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, CardLabel, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';

const { GREY_LIGHT, DISABLED, BRANDING_GREEN, PRIMARY, LIGHT_RED } = COLORS;
const { BODY } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const AdminCard = props => {
  const {
    index,
    admin,
    admin: { _id, name, username, active },
    handleDeleteAdmin,
    handleToggleConfirmModal,
    handleToggleAddEditAdminModal,
    handleToggleChangeAdminPasswordModal,
  } = props;
  const editAdminTooltip = 'تعديل صلاحيات مدير النظام';
  const deleteAdminTooltip = 'مسح مدير النظام';
  const deleteAdminConfirmModalHeader = 'تأكيد مسح مدير النظام';
  const deleteAdminConfirmModalConfirmText = 'هل أنت متأكد أنك تريد مسح مدير النظام؟';
  const changeAdminPasswordTooltip = 'تغير الباسورد';
  const deleteAdminConfirmModalFunction = () => {
    handleDeleteAdmin(_id, index);
  };

  return (
    <Card
      {...props}
      height="calc(100% - 16px)"
      minHeight={100}
      flexDirection="column"
      justifyContent="space-between"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody
        justifyContent="space-between"
        flexDirection={['column', 'row', 'row', 'row', 'row']}
        py={3}
        px={1}
      >
        <Flex ml={1} alignItems="center">
          <Flex
            width={1}
            flexWrap={['wrap', 'initial', 'initial', 'initial', 'initial']}
            flexDirection={['row-reverse', 'column', 'column', 'column', 'column']}
            mb={[3, 0, 0, 0, 0]}
          >
            <CardLabel
              py={1}
              px={2}
              mx={2}
              type={BODY}
              border={`1px solid ${COLORS_VALUES[active ? PRIMARY : DISABLED]}`}
              color={COLORS_VALUES[DISABLED]}
              fontWeight={NORMAL}
            >
              {active ? 'فعال' : 'غير فعال'}
            </CardLabel>
          </Flex>
        </Flex>
        <Flex alignItems="center" flexDirection="row-reverse" px={2} width={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {`[ ${username} ] - ${name}`}
          </Text>
        </Flex>
      </CardBody>
      <CardFooter flexDirection="row-reverse" justifyContent="space-between" flexWrap="wrap" p={2}>
        <Flex px={2}>
          <ReactTooltip id={`changeAdminPassword_${_id}`} type="light" effect="solid">
            <span>{changeAdminPasswordTooltip}</span>
          </ReactTooltip>
          <Box mr={3}>
            <CallToActionIcon
              data-for={`changeAdminPassword_${_id}`}
              data-tip
              onClick={() => {
                handleToggleChangeAdminPasswordModal(admin);
              }}
              icon={faLock}
              color={COLORS_VALUES[DISABLED]}
              size="sm"
            />
          </Box>
          <ReactTooltip id={`deleteAdmin_${_id}`} type="error" effect="solid">
            <span>{deleteAdminTooltip}</span>
          </ReactTooltip>
          <Box mr={3}>
            <CallToActionIcon
              data-for={`deleteAdmin_${_id}`}
              data-tip
              onClick={() =>
                handleToggleConfirmModal(
                  deleteAdminConfirmModalHeader,
                  deleteAdminConfirmModalConfirmText,
                  deleteAdminConfirmModalFunction,
                )
              }
              icon={faTrashAlt}
              color={COLORS_VALUES[LIGHT_RED]}
              size="sm"
            />
          </Box>
          <ReactTooltip id={`editAdmin_${_id}`} type="light" effect="solid">
            <span>{editAdminTooltip}</span>
          </ReactTooltip>
          <Box>
            <CallToActionIcon
              data-for={`editAdmin_${_id}`}
              data-tip
              onClick={() => handleToggleAddEditAdminModal(admin)}
              icon={faPen}
              color={COLORS_VALUES[DISABLED]}
              size="sm"
            />
          </Box>
        </Flex>
      </CardFooter>
    </Card>
  );
};
AdminCard.displayName = 'AdminCard';

AdminCard.propTypes = {
  index: PropTypes.number.isRequired,
  admin: PropTypes.shape({}).isRequired,
  handleDeleteAdmin: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleAddEditAdminModal: PropTypes.func,
  handleToggleChangeAdminPasswordModal: PropTypes.func,
};

AdminCard.defaultProps = {
  handleDeleteAdmin: () => {},
  handleToggleConfirmModal: () => {},
  handleToggleAddEditAdminModal: () => {},
  handleToggleChangeAdminPasswordModal: () => {},
};

export default AdminCard;
