import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const TransactionStatusSelect = props => {
  const {
    selectedTransactionStatus,
    handleChangeTransactionStatusSelect,
    transactionStatusSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        حالة العملية
      </Text>
      <StyledSelect
        mb={3}
        placeholder="حالة العملية"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedTransactionStatus}
        onChange={handleChangeTransactionStatusSelect}
        options={transactionStatusSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
TransactionStatusSelect.displayName = 'TransactionStatusSelect';

TransactionStatusSelect.propTypes = {
  transactionStatusSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedTransactionStatus: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeTransactionStatusSelect: PropTypes.func.isRequired,
};

TransactionStatusSelect.defaultProps = {
  selectedTransactionStatus: undefined,
};

export default TransactionStatusSelect;
