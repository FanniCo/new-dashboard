import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex, Box } from '@rebass/grid';
import { space, bgColor } from 'styled-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import Card from 'components/Card';
import Text from 'components/Text';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
import {
  CONFIRM_CHOICE,
  FIELDS,
  ACTIVE_STATUS,
  CITIES,
  REQUESTS_STATUSES,
  TICKETS_STATUS,
  PAYMENT_METHOD,
  PAYMENT_STATUS,
  // TRANSACTION_TYPES,
  // TRANSACTION_STATUS,
  COMMISSION_RATE,
  TRANSACTION_SIGN,
  ACTIVITIES_LOGS,
  GROUPED_TRANSACTION_TYPES,
  CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES,
  RATINGS,
  CANCELLATION_REASONS,
} from 'utils/constants';
import RequestReasonsSelect from 'components/Filter/RequestReasonsSelect';
import _ from 'lodash';
import FieldsSelect from './FieldsSelect';
import ActiveStatusSelect from './ActiveSelect';
import CitiesSelect from './CitiesSelect';
import ConfirmSelect from './ConfirmSelect';
import FilterInput from './FilterInput';
import RequestsStatusSelect from './RequestsStatusSelect';
import ComplainNameSelect from './ComplainNameSelect';
import TicketStatusSelect from './TicketStatusSelect';
import PaymentMethodsSelect from './PaymentMethodsSelect';
import PaymentStatusSelect from './PaymentStatusSelect';
import TransactionTypesSelect from './TransactionTypesSelect';
// import TransactionStatusSelect from './TransactionStatusSelect';
import TransactionClassificationSelect from './TransactionClassificationSelect';
import CommissionRateSelect from './CommissionRateSelect';
import TransactionSignSelect from './TransactionSignSelect';
import DatePicker from './DatePicker';
import ActivitiesLogsSelect from './ActivitiesLogsSelect';
import VendorsSelect from './VendorsSelect';
import WorkerRatingSelect from './WorkerRatingSelect';

const cancellationReasonsKeys = Object.keys(CANCELLATION_REASONS);
const fieldsKeys = Object.keys(FIELDS);
const activeStatusKeys = Object.keys(ACTIVE_STATUS);
const ratingKeys = Object.keys(RATINGS);
const citiesKeys = Object.keys(CITIES);
const requestsStatusKeys = Object.keys(REQUESTS_STATUSES);
const ticketStatusKeys = Object.keys(TICKETS_STATUS);
const paymentStatusKeys = Object.keys(PAYMENT_STATUS);
const paymentMethodKeys = Object.keys(PAYMENT_METHOD);
// const transactionTypesKeys = Object.keys(TRANSACTION_TYPES);
// const transactionStatusKeys = Object.keys(TRANSACTION_STATUS);
const commissionRateKeys = Object.keys(COMMISSION_RATE);
const transactionSignKeys = Object.keys(TRANSACTION_SIGN);
const confirmChoiceKeys = Object.keys(CONFIRM_CHOICE);
const activitiesLogsKeys = Object.keys(ACTIVITIES_LOGS);
const groupedTransactionsTypesKeys = Object.keys(GROUPED_TRANSACTION_TYPES);
const { DISABLED, WHITE_TRANSPARENT_BACKGROUND } = COLORS;
const { TITLE, SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;
const confirmChoiceSelectOptions = confirmChoiceKeys.map(key => ({
  value: key,
  label: CONFIRM_CHOICE[key].ar,
}));
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
const activeStatusSelectOptions = activeStatusKeys.map(key => ({
  value: key,
  label: ACTIVE_STATUS[key].ar,
}));
const cancellationReasonsSelectOptions = cancellationReasonsKeys.map(key => ({
  value: key,
  label: CANCELLATION_REASONS[key].ar,
}));
const workerRatingSelectOptions = ratingKeys.map(key => ({
  value: key,
  label: RATINGS[key].ar,
}));
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));
const requestsStatusSelectOptions = requestsStatusKeys
  .filter(key => REQUESTS_STATUSES[key].useInFilterRequestsByStatus === true)
  .map(key => ({
    value: key,
    label: REQUESTS_STATUSES[key].ar,
  }));

const groupedRequestsStatusSelectOptions = requestsStatusKeys
  .filter(key => REQUESTS_STATUSES[key].useInFilterRequestsByGroupedStatus === true)
  .map(key => ({
    value: key,
    label: REQUESTS_STATUSES[key].ar,
  }));
const ticketStatusSelectOptions = ticketStatusKeys.map(key => ({
  value: key,
  label: TICKETS_STATUS[key].ar,
}));
const paymentStatusSelectOptions = paymentStatusKeys.map(key => ({
  value: key,
  label: PAYMENT_STATUS[key].ar,
}));
const paymentMethodSelectOptions = paymentMethodKeys.map(key => ({
  value: key,
  label: PAYMENT_METHOD[key].ar,
}));
// const transactionTypesSelectOptions = transactionTypesKeys.map(key => ({
//   value: key,
//   label: TRANSACTION_TYPES[key].ar,
// }));
const groupedTransactionTypesSelectOptions = groupedTransactionsTypesKeys.map(key => ({
  value: GROUPED_TRANSACTION_TYPES[key].query,
  label: GROUPED_TRANSACTION_TYPES[key].message.ar,
  code: key,
}));

// const transactionStatusSelectOptions = transactionStatusKeys.map(key => ({
//   value: key,
//   label: TRANSACTION_STATUS[key].ar,
// }));
const commissionRateSelectOptions = commissionRateKeys.map(key => ({
  value: key,
  label: COMMISSION_RATE[key].value,
}));
const transactionSignSelectOptions = transactionSignKeys.map(key => ({
  value: key,
  label: TRANSACTION_SIGN[key].ar,
}));
const activitiesLogsOptions = activitiesLogsKeys.map(key => ({
  value: key,
  label: ACTIVITIES_LOGS[key].ar,
}));

const FilterContainer = styled.div`
  ${bgColor};
  ${space};
  position: fixed;
  display: flex;
  opacity: ${props => (props.isOpened ? 1 : 0)};
  justify-content: center;
  align-items: center;
  direction: rtl;
  flex-direction: column;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  transition: all 200ms ease-out;
  z-index: ${props => (props.isOpened ? 999 : -1)};
`;

const FilterContent = styled(Card)`
  position: absolute;
  left: ${props => (props.isOpened ? 0 : '-380px')};
  height: 100vh;
  transition: all 400ms ease-out;
`;

const FilterOverFlowContent = styled(Flex)`
  position: absolute;
  left: ${props => (props.isOpened ? '380px' : 'calc(100% + 380px)')};
  height: 100vh;
  transition: all 400ms ease-out;
`;

const ScrollableModalContent = styled(Flex)`
  min-height: calc(100% - 12px);
  overflow-y: auto;
`;

const ModalHeader = styled(Flex)`
  position: fixed;
  z-index: 1;
  background-color: ${COLORS_VALUES[COLORS.GREY_MEDIUM]};
  border-bottom: 2px solid ${COLORS_VALUES[COLORS.GREY_LIGHT]};
`;

const CloseButton = styled(FontAwesomeIcon)`
  cursor: pointer;
`;

class Filter extends Component {
  static propTypes = {
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    isOpened: PropTypes.bool,
    toggleFilter: PropTypes.func,
  };

  static defaultProps = {
    header: '',
    isOpened: false,
    toggleFilter: () => {},
  };

  constructor(props) {
    super(props);

    const { isOpened, filterQueries, cachedFilterPage } = props;
    const cachedFilterState = JSON.parse(sessionStorage.getItem(`${cachedFilterPage}FilterState`));
    let filterState = {
      ticketNumber: undefined,
      requestNumber: undefined,
      vendorUsername: undefined,
      clientUsername: undefined,
      toDateForRequests: undefined,
      fromDateForRequests: undefined,
      requestsCount: undefined,
      clientEmail: undefined,
      mostUsed: undefined,
      mostCompletedRequests: undefined,
      workerUsername: undefined,
      governmentId: undefined,
      selectedWorkerRating: undefined,
      requestsCancellationReasonQueryKey: undefined,
      selectedCancellationReasons: null,
      promoCode: undefined,
      selectedFields: null,
      selectedActiveStatus: null,
      selectedCities: null,
      selectedDelay: null,
      selectedRejectInvoiceThenApproved: null,
      contractExpired: null,
      selectedActive: null,
      selectedSuspended: null,
      selectedPaymentMethodChanged: null,
      selectedFeatured: null,
      selectedElite: null,
      selectedRequestsStatus: null,
      selectedGroupedRequestsStatus: null,
      selectedComplainName: null,
      selectedTicketStatus: null,
      selectedPaymentStatus: null,
      selectedPaymentMethod: null,
      selectedTransactionType: null,
      selectedTransactionStatus: null,
      selectedTransactionClassification: null,
      selectedCommissionRate: null,
      selectedTransactionSign: null,
      minCreditsValue: undefined,
      maxCreditsValue: undefined,
      fromDateValue: undefined,
      toDateValue: undefined,
      selectedFollowUps: undefined,
      isForNewCustomer: undefined,
      selectedActivitiesLogs: undefined,
      adminUsername: undefined,
      filterQueries,
      selectedGroupedTransactionType: undefined,
      selectedVendors: undefined,
      selectedTransactionClassificationForGroupedTransactionType: null,
      isForWorkerHasResidenceInfo: undefined,
      isApprovedWorkerHasResidenceInfo: undefined,
    };
    let transactionClassificationForGroupedTransactionTypesSelectOptions = [];

    if (cachedFilterState) {
      const {
        ticketNumber,
        requestNumber,
        vendorUsername,
        clientUsername,
        toDateForRequests,
        fromDateForRequests,
        requestsCount,
        clientEmail,
        mostUsed,
        mostCompletedRequests,
        workerUsername,
        governmentId,
        selectedWorkerRating,
        promoCode,
        selectedFields,
        selectedActiveStatus,
        selectedCancellationReasons,
        selectedCities,
        selectedDelay,
        selectedRejectInvoiceThenApproved,
        contractExpired,
        selectedActive,
        selectedSuspended,
        selectedPaymentMethodChanged,
        selectedFeatured,
        selectedElite,
        selectedRequestsStatus,
        selectedGroupedRequestsStatus,
        selectedComplainName,
        selectedTicketStatus,
        selectedPaymentStatus,
        selectedPaymentMethod,
        selectedTransactionType,
        selectedTransactionStatus,
        selectedTransactionClassification,
        selectedVendors,
        selectedCommissionRate,
        selectedTransactionSign,
        minCreditsValue,
        maxCreditsValue,
        fromDateValue,
        toDateValue,
        filterQueries: cachedFilterQueries,
        selectedFollowUps,
        isForNewCustomer,
        selectedActivitiesLogs,
        adminUsername,
        selectedGroupedTransactionType,
        selectedTransactionClassificationForGroupedTransactionType,
        isForWorkerHasResidenceInfo,
        isApprovedWorkerHasResidenceInfo,
      } = cachedFilterState;

      filterState = {
        ticketNumber,
        requestNumber,
        vendorUsername,
        clientUsername,
        toDateForRequests,
        fromDateForRequests,
        requestsCount,
        clientEmail,
        mostUsed,
        mostCompletedRequests,
        workerUsername,
        governmentId,
        selectedWorkerRating,
        promoCode,
        selectedFields,
        selectedActiveStatus,
        selectedCancellationReasons,
        selectedCities,
        selectedDelay,
        selectedRejectInvoiceThenApproved,
        contractExpired,
        selectedActive,
        selectedSuspended,
        selectedPaymentMethodChanged,
        selectedFeatured,
        selectedElite,
        selectedRequestsStatus,
        selectedGroupedRequestsStatus,
        selectedComplainName,
        selectedTicketStatus,
        selectedPaymentStatus,
        selectedPaymentMethod,
        selectedTransactionType,
        selectedTransactionStatus,
        selectedTransactionClassification,
        selectedVendors,
        selectedCommissionRate,
        selectedTransactionSign,
        minCreditsValue,
        maxCreditsValue,
        fromDateValue,
        toDateValue,
        filterQueries: cachedFilterQueries,
        selectedFollowUps,
        isForNewCustomer,
        selectedActivitiesLogs,
        adminUsername,
        selectedGroupedTransactionType,
        selectedTransactionClassificationForGroupedTransactionType,
        isForWorkerHasResidenceInfo,
        isApprovedWorkerHasResidenceInfo,
      };

      transactionClassificationForGroupedTransactionTypesSelectOptions = selectedGroupedTransactionType
        ? CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES[
          selectedGroupedTransactionType.code
        ].map(transactionClassification => ({
          value: transactionClassification.code,
          label: `${transactionClassification.name.ar} - ${transactionClassification.code}`,
        }))
        : [];
    }

    this.state = {
      isOpened,
      ...filterState,
      transactionClassificationForGroupedTransactionTypesSelectOptions,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { filterQueries, cachedFilterPage } = this.props;
    if (nextProps.filterQueries !== filterQueries && cachedFilterPage === 'tickets') {
      this.setState({ filterQueries: nextProps.filterQueries });
    }
  }

  componentDidUpdate = prevProps => {
    const { isOpened, cachedFilterPage } = this.props;
    const { isOpened: isOpenedState, ...otherState } = this.state;

    if (prevProps.isOpened !== isOpened) {
      this.toggleFilter(isOpened);
    }

    if (cachedFilterPage !== prevProps.cachedFilterPage) {
      const cache = JSON.parse(sessionStorage.getItem(`${cachedFilterPage}FilterState`)) || {
        ticketNumber: '',
        requestNumber: '',
        vendorUsername: '',
        clientUsername: '',
        toDateForRequests: '',
        fromDateForRequests: '',
        requestsCount: '',
        clientEmail: '',
        mostUsed: '',
        mostCompletedRequests: '',
        workerUsername: '',
        governmentId: '',
        selectedWorkerRating: '',
        promoCode: '',
        selectedFields: null,
        selectedActiveStatus: null,
        selectedCancellationReasons: null,
        selectedCities: null,
        selectedDelay: null,
        selectedRejectInvoiceThenApproved: null,
        contractExpired: null,
        selectedActive: null,
        selectedSuspended: null,
        selectedPaymentMethodChanged: null,
        selectedFeatured: null,
        selectedElite: null,
        selectedRequestsStatus: null,
        selectedGroupedRequestsStatus: null,
        selectedComplainName: null,
        selectedTicketStatus: null,
        selectedPaymentStatus: null,
        selectedPaymentMethod: null,
        selectedTransactionType: null,
        selectedTransactionStatus: null,
        selectedTransactionClassification: null,
        selectedCommissionRate: null,
        selectedTransactionSign: null,
        minCreditsValue: '',
        maxCreditsValue: '',
        fromDateValue: '',
        toDateValue: '',
        selectedFollowUps: null,
        isForNewCustomer: '',
        selectedActivitiesLogs: null,
        adminUsername: '',
        isForWorkerHasResidenceInfo: undefined,
        isApprovedWorkerHasResidenceInfo: undefined,
        filterQueries: {
          skip: 0,
          limit: 20,
        },
        selectedGroupedTransactionType: null,
        selectedTransactionClassificationForGroupedTransactionType: null,
      };

      this.setState({
        ...cache,
      });
      return;
    }

    sessionStorage.setItem(`${cachedFilterPage}FilterState`, JSON.stringify(otherState));
  };

  toggleFilter = isOpened => {
    const { isOpened: isOpenedState } = this.state;

    if (isOpenedState !== isOpened) {
      this.setState({ isOpened });
    }
  };

  handleTicketNumberInputChange = ticketNumber => {
    const { filterFunction, ticketNumberQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedTicketNumber = convertArabicNumbersToEnglish(ticketNumber);

    this.setState({ ticketNumber }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [ticketNumberQueryKey]: convertedTicketNumber,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(ticketNumberQueryKey, convertedTicketNumber);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleRequestNumberInputChange = requestNumber => {
    const { filterFunction, requestNumberQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedRequestNumber = convertArabicNumbersToEnglish(requestNumber);

    this.setState({ requestNumber }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [requestNumberQueryKey]: convertedRequestNumber,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(requestNumberQueryKey, convertedRequestNumber);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleVendorUsernameInputChange = vendorUsername => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedVendorUsername = convertArabicNumbersToEnglish(vendorUsername);

    this.setState({ vendorUsername }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            vendorUsername: convertedVendorUsername,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries('vendorUsername', convertedVendorUsername);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleClientUsernameInputChange = clientUsername => {
    const { filterFunction, clientUsernameQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedClientUsername = convertArabicNumbersToEnglish(clientUsername);

    this.setState({ clientUsername }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [clientUsernameQueryKey]: convertedClientUsername,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(clientUsernameQueryKey, convertedClientUsername);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleRequestsCountInputChange = requestsCount => {
    const { filterFunction, requestsCountQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedRequestsCount = convertArabicNumbersToEnglish(requestsCount);

    this.setState({ requestsCount }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [requestsCountQueryKey]: convertedRequestsCount,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(requestsCountQueryKey, convertedRequestsCount);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleChangeClientEmailSelect = clientEmail => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ clientEmail }, () => {
      const clientEmailQuery = clientEmail ? clientEmail.value : '';

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            clientEmail: clientEmailQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('clientEmail', clientEmailQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeMostUsedSelect = mostUsed => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ mostUsed }, () => {
      const mostUsedQuery = mostUsed ? mostUsed.value : '';

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            mostUsed: mostUsedQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('mostUsed', mostUsedQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeMostCompletedRequestsSelect = mostCompletedRequests => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ mostCompletedRequests }, () => {
      const mostCompletedRequestsQuery = mostCompletedRequests ? mostCompletedRequests.value : '';

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            mostCompletedRequests: mostCompletedRequestsQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('mostCompletedRequests', mostCompletedRequestsQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleWorkerUsernameInputChange = workerUsername => {
    const { filterFunction, workerUsernameQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedWorkerUsername = convertArabicNumbersToEnglish(workerUsername);

    this.setState({ workerUsername }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [workerUsernameQueryKey]: convertedWorkerUsername,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(workerUsernameQueryKey, convertedWorkerUsername);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleGovernmentIdInputChange = governmentId => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedWorkerGovernmentId = convertArabicNumbersToEnglish(governmentId).toString();

    this.setState({ governmentId: convertedWorkerGovernmentId }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            governmentId: convertedWorkerGovernmentId,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries('governmentId', convertedWorkerGovernmentId);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleChangeWorkerRatingSelect = selectedWorkerRating => {
    const {
      filterFunction,
      selectedWorkerRating: workerRatingQueryKey,
      handleChangeFilterQueries,
    } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedWorkerRating }, () => {
      const rating = selectedWorkerRating ? selectedWorkerRating.value : '';

      this.setState(
        {
          filterQueries: { ...filterQueries, [workerRatingQueryKey]: rating, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(workerRatingQueryKey, rating);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handlePromoCodeInputChange = promoCode => {
    const { filterFunction, promoCodeQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ promoCode }, () => {
      this.setState(
        {
          filterQueries: { ...filterQueries, [promoCodeQueryKey]: promoCode, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(promoCodeQueryKey, promoCode);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleChangeRequestReasonsSelect = selectedCancellationReasons => {
    const {
      filterFunction,
      requestCancellationReasonsQueryKey,
      handleChangeFilterQueries,
    } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedCancellationReasons }, () => {
      const requestReasonsQuery = _.map(
        selectedCancellationReasons,
        field => field.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [requestCancellationReasonsQueryKey]: requestReasonsQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(requestCancellationReasonsQueryKey, requestReasonsQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeFieldsSelect = selectedFields => {
    const { filterFunction, fieldsQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedFields }, () => {
      const fieldQuery = _.map(selectedFields, field => field.value).toString();
      this.setState(
        {
          filterQueries: { ...filterQueries, [fieldsQueryKey]: fieldQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(fieldsQueryKey, fieldQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeActiveStatusSelect = selectedActiveStatus => {
    const { filterFunction, activeStatusQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedActiveStatus }, () => {
      const activeStatusQuery = _.get(selectedActiveStatus, 'value',''); 
      
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            active:activeStatusQuery,
            skip: 0,
            limit: 20
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('active', activeStatusQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeCitiesSelect = selectedCities => {
    const { filterFunction, citiesQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedCities }, () => {
      const cityQuery = _.map(selectedCities, city => city.value).toString();

      this.setState(
        {
          filterQueries: { ...filterQueries, [citiesQueryKey]: cityQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(citiesQueryKey, cityQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeDelaySelect = selectedDelay => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedDelay }, () => {
      const delayQuery = _.get(selectedDelay, 'value');

      this.setState(
        {
          filterQueries: { ...filterQueries, delayed: delayQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('delayed', delayQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeRejectInvoiceThenApprovedSelect = selectedRejectInvoiceThenApproved => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedRejectInvoiceThenApproved }, () => {
      const rejectInvoiceThenApprovedQuery = _.get(selectedRejectInvoiceThenApproved, 'value');

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            rejectInvoiceThenApproved: rejectInvoiceThenApprovedQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('rejectInvoiceThenApproved', rejectInvoiceThenApprovedQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeVendorContractExpiredSelect = selectedContractExpired => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    this.setState({ contractExpired: selectedContractExpired }, () => {
      const expiredQuery = selectedContractExpired ? selectedContractExpired.value.toString() : '';

      this.setState(
        {
          filterQueries: { ...filterQueries, expired: expiredQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('expired', expiredQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeActiveSelect = selectedActive => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedActive }, () => {
      const activeQuery = _.get(selectedActive, 'value', false);

      this.setState(
        {
          filterQueries: { ...filterQueries, active: activeQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('active', activeQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeSuspendedSelect = selectedSuspended => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedSuspended }, () => {
      const suspendedQuery = _.get(selectedSuspended, 'value', false);
      this.setState(
        {
          filterQueries: { ...filterQueries, suspended: suspendedQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('suspended', suspendedQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangePaymentMethodChangedSelect = selectedPaymentMethodChanged => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedPaymentMethodChanged }, () => {
      const paymentMethodChangedQuery = _.get(selectedPaymentMethodChanged, 'value', false);
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            paymentMethodChanged: paymentMethodChangedQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('paymentMethodChanged', paymentMethodChangedQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeFeaturedSelect = selectedFeatured => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedFeatured }, () => {
      const featuredQuery = _.get(selectedFeatured, 'value', false);

      this.setState(
        {
          filterQueries: { ...filterQueries, featured: featuredQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('featured', featuredQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeEliteSelect = selectedElite => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedElite }, () => {
      const eliteQuery = _.get(selectedElite, 'value', false);

      this.setState(
        {
          filterQueries: { ...filterQueries, elite: eliteQuery, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('elite', eliteQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeRequestsStatusSelect = selectedRequestsStatus => {
    const { filterFunction, requestsStatusQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedRequestsStatus }, () => {
      const { isRequestsStatusMulti } = this.props;
      let requestStatusQuery = '';

      if (isRequestsStatusMulti) {
        requestStatusQuery = selectedRequestsStatus
          ? selectedRequestsStatus.map(status => status.value).toString()
          : '';
      } else if (selectedRequestsStatus) {
        requestStatusQuery = selectedRequestsStatus.value;
      }

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [requestsStatusQueryKey]: requestStatusQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(requestsStatusQueryKey, requestStatusQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeGroupedRequestsStatusSelect = selectedGroupedRequestsStatus => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    this.setState({ selectedGroupedRequestsStatus }, () => {
      const requestStatusQuery = selectedGroupedRequestsStatus
        ? selectedGroupedRequestsStatus.value
        : '';

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            groupedStatuses: requestStatusQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('groupedStatuses', requestStatusQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeComplainNameSelect = selectedComplainName => {
    const { filterFunction, complainNameQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedComplainName }, () => {
      const complainNameQuery = _.map(selectedComplainName, status => status.value).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [complainNameQueryKey]: complainNameQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(complainNameQueryKey, complainNameQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeTicketStatusSelect = selectedTicketStatus => {
    const { filterFunction, ticketStatusQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedTicketStatus }, () => {
      const ticketStatusQuery = _.map(selectedTicketStatus, status => status.value).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [ticketStatusQueryKey]: ticketStatusQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(ticketStatusQueryKey, ticketStatusQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangePaymentStatusSelect = selectedPaymentStatus => {
    const { filterFunction, paymentStatusQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedPaymentStatus }, () => {
      const paymentStatusQuery = _.map(selectedPaymentStatus, status => status.value).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [paymentStatusQueryKey]: paymentStatusQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(paymentStatusQueryKey, paymentStatusQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangePaymentMethodSelect = selectedPaymentMethod => {
    const { filterFunction, paymentMethodQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedPaymentMethod }, () => {
      const paymentMethodQuery = _.map(selectedPaymentMethod, status => status.value).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [paymentMethodQueryKey]: paymentMethodQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(paymentMethodQueryKey, paymentMethodQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeTransactionTypesSelect = selectedTransactionType => {
    const { filterFunction, transactionTypeQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedTransactionType }, () => {
      const transactionTypeQuery = _.map(
        selectedTransactionType,
        status => status.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [transactionTypeQueryKey]: transactionTypeQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(transactionTypeQueryKey, transactionTypeQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeGroupedTransactionTypesSelect = selectedTransactionType => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    let transactionClassificationForGroupedTransactionTypesSelectOptions = [];

    if (selectedTransactionType) {
      transactionClassificationForGroupedTransactionTypesSelectOptions = CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES[
        selectedTransactionType.code
      ].map(transactionClassification => ({
        value: transactionClassification.code,
        label: `${transactionClassification.name.ar} - ${transactionClassification.code}`,
      }));
    }

    this.setState(
      {
        selectedGroupedTransactionType: selectedTransactionType,
        transactionClassificationForGroupedTransactionTypesSelectOptions,
        selectedTransactionClassificationForGroupedTransactionType: null,
        selectedTransactionClassification: null,
      },
      () => {
        const transactionTypeQuery = selectedTransactionType ? selectedTransactionType.value : '';
        this.setState(
          {
            filterQueries: {
              ...transactionTypeQuery,
              skip: 0,
              limit: 20,
            },
          },
          () => {
            const { filterQueries: filterQueriesNextState } = this.state;
            Object.keys(transactionTypeQuery).forEach(key => {
              handleChangeFilterQueries(key, transactionTypeQuery[key]);
            });
            filterFunction(filterQueriesNextState, true);
          },
        );
      },
    );
  };

  handleChangeTransactionStatusSelect = selectedTransactionStatus => {
    const { filterFunction, transactionStatusQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedTransactionStatus }, () => {
      const transactionStatusQuery = _.map(
        selectedTransactionStatus,
        status => status.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [transactionStatusQueryKey]: transactionStatusQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(transactionStatusQueryKey, transactionStatusQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeTransactionClassificationSelect = selectedTransactionClassification => {
    const {
      filterFunction,
      transactionClassificationQueryKey,
      handleChangeFilterQueries,
    } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedTransactionClassification }, () => {
      const transactionClassificationQuery = _.map(
        selectedTransactionClassification,
        status => status.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [transactionClassificationQueryKey]: transactionClassificationQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(
            transactionClassificationQueryKey,
            transactionClassificationQuery,
          );
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeTransactionClassificationForGroupedTransactionTypesSelect = selectedTransactionClassification => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState(
      {
        selectedTransactionClassificationForGroupedTransactionType: selectedTransactionClassification,
      },
      () => {
        const transactionClassificationQuery = _.map(
          selectedTransactionClassification,
          status => status.value,
        ).toString();

        this.setState(
          {
            filterQueries: {
              ...filterQueries,
              customTransactionClassificationCode: transactionClassificationQuery,
              skip: 0,
              limit: 20,
            },
          },
          () => {
            const { filterQueries: filterQueriesNextState } = this.state;

            handleChangeFilterQueries(
              'customTransactionClassificationCode',
              transactionClassificationQuery,
            );
            filterFunction(filterQueriesNextState, true);
          },
        );
      },
    );
  };

  handleChangeVendorSelect = selectedVendors => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedVendors }, () => {
      const vendorsQuery = _.map(selectedVendors, status => status.value).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            vendorsId: vendorsQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('vendorsId', vendorsQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeCommissionRateSelect = selectedCommissionRate => {
    const { filterFunction, commissionRateQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedCommissionRate }, () => {
      const commissionRateQuery = _.map(selectedCommissionRate, status => status.value).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [commissionRateQueryKey]: commissionRateQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(commissionRateQueryKey, commissionRateQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeTransactionSignSelect = selectedTransactionSign => {
    const { filterFunction, transactionSignQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedTransactionSign }, () => {
      const transactionSignQuery = _.map(
        selectedTransactionSign,
        status => status.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [transactionSignQueryKey]: transactionSignQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(transactionSignQueryKey, transactionSignQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleMinCreditsInputChange = minCreditsValue => {
    const { filterFunction, minCreditsQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedMinCredits = convertArabicNumbersToEnglish(minCreditsValue);

    this.setState({ minCreditsValue }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [minCreditsQueryKey]: convertedMinCredits,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(minCreditsQueryKey, convertedMinCredits);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleMaxCreditsInputChange = maxCreditsValue => {
    const { filterFunction, maxCreditsQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedMaxCredits = convertArabicNumbersToEnglish(maxCreditsValue);

    this.setState({ maxCreditsValue }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [maxCreditsQueryKey]: convertedMaxCredits,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(maxCreditsQueryKey, convertedMaxCredits);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleFromDateInputChange = fromDateValue => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const fromDateString = fromDateValue
      ? fromDateValue.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';

    this.setState({ fromDateValue: fromDateString }, () => {
      this.setState(
        {
          filterQueries: { ...filterQueries, fromDate: fromDateString, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('fromDate', fromDateString);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleToDateInputChange = toDateValue => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const toDateString = toDateValue
      ? toDateValue.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';

    this.setState({ toDateValue: toDateString }, () => {
      this.setState(
        {
          filterQueries: { ...filterQueries, toDate: toDateString, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('toDate', toDateString);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleFromDateForRequestsDateInputChange = fromDateValue => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const fromDateString = fromDateValue
      ? fromDateValue.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';

    this.setState({ fromDateForRequests: fromDateString }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            fromDateForRequests: fromDateString,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('fromDateForRequests', fromDateString);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleToDateForRequestsDateInputChange = toDateValue => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const toDateString = toDateValue
      ? toDateValue.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
      })
      : '';

    this.setState({ toDateForRequests: toDateString }, () => {
      this.setState(
        {
          filterQueries: { ...filterQueries, toDateForRequests: toDateString, skip: 0, limit: 20 },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries('toDateForRequests', toDateString);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeRequestFollowUpsSelect = selectedRequestFollowUps => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedFollowUps: selectedRequestFollowUps }, () => {
      const requestFollowedUpsQuery = _.map(
        selectedRequestFollowUps,
        followUps => followUps.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            requestFollowedUps: requestFollowedUpsQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;
          handleChangeFilterQueries('requestFollowedUps', requestFollowedUpsQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeIsForNewCustomerSelect = selectedIsForNewCustomer => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ isForNewCustomer: selectedIsForNewCustomer }, () => {
      const isForNewCustomerQuery = _.map(
        selectedIsForNewCustomer,
        IsForNewCustomer => IsForNewCustomer.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            isForNewCustomer: isForNewCustomerQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;
          handleChangeFilterQueries('isForNewCustomer', isForNewCustomerQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeActivitiesLogsSelect = selectedActivitiesLogs => {
    const { filterFunction, activitiesLogsQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ selectedActivitiesLogs }, () => {
      const activitiesLogsQuery = _.map(
        selectedActivitiesLogs,
        activity => activity.value,
      ).toString();

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [activitiesLogsQueryKey]: activitiesLogsQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          handleChangeFilterQueries(activitiesLogsQueryKey, activitiesLogsQuery);
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleAdminUsernameInputChange = adminUsername => {
    const { filterFunction, adminUsernameQueryKey, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;
    const convertedAdminUsername = convertArabicNumbersToEnglish(adminUsername);

    this.setState({ adminUsername }, () => {
      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            [adminUsernameQueryKey]: convertedAdminUsername,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;

          setTimeout(() => {
            handleChangeFilterQueries(adminUsernameQueryKey, convertedAdminUsername);
            filterFunction(filterQueriesNextState, true);
          }, 1000);
        },
      );
    });
  };

  handleChangeIsForWorkerHasResidenceInfoSelect = selectedIsForWorkerHasResidenceInfo => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState({ isForWorkerHasResidenceInfo: selectedIsForWorkerHasResidenceInfo }, () => {
      const isForWorkerHasResidenceInfoQuery = selectedIsForWorkerHasResidenceInfo
        ? selectedIsForWorkerHasResidenceInfo.value
        : '';

      this.setState(
        {
          filterQueries: {
            ...filterQueries,
            isForWorkerHasResidenceInfo: isForWorkerHasResidenceInfoQuery,
            skip: 0,
            limit: 20,
          },
        },
        () => {
          const { filterQueries: filterQueriesNextState } = this.state;
          handleChangeFilterQueries(
            'isForWorkerHasResidenceInfo',
            isForWorkerHasResidenceInfoQuery,
          );
          filterFunction(filterQueriesNextState, true);
        },
      );
    });
  };

  handleChangeIsApprovedWorkerHasResidenceInfoSelect = selectedIsApprovedWorkerHasResidenceInfo => {
    const { filterFunction, handleChangeFilterQueries } = this.props;
    const { filterQueries } = this.state;

    this.setState(
      { isApprovedWorkerHasResidenceInfo: selectedIsApprovedWorkerHasResidenceInfo },
      () => {
        const isApprovedWorkerHasResidenceInfoQuery = selectedIsApprovedWorkerHasResidenceInfo
          ? selectedIsApprovedWorkerHasResidenceInfo.value
          : '';

        this.setState(
          {
            filterQueries: {
              ...filterQueries,
              isApprovedWorkerHasResidenceInfo: isApprovedWorkerHasResidenceInfoQuery,
              skip: 0,
              limit: 20,
            },
          },
          () => {
            const { filterQueries: filterQueriesNextState } = this.state;
            handleChangeFilterQueries(
              'isApprovedWorkerHasResidenceInfo',
              isApprovedWorkerHasResidenceInfoQuery,
            );
            filterFunction(filterQueriesNextState, true);
          },
        );
      },
    );
  };

  render() {
    const {
      header,
      toggleFilter,
      filterSections,
      transactionsClassification,
      complainsList,
      creditsTitle,
      vendorsList,
      isRequestsStatusMulti,
    } = this.props;
    const {
      ticketNumber,
      requestNumber,
      vendorUsername,
      clientUsername,
      fromDateForRequests,
      toDateForRequests,
      requestsCount,
      clientEmail,
      mostUsed,
      mostCompletedRequests,
      workerUsername,
      governmentId,
      selectedWorkerRating,
      promoCode,
      selectedFields,
      selectedActiveStatus,
      selectedCancellationReasons,
      selectedCities,
      selectedDelay,
      selectedRejectInvoiceThenApproved,
      contractExpired,
      selectedActive,
      selectedSuspended,
      selectedPaymentMethodChanged,
      selectedFeatured,
      selectedElite,
      minCreditsValue,
      maxCreditsValue,
      fromDateValue,
      toDateValue,
      selectedRequestsStatus,
      selectedGroupedRequestsStatus,
      selectedComplainName,
      selectedTicketStatus,
      selectedPaymentMethod,
      selectedPaymentStatus,
      // selectedTransactionType,
      // selectedTransactionStatus,
      selectedTransactionClassification,
      selectedCommissionRate,
      selectedTransactionSign,
      isOpened,
      selectedFollowUps,
      isForNewCustomer,
      selectedActivitiesLogs,
      adminUsername,
      selectedGroupedTransactionType,
      selectedTransactionClassificationForGroupedTransactionType,
      transactionClassificationForGroupedTransactionTypesSelectOptions,
      selectedVendors,
      isForWorkerHasResidenceInfo,
      isApprovedWorkerHasResidenceInfo,
    } = this.state;
    const showTicketNumberSection = filterSections.includes('ticketNumber');
    const showRequestNumberSection = filterSections.includes('requestNumber');
    const showVendorUsernameSection = filterSections.includes('vendorUsername');
    const showClientUsernameSection = filterSections.includes('clientUsername');
    const showRequestsDatesSection = filterSections.includes('requestsDates');
    const showRequestsCountSection = filterSections.includes('requestsCount');
    const showClientEmailSection = filterSections.includes('clientEmail');
    const showMostUsedSection = filterSections.includes('mostUsed');
    const showMostCompletedRequestsSection = filterSections.includes('mostCompletedRequests');
    const showWorkerUsernameSection = filterSections.includes('workerUsername');
    const showWorkerGovernmentIdSection = filterSections.includes('governmentId');
    const showWorkerRatingSection = filterSections.includes('selectedWorkerRating');
    const showPromoCodeSection = filterSections.includes('promoCode');
    const showDatesSection = filterSections.includes('dates');
    const showFieldSection = filterSections.includes('field');
    const showActiveStatus = filterSections.includes('activeStatus');
    const showCancellationReasonsSection = filterSections.includes('requestCancellationReasons');
    const showCitySection = filterSections.includes('city');
    const showDelaySection = filterSections.includes('delay');
    const showRejectInvoiceThenApprovedSection = filterSections.includes(
      'rejectInvoiceThenApproved',
    );
    const showContractExpiredSection = filterSections.includes('contractExpired');
    const showActiveSection = filterSections.includes('active');
    const showSuspendedSection = filterSections.includes('suspended');
    const showPaymentMethodChangedSection = filterSections.includes('paymentMethodChanged');
    const showFeaturedSection = filterSections.includes('featured');
    const showEliteSection = filterSections.includes('elite');
    const showCreditsSection = filterSections.includes('credits');
    const showRequestsStatusSection = filterSections.includes('requestsStatus');
    const showGroupedRequestsStatusSection = filterSections.includes('groupedRequestsStatus');
    const showComplainNameSection = filterSections.includes('complainName');
    const showTicketStatusSection = filterSections.includes('ticketStatus');
    const showPaymentStatusSection = filterSections.includes('paymentStatus');
    const showPaymentMethodSection = filterSections.includes('paymentMethod');
    // const showTransactionTypesSection = filterSections.includes('transactionTypes');
    // const showTransactionStatusSection = filterSections.includes('transactionStatus');
    const showTransactionClassificationSection = filterSections.includes(
      'transactionClassification',
    );
    const showVendorsSection = filterSections.includes('vendors');
    const showCommissionRateSection = filterSections.includes('commissionRate');
    const showTransactionSignSection = filterSections.includes('transactionSign');
    const showRequestFollowUpsSection = filterSections.includes('requestFollowUps');
    const showIsForNewCustomerSection = filterSections.includes('isForNewCustomer');
    const transactionClassificationSelectOptions =
      transactionsClassification &&
      transactionsClassification.workerClassifications.map(transactionClassification => ({
        value: transactionClassification.code,
        label: `${transactionClassification.name.ar} - ${transactionClassification.code}`,
      }));

    const vendorsSelectOptions =
      vendorsList &&
      vendorsList.map(vendor => ({
        value: vendor._id,
        label: `${vendor.name}`,
      }));
    const complainsSelectOptions =
      complainsList &&
      complainsList.map(complain => ({
        value: complain.value,
        label: complain.label,
      }));
    const showActivitiesLogsSection = filterSections.includes('activitiesLogs');
    const showAdminUsernameSection = filterSections.includes('adminUsername');
    const showGroupedTransactionTypesSection = filterSections.includes('groupedTransactionTypes');
    const showTransactionClassificationForGroupedTransactionTypesSection = filterSections.includes(
      'transactionClassificationForGroupedTransactionTypes',
    );

    const showIsForWorkerHasResidenceInfoSection = filterSections.includes(
      'isForWorkerHasResidenceInfo',
    );

    const showIsApprovedWorkerHasResidenceInfoSection = filterSections.includes(
      'isApprovedWorkerHasResidenceInfo',
    );

    return (
      <FilterContainer isOpened={isOpened} bg={WHITE_TRANSPARENT_BACKGROUND}>
        <FilterContent
          width={['280px', '380px', '380px', '380px', '380px']}
          isOpened={isOpened}
          flexDirection="column"
        >
          <ModalHeader
            header={header}
            width={['280px', '380px', '380px', '380px', '380px']}
            p={4}
            flexDirection="row-reverse"
            justifyContent="space-between"
          >
            <CloseButton
              color={COLORS_VALUES[DISABLED]}
              size="lg"
              icon={faTimes}
              onClick={() => toggleFilter()}
            />
            {header && (
              <Text
                textAlign="right"
                color={COLORS_VALUES[DISABLED]}
                type={TITLE}
                fontWeight={NORMAL}
              >
                {header}
              </Text>
            )}
          </ModalHeader>
          <ScrollableModalContent pt="56px" px={2} mt={3}>
            <Box width={1}>
              {showTicketNumberSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم المشكلة
                  </Text>
                  <FilterInput
                    value={ticketNumber}
                    handleInputChange={this.handleTicketNumberInputChange}
                    inputType="text"
                    inputPlaceholder="رقم المشكلة"
                  />
                </Flex>
              )}
              {showRequestNumberSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم الطلب
                  </Text>
                  <FilterInput
                    value={requestNumber}
                    handleInputChange={this.handleRequestNumberInputChange}
                    inputType="text"
                    inputPlaceholder="رقم الطلب"
                  />
                </Flex>
              )}
              {showVendorUsernameSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم جوال الشريك
                  </Text>
                  <FilterInput
                    value={vendorUsername}
                    handleInputChange={this.handleVendorUsernameInputChange}
                    inputType="text"
                    inputPlaceholder="رقم جوال الشريك"
                  />
                </Flex>
              )}
              {showClientUsernameSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم جوال العميل
                  </Text>
                  <FilterInput
                    value={clientUsername}
                    handleInputChange={this.handleClientUsernameInputChange}
                    inputType="text"
                    inputPlaceholder="رقم جوال العميل"
                  />
                </Flex>
              )}

              {showClientEmailSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={clientEmail}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeClientEmailSelect}
                  selectLabel="العميل لديه ايميل"
                />
              )}
              {showMostUsedSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={mostUsed}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeMostUsedSelect}
                  selectLabel="الاكثر استخداما"
                />
              )}
              {showMostCompletedRequestsSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={mostCompletedRequests}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeMostCompletedRequestsSelect}
                  selectLabel="الاكتر طلبات مكتملة"
                />
              )}
              {showWorkerUsernameSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم جوال الفني
                  </Text>
                  <FilterInput
                    value={workerUsername}
                    handleInputChange={this.handleWorkerUsernameInputChange}
                    inputType="text"
                    inputPlaceholder="رقم جوال الفني"
                  />
                </Flex>
              )}
              {showWorkerGovernmentIdSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم الهوية
                  </Text>
                  <FilterInput
                    value={governmentId}
                    handleInputChange={this.handleGovernmentIdInputChange}
                    inputType="text"
                    inputPlaceholder="رقم الهوية"
                  />
                </Flex>
              )}
              {showRequestsCountSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    عدد الطلبات
                  </Text>
                  <FilterInput
                    value={requestsCount}
                    handleInputChange={this.handleRequestsCountInputChange}
                    inputType="text"
                    inputPlaceholder="عدد الطلبات"
                  />
                </Flex>
              )}
              {showWorkerRatingSection && (
                <WorkerRatingSelect
                  selectedWorkerRating={selectedWorkerRating}
                  workerRatingSelectOptions={workerRatingSelectOptions}
                  handleChangeWorkerRatingSelect={this.handleChangeWorkerRatingSelect}
                />
              )}
              {showPromoCodeSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    البروموكود
                  </Text>
                  <FilterInput
                    value={promoCode}
                    handleInputChange={this.handlePromoCodeInputChange}
                    inputType="text"
                    inputPlaceholder="البروموكود"
                  />
                </Flex>
              )}
              {showFieldSection && (
                <FieldsSelect
                  selectedFields={selectedFields}
                  fieldsSelectOptions={fieldsSelectOptions}
                  handleChangeFieldsSelect={this.handleChangeFieldsSelect}
                />
              )}

              {showActiveStatus && (
                <ActiveStatusSelect
                selectedActiveStatus={selectedActiveStatus}
                activeStatusSelectOptions={activeStatusSelectOptions}
                handleChangeActiveStatusSelect={this.handleChangeActiveStatusSelect}
              />
              )}

              {showCitySection && (
                <CitiesSelect
                  selectedCities={selectedCities}
                  citiesSelectOptions={citiesSelectOptions}
                  handleChangeCitiesSelect={this.handleChangeCitiesSelect}
                />
              )}
              {showDelaySection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedDelay}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeDelaySelect}
                  selectLabel="متأخر"
                />
              )}
              {showRejectInvoiceThenApprovedSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedRejectInvoiceThenApproved}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeRejectInvoiceThenApprovedSelect}
                  selectLabel="رفض الفاتورة ثم وافق"
                />
              )}
              {showContractExpiredSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={contractExpired}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeVendorContractExpiredSelect}
                  selectLabel="العقد منتهى"
                />
              )}
              {showActiveSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedActive}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeActiveSelect}
                  selectLabel="نشط"
                />
              )}
              {showSuspendedSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedSuspended}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeSuspendedSelect}
                  selectLabel="متوقف"
                />
              )}
              {showPaymentMethodChangedSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedPaymentMethodChanged}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangePaymentMethodChangedSelect}
                  selectLabel="تغيرت وسيلة الدفع"
                />
              )}
              {showFeaturedSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedFeatured}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeFeaturedSelect}
                  selectLabel="متميز"
                />
              )}
              {showEliteSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={selectedElite}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeEliteSelect}
                  selectLabel="من النخبة"
                />
              )}
              {showRequestsStatusSection && (
                <RequestsStatusSelect
                  placeholder="أنواع الطلبات"
                  isMulti={isRequestsStatusMulti}
                  selectedRequestsStatus={selectedRequestsStatus}
                  requestsStatusSelectOptions={requestsStatusSelectOptions}
                  handleChangeRequestsStatusSelect={this.handleChangeRequestsStatusSelect}
                />
              )}
              {showCancellationReasonsSection && (
                <RequestReasonsSelect
                  selectedReasons={selectedCancellationReasons}
                  reasonsSelectOptions={cancellationReasonsSelectOptions}
                  handleChangeRequestReasonsSelect={this.handleChangeRequestReasonsSelect}
                />
              )}
              {showGroupedRequestsStatusSection && (
                <RequestsStatusSelect
                  placeholder="تتبع الطلبات"
                  isMulti={false}
                  selectedRequestsStatus={selectedGroupedRequestsStatus}
                  requestsStatusSelectOptions={groupedRequestsStatusSelectOptions}
                  handleChangeRequestsStatusSelect={this.handleChangeGroupedRequestsStatusSelect}
                />
              )}
              {showTicketStatusSection && (
                <TicketStatusSelect
                  selectedTicketStatus={selectedTicketStatus}
                  ticketStatusSelectOptions={ticketStatusSelectOptions}
                  handleChangeTicketStatusSelect={this.handleChangeTicketStatusSelect}
                />
              )}
              {showComplainNameSection && (
                <ComplainNameSelect
                  selectedComplainName={selectedComplainName}
                  complainsSelectOptions={complainsSelectOptions}
                  handleChangeComplainNameSelect={this.handleChangeComplainNameSelect}
                />
              )}
              {showPaymentMethodSection && (
                <PaymentMethodsSelect
                  selectedPaymentMethod={selectedPaymentMethod}
                  paymentMethodSelectOptions={paymentMethodSelectOptions}
                  handleChangePaymentMethodSelect={this.handleChangePaymentMethodSelect}
                />
              )}
              {showPaymentStatusSection && (
                <PaymentStatusSelect
                  selectedPaymentStatus={selectedPaymentStatus}
                  paymentStatusSelectOptions={paymentStatusSelectOptions}
                  handleChangePaymentStatusSelect={this.handleChangePaymentStatusSelect}
                />
              )}
              {showGroupedTransactionTypesSection && (
                <TransactionTypesSelect
                  labelText="انواع المعاملات"
                  isMulti={false}
                  selectedTransactionType={selectedGroupedTransactionType}
                  transactionTypesSelectOptions={groupedTransactionTypesSelectOptions}
                  handleChangeTransactionTypesSelect={
                    this.handleChangeGroupedTransactionTypesSelect
                  }
                />
              )}
              {showTransactionClassificationForGroupedTransactionTypesSection && (
                <TransactionClassificationSelect
                  selectedTransactionClassification={
                    selectedTransactionClassificationForGroupedTransactionType
                  }
                  transactionClassificationSelectOptions={
                    transactionClassificationForGroupedTransactionTypesSelectOptions
                  }
                  handleChangeTransactionClassificationSelect={
                    this.handleChangeTransactionClassificationForGroupedTransactionTypesSelect
                  }
                />
              )}
              {/* {showTransactionTypesSection && ( */}
              {/*  <TransactionTypesSelect */}
              {/*    labelText="أنواع السجل" */}
              {/*    isMulti */}
              {/*    selectedTransactionType={selectedTransactionType} */}
              {/*    transactionTypesSelectOptions={transactionTypesSelectOptions} */}
              {/*    handleChangeTransactionTypesSelect={this.handleChangeTransactionTypesSelect} */}
              {/*  /> */}
              {/* )} */}
              {/* {showTransactionStatusSection && ( */}
              {/*  <TransactionStatusSelect */}
              {/*    selectedTransactionStatus={selectedTransactionStatus} */}
              {/*    transactionStatusSelectOptions={transactionStatusSelectOptions} */}
              {/*    handleChangeTransactionStatusSelect={this.handleChangeTransactionStatusSelect} */}
              {/*  /> */}
              {/* )} */}
              {showTransactionClassificationSection && (
                <TransactionClassificationSelect
                  selectedTransactionClassification={selectedTransactionClassification}
                  transactionClassificationSelectOptions={transactionClassificationSelectOptions}
                  handleChangeTransactionClassificationSelect={
                    this.handleChangeTransactionClassificationSelect
                  }
                />
              )}
              {showVendorsSection && (
                <VendorsSelect
                  selectedVendors={selectedVendors}
                  vendorsSelectOptions={vendorsSelectOptions}
                  handleChangeVendorsSelect={this.handleChangeVendorSelect}
                />
              )}
              {showCommissionRateSection && (
                <CommissionRateSelect
                  selectedCommissionRate={selectedCommissionRate}
                  commissionRateSelectOptions={commissionRateSelectOptions}
                  handleChangeCommissionRateSelect={this.handleChangeCommissionRateSelect}
                />
              )}
              {showTransactionSignSection && (
                <TransactionSignSelect
                  selectedTransactionSign={selectedTransactionSign}
                  transactionSignSelectOptions={transactionSignSelectOptions}
                  handleChangeTransactionSignSelect={this.handleChangeTransactionSignSelect}
                />
              )}
              {showCreditsSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    {creditsTitle}
                  </Text>
                  <Flex width={1}>
                    <FilterInput
                      value={minCreditsValue}
                      handleInputChange={this.handleMinCreditsInputChange}
                      inputType="text"
                      inputPlaceholder="من"
                    />
                    <FilterInput
                      value={maxCreditsValue}
                      handleInputChange={this.handleMaxCreditsInputChange}
                      inputType="text"
                      inputPlaceholder="إلي"
                    />
                  </Flex>
                </Flex>
              )}
              {showDatesSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    التاريخ
                  </Text>
                  <Flex width={1}>
                    <DatePicker
                      value={fromDateValue}
                      handleDatePickerChange={this.handleFromDateInputChange}
                      placeholder="MM/DD/YYYY"
                    />
                    <DatePicker
                      value={toDateValue}
                      handleDatePickerChange={this.handleToDateInputChange}
                      placeholder="MM/DD/YYYY"
                    />
                  </Flex>
                </Flex>
              )}

              {showRequestsDatesSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    تاريخ انشاء الطلب
                  </Text>
                  <Flex width={1}>
                    <DatePicker
                      value={fromDateForRequests}
                      handleDatePickerChange={this.handleFromDateForRequestsDateInputChange}
                      placeholder="MM/DD/YYYY"
                    />
                    <DatePicker
                      value={toDateForRequests}
                      handleDatePickerChange={this.handleToDateForRequestsDateInputChange}
                      placeholder="MM/DD/YYYY"
                    />
                  </Flex>
                </Flex>
              )}

              {showRequestFollowUpsSection && (
                <ConfirmSelect
                  isMulti
                  selectedValues={selectedFollowUps}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeRequestFollowUpsSelect}
                  selectLabel="تم متابعته"
                />
              )}
              {showIsForNewCustomerSection && (
                <ConfirmSelect
                  isMulti
                  selectedValues={isForNewCustomer}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeIsForNewCustomerSelect}
                  selectLabel="هل مخصص للعملاء الجداد"
                />
              )}
              {showActivitiesLogsSection && (
                <ActivitiesLogsSelect
                  selectedActivitiesLogs={selectedActivitiesLogs}
                  activitiesLogsOptions={activitiesLogsOptions}
                  handleChangeActivitiesLogsSelect={this.handleChangeActivitiesLogsSelect}
                />
              )}
              {showAdminUsernameSection && (
                <Flex width={1} flexDirection="column" mb={2}>
                  <Text
                    mx={2}
                    mb={4}
                    type={SUBHEADING}
                    color={COLORS_VALUES[DISABLED]}
                    fontWeight={NORMAL}
                  >
                    رقم جوال الادمن
                  </Text>
                  <FilterInput
                    value={adminUsername}
                    handleInputChange={this.handleAdminUsernameInputChange}
                    inputType="text"
                    inputPlaceholder="رقم جوال الادمن"
                  />
                </Flex>
              )}
              {showIsForWorkerHasResidenceInfoSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={isForWorkerHasResidenceInfo}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeIsForWorkerHasResidenceInfoSelect}
                  selectLabel="تم ادخال بيانات الاقامة"
                />
              )}
              {showIsApprovedWorkerHasResidenceInfoSection && (
                <ConfirmSelect
                  isMulti={false}
                  selectedValues={isApprovedWorkerHasResidenceInfo}
                  selectOptions={confirmChoiceSelectOptions}
                  handleChangeSelect={this.handleChangeIsApprovedWorkerHasResidenceInfoSelect}
                  selectLabel="تم الموافقة على بيانات الاقامة"
                />
              )}
            </Box>
          </ScrollableModalContent>
        </FilterContent>
        <FilterOverFlowContent
          isOpened={isOpened}
          onClick={() => toggleFilter()}
          width={[
            'calc(100% - 280px)',
            'calc(100% - 380px)',
            'calc(100% - 380px)',
            'calc(100% - 380px)',
            'calc(100% - 380px)',
          ]}
        />
      </FilterContainer>
    );
  }
}

Filter.displayName = 'Filter';

Filter.propTypes = {
  cachedFilterPage: PropTypes.string.isRequired,
  filterFunction: PropTypes.func.isRequired,
  handleChangeFilterQueries: PropTypes.func,
  filterSections: PropTypes.arrayOf(
    PropTypes.oneOf([
      'ticketNumber',
      'requestNumber',
      'vendorUsername',
      'clientUsername',
      'requestsCount',
      'workerUsername',
      'governmentId',
      'selectedWorkerRating',
      'promoCode',
      'field',
      'activeStatus',
      'city',
      'delay',
      'rejectInvoiceThenApproved',
      'active',
      'suspended',
      'paymentMethodChanged',
      'featured',
      'elite',
      'requestsStatus',
      'groupedRequestsStatus',
      'ticketStatus',
      'complainName',
      'paymentMethod',
      'paymentStatus',
      'transactionTypes',
      'transactionStatus',
      'transactionClassification',
      'commissionRate',
      'transactionSign',
      'credits',
      'dates',
      'requestFollowUps',
      'isForNewCustomer',
      'activitiesLogs',
      'adminUsername',
      'groupedTransactionTypes',
      'transactionClassificationForGroupedTransactionTypes',
      'vendors',
      'contractExpired',
      'clientEmail',
      'requestsCount',
      'isForWorkerHasResidenceInfo',
      'isApprovedWorkerHasResidenceInfo',
    ]),
  ).isRequired,
  filterQueries: PropTypes.shape({}).isRequired,
  ticketNumberQueryKey: PropTypes.string,
  requestNumberQueryKey: PropTypes.string,
  requestCancellationReasonsQueryKey: PropTypes.string,
  fieldsQueryKey: PropTypes.string,
  activeStatusQueryKey: PropTypes.string,
  citiesQueryKey: PropTypes.string,
  clientUsernameQueryKey: PropTypes.string,
  requestsCountQueryKey: PropTypes.string,
  workerUsernameQueryKey: PropTypes.string,
  selectedWorkerRating: PropTypes.string,
  promoCodeQueryKey: PropTypes.string,
  requestsStatusQueryKey: PropTypes.string,
  ticketStatusQueryKey: PropTypes.string,
  paymentStatusQueryKey: PropTypes.string,
  paymentMethodQueryKey: PropTypes.string,
  transactionTypeQueryKey: PropTypes.string,
  transactionStatusQueryKey: PropTypes.string,
  transactionsClassification: PropTypes.shape({}),
  transactionClassificationQueryKey: PropTypes.string,
  complainsList: PropTypes.arrayOf(PropTypes.string),
  complainNameQueryKey: PropTypes.string,
  commissionRateQueryKey: PropTypes.string,
  transactionSignQueryKey: PropTypes.string,
  creditsTitle: PropTypes.string,
  minCreditsQueryKey: PropTypes.string,
  maxCreditsQueryKey: PropTypes.string,
  activitiesLogsQueryKey: PropTypes.string,
  adminUsernameQueryKey: PropTypes.string,
  selectedFields: PropTypes.arrayOf(PropTypes.shape({})),
  vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
  isRequestsStatusMulti: PropTypes.bool,
};

Filter.defaultProps = {
  handleChangeFilterQueries: () => {},
  ticketNumberQueryKey: 'prettyId',
  requestNumberQueryKey: 'requestNumber',
  requestCancellationReasonsQueryKey: 'requestCancellationReasons',
  fieldsQueryKey: 'field',
  activeStatusQueryKey: 'activeStatus',
  citiesQueryKey: 'city',
  clientUsernameQueryKey: 'partialUsername',
  requestsCountQueryKey: 'requestsCount',
  workerUsernameQueryKey: 'partialUsername',
  selectedWorkerRating: 'selectedWorkerRating',
  promoCodeQueryKey: 'promoCode',
  requestsStatusQueryKey: 'statuses',
  ticketStatusQueryKey: 'ticketStatus',
  paymentStatusQueryKey: 'paymentStatus',
  paymentMethodQueryKey: 'paymentMethod',
  transactionTypeQueryKey: 'types',
  transactionStatusQueryKey: 'status',
  transactionsClassification: undefined,
  transactionClassificationQueryKey: 'customTransactionClassificationCode',
  complainsList: undefined,
  complainNameQueryKey: 'complainName',
  commissionRateQueryKey: 'commissionRate',
  transactionSignQueryKey: 'sign',
  creditsTitle: 'الرصيد',
  minCreditsQueryKey: 'min_credits',
  maxCreditsQueryKey: 'max_credits',
  activitiesLogsQueryKey: 'action',
  adminUsernameQueryKey: 'username',
  selectedFields: [],
  vendorsList: undefined,
  isRequestsStatusMulti: true,
};

export default Filter;
