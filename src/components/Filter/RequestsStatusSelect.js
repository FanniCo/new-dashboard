import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const RequestsStatusSelect = props => {
  const {
    selectedRequestsStatus,
    handleChangeRequestsStatusSelect,
    requestsStatusSelectOptions,
    isMulti,
    placeholder,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        {placeholder}
      </Text>
      <StyledSelect
        mb={3}
        placeholder={placeholder}
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti={isMulti}
        isRtl
        backspaceRemovesValue={false}
        value={selectedRequestsStatus}
        onChange={handleChangeRequestsStatusSelect}
        options={requestsStatusSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
RequestsStatusSelect.displayName = 'RequestsStatusSelect';

RequestsStatusSelect.propTypes = {
  requestsStatusSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  // selectedRequestsStatus: PropTypes.any(),
  handleChangeRequestsStatusSelect: PropTypes.func.isRequired,
  isMulti: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired,
};

RequestsStatusSelect.defaultProps = {
  // selectedRequestsStatus: undefined,
};

export default RequestsStatusSelect;
