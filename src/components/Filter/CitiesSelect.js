import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const CitiesSelect = props => {
  const { selectedCities, handleChangeCitiesSelect, citiesSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        المدينة
      </Text>
      <StyledSelect
        mb={3}
        placeholder="المدينة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedCities}
        onChange={handleChangeCitiesSelect}
        options={citiesSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
CitiesSelect.displayName = 'CitiesSelect';

CitiesSelect.propTypes = {
  citiesSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedCities: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeCitiesSelect: PropTypes.func.isRequired,
};

CitiesSelect.defaultProps = {
  selectedCities: undefined,
};

export default CitiesSelect;
