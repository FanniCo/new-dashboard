import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const VendorsSelect = props => {
  const { selectedVendors, handleChangeVendorsSelect, vendorsSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        الشركاء
      </Text>
      <StyledSelect
        mb={3}
        placeholder="الشركاء"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedVendors}
        onChange={handleChangeVendorsSelect}
        options={vendorsSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
VendorsSelect.displayName = 'VendorsSelect';

VendorsSelect.propTypes = {
  vendorsSelectOptions: PropTypes.arrayOf(PropTypes.shape({})),
  selectedVendors: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeVendorsSelect: PropTypes.func.isRequired,
};

VendorsSelect.defaultProps = {
  vendorsSelectOptions: undefined,
  selectedVendors: undefined,
};

export default VendorsSelect;
