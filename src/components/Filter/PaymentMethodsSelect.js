import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const PaymentMethodsSelect = props => {
  const {
    selectedPaymentMethod,
    handleChangePaymentMethodSelect,
    paymentMethodSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        طريقة الدفع
      </Text>
      <StyledSelect
        mb={3}
        placeholder="طريقة الدفع"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedPaymentMethod}
        onChange={handleChangePaymentMethodSelect}
        options={paymentMethodSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
PaymentMethodsSelect.displayName = 'PaymentMethodsSelect';

PaymentMethodsSelect.propTypes = {
  paymentMethodSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedPaymentMethod: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangePaymentMethodSelect: PropTypes.func.isRequired,
};

PaymentMethodsSelect.defaultProps = {
  selectedPaymentMethod: undefined,
};

export default PaymentMethodsSelect;
