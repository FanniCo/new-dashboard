import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const TransactionSignSelect = props => {
  const {
    selectedTransactionSign,
    handleChangeTransactionSignSelect,
    transactionSignSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        الإشارة
      </Text>
      <StyledSelect
        mb={3}
        placeholder="الإشارة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedTransactionSign}
        onChange={handleChangeTransactionSignSelect}
        options={transactionSignSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
TransactionSignSelect.displayName = 'TransactionSignSelect';

TransactionSignSelect.propTypes = {
  transactionSignSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedTransactionSign: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeTransactionSignSelect: PropTypes.func.isRequired,
};

TransactionSignSelect.defaultProps = {
  selectedTransactionSign: undefined,
};

export default TransactionSignSelect;
