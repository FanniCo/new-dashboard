import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';
const ActiveStatusSelect = props => {

  const {
    selectedActiveStatus,
    handleChangeActiveStatusSelect,
    activeStatusSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;



  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        الحالة
      </Text>
      <StyledSelect
        mb={3}
        placeholder="الحالة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        backspaceRemovesValue={false}
        value={selectedActiveStatus}
        onChange={handleChangeActiveStatusSelect}
        options={activeStatusSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
ActiveStatusSelect.displayName = 'ActiveStatusSelect';

ActiveStatusSelect.propTypes = {
  activeStatusSelectOptions: PropTypes.arrayOf(PropTypes.shape({})),
  selectedActiveStatus: PropTypes.string,
  handleChangeActiveStatusSelect: PropTypes.func,
};

ActiveStatusSelect.defaultProps = {
  selectedActiveStatus: '',
};

export default ActiveStatusSelect;
