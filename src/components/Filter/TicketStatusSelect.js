import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const TicketStatusSelect = props => {
  const { selectedTicketStatus, handleChangeTicketStatusSelect, ticketStatusSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        حالة المشكلة
      </Text>
      <StyledSelect
        mb={3}
        placeholder="حالة المشكلة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedTicketStatus}
        onChange={handleChangeTicketStatusSelect}
        options={ticketStatusSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
TicketStatusSelect.displayName = 'TicketStatusSelect';

TicketStatusSelect.propTypes = {
  ticketStatusSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedTicketStatus: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeTicketStatusSelect: PropTypes.func.isRequired,
};

TicketStatusSelect.defaultProps = {
  selectedTicketStatus: undefined,
};

export default TicketStatusSelect;
