import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const TransactionTypesSelect = props => {
  const {
    selectedTransactionType,
    handleChangeTransactionTypesSelect,
    transactionTypesSelectOptions,
    labelText,
    isMulti,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        {labelText}
      </Text>
      <StyledSelect
        mb={3}
        placeholder="أنواع السجل"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti={isMulti}
        isRtl
        backspaceRemovesValue={false}
        value={selectedTransactionType}
        onChange={handleChangeTransactionTypesSelect}
        options={transactionTypesSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
TransactionTypesSelect.displayName = 'TransactionTypesSelect';

TransactionTypesSelect.propTypes = {
  labelText: PropTypes.string.isRequired,
  transactionTypesSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedTransactionType: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.object,
  ]),
  handleChangeTransactionTypesSelect: PropTypes.func.isRequired,
  isMulti: PropTypes.bool.isRequired,
};

TransactionTypesSelect.defaultProps = {
  selectedTransactionType: undefined,
};

export default TransactionTypesSelect;
