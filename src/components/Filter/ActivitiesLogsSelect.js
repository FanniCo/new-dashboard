import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const ActivitiesLogsSelect = props => {
  const { selectedActivitiesLogs, handleChangeActivitiesLogsSelect, activitiesLogsOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        النشاطات
      </Text>
      <StyledSelect
        mb={3}
        placeholder="أنواع النشاطات"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedActivitiesLogs}
        onChange={handleChangeActivitiesLogsSelect}
        options={activitiesLogsOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
ActivitiesLogsSelect.displayName = 'ActivitiesLogsSelect';

ActivitiesLogsSelect.propTypes = {
  activitiesLogsOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedActivitiesLogs: PropTypes.shape({}),
  handleChangeActivitiesLogsSelect: PropTypes.func.isRequired,
};

ActivitiesLogsSelect.defaultProps = {
  selectedActivitiesLogs: undefined,
};

export default ActivitiesLogsSelect;
