import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const ConfirmSelect = props => {
  const { selectedValues, handleChangeSelect, selectOptions, selectLabel, isMulti } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        {selectLabel}
      </Text>
      <StyledSelect
        mb={3}
        placeholder={selectLabel}
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti={isMulti}
        isRtl
        backspaceRemovesValue={false}
        value={selectedValues}
        onChange={handleChangeSelect}
        options={selectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
ConfirmSelect.displayName = 'ConfirmSelect';

ConfirmSelect.propTypes = {
  selectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  selectedValues: PropTypes.any,
  handleChangeSelect: PropTypes.func.isRequired,
  selectLabel: PropTypes.string.isRequired,
  isMulti: PropTypes.bool.isRequired,
};

ConfirmSelect.defaultProps = {
  selectedValues: undefined,
};

export default ConfirmSelect;
