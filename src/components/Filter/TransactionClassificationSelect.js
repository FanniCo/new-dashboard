import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const TransactionClassificationSelect = props => {
  const {
    selectedTransactionClassification,
    handleChangeTransactionClassificationSelect,
    transactionClassificationSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        تصنيف العملية
      </Text>
      <StyledSelect
        mb={3}
        placeholder="تصنيف العملية"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedTransactionClassification}
        onChange={handleChangeTransactionClassificationSelect}
        options={transactionClassificationSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
TransactionClassificationSelect.displayName = 'TransactionClassificationSelect';

TransactionClassificationSelect.propTypes = {
  transactionClassificationSelectOptions: PropTypes.arrayOf(PropTypes.shape({})),
  selectedTransactionClassification: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeTransactionClassificationSelect: PropTypes.func.isRequired,
};

TransactionClassificationSelect.defaultProps = {
  transactionClassificationSelectOptions: undefined,
  selectedTransactionClassification: undefined,
};

export default TransactionClassificationSelect;
