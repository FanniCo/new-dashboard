import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const FieldsSelect = props => {
  const { selectedFields, handleChangeFieldsSelect, fieldsSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        التخصص
      </Text>
      <StyledSelect
        mb={3}
        placeholder="التخصص"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedFields}
        onChange={handleChangeFieldsSelect}
        options={fieldsSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
FieldsSelect.displayName = 'FieldsSelect';

FieldsSelect.propTypes = {
  fieldsSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedFields: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeFieldsSelect: PropTypes.func.isRequired,
};

FieldsSelect.defaultProps = {
  selectedFields: undefined,
};

export default FieldsSelect;
