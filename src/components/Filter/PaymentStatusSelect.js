import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const PaymentStatusSelect = props => {
  const {
    selectedPaymentStatus,
    handleChangePaymentStatusSelect,
    paymentStatusSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        حالة الدفع
      </Text>
      <StyledSelect
        mb={3}
        placeholder="حالة الدفع"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedPaymentStatus}
        onChange={handleChangePaymentStatusSelect}
        options={paymentStatusSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
PaymentStatusSelect.displayName = 'PaymentStatusSelect';

PaymentStatusSelect.propTypes = {
  paymentStatusSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedPaymentStatus: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangePaymentStatusSelect: PropTypes.func.isRequired,
};

PaymentStatusSelect.defaultProps = {
  selectedPaymentStatus: undefined,
};

export default PaymentStatusSelect;
