import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const ComplainNameSelect = props => {
  const { selectedComplainName, handleChangeComplainNameSelect, complainsSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        طبيعة الشكوي
      </Text>
      <StyledSelect
        mb={3}
        placeholder="طبيعة الشكوي"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedComplainName}
        onChange={handleChangeComplainNameSelect}
        options={complainsSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
ComplainNameSelect.displayName = 'ComplainNameSelect';

ComplainNameSelect.propTypes = {
  complainsSelectOptions: PropTypes.arrayOf(PropTypes.shape({})),
  selectedComplainName: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeComplainNameSelect: PropTypes.func.isRequired,
};

ComplainNameSelect.defaultProps = {
  complainsSelectOptions: [],
  selectedComplainName: undefined,
};

export default ComplainNameSelect;
