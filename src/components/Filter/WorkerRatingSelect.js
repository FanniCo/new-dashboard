import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const WorkerRatingSelect = props => {
  const { selectedWorkerRating, handleChangeWorkerRatingSelect, workerRatingSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        التقييم
      </Text>
      <StyledSelect
        mb={3}
        placeholder="التقييم"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isRtl
        backspaceRemovesValue={false}
        value={selectedWorkerRating}
        onChange={handleChangeWorkerRatingSelect}
        options={workerRatingSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
WorkerRatingSelect.displayName = 'WorkerRatingSelect';

WorkerRatingSelect.propTypes = {
  workerRatingSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedWorkerRating: PropTypes.array.isRequired,
  handleChangeWorkerRatingSelect: PropTypes.func.isRequired,
};

WorkerRatingSelect.defaultProps = {
  selectedWorkerRating: undefined,
};

export default WorkerRatingSelect;
