import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const RequestReasonsSelect = props => {
  const { selectedReasons, handleChangeRequestReasonsSelect, reasonsSelectOptions } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        سبب الالغاء
      </Text>
      <StyledSelect
        mb={3}
        placeholder="سبب الالغاء"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedReasons}
        onChange={handleChangeRequestReasonsSelect}
        options={reasonsSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
RequestReasonsSelect.displayName = 'RequestReasonsSelect';

RequestReasonsSelect.propTypes = {
  reasonsSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedReasons: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeRequestReasonsSelect: PropTypes.func.isRequired,
};

RequestReasonsSelect.defaultProps = {
  selectedReasons: undefined,
};

export default RequestReasonsSelect;
