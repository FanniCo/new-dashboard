import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from '@rebass/grid';
import Text from 'components/Text';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS_VALUES, COLORS } from 'components/theme/colors';
import { FONT_TYPES, FONT_WEIGHTS } from 'components/theme/fonts';

const CommissionRateSelect = props => {
  const {
    selectedCommissionRate,
    handleChangeCommissionRateSelect,
    commissionRateSelectOptions,
  } = props;
  const { DISABLED } = COLORS;
  const { SUBHEADING } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;

  return (
    <Flex flexWrap="wrap" flexDirection="column" px={1} mb={2}>
      <Text mx={2} mb={4} type={SUBHEADING} color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
        معدل العمولة
      </Text>
      <StyledSelect
        mb={3}
        placeholder="معدل العمولة"
        noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
        isMulti
        isRtl
        backspaceRemovesValue={false}
        value={selectedCommissionRate}
        onChange={handleChangeCommissionRateSelect}
        options={commissionRateSelectOptions}
        theme={selectColors}
        styles={customSelectStyles}
        isClearable
      />
    </Flex>
  );
};
CommissionRateSelect.displayName = 'CommissionRateSelect';

CommissionRateSelect.propTypes = {
  commissionRateSelectOptions: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedCommissionRate: PropTypes.arrayOf(PropTypes.shape({})),
  handleChangeCommissionRateSelect: PropTypes.func.isRequired,
};

CommissionRateSelect.defaultProps = {
  selectedCommissionRate: undefined,
};

export default CommissionRateSelect;
