import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import VendorCard from 'components/VendorCard';
import AddNewVendorModal from 'components/AddNewVendorModal';
import ConfirmModal from 'components/ConfirmModal';
import ChangePasswordModal from 'components/ChangePasswordModal';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import EmptyState from 'components/EmptyState';
import Card from 'components/Card';
import { FONT_TYPES } from 'components/theme/fonts';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { ShimmerEffect, Rect } from 'components/ShimmerEffect';

const { BRANDING_GREEN, WHITE, GREY_LIGHT, GREY_DARK } = COLORS;
const { BIG_TITLE } = FONT_TYPES;

class Vendors extends Component {
  createVendorsList = VendorsList => {
    if (VendorsList.length) {
      const {
        user,
        handleToggleConfirmModal,
        handleToggleEditVendorModal,
        handleToggleChangePasswordModal,
        handleDeleteVendor,
        handleResetVendorCredits,
      } = this.props;
      return VendorsList.map((vendor, index) => {
        const { _id } = vendor;

        return (
          <Box key={`vendor${_id}`} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <VendorCard
              m={2}
              index={index}
              user={user}
              vendor={vendor}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditVendorModal={handleToggleEditVendorModal}
              handleToggleChangePasswordModal={handleToggleChangePasswordModal}
              handleDeleteVendor={handleDeleteVendor}
              handleResetVendorCredits={handleResetVendorCredits}
            />
          </Box>
        );
      });
    }
    return null;
  };

  getLoadingVendors = key => (
    <ShimmerEffect width={1} key={key}>
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
      <Rect width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]} height={250} m={2} />
    </ShimmerEffect>
  );

  createLoadingVendorsList = () => {
    const list = [];
    for (let counter = 0; counter < 3; counter += 1) {
      list.push(this.getLoadingVendors(counter));
    }
    return list;
  };

  render() {
    const {
      vendorsList,
      isAddNewVendorModalOpen,
      isConfirmModalOpen,
      isDeleteVendorFetching,
      handleToggleConfirmModal,
      isEditVendorModalOpen,
      handleToggleEditVendorModal,
      vendorDetails,
      vendorIndex,
      onSubmitChangePasswordForm,
      handleToggleChangePasswordModal,
      isChangePasswordModalOpen,
      isChangePasswordFetching,
      isChangePasswordError,
      changePasswordErrorMessage,
      isGetAllVendorsFetching,
      isGetAllVendorsSuccess,
      isGetAllVendorsError,
      getAllVendorsErrorMessage,
      applyNewFilter,
      confirmModalHeader,
      confirmModalText,
      confirmModalFunction,
      isResettingVendorCredits,
      isDeleteVendorError,
      isDeleteVendorErrorMessage,
    } = this.props;
    let vendorsRenderer;
    if (
      (isGetAllVendorsFetching && (!vendorsList || applyNewFilter)) ||
      (!isGetAllVendorsFetching && !isGetAllVendorsSuccess && !isGetAllVendorsError)
    ) {
      vendorsRenderer = this.createLoadingVendorsList();
    } else if (isGetAllVendorsError) {
      vendorsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text={getAllVendorsErrorMessage}
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else if (!vendorsList.length) {
      vendorsRenderer = (
        <Box width={1}>
          <Box width={[1, 1, 1, 1, 1]}>
            <Card
              minHeight={500}
              ml={[0, 0, 0, 0, 0]}
              mb={3}
              flexDirection="column"
              bgColor={COLORS_VALUES[GREY_DARK]}
            >
              <Flex m={2} justifyContent="center" alignItems="center">
                <Card width={1} minHeight={500} alignItems="center" backgroundColor={GREY_LIGHT}>
                  <EmptyState
                    icon={faFile}
                    iconColor={BRANDING_GREEN}
                    iconSize="3x"
                    textColor={COLORS_VALUES[WHITE]}
                    textSize={BIG_TITLE}
                    text="لا يوجد سجلات"
                  />
                </Card>
              </Flex>
            </Card>
          </Box>
        </Box>
      );
    } else {
      vendorsRenderer = this.createVendorsList(vendorsList);
    }

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {vendorsRenderer}
        {isAddNewVendorModalOpen && <AddNewVendorModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            isFail={isDeleteVendorError}
            errorMessage={isDeleteVendorErrorMessage}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={confirmModalFunction}
            isConfirming={isDeleteVendorFetching || isResettingVendorCredits}
          />
        )}
        {isEditVendorModalOpen && (
          <AddNewVendorModal
            toggleAddNewVendorModal={handleToggleEditVendorModal}
            vendorDetails={vendorDetails}
            vendorIndex={vendorIndex}
          />
        )}
        {isChangePasswordModalOpen && (
          <ChangePasswordModal
            adminDetails={vendorDetails}
            isChangePasswordModalOpen={isChangePasswordModalOpen}
            handleToggleChangePasswordModal={handleToggleChangePasswordModal}
            isChangePasswordFetching={isChangePasswordFetching}
            isChangePasswordError={isChangePasswordError}
            changePasswordErrorMessage={changePasswordErrorMessage}
            onSubmitChangePasswordForm={onSubmitChangePasswordForm}
          />
        )}
      </Flex>
    );
  }
}

Vendors.displayName = 'VendorsList';

Vendors.propTypes = {
  user: PropTypes.shape({}),
  vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewVendorModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  handleDeleteVendor: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isDeleteVendorFetching: PropTypes.bool,
  handleToggleEditVendorModal: PropTypes.func,
  handleToggleChangePasswordModal: PropTypes.func,
  onSubmitChangePasswordForm: PropTypes.func,
  isChangePasswordModalOpen: PropTypes.bool,
  isEditVendorModalOpen: PropTypes.bool,
  vendorDetails: PropTypes.shape({}),
  vendorIndex: PropTypes.number,
  isChangePasswordFetching: PropTypes.bool,
  isChangePasswordError: PropTypes.bool,
  changePasswordErrorMessage: PropTypes.string,
  isGetAllVendorsFetching: PropTypes.bool,
  isGetAllVendorsSuccess: PropTypes.bool,
  isGetAllVendorsError: PropTypes.bool,
  getAllVendorsErrorMessage: PropTypes.string,
  applyNewFilter: PropTypes.bool,
  confirmModalHeader: PropTypes.string,
  confirmModalText: PropTypes.string,
  confirmModalFunction: PropTypes.func,
  handleResetVendorCredits: PropTypes.func,
  isResettingVendorCredits: PropTypes.bool,
  isDeleteVendorError: PropTypes.bool,
  isDeleteVendorErrorMessage: PropTypes.string,
};

Vendors.defaultProps = {
  user: undefined,
  vendorsList: undefined,
  isAddNewVendorModalOpen: false,
  handleToggleConfirmModal: () => {},
  handleDeleteVendor: () => {},
  isConfirmModalOpen: false,
  isDeleteVendorFetching: false,
  handleToggleEditVendorModal: () => {},
  handleToggleChangePasswordModal: () => {},
  isEditVendorModalOpen: false,
  vendorDetails: {},
  vendorIndex: undefined,
  onSubmitChangePasswordForm: () => {},
  isChangePasswordModalOpen: false,
  isChangePasswordFetching: false,
  isChangePasswordError: false,
  changePasswordErrorMessage: undefined,
  isGetAllVendorsFetching: false,
  isGetAllVendorsSuccess: false,
  isGetAllVendorsError: false,
  getAllVendorsErrorMessage: undefined,
  applyNewFilter: false,
  confirmModalHeader: '',
  confirmModalText: '',
  confirmModalFunction: () => {},
  handleResetVendorCredits: () => {},
  isResettingVendorCredits: false,
  isDeleteVendorError: false,
  isDeleteVendorErrorMessage: undefined,
};

export default Vendors;
