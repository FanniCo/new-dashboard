import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { Flex } from '@rebass/grid';
import { addTicket } from 'redux-modules/requests/actions';
import ComplaintsDropDown from 'containers/shared/ComplaintsDropDown';
import ComplaintReceivedInDropDown from 'containers/shared/ComplaintReceivedInDropDown';
import InputField from 'components/InputField';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import Caution from 'components/Caution';

class OpenTicketModal extends Component {
  static propTypes = {
    request: PropTypes.shape({}),
    worker: PropTypes.shape({}),
    customer: PropTypes.shape({}),
    isAddTicketFetching: PropTypes.bool,
    // isAddTicketSuccuess: PropTypes.bool,
    isAddTicketError: PropTypes.bool,
    isAddTicketErrorMessage: PropTypes.string,
    addTicket: PropTypes.func,
    isOpened: PropTypes.bool,
    toggleFunction: PropTypes.func,
  };

  static defaultProps = {
    request: undefined,
    worker: undefined,
    customer: undefined,
    isAddTicketFetching: false,
    // isAddTicketSuccuess: false,
    isAddTicketError: false,
    isAddTicketErrorMessage: undefined,
    addTicket: () => {},
    toggleFunction: () => {},
    isOpened: false,
  };

  state = {
    complaintReceivedInSelectValue: null,
    complaintListSelectValue: null,
    comment: '',
  };

  handleComplaintsSelectChange = val => {
    this.setState({ complaintListSelectValue: val });
  };

  handleComplaintsReceivedInSelectChange = val => {
    this.setState({ complaintReceivedInSelectValue: val });
  };

  handleCommentInputFieldChange = value => {
    this.setState({ comment: value });
  };

  handleAddTicket = (request, worker) => {
    const { addTicket: addTicketAction } = this.props;
    const { complaintReceivedInSelectValue, complaintListSelectValue, comment } = this.state;

    const ticketInfo = {
      request: request ? request._id : '',
      complaints: [
        {
          complainName: complaintListSelectValue.value.code,
          complainReceivedIn: complaintReceivedInSelectValue.value.code,
          whoCausedTheComplaint: worker ? worker._id : '',
        },
      ],
      comments: comment === '' ? [] : [comment],
    };

    addTicketAction(ticketInfo);
  };

  render() {
    const {
      isOpened,
      toggleFunction,
      isAddTicketFetching,
      request,
      worker,
      isAddTicketError,
      isAddTicketErrorMessage,
    } = this.props;
    const { BRANDING_GREEN, BRANDING_BLUE, WHITE, LIGHT_RED, ERROR } = COLORS;
    const { HEADING } = FONT_TYPES;
    const { complaintReceivedInSelectValue, complaintListSelectValue, comment } = this.state;
    const OpenTicketModalHeader = 'فتح تذكرة مشكلة';

    return (
      <Modal toggleModal={toggleFunction} header={OpenTicketModalHeader} isOpened={isOpened}>
        <Flex flexDirection="column" px={3}>
          <Flex pb={3}>
            <Flex flexDirection="column" width={1} px={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                نوع الشكوي
                <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                  *
                </Text>
              </Text>
              <ComplaintsDropDown
                isMulti={false}
                complaintSelectedValue={complaintListSelectValue}
                handleInputChange={this.handleComplaintsSelectChange}
              />
            </Flex>
            <Flex flexDirection="column" width={1} px={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                طريقة استلام الشكوي
                <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                  *
                </Text>
              </Text>
              <ComplaintReceivedInDropDown
                isMulti={false}
                complaintReceivedInSelectedValue={complaintReceivedInSelectValue}
                handleInputChange={this.handleComplaintsReceivedInSelectChange}
              />
            </Flex>
          </Flex>
          <Flex pb={3} px={2}>
            <Flex flexDirection="column" width={1}>
              <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                تعليق
                <Text mb={2} type={HEADING} color={COLORS_VALUES[LIGHT_RED]}>
                  *
                </Text>
              </Text>
              <InputField
                type="textarea"
                placeholder="تعليق"
                ref={inputField => {
                  this.memoField = inputField;
                }}
                width={1}
                value={comment}
                onChange={value => this.handleCommentInputFieldChange(value)}
                mb={2}
                autoComplete="on"
              />
            </Flex>
          </Flex>
          <Flex flexDirection="row-reverse" py={3}>
            <Button
              color={BRANDING_BLUE}
              onClick={() => this.handleAddTicket(request, worker)}
              isLoading={isAddTicketFetching}
              disabled={!complaintReceivedInSelectValue || !complaintListSelectValue || !comment}
              mx={1}
            >
              تأكيد فتح مشكلة
            </Button>
            {isAddTicketError && (
              <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                {isAddTicketErrorMessage}
              </Caution>
            )}
          </Flex>
        </Flex>
      </Modal>
    );
  }
}
OpenTicketModal.displayName = 'OpenTicketModal';

const mapStateToProps = state => ({
  isAddTicketFetching: state.requests.addTicket.isFetching,
  isAddTicketSuccuess: state.requests.addTicket.isSuccess,
  isAddTicketError: state.requests.addTicket.isFail.isError,
  isAddTicketErrorMessage: state.requests.addTicket.isFail.message,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addTicket,
    },
    dispatch,
  );

export default compose(connect(mapStateToProps, mapDispatchToProps))(OpenTicketModal);
