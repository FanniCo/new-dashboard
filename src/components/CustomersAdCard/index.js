import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ReactTooltip from 'react-tooltip';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { CopyToClipboard } from 'pivotal-ui/react/copy-to-clipboard';
import { DefaultButton } from 'pivotal-ui/react/buttons';
import { Icon } from 'pivotal-ui/react/iconography';

const CustomersAdCard = props => {
  const {
    customersAd,
    customersAd: { _id, title, content, createdBy, createdAt, externalUrl },
    index,
    user: { permissions },
    handleToggleConfirmModal,
    handleToggleEditCustomersAdModal,
    deleteCustomersAdAction,
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, LIGHT_RED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const customersAdDate = new Date(createdAt);
  const deleteCustomersAdTooltip = 'مسح الاعلان';
  const editCustomersAdTooltip = 'تعديل الاعلان';

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {customersAdDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العنوان :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {title.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الوصف :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {content.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
        {externalUrl && (
          <Flex flexDirection="row-reverse" w={1} py={1} px={2} mb={1}>
            <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
              رابط المشاركة:
            </SubTitle>
            <CopyToClipboard text={externalUrl} tooltip="نسخ بنجاح">
              <DefaultButton
                {...{
                  iconOnly: true,
                  icon: <Icon src="copy" />,
                }}
              />
            </CopyToClipboard>
          </Flex>
        )}
      </CardBody>
      {permissions &&
        (permissions.includes('Delete Customers Ad') ||
          permissions.includes('Update Customers Ad')) && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
              {permissions && permissions.includes('Delete Customers Ad') && (
                <>
                  <ReactTooltip id={`deleteCustomersAd_${_id}`} type="error" effect="solid">
                    <span>{deleteCustomersAdTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`deleteCustomersAd_${_id}`}
                      data-tip
                      onClick={() =>
                        handleToggleConfirmModal(
                          'تأكيد مسح الاعلان',
                          'هل أنت متأكد أنك تريد مسح الاعلان؟',
                          () => {
                            deleteCustomersAdAction(_id, index);
                          },
                        )
                      }
                      icon={faTrashAlt}
                      color={COLORS_VALUES[LIGHT_RED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
              {permissions && permissions.includes('Update Customers Ad') && (
                <>
                  <ReactTooltip id={`editCustomer_${_id}`} type="light" effect="solid">
                    <span>{editCustomersAdTooltip}</span>
                  </ReactTooltip>
                  <Box>
                    <CallToActionIcon
                      data-for={`editCustomer_${_id}`}
                      data-tip
                      onClick={() => handleToggleEditCustomersAdModal(index, customersAd)}
                      icon={faPen}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
          </Flex>
        </CardFooter>
        )}
    </Card>
  );
};
CustomersAdCard.displayName = 'CustomersAdCard';

CustomersAdCard.propTypes = {
  customersAd: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditCustomersAdModal: PropTypes.func,
  deleteCustomersAdAction: PropTypes.func,
};

CustomersAdCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditCustomersAdModal: () => {},
  deleteCustomersAdAction: () => {},
};

export default CustomersAdCard;
