import React from 'react';
import styled from "styled-components";
import {Flex} from "@rebass/grid";
import {
  COLORS,
  COLORS_VALUES
} from "components/theme/colors";
import {
  FONT_TYPES,
  FONT_WEIGHTS
} from "components/theme/fonts";
import EmptyState from "components/EmptyState";
import {
  faCommentDots
} from "@fortawesome/free-solid-svg-icons";
import Text from "components/Text";
import {
  CardTitle,
  OddRow
} from "components/shared";
import PropTypes from "prop-types";
import Card from 'components/Card';
import ROUTES from "routes";
import Button from "components/Buttons";
import {width} from "styled-system";
import {REQUESTS_STATUSES} from "utils/constants";

const Container = styled(Flex)`
  height: 100%;
`;

const { DISABLED, GREY_LIGHT, BRANDING_GREEN, WHITE } = COLORS;
const { SUBHEADING } = FONT_TYPES;
const { NORMAL } = FONT_WEIGHTS;

const AppliedWorkerRow = styled(Flex)`
  cursor: pointer;
  border-bottom: 1px solid ${COLORS_VALUES[COLORS.GREY_LIGHT]};
  padding: ${props => props.theme.space[4]}px ${props => props.theme.space[2]}px;

  &:hover {
    background-color: ${COLORS_VALUES[COLORS.GREY_LIGHT]};
  }
`;

export const HyperLink = styled.a`
  ${width};
  text-decoration: none;
`;

const AppliedWorkersCard = (props) => {
  const {
    firebaseAppliedWorkersDetails,
    appliedWorkersDetails,
    requestId,
    acceptWorkerAction,
    handleToggleConfirmModal,
    requestStatus
  } = props;
  console.log('appliedWorkersappliedWorkers', appliedWorkersDetails);

  const { SINGLE_WORKER } = ROUTES;

  let appliedWorkersList;
  const acceptWorkerModalHeader = 'تأكيد تنشيط الطلب';
  console.log('firebaseAppliedWorkersDetails', firebaseAppliedWorkersDetails);
  console.log('appliedWorkersDetails', appliedWorkersDetails);

  if ((!firebaseAppliedWorkersDetails || !firebaseAppliedWorkersDetails.length) && (!appliedWorkersDetails || !appliedWorkersDetails.length)) {
    appliedWorkersList = (
      <EmptyState
        icon={faCommentDots}
        iconSize="3x"
        iconColor={BRANDING_GREEN}
        textColor={COLORS_VALUES[WHITE]}
        text="لا توجد فنيين"
        textSize={SUBHEADING}
      />
    );
  } else {
    const uniqueIds = [];

    const uniqueAppliedWorkers = [...appliedWorkersDetails, ...firebaseAppliedWorkersDetails].filter(element => {
      const isDuplicate = uniqueIds.includes(element._id);

      if (!isDuplicate) {
        uniqueIds.push(element._id);

        return true;
      }

      return false;
    });


    appliedWorkersList = uniqueAppliedWorkers.map((worker, index) => {
      const acceptWorkerConfirmModalConfirmText = `    هل أنت متأكد أنك تريد تنشيط الطلب بهذا الفني؟${worker.name} `;
      console.log('worker', worker);
      return (
        <OddRow key={worker._id.toString()} flexDirection="row-reverse" alignItems="center" width={1} py={2} px={2}>
          <AppliedWorkerRow width={1}>
              <Flex flexDirection="column" width={0.1}>
                {requestStatus === REQUESTS_STATUSES.pending.en.toLowerCase() && <Button
                  minWidth="100%"
                  ml={1}
                  mr={1}
                  color={BRANDING_GREEN}
                  onClick={() =>
                    handleToggleConfirmModal(
                      acceptWorkerModalHeader,
                      acceptWorkerConfirmModalConfirmText,
                      () => {
                        acceptWorkerAction(requestId, worker._id)
                      },
                    )
                  }
                  iconWidth="lg"
                  reverse
                  xMargin={3}
                  isLoading={false}
                >
                 موافقة
                </Button>}
              </Flex>

            <Flex flexDirection="column" width={0.3} textAlign="right">
              <Text cursor="pointer" textAlign="right" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                <span>طلبات مكتملة:</span>
                <span>{worker.doneRequestsCount}</span>
              </Text>
            </Flex>

              <Flex flexDirection="column" width={0.1} textAlign="right">
                  <Text cursor="pointer" textAlign="right" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                    {`${worker.averageRating}/5`}
                  </Text>
              </Flex>

              <Flex flexDirection="column" width={0.2} textAlign="right">
                <Text cursor="pointer" textAlign="right" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                  {`${worker.distanceFromRequestLocation} KM`}
                </Text>
              </Flex>

              <Flex flexDirection="column" width={0.3} textAlign="right">
                <HyperLink
                  key={worker.username}
                  width={1}
                  href={`${SINGLE_WORKER}/${worker.username}`}
                  target="_blank"
                  textAlign="right"
                >
                  <Text cursor="pointer" textAlign="left" color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
                    {worker.name}
                  </Text>
                </HyperLink>
              </Flex>
              <Flex width={0.2}>
                <Text
                  cursor="pointer"
                  textAlign="right"
                  width={1}
                  color={COLORS_VALUES[DISABLED]}
                  type={SUBHEADING}
                  fontWeight={NORMAL}
                >
                  {index + 1}
                </Text>
              </Flex>
          </AppliedWorkerRow>
        </OddRow>
      )
    })
  }

  return (
    <Card
      flexWrap="wrap"
      justifyContent="center"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
      mb={2}
      px={2}
      py={3}
    >
      <CardTitle px={1} py={3} width={1}>
        <Text
          textAlign="right"
          width={1}
          color={COLORS_VALUES[DISABLED]}
          type={SUBHEADING}
          fontWeight={NORMAL}
        >
          :الفنيين المتقدمين
        </Text>
      </CardTitle>
      <Container width={1} flexDirection="column" alignItems="center">
        {appliedWorkersList}
      </Container>
    </Card>
  )
}
AppliedWorkersCard.displayName = 'AppliedWorkersCard';

AppliedWorkersCard.propTypes = {
  appliedWorkersDetails: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  requestId: PropTypes.string.isRequired,
  acceptWorkerAction: PropTypes.func,
  handleToggleConfirmModal: PropTypes.func,
};

AppliedWorkersCard.defaultProps = {
  appliedWorkersDetails: [],
  requestId: undefined,
  acceptWorkerAction: () => {},
  handleToggleConfirmModal: () => {},
};

export default AppliedWorkersCard;