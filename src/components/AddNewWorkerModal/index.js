import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import {Box, Flex} from '@rebass/grid';
import styled from 'styled-components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { convertArabicNumbersToEnglish } from 'utils/shared/helpers';
import { addWorker, toggleAddNewWorkerModal } from 'redux-modules/workers/actions';
import InputField from 'components/InputField';
import Caution from 'components/Caution';
import Modal from 'components/Modal';
import Text from 'components/Text';
import Button from 'components/Buttons';
import { StyledSelect, selectColors, customSelectStyles } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_TYPES } from 'components/theme/fonts';
import { FIELDS, CITIES } from 'utils/constants';
import {Checkbox, Label} from "@rebass/forms";

const fieldsKeys = Object.keys(FIELDS);
const citiesKeys = Object.keys(CITIES);
const fieldsSelectOptions = fieldsKeys.map(key => ({
  value: key,
  label: FIELDS[key].ar,
}));
const citiesSelectOptions = citiesKeys.map(key => ({
  value: key,
  label: CITIES[key].ar,
}));

const AddNewWorkerFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

class AddNewWorkerModal extends Component {
  static propTypes = {
    isAddWorkerFetching: PropTypes.bool,
    isAddWorkerError: PropTypes.bool,
    addWorkerErrorMessage: PropTypes.string,
    addWorker: PropTypes.func,
    toggleAddNewWorkerModal: PropTypes.func,
    isAddWorkerModalOpen: PropTypes.bool,
    vendorsList: PropTypes.arrayOf(PropTypes.shape({})),
  };

  static defaultProps = {
    isAddWorkerFetching: false,
    isAddWorkerError: false,
    addWorkerErrorMessage: '',
    addWorker: () => {},
    toggleAddNewWorkerModal: () => {},
    isAddWorkerModalOpen: false,
    vendorsList: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      fields: [],
      username: '',
      workerReceivedShirt: false,
      additionalUsername: '',
      governmentId: '',
      city: null,
      sponsor: '',
      vendorId: '',
    };
  }

  handleInputFieldChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleChangeVendorSelect = vendorId => {
    this.setState({ vendorId });
  };

  handleChangeFieldsSelect = fields => {
    this.setState({ fields });
  };

  handleChangeCitiesSelect = city => {
    this.setState({ city });
  };

  handleCheckBoxChange = type => {
    this.setState(prevState => ({
      [type]: !prevState[type],
    }));
  };

  onSubmitAddWorkerForm = e => {
    const { addWorker: addWorkerAction } = this.props;
    const { name, username, additionalUsername, workerReceivedShirt, fields, governmentId, city, sponsor, vendorId } = this.state;
    const workerFields = fields.map(field => field.value);
    const workerCity = city.value;

    e.preventDefault();

    const newWorkerInfo = {
      name,
      username: convertArabicNumbersToEnglish(username),
      additionalUsername: convertArabicNumbersToEnglish(additionalUsername),
      workerReceivedShirt: workerReceivedShirt,
      fields: workerFields,
      governmentId: convertArabicNumbersToEnglish(governmentId),
      city: workerCity,
      sponsor,
      vendorId: vendorId ? vendorId.value : '',
    };

    addWorkerAction(newWorkerInfo);
  };

  render() {
    const {
      isAddWorkerFetching,
      isAddWorkerModalOpen,
      toggleAddNewWorkerModal: toggleAddNewWorkerModalAction,
      isAddWorkerError,
      addWorkerErrorMessage,
      vendorsList,
    } = this.props;
    const { name, username, additionalUsername, workerReceivedShirt, fields, governmentId, city, sponsor, vendorId } = this.state;
    const { BRANDING_GREEN, LIGHT_RED, ERROR, WHITE } = COLORS;
    const { HEADING } = FONT_TYPES;
    const addWorkerModalHeader = 'أضف فني جديد';
    const vendorsSelectOptions = vendorsList.map(vendor => ({
      value: vendor._id,
      label: `${vendor.name}`,
    }));
    return (
      <Modal
        toggleModal={toggleAddNewWorkerModalAction}
        header={addWorkerModalHeader}
        isOpened={isAddWorkerModalOpen}
      >
        <AddNewWorkerFormContainer onSubmit={this.onSubmitAddWorkerForm}>
          <Flex flexDirection="column">
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الفني
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم الفني"
                  ref={inputField => {
                    this.nameField = inputField;
                  }}
                  width={1}
                  value={name}
                  onChange={value => this.handleInputFieldChange('name', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الجوال
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الجوال"
                  ref={inputField => {
                    this.userNameField = inputField;
                  }}
                  width={1}
                  value={username}
                  onChange={value => this.handleInputFieldChange('username', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الجوال اضافي
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الجوال"
                  ref={inputField => {
                    this.userNameField = inputField;
                  }}
                  width={1}
                  value={additionalUsername}
                  onChange={value => this.handleInputFieldChange('additionalUsername', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1} pl={2}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  رقم الهوية
                </Text>
                <InputField
                  type="text"
                  placeholder="رقم الهوية"
                  ref={inputField => {
                    this.governmentIdField = inputField;
                  }}
                  width={1}
                  value={governmentId}
                  onChange={value => this.handleInputFieldChange('governmentId', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الكفيل
                </Text>
                <InputField
                  type="text"
                  placeholder="اسم الكفيل"
                  ref={inputField => {
                    this.sponsorField = inputField;
                  }}
                  width={1}
                  value={sponsor}
                  onChange={value => this.handleInputFieldChange('sponsor', value)}
                  mb={2}
                  autoComplete="on"
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  <Box>
                    <Label>
                      <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                        الفني استلم التشيرت
                      </Text>
                      <Checkbox
                        name="remember"
                        checked={workerReceivedShirt}
                        onChange={() => this.handleCheckBoxChange('workerReceivedShirt')}
                        sx={{ color: '#fcfffa' }}
                      />
                    </Label>
                  </Box>
                </Text>
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  اسم الشريك
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="اسم الشريك"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti={false}
                  isRtl
                  backspaceRemovesValue={false}
                  value={vendorId}
                  onChange={this.handleChangeVendorSelect}
                  options={vendorsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  التخصص
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="التخصص"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isMulti
                  isRtl
                  backspaceRemovesValue={false}
                  value={fields}
                  onChange={this.handleChangeFieldsSelect}
                  options={fieldsSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex pb={3} px={4}>
              <Flex flexDirection="column" width={1}>
                <Text mb={2} type={HEADING} color={COLORS_VALUES[BRANDING_GREEN]}>
                  المدينة
                </Text>
                <StyledSelect
                  mb={3}
                  placeholder="المدينة"
                  noOptionsMessage={() => 'لا يوجد اختيارات متاحة'}
                  isRtl
                  backspaceRemovesValue={false}
                  value={city}
                  onChange={this.handleChangeCitiesSelect}
                  options={citiesSelectOptions}
                  theme={selectColors}
                  styles={customSelectStyles}
                  isClearable
                />
              </Flex>
            </Flex>
            <Flex
              flexDirection="row-reverse"
              alignItems="center"
              justifyContent="space-between"
              pb={5}
              px={4}
            >
              <Button
                type="submit"
                primary
                color={BRANDING_GREEN}
                icon={faChevronLeft}
                reverse
                onClick={this.onSubmitAddWorkerForm}
                disabled={
                  isAddWorkerFetching ||
                  name === '' ||
                  !fields ||
                  !city ||
                  username === '' ||
                  governmentId === '' ||
                  sponsor === ''
                }
                isLoading={isAddWorkerFetching}
              >
                أضف فني جديد
              </Button>
              {isAddWorkerError && (
                <Caution mx={2} bgColor={LIGHT_RED} textColor={WHITE} borderColorProp={ERROR}>
                  {addWorkerErrorMessage}
                </Caution>
              )}
            </Flex>
          </Flex>
        </AddNewWorkerFormContainer>
      </Modal>
    );
  }
}
AddNewWorkerModal.displayName = 'AddNewWorkerModal';

const mapStateToProps = state => ({
  isAddWorkerFetching: state.workers.addWorker.isFetching,
  isAddWorkerError: state.workers.addWorker.isFail.isError,
  addWorkerErrorMessage: state.workers.addWorker.isFail.message,
  isAddWorkerModalOpen: state.workers.addNewWorkerModal.isOpen,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addWorker,
      toggleAddNewWorkerModal,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddNewWorkerModal);
