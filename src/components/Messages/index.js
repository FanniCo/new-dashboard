import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import MessageCard from 'components/MessageCard';
import AddNewMessageModal from 'components/AddNewMessageModal';
import ConfirmModal from 'components/ConfirmModal';
import EditMessageModal from 'components/EditMessageModal';

class Messages extends Component {
  createMessagesList = MessagesList => {
    if (MessagesList.length) {
      const { user, handleToggleConfirmModal, handleToggleEditWorkerModal } = this.props;
      return MessagesList.map((message, index) => {
        const { _id } = message;

        return (
          <Box key={`message_${_id}`} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <MessageCard
              m={2}
              index={index}
              user={user}
              message={message}
              handleToggleConfirmModal={handleToggleConfirmModal}
              handleToggleEditWorkerModal={handleToggleEditWorkerModal}
            />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const confirmModalHeader = 'تأكيد مسح الرسالة';
    const confirmModalText = 'هل أنت متأكد أنك تريد مسح الرسالة؟';
    const {
      messagesList,
      isAddNewMessageModalOpen,
      isConfirmModalOpen,
      isDeleteMessageFetching,
      handleDeleteMessage,
      handleToggleConfirmModal,
      isEditMessageModalOpen,
      handleToggleEditWorkerModal,
      messageDetails,
      messageIndex,
      isDeleteMessageError,
      isDeleteMessageErrorMessage,
    } = this.props;

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {this.createMessagesList(messagesList)}
        {isAddNewMessageModalOpen && <AddNewMessageModal />}
        {isConfirmModalOpen && (
          <ConfirmModal
            isFail={isDeleteMessageError}
            errorMessage={isDeleteMessageErrorMessage}
            isOpened={isConfirmModalOpen}
            header={confirmModalHeader}
            confirmText={confirmModalText}
            toggleFunction={handleToggleConfirmModal}
            confirmFunction={handleDeleteMessage}
            isConfirming={isDeleteMessageFetching}
          />
        )}
        {isEditMessageModalOpen && (
          <EditMessageModal
            handleToggleEditWorkerModal={handleToggleEditWorkerModal}
            messageDetails={messageDetails}
            messageIndex={messageIndex}
          />
        )}
      </Flex>
    );
  }
}

Messages.displayName = 'MessagesList';

Messages.propTypes = {
  user: PropTypes.shape({}),
  messagesList: PropTypes.arrayOf(PropTypes.shape({})),
  isAddNewMessageModalOpen: PropTypes.bool,
  handleToggleConfirmModal: PropTypes.func,
  handleDeleteMessage: PropTypes.func,
  isConfirmModalOpen: PropTypes.bool,
  isDeleteMessageFetching: PropTypes.bool,
  handleToggleEditWorkerModal: PropTypes.func,
  isEditMessageModalOpen: PropTypes.bool,
  messageDetails: PropTypes.shape({}),
  messageIndex: PropTypes.number,
  isDeleteMessageError: PropTypes.bool,
  isDeleteMessageErrorMessage: PropTypes.string,
};

Messages.defaultProps = {
  user: undefined,
  messagesList: undefined,
  isAddNewMessageModalOpen: false,
  handleToggleConfirmModal: () => {},
  handleDeleteMessage: () => {},
  isConfirmModalOpen: false,
  isDeleteMessageFetching: false,
  handleToggleEditWorkerModal: () => {},
  isEditMessageModalOpen: false,
  messageDetails: {},
  messageIndex: undefined,
  isDeleteMessageError: false,
  isDeleteMessageErrorMessage: undefined,
};

export default Messages;
