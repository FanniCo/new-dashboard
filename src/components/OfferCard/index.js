import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import Card from 'components/Card';
import Text from 'components/Text';
import { CardBody, SubTitle, CardFooter, CallToActionIcon } from 'components/shared';
import { COLORS, COLORS_VALUES } from 'components/theme/colors';
import { FONT_WEIGHTS, FONT_TYPES } from 'components/theme/fonts';
import ReactTooltip from 'react-tooltip';
import { faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const OfferCard = props => {
  const {
    offer,
    offer: { _id, title, body, createdBy, createdAt },
    index,
    user: { permissions },
    handleToggleConfirmModal,
    handleToggleEditOfferModal,
    deleteOfferAction,
  } = props;
  const { BRANDING_GREEN, GREY_LIGHT, DISABLED, LIGHT_RED } = COLORS;
  const { BODY } = FONT_TYPES;
  const { NORMAL } = FONT_WEIGHTS;
  const offerDate = new Date(createdAt);
  const deleteOfferTooltip = 'مسح العرض';
  const editOfferTooltip = 'تعديل العرض';

  return (
    <Card
      {...props}
      flexDirection="column"
      justifyContent="space-between"
      minHeight={100}
      height="calc(100% - 16px)"
      bgColor={COLORS_VALUES[GREY_LIGHT]}
    >
      <CardBody flexDirection="column" justifyContent="space-between" py={2} px={1}>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {offerDate.toLocaleDateString('en-US', {
              year: 'numeric',
              month: 'numeric',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              hour12: true,
            })}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            العنوان :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {title.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            الوصف :
          </SubTitle>
          <Text type={BODY} color={COLORS_VALUES[BRANDING_GREEN]} fontWeight={NORMAL}>
            {body.ar}
          </Text>
        </Flex>
        <Flex flexDirection="row-reverse" py={1} px={2} mb={1}>
          <SubTitle color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            بواسطة :
          </SubTitle>
          <Text color={COLORS_VALUES[DISABLED]} fontWeight={NORMAL}>
            {createdBy ? `[ ${createdBy.username} ] - ${createdBy.name}` : 'مدير'}
          </Text>
        </Flex>
      </CardBody>
      {permissions &&
        (permissions.includes('Delete Offer') || permissions.includes('Update Offer')) && (
        <CardFooter
          flexDirection="row-reverse"
          justifyContent="space-between"
          flexWrap="wrap"
          p={2}
        >
          <Flex px={2}>
              {permissions && permissions.includes('Delete Offer') && (
                <>
                  <ReactTooltip id={`deleteOffer_${_id}`} type="error" effect="solid">
                    <span>{deleteOfferTooltip}</span>
                  </ReactTooltip>
                  <Box mr={3}>
                    <CallToActionIcon
                      data-for={`deleteOffer_${_id}`}
                      data-tip
                      onClick={() =>
                        handleToggleConfirmModal(
                          'تأكيد مسح العرض',
                          'هل أنت متأكد أنك تريد مسح العرض؟',
                          () => {
                            deleteOfferAction(_id, index);
                          },
                        )
                      }
                      icon={faTrashAlt}
                      color={COLORS_VALUES[LIGHT_RED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
              {permissions && permissions.includes('Update Offer') && (
                <>
                  <ReactTooltip id={`editOffer_${_id}`} type="light" effect="solid">
                    <span>{editOfferTooltip}</span>
                  </ReactTooltip>
                  <Box>
                    <CallToActionIcon
                      data-for={`editOffer_${_id}`}
                      data-tip
                      onClick={() => handleToggleEditOfferModal(index, offer)}
                      icon={faPen}
                      color={COLORS_VALUES[DISABLED]}
                      size="sm"
                    />
                  </Box>
                </>
              )}
          </Flex>
        </CardFooter>
        )}
    </Card>
  );
};
OfferCard.displayName = 'OfferCard';

OfferCard.propTypes = {
  offer: PropTypes.shape({}).isRequired,
  index: PropTypes.number.isRequired,
  user: PropTypes.shape({}).isRequired,
  handleToggleConfirmModal: PropTypes.func,
  handleToggleEditOfferModal: PropTypes.func,
  deleteOfferAction: PropTypes.func,
};

OfferCard.defaultProps = {
  handleToggleConfirmModal: () => {},
  handleToggleEditOfferModal: () => {},
  deleteOfferAction: () => {},
};

export default OfferCard;
