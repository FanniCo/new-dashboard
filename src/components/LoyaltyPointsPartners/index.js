import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex } from '@rebass/grid';
import LoyaltyPointsPartnersCard from 'components/LoyaltyPointsPartnersCard';

class LoyaltyPointsPartners extends Component {
  createLoyaltyPointsPartnersList = LoyaltyPointsPartnersList => {
    if (LoyaltyPointsPartnersList.length) {
      const { user } = this.props;
      return LoyaltyPointsPartnersList.map((loyaltyPointsPartner, index) => {
        const { _id } = loyaltyPointsPartner;

        return (
          <Box key={`loyaltyPointsPartners_${_id}`} width={[1, 1, 1, 1 / 2, 1 / 2, 1 / 3]}>
            <LoyaltyPointsPartnersCard
              m={2}
              index={index}
              user={user}
              loyaltyPointsPartner={loyaltyPointsPartner}
            />
          </Box>
        );
      });
    }
    return null;
  };

  render() {
    const { loyaltyPointsPartnersList } = this.props;

    return (
      <Flex width={1} flexWrap="wrap" flexDirection="row-reverse">
        {this.createLoyaltyPointsPartnersList(loyaltyPointsPartnersList)}
      </Flex>
    );
  }
}

LoyaltyPointsPartners.displayName = 'LoyaltyPointsPartnersList';

LoyaltyPointsPartners.propTypes = {
  user: PropTypes.shape({}),
  loyaltyPointsPartnersList: PropTypes.arrayOf(PropTypes.shape({})),
};

LoyaltyPointsPartners.defaultProps = {
  user: undefined,
  loyaltyPointsPartnersList: undefined,
};

export default LoyaltyPointsPartners;
