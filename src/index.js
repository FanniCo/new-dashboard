import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import store from 'redux-modules/index';
import App from 'containers/App';
import theme from 'components/theme';
import * as serviceWorker from './serviceWorker';
import './static/style.css';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <Suspense fallback="loading...">
      <Router>
        <Route component={App} />
      </Router>
      </Suspense>
    </Provider>
  </ThemeProvider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
