const COOKIES_KEYS = {
  TOKEN: 'TK',
  PLAIN_USER: 'US',
};

const USER_ROLES = {
  ADMIN: 'admin',
  CUSTOMER: 'customer',
  WORKER: 'worker',
};

const USER_TARGETS = {
  customer: {
    ar: 'العملاء',
    en: 'customers',
  },
  worker: {
    ar: 'الفنيين',
    en: 'workers',
  },
};

const OPERATING_SYSTEMS = {
  android: {
    ar: 'أندرويد',
    en: 'android',
  },
  ios: {
    ar: 'أي أو أس',
    en: 'ios',
  },
};

const CONFIRM_CHOICE = {
  true: {
    ar: 'نعم',
    en: 'Yes',
  },
  false: {
    ar: 'لا',
    en: 'No',
  },
};

const FIELDS = {
  plumbing: {
    ar: 'سباكة',
    en: 'Plumbing',
  },
  electrical: {
    ar: 'كهرباء',
    en: 'Electrical',
  },
  'air-conditioning': {
    ar: 'تكييف',
    en: 'Air conditioning',
  },
  'satellite-tv': {
    ar: 'ستلايات',
    en: 'Satellite TV',
  },
  'furniture-transportation': {
    ar: 'نقل أثاث',
    en: 'Furniture transportation',
  },
  'home-appliances': {
    ar: 'أجهزة منزلية',
    en: 'Home appliances',
  },
  carpentry: {
    ar: 'نجارة',
    en: 'Carpentry',
  },
  painting: {
    ar: 'دهان',
    en: 'Painting',
  },
  cleaning: {
    ar: 'تنظيف و ابادة حشرات',
    en: 'Cleaning and Bug-extermination',
  },
  'water-saving': {
    ar: 'ترشيد مياه',
    en: 'Water Saving',
  },
  paved: {
    ar: 'تبليط',
    en: 'Paved',
  },
  cameras: {
    ar: 'كاميرات',
    en: 'Cameras',
  },
  mobiles: {
    ar: 'جوالات',
    en: 'Mobiles',
  },
  reconditioning: {
    ar: 'ترميم',
    en: 'reconditioning',
  },
  parquet: {
    ar: 'ارضية خشبية',
    en: 'parquet',
  },
  'furniture-upholstery': {
    ar: 'لمفروشات والأثاث',
    en: 'furniture-upholstery',
  },
  aluminum: {
    ar: 'الألومنيوم',
    en: 'aluminum',
  },
  construction: {
    ar: 'اعمال بناء',
    en: 'construction',
  },
  'gypsum-board': {
    ar: 'ألواح الجبس',
    en: 'gypsum-board',
  },
  elevadores: {
    ar: 'المصاعد',
    en: 'elevadores',
  },
};

const ACTIVE_STATUS = {
  true: {
    ar: 'مفعل',
    en: 'Active',
  },
  false: {
    ar: 'معطل',
    en: 'Disabled',
  },
};

const RATINGS = {
  1: {
    ar: 'أقل من 1',
    en: 'less than 1',
  },
  2: {
    ar: 'أقل من 2',
    en: 'less than 2',
  },
  3: {
    ar: 'أقل من 3',
    en: 'less than 3',
  },
  4: {
    ar: 'أقل من 4',
    en: 'less than 4',
  },
  5: {
    ar: 'أقل من 5',
    en: 'less than 5',
  },
};

const CITIES = {
  Jeddah: {
    ar: 'جدة',
    en: 'Jeddah',
  },
  Mecca: {
    ar: 'مكة',
    en: 'Mecca',
  },
  Riyadh: {
    ar: 'الرياض',
    en: 'Riyadh',
  },
  Medina: {
    ar: 'المدينة المنورة',
    en: 'Medina',
  },
  Dammam: {
    ar: 'الدمام',
    en: 'Dammam',
  },
  'Al Khobar': {
    ar: 'الخبر',
    en: 'Al Khobar',
  },
  Dhahran: {
    ar: 'الظهران',
    en: 'Dhahran',
  },
  'Al Ahsa': {
    ar: 'الأحساء',
    en: 'Al Ahsa',
  },
  Abha: {
    ar: 'أبها',
    en: 'Abha',
  },
  'Khamis Mushait': {
    ar: 'خميس مشيط',
    en: 'Khamis Mushait',
  },
  Taif: {
    ar: 'الطائف',
    en: 'Taif',
  },
  Tabuk: {
    ar: 'تبوك',
    en: 'Tabuk',
  },
  Najran: {
    ar: 'نجران',
    en: 'Najran',
  },
  'Al Bahah': {
    ar: 'الباحة',
    en: 'Al Bahah',
  },
  'Al Lith': {
    ar: 'الليث‎',
    en: 'Al Lith',
  },
  'Al Qassim': {
    ar: 'القاسم',
    en: 'Al Qassim',
  },
  Jazan: {
    ar: 'جيزان',
    en: 'Jazan',
  },
  Rabigh: { ar: 'رابغ', en: 'Rabigh' },
  'Al Jubail': { ar: 'الجبيل', en: 'Al Jubail' },
  Buqayq: { ar: 'بقيق', en: 'Buqayq' },
  'Al-Kharj': { ar: 'الخرج', en: 'Al-Kharj' },
  Hail: { ar: 'حائل', en: 'Hail' },
  buraydh: { ar: 'بريدة', en: 'buraydh' },
  'Hafar Al Batin': { ar: 'حفر الباطن', en: 'Hafar Al Batin' },
  Yanbu: { ar: 'ينبع', en: 'Yanbu' },
  '': { ar: 'لا يوجد مدينة', en: 'city not exist' },
};

const USER_STATUSES = [
  {
    filterKey: 'suspended',
    value: 'true',
    name: 'suspended',
    ar: 'متوقف',
    en: 'Suspended',
  },
  {
    filterKey: 'suspended',
    value: 'false',
    name: 'notSuspended',
    ar: 'غير متوقف',
    en: 'Not Suspended',
  },
  {
    filterKey: 'active',
    value: 'true',
    name: 'active',
    ar: 'نشط',
    en: 'ِActive',
  },
  {
    filterKey: 'active',
    value: 'false',
    name: 'notActive',
    ar: 'غير نشط',
    en: 'ِNot Active',
  },
];

const REQUESTS_STATUSES = {
  pending: {
    ar: 'في انتظار اعتماد الطلب',
    en: 'Pending',
    useInFilterRequestsByStatus: true,
  },
  canceled: {
    ar: 'ملغى',
    en: 'Canceled',
    useInFilterRequestsByStatus: true,
  },
  done: {
    ar: 'مكتمل',
    en: 'Done',
    useInFilterRequestsByStatus: true,
  },
  active: {
    ar: 'نشط',
    en: 'Active',
    useInFilterRequestsByStatus: true,
  },
  'timed-out': {
    ar: 'الغي لنفاذ الوقت',
    en: 'Timed out',
    useInFilterRequestsByStatus: true,
  },
  reviewed: {
    ar: 'تم تقييمه',
    en: 'Reviewed',
    useInFilterRequestsByStatus: true,
  },
  postponed: {
    ar: 'مؤجل',
    en: 'Postponed',
    useInFilterRequestsByStatus: true,
  },
  postponedDone: {
    ar: 'طلبات مؤجله ثم اكتملت',
    en: 'PostponedDone',
    useInFilterRequestsByGroupedStatus: true,
  },
  canceledDone: {
    ar: 'طلبات ملفيه ثم اكتملت',
    en: 'CanceledDone',
    useInFilterRequestsByGroupedStatus: true,
  },
  cancelledThenActive: {
    ar: 'طلبات ملفيه ثم تم تنشيطها',
    en: 'CanceledActive',
    useInFilterRequestsByGroupedStatus: true,
  },
  contactYou: {
    ar: 'ابشر بنتواصل معك',
    en: 'Great! we will contact you soon',
  },
};

const TICKETS_STATUS = {
  open: {
    code: 'open',
    ar: 'جارية',
    en: 'Open',
  },
  solving: {
    code: 'solving',
    ar: 'جاري الحل',
    en: 'Solving',
  },
  solved: {
    code: 'solved',
    ar: 'تم الحل',
    en: 'Solved',
  },
};

const PAYMENT_STATUS = {
  true: {
    ar: 'تم الدفع',
    en: 'Done',
  },
  false: {
    ar: 'لم يتم الدفع',
    en: 'Not paid',
  },
};

const PAYMENT_METHOD = {
  cash: {
    ar: 'نقدي',
    en: 'Cash',
  },
  creditcard: {
    ar: 'كرديت كارد',
    en: 'Credit Card',
  },
  mada: {
    ar: 'مدي',
    en: 'mada',
  },
  applepay: {
    ar: 'محفظة ابل',
    en: 'Apple Pay',
  },
  stcpay: {
    ar: 'Stc Pay',
    en: 'Stc Pay',
  },
  stcPayInApp: {
    ar: 'Stc Pay In App',
    en: 'Stc Pay In App',
  },
  tamara: {
    ar: 'تمارا',
    en: 'Tamara',
  },
  wallet: {
    ar: 'المحفظة',
    en: 'wallet',
  },
};

const CANCELLATION_REASONS = {
  cancelledByAdmin: {
    ar: 'تم الإلغاء من قبل المشرف',
    en: 'Cancelled by admin',
  },
  insufficientAppliedTechnicians: {
    ar: 'لا يوجد عدد كافي من الفنيين المقدمين',
    en: 'No enough technicians applied',
  },
  justCheckingTheApp: {
    ar: 'تجربة التطبيق',
    en: 'Trying the app',
  },
  couldNotUnderstandTheApp: {
    ar: 'عدم المقدرة على فهم عمل التطبيق',
    en: 'Could not understand the app workflow',
  },
  highPrice: {
    ar: 'السعر مرتفع',
    en: 'High price',
  },
  technicianNotSuitable: {
    ar: 'الفني غير مناسب',
    en: 'Technician is not suitable',
  },
  technicianWasANoShow: {
    ar: 'تأخر الفني عن الموعد',
    en: 'Technician did not show up',
  },
  technicianRequestedCancellation: {
    ar: 'الفني طلب الإلغاء',
    en: 'Technician requested to cancel',
  },
  other: {
    ar: 'سبب آخر',
    en: 'other',
  },
  requestedByMistake: {
    ar: 'طلب بالخطأ',
    en: 'requested by mistake',
  },
  needNewField: {
    ar: 'احتاج تخصص اخر',
    en: 'Need New Field',
  },
  noAppliedWorker: {
    ar: 'لا يوجد فنى',
    en: 'No Applied Worker',
  },
  theTechnicalPriceHigherThanTheApplication: {
    en: 'The technical price higher than the application',
    ar: 'الفني  طلب  سعر اعلى  من التطبيق'
  },
  feelThePriceOfTheApplicationIsHigh: {
    en: 'feel the price of the application is high',
    ar: 'سعر التطبيق  مرتفع'
  },
  'TheTechnicianRequestedToDelayTheAppointment':{
    en: 'The technician requested to delay the appointment',
    ar: 'الفني  طلب تأخير الموعد'
  },
  lateForTheAppointment: {
    en: 'late for the appointment',
    ar: 'انا بتآخر علي الموعد'
  },
  TheTechnicianWantsToWorkOutsideOfTheApp: {
    en: 'The technician wants to work outside of the app',
    ar: 'الفني  يريد العمل  خارج التطبيق'
  },
  TheTechnicianDoesNotKnowProblemAndRequestedCancellation: {
    en: 'The technician does not know the problem and requested cancellation',
    ar: 'الفني  لا يعرف  حل المشكلة'
  },
  'LocationIsTooFar': {
    ar: 'موقع العميل  بعيد',
    en: 'Location is too far',
  },
  'WorkerIsBusy': {
    ar: 'مشغول  حالياً',
    en: 'I am busy now',
  },
  'LocationIsTooFar': {
    ar: 'موقع العميل  بعيد',
    en: 'Location is too far',
  },
  'CustomerAskedForPostpone': {
    ar: 'العميل طلب تأجيل الموعد',
    en: 'Customer asked for postpone',
  }
}

const CANCELLATION_REASON_TYPES = [
  {
    value: 'positive',
    label: 'سبب إيجابي',
  },
  {
    value: 'negative',
    label: 'سبب سلبي',
  },
];

const TRANSACTION_TYPES = {
  custom: {
    ar: 'custom',
    en: 'custom',
  },
  referral: {
    ar: 'referral',
    en: 'referral',
  },
  payment: {
    ar: 'payment',
    en: 'payment',
  },
  commission: {
    ar: 'عمولة',
    en: 'commission',
  },
  onlinePayment: {
    ar: 'الدفع اونلين',
    en: 'Online Payment',
  },
  donation: {
    en: 'donation for charity',
    ar: 'تبرع لجميعة خيرية',
  },
};

const TRANSACTION_STATUS = {
  approved: {
    ar: 'تمت الموافقة',
    en: 'approved',
  },
  pending: {
    ar: 'معلق',
    en: 'pending',
  },
  rejected: {
    ar: 'مرفوض',
    en: 'rejected',
  },
  paid: {
    ar: 'تم التحويل',
    en: 'paid',
  },
  ready: {
    ar: 'جاري تنفيذ عملية الدفع',
    en: 'ready',
  },
  'customer informed': {
    ar: 'اتم ابلاغ صاحب الحساب بتحويل المبلغ',
    en: 'customer informed',
  },
  'processing payment': {
    ar: 'جاري تنفذ العملية',
    en: 'processing payment',
  },
  'payment failed': {
    ar: 'حدذ خطا فى تنفيذ العملية',
    en: 'payment failed',
  },
  expired: {
    ar: 'انتهى الوقت المخصص لتنفيذ عملية الدفع',
    en: 'expired',
  },
  cancelled: {
    ar: 'تم العاء العملية الدفع',
    en: 'cancelled',
  },
};

const COMMISSION_RATE = {
  0.05: {
    value: 0.05,
  },
  0.06: {
    value: 0.06,
  },
  0.07: {
    value: 0.07,
  },
  0.08: {
    value: 0.08,
  },
  0.09: {
    value: 0.09,
  },
  0.1: {
    value: 0.1,
  },
  0.115: {
    value: 0.115,
  },
  0.15: {
    value: 0.15,
  },
  0.2: {
    value: 0.2,
  },
  0.23: {
    value: 0.23,
  },
};

const TRANSACTION_SIGN = {
  positive: {
    ar: 'positive',
    en: 'positive',
  },
  negative: {
    ar: 'negative',
    en: 'negative',
  },
};

const REVIEWS_TAGS = {
  delay: {
    ar: 'تأخر',
    en: 'Delay',
  },
  notResponsive: {
    ar: 'غير متعاون',
    en: 'Not responsive',
  },
  talkative: {
    ar: 'كثير الكلام',
    en: 'Talkative',
  },
  slow: {
    ar: 'بطيء',
    en: 'Slow',
  },
  lowBehaviour: {
    ar: 'سيّء التعامل',
    en: 'Low behaviour',
  },
  badhygiene: {
    ar: 'سيّء المظهر',
    en: 'Bad hygiene',
  },
  notCommittedToPrices: {
    ar: 'غالي',
    en: 'Not committed to price',
  },
  quick: {
    ar: 'سريع',
    en: 'Quick',
  },
  responsive: {
    ar: 'متعاون',
    en: 'Responsive',
  },
  goodBehaviour: {
    ar: 'خلوق',
    en: 'Good behaviour',
  },
  goodHygiene: {
    ar: 'أنيق',
    en: 'Good hygiene',
  },
  onTime: {
    ar: 'ملتزم بالوقت',
    en: 'On time',
  },
  helpful: {
    ar: 'فنان',
    en: 'Helpful',
  },
};

const ARRIVAL_ACTIVITY = {
  StartMoving: {
    ar: 'في الطريق',
    en: 'Start moving',
  },
  Arrived: {
    ar: 'وصل',
    en: 'Arrived',
  },
};

const DARK_MODE_MAP_STYLE = [
  {
    featureType: 'all',
    elementType: 'all',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'all',
    elementType: 'labels',
    stylers: [
      {
        visibility: 'on',
      },
      {
        saturation: '-100',
      },
    ],
  },
  {
    featureType: 'all',
    elementType: 'labels.text.fill',
    stylers: [
      {
        saturation: 36,
      },
      {
        color: '#000000',
      },
      {
        lightness: 40,
      },
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'all',
    elementType: 'labels.text.stroke',
    stylers: [
      {
        visibility: 'on',
      },
      {
        color: '#000000',
      },
      {
        lightness: 16,
      },
    ],
  },
  {
    featureType: 'all',
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'administrative',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#000000',
      },
      {
        lightness: 20,
      },
    ],
  },
  {
    featureType: 'administrative',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#000000',
      },
      {
        lightness: 17,
      },
      {
        weight: 1.2,
      },
    ],
  },
  {
    featureType: 'landscape',
    elementType: 'geometry',
    stylers: [
      {
        color: '#000000',
      },
      {
        lightness: 20,
      },
    ],
  },
  {
    featureType: 'landscape',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#4d6059',
      },
    ],
  },
  {
    featureType: 'landscape',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#4d6059',
      },
    ],
  },
  {
    featureType: 'landscape.natural',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#4d6059',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [
      {
        lightness: 21,
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#4d6059',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#4d6059',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        visibility: 'on',
      },
      {
        color: '#7f8d89',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#7f8d89',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#7f8d89',
      },
      {
        lightness: 17,
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#7f8d89',
      },
      {
        lightness: 29,
      },
      {
        weight: 0.2,
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [
      {
        color: '#000000',
      },
      {
        lightness: 18,
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#7f8d89',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#7f8d89',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'geometry',
    stylers: [
      {
        color: '#000000',
      },
      {
        lightness: 16,
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#7f8d89',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#7f8d89',
      },
    ],
  },
  {
    featureType: 'transit',
    elementType: 'geometry',
    stylers: [
      {
        color: '#000000',
      },
      {
        lightness: 19,
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'all',
    stylers: [
      {
        color: '#2b3638',
      },
      {
        visibility: 'on',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#2b3638',
      },
      {
        lightness: 17,
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry.fill',
    stylers: [
      {
        color: '#24282b',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry.stroke',
    stylers: [
      {
        color: '#24282b',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.stroke',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
];

const TIME_UNITES = {
  second: {
    ar: 'ثانية',
    en: 'second',
  },
  minute: {
    ar: 'دقيقة',
    en: 'minute',
  },
  hour: {
    ar: 'ساعة',
    en: 'hour',
  },
};

const GENDER = {
  male: {
    ar: 'ذكر',
    en: 'male',
  },
  female: {
    ar: 'أنثى',
    en: 'female',
  },
};

const ACTIVITIES_LOGS = {
  makeWorkerElite: {
    ar: 'جعل الفنى من النخبة',
    en: 'Make Worker Elite',
  },
  deleteReview: {
    ar: 'حذف تقييم',
    en: 'review deletion',
  },
  approveTransaction: {
    ar: 'قبول عملية',
    en: 'transaction approved',
  },
  rejectTransaction: {
    ar: 'رفض عملية',
    en: 'transaction rejected',
  },
  cancelAllPendingOrders: {
    ar: 'إلغاء جميع الأوامر المعلقة',
    en: 'cancel all pending requests older then requestWaitPeriod',
  },
  hideReview: {
    ar: 'اخفاء التقيم',
    en: 'Hide Review',
  },
  unActivateWorker: {
    ar: 'إلغاء تنشيط الفنى',
    en: 'Un Activate Worker',
  },
  activateWorker: {
    ar: 'تنشيط الفنى',
    en: 'Activate Worker',
  },
  changeWorkerCity: {
    ar: 'تغير مدينة الفنى',
    en: 'Change Worker City',
  },
  signUpWorker: {
    ar: 'تسجيل فنى جديد',
    en: 'Sign Up Worker',
  },
  featuring: {
    ar: 'تمييز',
    en: 'feature/de-feature',
  },
  updateFields: {
    ar: 'تعديل تخصصات',
    en: 'fields update',
  },
  customerFollowUps: {
    ar: 'متابعة خدمة العملاء الطلب',
    en: 'Customer Follow Ups',
  },
  operationFollowUps: {
    ar: 'متابعة العمليات الطلب',
    en: 'Operation Follow Ups',
  },
  addCreditsToWorker: {
    ar: 'إضافة رصيد لى الفنى',
    en: 'add Credits To Worker',
  },
  addCreditsToClient: {
    ar: 'إضافة رصيد لى للعميل',
    en: 'add Credits To Client',
  },
  addCompensationToWorker: {
    ar: 'إضافة تعويض للفنى',
    en: 'add Compensation To Worker',
  },
  updateNameOfWorker: {
    ar: 'تعديل اسم الفنى',
    en: 'name update for Worker',
  },
  updateNameOfClient: {
    ar: 'تعديل اسم العميل',
    en: 'name update for Client',
  },
  updateUserNameForWorker: {
    ar: 'تحديث رقم تليفون الفنى',
    en: 'username update For Worker',
  },
  updateUserNameForClient: {
    ar: 'تحديث رقم تليفون العميل',
    en: 'username update For Worker',
  },
  updatePasswordForWorker: {
    ar: 'تغيير كلمة مرور للفنى',
    en: 'password update for worker',
  },
  updatePasswordForAdmin: {
    ar: 'تغيير كلمة مرور للادمن',
    en: 'password update for worker',
  },
  suspendWorker: {
    ar: 'ايقاف الفنى عن العمل',
    en: 'Suspend Worker',
  },
  suspendClient: {
    ar: 'ايقاف العميل من اسنخدام النظام',
    en: 'Suspend Client',
  },
  unsuspendWorker: {
    ar: 'الغاء ايقاف الفنى عن العمل',
    en: 'Unsuspend Worker',
  },
  unsuspendClient: {
    ar: 'الغاء ايقاف العميل من استخدام النظام',
    en: 'unsuspend Client',
  },
  cancelRequest: {
    ar: 'الغاء الطب بواسطة الادمن',
    en: 'cancel Request',
  },
  deleteWorker: {
    ar: 'مسح فنى بواسطة الادمن',
    en: 'delete Worker',
  },
  openTicket: {
    ar: 'فتح مشكلة على الطلب',
    en: 'open Ticket',
  },
  solvingTicket: {
    ar: 'جاري حل الممشكلة المتعلقة بى الطلب',
    en: 'solving Ticket',
  },
  ticketSolved: {
    ar: 'حل المشكلة المتعلقة بى الطلب',
    en: 'ticket Solved',
  },
  createNewRequest: {
    ar: 'طلب خدمة جديده للعميل من لوحة التحكم',
    en: 'create New Request',
  },
  cancelledRequestDone: {
    ar: 'تم انهاء الطلب الملغى',
    en: 'cancelled Request Done',
  },
  enableAutoApplyToRequests: {
    ar: 'تفعيل التقديم على الطلبات التلقائى',
    en: 'enable Auto Apply To Requests',
  },
  disableAutoApplyToRequests: {
    ar: 'الغاء التقديم على الطلبات التلقائى',
    en: 'disable Auto Apply To Requests',
  },
  addCommentOnRequest: {
    ar: 'اضافة تعليق جديد على الطلب',
    en: 'add Comment On Request',
  },
  addCommentOnWorker: {
    ar: 'اضافة تعليق جديد على حساب الفتى',
    en: 'add Comment On Worker',
  },
  addCommentOnClient: {
    ar: 'اضافة تعليق جديد على حساب العميل',
    en: 'add Comment On Client',
  },
  disableMultiFieldsSubscription: {
    ar: 'الغاء العمل باكثر من خدمة',
    en: 'disable Multi Fields Subscription',
  },
  adminChooseTheWorker: {
    ar: 'اختيار فني للطلب بواسطة ادمن',
    en: 'admin chosen the Worker for the request',
  },
};

const GROUPED_TRANSACTION_TYPES = {
  reCharge: {
    message: {
      ar: 'شحن',
      en: 'reCharge',
    },
    query: {
      // type: 'custom',
      customTransactionClassificationCode: '101, 102, 103, 105, 106, 107',
    },
  },
  reChargeClients :{
    message: {
      ar: 'شحن العملاء',
      en: 'reChargeClients',
    },
    query: {
      // type: 'custom',
      customTransactionClassificationCode: '507,508',
    },
  },
  deductions: {
    message: {
      ar: 'خصومات',
      en: 'deductions',
    },
    query: {
      // type: 'custom',
      customTransactionClassificationCode: '200, 201, 202, 203, 204, 205, 206, 207, 208, 209,212',
    },
  },
  awards: {
    message: {
      ar: 'منحة',
      en: 'awards',
    },
    query: {
      // type: 'custom',
      customTransactionClassificationCode: '104',
    },
  },
  commissions: {
    message: {
      ar: 'عمولات',
      en: 'commissions',
    },
    query: { type: 'commission' },
  },
  incomeDeduction: {
    message: {
      ar: 'سحب الدخل',
      en: 'Income Deduction',
    },
    query: {
      worker: true,
      customTransactionClassificationCode: '600',
    },
  },
  subscriptions: {
    message: {
      ar: 'اشتراكات',
      en: 'subscriptions',
    },
    query: {
      worker: true,
      customTransactionClassificationCode: '210,211,213',
    },
  },
  donation: {
    message: {
      ar: 'تبرع لجميعة خيرية',
      en: 'donation for charity',
    },
    query: {
      customTransactionClassificationCode: '700',
    },
  },
  cashBack: {
    message: {
      ar: 'كاش باك',
      en: 'cash back',
    },
    query: {
      customTransactionClassificationCode: '800',
    },
  },
};

const CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES = {
  reCharge: [
    {
      code: '101',
      name: {
        en: 'deposit - cash',
        ar: 'نقدي',
      },
    },
    {
      code: '102',
      name: {
        en: 'deposit - bank transfer - Ahly',
        ar: 'تحويل بنكي- اهلي',
      },
    },
    {
      code: '105',
      name: {
        en: 'deposit - bank transfer - Rajhi',
        ar: 'تحويل بنكي- راجحي',
      },
    },
    {
      code: '103',
      name: {
        en: 'deposit - cheque',
        ar: 'إيداع - شيك',
      },
    },
    {
      code: '106',
      name: {
        en: 'deposit - bank transfer - STC',
        ar: 'تحويل بنكى- stc',
      },
    },
    {
      code: '107',
      name: {
        en: 'Recharge His Balance From His Income Wallet',
        ar: 'شحن رصيد من محفظة الدخل',
      },
    },
  ],
  reChargeClients: [
    {
      code: '507',
      name: {
        en: 'STC Pay',
        ar: 'شحن العملاء - STC Pay',
      },
    },
    {
      code: '508',
      name: {
        en: 'moyasar',
        ar: 'شحن العملاء - ميسر',
      },
    },
  ],
  deductions: [
    {
      code: '200',
      name: {
        en: 'deduction',
        ar: 'حسم',
      },
    },
    {
      code: '201',
      name: {
        en: 'deduction - carelessness',
        ar: 'حسم - تهاون وإهمال',
      },
    },
    {
      code: '202',
      name: {
        en: 'deduction - client compliant',
        ar: 'حسم - شكوى عميل',
      },
    },
    {
      code: '203',
      name: {
        en: 'deduction - fraud',
        ar: 'حسم - احتيال',
      },
    },
    {
      code: '204',
      name: {
        en: 'deduction - requests cancellation',
        ar: 'حسم - إلغاء طلبات',
      },
    },
    {
      code: '205',
      name: {
        en: 'deduction - low quality work',
        ar: 'حسم - قلة جودة العمل',
      },
    },
    {
      code: '206',
      name: {
        en: 'deduction - late appointments',
        ar: 'حسم - تأخير المواعيد',
      },
    },
    {
      code: '207',
      name: {
        en: 'deduction - harassment',
        ar: 'حسم - تحرش',
      },
    },
    {
      code: '208',
      name: {
        en: 'deduction - personal hygiene issue',
        ar: 'حسم - مشكلة نظافة شخصية',
      },
    },
    {
      code: '209',
      name: {
        en: 'deduction - uniform issue',
        ar: 'حسم - مشكلة في الزي الموحد',
      },
    },
    {
      code: '212',
      name: {
        en: 'deduction - temporary credits for non requests',
        ar: 'حسم - الرصيد المؤقت لعدم اتمام طلبات مكتملة',
      },
    },
  ],
  awards: [
    {
      code: '104',
      name: {
        en: 'deposit - award',
        ar: 'منحة',
      },
    },
  ],
  commissions: [],
  incomeDeduction: [],
  subscriptions: [
    {
      code: '210',
      name: {
        en: 'subscription - auto applied to requests',
        ar: 'اشتراك التقديم التلقائى على الطلبات',
      },
    },
    {
      code: '211',
      name: {
        en: 'subscription - multi fields',
        ar: 'اشتراك تعدد الخدمات',
      },
    },
    {
      code: '213',
      name: {
        en: 'subscription - featured',
        ar: 'اشتراك جعل الفنى مميز',
      },
    },
  ],
  donation: [],
  cashBack: [],
};

const POSTPONE_REASONS = {
  partsNotAvailableNow: {
    ar: 'العميل طلب التآجيل',
    en: 'customer request to postpone',
  },
  preparePartsForCustomer: {
    ar: 'توفير مواد للعميل',
    en: 'prepare parts for customer',
  },
  customerNotPayThePill: {
    ar: 'العميل لم يدفع الاجرة',
    en: 'customer Do not pay the pill',
  },
};

const ACTIONS = {
  serviceRequest: {
    ar: 'طلب خدمة',
    en: 'serviceRequest',
  },
  externalUrl: {
    ar: 'عنوان الكترونى خارجى',
    en: 'externalUrl',
  },
};

const WORK_LIST_FOR_EVERY_SERVICE = {
  ar: {
    'home-appliances': [
      {
        "code": "Does not work at all on less than 55 inches",
        "name": "صيانة تلفزيون لا يعمل \"أقل من 55 بوصة\"",
        "cost": 150,
        "deviceCode": "tv"
      },
      {
        "code": "Does not work at all on more than 55 inches",
        "name": "صيانة تلفزيون لا يعمل \"55 بوصة وأكثر\"",
        "cost": 200,
        "deviceCode": "tv"
      },
      {
        "code": "Turning on and off the TV screen less than 55 inches",
        "name": "فتح واغلاق الشاشة  \"أقل من 55 بوصة\"",
        "cost": 150,
        "deviceCode": "tv"
      },
      {
        "code": "Turning on and off the TV screen more than 55 inches",
        "name": "فتح واغلاق الشاشة  \"55 بوصة وأكثر\"",
        "cost": 200,
        "deviceCode": "tv"
      },
      {
        "code": "TV screen does not show anything less than 55 inches",
        "name": "صيانة الصورة  \"أقل من 55 بوصة\"",
        "cost": 190,
        "deviceCode": "tv"
      },
      {
        "code": "TV screen does not show anything more than 55 inches",
        "name": "صيانة الصورة \"55 بوصة وأكثر\"",
        "cost": 260,
        "deviceCode": "tv"
      },
      {
        "code": "Sound problem in TV less than 55 inches",
        "name": "صيانة الصوت \"أقل من 55 بوصة\"",
        "cost": 130,
        "deviceCode": "tv"
      },
      {
        "code": "Sound problem in TV more than 55 inches",
        "name": "صيانة الصوت \"55 بوصة وأكثر\"",
        "cost": 180,
        "deviceCode": "tv"
      },
      {
        "code": "The remote control does not work for a TV less than 55 inches",
        "name": "صيانة جهاز التحكم عن بعد \"أقل من 55 بوصة\"",
        "cost": 120,
        "deviceCode": "tv"
      },
      {
        "code": "The remote control does not work for a TV more than 55 inches",
        "name": "صيانة جهاز التحكم عن بعد \"55 بوصة وأكثر\"",
        "cost": 160,
        "deviceCode": "tv"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "tv"
      },
      {
        "code": "Removing and installing the electronic board",
        "name": "فك وتركيب اللوحة الالكترونية",
        "cost": 120,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Removing and installing the drying machine",
        "name": "فك وتركيب مكينة التنشيف",
        "cost": 180,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Removing and installing the washing machine",
        "name": "فك وتركيب مكينة الغسيل",
        "cost": 150,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Removing and installing an automatic washing machine door",
        "name": "فك وتركيب باب ",
        "cost": 120,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Removing and installing the stabilizer bar",
        "name": "فك وتركيب قضيب التعليق التوازن",
        "cost": 130,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "water drain repair",
        "name": "صيانة تصريف الماء",
        "cost": 120,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "water flow repair",
        "name": "صيانة تدفق الماء",
        "cost": 100,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Electrical seam repair",
        "name": "صيانة إلتماس كهربائي",
        "cost": 100,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Electricity access repair",
        "name": "إصلاح وصول التيار الكهربائي",
        "cost": 70,
        "deviceCode": "automatic-washing-machine"
      },
      {
        "code": "Decoding and installing the timer",
        "name": "فك وتركيب التايمر",
        "cost": 90,
        "deviceCode": "normal-washing-machine"
      },
      {
        "code": "Dismantling and installing the drying machine",
        "name": "فك وتركيب مكينة التنشيف",
        "cost": 130,
        "deviceCode": "normal-washing-machine"
      },
      {
        "code": "Dismantling and installing the washing machine",
        "name": "فك وتركيب مكينة الغسيل",
        "cost": 120,
        "deviceCode": "normal-washing-machine"
      },
      {
        "code": "Change knobs for ovens",
        "name": "تغيير المفاتيح",
        "cost": 120,
        "deviceCode": "oven"
      },
      {
        "code": "Change the door furnaces for ovens",
        "name": "تغيير مفصلات الباب",
        "cost": 120,
        "deviceCode": "oven"
      },
      {
        "code": "Clean the oven",
        "name": "تنظيف ",
        "cost": 160,
        "deviceCode": "oven"
      },
      {
        "code": "Repair of the smuggling of the oven",
        "name": "صيانة تهريب",
        "cost": 160,
        "deviceCode": "oven"
      },
      {
        "code": "Repair your microwave",
        "name": "صيانة مايكرويف",
        "cost": 200,
        "deviceCode": "oven"
      },
      {
        "code": "Change composer refrigerator without freon",
        "name": "تغيير كمبروسر ",
        "cost": 170,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change an external fan for refrigerator",
        "name": "تغيير المروحة الخارجية",
        "cost": 130,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change an internal fan for refrigerator",
        "name": "تغيير المروحة الداخلية",
        "cost": 130,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change the word for refrigerator",
        "name": "تغيير الدفاية",
        "cost": 150,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change the sensitive dumb",
        "name": "تغيير الحساس \"دبفارست\"",
        "cost": 90,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Filling of American Freon",
        "name": "تعبئة فريون امريكي",
        "cost": 200,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Filling of Indian Freon for the refrigerator according to the quantity of the Freon",
        "name": "تعبئة فريون هندي",
        "cost": 170,
        "deviceCode": "refrigerator"
      },
      {
        "code": "External infusion repair for refrigerator without filling freon",
        "name": "صيانة تهريب خارجي",
        "cost": 150,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change Taimer",
        "name": "تغيير وضبط التايمر",
        "cost": 90,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change composer refrigerator 1/6 - 1/5 - 1/4 - 1/3",
        "name": "تغيير كمبروسر ثلاجة 1/6 - 1/5 - 1/4 - 1/3",
        "cost": 170,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Change Compression Refrigerator 1/2 - 3/4 - 1",
        "name": "تغيير كمبروسر ثلاجة 1/2 - 3/4 - 1",
        "cost": 170,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Repair of external smuggling with US Freon Filling R134",
        "name": "صيانة تهريب خارجي + تعبئة فريون أمريكي R134",
        "cost": 250,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Repair of external smuggling with American Freon Filling R12",
        "name": "صيانة تهريب خارجي + تعبئة فريون أمريكي R12",
        "cost": 330,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Repair of external smuggling with Chinese Freon filling R134",
        "name": "صيانة تهريب خارجي + تعبئة فريون صيني R134",
        "cost": 330,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Repair of external smuggling with Chinese Freon Filling",
        "name": "صيانة تهريب خارجي + تعبئة فريون صيني R12",
        "cost": 190,
        "deviceCode": "refrigerator"
      },
      {
        "code": "Repair of external smuggling with Indian Freon filling R134",
        "name": "صيانة تهريب خارجي + تعبئة فريون هندي R134",
        "cost": 290,
        "deviceCode": "refrigerator"
      },
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    'air-conditioning': [
      {
        "code": "Installation of air conditioner",
        "name": "تركيب مكيف دولاب",
        "cost": 300,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Installation and decomposition of an electronic plate",
        "name": "تركيب وفك لوحة الكترونية",
        "cost": 130,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Installation and decomposition of external fan",
        "name": "تركيب وفك مروحة خارجية",
        "cost": 150,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Installation of cassette air conditioner",
        "name": "تركيب مكيف كاسيت",
        "cost": 650,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Change the Split interior crystal",
        "name": "تغيير بلور داخلي سبليت",
        "cost": 200,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Change Dynamo Indoor Machine",
        "name": "تغيير دينمو مكينه داخلي",
        "cost": 150,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Change composer 36 - 48 - 60 units",
        "name": "تغيير كمبروسر 36 – 48 – 60 وحدة",
        "cost": 150,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Change place - Mobbat - Capster - Battery",
        "name": "تغيير مكتن – بوبينه – كوبستر -- بطارية",
        "cost": 70,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Change and install Contackur",
        "name": "تغيير وتركيب كونتاكتر",
        "cost": 80,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "central-air-condition"
      },
      {
        "code": "Installation of new window with electricity connection",
        "name": "تركيب مكيف جديد مع توصيل الكهرباء",
        "cost": 80,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "Demo change",
        "name": "تغيير دينمو",
        "cost": 90,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "Only air conditioner washing - window",
        "name": "تنظيف",
        "cost": 80,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "Air Conditioner with Freon - Window",
        "name": "تنظيف + تعبئة فريون",
        "cost": 110,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "The laundry is an internal machine with the cleaning of the stirring without decomposition - nets",
        "name": "غسيل داخلي مع تنظيف المجرى بدون فك ",
        "cost": 120,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "The installation and installation of an electric air conditioner inside the house",
        "name": "فك وتركيب ونقل داخل المنزل",
        "cost": 80,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "Freon smuggling repair with only welding - window",
        "name": "اصلاح تهريب الفريون مع اللحام",
        "cost": 90,
        "deviceCode": "window-air-condition"
      },
      {
        "code": "Install new spice without internal extension",
        "name": "تركيب مكيف جديد بدون تمديد داخلي",
        "cost": 250,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Installation and decomposition of an electronic plate - Split",
        "name": "تركيب وفك اللوحة الكترونية ",
        "cost": 130,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Installation and decomposition fan - Split",
        "name": "تركيب وفك مروحة خارجية ",
        "cost": 150,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Installation and decomposition and transportation of an air conditioner outside the house",
        "name": "فك ونقل وتركيب المكيف خارج المنزل",
        "cost": 300,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Installation and decomposition and transportation in the house",
        "name": "فك ونقل وتركيب المكيف داخل المنزل",
        "cost": 250,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Change Dynamo External Machine",
        "name": "تغيير دينمو مكينة خارجي",
        "cost": 120,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Washing air conditioner and external machine with freon",
        "name": "تنظيف داخلي وخارجي + الفريون",
        "cost": 250,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Washing and outside without decoding - Split",
        "name": "تنظيف \"داخلي و خارجي بدون فك\"",
        "cost": 120,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Washing machine and installation - Split",
        "name": "غسيل داخلي فك وتركيب ",
        "cost": 150,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Washing with an outside without decoding - Split",
        "name": "غسيل داخلي و خارجي بدون فك ",
        "cost": 150,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Washing and external machine with jaw - Split",
        "name": "غسيل داخلي وخارجي مع الفك ",
        "cost": 200,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Leakage - Indoor - Split with cleaning filters",
        "name": "تصليح التسريبات مع تنظيف الفلاتر",
        "cost": 100,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Dynamo Air Conditioner - An external decoding machine",
        "name": "دينمو المكيف - مكينة فك وتركيب خارجي",
        "cost": 90,
        "deviceCode": "split-air-condition"
      },
      {
        "code": "Unfurnished Air Conditioner",
        "name": "فك مكيف",
        "cost": 120,
        "deviceCode": "split-air-condition"
      },
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    plumbing: [
      {
        "code": "installOrReplaceDynamo",
        "name": "تركيب او تغيير دينمو",
        "cost": 160,
        "deviceCode": "Installation"
      },
      {
        "code": "installOrReplaceHiddenHeater",
        "name": "تركيب أو تغيير سخان مخفي",
        "cost": 160,
        "deviceCode": "Installation"
      },
      {
        "code": "installOneSprinklerShowerHead",
        "name": "تركيب سماعة دش",
        "cost": 50,
        "deviceCode": "Installation"
      },
      {
        "code": "installTwoOrMoreSprinklerShowerHead",
        "name": "غسيل داخلي وخارجي بدون فك",
        "cost": 40,
        "deviceCode": "Installation"
      },
      {
        "code": "installBathroomDrainage",
        "name": "تركيب صفاية",
        "cost": 60,
        "deviceCode": "Installation"
      },
      {
        "code": "installShowerRoom",
        "name": "تركيب كابينة مروش",
        "cost": 220,
        "deviceCode": "Installation"
      },
      {
        "code": "installHeater",
        "name": "تركيب سخان",
        "cost": 90,
        "deviceCode": "Installation"
      },
      {
        "code": "installUniversalFlush",
        "name": "تركيب سيفون افرنجي",
        "cost": 90,
        "deviceCode": "Installation"
      },
      {
        "code": "installArabicFlush",
        "name": "تركيب سيفون عربي",
        "cost": 90,
        "deviceCode": "Installation"
      },
      {
        "code": "installWashbasin",
        "name": "تركيب مغسلة",
        "cost": 130,
        "deviceCode": "Installation"
      },
      {
        "code": "installWashbasinCabinet",
        "name": "تركيب مغسلة دولاب",
        "cost": 200,
        "deviceCode": "Installation"
      },
      {
        "code": "installUniversalToiletSeat",
        "name": "تركيب كرسي افرنجي فقط",
        "cost": 140,
        "deviceCode": "Installation"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "Installation"
      },
      {
        "code": "replaceAUniversalToiletSeatCover",
        "name": "تغيير غطاء كرسي افرنجي",
        "cost": 60,
        "deviceCode": "change"
      },
      {
        "code": "replaceArabicToiletSeat",
        "name": "تغيير كرسي عربي",
        "cost": 170,
        "deviceCode": "change"
      },
      {
        "code": "replaceElectricHeater",
        "name": "تغيير قلب سخان عادي",
        "cost": 60,
        "deviceCode": "change"
      },
      {
        "code": "replaceAToiletFlushPumpArabicAndUniversal",
        "name": "تغيير ماكينة سيفون افرنجي أو عربي",
        "cost": 70,
        "deviceCode": "change"
      },
      {
        "code": "replaceBathtub1Foot",
        "name": "تغيير بانيو قدم",
        "cost": 150,
        "deviceCode": "change"
      },
      {
        "code": "replaceMixerBasinOrWall",
        "name": "تغيير خلاط حوض أو جدار",
        "cost": 90,
        "deviceCode": "change"
      },
      {
        "code": "replaceOneSprayerForToilet",
        "name": "تغيير شطاف",
        "cost": 40,
        "deviceCode": "change"
      },
      {
        "code": "replaceTwoOrMoreSprayerForToilet",
        "name": "تغيير 2 وأكثر شطاف",
        "cost": 30,
        "deviceCode": "change"
      },
      {
        "code": "replaceFloatValve",
        "name": "تغيير عوامة الخزان",
        "cost": 80,
        "deviceCode": "change"
      },
      {
        "code": "angleOneValve",
        "name": "تغيير ليات مع محبس زاوية",
        "cost": 50,
        "deviceCode": "change"
      },
      {
        "code": "angleTwoOrMoreValve",
        "name": "تغيير 2 وأكثر ليات مع محبس زاوية",
        "cost": 35,
        "deviceCode": "change"
      },
      {
        "code": "replaceOneStopcockWithHose",
        "name": "تغيير محبس زاوية مع لي",
        "cost": 50,
        "deviceCode": "change"
      },
      {
        "code": "replaceTwoOrMoreStopcockWithHose",
        "name": "تغيير 2 وأكثر محبس زاوية مع لي",
        "cost": 35,
        "deviceCode": "change"
      },
      {
        "code": "washbasinWasteTrap",
        "name": "تغيير هراب عادي",
        "cost": 60,
        "deviceCode": "change"
      },
      {
        "code": "koreanWashbasinWasteTrap",
        "name": "تغيير هراب كوري",
        "cost": 90,
        "deviceCode": "change"
      },
      {
        "code": "extensionAndDischargeOfAWashbasinEstablishing",
        "name": "تمديد وتصريف غسالة - تأسيس",
        "cost": 220,
        "deviceCode": "others"
      },
      {
        "code": "extensionAndDischargeOfAWashbasinOnlyLinkHose",
        "name": "تمديد وتصريف غسالة - ربط الليات فقط",
        "cost": 80,
        "deviceCode": "others"
      },
      {
        "code": "drainageBlockage",
        "name": "تسليك انسداد التصريف",
        "cost": 200,
        "deviceCode": "others"
      },
      {
        "code": "Tsic blockage kitchen basin",
        "name": "تسليك انسداد حوض المطبخ",
        "cost": 120,
        "deviceCode": "others"
      },
      {
        "code": "RemoveUniversalToiletSeatAndInstallToAnotherPlace",
        "name": "فك وتركيب كرسي افرنجي جديد",
        "cost": 160,
        "deviceCode": "others"
      },
      {
        code: 'other',
        name: 'خدمة أخرى',
        cost: 0,
      },
    ],
    'satellite-tv': [
      {
        "code": "Internal programming",
        "name": "برمجة داخلية",
        "cost": 60,
        "deviceCode": "programming"
      },
      {
        "code": "Shower programming (weight dish)",
        "name": "برمجة دش (وزن صحن)",
        "cost": 80,
        "deviceCode": "programming"
      },
      {
        "code": "A description of the weight of a dish",
        "name": "برمجة رسيفر + وزن صحن",
        "cost": 90,
        "deviceCode": "programming"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "programming"
      },
      {
        "code": "Installation of heads 4 lines",
        "name": "تركيب رأس 4 خطوط",
        "cost": 150,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of one line",
        "name": "تركيب رأس خط واحد",
        "cost": 60,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of two lines",
        "name": "تركيب رأس خطين",
        "cost": 100,
        "deviceCode": "Installation"
      },
      {
        "code": "Install full dish with weight and programming",
        "name": "تركيب صحن + وزن وبرمجة",
        "cost": 130,
        "deviceCode": "Installation"
      },
      {
        "code": "New flood outside wall",
        "name": "تسليك جديد \"خارج الجدار\"",
        "cost": 70,
        "deviceCode": "others"
      },
      {
        "code": "New Tsic inside the wall",
        "name": "تسليك جديد \"داخل الجدار\"",
        "cost": 150,
        "deviceCode": "others"
      },
      {
        "code": "Commentary TV on the wall",
        "name": "تعليق تلفزيون على الجدار \"صغير\"",
        "cost": 70,
        "deviceCode": "others"
      },
      {
        "code": "Suspension a large TV on the wall",
        "name": "تعليق تلفزيون على الجدار \"كبير\"",
        "cost": 90,
        "deviceCode": "others"
      },
      {
        "code": "Move a shower to another place",
        "name": "نقل دش إلى مكان آخر",
        "cost": 150,
        "deviceCode": "others"
      },
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    electrical: [
      {
        "code": "installExhaustFan",
        "name": "تركيب مروحة شفاط",
        "cost": 70,
        "deviceCode": "Installation"
      },
      {
        "code": "installBathroomHeater",
        "name": "تركيب سخان",
        "cost": 90,
        "deviceCode": "Installation"
      },
      {
        "code": "installElectricMeter",
        "name": "تركيب طبلون داخلي 12/24",
        "cost": 370,
        "deviceCode": "Installation"
      },
      {
        "code": "installOneSpotlightWithHolesLargeSize",
        "name": "تركيب سبوت لايت + تخريم  \"الحجم الكبير\"",
        "cost": 70,
        "deviceCode": "Installation"
      },
      {
        "code": "installTwoOrMoreSpotlightWithHolesLargeSize",
        "name": "تركيب 2 وأكثر سبوت لايت + التخريم  \"حجم كبير\"",
        "cost": 12,
        "deviceCode": "Installation"
      },
      {
        "code": "installOneSpotlightWithHolesSmallSize",
        "name": "تركيب سبوت لايت مع التخريم \"الحجم الصغير\"",
        "cost": 60,
        "deviceCode": "Installation"
      },
      {
        "code": "installTwoOrMoreSpotlightWithHolesSmallSize",
        "name": "تركيب سبوت لايت + التخريم \"حجم صغير\"",
        "cost": 10,
        "deviceCode": "Installation"
      },
      {
        "code": "Install your new intercom",
        "name": "تركيب انتركوم",
        "cost": 300,
        "deviceCode": "Installation"
      },
      {
        "code": "Install normal bell",
        "name": "تركيب جرس",
        "cost": 80,
        "deviceCode": "Installation"
      },
      {
        "code": "installSwitchesInTheGround",
        "name": "تركيب فيش أرضي",
        "cost": 40,
        "deviceCode": "Installation"
      },
      {
        "code": "installOneElectronicSwitches",
        "name": "تركيب فيش كهرباء",
        "cost": 40,
        "deviceCode": "Installation"
      },
      {
        "code": "installTwoOrMoreElectronicSwitches",
        "name": "تركيب 2 وأكثر أفياش كهرباء",
        "cost": 20,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of light cell - photospelliate",
        "name": "تركيب مفتاح خلية ضوئية - فوتوسيل",
        "cost": 40,
        "deviceCode": "Installation"
      },
      {
        "code": "installOneSwitch(AC-Stove-Heater)",
        "name": "تركيب مفتاح مكيف - فرن - سخان",
        "cost": 40,
        "deviceCode": "Installation"
      },
      {
        "code": "installTwoOrMoreSwitch(AC-Stove-Heater)",
        "name": "تركيب 2 وأكثر مفتاح مكيف - فرن - سخان",
        "cost": 30,
        "deviceCode": "Installation"
      },
      {
        "code": "installOneHiddenLedStrips",
        "name": "تركيب متر شريط ليد مخفي بالمتر",
        "cost": 80,
        "deviceCode": "Installation"
      },
      {
        "code": "installTwoOrMoreHiddenLedStrips",
        "name": "تركيب متر فأكثر شريط ليد مخفي بالمتر",
        "cost": 7,
        "deviceCode": "Installation"
      },
      {
        "code": "Install new smartphone",
        "name": "تثبيت جرس الذكي جديد",
        "cost": 340,
        "deviceCode": "Installation"
      },
      {
        "code": "Install the smart lock",
        "name": "تثبيت القفل الذكي",
        "cost": 128,
        "deviceCode": "Installation"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "Installation"
      },
      {
        "code": "replaceOneSpotlightSmallSize",
        "name": "تغيير سبوت لايت \" حجم صغير\"",
        "cost": 50,
        "deviceCode": "change"
      },
      {
        "code": "replaceTwoOrMoreSpotlightSmallSize",
        "name": "تغيير 2 وأكثر سبوت لايت \"حجم صغير\"",
        "cost": 6,
        "deviceCode": "change"
      },
      {
        "code": "replaceOneSpotlightLargeSize",
        "name": "تغيير سبوت لايت \"حجم كبير\"",
        "cost": 60,
        "deviceCode": "change"
      },
      {
        "code": "replaceTwoOrMoreSpotlightLargeSize",
        "name": "تغيير 2 وأكثر سبوت لايت \"حجم كبير\"",
        "cost": 7,
        "deviceCode": "change"
      },
      {
        "code": "replaceRoofLights",
        "name": "تغيير لمبات السطح",
        "cost": 60,
        "deviceCode": "change"
      },
      {
        "code": "replaceSubKeyOfElectricMeter",
        "name": "تغيير مفتاح فرعي للطبلون",
        "cost": 150,
        "deviceCode": "change"
      },
      {
        "code": "Change the smart bell",
        "name": "تغيير الجرس الذكي",
        "cost": 280,
        "deviceCode": "change"
      },
      {
        "code": "replaceRegularElectronicSwitches",
        "name": "تغيير فيش كهرباء عادي",
        "cost": 20,
        "deviceCode": "change"
      },
      {
        "code": "Change the electric bell transformers",
        "name": "تغيير المحولات الكهربائية الجرس",
        "cost": 170,
        "deviceCode": "change"
      },
      {
        "code": "Wall Candle - 400 Scouts",
        "name": "شمعة جداري \"٤٠٠ كشاف\"",
        "cost": 100,
        "deviceCode": "others"
      },
      {
        "code": "Candle (400)",
        "name": "شمعة عمود \" ٤٠٠\"",
        "cost": 130,
        "deviceCode": "others"
      },
      {
        "code": "electricMeterPrimaryKey",
        "name": "مفتاح طبلون رئيسي",
        "cost": 150,
        "deviceCode": "others"
      },
      {
        "code": "Repair of Intercom",
        "name": "صيانة إنتركوم",
        "cost": 150,
        "deviceCode": "others"
      },
      {
        "code": "Cable repair bell",
        "name": "كابل إصلاح الجرس",
        "cost": 170,
        "deviceCode": "others"
      },
      {
        "code": "Extension of electricity for washing machine or clothing - 6 mm",
        "name": "تمديد كهرباء غسالة أو نشافة \"6 ملم\"",
        "cost": 120,
        "deviceCode": "others"
      },
      {
        "code": "Electricity conversion from 110 - 220 full apartment",
        "name": "تحويل كهرباء من ١١٠ - ٢٢٠ شقة كاملة",
        "cost": 250,
        "deviceCode": "others"
      },
      {
        code: 'other',
        name: 'خدمة أخرى',
        cost: 0,
      },
    ],
    carpentry: [
      {
        "code": "Installation of a Saudi door or a full persistent",
        "name": "تركيب باب سعودي أو مقنو كامل",
        "cost": 230,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of door brochure",
        "name": "تركيب برواز باب",
        "cost": 80,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of IKEA wheel - small",
        "name": "تركيب دولاب ايكيا \"صغير\"",
        "cost": 150,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of IKEA - Large",
        "name": "تركيب دولاب ايكيا \"كبير\"",
        "cost": 300,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of IKEA wheel - middle",
        "name": "تركيب دولاب ايكيا  \"وسط\"",
        "cost": 220,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of a national wheel",
        "name": "تركيب دولاب وطني",
        "cost": 200,
        "deviceCode": "Installation"
      },
      {
        "code": "Install new office",
        "name": "تركيب مكتب جديد",
        "cost": 200,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of air conditioner frame",
        "name": "تركيب برواز مكيف",
        "cost": 59,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of hairstyle",
        "name": "تركيب تسريحة",
        "cost": 120,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation One panel",
        "name": "تركيب لوحة ",
        "cost": null,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation TwoOrMore panel",
        "name": "تركيب أكثر من لوحة",
        "cost": 30,
        "deviceCode": "Installation"
      },
      {
        "code": "Installation of mirrors",
        "name": "تركيب مرايا",
        "cost": 60,
        "deviceCode": "Installation"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "Installation"
      },
      {
        "code": "Unpack and install the door",
        "name": "فك و تركيب باب",
        "cost": 170,
        "deviceCode": "Installation change"
      },
      {
        "code": "Decoding IKEA - National",
        "name": "فك دولاب ايكيا",
        "cost": 150,
        "deviceCode": "Installation change"
      },
      {
        "code": "Decoding and installation bed",
        "name": "فك و تركيب سرير",
        "cost": 170,
        "deviceCode": "Installation change"
      },
      {
        "code": "Decode",
        "name": "فك مكتب",
        "cost": 130,
        "deviceCode": "Installation change"
      },
      {
        "code": "Unpack and install curtain",
        "name": "فك و تركيب ستارة",
        "cost": 90,
        "deviceCode": "Installation change"
      },
      {
        "code": "Decoding and installing a shelf",
        "name": "فك و تركيب رف",
        "cost": 60,
        "deviceCode": "Installation change"
      },
      {
        "code": "Unpack and install curtain 3 and Joe - big",
        "name": "فك و تركيب ستارة 3 وجوه \"كبيرة\"",
        "cost": 140,
        "deviceCode": "Installation change"
      },
      {
        "code": "Cut the door",
        "name": "قص باب",
        "cost": 69,
        "deviceCode": "others"
      },
      {
        "code": "Open a closed door",
        "name": "فتح باب مغلق",
        "cost": 100,
        "deviceCode": "others"
      },
      {
        "code": "Door repair - abrasion, weight, installation lashes",
        "name": "صيانة باب - كشط / وزن/ تركيب جلدة",
        "cost": 90,
        "deviceCode": "others"
      },
      {
        "code": "Mounting Kellon",
        "name": "تركيب كيلون",
        "cost": 69,
        "deviceCode": "others"
      },
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    mobiles: [
      {
        "code": "Change the iPhone 5G screen",
        "name": "تغيير شاشة IPhone 5G",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i Phone 5s",
        "name": "تغيير شاشة IPhone 5S",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone 5c",
        "name": "تغيير شاشة IPhone 5C",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change the iPhone 6G screen",
        "name": "تغيير شاشة IPhone 6G",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone +6",
        "name": "تغيير شاشة IPhone +6",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change i Phone 6s",
        "name": "تغيير شاشة IPhone 6S",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone + 6s",
        "name": "تغيير شاشة IPhone +6S",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone 7g",
        "name": "تغيير شاشة IPhone 7G",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone +7",
        "name": "تغيير شاشة IPhone +7",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Change the iPhone 8G screen",
        "name": "تغيير شاشة IPhone 8G",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone +8",
        "name": "تغيير شاشة IPhone +8",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Change your phone screen x",
        "name": "تغيير شاشة IPhone X",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "Change the i phone XS screen",
        "name": "تغيير شاشة IPhone Xs",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "Change screen i phone XR",
        "name": "تغيير شاشة IPhone Xr",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "Change the iPhone XSMAX screen",
        "name": "تغيير شاشة IPhone xsmax",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "IPhone 5 - 6 - 6 S - 6 S Plus",
        "name": "صيانة المايكرفون ايفون 5 - 6 - 6اس - 6اس بلس",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "IPhone 7 - 7 Plus - 8 - 8 Plus - X",
        "name": "صيانة المايكرفون \"ايفون 7 - 7 بلس - 8 - 8 بلس - x\"",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Headset for iPhone 5 - 6 - 6 S - 6 S Plus",
        "name": "صيانة مشاكل مكبر الصوت \"ايفون 5 - 6 - 6 اس - 6 اس بلس\"",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Headset for iPhone 7 - 7 Plus",
        "name": "صيانة مشاكل مكبر الصوت \"ايفون 7 - 7 بلس\"",
        "cost": 350,
        "deviceCode": "fix"
      },
      {
        "code": "Headset - iPhone 8 - 8 Plus - X",
        "name": "صيانة مشاكل مكبر الصوت \"ايفون 8 - 8 بلس - x\"",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Problem with Mobile Shipping iPhone 5 - 6 - 6 Plus - 6 S - 6 S Plus",
        "name": "صيانة مدخل الشحن \"ايفون 5 - 6 - 6بلس - 6 اس - 6 اس بلس\"",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Mobile Shipping Problem iPhone 7 - 7 Plus - 8 - 8 Plus - X",
        "name": "صيانة مدخل الشحن \"ايفون 7 - 7 بلس - 8 - 8 بلس - X\"",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "A problem in the battery from iPhone 5 to 6 S Plus",
        "name": "صيانة البطارية \"ايفون 5 الى  6S بلس\"",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "A problem in the battery from iPhone 7 to iPhone x",
        "name": "صيانة البطارية \"ايفون 7 الى X\"",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Problem with iPhone 5 to 6 S",
        "name": "صيانة الكاميرا \"ايفون 5 الى 6S\"",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Problem with iPhone 7 to X",
        "name": "صيانة الكاميرا \"ايفون 7 الى x\"",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Software Software",
        "name": "صيانة مشاكل البرمجة",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Network problems",
        "name": "صيانة مشاكل الشبكة",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "Mobile does not work can not be operated",
        "name": "الجوال لا يمكن تشغيله",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "Reference problem",
        "name": "مشكلة في الاشارة",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "I do not shake",
        "name": "صيانة الاهتزاز",
        "cost": 250,
        "deviceCode": "fix"
      },
      {
        "code": "Problem in side buttons, audio",
        "name": "صيانة الأزرار الجانبية والصوت",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Touch problem",
        "name": "صيانة اللمس",
        "cost": 280,
        "deviceCode": "fix"
      },
      {
        "code": "Water scares",
        "name": "صيانة خرابات الماء",
        "cost": 200,
        "deviceCode": "fix"
      },
      {
        "code": "Transportation is working only hand",
        "name": "موصلات وشغل يد فقط",
        "cost": 150,
        "deviceCode": "fix"
      },
      {
        "code": "Samsung Huawei Sony Tab HTC",
        "name": "سامسونج هواوي سوني تاب HTC",
        "cost": null,
        "deviceCode": "fix"
      },
      {
        "code": "other",
        "name": "خدمة أخرى",
        "cost": 0,
        "deviceCode": "fix"
      },
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    painting: [
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    'furniture-transportation': [
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    cleaning: [
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    paved: [
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    cameras: [
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
    reconditioning: [
      {
        code: 'other',
        name: 'خدمة اخرى',
        cost: 0,
      },
    ],
  },
};

const PAYMENT_STATUSES = {
  requested: {
    ar: 'الفنى اصدر الفاتورة',
    en: 'requested',
  },
  rejected: {
    ar: 'العميل رفض الفاتورة',
    en: 'rejected',
  },
  proceed: {
    ar: 'العميل وافق على الفاتورة',
    en: 'proceed',
  },
  done: {
    ar: 'تم تقفيل الفاتورة',
    en: 'done',
  },
  paid: {
    ar: 'تم الدفع',
    en: 'done',
  },
  updatedByAdmin: {
    ar: 'تم تعديل الفاتورة من قبل الادمن',
    en: 'updated By Admin',
  },
};

const RESIDENCE_TYPE = {
  personal: {
    ar: 'فردى',
    en: 'personal',
  },
  professional: {
    ar: 'مهنى',
    en: 'professional',
  },
};

const NATIONALITY = {
  KSA: {
    ar: 'سعودى',
    en: 'KSA',
  },
  other: {
    ar: 'جنسية اخرى',
    en: 'other',
  },
};

const DISCOUNT_TYPES = [
  {
    name: {
      ar: 'نسبة مئوية',
      en: 'percentage',
    },
    value: 'percentage',
  },
  {
    name: {
      ar: 'قيمة ثابتة',
      en: 'Fixed value',
    },
    value: 'fixed',
  },
  {
    name: {
      ar: 'كاش باك',
      en: 'cash back',
    },
    value: 'cashBack',
  },
  {
    name: {
      ar: 'كاش باك بنسبة مئوية',
      en: 'cash back percentage',
    },
    value: 'cashBackPercentage',
  },
];

export {
  COOKIES_KEYS,
  USER_ROLES,
  USER_TARGETS,
  OPERATING_SYSTEMS,
  CONFIRM_CHOICE,
  FIELDS,
  ACTIVE_STATUS,
  RATINGS,
  CITIES,
  USER_STATUSES,
  REQUESTS_STATUSES,
  TICKETS_STATUS,
  PAYMENT_STATUS,
  PAYMENT_METHOD,
  CANCELLATION_REASONS,
  TRANSACTION_TYPES,
  TRANSACTION_STATUS,
  COMMISSION_RATE,
  TRANSACTION_SIGN,
  REVIEWS_TAGS,
  ARRIVAL_ACTIVITY,
  DARK_MODE_MAP_STYLE,
  TIME_UNITES,
  GENDER,
  ACTIVITIES_LOGS,
  GROUPED_TRANSACTION_TYPES,
  CLASSIFICATIONS_CODE_FOR_GROUPED_TRANSACTION_TYPES,
  POSTPONE_REASONS,
  ACTIONS,
  WORK_LIST_FOR_EVERY_SERVICE,
  CANCELLATION_REASON_TYPES,
  PAYMENT_STATUSES,
  RESIDENCE_TYPE,
  NATIONALITY,
  DISCOUNT_TYPES,
};
